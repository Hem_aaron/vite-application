//
//  AppDelegate.swift
//  Vite
//
//  Created by Hem Poudyal on 12/4/17.
//  Copyright © 2017 Eepos IT. All rights reserved.
//

import UIKit
import CoreData
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import IQKeyboardManagerSwift
import MapKit
import Fabric
import Crashlytics
import BRYXBanner
import FirebaseDatabase
import FirebaseAuth
import Firebase
import UberRides
import Siren
import Stripe
import SafariServices
import Branch
import TwitterKit
import OAuthSwift
import SlideMenuControllerSwift
import UserNotifications
import Braintree


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UNUserNotificationCenterDelegate{
     var timer : Timer!
    var window: UIWindow?
    let isLocationEnabled = CLLocationManager.locationServicesEnabled()
    var user = UserDefaultsManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        user.isBusinessAccountActivated = false
        configureApp()
        Twitter.sharedInstance().start(withConsumerKey: "1vP5IwtGBKPYLRcXKDy1njmYQ", consumerSecret: "s3bpETjromq6Ltp3aw8DkIuV8ixEM4eHGj87DJLOsdvyeFaGuI")
       // LyftConfiguration.developer = (token: "zxaoCiFhJR2zXb//4pWnvv85U9cNe+dpeNaEmROJvHFq5ql0ETbyNfXYlgDQvrEW9US75ZCuxNx6FGQaowUpMYulNG3G6VAq0ROiuISsrbr1G2f9UOuZmpI=", clientId: "baVq6TSFajbw")
        
        Fabric.with([Branch.self, Crashlytics.self, STPAPIClient.self, Twitter.self])
        
        configureStripe()
        
        configureInitialViewController()
        //VServices.updateDeviceToken(nil, failureBlock: nil)
        configureIQKeyboard()
        self.registerForPushNotification(application: application)
        Branch.getInstance().setDebug()
        // listener for Branch Deep Link data
        
        Branch.getInstance().initSession(launchOptions: launchOptions) { (params, error) in
            guard error == nil else { return }
            guard let param = params else {return}
            guard let userDidClick = params!["+clicked_branch_link"] as? Bool else { return }
                if userDidClick {
                    let nc = NotificationCenter.default
                    if let isFromHome = param["isFromHome"] as? String {
                        if isFromHome == "true"{
                            nc.post(name: NSNotification.Name(rawValue: kNotificationBranchEventShare), object: nil, userInfo: param)
                        }
                    } else {
                        nc.post(name: NSNotification.Name(rawValue: kNotificationBranchShare), object: nil, userInfo: param)
                    }
                }
            // do stuff with deep link data (nav to page, display content, etc)
            print(params as? [String: AnyObject] ?? {})
        }
        BTAppSwitch.setReturnURLScheme("com.eeposit.Vite.payments")

        return true
    }

    
    func application(_ application: UIApplication,
                     continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([Any]?) -> Void) -> Bool{
        // handler for Universal Links
        Branch.getInstance().continue(userActivity)
        return true
    }
    

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if (url.host == "oauth-callback") {
            OAuthSwift.handle(url: url)
        }
        if url.scheme == "li4894335"{
            return MGLinkedinSDKWrapper.application(application, open: url as URL, sourceApplication: sourceApplication, annotation: annotation as AnyObject)
        }
        Branch.getInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if url.scheme?.localizedCaseInsensitiveCompare("com.eeposit.Vite.payments") == .orderedSame {
            return BTAppSwitch.handleOpen(url, options: options)
        }
        FBSDKApplicationDelegate.sharedInstance().application(
            app,
            open: url,
            sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String,
            annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        return Twitter.sharedInstance().application(app, open: url, options: options)
    }
    
    // this count is shown on the appicon badge
    func getBatchCount() {
        NotificationServices.getNotificationCount({ (count) in
            let application = UIApplication.shared
            application.applicationIconBadgeNumber = count
        }) { (msg) in
            print(msg)
        }
    }
    
    // Push Notification
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        getBatchCount()
        print(userInfo)
        if let notification = userInfo["aps"] as? NSDictionary,
            let alert = notification["alert"] as? String {
            print(alert)
            if let customMessage = userInfo["customMessage"] as? String{
                print(customMessage)
                if customMessage.contains("/")
                {
                    //If notification is from our server
                    let customMessageArr = customMessage.components(separatedBy: "/")
                    let userDefaults = UserDefaults.standard
                    
                    
                    if  !customMessageArr[0].isEmpty {
                        let params = customMessageArr[0].components(separatedBy: ":")//componentsSeparatedByString(":")
                        let eventIdFromNotification = params[1]
                        userDefaults.set(eventIdFromNotification, forKey: "eventIdFromNotification")
                    }
                    
                    if customMessageArr.count > 2 {
                        //if the notification is related to event
                        //let eventName = customMessageArr[1].componentsSeparatedByString(":")
                        let eventType = customMessageArr[2].components(separatedBy: ":")
                        let eventIdParam =   customMessageArr[1].components(separatedBy: ":")
                        // for notification from comment
                        if eventType[1] == "comment" {
                            let mediaType = customMessageArr[3].components(separatedBy: ":")
                            let mediaUrl = customMessageArr[4].components(separatedBy: ":")
                            let feedId = customMessageArr[5].components(separatedBy: ":")
                            let finalUrl = mediaUrl[1].replacingOccurrences(of: "|", with: "/", options: .literal, range: nil)
                            //stringByReplacingOccurrencesOfString("|", withString: "/", options: .LiteralSearch, range: nil)
                            userDefaults.set(mediaType[1], forKey: "mediaTypeFromNotification")
                            userDefaults.set(finalUrl, forKey: "mediaUrlFromNotification")
                            userDefaults.set(feedId[1], forKey: "feedIdFromNotification")
                        }
                        userDefaults.set(eventIdParam[1], forKey: "eventIdFromScreenshotNotification")
                        
                        if (application.applicationState == UIApplicationState.active) {
                            let banner = Banner(title: "", subtitle: alert as String, image: UIImage(named: ""), backgroundColor:  UIColor(rgb: AppProperties.viteAppThemeColor), didTapBlock:{
                                () -> Void in
                                self.handleNotifications(eventType: eventType[1])
                            })
                            banner.dismissesOnTap = true
                            banner.show(duration: 3.0)
                        }else{
                            self.handleNotifications(eventType: eventType[1])
                        }
                        
                        self.NotificationsForIcons(eventType: eventType[1])
                        
                    }
                    else{
                        //if the notification is related to vip request
                        
                        //let eventName = customMessageArr[1].componentsSeparatedByString(":")
                        let type = customMessageArr[1].components(separatedBy: ":")
                        let banner = Banner(title: "", subtitle: alert as String, image: UIImage(named: ""), backgroundColor:  UIColor(rgb: AppProperties.viteAppThemeColor), didTapBlock:{
                            () -> Void in
                            self.handleNotifications(eventType: type[1])
                        })
                        
                        banner.dismissesOnTap = true
                        banner.show(duration: 3.0)
                        
                        self.NotificationsForIcons(eventType: type[1])
                    }
                }
                else if customMessage.contains(":")
                    
                {
                    //if notification is from firebase
                    
                    let customMessageArr = customMessage.components(separatedBy: ":")
                    let type = customMessageArr[1]
                    print(type)
                    if type == "message"
                    {
                        let banner = Banner(title: "", subtitle: alert as String, image: UIImage(named: ""), backgroundColor:  UIColor(rgb: AppProperties.viteAppThemeColor), didTapBlock:{
                            () -> Void in
                            self.handleNotifications(eventType: alert)
                        })
                        banner.dismissesOnTap = true
                        banner.show(duration: 3.0)
                        self.NotificationsForIcons(eventType: type)
                        //self.handleNotifications(eventType: "message")
                      
                    }
                }
            }
            else{
                if (application.applicationState == UIApplicationState.active) {
                    let banner = Banner(title: "", subtitle: alert as String, image: UIImage(named: ""), backgroundColor:  UIColor(rgb: AppProperties.viteAppThemeColor), didTapBlock:{
                        () -> Void in
                        self.handleNotifications(eventType: alert)
                    })
                    banner.dismissesOnTap = true
                    banner.show(duration: 3.0)
                }else{
                    self.handleNotifications(eventType: alert)
                }
                
                self.NotificationsForIcons(eventType: alert)
               
                // do nothing for now
                
            }
            
            completionHandler(UIBackgroundFetchResult.noData)
        }

    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Couldn't register: \(error)")

    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print(token)
        let user = UserDefaultsManager()
        user.userDeviceToken = String(token)
      //  UserDefaults.standard.set(filterDeviceTokenOfSpaceAndAngles(deviceToken: deviceToken), forKey: UserDefaultsKey.deviceToken)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kTokenReceived"), object: nil)
    }
    

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void) {
        
    }

    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void) {
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}
extension AppDelegate {
    
    func configureApp() {
        FIRApp.configure()
    }
    
    func configureIQKeyboard() {
        IQKeyboardManager.sharedManager().enable = true
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436,2488:
                print("iPhone X")
                IQKeyboardManager.sharedManager().keyboardDistanceFromTextField = 35
                break
            default:
                print("unknown")
            }
        }
        //IQKeyboardManager.sharedManager().disableToolbarInViewControllerClass(ChatMainViewController)
    }
    
    
    func configureInitialViewController() {
        var loginStatus = Bool()
        var controller:UIViewController?
         let walkthroughSeen = UserDefaults.standard.bool(forKey: "walkthroughSeen")
         print(walkthroughSeen)
        if let loginState = UserDefaultsManager().loginSuccessStatus {
            loginStatus = loginState
        }
        print(UserDefaultsManager().userAccessToken)
        if  let _ = UserDefaultsManager().userAccessToken , let _ = UserDefaultsManager().userFBID {
            SlideMenuOptions.contentViewDrag = true
            if isLocationEnabled {
                if loginStatus{
                    SlideMenuOptions.contentViewDrag = true
                    let slideMenuController = SlideMenuController(mainViewController: Route.masterViewController, leftMenuViewController: Route.menuViewController)
                    self.window?.rootViewController = slideMenuController
                    self.window?.makeKeyAndVisible()
                } else {
                     controller = Route.signUpViewController
                    SlideMenuController().changeMainViewController(controller!, close: false)
                    self.window?.rootViewController = controller
                }
            } else {
                 controller = Route.noLocationViewController
                SlideMenuController().changeMainViewController(controller!, close: false)
                self.window?.rootViewController = controller
            }
        }
        else{
            if !walkthroughSeen {
                controller =  Route.walkthrough
                 self.window?.rootViewController = controller
            } else {
                controller = Route.signUpViewController
                 self.window?.rootViewController = controller
            }
//            let controller = Route.signUpViewController
//            SlideMenuController().changeMainViewController(controller, close: false)
//            self.window?.rootViewController = controller
        }
        
    }
    // Push Notification
    func handleNotifications(eventType : String){
        let slideMenuController = SlideMenuController(mainViewController: Route.masterViewController, leftMenuViewController: Route.menuViewController)
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(notificationFlow(_:)), userInfo: eventType, repeats: false)
    }
    
    @objc func notificationFlow(_ timmer: Timer) {
        //handle anyway you like
        let notificationCenter = NotificationCenter.default
        let eventType = timmer.userInfo as! String
        if(eventType == "createEvent"){
            //notification to reload card on home
            notificationCenter.post(name: NSNotification.Name(rawValue: "ViteNowReceived"), object: nil)
        }
        else if(eventType == "myVite" || eventType ==  "updateEvent")
        {
            notificationCenter.post(name: NSNotification.Name(rawValue: "DirectToMyVites"), object: nil)
        }
        else if(eventType == "VIPRequest"){
            notificationCenter.post(name: NSNotification.Name(rawValue: "DirectToAddMyVip"), object: nil)
        }
        else if(eventType == "VIPList"){
            notificationCenter.post(name: NSNotification.Name(rawValue: "DirectToMyVip"), object: nil)
        }
        else if(eventType == "ScreenshotTaken"){
            notificationCenter.post(name: NSNotification.Name(rawValue: "DirectToMyAttendee"), object: nil)
        }
        else if(eventType == "comment"){
            notificationCenter.post(name: NSNotification.Name(rawValue: "a"), object: nil)
        }
        else if(eventType == "rate"){
            notificationCenter.post(name: NSNotification.Name(rawValue: "DirectToMyProfile"), object: nil)
        }
        else {
            notificationCenter.post(name: NSNotification.Name(rawValue: "DirectToMsgCenter"), object: nil)
        }
    }
    
    func registerForPushNotification(application:UIApplication) {
            if #available(iOS 10, *) {
                let center = UNUserNotificationCenter.current()
                center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                    // Enable or disable features based on authorization.
                }
                application.registerForRemoteNotifications()
            }
                // iOS 9 support
            else if #available(iOS 9, *) {
                UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
                UIApplication.shared.registerForRemoteNotifications()
            }
                // iOS 8 support
            else if #available(iOS 8, *) {
                UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
                UIApplication.shared.registerForRemoteNotifications()
            }
                // iOS 7 support
            else {
                application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
            }
        }
    
    
    func NotificationsForIcons(eventType : String){
        let userDefaults = UserDefaults.standard
        let notificationCenter = NotificationCenter.default
        
        if(eventType == "myVite" || eventType ==  "updateEvent")
        {
            userDefaults.set(true, forKey: "myViteNotification")
            notificationCenter.post(name: NSNotification.Name(rawValue: "customNotificationReceived"), object: nil)
        }
        else if(eventType == "myEvent" || eventType ==  "message")
        {
            userDefaults.set(true, forKey: "msgCenterNotification")
            notificationCenter.post(name: NSNotification.Name(rawValue: "customNotificationReceived"), object: nil)
        }
        else{
        }
    }
    
    func configureStripe(){
        #if DEVELOPMENT
        Stripe.setDefaultPublishableKey("pk_test_oCCKkG41xNuR5DxWuNAe3Qf1")
        #else
        Stripe.setDefaultPublishableKey("pk_live_wowzGUNabWag4LN3N8rc5FFu")
        #endif
        
    }

}
