//
//  ButtonWithRoundedBorder.swift
//  Vite
//
//  Created by Ujjwal Shrestha on 11/6/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class ButtonWithRoundedBorder: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 2
        self.layer.cornerRadius = 10
    }

}
