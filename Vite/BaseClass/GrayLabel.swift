//
//  GrayLabel.swift
//  Vite
//
//  Created by sajjan giri on 11/10/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation

class GrayLabel : UILabel  {

    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.textColor = UIColor.colorFromRGB(rgbValue: 0x7f7f7f)
    }

}
