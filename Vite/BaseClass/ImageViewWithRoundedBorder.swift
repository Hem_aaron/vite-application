//
//  ImageViewWithRoundedBorder.swift
//  Vite
//
//  Created by Eeposit1 on 2/16/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class ImageViewWithRoundedBorder: UIImageView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
    }

}
