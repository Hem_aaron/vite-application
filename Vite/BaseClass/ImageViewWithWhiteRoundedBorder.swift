//
//  ImageViewWithWhiteRoundedBorder.swift
//  Vite
//
//  Created by Eeposit1 on 2/16/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class ImageViewWithWhiteRoundedBorder: UIImageView {

    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 2.0
        self.clipsToBounds = true
    }

}
