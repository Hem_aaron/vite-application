//
//  SocialMediaCustomButton.swift
//  Vite
//
//  Created by eeposit2 on 1/17/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class SocialMediaCustomButton: UIButton {
    var networkId: String?
    var networkUserName: String?
    var networkUrl: String? 
}
