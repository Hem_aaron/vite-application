//
//  WrapperBlueNavControllerViewController.swift
//  Vite
//
//  Created by Ujjwal Shrestha on 11/6/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class WrapperBlueNavControllerViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let nav = self.navigationController {
            nav.navigationBar.backgroundColor = UIColor.blue
            nav.navigationBar.barTintColor = UIColor(red: 6/255, green: 164/255, blue: 233/255, alpha: 1.0)

            nav.navigationBar.isTranslucent                  = true
            nav.view.backgroundColor                       = UIColor.blue
            self.navigationItem.hidesBackButton            = true
            self.navigationController?.isNavigationBarHidden = false
            let navBar                                     = self.navigationController!.navigationBar
            navBar.titleTextAttributes                     = [NSAttributedStringKey.foregroundColor: UIColor.white]
            nav.navigationBar.isTranslucent = false
        }
    }

}
