//
//  WrapperController.swift
//  Vite
//
//  Created by Hem Poudyal on 1/8/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation
import MapKit

class WrapperController: UIViewController, CLLocationManagerDelegate {
    let locationHelper            = LocationManager()
    var manager:CLLocationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        manager.delegate = self
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showNoInternetController(_:)),
                                               name: NSNotification.Name(rawValue: kNotificationNoInternet),
                                               object: nil)
        if let nav = self.navigationController {
            nav.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            nav.navigationBar.shadowImage                  = UIImage()
            nav.view.backgroundColor                       = UIColor.clear
            self.navigationItem.hidesBackButton            = true
            nav.isNavigationBarHidden                      = false
            let navBar                                     = self.navigationController!.navigationBar
            navBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.purple , NSAttributedStringKey.font: UIFont(name: AppProperties.viteFontName , size: AppProperties.viteNavigationTitleFontSize)!]
               // self.automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        guard status == CLAuthorizationStatus.authorizedAlways ||  status == CLAuthorizationStatus.authorizedWhenInUse else {
            let storboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storboard.instantiateViewController(withIdentifier: "NoLocationViewController")
            AppDel.delegate?.window??.rootViewController = controller
            self.manager.delegate = nil
            return
        }
    }
    
    @objc func showNoInternetController(_ notification: NSNotification) {
        let controller = ConnectionErrViewController()
        
        if let nav = self.navigationController {
            nav.present(controller, animated: true, completion: nil)
            return
        }
        
        if let tab = self.tabBarController {
            tab.present(controller, animated: true, completion: nil)
            return
        }
        
        present(controller, animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         NotificationService().removeNotificationObserverFromMetaNode()
    }
}
