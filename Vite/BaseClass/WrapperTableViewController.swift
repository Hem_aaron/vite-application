//
//  WrapperTableViewController.swift
//  Vite
//
//  Created by EeposIT_X on 2/21/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation
import UIKit

class WrapperTableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let nav = self.navigationController {
            nav.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            nav.navigationBar.shadowImage                  = UIImage()
            nav.navigationBar.isTranslucent                  = true
            nav.view.backgroundColor                       = UIColor.clear
            self.navigationItem.hidesBackButton            = true
            self.navigationController?.isNavigationBarHidden = false
            let navBar                                     = self.navigationController!.navigationBar
            navBar.titleTextAttributes                     = [NSAttributedStringKey.foregroundColor: UIColor.purple , NSAttributedStringKey.font: UIFont(name: AppProperties.viteFontName, size: AppProperties.viteNavigationTitleFontSize)!]
        }
    }
    
}
