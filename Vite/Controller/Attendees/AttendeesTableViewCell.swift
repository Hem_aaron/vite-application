//
//  AttendeesTableViewCell.swift
//  Vite
//
//  Created by Bibhut on 2/26/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class AttendeesTableViewCell: UITableViewCell {

    @IBOutlet weak var attendeeFullName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgTick: UIImageView!
    @IBOutlet weak var lblCheckedIn: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgProfile.layer.cornerRadius = 25.0
        imgProfile.clipsToBounds = true

    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    


}
