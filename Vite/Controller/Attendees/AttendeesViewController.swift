//
//  AttendeesViewController.swift
//  Vite
//
//  Created by Bibhut on 2/26/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import SDWebImage

protocol AttendeeDelegate : AnyObject {
    func displayAttendeeCount(_ count: Int)
}

class AttendeesViewController: WrapperController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: Outlets
    
    @IBOutlet weak var noAttendeeLbl: UILabel!

    @IBOutlet weak var tblView: UITableView!
    //MARK: Variables Used
    var event : EventsOrganizer?
   weak var delegate : AttendeeDelegate?
    // from screenshot notification server provides eventId
    var eventIdFromNotification = String()
    var eventId = String()
    var userArray : [MyEventAttendee]? = [MyEventAttendee]()
    var tickImage = true
    let indicator = ActivityIndicator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ServiceCall(eventId)
        tblView.dataSource = self
        tblView.delegate = self
    }
    
    //MARK: Service Call
    
    func ServiceCall(_ id: String){
        self.userArray?.removeAll()
        self.indicator.start()
        self.view.isUserInteractionEnabled = false
        AttendeeService.getAllAttendeeList(eventId: id, successBlock: { [weak self] (users) in
            if users.count != 0 {
                self?.view.isUserInteractionEnabled = true
                for user in users{
                    self?.userArray?.append(user)
                }
                self?.noAttendeeLbl.isHidden = true
            }else{
                self?.noAttendeeLbl.isHidden = false
                
            }
            if let user = self?.userArray  {
                self?.delegate?.displayAttendeeCount(user.count)
            }
            self?.tblView.reloadData()
            self?.indicator.stop()
            
        }) { [weak self] (message) in
            self?.view.isUserInteractionEnabled = true
            showAlert(controller: self!, title: "", message: message)
            self?.indicator.stop()
        }
    }
    
    
    // MARK: Table View DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let arr = userArray {
            return arr.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    
    // MARK: Table view Delegates
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttendeesCell") as! AttendeesTableViewCell
        let attendee = self.userArray![indexPath.row]
        cell.attendeeFullName.text = attendee.fullName!
        if let profileUrl = attendee.profileImageUrl{
            let url = profileUrl.convertIntoViteURL()
            cell.imgProfile.sd_setImage(with: URL(string: url), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
        }
        cell.imgTick.isHidden = attendee.qrScanned! || attendee.manualScanned! || attendee.screenShotTaken! ? false : true
        if let screenShot = attendee.screenShotTaken{
            cell.imgTick.image = screenShot ? UIImage(named: "attendeeScreenshot") : UIImage(named: "attendeeTick")
        }
        cell.lblCheckedIn.isHidden =  attendee.qrScanned! || attendee.manualScanned! ? false  : true
        if let checkedInTime = attendee.qrScannedDate{
            cell.lblCheckedIn.text = getCheckedInTimeFromTimestamp(checkedInTime)
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //delete attendee by the organizer
            let attendee = self.userArray![indexPath.row]
            if let fbId = attendee.facebookId{
                  removeAttendee(fbId)
            }
            }else{
            showAlert(controller: self, title: "", message:localizedString(forKey: "CANNOT_REMOVE_CHECKEDIN_ATTENDEE"))
            }
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
          let attendee = self.userArray![indexPath.row]
            return attendee.qrScanned! || attendee.manualScanned! ? false : true
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! AttendeesTableViewCell
        let attendee = self.userArray![indexPath.row]
        
        // if manually scanned i.e not scanned from qr, then organizer can tick or untick the attendee
        if !attendee.qrScanned! && !attendee.screenShotTaken! {
            let msg =  attendee.manualScanned! ? "unmark" : "mark"
            let alert = UIAlertController(title: "",
                                          message: " Would you like to \(msg) the attendee ? ",
                                          preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Yes",
                                         style: .default,
                                         handler: { [weak self] (action:UIAlertAction) -> Void in
                                            self?.tickAttendee(attendee, successBlock: { (status) in
                                                self?.ServiceCall((self?.eventId)!)
                                                cell.imgTick.isHidden =  !cell.imgTick.isHidden
                                            })
                                            
            })
            
            let cancelAction = UIAlertAction(title: "No",
                                             style: .default) {
                                                (action: UIAlertAction) -> Void in
                                                
            }
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            present(alert,
                                  animated: false,
                                  completion: nil)
        }
    }
    
    //MARK: Private Methods
    
    func removeAttendee(_ fbId: NSNumber){
        let alert = UIAlertController(title: "",
                                      message: localizedString(forKey: "REMOVE_THIS_ATTENDEE"),
                                      preferredStyle: .alert)

        let okAction = UIAlertAction(title: "Yes",
                                     style: .default,
                                     handler: { [weak self] (action:UIAlertAction) -> Void in
                                        EventServices.respondToRequest(status: false, userFbID: String(describing: fbId), eventID: (self?.eventId)!, completionBlock: { (status) in
                                            self?.ServiceCall((self?.eventId)!)
                                            showAlert(controller: self!, title: localizedString(forKey: "SUCCESS"), message: localizedString(forKey: "USER_REMOVED_SUCCESSFULLY"))
                                        })
        })

        let cancelAction = UIAlertAction(title: "No",
                                         style: .default) {
                                            (action: UIAlertAction) -> Void in
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert,
                              animated: false,
                              completion: nil)
        

    }
    
    
    
    func  tickAttendee(_ attendee: MyEventAttendee, successBlock:@escaping (_ status: Bool) -> Void) {
        
        guard let fbID = attendee.facebookId, let status = attendee.manualScanned else {
            return
        }
    
        let url = kAPIChangeQrScanStatus + "\(eventId)/" + "\(fbID)/" + "\(!status)"
        self.view.isUserInteractionEnabled = false
        
        WebService.request(method: .put, url: url, success: { [weak self] (response) in
            self?.view.isUserInteractionEnabled = true
            guard let response = response.result.value as? [String:AnyObject] else {
                return
            }
            print(response)
            guard let success = response["success"] as? Bool else{
                return
            }
            if success {
                successBlock(true)
            }
        }, failure: { [weak self] (message, err) in
            self?.view.isUserInteractionEnabled = true
            showAlert(controller: self!, title: localizedString(forKey: "ERROR"), message: message)
        })
    }
    
    func getCheckedInTimeFromTimestamp(_ timeStamp: NSNumber) -> String{
        let date = Date(timeIntervalSince1970: timeStamp.doubleValue / 1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm a"
        let strDate = dateFormatter.string(from: date)
        let checkedInTime = strDate.split{$0 == " "}.map(String.init)
        return "Checked in at \(checkedInTime[1]) \(checkedInTime[2])"
    }
}

//MARK: IBActions

extension AttendeesViewController {
    
    @IBAction func back(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
