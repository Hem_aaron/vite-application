//
//  SwipeAttendee.swift
//  Vite
//
//  Created by EeposIT_X on 2/28/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation
import UIKit
import Koloda

class SwipeAttendee: KolodaView{
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblGender_Age: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        Bundle.main.loadNibNamed("UserSwipe", owner: self, options: nil)
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("UserSwipe", owner: self, options: nil)
        view.frame = self.bounds
        self.addSubview(view)
    }


}
