    //
    //  AddAdminsVC.swift
    //  Vite
    //
    //  Created by yaman on 10/26/16.
    //  Copyright © 2016 EeposIT_X. All rights reserved.
    //
    
    import UIKit
    
    protocol SendDataDelegate {
        func sendData(_ data:[String],users:[ViteUser])
        
    }
    
    class AddAdminsVC: WrapperController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{
        
        @IBOutlet weak var addAdminTblview: UITableView!
        @IBOutlet weak var searchBar: UISearchBar!
        var friendsArr:[ViteUser]? = [ViteUser]()
        var filteredAdminIndex:[Int] = [Int]()
        let indicator = ActivityIndicator()
        var arrayOfUserFbId = [String]()
        var arrayOfAddedUser = [ViteUser]()
        var selectedIndex = [String]()
        var delegate: SendDataDelegate!
        var vipListImg = [String]()
        var memberList = [String]()
        var pageNo = Int()
        let viteUserDataController = AllViteUsersService()
        var memberCount = Int()
        //Search variables
        var isSearching                      = false
        var globalSearchText = String()
        var noUser = false
        
        
        //MARK: OBJECT METHODS
        override func viewDidLoad() {
            super.viewDidLoad()
            addAdminTblview.delegate = self
            addAdminTblview.dataSource = self
            self.navigationItem.title = "Add Admin"
            searchBar.delegate = self
           // fetchVipFriendList()
        }
        
        
        //MARK: Private Methods
        
        //MARK: Private METHODS
        
        @objc func getUserFbId (_ sender:UIButton) {
            let data = self.friendsArr?[sender.tag]
            if data != nil{
                if arrayOfUserFbId.contains(String(describing:data!.facebookId!)){
                    if let index = self.arrayOfUserFbId.index(of: String(describing: data!.facebookId!)){
                        self.arrayOfUserFbId.remove(at: index)
                        self.arrayOfAddedUser.remove(at: index)
                    }
                }
                else{
                    self.arrayOfAddedUser.append(data!)
                    self.arrayOfUserFbId.append(String(describing:data!.facebookId!))
                }
                self.addAdminTblview.reloadData()

            }
        }
        
        
        
        //MARK: TABLEVIEW DELEGATE AND DATASOURCE METHODS

        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return (self.friendsArr?.count)!
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let data = self.friendsArr?[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "AllUserCell", for: indexPath) as! AllUserTableViewCell
            
            if data != nil{
                cell.userNameLbl.text = "\(data!.fullName!)"
                cell.imgProfile.sd_setImage(with: URL(string: data!.profileImgUrl!.convertIntoViteURL()))
                // Do something!
                cell.addButton.tag = indexPath.row
                cell.addButton.addTarget(self, action:#selector(getUserFbId), for: .touchUpInside)
                if(arrayOfUserFbId.contains(String(describing:data!.facebookId!))){
                    cell.addButton.setImage(UIImage(named: "AddedMember"), for: UIControlState())
                }else {
                    cell.addButton.setImage(UIImage(named: "AddMember"), for: UIControlState())
                }
            }
     
            return cell
        }

        
        func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            if indexPath.item == ((self.friendsArr?.count)! - 1){
                self.pageNo = self.pageNo + 1
            }
        }
        
        //MARK: - Search Methods Service Call
        
        func searchViteUsers(_ searchText:String){
            pageNo = 1
            self.indicator.start()
            self.viteUserDataController.getViteUsers(pageNo, searchString: searchText,
                                                           successBlock:{ (viteusers , filterUserInPrivateFriendModel) in
                                                            if viteusers.count < 10 {
                                                                self.noUser = true
                                                            }else{
                                                                self.noUser = false
                                                            }
                                                            self.friendsArr = viteusers.filter({ (user) -> Bool in
                                                                return self.memberList.contains(String(describing:user.facebookId!)) ? false : true
                                                            })
                                                            self.addAdminTblview.reloadData()
                                                            self.indicator.stop()
                },
                                                           failureBlock:{ (message) in
                                                            self.indicator.stop()
                                                            showAlert(controller: self, title: "", message: message)
            })
        }


        
        //MARK:- Search Bar Delegates
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
          let trimmedString = searchText.trimmingCharacters(in: CharacterSet.whitespaces)
            if trimmedString.isEmpty {
                let delay = 1 * Double(NSEC_PER_SEC)
                let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: time) {
                    self.friendsArr?.removeAll()
                    self.addAdminTblview.reloadData()
                }
            }else{
                self.searchViteUsers(trimmedString)
            }
        }
        
        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            searchBar.resignFirstResponder()
        }

    }
    
       //MARK: IBACTION METHODS
    extension AddAdminsVC{
        @IBAction func back(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)
        }
        
        @IBAction func sendUserList(_ sender: Any) {
            self.delegate.sendData(self.arrayOfUserFbId,users:self.arrayOfAddedUser)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    
    
