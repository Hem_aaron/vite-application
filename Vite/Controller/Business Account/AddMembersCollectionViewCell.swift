//
//  AddMembersCollectionViewCell.swift
//  Vite
//
//  Created by yaman on 10/27/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class AddMembersCollectionViewCell:UICollectionViewCell{
    
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var deleteBtn: UIButton!
}
