//
//  AddAdminsTableViewCell.swift
//  Vite
//
//  Created by yaman on 10/26/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit


class AllUserTableViewCell: UITableViewCell {

    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var imgProfile: CircleImage!
    @IBOutlet weak var addButton: UIButton!
    
}
