//
//  BusinessPlanCell.swift
//  Vite
//
//  Created by EeposIT_X on 8/4/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class BusinessPlanCell: UITableViewCell {

    @IBOutlet weak var lblOffer: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
