//
//  BusinessPlansVC.swift
//  Vite
//
//  Created by EeposIT_X on 8/4/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit


class BusinessPlansVC: WrapperBlueNavControllerViewController {
    
    @IBOutlet weak var tableView: UITableView!
     var schemeList:[String] = ["One Members","Two Members","Three Members","Eight Members"]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
    }

    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return schemeList.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        
        let cellIdentifier = "BusinessPlanCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! BusinessPlanCell
        cell.layer.cornerRadius = 15
        cell.lblTitle.text = schemeList[indexPath.row]

        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
//        
//        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("createBusinessACVC") as! CreateBusinessAccountVC
//        self.navigationController?.pushViewController(controller, animated: true)

    }


}
