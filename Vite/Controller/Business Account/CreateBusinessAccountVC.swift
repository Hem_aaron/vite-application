//
//  CreateBusinessAccountVC.swift
//  Vite
//
//  Created by EeposIT_X on 8/8/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.


import UIKit
import MapKit


class CreateBusinessAccountVC: WrapperController, UICollectionViewDelegate, UICollectionViewDataSource,SendDataDelegate {
    
    @IBOutlet weak var tfLocation: CustomTextField!
    @IBOutlet weak var txtWebUrl: CustomTextField!
    @IBOutlet weak var txtName: CustomTextField!
    @IBOutlet weak var btnStripe: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblLocation: UILabel!
    var imageHelper:ImageHelper!
    var selectedIndexPath: IndexPath!
    var userImageLists       = [UIImage]()
    var memberList = [String]()
    var memberImageUrlList = [String]()
    var layoutSubviewsCalled = false
    var locationManager:LocationManager? = LocationManager()
    var userLocation:CLLocationCoordinate2D?
    var businessImageUrls = [[String: AnyObject]]()
    var cellWidth:CGFloat!
    var imgString = String()
    var lat:Double = 0.0
    var lon:Double = 0.0
    let indicator = ActivityIndicator()
    var accountStatus:Bool = false
    
    @IBOutlet weak var profileCollectionView: UICollectionView!
    @IBOutlet weak var AddMemberCollectionView: UICollectionView!
    
    @IBOutlet weak var profileCollectionViewHeight: NSLayoutConstraint!
    
    
    
    
    //MARK:- Object Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let userDefault = UserDefaults.standard
        if userDefault.bool(forKey: UserDefaultKeys.isStripeConnected){
            self.btnStripe.isEnabled = false
            self.btnStripe.backgroundColor = UIColor.gray
            self.btnStripe.setTitle("Connected With Stripe", for: UIControlState())

        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if layoutSubviewsCalled {
            let collectionViewWidth            = self.profileCollectionView.frame.width
            self.cellWidth                     = (collectionViewWidth - 20) / 3
            self.profileCollectionViewHeight.constant = (self.cellWidth * 2) + 30
            self.profileCollectionView.delegate = self
            self.profileCollectionView.dataSource = self
            self.profileCollectionView.reloadData()
            return
        }
        layoutSubviewsCalled = true
    }
    
    
    
    //MARK: Private Utility Methods
    func configureView(){
        self.scrollView.layer.cornerRadius = 10.0
        self.imageHelper = ImageHelper(controller: self)
        self.AddMemberCollectionView.backgroundColor =   UIColor.colorFromRGB(rgbValue: 0xEFEFF4)
        self.lblLocation.font = lblLocation.font.withSize(12)
        self.navigationItem.title = "Create Account"
        
        self.navigationController!.navigationBar.barTintColor = UIColor.white
    }
    
    func loadData(){
        
    }
    
    
    func validateFormValuesAndGetFormValueAsParameter() -> (status:Bool,message:String) {
        var errorMsg = ""
        if txtName.text!.isEmpty {
            errorMsg = localizedString(forKey: "TITLE_BLANK")
            return (false, errorMsg)
        }
        
        if lblLocation.text!.isEmpty {
            errorMsg = localizedString(forKey: "LOCATION_BLANK")
            return (false, errorMsg)
        }
        if businessImageUrls.count == 0 {
            errorMsg = localizedString(forKey: "SELECT_IMG")
            return (false, errorMsg)
        }
        if accountStatus == false{
            errorMsg = localizedString(forKey: "CONNECT_STRIPE_FOR_PAID_EVENT")
        }
        if (self.txtWebUrl.text!.isEmpty == true) {
            return (true, "")
            
        } else {
            if (isValidURL(self.txtWebUrl.text! as NSString) == false) {
                errorMsg = localizedString(forKey: "ENTER_VALID_URL")
                return (false, errorMsg)
            }
        }
        return (true, "")
    }
    
    
    func createBusinessAccount(){
        self.view.isUserInteractionEnabled = false
        let coord: [String:AnyObject] =
            [
                "givenLocation":(self.lblLocation.text as AnyObject) ,
                "longitude": (self.userLocation?.latitude)! as AnyObject,
                "latitude" : (self.userLocation?.longitude)! as AnyObject
        ]
        let business:[String:AnyObject] = [
            "businessName":self.txtName.text! as AnyObject,
            "description":"" as AnyObject,
            "webUrl":self.txtWebUrl.text! as AnyObject,
            "maxMember":8 as AnyObject,
            "location":coord as AnyObject,
            "businessImages":businessImageUrls as AnyObject]
        
        let parameter:[String:AnyObject]!
        parameter =
            ["fbIdList":self.memberList as AnyObject,
             "business":business as AnyObject,
             "chargeAmount":10 as AnyObject]
        BusinessAccountWebService().createBusinessAccount(parameter, successBlock: { (msg) in
            self.indicator.stop()
            self.view.isUserInteractionEnabled = true
            self.popView()
            showAlert(controller: self, title:localizedString(forKey: "SUCCESS"), message: msg)
        }) { (msg) in
            self.indicator.stop()
            self.view.isUserInteractionEnabled = true
            showAlert(controller: self, title: "", message: msg)
        }
        
    }
    
    
    func showImagePickerSheet() {
        
        let alert = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.purple
        alert.view.backgroundColor = UIColor.white
        let camera = UIAlertAction(title: "Camera",
                                   style: .default,
                                   handler: {
                                    (action:UIAlertAction) -> Void in
                                    self.openImagePicker(.camera)
        })
        
        let gallery = UIAlertAction(title: "Photos from gallery",
                                    style: .default) {
                                        (action: UIAlertAction) -> Void in
                                        self.imageHelper.getImageController(.photoLibrary, isForBusinessAccount: true)
                                        self.imageHelper.imagePicked =
                                            {
                                                image in
                                                self.userImageLists.append(image)
                                                self.profileCollectionView.reloadData()
                                                self.imgString = Base64.getCompressedBase64Image(image)
                                                self.createBusinessImagesUrl(self.imgString)
                                        }
        }
        
        
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel) {
                                            (action: UIAlertAction) -> Void in
        }
        
        alert.addAction(camera)
        alert.addAction(gallery)
        alert.addAction(cancelAction)
        
        present(alert,
                              animated: false,
                              completion: nil)
        
    }
    
    func openImagePicker(_ sourceType: UIImagePickerControllerSourceType){
        
        self.imageHelper.determineStatus {
            (status) -> () in
            guard status else {
                print("Not authenticated to use photo")
                return
            }
            
            //If app is authorized to use the gallery only then load image controller
            self.imageHelper.getImageController(sourceType, isForBusinessAccount: true)
            self.imageHelper.imagePicked = {
                image in
                let ratio = Int(image.size.width/image.size.height);
                let maximumRatioForNonePanorama = 4/3
                if (ratio > maximumRatioForNonePanorama){
                    self.view.makeToast(message: localizedString(forKey: "IMG_NOT_APPROPRIATE_SIZE"))
                    //self.selectedImageView.image = nil
                } else {
                    self.userImageLists.append(image)
                    self.profileCollectionView.reloadData()
                    self.imgString = ""
                    self.imgString = Base64.getCompressedBase64Image(image)
                    self.createBusinessImagesUrl(self.imgString)
                }
                //When the view is editable if the image view's image is changed do not send the image parameter.
                //For that this flag is set.
                // if self.isEditable {
                //   self.isImageChanged = true
                //}
            }
        }
    }
    
    
    func createBusinessImagesUrl(_ image:String) {
        let userImage:[String:AnyObject] =
            [
                "businessImageUrl": image as AnyObject,
                "priority": self.selectedIndexPath.row as AnyObject,
                ]
        self.businessImageUrls.append(userImage)
    }
    
    
    
    //MARK: IBAction Methods
    @IBAction func DeleteImageButton(_ sender: UIButton) {
        let i : Int = (sender.layer.value(forKey: "index")) as! Int
        self.userImageLists.remove(at: i)
        self.profileCollectionView!.reloadData()
    }
    @IBAction func DeleteMemberAction(_ sender: UIButton) {
        let i : Int = (sender.layer.value(forKey: "index")) as! Int
        self.memberList.remove(at: i)
        self.memberImageUrlList.remove(at: i)
        self.AddMemberCollectionView!.reloadData()
    }
    @IBAction func selectLocation(_ sender: UIButton) {
        
        locationManager?.getUserLocation()
        locationManager?.fetchedUserLocation = {
            location in ()
            self.getEventLocation(location)
        }
    }
    
    @IBAction func connectWithStripe(_ sender: AnyObject) {
        let storyboard = UIStoryboard.init(name: "CreateEvent", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "stripeAccountVC")
        self.navigationController?.show(controller, sender: nil)
    }
    
    @IBAction func popView(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCreateAccountAction(_ sender: AnyObject) {
        let response = self.validateFormValuesAndGetFormValueAsParameter()
        if response.status{
            self.indicator.parentView = scrollView
            self.indicator.start()
            self.createBusinessAccount()
        }
        else {
            showAlert(controller: self, title: localizedString(forKey: "REQUIRED"), message: response.message)
        }
    }
    
    @IBAction func btnAddAdminAction(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showAddAdminSegue", sender: nil)
    }
    
    
    
    //MARK: Delegate Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.profileCollectionView{
            return 6
        }
        
        return self.memberList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.profileCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! ImageCollectionViewCell
            cell.layer.cornerRadius = 10.0
            
            if indexPath.row < self.userImageLists.count {
                let image = userImageLists[indexPath.row]
                
                cell.profileImage.image = image
                cell.deleteImageButton.isHidden = false
                cell.cameraLogoImageView.isHidden = true
                
                
            } else {
                cell.profileImage.image = nil
                cell.deleteImageButton.isHidden = true
                cell.cameraLogoImageView.isHidden = false
            }
            cell.deleteImageButton?.layer.setValue(indexPath.row, forKey: "index")
            cell.deleteImageButton?.addTarget(self, action: #selector(CreateBusinessAccountVC.DeleteImageButton(_:)), for: UIControlEvents.touchUpInside)
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "profileImgCell", for: indexPath) as! AddMembersCollectionViewCell
            cell.profileImg.layer.cornerRadius = cell.profileImg.frame.size.width / 2
            cell.profileImg.clipsToBounds = true
            cell.profileImg.backgroundColor = UIColor.white
            cell.profileImg.sd_setImage(with: URL(string: self.memberImageUrlList[indexPath.row].convertIntoViteURL()))
            cell.deleteBtn?.layer.setValue(indexPath.row, forKey: "index")
            cell.deleteBtn?.addTarget(self, action: #selector(CreateBusinessAccountVC.DeleteMemberAction(_:)), for: UIControlEvents.touchUpInside)
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.profileCollectionView{
            let cell = collectionView.cellForItem(at: indexPath) as! ImageCollectionViewCell
            if cell.profileImage.image == nil {
                self.selectedIndexPath = indexPath
                self.showImagePickerSheet()
            }
        }
        
    }
    //Use for size
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.profileCollectionView {
            return CGSize(width: self.cellWidth, height: self.cellWidth)
        } else {
            return CGSize(width: 52, height: 50)
        }
    }
    
    
    //MARK : LOCATION DELEGATE METHODS
    func getEventLocation (_ eventLocation: CLLocationCoordinate2D){
        let controller = Route.mapController
        controller.currentCoordinateOfAnnotation = {
            coordinate in
            self.userLocation = coordinate
            self.indicator.parentView = self.lblLocation
            self.indicator.tintColor = UIColor.gray
            self.indicator.start()

            LocationManager.getLocationStringFromCoordinate(coordinate: coordinate,
                                                            completionBlock: {
                                                                (location) -> Void in
                                                                self.lblLocation.text = location ?? ""
                                                                self.tfLocation.placeholder = ""
                                                                self.indicator.stop()
            })
        }
        controller.currentLocation = eventLocation
        self.show(controller, sender: nil)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showAddAdminSegue"{
            let destinationController = (segue.destination as! AddAdminsVC)
            destinationController.delegate = self
            destinationController.memberList = self.memberList
        }
        
    }
    
    //MARK: SendDataDelegate methods
    func sendData(_ data:[String],users:[ViteUser]){
        for url in data {
            self.memberImageUrlList.append("http://graph.facebook.com/\(url)/picture?type=large")
        }
        
        for fbID in data {
            self.memberList.append(fbID)
        }
        let uniqueMemberArr = Array(Set(self.memberList))
        let uniqueMemberImgArr = Array(Set(self.memberImageUrlList))
        self.memberList = uniqueMemberArr
        self.memberImageUrlList = uniqueMemberImgArr
        
        self.AddMemberCollectionView.reloadData()
    }
    
    
}




