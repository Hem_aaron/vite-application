//
//  EditBusinessAccountMemberCollectionViewCell.swift
//  Vite
//
//  Created by sajjan giri on 11/24/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class EditBusinessAccountMemberCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var addMemberImg: CircleImage!
    @IBOutlet weak var deletebtn: UIButton!
}
