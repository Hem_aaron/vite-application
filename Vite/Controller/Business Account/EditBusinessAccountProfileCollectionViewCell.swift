//
//  EditBusinessAcoountProfileCollectionViewCell.swift
//  Vite
//
//  Created by sajjan giri on 11/24/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class EditBusinessAccountProfileCollectionViewCell: UICollectionViewCell {
    var deleteAction:((_ indexPath:Int) -> ())!
    
    var row:Int!
    @IBOutlet weak var cameraLogo: UIImageView!
    @IBOutlet weak var deletebtn: UIButton!
    
    @IBOutlet weak var businessProfileImg: ImageViewWithRoundedBorder!
    
    @IBAction func deleteImage(_ sender: AnyObject) {
        if let deleteAction = self.deleteAction {
            deleteAction(row);
        }
    }
}
