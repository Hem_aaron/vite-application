//
//  EditBusinessAccountViewController.swift
//  Vite
//
//  Created by sajjan giri on 11/24/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import MapKit

protocol GetBusinessUserInfoDelegate {
    func getBusinessUserInformation()
}

class EditBusinessAccountViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource, SendDataDelegate ,UITextFieldDelegate{
    
    var delegate: GetBusinessUserInfoDelegate?
    var imageHelper:ImageHelper!
    var memberList = [String]()
    var memberImageUrlList = [String]()
    var userLocation:CLLocationCoordinate2D?
    var businessAccounts : BusinessAccountUserModel!
    var locationManager:LocationManager? = LocationManager()
    var businessImageArr = [BusinessImageModel]()
    var imageEntityIds = [String]()
    var memberAdminStatus = [Bool]()
    var imgString = String()
    var businessImageUrls = [[String: AnyObject]]()
    var selectedIndex = Int()
    var selectedIndexPath: Int!
    let indicator = ActivityIndicator()
    var userImageList = [AnyObject]()
    var layoutSubviewsCalled = false
    var cellWidth:CGFloat!
    @IBOutlet weak var businessCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var businessProfileCollectionView: UICollectionView!
    @IBOutlet weak var businessMemberCollectionView: UICollectionView!
    @IBOutlet weak var txtName: CustomTextField!
    @IBOutlet weak var txtLocation: CustomTextField!
    @IBOutlet weak var txtWebUrl: CustomTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //   print(businessAccounts.businessImages?.toJSON())
        
        
        self.imageHelper                  = ImageHelper(controller: self)
        // Do any additional setup after loading the view.
        loadData()
        // configureView()
        self.txtLocation.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureView()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if layoutSubviewsCalled {
            let collectionViewWidth            = self.businessProfileCollectionView.frame.width
            self.cellWidth                     = (collectionViewWidth - 20) / 3
            self.businessCollectionViewHeight.constant = (self.cellWidth * 2) + 30
            self.businessProfileCollectionView.delegate = self
            self.businessProfileCollectionView.dataSource = self
            self.businessProfileCollectionView.reloadData()
            return
        }
        layoutSubviewsCalled = true
    }
    
    
    
    //MARK:  Private Method
    
    
    func configureView(){
        self.imageHelper = ImageHelper(controller: self)
        self.scrollView.layer.cornerRadius = 10.0
        self.navigationItem.title = "Edit My Account"
        self.navigationController!.navigationBar.barTintColor = UIColor.white
        self.txtName.text = self.businessAccounts.businessName!.uppercaseFirst
        self.txtWebUrl.text = self.businessAccounts.webUrl
    }
    
    func loadData(){
        if let lat = self.businessAccounts.location?.latitude,let long = self.businessAccounts.location?.longitude {
            let coordinate = CLLocationCoordinate2DMake(Double(lat)!, Double(long)!)
            self.userLocation = coordinate
        }
        
        if let givenLocation = self.businessAccounts.location?.givenLocation{
            txtLocation.text = givenLocation
        }
        
        if let images =  self.businessAccounts.businessImages{
            self.businessImageArr = images
            var value = String()
            for data in self.businessImageArr{
                value = data.businessImageUrl!
                self.userImageList.append(value as AnyObject)
                self.createBusinessImgUrl(value,entityId: data.entityId!)
            }
        }
        
        for members in self.businessAccounts.businessMembers!{
            if let imgUrl = members.profileImageUrl{
                self.memberImageUrlList.append(imgUrl)
            }
            if let fbId = members.facebookId{
                self.memberList.append(String(describing: fbId))
            }
            if let isAdmin =  members.admin{
                self.memberAdminStatus.append(isAdmin)
            }
        }
        
        for entity in self.businessAccounts.businessImages!{
            if let ids = entity.entityId{
                self.imageEntityIds.append(ids)
            }
        }

        self.businessMemberCollectionView.reloadData()
    }
    
    func createBusinessImgUrl(_ image:String,entityId:String) {
        var userImage:[String:AnyObject] = [String:AnyObject]()
        userImage =
            [
                "businessImageUrl": image as AnyObject,
                "priority": self.selectedIndex as AnyObject,
                "entityId":entityId as AnyObject
        ]
        self.businessImageUrls.append(userImage)
    }
    
    
    func UpdateBusinessImagesUrl(_ image:String) {
        var userImage:[String:AnyObject] = [String:AnyObject]()
        if (self.imageEntityIds.count == self.selectedIndex) {
            userImage =
                [
                    "businessImageUrl": image as AnyObject,
                    "priority": self.selectedIndex as AnyObject,
                    "entityId":self.imageEntityIds[self.selectedIndex] as AnyObject
            ]
        } else {
            userImage =
                [
                    "businessImageUrl": image as AnyObject,
                    "priority": self.selectedIndex+1 as AnyObject,
            ]
            
        }
        
        self.businessImageUrls.append(userImage)
    }
    
    @objc func DeleteImageButton(_ sender: UIButton) {
        let i : Int = (sender.layer.value(forKey: "index")) as! Int
        self.memberImageUrlList.remove(at: i)
        self.memberList.remove(at: i)
        self.businessMemberCollectionView!.reloadData()
    }
    
    func deleteProfileImage(_ sender:UIButton) {
        let i : Int = (sender.layer.value(forKey: "index")) as! Int
        self.userImageList.remove(at: i)
        self.businessImageUrls.remove(at: i)
        self.businessProfileCollectionView!.reloadData()
    }
    
    
    
    func openImagePicker(_ sourceType: UIImagePickerControllerSourceType){
        
        self.imageHelper.determineStatus {
            (status) -> () in
            guard status else {
                print("Not authenticated to use photo")
                return
            }
            
            //If app is authorized to use the gallery only then load image controller
            self.imageHelper.getImageController(sourceType)
            self.imageHelper.imagePicked = {
                image in
                self.indicator.start()
                let ratio = Int(image.size.width/image.size.height);
                let maximumRatioForNonePanorama = 4/3
                if (ratio > maximumRatioForNonePanorama){
                    self.view.makeToast(message: localizedString(forKey: "IMG_NOT_APPROPRIATE_SIZE"))
                    self.indicator.stop()
                    //self.selectedImageView.image = nil
                } else {
                    
                    self.imgString = Base64.getCompressedBase64Image(image)
                    self.userImageList.append(image)
                    self.UpdateBusinessImagesUrl(self.imgString)
                    self.businessProfileCollectionView.reloadData()
                    self.indicator.stop()
                }
                //When the view is editable if the image view's image is changed do not send the image parameter.
                //For that this flag is set.
                // if self.isEditable {∫
                //   self.isImageChanged = true
                //}
            }
        }
    }
    
    
    func showImagePickerSheet() {
        
        let alert = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.purple
        alert.view.backgroundColor = UIColor.white
        let camera = UIAlertAction(title: "Camera",
                                   style: .default,
                                   handler: {
                                    (action:UIAlertAction) -> Void in
                                    self.openImagePicker(.camera)
                                    
        })
        
        let gallery = UIAlertAction(title: "Photos from gallery",
                                    style: .default) {
                                        (action: UIAlertAction) -> Void in
                                        self.imageHelper.getImageController(.photoLibrary)
                                        self.imageHelper.imagePicked =
                                            {
                                                image in
                                                self.indicator.start()
                                                self.userImageList.append(image)
                                                self.imgString = Base64.getCompressedBase64Image(image)
                                                self.UpdateBusinessImagesUrl(self.imgString)
                                                self.businessProfileCollectionView.reloadData()
                                                self.indicator.stop()
                                        }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel) {
                                            (action: UIAlertAction) -> Void in
        }
        
        alert.addAction(camera)
        alert.addAction(gallery)
        alert.addAction(cancelAction)
        
        present(alert,
                              animated: false,
                              completion: nil)
        
    }
    
    
    //MARK: BUTTON METHOD
    
    @IBAction func backButton(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addMemberButton(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showAddAdminFromEditSegue",sender: nil)
        
    }
    
    @IBAction func updateAction(_ sender: AnyObject) {
        if memberAdminStatus.contains(true){
            showAlert(controller: self, title: localizedString(forKey: "ALERT"), message:localizedString(forKey: "NO_PERMISSION_CHANGING_ACC") )
        }else{
            let response = self.validateFormValuesAndGetFormValueAsParameter()
            if response.status{
                self.indicator.start()
                self.updateBusinessAccount()
            }
            else {
                showAlert(controller: self, title: localizedString(forKey: "REQUIRED"), message: response.message)
            }
        }
        
    }
    
    //MARK: Collection Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == businessProfileCollectionView {
            return 6
        }else {
            
            return self.memberImageUrlList.count
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if collectionView == self.businessProfileCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "businessProfileCell", for: indexPath) as! EditBusinessAccountProfileCollectionViewCell
            if indexPath.row < self.userImageList.count {
                let imgData = userImageList[indexPath.row]
                if let data = imgData as? String {
                    cell.deletebtn?.layer.setValue(indexPath.row, forKey: "index")
                    cell.businessProfileImg.sd_setImage(with: URL(string: data.convertIntoViteURL()))
                }
                else {
                    cell.businessProfileImg.image = imgData as? UIImage
                    // obj is not a string array
                }
                //cell.businessProfileImg.sd_setImageWithURL(NSURL(string: imgData.businessImageUrl!.convertIntoViteURL()))
                cell.cameraLogo.isHidden = true
                cell.deletebtn.isHidden = false
                cell.row = indexPath.row
                cell.deleteAction = {
                    index in
                    self.userImageList.remove(at: index)
                    self.businessImageUrls.remove(at: index)
                    self.businessProfileCollectionView!.reloadData()
                }
                
            } else {
                
                cell.businessProfileImg.image = nil
                cell.cameraLogo.isHidden = false
                cell.deletebtn.isHidden = true
                
            }
            return cell
            
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "memberProfileImgCell", for: indexPath) as!EditBusinessAccountMemberCollectionViewCell
            cell.addMemberImg.sd_setImage(with: URL(string: self.memberImageUrlList[indexPath.row].convertIntoViteURL()))
            cell.deletebtn?.layer.setValue(indexPath.row, forKey: "index")
            cell.deletebtn?.addTarget(self, action: #selector(DeleteImageButton(_:)), for: UIControlEvents.touchUpInside)
            
            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == businessProfileCollectionView {
            let cell = collectionView.cellForItem(at: indexPath) as! EditBusinessAccountProfileCollectionViewCell
            self.selectedIndexPath = indexPath.row
            if cell.businessProfileImg.image == nil {
                self.showImagePickerSheet()
            }
            
        }
        
    }
    
    //Use for size
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.businessProfileCollectionView {
            return CGSize(width: self.cellWidth, height: self.cellWidth)
        } else {
            return CGSize(width: 52, height: 50)
        }
        
        
        
    }
    
    //MARK: PRIVATE DELEGATE METHOD
    
    func sendData(_ data:[String],users:[ViteUser]){
        for url in data {
            self.memberImageUrlList.append("http://graph.facebook.com/\(url)/picture?type=large")
        }
        
        for fbID in data {
            self.memberList.append(fbID)
        }
        let uniqueMemberArr = Array(Set(self.memberList))
        let uniqueMemberImgArr = Array(Set(self.memberImageUrlList))
        self.memberList = uniqueMemberArr
        self.memberImageUrlList = uniqueMemberImgArr
        self.businessMemberCollectionView.reloadData()
    }
    
    //MARK: UITextField Delegate Methods
    func textFieldDidBeginEditing(_ textField: UITextField){
        if (textField == self.txtLocation) {
            self.selectLocation()
            self.view.endEditing(true)
        }
    }
    
    
    
    
    //MARK: SEQUE Func
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAddAdminFromEditSegue"{
            let destinationController = (segue.destination as! AddAdminsVC)
            destinationController.delegate = self
            destinationController.memberList = self.memberList
        }
    }
    
}

extension EditBusinessAccountViewController {
    func selectLocation() {
//        locationManager?.getUserLocation()
//        locationManager?.fetchedUserLocation = {
//            location in ()
//            self.getEventLocation(location)
//        }
        if let location = self.userLocation {
             self.getEventLocation(location)
        }
       
    }
    //MARK : LOCATION DELEGATE METHODS
    
    func getEventLocation (_ businessAccLocation: CLLocationCoordinate2D){
        let controller = Route.mapController
        controller.currentCoordinateOfAnnotation = {
            coordinate in
            self.userLocation = coordinate
            self.indicator.parentView = self.txtLocation
            self.indicator.tintColor = UIColor.gray
            self.indicator.start()
            
            LocationManager.getLocationStringFromCoordinate(coordinate: coordinate,
                                                            completionBlock: {
                                                                (location) -> Void in
                                                                self.txtLocation.text = location ?? ""
                                                                self.indicator.stop()
            })
        }
        controller.currentLocation = businessAccLocation
        self.show(controller, sender: nil)
    }
    
}

extension EditBusinessAccountViewController{
    
    func validateFormValuesAndGetFormValueAsParameter() -> (status:Bool,message:String) {
        var errorMsg = ""
        if txtName.text!.isEmpty{
            errorMsg = localizedString(forKey: "NAME_BLANK")
            return (false, errorMsg)
        }
        
        if txtLocation.text!.isEmpty{
            errorMsg = localizedString(forKey: "LOCATION_BLANK")
            return (false, errorMsg)
        }
        
        if (self.txtWebUrl.text!.isEmpty == true) {
            return (true, "")
            
        } else {
            if (isValidURL(self.txtWebUrl.text! as NSString) == false) {
                errorMsg = localizedString(forKey: "ENTER_VALID_WEB_URL")
                return (false, errorMsg)
            }
        }
        if businessImageUrls.count == 0{
            errorMsg = localizedString(forKey: "SELECT_IMG")
            return (false, errorMsg)
        }
        return (true, "")
    }
    
    func updateBusinessAccount(){
        self.indicator.start()
        self.view.isUserInteractionEnabled = false
        let coord =
            [
                "givenLocation":self.txtLocation.text as AnyObject,
                "longitude":userLocation?.longitude as AnyObject,
                "latitude" : userLocation?.latitude as AnyObject
                ] as [String : Any]
        
        let business:[String:AnyObject] = [
            "businessName":self.txtName.text! as AnyObject,
            "description":"" as AnyObject,
            "webUrl":self.txtWebUrl.text! as AnyObject,
            "maxMember":8 as AnyObject,
            "location":coord as AnyObject,
            "businessImages":self.businessImageUrls as AnyObject,
            "entityId":self.businessAccounts.entityId! as AnyObject]
        
        let parameter:[String:AnyObject]!
        parameter =
            ["fbIdList":self.memberList as AnyObject,
             "business":business as AnyObject,
             "chargeAmount":10 as AnyObject]
        
        BusinessAccountWebService().updateBusinessAccount(parameter, successBlock: { (msg) in
            self.view.isUserInteractionEnabled = true
             self.delegate?.getBusinessUserInformation()
            self.navigationController?.popViewController(animated: true)
            showAlert(controller: self, title: localizedString(forKey: "SUCCESS"), message: msg)
             self.indicator.stop()
        }) { (msg) in
            self.indicator.stop()
            showAlert(controller: self, title: "", message: msg)
        }
    }
    
}

