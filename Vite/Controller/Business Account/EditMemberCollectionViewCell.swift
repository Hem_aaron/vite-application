//
//  EditMemberCollectionViewCell.swift
//  Vite
//
//  Created by sajjan giri on 11/14/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class EditMemberCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var memberdelButton: UIButton!
    @IBOutlet weak var memberImg: UIImageView!
}
