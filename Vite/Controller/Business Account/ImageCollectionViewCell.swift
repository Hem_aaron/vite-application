//
//  ImageCollectionViewCell.swift
//  Vite
//
//  Created by sajjan giri on 10/2/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
      //6 Images camera images
    @IBOutlet weak var deleteImageButton: UIButton!
    @IBOutlet weak var cameraLogoImageView: UIImageView!
    @IBOutlet weak var profileImage: ImageViewWithRoundedBorder!
   
}
