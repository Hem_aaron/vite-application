//
//  ManageAccountCell.swift
//  Vite
//
//  Created by Ujjwal Shrestha on 11/6/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class ManageAccountCell: UITableViewCell {
    
    @IBOutlet weak var imgProfile: ImageViewWithRoundedBorder!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var toggle: UISwitch!
    var pushBlock:(()->())?
    
    
    
    @IBAction func makeBusinessAccountPrimary(_ sender: AnyObject) {
        if let block = pushBlock {
            block()
        }
        
    }
    
    
}
