//
//  ManageAccountVC.swift
//  Vite
//
//  Created by Ujjwal Shrestha on 11/6/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import Branch

class ManageAccountVC: WrapperBlueNavControllerViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var topLbl: UILabel!
    @IBOutlet weak var tblManageAccount: UITableView!
    
    var businessAccounts:[BusinessAccountUserModel] = [BusinessAccountUserModel]()
    var selectedBusinessAccount:BusinessAccountUserModel!
    var imgUrls:[String] = [String]()
    var isEdit:Bool = false
    let indicator = ActivityIndicator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Manage Accounts"
        self.topLbl.textColor = UIColor.brown
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getAllBusinesssAccounts()
        self.configureNav()
    }
    
    func configureNav(){
        if let nav = self.navigationController {
            nav.navigationBar.backgroundColor = UIColor.blue
            nav.navigationBar.barTintColor = UIColor(red: 6/255, green: 164/255, blue: 233/255, alpha: 1.0)
            nav.navigationBar.isTranslucent                  = true
            nav.view.backgroundColor                       = UIColor.blue
            self.navigationItem.hidesBackButton            = true
            self.navigationController?.isNavigationBarHidden = false
            let navBar                                     = self.navigationController!.navigationBar
            navBar.titleTextAttributes                     = [NSAttributedStringKey.foregroundColor: UIColor.white]
            nav.navigationBar.isTranslucent = false
        }
    }

    //MARK: IBACTION METHODS
    
    @IBAction func backBtn(_ sender: Any) {
        Route.goToHomeController()
    }
    
    @IBAction func CreateBusinessAccount(_ sender: Any) {
         self.navigationController?.pushViewController(Route.createBusinessAccount, animated: true)
    }
    
    
    
    //MARK: GET ALL BUSINESS ACCOUNT
    func getAllBusinesssAccounts(){
        self.indicator.start()
        BusinessAccountWebService().getBusinessAccountList({ (businessAccountList) in
            self.indicator.stop()
            self.businessAccounts = businessAccountList
            self.tblManageAccount.reloadData()
        }) { (message) in
            self.indicator.stop()
            showAlert(controller: self, title: localizedString(forKey: "FAILED_GETTING_BUSINESS_ACCOUNT"), message: message)
        }
    }
    
    //MARK: DELETE BUSINESS ACCOUNT
    func deleteBusinessAccount(){
        
        BusinessAccountWebService().deleteBusinessAccount(self.selectedBusinessAccount.entityId!, successBlock: { (msg) in
            showAlert(controller: self, title: localizedString(forKey: "SUCCESS"), message: msg)
        }) { (msg) in
            showAlert(controller: self, title: localizedString(forKey: "FAILURE"), message: msg)
        }
    }
    
    //Mark: Delete Member from Business Account
    func deleteMemberFromBusinessAccount(){
        
        let memberId = [UserDefaultsManager().userFBID!]
        
        let parameters:[String:AnyObject] = [
            "fbIdList" :memberId as AnyObject
        ]
        
        BusinessAccountWebService().deleteMemberFromBusinessAccount(self.selectedBusinessAccount.entityId!, parameters, successBlock: { (msg) in
            showAlert(controller: self, title: localizedString(forKey: "SUCCESS"), message: "You have been removed successfully from \(self.selectedBusinessAccount.businessName!)")
              self.getAllBusinesssAccounts()
        }) { (msg) in
            showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: msg)
        }
    }
    
    func makeBusinessAccountPrimary(businessId: String, status: Bool){
        
        let titleMessage  = status ? localizedString(forKey: "MAKE_THIS_BUSINESS_ACC_PRIM") : localizedString(forKey: "REMOVE_BUSINESS_ACC_FROM_PRIM")
        let alert = UIAlertController(title: "",
                                      message: titleMessage,
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Yes",
                                     style: .default,
                                     handler: { (action:UIAlertAction) -> Void in
                                        self.indicator.start()
                                        BusinessAccountWebService().makeBusinessAccountPrimary(businessId, status, successBlock: { (msg) in
                                            self.indicator.stop()
                                            self.getAllBusinesssAccounts()
                                        }, failureBlock: { (msg) in
                                            self.indicator.stop()
                                            showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: msg)
                                        })
                                        
        })
        let cancelAction = UIAlertAction(title: "No",
                                         style: .default) {
                                            (action: UIAlertAction) -> Void in
                                            self.getAllBusinesssAccounts()
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert,
                              animated: false,
                              completion: nil)
        
    }
    
    func deleteBusinessAccountOrMember(title: String, businessName: String){
        
        let titleMessage  = title == "BusinessAccount" ? "Are you sure you want to delete \(businessName)?" : "Are you sure you want to remove yourself from \(businessName)"
        let alert = UIAlertController(title: "",
                                      message: titleMessage,
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Yes",
                                     style: .default,
                                     handler: { (action:UIAlertAction) -> Void in
                                        title == "BusinessAccount" ? self.deleteBusinessAccount() : self.deleteMemberFromBusinessAccount()
                                        
        })
        let cancelAction = UIAlertAction(title: "No",
                                         style: .default) {
                                            (action: UIAlertAction) -> Void in
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert,
                              animated: false,
                              completion: nil)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "updateBusinessSegue"{
            let destinationController = (segue.destination as! UpdateBusinessAccount)
            destinationController.businessAccounts = self.selectedBusinessAccount
        }
    }
}

extension ManageAccountVC {
    //MARK :- TABLEVIEW DELEGATE METHODS
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 0
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return self.businessAccounts.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ManageAcFooterCell") as! ManageAcFooterCell
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ManageAccountCell") as! ManageAccountCell
        let businessAc = self.businessAccounts[indexPath.row]
        cell.lblName.text = businessAc.businessName
        cell.imgProfile.layer.cornerRadius = 20
        cell.selectionStyle = .none
        if let imageUrl = businessAc.profileImage {
            cell.imgProfile.sd_setImage(with: NSURL(string: imageUrl.convertIntoViteURL())! as URL)
        }
        
        if let toggleStatus = businessAc.isPrimaryBusinessAccount {
            cell.toggle.setOn(toggleStatus, animated: true)
        } else {
            cell.toggle.setOn(false, animated: true)
        }
        
        cell.pushBlock = {
            let status = cell.toggle.isOn ? true : false
            self.makeBusinessAccountPrimary(businessId: businessAc.entityId!, status: status)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedBusinessAccount = self.businessAccounts[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
         return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    func tableView(tableView: UITableView, editingStyleForRowAt indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive , title: "Delete"){ action, index in
            self.selectedBusinessAccount = self.businessAccounts[indexPath.row]
            var memberAdminStatus = [Bool]()
            for adminStatus in self.selectedBusinessAccount.businessMembers!{
                if let admin = adminStatus.admin{
                    memberAdminStatus.append(admin)
                }
            }
            if memberAdminStatus.contains(true){
                self.deleteBusinessAccountOrMember(title: "Member", businessName: self.selectedBusinessAccount.businessName!)
            }else{
                self.deleteBusinessAccountOrMember(title: "BusinessAccount", businessName: self.selectedBusinessAccount.businessName!)
            }
            
            
        }
        delete.backgroundColor = UIColor.red
        return [delete]
    }
}

