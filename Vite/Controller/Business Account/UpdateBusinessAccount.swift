
//


//  UpdateBusinessAccount.swift
//  Vite
//
//  Created by Ujjwal Shrestha on 11/10/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import MapKit

protocol UpdateBusinessAccountDelegate {
    func showDescriptionFor(_ imageId: Int)
}

class UpdateBusinessAccount: WrapperController, UITextFieldDelegate, GetBusinessUserInfoDelegate , UICollectionViewDelegate , UICollectionViewDataSource{
 
    
    
    @IBOutlet weak var lblAdminOrMembers: UILabel!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtLocation: UILabel!
    @IBOutlet weak var txtWebUrl: UILabel!
    @IBOutlet weak var lblNoMembers: UILabel!
    @IBOutlet weak var btnEditBusinessAccount: UIBarButtonItem!
    @IBOutlet weak var webUrlView: UIView!
    @IBOutlet weak var kiPager: KIImagePager!
    @IBOutlet weak var scrollView: UIScrollView!
    var isFromSideMenu = Bool()
    
    @IBOutlet weak var lblNoWebUrl: UILabel!
    @IBOutlet weak var editMemberCollectionView: UICollectionView!
    
    //MARK: RATE AND COMMENT ORGANIZER
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var tblRateAndComment: UITableView!
    @IBOutlet weak var lblCommentCount: UILabel!
    @IBOutlet weak var commentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblCommentHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnViewAllComment: UIButton!
    @IBOutlet weak var commentViewTopSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var commentViewButtomSpaceConstraint: NSLayoutConstraint!
    
    let cursor = VCursor()
    var rateAndCommentArr = [VRateComment]()
    
    var delegate: UpdateBusinessAccountDelegate?
    var businessAccounts:BusinessAccountUserModel!
    var isForViewingOthersBusinessProfile = Bool()
    var businessImageArr = [BusinessImageModel]()
    var memberAdminStatus = [Bool]()
    var memberList = [String]()
    var memberImageUrlList = [String]()
    var imageEntityIds = [String]()
    var selectedIndexPath: Int!
    let indicator = ActivityIndicator()
    var userInfo = UserDefaultsManager()
    var isFromViteFeed: Bool = false
    var businessID = String()
    
    //MARK:- Object Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.layer.cornerRadius = 10.0
        self.navigationItem.title = "My Account"
        self.navigationController!.navigationBar.barTintColor = UIColor.white
        if isForViewingOthersBusinessProfile{
            self.btnEditBusinessAccount.isEnabled = false
            self.navigationItem.title = "Business Profile"
            
        }else{
            self.btnEditBusinessAccount.isEnabled = true
        }
       
        if isFromViteFeed{
         self.getBusinessProfile()
        }else{
            self.kiPager.dataSource = self
            self.kiPager.delegate = self
            self.loadData()
        }
         self.fetchComments(shouldReset: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if !isFromViteFeed{
            self.configureView()
        }
    }
    
    func maintainTblViewHeight() {
        self.tblCommentHeightConstraint.constant = self.tblRateAndComment.contentSize.height
        self.commentViewHeightConstraint.constant = self.tblRateAndComment.contentSize.height + 85 // 38 is the height of comment(200) lbl and the top space of the label plus the top space of the table view  and the viewallcomment view height
    }
    
    //populate user business info when the information is updated
    func getBusinessUserInformation(){
        getBusinessProfile()
    }
    
    func getBusinessProfile(){

        self.indicator.start()
        self.memberImageUrlList.removeAll()
        self.memberList.removeAll()
        self.businessImageArr.removeAll()
        self.imageEntityIds.removeAll()
        self.memberAdminStatus.removeAll()
        BusinessAccountWebService().getBusinessProfile(self.businessAccounts.entityId!, successBlock: { (businessAccount) in
            self.businessAccounts = businessAccount
            self.configureView()
            self.loadData()
            self.kiPager.dataSource = self
            self.kiPager.delegate = self
            self.indicator.stop()
            self.kiPager.reloadData()
        }) { (message) in
            showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: message)
            self.indicator.stop()
        }
    }
    
    
    
    
    //MARK: Private Utility Methods
    func configureView(){
        self.txtName.text = self.businessAccounts.businessName
        self.txtWebUrl.text = self.businessAccounts.webUrl
        self.txtLocation.text = self.businessAccounts.location?.givenLocation
        let maskLayer = CAShapeLayer()
        let path = UIBezierPath(roundedRect:self.kiPager.bounds, byRoundingCorners:[.topRight, .topLeft], cornerRadii: CGSize(width: 10, height: 10))
        maskLayer.path = path.cgPath
        self.kiPager.layer.mask = maskLayer
        
    }
    
    
    func loadData(){
        if let images =  self.businessAccounts.businessImages{
            self.businessImageArr = images
        }
        self.lblAdminOrMembers.isHidden =  self.businessAccounts.businessMembers?.count == 0 ? true: false
        self.lblNoMembers.isHidden =  self.businessAccounts.businessMembers?.count == 0 ? false: true
        self.lblNoWebUrl.isHidden = self.businessAccounts.webUrl?.count == 0 ? false : true
        if let businessMembers =  self.businessAccounts.businessMembers{
            for members in businessMembers{
                if let imgUrl = members.profileImageUrl{
                    self.memberImageUrlList.append(imgUrl)
                }
                if let fbId = members.facebookId{
                    self.memberList.append(String(describing: fbId))
                }
                if let isAdmin =  members.admin{
                    self.memberAdminStatus.append(isAdmin)
                }
            }
        }
        
        if  self.memberAdminStatus.contains(true){
            btnEditBusinessAccount.tintColor = UIColor.clear
            self.btnEditBusinessAccount.isEnabled = false
        }else{
             self.btnEditBusinessAccount.isEnabled = true
            // btnEditBusinessAccount.tintColor = nil
        }
        
        for entity in self.businessAccounts.businessImages!{
            if let ids = entity.entityId{
                self.imageEntityIds.append(ids)
            }
        }
        self.editMemberCollectionView.reloadData()
        self.kiPager.reloadData()
    }
    
    
    //MARK: IBAction Methods
    @IBAction func btnBackAction(_ sender: Any) {
        if isFromSideMenu{
            Route.goToHomeController()
        }else{
           self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    // MARK : SEQUE FUNC
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showEditViewSeque"{
            let destinationController = (segue.destination as! EditBusinessAccountViewController)
            destinationController.businessAccounts = self.businessAccounts
            destinationController.delegate = self
            //  destinationController. = self.memberList
        }
    }
    
    //MARK: SendDataDelegate methods
    
    
    //MARK: COLLECTION DELEGATE
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.memberImageUrlList.count
    }
   
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let fbId  = memberList[indexPath.row]
        let controller = Route.profileViewController
        controller.fbId = fbId
        controller.isForViewingOthersProfile = true
        let nav = UINavigationController.init(rootViewController: controller)
        self.present(nav, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "editMemberColl", for: indexPath) as! EditMemberCollectionViewCell
                cell.memberImg.sd_setImage(with: URL(string: self.memberImageUrlList[indexPath.row].convertIntoViteURL()))
                return cell
    }
}



//MARK: IMAGE PAGER DELEGATE AND DATA SOURCE
extension UpdateBusinessAccount: KIImagePagerDelegate, KIImagePagerDataSource{

    
    //MARK: Image pager Data source
    
    func array(withImages pager: KIImagePager!) -> [Any]! {
        return self.businessAccounts.businessImages!.map {value in value.businessImageUrl!.convertIntoViteURL()}
    }
    func contentMode(forImage image: UInt, in pager: KIImagePager!) -> UIViewContentMode {
         return .scaleAspectFill
    }
    
    
    //MARK: KIImagePager Delegate
    func imagePager(_ imagePager: KIImagePager!, didScrollTo index: UInt) {
        
    }
    
    
    func imagePager(_ imagePager: KIImagePager!, didSelectImageAt index: UInt) {
        self.selectedIndexPath = Int(index)
        // self.showImagePickerSheet()
    }
    
}

extension UpdateBusinessAccount: UITableViewDataSource, UITableViewDelegate {
    
    @IBAction func viewAllComments(_ sender: Any) {
        let controller = Route.rateCommentList
        controller.businessId = self.businessAccounts.entityId
        self.present(controller, animated: true, completion: nil)
    }
    
    
    func fetchComments(shouldReset reset:Bool) {
        indicator.tintColor = UIColor.gray
        indicator.start()
        if reset {
            self.cursor.reset()
        }
        //isFromViteFeed is checked as vite feed is the only place which doesnot pass business account model to this page but only business ID
        VRateCommentWebService.getRateAndCommentListForBusinessAccount(businessAccountId: isFromViteFeed ? self.businessID : self.businessAccounts.entityId!,  cursor.nextPage(), pageSize: cursor.pageSize, successBlock: { (commentList, totalCount) in

            if reset {
                self.rateAndCommentArr.removeAll()
            }
            for list in commentList! {
                // to display only 2 comments  in the design
                if self.rateAndCommentArr.count < 2 {
                    self.rateAndCommentArr.append(list)
                }
            }
            self.cursor.totalCount = totalCount.intValue
            self.lblCommentCount.text = "Comments (\(totalCount.intValue))"
            self.cursor.totalLoadedDataCount = self.rateAndCommentArr.count
            if self.rateAndCommentArr.count == 0 {
                self.commentViewHeightConstraint.constant = 0
                self.commentViewTopSpaceConstraint.constant = 0
                self.btnViewAllComment.isHidden = true
            } else {
                self.tblRateAndComment.reloadData()
                self.tblRateAndComment.layoutIfNeeded()
                self.maintainTblViewHeight()
            }
            self.indicator.stop()
        }) { (msg) in
            print(msg)
            self.indicator.stop()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rateAndCommentArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rateComment = rateAndCommentArr[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "rateAndCommentCell", for: indexPath) as! RateAndCommentTableViewCell
        cell.lblOrganizerName.text = rateComment.fullName
        cell.lblComment.text = rateComment.comment
        cell.rating.rating = Double( rateComment.rating!)
        return cell
    }
}
