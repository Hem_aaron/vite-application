  //
//  CelebrityLoginViewController.swift
//  Vite
//
//  Created by eeposit2 on 1/23/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit
import TwitterKit

class CelebrityLoginViewController: WrapperController {
    
    let indicator = ActivityIndicator()
    var user = UserDefaultsManager()
    var isFromElite = false
    var networkTypeArr = [String]()
    @IBOutlet weak var btnConnectInsta: UIButton!
    @IBOutlet weak var btnConnectTwitter: UIButton!
    @IBOutlet weak var instaTickImgView: UIImageView!
    @IBOutlet weak var twitterTickImgView: UIImageView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var celebEliteLoginImgView: UIImageView!
    @IBOutlet weak var validateLbl: UILabel!
    @IBOutlet weak var welcomeLbl: UILabel!
    @IBOutlet weak var celebEliteBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var celebEliteImgTopSpace: NSLayoutConstraint!
    
    @IBOutlet weak var btnInstaLeading: NSLayoutConstraint!
    
    @IBOutlet weak var btnInstaTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var validateLblTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var validateLblLeading: NSLayoutConstraint!
    
    @IBOutlet weak var btnContinueTop: NSLayoutConstraint!
    @IBOutlet weak var btnInstaTop: NSLayoutConstraint!
    @IBOutlet weak var lblViteCelebElite: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if DeviceType.IS_IPHONE_5{
            changeFontAndSpace()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(CelebrityLoginViewController.instaLogin(_:)), name:NSNotification.Name(rawValue: "InstaTokenNotification"), object: nil)
        self.getUserNetworkStatus()
        if isFromElite {
            celebEliteBtnWidth.constant = DeviceType.IS_IPHONE_5 ? 110 : 160
            eliteSetup()
        }
    }
    
    func changeFontAndSpace(){
        celebEliteImgTopSpace.constant = -30
         validateLbl.font = validateLbl.font.withSize(15)
        btnInstaLeading.constant = 20
        btnInstaTrailing.constant = 20
        validateLblTrailing.constant = 10
        validateLblLeading.constant = 10
        btnContinueTop.constant = 30
        btnInstaTop.constant = 35
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getUserNetworkStatus()
    }
    
    
    @IBAction func back(_ sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController")
        AppDel.delegate?.window??.rootViewController = controller
    }
    
    
    @IBAction func connectTwitter(_ sender: AnyObject) {
        loginWithTwitter()
    }
    
    
    //MARK: TWITTER LOGIN
    func loginWithTwitter(){
        self.indicator.start()
        Twitter.sharedInstance().logIn { session, error in
            if (session != nil) {
                self.getTwitterProfileData()
              //  print("signed in as \(session!.userName)");
            } else {
                self.indicator.stop()
              //  print("error: \(error!.localizedDescription)");
            }
        }
    }
    
    func getTwitterProfileData(){
        TwitterApiService().getTwitterprofile({ (userResponse) in
            self.user.twitterUserName = userResponse["name"] as? String
            self.user.twitterID = userResponse["id_str"] as? String
            self.user.twitterScreenName = userResponse["screen_name"] as? String
            if let twitterId =   self.user.twitterID, let twitterScreenName = self.user.twitterScreenName{
                let userName = twitterScreenName.replacingOccurrences(of: " ", with: "")
                self.connectWithSocial(twitterId,networkUserName: userName,networkName: "Twitter" ,networkProfileUrl: "https://twitter.com/\(userName)" )
            }
            
        }) { (error) in
            self.indicator.stop()
            showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: error as! String)
        }
    }
    
    //MARK: INSTAGRAM LOGIN
    
    @objc func instaLogin(_ notification: Foundation.Notification){
        let defaults = UserDefaults.standard
        user.instaUserName = defaults.string(forKey: "kInstaUserName")!
        user.instaId = defaults.string(forKey: "InstaId")!
        if let userName =   user.instaUserName,let id =   user.instaId {
            self.connectWithSocial(id,networkUserName: userName,networkName: "Instagram",networkProfileUrl:  "https://www.instagram.com/\(userName)")
        }
        
    }
    
    
    //MARK: CONNECT WITH SOCIAL - SEND FRIENDS TO SERVER
    func connectWithSocial(_ networkID: String,networkUserName:String, networkName: String, networkProfileUrl: String){
        self.indicator.start()
        let parameter :[String: AnyObject] = [
            "networkId":networkID as AnyObject,
            "networkType":networkName as AnyObject,
            "networkUserName":networkUserName as AnyObject,
            "networkProfileUrl": networkProfileUrl as AnyObject
        ]
        VSocialNetworkWebService.connectWithSocial(parameter, successBlock: { (message) in
            self.getUserNetworkStatus()
            self.indicator.stop()
        }) { (message) in
            if let msg = message{
                self.view.isUserInteractionEnabled = true
                self.indicator.stop()
                showAlert(controller: self, title: "Error", message: msg)
            }
        }
    }
    
    func getUserNetworkStatus() {
        guard let fbId = UserDefaultsManager().userFBID else{
            self.view.isUserInteractionEnabled = true
            return
        }
        VSocialNetworkWebService.getUserNetworkStatus(fbId, successBlock: { (networkStatusList) in
            if let list = networkStatusList{
                self.getStatus(list)
            }
            self.view.isUserInteractionEnabled = true
        }) { (message) in
            showAlert(controller: self, title: "", message: message)
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func getStatus( _ networkArr : [NetworkStatus]) -> [String] {
        networkTypeArr.removeAll()
        for network in networkArr{
            if let networkType =  network.networkType, network.networkConnectStatus == true {
                networkTypeArr.append(networkType)
            }
        }
        btnConnectInsta.isEnabled = networkTypeArr.contains("Instagram") ? false : true
        btnConnectTwitter.isEnabled = networkTypeArr.contains("Twitter") ? false : true
        instaTickImgView.isHidden = networkTypeArr.contains("Instagram") ? false : true
        twitterTickImgView.isHidden = networkTypeArr.contains("Twitter") ? false : true
        btnContinue.isEnabled = networkTypeArr.contains("Instagram") && networkTypeArr.contains("Twitter") ? true : false
        print(networkTypeArr)
        return networkTypeArr
    }
    
    
    @IBAction func SendEmailForVerification(_ sender: AnyObject) {
        self.indicator.start()
        self.view.isUserInteractionEnabled = false
        let verificationURL = isFromElite ? KAPISendEmailForEliteVerification : KAPISendEmailForCelebVerification
        
        
        WebService.request(method: .post, url: verificationURL, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            print(response)
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                self.user.loginSuccessStatus = true
                let controller = Route.celebSuccess
                self.present(controller, animated: true, completion: nil)
            }
            self.indicator.stop()
            self.view.isUserInteractionEnabled = true
        }) { (msg, err) in
            self.indicator.stop()
            showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: msg)
            self.view.isUserInteractionEnabled = true
        }
    }
    
    
    
}
  
  //MARK: ELITE SETUP
  extension CelebrityLoginViewController {
    
    func eliteSetup(){
        btnConnectInsta.setBackgroundImage(UIImage(named: "EliteConnectInsta"), for: UIControlState())
        btnConnectTwitter.setBackgroundImage(UIImage(named: "EliteConnectTwit"), for: UIControlState())
        instaTickImgView.image = UIImage(named: "EliteTick")
        twitterTickImgView.image = UIImage(named: "EliteTick")
        celebEliteLoginImgView.image = UIImage(named: "EliteCrown")
        validateLbl.text = localizedString(forKey: "VALIDATE_ELITE_STATUS")
        lblViteCelebElite.text = "ViteElite"
    }
  }
