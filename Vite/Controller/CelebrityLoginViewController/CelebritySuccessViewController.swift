//
//  CelebritySuccessViewController.swift
//  Vite
//
//  Created by eeposit2 on 1/23/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class CelebritySuccessViewController: WrapperController {
    override func viewDidLoad(){
        super.viewDidLoad()
    }

    @IBAction func goHome(_ sender: Any) {
        Route.goToHomeController()
    }
    
    @IBAction func back(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
  }
