//
//   AsyncPhotoMediaItem.swift
//  Vite
//
//  Created by Eeposit 01 on 7/31/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import SDWebImage

class AsyncPhotoMediaItem: JSQPhotoMediaItem {
    var asyncImageView: UIImageView!
    var syncImageView: UIImageView!
    
    override init!(maskAsOutgoing: Bool) {
        super.init(maskAsOutgoing: maskAsOutgoing)
    }
     
    init(withURL url: URL, isOperator: Bool) {
        super.init()
        
        let size                        = super.mediaViewDisplaySize()
        appliesMediaViewMaskAsOutgoing = (isOperator == false)
        asyncImageView = UIImageView()
        asyncImageView.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        asyncImageView.contentMode = .scaleAspectFill
        asyncImageView.clipsToBounds = true
        asyncImageView.layer.cornerRadius = 20
        asyncImageView.backgroundColor = UIColor.jsq_messageBubbleLightGray()
        
        
        let activityIndicator = JSQMessagesMediaPlaceholderView.withActivityIndicator()
        activityIndicator?.frame = asyncImageView.frame
        asyncImageView.addSubview(activityIndicator!)
        asyncImageView.sd_setImage(with: url) { (image, error, type, Url) in
            self.asyncImageView.image = image
            activityIndicator?.removeFromSuperview()
        }
    }
    
    override func mediaView() -> UIView! {
        return asyncImageView
    }
    
    override func mediaViewDisplaySize() -> CGSize {
        return asyncImageView.frame.size
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

