
//  AsyncVideoMediaItem.swift
//  Vite
//
//  Created by Eeposit 01 on 8/4/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.


import UIKit
import JSQMessagesViewController


class AsyncVideoMediaItem: JSQVideoMediaItem {
    var videoImageView:  UIImageView!
    var playImageView: UIImageView!
    
    override init!(maskAsOutgoing: Bool) {
        super.init(maskAsOutgoing: maskAsOutgoing)
    }
    init(fileURL: URL,imageUrl : URL) {
        super.init()
        
        let size                        = super.mediaViewDisplaySize()
        videoImageView = UIImageView()
        videoImageView.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        videoImageView.contentMode = .scaleAspectFill
        videoImageView.clipsToBounds = true
        videoImageView.layer.cornerRadius = 20
        videoImageView.backgroundColor = UIColor.jsq_messageBubbleLightGray()
        
        playImageView = UIImageView()
        playImageView.frame = CGRect(x: 80, y: 50, width: 50, height: 50)
        playImageView.contentMode = .scaleAspectFill
        playImageView.image = UIImage(named: "Play")
        videoImageView.addSubview(playImageView)
        
        let activityIndicator = JSQMessagesMediaPlaceholderView.withActivityIndicator()
        activityIndicator?.frame = videoImageView.frame
        videoImageView.addSubview(activityIndicator!)
        videoImageView.sd_setImage(with: imageUrl) { (image, error, tye, url) in
            self.videoImageView.image = image
            activityIndicator?.removeFromSuperview()
        }
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func mediaView() -> UIView! {
        return videoImageView
    }
    
    override func mediaViewDisplaySize() -> CGSize {
        return videoImageView.frame.size
    }
    
    
    
    
}
