//
//  CustomInputAccesoryView.swift
//  Vite
//
//  Created by Eeposit1 on 2/14/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class MessageInput: UIView {
    
    var doneHandler:(()->())?
    @IBOutlet var view: UIView!

    @IBAction func doneAction(_ sender: AnyObject) {
        if let handler = self.doneHandler {
            handler()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        Bundle.main.loadNibNamed("MessageInput", owner: self, options: nil)
        self.view.frame = self.bounds
        self.addSubview(view)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        Bundle.main.loadNibNamed("MessageInput", owner: self, options: nil)
        self.addSubview(view)
    }
    
}
