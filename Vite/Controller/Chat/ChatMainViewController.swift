//
//  ChatMainViewController.swift
//  FirebaseTest
//
//  Created by Eeposit1 on 3/6/16.
//  Copyright © 2016 Prajeet Shrestha. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import JSQMessagesViewController
import MobileCoreServices
import FirebaseStorage
import SDWebImage
import AVFoundation
import AVKit

class ChatMainViewController: JSQMessagesViewController, UITextFieldDelegate, ChatServiceDelegate, UIGestureRecognizerDelegate   {
    var chatservice:ChatControllerServiceProtocol!
    var sender:ChatUser!
    var receiver:ChatUser!
    var message:String!
    var asyncronousPhotoMedia:AsyncPhotoMediaItem!
    var videoMediaItem: AsyncVideoMediaItem!
    var jsMessages = [JSQMessage]()
    var messageModel = [MessageModel]()
    var receiverImage = UIImageView()
    var senderImage = UIImageView()
    var inputToolBar : JSQMessagesInputToolbarDelegate!
    var outgoingBubbleImageView: JSQMessagesBubbleImage!
    var incomingBubbleImageView: JSQMessagesBubbleImage!
    let imagePicker = UIImagePickerController()
    var selectedImg = UIImage()
    
    var timeStamp: String {
        return "\(Date().timeIntervalSince1970 * 1000)"
    }
    
    fileprivate func setupBubbles() {
        let factory = JSQMessagesBubbleImageFactory()
        outgoingBubbleImageView = factory?.outgoingMessagesBubbleImage(
            with: UIColor.jsq_messageBubbleBlue())
        incomingBubbleImageView = factory?.incomingMessagesBubbleImage(
            with: UIColor.jsq_messageBubbleGreen())
    }
    
    
    var newMessages = [MessageModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.chatservice       = ChatService(sender:self.sender, receiver: self.receiver, delegate: self)
        self.senderId          = self.sender.fbID
        self.senderDisplayName = self.sender.name
        self.setupNavigationbar()
        self.setupNotifications()
        self.initiateFirebaseObserver()
        self.title = receiver.name
        self.setupBubbles()
        self.setupProfileImages()
      
        
       UIApplication.shared.statusBarStyle = .lightContent
        //Swipe back 
       self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
    }
    
    
    func setupProfileImages() {
      
        let senderURL = self.sender.profileImageUrl.convertIntoViteURL()
        self.senderImage.sd_setImage(with: URL(string: senderURL), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
        let receiverURL = self.receiver.profileImageUrl.convertIntoViteURL()
        self.receiverImage.sd_setImage(with: URL(string: receiverURL), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setPresenceOn()
        self.chatservice.updateLastMessageCount()
        NotificationService().resetUnseenMessageCount(self.receiver.fbID)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.setPresenceOff()
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func openProfile(_ sender: MyTapGesture) {
        let ctrl = Route.profileViewController
        ctrl.fbId = sender.facebookId
        ctrl.isForViewingOthersProfile = sender.isViewingOthersProfile
        ctrl.isFromChatVc = true
        let nav = UINavigationController.init(rootViewController: ctrl)
        self.present(nav, animated: true, completion: nil)
    }
    
    
    func setupNavigationbar() {
        if let nav = self.navigationController {
        nav.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        nav.navigationBar.shadowImage                  = UIImage()
        nav.view.backgroundColor                       = UIColor.white
        self.navigationItem.hidesBackButton            = true
        nav.isNavigationBarHidden = false
        let navBar                                     = self.navigationController!.navigationBar
        
        navBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.purple , NSAttributedStringKey.font: UIFont(name: AppProperties.viteFontName , size: AppProperties.viteNavigationTitleFontSize)!]
        }
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(appActive), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appDeactive), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
    func sendMessage(_ text:String) {
        var msgString = text
        msgString     = msgString.trimmingCharacters(in: CharacterSet.whitespaces)
        if msgString.isEmpty {
            return
        }
        
        let m = MessageModel(
            sender : self.sender,
            receiver : self.receiver,
            message : msgString,
            timestamp : "" ,
            uniqueId : "",
            mediaImageUrl: "",
            mediaVideoUrl: "",
            isMedia: false,
            isVideo: false)
        
        self.chatservice.sendMessage(m)
    }
    // upload video to firebase storage
    func uploadVideoToFireBase(_ url: URL) {
        let videoFileName = self.senderId + timeStamp
        let storageRef = FIRStorage.storage().reference(withPath: "myVideo/\(videoFileName).mov")
        let uploadMetadata = FIRStorageMetadata()
        uploadMetadata.contentType = "video/quicktime"
        storageRef.putFile(url, metadata: uploadMetadata) { (metadata, error) in
            if let videoUrl = metadata?.downloadURL()?.absoluteString {
                let videoThumbnail = thumbnailForVideoAtURL((metadata?.downloadURL())!)
                let imgFileName = self.senderId + self.timeStamp
                let storageRef = FIRStorage.storage().reference(withPath: "myImage/\(imgFileName).jpeg")
                let uploadMetadata = FIRStorageMetadata()
                uploadMetadata.contentType = "myImage/jpeg"
                // to upload video thumbnail
                storageRef.put((videoThumbnail?.lowestQualityJPEGNSData)! as Data, metadata: uploadMetadata) { (metadata, error) in
                    if error == nil {
                        if let thumbUrl = metadata?.downloadURL()?.absoluteString {
                            let m = MessageModel(
                                sender : self.sender,
                                receiver : self.receiver,
                                message : "Video Message",
                                timestamp : "" ,
                                uniqueId : "",
                                mediaImageUrl: thumbUrl,
                                mediaVideoUrl: videoUrl,
                                isMedia: true,
                                isVideo: true)
                            self.chatservice.sendMessage(m)
                        }
                    } else {
                        return
                    }
                }
                
            }
            
        }
        
    }
    // Upload image to firebase storage
    func uploadImageToFireBase(_ data:Data) {
        let imgFileName = self.senderId + timeStamp
        let storageRef = FIRStorage.storage().reference(withPath: "myImage/\(imgFileName).jpeg")
        let uploadMetadata = FIRStorageMetadata()
        uploadMetadata.contentType = "myImage/jpeg"
        storageRef.put(data, metadata: uploadMetadata) { (metadata, error) in
            if let url = metadata?.downloadURL()?.absoluteString {
                let m = MessageModel(
                    sender : self.sender,
                    receiver : self.receiver,
                    message : "Photo Message",
                    timestamp : "" ,
                    uniqueId : "",
                    mediaImageUrl: url,
                    mediaVideoUrl: "",
                    isMedia: true,
                    isVideo: false)
                self.chatservice.sendMessage(m)
            }
        }
    }
}


extension ChatMainViewController {
    func initiateFirebaseObserver() {
        //presenceRef.setValue(true)
        //presenceRef.onDisconnectSetValue(false)
    }
    @objc func appActive() {
        self.setPresenceOn()
    }
    
    @objc func appDeactive() {
        self.setPresenceOff()
    }
    
    func setPresenceOn() {
        FNode.presenceNode.child(self.sender.fbID).setValue(true)
    }
    
    func setPresenceOff() {
        FNode.presenceNode.child(self.sender.fbID).setValue(false)
    }
}

//MARK: ChatServiceDelegate
extension ChatMainViewController {
    func messageAdded(_ message:MessageModel) {
        var displayname = String()
        
        if message.sender.fbID == self.sender.fbID {
            displayname = self.sender.name
        } else {
            displayname = self.receiver.name
        }
        if let imageUrl =  URL(string: message.mediaImageUrl) {
            self.asyncronousPhotoMedia = AsyncPhotoMediaItem(withURL: imageUrl, isOperator: true )
        }
        if let videoImgUrl = URL(string: message.mediaImageUrl) {
            if let  videoUrl = URL(string: message.mediaVideoUrl) {
            self.videoMediaItem = AsyncVideoMediaItem(fileURL: videoUrl, imageUrl: videoImgUrl)
            }
        }
    
      
       if message.isMedia {
            if message.isVideo {
                 let jmV = JSQMessage(senderId: message.sender.fbID, displayName: displayname, media: self.videoMediaItem!)
                self.jsMessages.append(jmV!)
            } else {
                 let jmP = JSQMessage(senderId:  message.sender.fbID, displayName: displayname, media: self.asyncronousPhotoMedia!)
               self.jsMessages.append(jmP!)
            }
        } else {
            let jm = JSQMessage(senderId: message.sender.fbID, displayName: displayname, text: message.message)
            self.jsMessages.append(jm!)
        }
        self.messageModel.append(message)
        self.finishReceivingMessage()
    }
    
    func messageDeleted(_ message:MessageModel) {
        
    }
    
    func receiverIsWritingMessage() {
        
    }
    
    func receiverHasGoneOffline() {
        
    }
    
    func receiverHasComeOnline() {
        
    }
    
    func messageIsDelivered(_ message:MessageModel) {
        
    }
    
    func messageFailedToDeliver() {
        
    }
}

extension ChatMainViewController {
    //MARK: JSQMessageDelegate
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
       return self.jsMessages[indexPath.item]
    }

    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return self.jsMessages.count
    }

    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let tap = MyTapGesture(target: self, action: #selector(self.openProfile(_:)))
        tap.delegate = self
        let message = jsMessages[indexPath.item]
        if message.senderId == self.sender.fbID {
            cell.textView?.textColor = UIColor.white
            tap.facebookId = sender.fbID
            tap.isViewingOthersProfile = false
        } else {
            tap.facebookId = receiver.fbID
            tap.isViewingOthersProfile = true
            cell.textView?.textColor = UIColor.white
        }
        cell.avatarImageView.addGestureRecognizer(tap)
        // make sure imageView can be interacted with by user
        cell.avatarImageView.isUserInteractionEnabled = true
        return cell
    }
    

    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = jsMessages[indexPath.item]
        if message.senderId == senderId {
            return outgoingBubbleImageView
        } else {
            return incomingBubbleImageView
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        let message = jsMessages[indexPath.item]
        if message.senderId == senderId {
            if let img = self.senderImage.image {
                return JSQMessagesAvatarImageFactory.avatarImage(withPlaceholder: img, diameter: 20)
            }
        } else {
            if let img = self.receiverImage.image {
                return JSQMessagesAvatarImageFactory.avatarImage(withPlaceholder: img, diameter: 20)
            }
        }
        return  JSQMessagesAvatarImageFactory.avatarImage(withPlaceholder: UIImage(named: "ProfilePlaceholder"), diameter: 20)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        let message = messageModel[indexPath.row]
        if message.isMedia {
            let ctrl = Route.imagePreview
            if message.isVideo {
                playVideo(message.mediaVideoUrl, viewController: self)
            } else {
                
                ctrl.imageString = message.mediaImageUrl
                self.present(ctrl, animated: true, completion: nil)
            }
            
        }
        self.view.endEditing(true)
    }
    
    //MARK: SEND Data
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!,
                                     senderDisplayName: String!, date: Date!) {
        self.sendMessage(text)
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        self.finishSendingMessage(animated: true)
    }
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.mediaTypes = [kUTTypeImage as String , kUTTypeMovie as String]
        self.imagePicker.delegate  = self
        self.present(imagePicker, animated: true, completion: nil)
    }
}

extension ChatMainViewController: UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let mediaType:String = info[UIImagePickerControllerMediaType] as? String else {
                        self.dismiss(animated: true, completion: nil)
                        return}
        
                    if mediaType == (kUTTypeImage as String) {
                        if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage , let data = UIImageJPEGRepresentation(originalImage, 0.8) {
                            self.selectedImg = originalImage
                            uploadImageToFireBase(data)
            
                        }
                    } else if mediaType == (kUTTypeMovie as String) {
                        if let movieUrl = info[UIImagePickerControllerMediaURL] as? URL {
                            uploadVideoToFireBase(movieUrl)
                        }
                    }
                    self.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

class MyTapGesture: UITapGestureRecognizer {
    var facebookId = String()
    var isViewingOthersProfile = Bool()
}
