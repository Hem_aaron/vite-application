////
////  FirebaseHelper.swift
////  FirebaseTest
////
////  Created by Prajeet Shrestha on 2/1/16.
////  Copyright © 2016 Prajeet Shrestha. All rights reserved.
////
//
//import Firebase
//import FirebaseDatabase
//import FirebaseAuth
//import UIKit
//import Foundation
//typealias SuccessStatus = (Bool) -> Void
////Reference to firebase database
//let kFireRootRefString = "https://viteexclusive.firebaseio.com/"
////Test
////let kFireRootRefString = "https://templateprajeet.firebaseio.com/"
//
//
//let myRootRef = FIRDatabase.database().reference()
//let kFireChatNode = "ChatHistory"
//let kChatNode     = myRootRef.child("ChatHistory")
//let kChatMetaNode = myRootRef.child("ChatMeta")
//let kUserMetaNode = myRootRef.child("UserMeta")
//
//let kFirebaseServerValueTimestamp = [".sv":"timestamp"]
////Directly accessing child nodes
//
////var myRootRef = Firebase(url:"https://docs-examples.firebaseio.com/web/data/users/mchen/name")
//
////Creating sub ref
//var userRef = myRootRef.child("users/user")
//
///*
// Array in Firebase
// _________________
// _________________
// 
// 
// */
//
//class FirebaseHelper {
//    /*
//     Save data to Firebase
//     _______________________
//     1. setValue -> Write data to defined path, like message/user/date
//     2. updateChildValues -> Update Existing database
//     3. childByAutoId -> Generate a location with unique ID in database.
//     4. runTransactionBlock -> data that could be affected by concurrent usage.
//     */
//    // MARK: Save
//    internal func saveData(data:NSDictionary, onNode:FIRDatabaseReference, completionStatus:SuccessStatus) -> Void {
//        onNode.setValue(data) { (error, base) -> Void in
//            if error != nil {
//                completionStatus(false)
//            } else  {
//                completionStatus(true)
//            }
//        }
//    }
//    
//    func getFirebaseStatusRefForUserID(userID:String) -> FIRDatabaseReference {
//        return myRootRef.child("userStatus/\(userID)")
//    }
//    
//    func isUserOnline(userID:String, statusBlock:((status:Bool) -> ())) {
//        let ref = getFirebaseStatusRefForUserID(userID)
//        ref.observeSingleEventOfType(.Value, withBlock: { snapshot in
//            // do some stuff once
//            //            print(snapshot)
//            if let st = snapshot.value as? Bool where st == true {
//                statusBlock(status:st)
//            } else {
//                statusBlock(status: false)
//            }
//        })
//    }
//    
//     func updateData(data:NSDictionary, onNode:FIRDatabaseReference,completionStatus:SuccessStatus) {
//        onNode.updateChildValues(data as [NSObject : AnyObject]) { (error , base ) -> Void in
//            if error != nil {
//                print("success")
//                completionStatus(true)
//            } else {
//                print("failed")
//                completionStatus(false)
//            }
//        }
//    }
//    //MARK: RETRIEVE
//    
//    internal func retrieve (onNode:FIRDatabaseReference,
//                            messages:(([ChatUser]?) -> ())) {
//        onNode.observeEventType(.Value, withBlock: {
//            snapshot in
//            if snapshot.value is NSNull {
//                messages(nil)
//                return
//            }
//            // Loop through all users
//            var usersArray = [ChatUser]()
//            
//            for (key,value) in snapshot.value as! NSDictionary {
//                var msgs = [Message]()
//                for v in value.allValues {
//                    let m     = Message()
//                    m.message = v["message"] as? String
//                    m.time    = String(v["time"])
//                    msgs.append(m)
//                }
//                msgs.sortInPlace({ $0.time < $1.time })
//                let user             = ChatUser()
//                user.fbID            = key as! String
//                user.lastMessage     = msgs[msgs.count - 1].message
//                user.lastMessageTime = msgs[msgs.count - 1].time
//                usersArray.append(user)
//            }
//            
//            messages(usersArray)
//            }, withCancelBlock: {
//                error in
//                print(error.description)
//        })
//    }
//    
//    
//    func saveMessage(senderID: String, receiverID: String, message:String) {
//        var data =
//            [
//                "time":kFirebaseServerValueTimestamp,
//                "message":message,
//                "sender":"self"
//        ]
//        
//        
//        let senderNode = myRootRef.child("\(kFireChatNode)/\(senderID)/\(receiverID)").childByAutoId()
//        
//        // Save Data on Sender Node
//        self.saveData(
//            data,
//            onNode: senderNode)
//        {
//            status in
//            if status {
//            }
//        }
//        
//        data =
//            [
//                "time":kFirebaseServerValueTimestamp,
//                "message":message,
//                "sender":senderID,
//                "seen" : false
//        ]
//        
//        //Save data on Receiver Node
//        let receiverNode = myRootRef.child("\(kFireChatNode)/\(receiverID)/\(senderID)").childByAutoId()
//        self.saveData(
//            data,
//            onNode: receiverNode)
//        {
//            status in
//            if status {
//                
//            }
//        }
//    }
//    
//    /*
//     {
//     "lambeosaurus": {
//     "dimensions": {
//     "height" : 2.1,
//     "length" : 12.5,
//     "weight": 5000
//     }
//     },
//     "stegosaurus": {
//     "dimensions": {
//     "height" : 4,
//     "length" : 9,
//     "weight" : 2500
//     }
//     }
//     }
//     */
//    
//    
//    internal func retrieveChatWitSingleUser (onNode:FIRDatabaseReference,
//                                             messages:(([Message]?) -> ())) {
//        // print(onNode)
//        onNode.observeEventType(.Value, withBlock: {
//            snapshot in
//            if snapshot.value is NSNull {
//                messages(nil)
//                return
//            }
//            var messageArray = [Message]()
//            // Loop through all users
//            for (key,value) in snapshot.value as! NSDictionary {
//                let msg     = Message()
//                msg.user    = key as! String
//                msg.message = value["message"] as! String
//                msg.time    = String(value["time"])
//                msg.sender  = value["sender"] as! String
//                
//                /*
//                 If user has at anytime accessed this key it means that user has seen the message. Therefore
//                 seenStatus is set on the node.
//                 */
//                let seenStatus = ["seen":true]
//                onNode.childByAppendingPath(key as! String).updateChildValues(seenStatus)
//                messageArray.append(msg)
//            }
//            
//            messageArray.sortInPlace({ $0.time < $1.time })
//            messages(messageArray)
//            
//            }, withCancelBlock: { error in
//                print(error.description)
//        })
//        
//        
//        
//        
//    }
//    
//    internal func removeAllObservers (onNode:FIRDatabaseReference) {
//        onNode.removeAllObservers()
//        //onNode.removeObserverWithHandle("handle");
//    }
//    
//    internal func observeOnce(onNode:FIRDatabaseReference) {
//        onNode.observeSingleEventOfType(.Value, withBlock: { snapshot in
//            // do some stuff once
//        })
//    }
//
//    //MARK: Messages Count
//    
//    func getTotalUnseenMessageCountForUser(user:String,successBlock:((count:Int) -> ())) {
//        let userRef = myRootRef.child(kFireChatNode).childByAppendingPath(user)
//        userRef.observeEventType(.Value, withBlock: { userSnap in
//            var unseenMessagesCount = 0
//            if let chatData = userSnap.value as? [NSObject:AnyObject] {
//                for (_,value) in chatData {
//                    for (_,v) in value as! [NSObject:AnyObject]{
//                        if let seenStatus = v["seen"] as? Bool where seenStatus == false {
//                            unseenMessagesCount += 1
//                        }
//                    }
//                }
//            }
//            successBlock(count: unseenMessagesCount)
//            }, withCancelBlock: { error in
//                successBlock(count: 0)
//                print(error.description)
//        })
//    }
//    
//    func getUnseenMessageCountForUser(sender:String,receiver:String,successBlock:((count:Int) -> ())) {
//        let userRef = myRootRef.child(kFireChatNode).childByAppendingPath(sender).childByAppendingPath(receiver)
//        
//        
//        userRef.observeSingleEventOfType(.Value, withBlock: { userSnap in
//            var unseenMessagesCount = 0
//            if let snapVal = userSnap.value as? [NSObject:AnyObject] {
//                for (_,v) in snapVal{
//                    if let seenStatus = v["seen"] as? Bool where seenStatus == false {
//                        unseenMessagesCount += 1
//                    }
//                }
//            }
// 
//            successBlock(count: unseenMessagesCount)
//            }, withCancelBlock: { error in
//                successBlock(count: 0)
//                print(error.description)
//        })
//    }
//    
//    func saveUserMeta(userMeta:[String:AnyObject], user:String) {
//        kUserMetaNode.child(user).setValue(userMeta) {
//            (error, base) -> Void in
//            if error != nil {
//                print("Successfully saved userMeta")
//            } else  {
//                print("Failed to save userMeta")
//            }
//        }
//    }
//    
//    
//    //MARK: Helpers
//    
//    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
//        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
//            do {
//                let json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers) as? [String:AnyObject]
//                return json
//            } catch {
//                print("Something went wrong")
//            }
//        }
//        return nil
//    }
//    
//    
//    func testType(value:AnyObject!){
//        
//        if let _
//            = value as? NSString{
//            
//            print("NSString")
//            
//        }else if let _ = value as? NSNumber{
//            
//            print("NSNumber")
//            
//        }else if let _ = value as? Double{
//            
//            print("Double")
//            
//        }else if let _ = value as? Int{
//            print("Int")
//        }
//    }
//}
//
//
//
