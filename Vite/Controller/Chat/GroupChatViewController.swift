//
//  ChatMainViewController.swift
//  FirebaseTest
//
//  Created by Eeposit1 on 3/6/16.
//  Copyright © 2016 Prajeet Shrestha. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import JSQMessagesViewController
import MobileCoreServices
import FirebaseStorage
import AVFoundation
import AVKit


class GroupChatViewController: JSQMessagesViewController, UITextFieldDelegate  {
    var chatservice:GroupChatService!
    //Always will be current loggedin user
    var users = [String]()
    var currentUser:ChatUser!
    var event:NEvent?
    var message:String!
    var jsMessages = [JSQMessage]()
    var messageModel = [GroupChatMessageModel]()
    var asyncronousPhotoMedia:AsyncPhotoMediaItem!
    var videoMediaItem: AsyncVideoMediaItem!
    let imagePicker = UIImagePickerController()
    
    
    var outgoingBubbleImageView: JSQMessagesBubbleImage!
    var incomingBubbleImageView: JSQMessagesBubbleImage!
    
    var timeStamp: String {
        return "\(Date().timeIntervalSince1970 * 1000)"
    }
    
    
    fileprivate func setupBubbles() {
        let factory = JSQMessagesBubbleImageFactory()
        outgoingBubbleImageView = factory?.outgoingMessagesBubbleImage(
            with: UIColor.jsq_messageBubbleBlue())
        incomingBubbleImageView = factory?.incomingMessagesBubbleImage(
            with: UIColor.jsq_messageBubbleGreen())
    }
    
    var newMessages = [GroupChatMessageModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.chatservice = GroupChatService(delegate: self, event: event!)
        self.chatservice.updateLastMessageCount(currentUser.fbID)
        self.setupNavigationbar()
        //self.setupNotifications()
        self.initiateFirebaseObserver()
        self.senderId = self.currentUser.fbID
        UIApplication.shared.statusBarStyle = .lightContent
        //TODO:
        self.title = self.event?.name
        self.setupBubbles()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.chatservice.updateLastMessageCount()
        // NotificationService().resetUnseenMessageCount(self.receiver.fbID)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.setPresenceOff()
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupNavigationbar() {
        if let nav = self.navigationController {
            nav.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            nav.navigationBar.shadowImage                  = UIImage()
            nav.view.backgroundColor                       = UIColor.white
            self.navigationItem.hidesBackButton            = true
            nav.isNavigationBarHidden = false
            let navBar                                     = self.navigationController!.navigationBar
            
            navBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.purple , NSAttributedStringKey.font: UIFont(name: AppProperties.viteFontName , size: AppProperties.viteNavigationTitleFontSize)!]
        }
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(appActive), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appDeactive), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
    func sendMessage(_ text:String) {
        var msgString = text
        msgString     = msgString.trimmingCharacters(in: CharacterSet.whitespaces)
        if msgString.isEmpty {
            return
        }
        
        let m = GroupChatMessageModel(userName: currentUser.name,
                                      userId: currentUser.fbID,
                                      userImage: currentUser.profileImageUrl,
                                      message: msgString, mediaImageUrl: "" ,mediaVideoUrl: "", isMedia: false , isVideo: false)
        GroupChatService.sendMessage(m, event: self.event!, usersInGroup: self.users)
    }
    
    // upload video to firebase storage
    func uploadVideoToFireBase(_ url: URL) {
        let videoFileName = self.senderId + timeStamp
        let storageRef = FIRStorage.storage().reference(withPath: "myGroupChatVideo/\(videoFileName).mov")
        let uploadMetadata = FIRStorageMetadata()
        uploadMetadata.contentType = "video/quicktime"
        storageRef.putFile(url, metadata: uploadMetadata) { (metadata, error) in
            if let videoUrl = metadata?.downloadURL()?.absoluteString {
                let videoThumbnail = thumbnailForVideoAtURL((metadata?.downloadURL())!)
                let imgFileName = self.senderId + self.timeStamp
                let storageRef = FIRStorage.storage().reference(withPath: "myGroupChatImage/\(imgFileName).jpeg")
                let uploadMetadata = FIRStorageMetadata()
                uploadMetadata.contentType = "myImage/jpeg"
                // to upload video thumbnail
                storageRef.put((videoThumbnail?.lowestQualityJPEGNSData)! as Data, metadata: uploadMetadata) { (metadata, error) in
                    if error == nil {
                        if let thumbUrl = metadata?.downloadURL()?.absoluteString {
                            let m = GroupChatMessageModel(userName: self.currentUser.name,
                                                          userId: self.currentUser.fbID,
                                                          userImage: self.currentUser.profileImageUrl,
                                                          message: "Video Message",
                                                          mediaImageUrl: thumbUrl ,
                                                          mediaVideoUrl: videoUrl ,
                                                          isMedia:true ,
                                                          isVideo: true)
                            
                            GroupChatService.sendMessage(m, event: self.event!, usersInGroup: self.users)
                            
                        }
                    } else {
                        return
                    }
                }
                
            }
            
        }
    }
    // Upload image to firebase storage
    func uploadImageToFireBase(_ data:Data) {
        let imgFileName = self.senderId + timeStamp
        let storageRef = FIRStorage.storage().reference(withPath: "myGroupChatImage/\(imgFileName).jpeg")
        let uploadMetadata = FIRStorageMetadata()
        uploadMetadata.contentType = "myImage/jpeg"
        storageRef.put(data, metadata: uploadMetadata) { (metadata, error) in
            if let url = metadata?.downloadURL()?.absoluteString {
                let m = GroupChatMessageModel(userName: self.currentUser.name,
                                              userId: self.currentUser.fbID,
                                              userImage: self.currentUser.profileImageUrl,
                                              message: "Photo Message",
                                              mediaImageUrl: url ,
                                              mediaVideoUrl: "" ,
                                              isMedia:true ,
                                              isVideo: false)
                GroupChatService.sendMessage(m, event: self.event!, usersInGroup: self.users)
                
            }
        }
    }
    
}

extension GroupChatViewController {
    func initiateFirebaseObserver() {
        
    }
    @objc func appActive() {
        FNode.presenceNode.child(self.currentUser.fbID).setValue(true)
    }
    
    @objc func appDeactive() {
        FNode.presenceNode.child(self.currentUser.fbID).setValue(false)
    }
}

//MARK: ChatServiceDelegate
extension GroupChatViewController: GroupChatServiceDelegate {
    func messageAdded(_ message:GroupChatMessageModel) {
        var displayname = String()
        newMessages.append(message)
        if message.userFBID == self.currentUser.fbID {
            displayname = self.currentUser.name
        } else {
            if let displayName = message.userDisplayName {
                displayname = displayName
            }
        }
        if let image = message.mediaImageUrl{
            if  let url = URL(string: image) {
            self.asyncronousPhotoMedia = AsyncPhotoMediaItem(withURL: url, isOperator: true )
            }
        }
        if let videoUrl = message.mediaImageUrl , let videoMediaUrl =  message.mediaVideoUrl{
            if  let videoThubUrl = URL(string: videoUrl) , let vMediaUrl = URL(string: videoMediaUrl){
            self.videoMediaItem = AsyncVideoMediaItem(fileURL:  vMediaUrl, imageUrl: videoThubUrl)
            }
        }
        if message.isMedia == true{
            if message.isVideo == true{
                let jmV = JSQMessage(senderId: message.userFBID, displayName: displayname, media: self.videoMediaItem)
                self.jsMessages.append(jmV!)
            } else {
                let jmP = JSQMessage(senderId:  message.userFBID, displayName: displayname, media: self.asyncronousPhotoMedia)
                self.jsMessages.append(jmP!)
            }
        } else {
              let jm = JSQMessage(senderId: message.userFBID, displayName: displayname, text: message.message)
            self.jsMessages.append(jm!)
        }
        self.messageModel.append(message)
        self.finishReceivingMessage()
    }
    
    func messageDeleted(_ message:GroupChatMessageModel) {
        
    }
    
    func receiverIsWritingMessage() {
        
    }
    
    func receiverHasGoneOffline() {
        
    }
    
    func receiverHasComeOnline() {
        
    }
    
    func messageIsDelivered(_ message:GroupChatMessageModel) {
        
    }
    
    func messageFailedToDeliver() {
        
    }
}

extension GroupChatViewController {
    //MARK: JSQMessageDelegate
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return self.jsMessages[indexPath.item]
    }

    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return self.jsMessages.count
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell

        let message = jsMessages[indexPath.item]
        let gM = newMessages[indexPath.item]
        if let image = gM.userProfileURL?.convertIntoViteURL() {
            cell.avatarImageView.sd_setImage(with: URL(string: image))
        }
        
        if message.senderId == self.currentUser.fbID {
            cell.textView?.textColor = UIColor.white
        } else {
            cell.textView?.textColor = UIColor.white
        }
        
        cell.avatarImageView.layer.cornerRadius = 15
        cell.avatarImageView.clipsToBounds = true
        cell.avatarImageView.contentMode = .scaleAspectFill
        cell.delegate = self
        
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = jsMessages[indexPath.item]
        if message.senderId == senderId {
            return outgoingBubbleImageView
        } else {
            return incomingBubbleImageView
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return  JSQMessagesAvatarImageFactory.avatarImage(withPlaceholder: UIImage(named: "ProfilePlaceholder"), diameter: 20)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        let message = messageModel[indexPath.row]
        if message.isMedia == true {
           let ctrl =  Route.imagePreview
            if message.isVideo == true , let videoU = message.mediaVideoUrl {
               playVideo(videoU, viewController: self)
            } else {
                ctrl.imageString = message.mediaImageUrl
                self.present(ctrl, animated: true, completion: nil)
            }
            
        }
        self.view.endEditing(true)
    }

    
    
    //MARK: SEND Data
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!,
                               senderDisplayName: String!, date: Date!) {
        self.sendMessage(text)
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        self.finishSendingMessage(animated: true)
    }
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.mediaTypes = [kUTTypeImage as String , kUTTypeMovie as String]
        self.imagePicker.delegate  = self
        self.present(imagePicker, animated: true, completion: nil)
    }
}
extension GroupChatViewController : JSQMessagesCollectionViewCellDelegate {
    func messagesCollectionViewCellDidTapAvatar(_ cell: JSQMessagesCollectionViewCell!) {
        
        
        let indexPath = self.collectionView.indexPath(for: cell)
        let message = newMessages[(indexPath?.row)!]
        let userId = message.userFBID
        
        if userId == self.currentUser.fbID {
            //Do not show user profile if it's
        } else {
            showProfileOfUser(userId!)
        }
    }
    
    func messagesCollectionViewCellDidTapMessageBubble(_ cell: JSQMessagesCollectionViewCell!) {
        
    }
    
    
    func messagesCollectionViewCellDidTap(_ cell: JSQMessagesCollectionViewCell!, atPosition position: CGPoint) {
        
    }
    
    func messagesCollectionViewCell(_ cell: JSQMessagesCollectionViewCell!, didPerformAction action: Selector, withSender sender: Any!) {
        
    }
    
    func showProfileOfUser(_ fbID:String) {
        // todo
//        let controller                       = Controller.Profile
//        controller.fbId                      = fbID
//        controller.isForViewingOthersProfile = true
//        self.navigationController?.show(controller, sender: nil)
    }
}
extension GroupChatViewController: UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let mediaType:String = info[UIImagePickerControllerMediaType] as? String else {
                        self.dismiss(animated: true, completion: nil)
                        return}
        
                    if mediaType == (kUTTypeImage as String) {
                        if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage , let data = UIImageJPEGRepresentation(originalImage, 0.8) {
                            uploadImageToFireBase(data)
            
                        }
                    } else if mediaType == (kUTTypeMovie as String) {
                        if let movieUrl = info[UIImagePickerControllerMediaURL] as? URL {
                            uploadVideoToFireBase(movieUrl)
                        }
                    }
                    self.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}

