//
//  ImagePreviewViewController.swift
//  Vite
//
//  Created by Eeposit 01 on 8/1/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit
import AVFoundation

class ImagePreviewViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    var imageString: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let image = imageString {
            self.imageView.sd_setImage(with: URL(string: image.convertIntoViteURL()))
        }
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeView(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
}
