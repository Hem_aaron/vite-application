//
//  ConnectionErrViewController.swift
//  Vite
//
//  Created by Eeposit  on 5/2/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit

class ConnectionErrViewController: UIViewController {

    @IBOutlet weak var imgVite: UIImageView!
    @IBOutlet weak var btnTryAgain: UIButton!
    @IBOutlet weak var constWidth: NSLayoutConstraint!
    @IBOutlet weak var constHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        changeSize(0, width: 0)
        btnTryAgain.isEnabled = false
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        animate()
    }
    func animate(){
        UIView.animate(withDuration: 3, delay: 1, usingSpringWithDamping: 0.2, initialSpringVelocity: 0.3, options: [.curveEaseInOut], animations: {
            self.changeSize(150, width: 150)
        }, completion:{_ in
            self.btnTryAgain.isEnabled = true
        })
    }
    func changeSize(_ height:CGFloat, width:CGFloat){
        self.constHeight.constant = height
        self.constWidth.constant = width
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tryAgain(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
