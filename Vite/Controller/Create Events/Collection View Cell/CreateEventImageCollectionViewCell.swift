//
//  CreateEventCollectionViewCell.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 4/11/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class CreateEventImageCollectionViewCell: UICollectionViewCell {
    var removeImage:(()->())?
    @IBOutlet weak var eventImage: ImageViewWithRoundedBorder!
    
    @IBAction func removeImage(_ sender: AnyObject) {
        if let rem = removeImage{
            rem()
        }
    }
}
