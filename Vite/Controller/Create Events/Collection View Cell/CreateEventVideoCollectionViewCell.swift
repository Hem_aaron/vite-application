//
//  CreateEventVideoCollectionViewCell.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 4/26/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class CreateEventVideoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var btnPlay: UIButton!
     var playVideo:(()->())?
     var removeVideo:(()->())?
    @IBOutlet weak var eventVideoThumbNailImg: ImageViewWithRoundedBorder!
    
    @IBAction func playVideo(_ sender: AnyObject) {
        if let video = playVideo{
            video()
        }
    }
    
    @IBAction func removeVideo(_ sender: AnyObject) {
        if let rem = removeVideo{
            rem()
        }

    }
}
