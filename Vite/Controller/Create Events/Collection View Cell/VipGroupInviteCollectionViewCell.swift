//
//  VipGroupInviteCollectionViewCell.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 5/15/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class VipGroupInviteCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var userImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.userImg.layer.cornerRadius = self.userImg.layer.frame.size.height / 2
        self.userImg.clipsToBounds = true
    }

}
