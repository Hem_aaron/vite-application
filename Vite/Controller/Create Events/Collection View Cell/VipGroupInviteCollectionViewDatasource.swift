//
//  VipGroupInviteCollectionViewDatasource.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 5/15/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class VipGroupInviteCollectionViewDatasource: NSObject {

    fileprivate var collectionView : UICollectionView!
    var vipCategoryFriendLists = [MyVIPFriend]()
    
    
    init(collectionView : UICollectionView , dataSource : [MyVIPFriend]) {
        
        self.collectionView = collectionView
        self.vipCategoryFriendLists = dataSource
        super.init()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
}
extension VipGroupInviteCollectionViewDatasource : UICollectionViewDataSource , UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vipCategoryFriendLists.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = vipCategoryFriendLists[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VipGrpCVCell", for: indexPath) as! VipGroupInviteCollectionViewCell
        if let url = data.profileImageUrl {
            cell.userImg.sd_setImage(with: URL(string: url.convertIntoViteURL()), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
        }
        
        return cell
    }
    

}
