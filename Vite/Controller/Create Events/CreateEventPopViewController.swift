//
//  CreateEventPopViewController.swift
//  Vite
//
//  Created by sajjan giri on 11/16/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import AVFoundation

enum eType:String {
    case Public  = "Public"
    case Private = "Private"
    case Paid    = "Paid"
    case Recurring = "Recurring"
    case RecurringPrivate = "RecurringPrivate"
    case RecurringPaid = "RecurringPaid"
    
}


class CreateEventPopViewController: UIViewController {

    var viteNow:Bool = false
    let indicator = ActivityIndicator()
    var parameter = [String:AnyObject]()
    var forFacebookEvent = Bool()
    var controller = UIViewController()
    var image = UIImage()
    var eventID = String()
    var imageUrl = String()
    var eventType = String()
    var videoPath = String()
    
    @IBOutlet weak var titleAndDescView: UIView!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventDesc: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnViteNow: UIButton!
   
    @IBOutlet weak var btnShareEvent: UIButton!
    
    @IBOutlet weak var btnInviteAll: UIButton!
    @IBOutlet weak var btnPeople: UIButton!
   
    @IBOutlet weak var btnInvite: UIButton!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        eventTypeLogic(eventType)
        fetchingEventDetail()
      self.indicator.stop()
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        self.imgView.roundCorners([.topRight , .topLeft], radius: 10)
    }
    
    
    func eventTypeLogic(_ eventType: String){
        btnInviteAll.isHidden = true
        switch eventType {
        case eType.Paid.rawValue:
            btnPeople.isHidden = true
            btnInviteAll.isHidden = true
            btnInvite.isHidden = true
        case eType.Private.rawValue:
             btnViteNow.isHidden = true
             btnInviteAll.isHidden = true
             btnShareEvent.isHidden = true
        case eType.Recurring.rawValue:
            btnViteNow.isHidden = true
        case eType.RecurringPrivate.rawValue:
            btnViteNow.isHidden = true
            btnInviteAll.isHidden = true
            btnShareEvent.isHidden = true
        case eType.RecurringPaid.rawValue:
            btnInviteAll.isHidden = true
            btnInvite.isHidden = true
            btnPeople.isHidden = true
        default:
            break
        }
    }
    
    func showPrivateFriendVC (_ eventId:String) {
        let controller = Route.Private
        controller.eventID = eventId
        let targetNavigationController = UINavigationController(rootViewController: controller)
        self.present(targetNavigationController, animated: true, completion: nil)
    }
    
    func fetchingEventDetail () {

        if self.videoPath.isEmpty{
            self.imgView.image = image
        }
        else{
            let player = AVPlayer(url: URL(fileURLWithPath:self.videoPath))
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = self.imgView.bounds
            playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            self.imgView.layer.addSublayer(playerLayer)
            player.play()
        }
        self.eventTitle.text = parameter["eventTitle"] as? String
        self.eventDesc.text  = parameter["eventDetail"] as? String
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        //need to uncomment
       //  Create a variable that you want to send
        let newProgramVar = CreateEventModel(eventID: eventID, eventTitle: self.eventTitle.text!, eventDetail: self.eventDesc.text!, imageUrl:self.imageUrl.convertIntoViteURL())
        // Create a new variable to store the instance of PlayerTableViewController
        let destinationVC = segue.destination as! InviteFriendsVC
        destinationVC.eventVar = newProgramVar
    }

    
   // MARK : BUTTON ACTION
    
    @IBAction func viteNow(_ sender: AnyObject) {
        self.btnViteNow.isEnabled = false
        self.indicator.start()
        
        
        // need to uncomment
        VCreateEventService.sendViteNow(self.eventID, successBlock: { (status) in
            if status {
                self.btnViteNow.isEnabled = false
                self.view.makeToast(message: localizedString(forKey: "VIEWNOW_NOTIFIED_ALL"))
            }
             self.indicator.stop()
        }) { (message) in
            showAlert(controller: self, title: "", message: message)
            self.btnViteNow.isEnabled = true
            self.indicator.stop()
        }
    }

    
    

    
    @IBAction func viteInviteFriend(_ sender: AnyObject) {
    
    }
   
    @IBAction func viteShareEvent(_ sender: AnyObject) {
        BranchShareHelper.ShareWithBranchActivity(self, eventID: self.eventID, eventTitle: self.eventTitle.text!, eventDesc: self.eventDesc.text!, shareText: self.eventTitle.text!, imageURL: self.imageUrl.convertIntoViteURL())
    }

    
    
    @IBAction func eventSkipAction(_ sender: AnyObject) {
        
        if forFacebookEvent{
            self.dismiss(animated: false, completion: {
                self.controller.navigationController?.popViewController(animated: false)
            })
            
        } else {
            self.dismiss(animated: true, completion: nil)
            Route.goToHomeController()
        }
 }
    
    
    @IBAction func invitePeople(_ sender: AnyObject) {
        showPrivateFriendVC(eventID)
    }
    
    @IBAction func inviteAllUsersAround(_ sender: AnyObject) {
        if let premStatus = UserDefaultsManager().userPremiumStatus {
            if premStatus == true{
                showAlertToInviteAll()
            } else {
                alertVerifyPremium()
            }
        }

    }
    
    func alertVerifyPremium() {
        let popController = Route.passportPop
        popController.premiumType = "viteAll"
        self.present(popController, animated: true, completion: nil)
    }
    
    func showAlertToInviteAll() {
        let alert = UIAlertController(title: "", message: localizedString(forKey: "INVITE_ALL_USERS"), preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.view.isUserInteractionEnabled = false
            self.indicator.start()
            if !self.eventID.isEmpty{
                VinviteFriendWebService.inviteAll(self.eventID, successBlock: { (success) in
                    if success{
                        self.indicator.stop()
                        self.view.isUserInteractionEnabled = true
                        Route.goToHomeController()
                        showAlert(controller: self, title: "", message: localizedString(forKey: "INVITED_ALL_USERS"))
                    }
                    }, failureBlock: { (msg) in
                        self.indicator.stop()
                        self.view.isUserInteractionEnabled = true
                        showAlert(controller: self, title: "", message: msg)
                })
            }

        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .default) { (action: UIAlertAction) -> Void in
                                            
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        present(alert,
                              animated: false,
                              completion: nil)
    }

}

