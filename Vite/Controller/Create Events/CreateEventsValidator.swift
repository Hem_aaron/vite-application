

extension CreateEventsViewController {
    func validateFormValuesAndGetFormValueAsParameter() -> (status:Bool,parameter:[String:AnyObject]?) {
        var errorMsg = [String:AnyObject]()
        
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.")
        let eventName = self.tfEventName.text
        if tfEventName.text!.isEmpty{
            errorMsg = [
                "msg": localizedString(forKey: "EVENT_NAME_REQUIRED") as AnyObject
            ]
            return (false, errorMsg)
            
        }
        
        let priceTicket = self.tfPrice.text
        let maxTicket = self.tfMaxTicketSale.text
        var discountPercent: Double?
        var discountAmt: Double?
        var promoCode: String?
        if eventTypeSegmentedControl.selectedSegmentIndex == 1 {
            if tfPrice.text!.rangeOfCharacter(from: characterset.inverted) != nil {
                errorMsg = [
                    "msg": localizedString(forKey: "PRICE_ERROR_SPECIAL_CHAR") as AnyObject
                ]
                return (false, errorMsg)
            }

            if tfPrice.text!.isEmpty{
                errorMsg = [
                    "msg": localizedString(forKey: "PRICE_FIELD_REQUIRED") as AnyObject
                ]
                return (false, errorMsg)
            }
            else if tfPrice.text == "0"{
                errorMsg = [
                    "msg": localizedString(forKey: "NO_ZERO_PRICE") as AnyObject
                ]
                return (false, errorMsg)
            }
            
            if tfMaxTicketSale.text!.isEmpty{
                errorMsg = [
                    "msg": localizedString(forKey: "MAX_TICKETS_REQUIRED") as AnyObject
                ]
                return (false, errorMsg)
                
            }   else if tfMaxTicketSale.text == "0"{
                errorMsg = [
                    "msg": localizedString(forKey: "MAX_TICKETS_NO_ZERO") as AnyObject
                ]
                return (false, errorMsg)
            }
            
            
            
            
            if  promoState {
                if txtPromoCode.text!.isEmpty{
                    errorMsg = [
                        "msg": localizedString(forKey: "PROMO_CODE_BLANK") as AnyObject
                    ]
                    return (false, errorMsg)
                } else if dType == DiscountType.NoSelect {
                    errorMsg = [
                        "msg": localizedString(forKey: "SELECT_DISCOUNT_TYPE") as AnyObject
                    ]
                    return (false, errorMsg)
                } else if txtDiscountAmtOrPercent.text!.isEmpty {
                    errorMsg = [
                        "msg": localizedString(forKey: "DISCOUNT_AMT_BLANK") as AnyObject
                    ]
                    return (false, errorMsg)
                }
                if txtDiscountAmtOrPercent.text!.rangeOfCharacter(from: characterset.inverted) != nil {
                    errorMsg = [
                        "msg": localizedString(forKey: "DISCOUNT_AMT_ERROR_CHAR") as AnyObject
                    ]
                    return (false, errorMsg)
                }
                if dType == DiscountType.Amount {
                    
                    if let amt = self.txtDiscountAmtOrPercent.text {
                        discountAmt = Double(amt)!
                        if Double(amt)! >= Double(tfPrice.text!)! {
                            errorMsg = [
                                "msg":localizedString(forKey: "DISCOUNT_AMT_ERROR_HIGH") as AnyObject
                            ]
                            return (false, errorMsg)
                        } else   if discountAmt == 0 {
                            errorMsg = [
                                "msg": localizedString(forKey: "DISCOUNT_AMT_ZERO") as AnyObject
                            ]
                            return (false, errorMsg)
                        }
                    }
                } else {
                    if let percent = self.txtDiscountAmtOrPercent.text {
                        discountPercent = Double(percent)!
                        if discountPercent! > 100.00 {
                            errorMsg = [
                                "msg": localizedString(forKey: "DISCOUNT_PERCENT_HIGH") as AnyObject
                            ]
                            return (false, errorMsg)
                        } else   if discountPercent == 0 {
                            errorMsg = [
                                "msg": localizedString(forKey: "DISCOUNT_PERCENT_ZERO") as AnyObject
                            ]
                            return (false, errorMsg)
                        }
                        
                    }
                }
                promoCode = self.txtPromoCode.text!.replacingOccurrences(of: " ", with: "")
            }
        }
        
        if self.dateTimeDisplay.text!.isEmpty{
            errorMsg = [
                "msg": localizedString(forKey: "EVENT_DATE_REQUIRED") as AnyObject
            ]
            return (false, errorMsg)
        }
        
        var dateFormatter = DateFormatter()
        self.dateFormatter.dateFormat = "YYYY-MM-dd HH:mm"
        let strDate = self.dateFormatter.date(from: self.dateTimeDisplay.text!)
        
        guard let date = strDate else {
            errorMsg = [
                "msg": localizedString(forKey: "EVENT_DATE_ERROR") as AnyObject
            ]
            return (false, errorMsg)
        }
        
        guard let eventAddress = self.lblLocation.text else{
            errorMsg = [
                "msg":  localizedString(forKey: "EVENT_ADDRESS_BLANK") as AnyObject
            ]
            return (false, errorMsg)
        }
        
        guard let eventDetail = self.tVDescription.text else {
            errorMsg = [
                "msg": localizedString(forKey: "EVENT_ADDRESS_BLANK") as AnyObject
            ]
            return (false, errorMsg)
        }
        
        guard let location = self.eventLocation else {
            errorMsg = [
                "msg": localizedString(forKey: "LOCATION_ERROR") as AnyObject
            ]
            return (false, errorMsg)
        }
        
        var imageString = String()
        if isFromFbEvent{
            if !userFbEvent!.imageUrl.isEmpty{
                imageString =  userFbEvent!.imageUrl
            }else{
                imageString = self.imgString//self.getCompressedBase64Image(image)
            }
        }else{
            imageString = self.imgString//self.getCompressedBase64Image(image)
        }
        
        if !self.isEditable{
            if imageString.isEmpty && self.videoID.isEmpty{
                errorMsg = [
                    "msg": localizedString(forKey: "SELECT_IMAGE") as AnyObject
                ]
                return (false, errorMsg)
            }
        }
        
        var localTimeZoneName: String { return (TimeZone.autoupdatingCurrent).identifier }
        
        let coord =
            [
                "givenLocation":eventAddress,
                "longitude":location.longitude,
                "latitude" : location.latitude
        ] as [String : Any]
        
        
        var business = [String: AnyObject]()
        if UserDefaultsManager().isBusinessAccountActivated{
            business =  [
                "entityId": UserDefaultsManager().userBusinessAccountID! as AnyObject,
            ]
        }
        
        
        
        //For Edit Event
        var parameters:[String:AnyObject]!
        if self.isEditable {
            parameters = [
                "eventTitle":eventName! as AnyObject,
                "location":coord as AnyObject,
                "eventDetail":eventDetail as AnyObject,
                "entityId":(self.event?.entityId)! as AnyObject,
                "bringAFriend":self.state as AnyObject,
                "eventDate":self.eventDate as AnyObject,
                "eventStartTime":self.eventTime as AnyObject,
                "business": business as AnyObject ,
                "timeZone": localTimeZoneName as AnyObject ,
                "interests" : interest as AnyObject ,
                "maxAge": maxAge as AnyObject ,
                "minAge": minAge as AnyObject ,
                "eventVideoId" : self.videoID as AnyObject ,
                "media": self.mediaParamArr as AnyObject ,
                "featured": self.isFeatureEvent as AnyObject
            ]
            
            if isImageChanged {
                    parameters["imageUrl"] = imageString as AnyObject
          
            }
            return (true, parameters)
            
        } else { //FOR Create Event
            parameters = [
                "featured" : isFeatureEvent as AnyObject,
                "imageUrl":imageString as AnyObject,
                "eventTitle":eventName! as AnyObject,
                "location":coord as AnyObject,
                "eventDetail":eventDetail as AnyObject,
                "viteNow":self.viteNow as AnyObject,
                "bringAFriend":self.state as AnyObject,
                "eventDate":self.eventDate as AnyObject,
                "eventStartTime":self.eventTime as AnyObject,
                "privateEvent":self.isPrivate as AnyObject,
                "facebookEventId": userFbEvent?.eventId as AnyObject,
                "business": business as AnyObject ,
                "timeZone": localTimeZoneName  as AnyObject,
                "interests" : interest  as AnyObject,
                "maxAge": maxAge  as AnyObject,
                "minAge": minAge  as AnyObject,
                "eventVideoId" : self.videoID  as AnyObject,
                "media": self.mediaParamArr  as AnyObject
            ]
            
            if eventTypeSegmentedControl.selectedSegmentIndex == 1 {

                //For paid events
                if self.accountStatus || self.paypalStatus {
                    parameters["paidEvent"] = true as AnyObject
                    parameters["maxNumAttendant"] = maxTicket! as AnyObject
                    parameters["eventPrice"] = priceTicket! as AnyObject
                    parameters["promoCode"] = promoCode as AnyObject
                    parameters["discountPercent"] = discountPercent as AnyObject
                    parameters["discountAmount"] = discountAmt as AnyObject 
                } else {
                    errorMsg = [
                        "msg": localizedString(forKey: "CONNECT_PAYPAL_STRIPE") as AnyObject
                    ]
                    
                    return (false, errorMsg)
                }
            }
            print(parameters)
            return (true, parameters)
        }
        
    }
    
    
    func getCompressedImage(_ image:UIImage) -> Data {
        self.indicator.start()
        var smallImage:UIImage = image
        
        while true {
            let imgData: Data = NSData(data: UIImageJPEGRepresentation((smallImage), 1)!) as Data
            let sizeInKB = imgData.count / 1024
            if sizeInKB <= 512 {
                
                return imgData
            }
            let newWidth = smallImage.size.width * (80/100)
            smallImage = self.resizeImage(smallImage, newWidth: newWidth)
            
        }
    }
    
    func getCompressedBase64Image(_ image:UIImage) -> String {
        self.indicator.start()
        var smallImage:UIImage = image
        
        while true {
            let imgData: Data = NSData(data: UIImageJPEGRepresentation((smallImage), 1)!) as Data
            let sizeInKB = imgData.count / 1024
            if sizeInKB <= 512 {
                
                return Base64.stringFromDataImage(imgData)!
            }
            
            let newWidth = smallImage.size.width * (80/100)
            smallImage = self.resizeImage(smallImage, newWidth: newWidth)
        }
    }
    
    func resizeImage(_ image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: floor(newWidth), height: floor(newHeight)))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
