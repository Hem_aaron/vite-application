//
//  CreateEventsViewController.swift
//  Vite
//
//  Created by Eeposit1 on 2/14/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import MapKit
import HTagView
import Alamofire
import AVFoundation
import CropViewController

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


protocol CreateEventsViewControllerDelegate : class {
    func editedEvent(_ eventID:String)
}



enum UserViteResponse:String {
    case Requested = "REQUESTED"
    case Rejected  = "USER_REJECTED"
}


enum EventType:String {
    case Public  = "Public"
    case Private = "Private"
    case Paid    = "Paid"
}

enum DiscountType: String {
    case Amount = "Amount"
    case Percent = "Percent"
    case NoSelect = ""
}

class CreateEventsViewController: WrapperController,UITextViewDelegate, UIScrollViewDelegate,HTagViewDelegate, HTagViewDataSource,UICollectionViewDataSource, UICollectionViewDelegate,UITextFieldDelegate,CreateEventVideoTrimDelegate, CropViewControllerDelegate{
   weak var delegate:CreateEventsViewControllerDelegate?
    @IBOutlet weak var tfEventName: CustomTextField!
    //@IBOutlet weak var dateTimeDisplay: CustomTextField!
    @IBOutlet weak var tfPrice: CustomTextField!
    @IBOutlet weak var tfMaxTicketSale: CustomTextField!
    @IBOutlet weak var tfLocation: CustomTextField!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var tVDescription: UITextView!
    // @IBOutlet weak var tickImage: UIImageView!
    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var btnCreate: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnFbEvent: UIBarButtonItem!
    @IBOutlet weak var btnBack: UIBarButtonItem!
    @IBOutlet weak var comingSoonImgView: UIImageView!
    @IBOutlet weak var dateTimeDisplay: DatePickerText!
    
    @IBOutlet weak var scrollView: UIScrollView!
    //@IBOutlet weak var checkBoxImg: UIImageView!
    //@IBOutlet weak var btnPrivateOption: UIButton!
    @IBOutlet weak var lblDescriptionCount: UILabel!
    @IBOutlet weak var eventImageHeight: NSLayoutConstraint!
    @IBOutlet weak var descTopSpaceToEventType: NSLayoutConstraint!
    @IBOutlet weak var eventTypeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var paymentView: UIView!
    @IBOutlet weak var btnStripeConnect: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnFeatureEvent: UIButton!
    @IBOutlet weak var btnStripeConnecttHeight: NSLayoutConstraint!
    @IBOutlet weak var btnPaypaltHeight: NSLayoutConstraint!
    
    //Mark : optional outlet to hide guest can bring a friend view
    
    //   @IBOutlet weak var guestContainerHeight: NSLayoutConstraint!
    
    //@IBOutlet weak var guestContainerTopSpace: NSLayoutConstraint!
    
    
    
    //MARK: Promo code outlets and variables
    @IBOutlet weak var btnTogglePromo: UIButton!
    @IBOutlet weak var lblPercent: UILabel!
    @IBOutlet weak var promoCodeView: UIView!
    @IBOutlet weak var topSpacePromo: NSLayoutConstraint!
    @IBOutlet weak var txtDiscountAmtOrPercent: CustomTextField!
    @IBOutlet weak var txtPromoCode: CustomTextField!
    @IBOutlet weak var txtDiscountType: CustomTextField!
    var promoState = false
    var dType:DiscountType = DiscountType.NoSelect
    var discountType: DiscountType {
        get {
            return self.dType
        } set {
            self.dType = newValue
        }
    }
    @IBOutlet weak var btnDiscountType: UIButton!
   
    
    //MARK: Reoccuring event constraints
    @IBOutlet weak var btnToggleRecurringEvent: UIButton!
    @IBOutlet weak var recurringEventFieldHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topSpaceForRecurring: NSLayoutConstraint!
    @IBOutlet weak var recurringEventFieldView: UIView!
    @IBOutlet weak var btnNever: UIButton!
    @IBOutlet weak var btnAfter: UIButton!
    @IBOutlet weak var btnOn: UIButton!
    @IBOutlet weak var txtStartsOn: DatePickerText!
    @IBOutlet weak var txtRepeat: CustomTextField!
    @IBOutlet weak var lblRepeatOn: UILabel!
    @IBOutlet weak var lblRepeatEvery: UILabel!
    @IBOutlet weak var lblRepeatsEveryText: UILabel!
    @IBOutlet weak var lblRepeat: UILabel!
    @IBOutlet weak var lblStartsOn: UILabel!
    @IBOutlet weak var lblEnd: UILabel!
    
    @IBOutlet weak var txtRepeatEvery: CustomTextField!
    @IBOutlet weak var repeatEveryView: UIView!
    @IBOutlet weak var repeatEveryViewHeight: NSLayoutConstraint!
    @IBOutlet weak var topSpaceStartsOnToRepeatField: NSLayoutConstraint!
    @IBOutlet weak var dayStackView: UIStackView!
    @IBOutlet weak var weekStackView: UIStackView!
    @IBOutlet weak var btnDayOfMonth: ButtonWithRoundedBorder!
    @IBOutlet weak var btnDayOfWeek: ButtonWithRoundedBorder!
    @IBOutlet weak var txtOccurences: CustomTextField!
    @IBOutlet weak var txtEndDate: DatePickerText!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    
    var isFromMyEventMenu = false
    var isRecurringEvent  = false
    var isForAddingUsersToInviteList = false
    var endDateType = "FOREVER"
    var repeatType = "REPEAT_WEEKLY"
    
    //MARK: Collection View Outlets
    @IBOutlet weak var eventImageVideoCollectionView: UICollectionView!
    @IBOutlet weak var eventImageVideoCVHeight: NSLayoutConstraint!
    @IBOutlet weak var multipleImageTransparantView: UIView!
    @IBOutlet weak var multimediaProgressPercentLbl: UILabel!
    @IBOutlet weak var multimediaProgressView: UIProgressView!
    @IBOutlet weak var uploadView: DashedBorderView!
    var eventImageArr = [String]()
    var isForMultipleImages = false
    var mediaArr = [VMedia]()
    var mediaArrId = [String]()
    var mediaParamArr = [[String: AnyObject]]()
    var mainMediaUrl: String?
    var mainMediaType: String?
    var mainMediaThumbnailUrl: String?
    var isFromFeaturedView = false
    
    @IBOutlet var repeatToggleBtns: [ButtonWithRoundedBorder]!
    var daysArr = [Int]()
    var daysValArr = [1,2,3,4,5,6,7]
    
    
    //MARK: age Filter
    @IBOutlet weak var ageRangeSlider: RangeSlider!
    @IBOutlet weak var lblMinAge: UILabel!
    @IBOutlet weak var progressPercentage: UILabel!
    @IBOutlet weak var lblMaxAge: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    var maxAge = Int()
    var minAge = Int()
    
    // MARK: Interest View Variables
    @IBOutlet weak var interestView: HTagView!
    var previousInterests    = [[String: AnyObject]]()
    var interest            = [[String: AnyObject]]()
    var tagLists = [String]()
    
    //FeaturedPopUpmsg
    @IBOutlet weak var imgFeaturedInfo: UIImageView!
    var featuredInfoState: Bool = false
    
    @IBOutlet weak var hiddenView: UIView!
    
    @IBOutlet weak var btnConnectPaypal: UIButton!
    
    
    //tapGesture Recognizer
    
    //Mark : Public Declarations
    fileprivate var eType:EventType!
    
    var eventType:EventType {
        get {
            return self.eType
        } set {
            if newValue == .Paid {
                self.showEventPaymentOption(250)
                self.isPrivate = false
                self.isPaid = true
            } else if newValue == .Private{
                self.showEventPaymentOption(0)
                self.isPrivate = true
                self.isPaid = false
            }else {
                self.showEventPaymentOption(0)
                self.isPrivate = false
                self.isPaid = false
            }
            self.eType = newValue
        }
    }
    var event:EventsOrganizer?
    var userFbEvent:FBUserEvent?
    var isFromSideMenu:Bool = false
    var isEditable:Bool = false
    var isImageChanged:Bool = false
    var viteNow:Bool = false
    var isFromFbEvent = false
    var imageHelper:ImageHelper!
    var state:Bool = false
    var isPrivate:Bool = false
    var isPaid:Bool = false
    var isForCheckingEventParam:Bool = true
    var locationManager:LocationManager? = LocationManager()
    var eventLocation:CLLocationCoordinate2D?
    let dateFormatter = DateFormatter()
    let timeFormatter = DateFormatter()
    var eventDate = String()
    var eventTime = String()
    let indicator = ActivityIndicator()
    var fbImageUrl = String()
    var params = [String:AnyObject]()
    var imgString = ""
    var accountStatus:Bool = false
    var paypalStatus:Bool = false
    var videoPath: URL? = nil
    var videoID = String()
    var videoLocalPath = String()
    var userConnectedToStripe = false
    var isFeatureEvent = false
    
    //Mark : CreateEvent Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hiddenView.isHidden = true
        imgFeaturedInfo.isHidden = true
        showHideMultiMediaTransView(true)
        self.progressView.isHidden = true
        self.progressPercentage.isHidden = true
        self.eventImageVideoCollectionView.delegate = self
        self.eventImageVideoCollectionView.dataSource = self
        self.txtPromoCode.delegate = self
        self.tfPrice.delegate = self
        self.txtDiscountAmtOrPercent.delegate = self
        self.txtPromoCode.autocorrectionType = UITextAutocorrectionType.no
        self.txtPromoCode.keyboardType = UIKeyboardType.asciiCapable
        self.tfEventName.keyboardType = UIKeyboardType.asciiCapable
        
        setDefaultValue()
        ageRangeSlider.addTarget(self, action: #selector(CreateEventsViewController.rangeSliderValueChanged(_:)), for: .valueChanged)
        getTagList()
        
        if DeviceType.IS_IPHONE_5 {
            changeFontForSmallScreenDevice(UIFont(name: "Avenir Heavy", size: 12)!)
        }
        
        dateTimeDisplay.addTarget(self, action: #selector(CreateEventsViewController.textFieldDidChange(_:)), for: UIControlEvents.editingDidEnd)
        self.tfPrice.keyboardType = UIKeyboardType.decimalPad
        self.title                            = "Create Event"
        self.paymentView.alpha                = 0
        self.descTopSpaceToEventType.constant = 0
        self.topSpaceForRecurring.constant = 70
        self.recurringEventFieldView.alpha = 0
        self.topSpacePromo.constant = 70
        self.promoCodeView.alpha = 0
        self.eventType                        = .Public
        self.imageHelper                      = ImageHelper(controller: self)
        
        if self.isEditable {
            if let recEvent = event?.recurringEvent {
                if recEvent{
                    btnToggleRecurringEvent.setImage(UIImage(named: "TickRecurringEvent"), for: UIControlState())
                    showRecurringEventView(378, alphaValue: 1)
                } else {
                    showRecurringEventView(70, alphaValue: 0)
                    btnToggleRecurringEvent.isEnabled = false
                    btnToggleRecurringEvent.setImage(UIImage(named: "NoTickRecurringEvent"), for: UIControlState())
                }
                
            }
            self.title = "Edit Event"
            self.tfLocation.placeholder = ""
            
            self.btnCancel.setTitle("CANCEL", for: UIControlState())
            self.btnCancel.addTarget(self, action: #selector(self.cancel), for: .touchUpInside)
            
            self.btnCreate.setTitle("DONE", for: UIControlState())
            self.btnCreate.addTarget(self, action: #selector(self.done), for: .touchUpInside)
            
            btnFbEvent.isEnabled = false
            self.preloadFieldsWithEvent()
        }
        btnFbEvent.isEnabled = UserDefaultsManager().isBusinessAccountActivated ? false : true
        if self.isFromFbEvent {
            populateFbEvent()
            btnFbEvent.isEnabled = false
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(textViewChanged), name: NSNotification.Name.UITextViewTextDidChange, object:self.tVDescription)
        // tapGesture action
        tapGesture()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.promoCodeDisabled), name: NSNotification.Name(rawValue: "promoButtonNotification"), object: nil)
         notificationCenter.addObserver(self, selector: #selector(self.getPaypalEmailStatus), name: NSNotification.Name(rawValue: "PayPalEmailAdded"), object: nil)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.indicator.parentView = self.selectedImageView
        self.indicator.stop()
        self.view.endEditing(false)
        self.getStripeAccountStatus()
        self.getPaypalEmailStatus()
        self.stopIndicator()
    }
    
    @objc func promoCodeDisabled(){
        btnTogglePromo.isEnabled = false
        showPromoView(70, alphaValue: 0)
        btnTogglePromo.setImage(UIImage(named: "NoTickRecurringEvent"), for: UIControlState())
    }

    
    //tapgesture initialize
    func tapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapHiddenView))
        hiddenView.addGestureRecognizer(tap)
    }
    
    //MARK: Notification call for custom texfield delegate
    @objc func didTapHiddenView() {
        featuredInfoState = false
        imgFeaturedInfo.isHidden = true
        self.hiddenView.isHidden = true
    }
    
    // Method to animate hide or unhide payment option according to the event type choosen
    func showEventPaymentOption(_ constantValue: CGFloat) {
        descTopSpaceToEventType.constant = constantValue
        UIView.animate(withDuration: 0.3 , animations: {
            self.view.layoutIfNeeded()
            if(self.eventTypeSegmentedControl.selectedSegmentIndex == 1){
                self.paymentView.alpha = 1
            } else {
                self.paymentView.alpha = 0
            }
        })
    }
    
    //method to check organizer's stripe status and paypal email status
    func getStripeAccountStatus(){
        var fbID = UserDefaultsManager().userFBID!
        if UserDefaultsManager().isBusinessAccountActivated{
            fbID = UserDefaultsManager().userBusinessOwnerId!
        }
        VStripeWebService.checkStripeStatusForAccount(fbId: fbID,{ (accountStatus) in
            self.accountStatus = accountStatus
            if self.accountStatus{
                self.btnStripeConnect.isEnabled = false
                self.btnStripeConnect.alpha = 1.0
                //self.btnStripeConnect.backgroundColor = UIColor.gray
               //self.btnStripeConnect.setTitle("Connected With Stripe", for: UIControlState())
                self.btnStripeConnect.setImage(UIImage(named: "StripeConnected"), for: UIControlState.normal)
            } else {
                if UserDefaultsManager().isBusinessAccountActivated{
                    if fbID != UserDefaultsManager().userFBID{
                        self.eventTypeSegmentedControl.setEnabled(false , forSegmentAt: 1)
                    }
                }
                  self.btnStripeConnect.isEnabled = true
                self.btnStripeConnect.setImage(UIImage(named: "StripeNotConnected"), for: UIControlState.normal)
            }
        }) { (msg) in
             print(msg)
        }
    }
    
    func getPaypalEmailStatus() {
        var userFbId = UserDefaultsManager().userFBID!
        if UserDefaultsManager().isBusinessAccountActivated{
            userFbId = UserDefaultsManager().userBusinessOwnerId!
        }
        PaypalWebService.checkPaypalStatus(fbId: userFbId, { (paypalEmailStatus) in
            self.paypalStatus = paypalEmailStatus
            if self.paypalStatus {
                self.btnConnectPaypal.isEnabled = false
                self.btnConnectPaypal.setImage(UIImage(named: "PaypalConnected"), for: UIControlState.normal)
            } else {
                self.btnConnectPaypal.isEnabled = true
                self.btnConnectPaypal.setImage(UIImage(named: "PaypalNotConnected"), for: UIControlState.normal)
            }
        }) { (msg) in
            print(msg)
        }
    }
    
    @IBAction func goToPaypalEmail(_ sender: Any) {
        if UserDefaultsManager().isBusinessAccountActivated{
            if UserDefaultsManager().userBusinessOwnerId! == UserDefaultsManager().userFBID! {
                let controller = Route.paypalEmailVc
                self.present(controller, animated: true, completion: nil)
            } else {
                showAlert(controller: self, title: "Alert", message: localizedString(forKey: "MUST_BE_BUSINESS_ACCOUNT_OWNER_PAYPAL"))
            }
        } else {
            let controller = Route.paypalEmailVc
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    
    // parameter of event before any edit is done
    func getPreEventParameter() -> [String:AnyObject]{
        guard let e = event , let lon = e.location?.longitude, let lat = e.location?.latitude else {
            return [String:AnyObject]()
        }
        var localTimeZoneName: String { return (TimeZone.autoupdatingCurrent).identifier }
        
        var business = [String: AnyObject]()
        if UserDefaultsManager().isBusinessAccountActivated{
            business =  [
                "entityId": UserDefaultsManager().userBusinessAccountID! as AnyObject,
            ]
        }
        
        let coord: [String:AnyObject] =
            [
                "givenLocation":(event?.location?.givenLocation as AnyObject) ,
                "longitude": Double(lon)! as AnyObject,
                "latitude" : Double(lat)! as AnyObject
        ]
        
        let  parameter: [String:AnyObject] =
            [
                "entityId":event?.entityId as AnyObject ,
                "eventTitle":event?.eventTitle as AnyObject ,
                "location":coord as AnyObject ,
                "eventDetail":event?.eventDetail as AnyObject ,
                "bringAFriend":event?.bringAFriend as AnyObject ,
                "eventDate":event?.eventDate as AnyObject ,
                "eventStartTime": event?.eventStartTime as AnyObject ,
                "business": business as AnyObject ,
                "timeZone": localTimeZoneName as AnyObject
                
                
        ]
        return parameter
    }
    
    // parameter of event when some value is changed
    func getPostEventParameter() -> [String:AnyObject]{
        setDateAndTime()
        let response = validateFormValuesAndGetFormValueAsParameter()
        return response.parameter!
    }
    
    // check if the post and pre event parameters are equal (this is done to show the alert message)
    func checkValueChangeInEventParameter() -> Bool {
        
        let preParameter  = getPreEventParameter()
        let postParameter = getPostEventParameter()
        // check if this method is called when back button is pressed i.e not when edit done button is clicked
        if isForCheckingEventParam {
            if NSDictionary(dictionary: preParameter).isEqual(to: postParameter){
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    }
    
    
    @objc func textViewChanged(_ notification:Foundation.Notification) {
        let object = notification.object as! UITextView
        
        if object.text.count >= 151  {
            object.text =  object.text.substring(to: object.text.index(before: object.text.endIndex))
        }
        self.lblDescriptionCount.text = "\(150 - object.text.count)"
    }
    
    
    @IBAction func playEventMainVideo(_ sender: AnyObject) {
        guard let url = mainMediaUrl?.convertIntoViteURL() else {
            showAlert(controller: self, title: "", message: localizedString(forKey: "ERROR_PLAYING_VIDEO"))
            return
        }
        playVideo(url, viewController: self)
        
    }
    
    func preloadFieldsWithEvent() {
        self.mainMediaType = event?.fileType
        self.mainMediaUrl = event?.eventVideoUrl
        self.btnToggleRecurringEvent.isEnabled = false
        self.eventTypeSegmentedControl.isEnabled = false
        self.tfEventName.text = self.event?.eventTitle
        if let eventPrice =  self.event?.eventPrice {
            self.tfPrice.text  = "\(eventPrice)"
        }
        if let maxNumberOfAttende = event?.maxNumAttendant {
            self.tfMaxTicketSale.text = "\(maxNumberOfAttende)"
        }
        self.tVDescription.text = self.event?.eventDetail
        self.lblDescriptionCount.text = "\(150 - self.tVDescription.text.count)"
        if event?.fileType == "IMAGE"{
            self.btnPlay.isHidden = true
            self.selectedImageView.sd_setShowActivityIndicatorView(true)
            self.selectedImageView.sd_setImage(with: URL(string:(self.event?.imageUrl?.convertIntoViteURL())!))
        } else {
            self.btnPlay.isHidden = false
            self.selectedImageView.sd_setShowActivityIndicatorView(true)
            if let thumbnailUrl = self.event?.videoProfileThumbNail{
                self.selectedImageView.sd_setImage(with: URL(string: thumbnailUrl.convertIntoViteURL()))
            }
        }
        
        if(self.event?.eventDate != nil && self.event?.eventStartTime != nil){
            self.dateTimeDisplay.text = (self.event?.eventDate)! + " " + (self.event?.eventStartTime)!
            let dateTime = self.dateTimeDisplay.text!
            let endIndex = dateTime.index(dateTime.endIndex, offsetBy: -3)
            self.dateTimeDisplay.text = String(dateTime[..<endIndex])
            self.txtStartsOn.text = (self.event?.eventDate)!
        }
        
        if let givenLocation = self.event?.location?.givenLocation{
            lblLocation.text = givenLocation
        }
        let coordinate = CLLocationCoordinate2DMake(Double(event!.location!.latitude!)!, Double(event!.location!.longitude!)!)
        self.eventLocation = coordinate
        if let privateEvent = event?.isPrivateEvent {
            switch privateEvent {
            case true:
                self.eventTypeSegmentedControl.selectedSegmentIndex = 2
            default:
                self.eventTypeSegmentedControl.selectedSegmentIndex = 0
            }
            
        }
        if let paidEvent = event?.isPaidEvent {
            switch paidEvent {
            case true:
                self.eventTypeSegmentedControl.selectedSegmentIndex = 1
               // self.tfMaxTicketSale
                self.showEventPaymentOption(150)
                self.paymentView.isUserInteractionEnabled = false
                self.btnStripeConnecttHeight.constant = 0
                self.btnPaypaltHeight.constant = 0
            default:
                break
            }
        }
        
        if let interestsList = event?.interests{
            self.previousInterests = interestsList
            self.interest = interestsList
        }
        if let maxmAge = event?.maxAge, let minmAge = event?.minAge {
            maxAge = maxmAge
            minAge  = minmAge
            lblMaxAge.text = "\(maxmAge)"
            lblMinAge.text = "\(minmAge)"
            ageRangeSlider.lowerValue = Double(minmAge)
            ageRangeSlider.upperValue = Double(maxmAge)
        }
        
        if let recEvent = event?.recurringEvent {
             self.isRecurringEvent = recEvent
            if recEvent  {
                btnFeatureEvent.isEnabled = false
            } else {
                if let featureEvent = event!.featuredEvent {
                    if featureEvent {
                        btnFeatureEvent.isEnabled = false
                        self.btnFeatureEvent.setImage(UIImage(named: "TickRecurringEvent"), for: UIControlState())
                    } else {
                        btnFeatureEvent.isEnabled = true
                        self.btnFeatureEvent.setImage(UIImage(named: "NoTickRecurringEvent"), for: UIControlState())
                    }
                }
            }
        }
        

        
        if let code = event?.promoCode {
            txtPromoCode.text = code
            txtPromoCode.isEnabled = false
            txtDiscountAmtOrPercent.isEnabled = false
            btnDiscountType.isEnabled = false
            txtDiscountType.isEnabled = false
            btnTogglePromo.setImage(UIImage(named: "TickRecurringEvent"), for: UIControlState())
            showPromoView(250, alphaValue: 1)
            if let amt = event?.discountAmount{
                lblPercent.isHidden = true
                txtDiscountType.text = "Dollar"
                txtDiscountAmtOrPercent.text = "\(amt)"
            }
            if let percent = event?.discountPercent{
                txtDiscountType.text = "Percentage"
                lblPercent.isHidden = false
                txtDiscountAmtOrPercent.text = "\(percent)"
            }
        } else {
            txtPromoCode.isEnabled = true
             txtDiscountType.isEnabled = true
            txtDiscountAmtOrPercent.isEnabled = true
            btnDiscountType.isEnabled = true
            showPromoView(70, alphaValue: 0)
            btnTogglePromo.setImage(UIImage(named: "NoTickRecurringEvent"), for: UIControlState())
        }
        
        if let rType = event?.repeatType{
            populateRepeatType(rType)
        }
        
        if let rOnOccurence = event?.repeatOnOccurence{
            txtRepeatEvery.text = "\(rOnOccurence)"
        }
        
        if let eType = event?.endType, let eValue = event?.endValue{
            populateEndType(eType,endValue: eValue)
        }
        
        if let evDays = event?.eventDays{
            daysArr = evDays
            for btn in repeatToggleBtns {
                let value = btn.tag
                if daysArr.contains(value){
                    btn.backgroundColor = UIColor.colorFromRGB(rgbValue: 0x1990DB)
                }else {
                    btn.backgroundColor = UIColor.lightGray
                }
            }
        }
        
        if let media = event?.media {
            self.mediaArr = media
            self.getParamForMultipleFileUpload(mediaArr)
            if media.count > 0 {
                self.eventImageVideoCVHeight.constant = 250
                self.eventImageVideoCollectionView.reloadData()
            }else {
                showHideCollectionView(0)
            }
        }
    }
    
    func startIndicator(){
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
    }
    
    func stopIndicator(){
        self.activityIndicator.isHidden = true
        self.activityIndicator.startAnimating()
    }
    
    //populate event information to "create event page" from facebook event
    func populateFbEvent(){
        self.tfEventName.text         = self.userFbEvent?.eventName
        self.tVDescription.text       = self.userFbEvent?.eventDescription
        self.lblDescriptionCount.text = "\(90 - self.tVDescription.text.count)"
        
        self.selectedImageView.sd_setImage(with: URL(string:(self.self.userFbEvent?.imageUrl)!))
        
        let dateAndTime = convertDateFormaterFromFb((self.userFbEvent?.eventStartTime)!)
        self.dateTimeDisplay.text = dateAndTime.0 + " " + dateAndTime.1
        let eventAddress = (self.userFbEvent?.city)! + ", " + (self.userFbEvent?.country)!
        
        //check if the event Address contains text other than ", "
        if eventAddress.count > 2{
            self.tfLocation.placeholder = ""
            lblLocation.text = eventAddress
        }
        if let lat = userFbEvent?.latitude , let lon = userFbEvent?.longitude, lat != 0{
            let coordinate = CLLocationCoordinate2DMake(Double(lat), Double(lon))
            self.eventLocation = coordinate
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Actions when view is editable
    //MARK: Edit Event done
    @objc func done() {
        if !checkValueChangeInEventParameter() {
            self.setDateAndTime()
            let response = self.validateFormValuesAndGetFormValueAsParameter()
            if response.status {
                let indicator = ActivityIndicator()
                indicator.parentView = self.view
                indicator.start()
                btnBack.isEnabled = true
                btnFbEvent.isEnabled = true
                if self.isRecurringEvent{
                    let reoccuringResponse = self.validateFormValuesAndGetFormValueAsParameterForRecurringEvent()
                    if reoccuringResponse.status {
                        editRecurringEventServiceCall(reoccuringResponse.parameter!)
                    } else {
                        if let errorMsg = reoccuringResponse.parameter!["msg"] as? String{
                            indicator.stop()
                            showAlert(controller: self, title: "", message: errorMsg)
                        }
                    }
                } else {
                    if !isFeatureEvent {
                        editEventServiceCall(response.parameter!)
                    } else {
                        goToFeatureTicket()
                        indicator.stop()
                    }
                }
                
            } else {
                if let errorMsg = response.parameter!["msg"] as? String{
                    showAlert(controller: self, title: "", message: errorMsg)
                }
            }
            
        } else {
            cancel()
        }
    }
    // MARK: Edit Event API Call
    func editEventServiceCall(_ parameter: [String:AnyObject]){
        VCreateEventService.editEvent(parameter, successBlock: { event in
            if let imageurl = event["imageUrl"] as? String{
                self.triggerNotificationForEdit(parameter, imageURL: imageurl.convertIntoViteURL())
            } else {
                if let url = self.mainMediaThumbnailUrl{
                    self.triggerNotificationForEdit(parameter, imageURL: url.convertIntoViteURL())
                }
            }
            self.isForCheckingEventParam = false
            if let del = self.delegate {
                del.editedEvent((self.event?.entityId)!)
            }
            self.indicator.stop()
            if self.isFromFeaturedView {
                self.navigationController?.popToRootViewController(animated: true)
            } else {
                self.goBack()
            }
            self.btnBack.isEnabled = true
            self.btnFbEvent.isEnabled = true

            }, failureBlock: {
                (message) in
                self.indicator.stop()
                self.btnBack.isEnabled = true
                self.btnFbEvent.isEnabled = true
                showAlert(controller: self, title: "", message: message)
        })
    }
    
    
    func triggerNotificationForEdit(_ eventData:[String:AnyObject], imageURL:String) {
        var eventUID = String()
        if !self.isRecurringEvent{
            eventUID = eventData["entityId"] as! String
        }else {
            if let recEvent = event?.scheduledEventId{
                eventUID = recEvent
            }
            
        }
        let eventName = eventData["eventTitle"] as! String
        indicator.stop()
        let events = NEvent(name: eventName, uid: eventUID, imageUrl: imageURL)
        let sender    = UserDefaultsManager.getActiveUserForNotification()
        NotificationService().sendEditNotificationToAttendeeOfEvent(events, sender: sender)
    }
    
    @objc func cancel() {
        self.popOut()
    }
}

//MARK: Helper Methods
extension CreateEventsViewController {
    
    func setDateAndTime() {
        if self.dateTimeDisplay.text != "" {
            let dateStringArr = self.dateTimeDisplay.text!.components(separatedBy: " ")
            eventDate = dateStringArr[0]
            eventTime = dateStringArr[1]+":00"
            txtStartsOn.text = eventDate
        }
        
    }
    
    
    func clearEventDetails(){
        self.tfEventName.text                               = nil
        self.dateTimeDisplay.text                           = nil
        self.tVDescription.text                             = nil
        self.lblDescriptionCount.text                       = "90"
        self.selectedImageView.image                        = UIImage(named: "NoEvent")
        lblLocation.text                                    = ""
        self.tfLocation.placeholder                         = "Location"
        self.eventLocation                                  = nil
        //tickImage.image                                     = UIImage(named: "BoxUnselected")
        self.eventTypeSegmentedControl.selectedSegmentIndex = 0
    }
    
    func popOut() {
        guard let nav = self.navigationController else {
            return
        }
        nav.popViewController(animated: true)
    }
    
    
    func convertFbEventDateToNSDate(_ date: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let finaldate = dateFormatter.date(from: date)
        return finaldate!
    }
    
    func getEventLocation (_ eventLocation: CLLocationCoordinate2D){
        let controller = Route.mapController
        controller.isFromMyEventMenu = isFromMyEventMenu
        controller.currentCoordinateOfAnnotation = {
            coordinate in
            self.eventLocation = coordinate
            let indicator = ActivityIndicator()
            indicator.parentView = self.lblLocation
            indicator.tintColor = UIColor.gray
            indicator.start()
            
            LocationManager.getLocationStringFromCoordinate(coordinate: coordinate,
                                                            completionBlock: {
                                                                (location) -> Void in
                                                                self.lblLocation.text = location ?? ""
                                                                self.tfLocation.placeholder = ""
                                                                indicator.stop()
                                                                
            })
        }
        controller.currentLocation = eventLocation
        self.show(controller, sender: nil)
    }
    
    func postEventWithParameter(_ parameter:[String:AnyObject]) {
        self.view.isUserInteractionEnabled = false
        btnBack.isEnabled = false
        btnFbEvent.isEnabled = false
        //Update viteNow key
        var updatedParam:[String:AnyObject]!
        updatedParam = parameter
        updatedParam.updateValue(self.viteNow as AnyObject, forKey: "viteNow")
        VCreateEventService.createEvent(parameter, successBlock: {
            (eventID,imageUrl) in
            self.btnBack.isEnabled = true
            self.btnFbEvent.isEnabled = true
            self.view.isUserInteractionEnabled = true
            self.stopIndicator()
            //CreatePopSegue
            self.showCreateEventPopup(eventID, eventImage: imageUrl, eventTypeName: self.eventType.rawValue)
            //  }
            let eventName = parameter["eventTitle"] as! String
            //PUT Empty string from now need to get imageURL From server
            let eventURL = imageUrl.convertIntoViteURL()
            let event    = NEvent(name: eventName, uid: eventID, imageUrl: eventURL)
            let sender   = UserDefaultsManager.getActiveUserForNotification()
            if self.eventType != .Private {
                NotificationService().sendNotificationToUsersInPeripheryOfEventCreated(event, sender: sender)
            }

            if self.isForAddingUsersToInviteList {
                self.inviteAllSwappedUsers(eventID)
            }

            }, failureBlock: {
                message in
                self.btnBack.isEnabled = true
                self.btnFbEvent.isEnabled = true
                self.view.isUserInteractionEnabled = true
                showAlert(controller: self, title: "Error", message: message)
                self.stopIndicator()
        })
    }
    
    // Invites users to the created event if it is for adding swiped list
    func inviteAllSwappedUsers(_ eventId:String) {
        VEventWebService.inviteUsers(SwappedPeopleService.getPeopleList(),
                                     withEventId:eventId,
                                     successBlock: {
                                        SwappedPeopleService.clearSwappedList()
            }, failureBlock: {
                message in
                showAlert(controller: self, title: "", message: localizedString(forKey: "FAILED_SENDING_VITE"))
        })
    }
    
    func showToast(_ message:String) {
        
    }
    
    func showPrivateFriendVC (_ eventID:String) {
        let controller = Route.Private
         controller.eventID = eventID
        self.present(controller, animated: true, completion: nil)
        self.show(controller, sender: nil)
    }
    
    func showCreateEventPopup(_ eventID:String,eventImage:String, eventTypeName: String) {
        let viewController = Route.CreateEventPopup
        viewController.modalPresentationStyle = .overCurrentContext
        viewController.eventID = eventID
        viewController.imageUrl = eventImage
        viewController.eventType = eventTypeName
        viewController.parameter = params
        if selectedImageView.image != nil {
            if videoID.isEmpty{
                viewController.image = selectedImageView.image!
            }
            else{
                viewController.videoPath = self.videoLocalPath
            }
        }
        viewController.forFacebookEvent = self.isFromFbEvent
        viewController.controller = self
        self.present(viewController, animated: true, completion: nil)
    }
}
//MARK: Actions

extension CreateEventsViewController {
    func goBack() {
        if isFromSideMenu {
            Route.goToHomeController()
            return
        }
        if isEditable{
           self.navigationController?.popViewController(animated: true)
        } else {
            if mediaArr.count > 0{
                for media in mediaArr{
                    mediaArrId.append(media.mediaId!)
                }
            }
            if !videoID.isEmpty{
                mediaArrId.append(videoID)
            }
            
            if mediaArrId.count > 0 {
                let param = [
                    "mediaIds" : mediaArrId
                ]
                VMediaWebService.deleteMultipleMedia(param as [String : AnyObject], successBlock: { (msg) in
                    print(msg)
                    }, failureBlock: { (msg) in
                        print(msg)
                })
            }
           Route.goToHomeController()
        }
    }
    
    @IBAction func infoFeatureEvent(_ sender: AnyObject) {
        featuredInfoState = !featuredInfoState
        if featuredInfoState {
            self.hiddenView.isHidden = false
            imgFeaturedInfo.isHidden = false
        } else {
            imgFeaturedInfo.isHidden = true
        }
        
    }
    
    @IBAction func back(_ sender: AnyObject) {
        goBack()
    }
    
    @IBAction func checkIfEventExists(_ sender: AnyObject) {
        let fbEventCount = UserDefaults.standard.integer(forKey: "fbEventListCount")
        if fbEventCount == 0 {
            self.view.makeToast(message: localizedString(forKey: "NO_FB_EVENTS"))
        }
        else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "fbEventVc") as! UINavigationController
            controller.view.backgroundColor = UIColor.clear
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func selectEventType(_ sender: AnyObject) {
        if(eventTypeSegmentedControl.selectedSegmentIndex == 1) {
            self.eventType = .Paid
            btnTogglePromo.isEnabled = true
        } else if(eventTypeSegmentedControl.selectedSegmentIndex == 2) {
            self.eventType = .Private
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: "promoButtonNotification"), object: nil)
        } else {
            self.eventType = .Public
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: "promoButtonNotification"), object: nil)
        }
    }
    
    @IBAction func message(_ sender: AnyObject) {
        
    }
    
    @IBAction func createFeatureEvent(_ sender: AnyObject) {
        state =  !state
        if state {
            showPopUpFeatureEventConfirmView()
        } else {
            self.isFeatureEvent = false
            self.btnFeatureEvent.setImage(UIImage(named: "NoTickRecurringEvent"), for: UIControlState())
            self.btnToggleRecurringEvent.isEnabled = true
        }
        
    }
    
    @IBAction func selectLocation(_ sender: AnyObject) {
        if isEditable {
            getEventLocation(self.eventLocation!)
        } else {
            locationManager?.getUserLocation()
            locationManager?.fetchedUserLocation = {
                location in ()
                self.getEventLocation(location)
            }
        }
    }
    
    
    @IBAction func resetEventDetail(_ sender: AnyObject) {
        if self.isEditable {
            return
        }
        clearEventDetails()
    }
    
    
    @IBAction func create(_ sender: AnyObject) {
        self.view.endEditing(true)
        //removed pop up
        if self.isEditable {
            return
        }
        
        self.startIndicator()
        //Get Date and Time
        self.setDateAndTime()
        let response = self.validateFormValuesAndGetFormValueAsParameter()
        let recurrenceEventResponse = self.validateFormValuesAndGetFormValueAsParameterForRecurringEvent()
        self.params = response.parameter!
        if response.status {
            if self.isRecurringEvent{
                if recurrenceEventResponse.status {
                    self.postRecurringEventWithParameter(recurrenceEventResponse.parameter!)
                } else {
                    if let errorMsg = recurrenceEventResponse.parameter!["msg"] as? String{
                        showAlert(controller: self, title: "", message: errorMsg)
                    }
                    self.stopIndicator()
                }
                
            } else {
                if !isFeatureEvent {
                    self.postEventWithParameter(response.parameter!)
                } else {
                    goToFeatureTicket()
                }
            }
            self.viteNow = false
        } else {
            if let errorMsg = response.parameter!["msg"] as? String{
                showAlert(controller: self, title: "", message: errorMsg)
            }
            self.stopIndicator()
        }
    }
    
    @IBAction func selectImage(_ sender: AnyObject) {
        self.isForMultipleImages = false
        showActionPicker()
    }
    
    @IBAction func selectMultipleImage(_ sender: AnyObject) {
        self.isForMultipleImages = true
        showActionPicker()
    }
    
    func showPopUpFeatureEventConfirmView() {
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "featurePopUp") as! FeatureEvnetPopUpViewController
        controller.delegate = self
        controller.modalPresentationStyle = .overCurrentContext
        self.present(controller, animated: true, completion: nil)
    }
    
    func goToFeatureTicket() {
        let response = self.validateFormValuesAndGetFormValueAsParameter()
        let controller = Route.featurePayment
        if isEditable {
            controller.eventName = self.event?.eventTitle
            controller.isFromEditView = true
        } else {
            controller.eventName = self.tfEventName.text
        }
        controller.parameter = response.parameter
        controller.delegate = self
        controller.evenType  = eventType.rawValue
        self.navigationController?.pushViewController(controller, animated: true)
        self.view.endEditing(false)
        indicator.stop()

    }
    
    func showActionPicker(){
        
        let alert = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.purple
        //alert.view.backgroundColor = UIColor.whiteColor(
        
        let camera = UIAlertAction(title: "Camera",
                                   style: .default,
                                   handler: {
                                    (action:UIAlertAction) -> Void in
                                    self.openImagePicker(.camera)
        })
        
        let gallery = UIAlertAction(title: "Photo Album",
                                    style: .default,
                                    handler: {
                                        (action:UIAlertAction) -> Void in
                                        self.openImagePicker(.photoLibrary, allowEdit: self.isForMultipleImages ? false : true)
                                        
        })
        
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel) {
                                            (action: UIAlertAction) -> Void in
        }
        
        alert.addAction(camera)
        alert.addAction(gallery)
        alert.addAction(cancelAction)
        
        present(alert,
                              animated: false,
                              completion: nil)
    }
    
    
    func openImagePicker(_ sourceType: UIImagePickerControllerSourceType, allowEdit: Bool = false){
        self.indicator.parentView = self.selectedImageView
        self.indicator.start()
        
        self.imageHelper.determineStatus {
            (status) -> () in
            guard status else {
                print("Not authenticated to use photo")
                return
            }
            
            //If app is authorized to use the gallery only then load image controller
            self.imageHelper.getImageController(sourceType, allowEdit: allowEdit)
            self.imageHelper.imagePicked = {
                image in
                
                let ratio = Int(image.size.width/image.size.height);
                let maximumRatioForNonePanorama = 4/3
                if (ratio > maximumRatioForNonePanorama){
                    self.view.makeToast(message: localizedString(forKey: "IMG_NOT_APPROPRIATE_SIZE"))
                    self.selectedImageView.image = nil
                } else {
                    self.btnPlay.isHidden = true
                    self.progressView.isHidden = true
                    self.progressPercentage.isHidden = true
                    self.presentCropViewController(image)
                }
                self.indicator.stop()
                //When the view is editable if the image view's image is changed do not send the image parameter.
                //For that this flag is set.
                if self.isEditable {
                    self.isImageChanged = true
                }
                
            }
            
            self.imageHelper.videoPath = {
                video in
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "trimmerViewController") as! VideoTrimmerViewController
                controller.asset = AVAsset(url: video)
                controller.url = video
                controller.delegate = self
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    
    func presentCropViewController(_ image : UIImage) {
        let cropViewController = CropViewController(image: image)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        if self.isForMultipleImages{
            self.showHideMultiMediaTransView(false)
            self.updateProgressBar()
            self.multimediaProgressView.setProgress(0, animated: true)
            let imgData = self.getCompressedImage(image)
            let imageName = String(UInt64(floor(Date().timeIntervalSince1970 * 1000))) + ".jpeg"
            
            let mediaFormat = "jpeg/png"
            //thumbnail data for the image is not used
            self.uploadVideoOrImage(imgData as Data, thumbnailData: imgData as Data,mediaName: imageName, mediaFormat: mediaFormat,mediaType: "IMAGE",mediaWidth: Int(image.size.width),mediaHeight: Int(image.size.height))
        } else {
            self.imgString = self.getCompressedBase64Image(image)
            if !self.videoID.isEmpty{
                self.mediaArrId.append(self.videoID)
            }
            self.videoID = ""
            self.selectedImageView.sd_setShowActivityIndicatorView(true)
            self.selectedImageView.image = image
            self.selectedImageView.contentMode = UIViewContentMode.scaleAspectFill
        }
        self.dismiss(animated: true, completion: nil)
          }
    
    
    func sendVideoURL(vidUrl: URL) {
        self.uploadVidToServer(video: vidUrl)
    }
    
    func uploadVidToServer(video: URL) {
        let videoData = try? Data(contentsOf: video as URL)
        //file name for video saved
        //video format
        let mediaFormat = "mov/mp4"
        let thumbNailImage = thumbnailForVideoAtURL(video)
        let thumbNailImgData = self.getCompressedImage(thumbNailImage!)
        
        //if video is less than 10 MB , user can upload it
        if videoData?.count < 10000000 {
            if !self.isForMultipleImages{
                self.progressView.isHidden = false
                self.progressPercentage.isHidden = false
                self.progressPercentage.text = "0%"
                self.progressView.progress = 0
                self.progressView.tintColor = UIColor.blue
                self.progressPercentage.textColor = UIColor.blue
                self.videoLocalPath = video.relativePath
            } else {
                self.showHideMultiMediaTransView(false)
                self.updateProgressBar()
            }
            
            let videoFileName = String(UInt64(floor(Date().timeIntervalSince1970 * 1000))) + UserDefaultsManager().userFBID! + ".MOV"
            if let thumbNailheight = thumbNailImage?.size.height , let thumbNailWidth = thumbNailImage?.size.width{
                self.uploadVideoOrImage(videoData!,thumbnailData: thumbNailImgData as Data, mediaName: videoFileName, mediaFormat: mediaFormat,mediaType: "VIDEO", mediaWidth: Int(thumbNailWidth),mediaHeight: Int(thumbNailheight))
            }
        } else {
            showAlert(controller: self, title: localizedString(forKey: "ALERT"), message: localizedString(forKey: "UPLOAD_VIDEO"))
        }
        
        self.indicator.stop()
        
        if self.isEditable {
            // if the image is replaced by video
            self.isImageChanged = false
        }
    }
    
    // MARK: Upload multiple images and videos
    
    //MARK: Multiple Upload Class Methods
    
    // update progress view to initial state
    func updateProgressBar() {
        self.multimediaProgressPercentLbl.text = "0%"
        self.multimediaProgressView.progress = 0
    }
    
    // show hide media loading view
    func showHideMultiMediaTransView(_ state: Bool){
        multipleImageTransparantView.isHidden = state
        multimediaProgressView.isHidden = state
        multimediaProgressPercentLbl.isHidden = state
        uploadView._border.strokeColor = state ? UIColor.lightGray.cgColor : UIColor.white.cgColor
    }
    
    // Show hide logic of multiple media collection view
    func showHideCollectionView(_ heightConstant: CGFloat){
        self.eventImageVideoCVHeight.constant = heightConstant
        self.eventImageVideoCollectionView.reloadData()
        self.view.layoutIfNeeded()
        if self.mediaArr.count != 0 {
            // scroll to the last index of the image
            let lastIndex = IndexPath(item: (self.mediaArr.count) - 1, section: 0)
            self.eventImageVideoCollectionView.scrollToItem(at: lastIndex, at: UICollectionViewScrollPosition.right, animated: true)
        }
    }
    
    func alertUserToDelete(_ index: Int,media: VMedia){
        let mediaName = media.fileType == "IMAGE" ? "image" : "video"
        let alert = UIAlertController(title: "",
                                      message: " Are you sure you want to delete this \(mediaName)? ",
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Yes",
                                     style: .default,
                                     handler: { (action:UIAlertAction) -> Void in
                                        self.removeImageOrVideo(index, media: media)
        })
        
        let cancelAction = UIAlertAction(title: "No",
                                         style: .default) {
                                            (action: UIAlertAction) -> Void in
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert,
                              animated: false,
                              completion: nil)
        
    }
    
    // Delete media
    func removeImageOrVideo(_ index: Int,media: VMedia){
        if let id = media.mediaId{
            // delete media from the server
            VMediaWebService.deleteMedia(id, successBlock: { (msg) in
                self.mediaArr.remove(at: index)
                self.mediaParamArr.remove(at: index)
                if self.mediaArr.count == 0 {
                    self.showHideCollectionView(0)
                }else{
                    self.eventImageVideoCollectionView.reloadData()
                }
                print("Successfully deleted Media ")
                }, failureBlock: { (msg) in
                    showAlert(controller: self, title: "", message: msg)
            })
        }
    }
    
    //MARK: Upload multiple images and videos to server
    
    func uploadVideoOrImage(_ mediaData: Data,thumbnailData: Data?, mediaName: String, mediaFormat: String, mediaType: String,mediaWidth: Int, mediaHeight:Int){
        VMediaWebService.uploadMedia(mediaData, videoThumbnailData: thumbnailData!, mediaName: mediaName, mediaType: mediaType, mediaWidth: mediaWidth, mediaHeight:mediaHeight, mediaFormat: mediaFormat, progressBlock: { (progressResult) in
            progressResult.uploadProgress(closure: { (progress) in
                let percent = Int(100 * progress.fractionCompleted)
                    DispatchQueue.main.async{
                        if self.isForMultipleImages{
                            // show transparant media loading View
                            self.showHideMultiMediaTransView(false)
                            self.multimediaProgressPercentLbl.text = "\(percent)%"
                            self.multimediaProgressView.setProgress(Float(progress.fractionCompleted), animated: true)
                        } else {
                            self.progressPercentage.text = "\(percent)%"
                            self.progressView.setProgress(Float(progress.fractionCompleted), animated: true)
                        }
                    }
            })

            }, successBlock: { (media) in
                if !self.isForMultipleImages{
                    self.progressPercentage.text = "Upload complete"
                    self.progressPercentage.textColor = UIColor.green
                    self.progressView.progressTintColor = UIColor.green
                    if let id = media.mediaId{
                        self.videoID = id
                    }
                    if let url = media.thumbNailUrl{
                        self.btnPlay.isHidden = false
                        self.selectedImageView.sd_setShowActivityIndicatorView(true)
                        self.selectedImageView.sd_setImage(with: NSURL(string: url.convertIntoViteURL())! as URL)
                        self.mainMediaUrl = media.mediaUrl
                        self.mainMediaType = media.fileType
                        self.mainMediaThumbnailUrl = url
                    }

                } else{
                    self.multimediaProgressView.progressTintColor = UIColor.green
                    self.multimediaProgressPercentLbl.text = "Upload complete"
                    self.showHideMultiMediaTransView(true)
                    self.mediaArr.append(media)
                    let mediaData : [String: AnyObject] = [
                        "fileType" : media.fileType! as AnyObject,
                        "mediaId"  : media.mediaId! as AnyObject
                    ]

                    self.mediaParamArr.append(mediaData)
                    self.isForMultipleImages = false
                    self.showHideCollectionView(250)
                }

            }, failureBlock: { (message) in
                self.isForMultipleImages = false
                showAlert(controller: self, title: "", message: message)
        })
    }
    
    // used for edit event case
    func getParamForMultipleFileUpload(_ mediaList: [VMedia]){
        for media in mediaList{
            let mediaData : [String: AnyObject] = [
                "fileType" : media.fileType! as AnyObject,
                "mediaId"  : media.mediaId! as AnyObject
            ]
            self.mediaParamArr.append(mediaData)
        }
    }
    
    //MARK: Multiple Media Collection View delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mediaArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let mediaData = mediaArr[indexPath.row]
        if mediaData.fileType == "IMAGE"{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCVCell", for: indexPath) as! CreateEventImageCollectionViewCell
            if let url = mediaData.mediaUrl{
                cell.eventImage.sd_setShowActivityIndicatorView(true)
                cell.eventImage.sd_setImage(with: URL(string: url.convertIntoViteURL()))
            }
            cell.removeImage = {
                self.alertUserToDelete(indexPath.row, media: mediaData)
            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoCVCell", for: indexPath) as! CreateEventVideoCollectionViewCell
            if let url = mediaData.thumbNailUrl{
                cell.eventVideoThumbNailImg.sd_setShowActivityIndicatorView(true)
                cell.eventVideoThumbNailImg.sd_setImage(with: URL(string: url.convertIntoViteURL()))
            }
            cell.playVideo = {
                cell.btnPlay.isHidden = true
                if let url = mediaData.mediaUrl?.convertIntoViteURL(){
                    playVideo(url, viewController: self)
                }
            }
            cell.removeVideo = {
                self.alertUserToDelete(indexPath.row, media: mediaData)
            }
            return cell
        }
    }
}

//MARK: Recurring Event
extension CreateEventsViewController{
    
    // Mark: Recurring Event Methods
    @IBAction func recurringEventToggle(_ sender: AnyObject) {
        state = !state
        if state {
            if self.dateTimeDisplay.text != "" {
                let dateStringArr = self.dateTimeDisplay.text!.components(separatedBy: " ")
                eventDate = dateStringArr[0]
                txtStartsOn.text = eventDate
            }
            self.isRecurringEvent = true
            self.btnFeatureEvent.isEnabled = false
            btnToggleRecurringEvent.setImage(UIImage(named: "TickRecurringEvent"), for: UIControlState())
            showRecurringEventView(378, alphaValue: 1)
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: "promoButtonNotification"), object: nil)
        } else {
            self.isRecurringEvent = false
            self.btnFeatureEvent.isEnabled = true
            showRecurringEventView(70, alphaValue: 0)
            if self.eventType == .Paid {
                self.btnTogglePromo.isEnabled = true
            }
            btnToggleRecurringEvent.setImage(UIImage(named: "NoTickRecurringEvent"), for: UIControlState())
        }
    }
    
    func showRecurringEventView(_ constant: CGFloat, alphaValue: CGFloat){
        topSpaceForRecurring.constant = constant
        UIView.animate(withDuration: 0.3 , animations: {
            self.view.layoutIfNeeded()
            self.recurringEventFieldView.alpha = alphaValue
        })
    }
    
    
    @IBAction func toggleEndsField(_ sender: AnyObject) {
        btnNever.setImage(UIImage(named: "NoTickRecurringEvent"), for: UIControlState())
        btnAfter.setImage(UIImage(named: "NoTickRecurringEvent"), for: UIControlState())
        btnOn.setImage(UIImage(named: "NoTickRecurringEvent"), for: UIControlState())
        if sender.tag == 0{
            endDateType = "FOREVER"
            txtOccurences.isEnabled = false
            txtEndDate.isEnabled = false
            txtOccurences.text = ""
            txtEndDate.text = ""
            btnNever.setImage(UIImage(named: "TickRecurringEvent"), for: UIControlState())
        }else if sender.tag == 1{
            endDateType = "NUMBER_OF_OCCURRENCE"
            txtOccurences.isEnabled = true
            txtEndDate.isEnabled = false
            txtEndDate.text = ""
            btnAfter.setImage(UIImage(named: "TickRecurringEvent"), for: UIControlState())
        }else {
            endDateType = "END_DATE"
            txtOccurences.isEnabled = false
            txtEndDate.isEnabled = true
            txtOccurences.text = ""
            btnOn.setImage(UIImage(named: "TickRecurringEvent"), for: UIControlState())
        }
    }
    
    // populate end type and end Value
    func populateEndType(_ endType: String , endValue: String) {
        btnNever.setImage(UIImage(named: "NoTickRecurringEvent"), for: UIControlState())
        btnAfter.setImage(UIImage(named: "NoTickRecurringEvent"), for: UIControlState())
        btnOn.setImage(UIImage(named: "NoTickRecurringEvent"), for: UIControlState())
        switch endType {
        case  "FOREVER":
            endDateType = "FOREVER"
            txtOccurences.isEnabled = false
            txtEndDate.isEnabled = false
            txtOccurences.text = ""
            txtEndDate.text = ""
            btnNever.setImage(UIImage(named: "TickRecurringEvent"), for: UIControlState())
        case  "NUMBER_OF_OCCURRENCE":
            endDateType = "NUMBER_OF_OCCURRENCE"
            txtOccurences.isEnabled = true
            txtEndDate.isEnabled = false
            txtEndDate.text = ""
            txtOccurences.text = endValue
            btnAfter.setImage(UIImage(named: "TickRecurringEvent"), for: UIControlState())
        case  "END_DATE":
            endDateType = "END_DATE"
            txtOccurences.isEnabled = false
            txtEndDate.isEnabled = true
            txtOccurences.text = ""
            txtEndDate.text = endValue
            btnOn.setImage(UIImage(named: "TickRecurringEvent"), for: UIControlState())
        default:
            break
        }
    }
    
    @IBAction func selectDays(_ sender: AnyObject) {
        let selectedValue = daysValArr[sender.tag - 1]
        if daysArr.contains(selectedValue) {
            if let index = daysArr.index(of: selectedValue){
                daysArr.remove(at: index)
            }
        }else {
            daysArr.append(selectedValue)
        }
        for btn in repeatToggleBtns {
            let value = btn.tag
            if daysArr.contains(value){
                btn.backgroundColor = UIColor.colorFromRGB(rgbValue: 0x1990DB)
            }else {
                btn.backgroundColor = UIColor.lightGray
            }
        }
    }
    
    
    @IBAction func openDayPicker(_ sender: AnyObject) {
        
        let alert = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.purple
        
        //        let daily = UIAlertAction(title: "Daily",
        //                                  style: .Default,
        //                                  handler: {
        //                                    (action:UIAlertAction) -> Void in
        //                                    self.daysArr.removeAll()
        //                                    self.repeatType = "REPEAT_DAILY"
        //                                    self.txtRepeat.text = action.title
        //                                    self.lblRepeatEvery.text = "days"
        //                                    // show repeat every and start on
        //                                    self.showHideRepeatOnAndStartsOn(50, alphaValue: 1)
        //                                    self.showHideLogicForDailyYearlyAndMonthly(true)
        //        })
        
        //        let weekdayMonToFri = UIAlertAction(title: "Every weekday (Monday to Friday)",
        //                                            style: .Default,
        //                                            handler: {
        //                                                (action:UIAlertAction) -> Void in
        //                                                 self.daysArr = [2,3,4,5,6]
        //                                                 self.repeatType = "REPEAT_WEEKLY"
        //                                                self.txtRepeat.text = action.title
        //                                                // hide repeat every and start on
        //                                                self.showHideRepeatOnAndStartsOn(8, alphaValue: 0)
        //
        //        })
        //
        //        let monWedAndFri = UIAlertAction(title: "Every Monday, Wednesday and Friday",
        //                                         style: .Default,
        //                                         handler: {
        //                                            (action:UIAlertAction) -> Void in
        //                                             self.daysArr = [2,4,6]
        //                                            self.repeatType = "REPEAT_WEEKLY"
        //                                            self.txtRepeat.text = action.title
        //                                            // hide repeat every and start on
        //                                            self.showHideRepeatOnAndStartsOn(8, alphaValue: 0)
        //
        //
        //        })
        //        let tueAndThur = UIAlertAction(title: "Every Tuesday and Thursday",
        //                                       style: .Default,
        //                                       handler: {
        //                                        (action:UIAlertAction) -> Void in
        //                                        self.daysArr = [3,5]
        //                                        self.repeatType = "REPEAT_WEEKLY"
        //                                        self.txtRepeat.text = action.title
        //                                        // hide repeat every and start on
        //                                        self.showHideRepeatOnAndStartsOn(8, alphaValue: 0)
        //                                        self.lblRepeatOn.hidden = true
        //
        //        })
        
        let weekly = UIAlertAction(title: "Weekly",
                                   style: .default,
                                   handler: {
                                    (action:UIAlertAction) -> Void in
                                    self.daysArr.removeAll()
                                    self.repeatType = "REPEAT_WEEKLY"
                                    self.txtRepeat.text = action.title
                                    self.lblRepeatEvery.text = "weeks"
                                    // show repeat every and start on
                                    self.showHideRepeatOnAndStartsOn(86, alphaValue: 1)
                                    self.weekStackView.isHidden = true
                                    self.dayStackView.isHidden = false
                                    self.lblRepeatOn.isHidden = false
                                    
        })
        
        let monthly = UIAlertAction(title: "Monthly",
                                    style: .default,
                                    handler: {
                                        (action:UIAlertAction) -> Void in
                                        self.daysArr.removeAll()
                                        self.repeatType = "REPEAT_MONTHLY"
                                        self.txtRepeat.text = action.title
                                        self.lblRepeatEvery.text = "months"
                                        // show repeat every and start on
                                        self.showHideRepeatOnAndStartsOn(50, alphaValue: 1)
                                        self.showHideLogicForDailyYearlyAndMonthly(true)
                                        
        })
        
        let yearly = UIAlertAction(title: "Yearly",
                                   style: .default,
                                   handler: {
                                    (action:UIAlertAction) -> Void in
                                    self.repeatType = "REPEAT_YEARLY"
                                    self.txtRepeat.text = action.title
                                    self.lblRepeatEvery.text = "years"
                                    // show repeat every and hide start on
                                    self.showHideRepeatOnAndStartsOn(50, alphaValue: 1)
                                    self.showHideLogicForDailyYearlyAndMonthly(true)
                                    
        })
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel) {
                                            (action: UIAlertAction) -> Void in
        }
        
        //  alert.addAction(daily)
        //        alert.addAction(weekdayMonToFri)
        //        alert.addAction(monWedAndFri)
        //        alert.addAction(tueAndThur)
        alert.addAction(weekly)
        alert.addAction(monthly)
        alert.addAction(yearly)
        alert.addAction(cancelAction)
        
        present(alert,
                              animated: false,
                              completion: nil)
    }
    
    
    
    
    func populateRepeatType(_ repeatType: String){
        switch repeatType {
        case  "REPEAT_WEEKLY":
            self.repeatType = "REPEAT_WEEKLY"
            self.txtRepeat.text = "Weekly"
            self.lblRepeatEvery.text = "weeks"
            // show repeat every and start on
            self.showHideRepeatOnAndStartsOn(86, alphaValue: 1)
            self.weekStackView.isHidden = true
            self.dayStackView.isHidden = false
            self.lblRepeatOn.isHidden = false
            
        case  "REPEAT_MONTHLY":
            self.repeatType = "REPEAT_MONTHLY"
            self.txtRepeat.text = "Monthly"
            self.lblRepeatEvery.text = "months"
            // show repeat every and start on
            self.showHideRepeatOnAndStartsOn(50, alphaValue: 1)
            self.showHideLogicForDailyYearlyAndMonthly(true)
            
        case  "REPEAT_YEARLY":
            self.repeatType = "REPEAT_YEARLY"
            self.txtRepeat.text = "Yearly"
            self.lblRepeatEvery.text = "years"
            // show repeat every and hide start on
            self.showHideRepeatOnAndStartsOn(50, alphaValue: 1)
            self.showHideLogicForDailyYearlyAndMonthly(true)
            
        case  "REPEAT_DAILY":
            self.repeatType = "REPEAT_DAILY"
            self.txtRepeat.text = "Daily"
            self.lblRepeatEvery.text = "days"
            // show repeat every and start on
            self.showHideRepeatOnAndStartsOn(50, alphaValue: 1)
            self.showHideLogicForDailyYearlyAndMonthly(true)
        default:
            break
        }
        
    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if self.dateTimeDisplay.text != "" {
            let dateStringArr = self.dateTimeDisplay.text!.components(separatedBy: " ")
            eventDate = dateStringArr[0]
            txtStartsOn.text = eventDate
        }
    }
    
    
    
    
    func showHideRepeatOnAndStartsOn(_ constant: CGFloat, alphaValue: CGFloat){
        topSpaceStartsOnToRepeatField.constant = constant
        UIView.animate(withDuration: 0.3 , animations: {
            self.view.layoutIfNeeded()
            self.repeatEveryView.alpha = alphaValue
        })
    }
    
    // This function is called when user selects repeat type to daily, yearly and monthly
    func showHideLogicForDailyYearlyAndMonthly(_ hiddenStatus: Bool){
        daysArr.removeAll()
        for btn in repeatToggleBtns {
            btn.backgroundColor = UIColor.lightGray
        }
        dayStackView.isHidden = hiddenStatus
        weekStackView.isHidden = hiddenStatus
        lblRepeatOn.isHidden = hiddenStatus
    }
    
    @IBAction func toggleWeek(_ sender: AnyObject) {
        if sender.tag == 0 {
            btnDayOfMonth.backgroundColor = UIColor.colorFromRGB(rgbValue: 0x1990DB)
            btnDayOfWeek.backgroundColor = UIColor.lightGray
        }else {
            btnDayOfMonth.backgroundColor = UIColor.lightGray
            btnDayOfWeek.backgroundColor = UIColor.colorFromRGB(rgbValue: 0x1990DB)
        }
        
    }
    
    
    func validateFormValuesAndGetFormValueAsParameterForRecurringEvent() -> (status:Bool,parameter:[String:AnyObject]?) {
        var errorMsg = [String:AnyObject]()
        
        let eventName = self.tfEventName.text
        if tfEventName.text!.isEmpty{
            errorMsg = [
                "msg": localizedString(forKey: "EVENT_NAME_REQUIRED") as AnyObject
            ]
            return (false, errorMsg)
            
        }
        let priceTicket = self.tfPrice.text
        let maxTicket = self.tfMaxTicketSale.text
        if eventTypeSegmentedControl.selectedSegmentIndex == 1 {
            if tfPrice.text!.isEmpty{
                errorMsg = [
                    "msg": localizedString(forKey: "PRICE_FIELD_REQUIRED") as AnyObject
                ]
                return (false, errorMsg)
            }
            else if tfPrice.text == "0"{
                errorMsg = [
                    "msg": localizedString(forKey: "NO_ZERO_PRICE") as AnyObject
                ]
                return (false, errorMsg)
            }
            
            if tfMaxTicketSale.text!.isEmpty{
                errorMsg = [
                    "msg": localizedString(forKey: "MAX_TICKETS_REQUIRED") as AnyObject
                ]
                return (false, errorMsg)
                
            }   else if tfMaxTicketSale.text == "0"{
                errorMsg = [
                    "msg": localizedString(forKey: "MAX_TICKETS_NO_ZERO") as AnyObject
                ]
                return (false, errorMsg)
            }
        }
        
        if self.dateTimeDisplay.text!.isEmpty{
            errorMsg = [
                "msg": localizedString(forKey: "EVENT_DATE_REQUIRED") as AnyObject
            ]
            return (false, errorMsg)
        }
        
        if (self.txtRepeatEvery.text! == "0" || self.txtRepeatEvery.text!.count == 0) || (endDateType == "NUMBER_OF_OCCURRENCE" && (self.txtOccurences.text?.count == 0 || self.txtOccurences.text! == "0")){
            errorMsg = [
                "msg": localizedString(forKey: "OCCURENCE_NO_ZERO") as AnyObject
            ]
            return (false, errorMsg)
        }
        
        var dateFormatter = DateFormatter()
        self.dateFormatter.dateFormat = "YYYY-MM-dd HH:mm"
        let strDate = self.dateFormatter.date(from: self.dateTimeDisplay.text!)
        
        
        
        
        guard let date = strDate else {
            errorMsg = [
                "msg": localizedString(forKey: "EVENT_DATE_ERROR") as AnyObject
            ]
            return (false, errorMsg)
        }
        
        if endDateType == "END_DATE" && txtEndDate.text?.count != 0 {
            var eventStartDate = convertStringToNSDate(eventDate, format: "YYYY-MM-dd")
            var eventEndDate = convertStringToNSDate(txtEndDate.text!, format: "YYYY-MM-dd")
            
            if eventStartDate! > eventEndDate! {
                errorMsg = [
                    "msg": localizedString(forKey: "START_DATE_ERROR") as AnyObject
                ]
                return (false, errorMsg)
                
            }
        }
        
        
        guard let eventAddress = self.lblLocation.text else{
            errorMsg = [
                "msg": localizedString(forKey: "EVENT_ADDRESS_BLANK") as AnyObject
            ]
            return (false, errorMsg)
        }
        
        guard let eventDetail = self.tVDescription.text else {
            errorMsg = [
                "msg": localizedString(forKey: "DESCRIPTON_BLANK") as AnyObject
            ]
            return (false, errorMsg)
        }
        
        guard let location = self.eventLocation else {
            errorMsg = [
                "msg": localizedString(forKey: "LOCATION_ERROR") as AnyObject
            ]
            return (false, errorMsg)
        }
        
        //    if self.isRecurringEvent &&  self.isPaid{
        //      errorMsg = [
        //        "msg": "Recurring Event cannot be created for paid event."
        //      ]
        //      return (false, errorMsg)
        //    }
        
        
        var imageString = String()
        if isFromFbEvent{
            if !userFbEvent!.imageUrl.isEmpty{
                imageString =  userFbEvent!.imageUrl
            }else{
                imageString = self.imgString
            }
        }else{
            imageString = self.imgString
        }
        
        if !self.isEditable{
            if imageString.isEmpty && self.videoID.isEmpty{
                errorMsg = [
                    "msg":localizedString(forKey: "SELECT_IMAGE") as AnyObject
                ]
                return (false, errorMsg)
            }
        }
        
        
        var localTimeZoneName: String { return (TimeZone.autoupdatingCurrent).identifier }
        
        let coord =
            [
                "givenLocation":eventAddress,
                "longitude":location.longitude,
                "latitude" : location.latitude,
                "street":"",
                "city":"",
                "state":"",
                "country":""
        ] as [String : Any]
        
        
        var business = [String: AnyObject]()
        if UserDefaultsManager().isBusinessAccountActivated{
            business =  [
                "entityId": UserDefaultsManager().userBusinessAccountID! as AnyObject,
            ]
        }
        
        var parameters:[String:AnyObject]!
        
        if self.isEditable {
            parameters = [
                "eventTitle": eventName! as AnyObject,
                "eventDetail": eventDetail as AnyObject,
                "eventDate": self.eventDate as AnyObject,
                "eventStartTime": self.eventTime as AnyObject,
                "paidEvent": self.isPaid as AnyObject,
                "bringAFriend": self.state as AnyObject,
                "viteNow": self.viteNow as AnyObject,
                "location": coord as AnyObject,
                "privateEvent": self.isPrivate as AnyObject,
                "sendPush": true as AnyObject,
                "timeZone": localTimeZoneName as AnyObject,
                "repeatType": repeatType as AnyObject,
                "eventDays": daysArr as AnyObject,
                "repeatOnOccurrence": txtRepeatEvery.text as AnyObject,
                "endType": endDateType as AnyObject,
                "endValue": getEndValue() as AnyObject,
                //check this once
               // "businessId": UserDefaultsManager().isBusinessAccountActivated ? UserDefaultsManager().userBusinessAccountID! : "" as AnyObject,
                "interests" : interest as AnyObject,
                "maxAge": maxAge as AnyObject,
                "minAge": minAge as AnyObject,
                "eventVideoId" : self.videoID as AnyObject,
                "media": self.mediaParamArr as AnyObject
            ]
            if UserDefaultsManager().isBusinessAccountActivated {
                parameters["businessId"] = UserDefaultsManager().userBusinessAccountID! as AnyObject
                
            }
            if isImageChanged {
                parameters["imageUrl"] = imageString as AnyObject
                
            }
            if self.isPaid {
                parameters["maxNumAttendant"] = maxTicket! as AnyObject
                parameters["eventPrice"] = priceTicket! as AnyObject
            }

            
        } else {
            parameters = [
                "eventTitle": eventName! as AnyObject,
                "imageUrl": imageString as AnyObject,
                "eventDetail": eventDetail as AnyObject,
                "eventDate": self.eventDate as AnyObject,
                "eventStartTime": self.eventTime as AnyObject,
                "paidEvent": self.isPaid as AnyObject,
                "bringAFriend": self.state as AnyObject,
                "viteNow": self.viteNow as AnyObject,
                "location": coord as AnyObject,
                "privateEvent": self.isPrivate as AnyObject,
                "sendPush": true as AnyObject,
                "timeZone": localTimeZoneName as AnyObject,
                "repeatType": repeatType as AnyObject,
                "eventDays": daysArr as AnyObject,
                "repeatOnOccurrence": txtRepeatEvery.text as AnyObject ?? "" as AnyObject,
                "endType": endDateType as AnyObject,
                "endValue": getEndValue() as AnyObject,
                //check this once
                //"businessId": User().isBusinessAccountActivated ? User().userBusinessAccountID! : "" as AnyObject,
                "interests" : self.interest as AnyObject,
                "maxAge": maxAge as AnyObject,
                "minAge": minAge as AnyObject,
                "eventVideoId" : self.videoID as AnyObject,
                "media": self.mediaParamArr as AnyObject
            ]
            
            if UserDefaultsManager().isBusinessAccountActivated {
                parameters["businessId"] = UserDefaultsManager().userBusinessAccountID! as AnyObject
                
            }
            
            if self.isPaid {
                parameters["maxNumAttendant"] = maxTicket! as AnyObject
                parameters["eventPrice"] = priceTicket! as AnyObject
                
            }
        }
        // print(parameters)
        return (true, parameters)
    }
    
    
    func postRecurringEventWithParameter(_ parameter:[String:AnyObject]) {
        self.view.isUserInteractionEnabled = false
        btnBack.isEnabled = false
        btnFbEvent.isEnabled = false
        VCreateEventService.createRecurringEvent(parameter, successBlock: {
            (eventID,imageUrl) in
            self.btnBack.isEnabled = true
            self.btnFbEvent.isEnabled = true
            self.view.isUserInteractionEnabled = true
            self.indicator.stop()
            if self.eventType == .Private {
                self.showCreateEventPopup(eventID, eventImage: imageUrl, eventTypeName: "RecurringPrivate")
            } else if self.eventType == .Paid {
                self.showCreateEventPopup(eventID, eventImage: imageUrl, eventTypeName: "RecurringPaid")
            }else {
                //CreatePopSegue
                self.showCreateEventPopup(eventID, eventImage: imageUrl, eventTypeName: "Recurring")
            }
            let eventName = parameter["eventTitle"] as! String
            //PUT Empty string from now need to get imageURL From server
            let eventURL = imageUrl.convertIntoViteURL()
            let event    = NEvent(name: eventName, uid: eventID, imageUrl: eventURL)
            let sender   = UserDefaultsManager.getActiveUserForNotification()
            NotificationService().sendNotificationToUsersInPeripheryOfEventCreated(event, sender: sender)
            if self.isForAddingUsersToInviteList {
                self.inviteAllSwappedUsers(eventID)
            }
            }, failureBlock: {
                message in
                self.btnBack.isEnabled = true
                self.btnFbEvent.isEnabled = true
                self.view.isUserInteractionEnabled = true
                showAlert(controller: self, title: "Error", message: message)
                self.indicator.stop()
        })
    }
    
    
    
    func editRecurringEventServiceCall(_ parameter: [String:AnyObject]){
        guard let recEventId = event?.scheduledEventId else {
            return
        }
        VCreateEventService.editRecurringEvent(recEventId, parameter: parameter, successBlock: { (imageUrl) in
            self.triggerNotificationForEdit(parameter, imageURL: imageUrl.convertIntoViteURL())
            self.isForCheckingEventParam = false
            if let del = self.delegate {
                del.editedEvent((self.event?.entityId)!)
            }
            self.goBack()
            self.showToast(localizedString(forKey: "EVENT_UPDATED"))
            self.btnBack.isEnabled = false
            self.btnFbEvent.isEnabled = false

        }) { (message) in
            showAlert(controller: self, title: "", message: message)
        }
    }
    
    
    func getEndValue() -> String{
        if txtEndDate.text == ""{
            return txtOccurences.text!
        }else if txtOccurences.text == ""{
            return txtEndDate.text!
        }else {
            return ""
        }
    }
    
    func getCurrentDate() -> String{
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "YYYY-MM-dd"
        let result = dateformatter.string(from: Date())
        return result
    }
    
    
    
    func changeFontForSmallScreenDevice(_ font: UIFont){
        lblRepeatOn.font =  font
        lblRepeatEvery.font = font
        lblRepeat.font =  font
        lblStartsOn.font =  font
        lblEnd.font = font
        lblRepeatsEveryText.font = font
    }
    
    
}

//MARK: Age Filter
extension CreateEventsViewController{
    func setDefaultValue(){
        maxAge = Int(ageRangeSlider.upperValue)
        minAge = Int(ageRangeSlider.lowerValue)
        lblMinAge.text = "\(minAge)"
        lblMaxAge.text = "\(maxAge)+"
    }
    
    @objc func rangeSliderValueChanged(_ rangeSlider: RangeSlider) {
        maxAge = Int(rangeSlider.upperValue)
        minAge = Int(rangeSlider.lowerValue)
        lblMinAge.text = "\(minAge)"
        lblMaxAge.text =  maxAge == 60 ? "\(maxAge)+" : "\(maxAge)"
    }
}

//MARK: Interest View Methods
extension CreateEventsViewController{
    
    // MARK: - TagViewDataSource
    func numberOfTags( _ tagView: HTagView) -> Int {
        return tagLists.count
    }
    
    func tagView(_ tagView: HTagView, titleOfTagAtIndex index: Int) -> String {
        return  tagLists[index]
    }
    
    func tagView(_ tagView: HTagView, tagTypeAtIndex index: Int) -> HTagType {
        return .select
    }
    
    // MARK: - TagViewDelegate
    func tagView(_ tagView: HTagView, tagSelectionDidChange selectedIndices: [Int]) {
        interest.removeAll()
        for select in selectedIndices{
            interest.append([ "filterName" : tagLists[select] as AnyObject])
        }
    }
    
    func getTagList(){
        VProfileWebService.getUserInterestList(successBlock: { (interestList) in
            self.tagLists = interestList
            self.interestView.delegate = self
            self.interestView.dataSource = self
            self.populateInterests(self.previousInterests)
        }) { (message) in
            showAlert(controller: self, title: "", message: message)
        }
    }
    
    
    func populateInterests(_ interests: [[String: AnyObject]]){
        for i in interests {
            for tagIndex in 0 ..< tagLists.count {
                if String(describing: i["filterName"]!) == tagLists[tagIndex] {
                    //intrestView.reloadData()
                    interestView.selectTagAtIndex(tagIndex)
                }
            }
        }
    }
}


//MARK: Feature event delegate



extension CreateEventsViewController: FeaturePaymentDelegate{
    func featurePaymentPassParameter(_ parameter: [String : AnyObject], isFromFeaturedView: Bool) {
        self.isFromFeaturedView = isFromFeaturedView
        if isFromFeaturedView {
            editEventServiceCall(parameter)
        } else {
            postEventWithParameter(parameter)
        }
        indicator.stop()
    }
}

//MARK: Feature event popup delergate
extension CreateEventsViewController: FeatureEvnetPopUpViewControllerDelegate{
    
    func checkFeatureEvent(_ featureEvent: Bool) {
        if featureEvent {
            self.isFeatureEvent = true
            self.btnFeatureEvent.setImage(UIImage(named: "TickRecurringEvent"), for: UIControlState())
            self.btnToggleRecurringEvent.isEnabled = false
            if self.isEditable {
                self.isFeatureEvent = true
            }
        } else {
            self.isFeatureEvent = false
            self.btnFeatureEvent.setImage(UIImage(named: "NoTickRecurringEvent"), for: UIControlState())
            self.btnToggleRecurringEvent.isEnabled = true
        }
    }
}

//MARK: PROMO CODE
extension CreateEventsViewController {
    
    @IBAction func selectDiscountType(_ sender: AnyObject) {
        showDiscountTypeSheet()
    }
    
    @IBAction func togglePromoCode(_ sender: AnyObject) {
        promoState = !promoState
        if promoState {
            btnTogglePromo.setImage(UIImage(named: "TickRecurringEvent"), for: UIControlState())
            showPromoView(250, alphaValue: 1)
        } else {
            showPromoView(70, alphaValue: 0)
            btnTogglePromo.setImage(UIImage(named: "NoTickRecurringEvent"), for: UIControlState())
        }
    }
    
    func showPromoView(_ constant: CGFloat, alphaValue: CGFloat){
        topSpacePromo.constant = constant
        UIView.animate(withDuration: 0.3 , animations: {
            self.view.layoutIfNeeded()
            self.promoCodeView.alpha = alphaValue
        })
    }
    
    func showDiscountTypeSheet() {
        let alert = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.purple
        let percent = UIAlertAction(title: "Percentage %",
                                    style: .default,
                                    handler: {
                                        (action:UIAlertAction) -> Void in
                                        self.lblPercent.isHidden = false
                                        self.txtDiscountAmtOrPercent.text = ""
                                        self.discountType = .Percent
                                        self.txtDiscountType.text = "Percentage"
                                        
        })
        
        let dollar = UIAlertAction(title: "Dollar $",
                                   style: .default) {
                                    (action: UIAlertAction) -> Void in
                                    self.lblPercent.isHidden = true
                                    self.txtDiscountAmtOrPercent.text = ""
                                    self.discountType = .Amount
                                    self.txtDiscountType.text = "Dollar"
        }
        
        
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel) {
                                            (action: UIAlertAction) -> Void in
        }
        
        alert.addAction(percent)
        alert.addAction(dollar)
        alert.addAction(cancelAction)
        
        present(alert,
                              animated: false,
                              completion: nil)
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let countdots = textField.text!.components(separatedBy: ".").count - 1
        if (string == " ") || (countdots > 0 && string == ".") {
            return false
        }
        return true
    }
}





