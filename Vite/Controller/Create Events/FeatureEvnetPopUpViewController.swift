//
//  FeatureEvnetPopUpViewController.swift
//  Vite
//
//  Created by Eeposit 01 on 6/21/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

protocol FeatureEvnetPopUpViewControllerDelegate {
  func checkFeatureEvent(_ featureEvent:Bool)
}
class FeatureEvnetPopUpViewController: UIViewController {
  
  var isFeatureEvent = false
  var delegate: FeatureEvnetPopUpViewControllerDelegate?
 
  @IBOutlet weak var popUpView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
      self.popUpViewDesign()
    }
  
  func popUpViewDesign() {
    self.popUpView.layer.cornerRadius = 15
    self.popUpView.clipsToBounds = true
    
  }
  
  @IBAction func btnCancel(_ sender: Any) {
    
    if let del = delegate {
      del.checkFeatureEvent(isFeatureEvent)
   }
    self.dismiss(animated: true, completion: nil)
    
  }

  @IBAction func btnContinue(_ sender: Any) {
    self.isFeatureEvent = true
    if let del = delegate {
      del.checkFeatureEvent(isFeatureEvent)
    }
    self.dismiss(animated: true, completion: nil)
   }

}
