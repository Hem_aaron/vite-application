//
//  InviteFriendsVC.swift
//  Vite
//
//  Created by Hem Poudyal on 1/10/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit
import ContactsUI
import AddressBook
import MessageUI
import Branch
import IQKeyboardManagerSwift

struct CreateEventModel {
    var eventID : String?
    var eventTitle : String?
    var eventDetail : String?
    var imageUrl : String?
}

enum Sections:Int {
    case firstSection = 0
    case secondSection = 1
    case thirdSection = 2
    
}


class InviteFriendsVC: WrapperController,  UITableViewDelegate, UITableViewDataSource, MFMessageComposeViewControllerDelegate,UISearchBarDelegate {

    @IBOutlet weak var searchBar: CustomSearchBar!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableView.delegate = self
            self.tableView.dataSource = self
        }
    }
    var contactArray = [PhoneContact]()
    var originalContactArray = [PhoneContact]()
    var eventVar : CreateEventModel!
    let user = UserDefaultsManager()
    let indicator                        = ActivityIndicator()
    var friendsArr:[MyVIPFriend]         = [MyVIPFriend]()
    var friendsList:[PrivateFriend]       = [PrivateFriend]()
    var originalFriendListPrivateFriendModel:[PrivateFriend] = [PrivateFriend]()
    var viteUserArrayInPrivateFriendModel:[PrivateFriend] = [PrivateFriend]()
    var selectedFriendsArr:[MyVIPFriend] = [MyVIPFriend]()
    var selectedFriendsOnSearchArr:[ViteUser] = [ViteUser]()
    var originalFriendsArr:[MyVIPFriend] = [MyVIPFriend]()
    var vipCategoryArr = [VipGroupCategory]()
    var selectedVIPCategoryArr = [VipGroupCategory]()
    let VIPDataController             = VIPListService()
    var isSearching                      = false
    var searchShouldBegin                = false
    private var _UserArr = [ViteUser]()
    let viteUserDataController = AllViteUsersService()
    var page = Int()
    var globalSearchText = String()
    var noUser = false
    var isFromContact = Bool()

    override func viewDidLoad() {
        super.viewDidLoad()
        setSearchBarTextColor()
        searchBar.delegate = self
        IQKeyboardManager.sharedManager().enable = true
        self.tableView.dataSource = self
        self.tableView.delegate = self
         getContactsList()
        self.searchBar.cursorColor = UIColor.blue
        self.tableView.keyboardDismissMode = .onDrag
        if isFromContact {
            getNonVipFriend()
        } else{
        self.getFriendsListToInvite()
        self.getVIPCategoryList()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
         self.indicator.stop()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    

    
    @IBAction func doneClicked(sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }

    func setSearchBarTextColor(){
        searchBar.tintColor = UIColor.clear
        searchBar.backgroundImage = UIImage()
        for subView in searchBar.subviews  {
            for subsubView in subView.subviews  {
                if let textField = subsubView as? UITextField {
                    textField.backgroundColor = UIColor.colorFromRGB(rgbValue: 0xdddddd)
                }
            }
        }

    }
    
    func getNonVipFriend(){
        self.indicator.start()
        self.VIPDataController.getUserNonVIPList({ (friends, friendListInPrivateFriendModel) in
            self.friendsArr = friends
            self.originalFriendsArr = self.friendsArr
            self.friendsList = friendListInPrivateFriendModel
            self.tableView.reloadData()
            self.indicator.stop()
           }) { (message) in
                self.indicator.stop()
            showAlert(controller: self, title: "", message: message)
        }
    }
    
    func checkIfInvitaionSentonSearch(myfriend:ViteUser) -> Bool {
        for friend in self.selectedFriendsOnSearchArr{
            if myfriend.facebookId == friend.facebookId {
                return true
            }
        }
        return false
    }
    
    func checkIfInvitaionSent(myfriend:MyVIPFriend) -> Bool {
        for friend in self.selectedFriendsArr{
            if String(describing: myfriend.facebookIdInt) == String(describing: friend.facebookIdInt) {
                return true
            }
        }
        return false
    }
    
    func checkIfInvitaionSentToVipCategory(category:VipGroupCategory) -> Bool {
        if self.selectedVIPCategoryArr.contains(where: { $0.entityId == category.entityId }) {
          return true
        } else {
          return false
        }
    }


    @objc func inviteFriends(sender: UIButton){
        let phoneContact      = self.contactArray[sender.tag].number
        if isFromContact {
            if MFMessageComposeViewController.canSendText() {
                let messageComposer = MFMessageComposeViewController()
//                messageComposer.body = "I'd like to invite you to my Event on Vite. Download the app now! https://itunes.apple.com/us/app/vite-exclusive-events/id1087246453?mt=8 "
               
                messageComposer.body = localizedString(forKey: "INVITE_TO_EVENT_SMS")
                
                messageComposer.messageComposeDelegate = self
                messageComposer.recipients = ["\(phoneContact)"]
                self.present(messageComposer, animated: true, completion: nil)
             } else {
                sendMessageAlert()
             }
            } else {
                let params = [ "userID": "\(self.user.userFBID!)",
                               "eventID": "\(self.eventVar!.eventID!)" ]
            Branch.getInstance().getShortURL(withParams: params, andChannel: "SMS", andFeature: "Referral") { (url: String!, NSError) in
                    if MFMessageComposeViewController.canSendText() {
                        let messageComposer = MFMessageComposeViewController()
                        messageComposer.body = String(format: "I'd like to invite you to my Event, \(self.eventVar!.eventTitle!). Use this link to get invitation: %@", url)
                        messageComposer.messageComposeDelegate = self
                        messageComposer.recipients = ["\(phoneContact)"]
                        self.present(messageComposer, animated: true, completion: nil)
                    } else {
                          self.sendMessageAlert()
                    }
                }
         }
    }
    
    
    
    func sendMessageAlert() {
        let alert = UIAlertController(title: "Error", message: localizedString(forKey: "SMS_NOT_ALLOWED"), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getFriendsListToInvite(){
        self.indicator.start()
        VinviteFriendWebService.getUserFriendsToInviteOnEvent(eventVar.eventID!, successBlock: { (friends) in
            self.friendsArr = friends
            self.originalFriendsArr =  self.friendsArr
            self.tableView.reloadData()
            self.indicator.stop()

            }) { (message) in
                self.indicator.stop()
                showAlert(controller: self, title: localizedString(forKey: "NETWORK_ERROR"), message: message)
        }
    }
    
    func getVIPCategoryList(){
        VipGroupService.listVipCategory({ (list) in
            self.vipCategoryArr = list
             self.tableView.reloadData()
            }) { (message) in
                showAlert(controller: self, title: "", message: message)
        }
    }

    @objc func inviteToEvent(sender: UIButton){
        if isSearching{
            if isFromContact {
                
                let friends =  self.viteUserArrayInPrivateFriendModel[Int(sender.tag)]
                let friens  = self._UserArr[Int(sender.tag)]
                let sav = SwappedPeopleService.getPeopleList()

                var idPresent = false
                for savList in sav {
                    if savList.facebookId == friends.facebookId {
                      idPresent = true
                    }
                 }
                if idPresent == false {
                    SwappedPeopleService.addPeople(people: friends)
                    self.selectedFriendsOnSearchArr.append(friens)
                } else {
                    showAlert(controller: self, title: localizedString(forKey: "ALERT"), message: localizedString(forKey: "USER_ALREADY_ADDED"))
                }
                self.tableView.reloadData()
            
            } else {
            
           let friends =  self._UserArr[Int(sender.tag)]
            guard let eventId = eventVar?.eventID! else {
                return
            }
                EventServices.respondToRequest(status: true,
                                               userFbID: String(describing: friends.facebookId!),
                                                eventID: eventId,
                                                completionBlock: {
                                                    (status) -> () in
                                                    if status {
                                                        self.indicator.stop()
                                                        self.selectedFriendsOnSearchArr.append(friends)
                                                        self.tableView.reloadData()
                                                    } else {
                                                        self.indicator.stop()
                                                        print("Failed to respond to request")
                                                    }
            })
        }
 
        }
        else{
            if isFromContact {
                
                var idPresent = false
                let friends =  self.friendsList[Int(sender.tag)]
                let friens  = self.friendsArr[Int(sender.tag)]
                let sav = SwappedPeopleService.getPeopleList()
                for savList in sav {
                    if savList.facebookId == friends.facebookId {
                        idPresent = true
                    }
                }
                if idPresent == false {
                    SwappedPeopleService.addPeople(people: friends)
                    self.selectedFriendsArr.append(friens)
                    
                } else {
                    showAlert(controller: self, title: localizedString(forKey: "ALERT"), message: localizedString(forKey: "USER_ALREADY_ADDED"))
                }

              self.tableView.reloadData()
            } else {
            
            let friends =  self.friendsArr[Int(sender.tag)]
            guard let friendsFbId = friends.facebookIdInt else {return}
            guard let eventId = eventVar?.eventID! else {
                return
            }
                EventServices.respondToRequest(status: true,
                                                userFbID: String(describing: friendsFbId),
                                                eventID: eventId,
                                                completionBlock: {
                                                    (status) -> () in
                                                    if status {
                                                        self.indicator.stop()
                                                        self.selectedFriendsArr.append(friends)
                                                        self.tableView.reloadData()
                                                    } else {
                                                        self.indicator.stop()
                                                        print("Failed to respond to request")
                                                    }
            })
        }

        }
    }

    func inviteVIPCategoryToEvent(category: VipGroupCategory){
        guard let eventId = eventVar?.eventID!, let categoryID = category.entityId  else {
            return
        }
        if category.friends?.count == 0 {
            showAlert(controller: self, title: "", message: localizedString(forKey: "NO_USERS_IN_CATEGORY"))
        }else {
            self.indicator.start()
            VinviteFriendWebService.inviteVIPsCategory(eventId, categoryId: categoryID, successBlock: { (message) in
                self.indicator.stop()
                self.view.makeToast(message: "Invitation to VIP Category \(category.categoryName!) has been sent successfully")
                self.selectedVIPCategoryArr.append(category)
                self.tableView.reloadData()
            }) { (message) in
                self.indicator.stop()
                showAlert(controller: self, title: "", message: message)
            }
        }
    }
    
    //MARK: - Methods
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result.rawValue) {
        case MessageComposeResult.cancelled.rawValue:
            print("Message was cancelled")
            self.dismiss(animated: true, completion: nil)
        case MessageComposeResult.failed.rawValue:
            print("Message failed")
            self.dismiss(animated: true, completion: nil)
        case MessageComposeResult.sent.rawValue:
            print("Message was sent")
            self.dismiss(animated: true, completion: nil)
        default:
            break;
        }
    }
    
    
    
    // MARK: - Table view data sourc
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case Sections.firstSection.rawValue:
            if self.isSearching{
                return ""
            }
            else{
            return "Friend List"
            }
        case Sections.secondSection.rawValue:
            return "VIP Category"
        case Sections.thirdSection.rawValue:
             return "Phone Number"
        default:
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.firstSection.rawValue:
            if self.isSearching {
                return self._UserArr.count
            }
            return friendsArr.count
        case Sections.secondSection.rawValue:
             return self.vipCategoryArr.count
        case Sections.thirdSection.rawValue:
            return self.contactArray.count
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == Sections.secondSection.rawValue{
            return 118
        }
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case Sections.firstSection.rawValue:
            if self.isSearching {
                return 0
            }
            return friendsArr.count == 0 ? 0 : 40
        case Sections.secondSection.rawValue:
            return vipCategoryArr.count == 0 ? 0 : 40
        case Sections.thirdSection.rawValue:
              return contactArray.count == 0 ? 0 : 40
        default:
            return 20
        }
    }
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //User can invite their friends to Vite
        switch indexPath.section {
        case Sections.firstSection.rawValue:
            let userCell = tableView.dequeueReusableCell(withIdentifier: "FBFriendCell") as! FBFriendCell
            if self.isSearching{
                if self._UserArr.count>0 {
                    let friends =  self._UserArr[indexPath.row]
                    userCell.lblFullName.text = friends.fullName
                    let profileUrl  = friends.profileImgUrl?.convertIntoViteURL()
                    if isValidImage(url: profileUrl!){
                        userCell.profileImg.sd_setImage(with: URL(string:profileUrl!))
                    } else {
                        let profileUrl = "http://graph.facebook.com/\(String(describing: friends.facebookId))/picture?type=large"
                        userCell.profileImg.sd_setImage(with: URL(string: profileUrl), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
                    }
                    if checkIfInvitaionSentonSearch(myfriend: friends){
                        userCell.btnInvite.isEnabled = false
                        userCell.btnInvite.setImage(UIImage(named:"InvitedToVIP"), for: UIControlState.disabled)
                    }else{
                        userCell.btnInvite.isEnabled = true
                        userCell.btnInvite.setImage(UIImage(named:"AddToVIP"), for: .normal)
                        userCell.btnInvite.addTarget(self, action: #selector(self.inviteToEvent(sender:)), for: .touchUpInside)
                    }
                    userCell.btnInvite.tag = indexPath.row
                }
            }
            else{
                let friends = self.friendsArr[indexPath.row]
                 userCell.lblFullName.text = friends.fullName
                if let imageProfile = friends.profileImageUrl?.convertIntoViteURL(), let fbId = friends.facebookIdInt {
                    if isValidImage(url: imageProfile){
                        userCell.profileImg.sd_setImage(with: URL(string: imageProfile.convertIntoViteURL()))
                    } else {
                        let profileUrl = "http://graph.facebook.com/\(fbId)/picture?type=large"
                        userCell.profileImg.sd_setImage(with: URL(string: profileUrl), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
                    }
                }
                if checkIfInvitaionSent(myfriend: friends){
                    userCell.btnInvite.isEnabled = false
                    userCell.btnInvite.setImage(UIImage(named:"InvitedToVIP"), for: UIControlState.disabled)
                }else{
                    userCell.btnInvite.isEnabled = true
                    userCell.btnInvite.setImage(UIImage(named:"AddToVIP"), for: .normal)
                    userCell.btnInvite.addTarget(self, action: #selector(self.inviteToEvent(sender:)), for: .touchUpInside)
                }
                userCell.btnInvite.tag = indexPath.row
            }
            return userCell

        case Sections.secondSection.rawValue:
            let category      = self.vipCategoryArr[indexPath.row]
            let cell              = tableView.dequeueReusableCell(withIdentifier: "VipCategoryCell") as! VipCategoryInviteCell
            cell.titleName.text   = category.categoryName
            if let categoryFriendList  = category.friends {
                cell.groupCollectionDataSource = VipGroupInviteCollectionViewDatasource(collectionView: cell.collectionView, dataSource: categoryFriendList)
            }
            if checkIfInvitaionSentToVipCategory(category: category){
                cell.btnInvite.isEnabled = false
                cell.btnInvite.setImage(UIImage(named:"InvitedToVIP"), for: UIControlState.disabled)
            }else{
                cell.btnInvite.isEnabled = true
                cell.btnInvite.setImage(UIImage(named:"AddToVIP"), for: UIControlState.normal)
            }
            cell.invite = {
                self.inviteVIPCategoryToEvent(category: category)
            }
            return cell
            
        case Sections.thirdSection.rawValue:
            let phoneContact      = self.contactArray[indexPath.row]
            let cell              = tableView.dequeueReusableCell(withIdentifier: "InviteFriendsCell") as! InviteFriendsCell
            cell.fullName.text    = phoneContact.name
            cell.lblPhoneNum.text = phoneContact.number
            cell.btnInvite.tag    = indexPath.row
            cell.btnInvite.addTarget(self, action: #selector(self.inviteFriends), for: .touchUpInside)
            return cell
            
        default:
            let cell  = tableView.dequeueReusableCell(withIdentifier: "InviteFriendsCell") as! InviteFriendsCell
            return cell
        }
    }
    
    
    //MARK: Search VIPs
    
    func searchViteUsersToInvite(searchText:String){
        if !isFromContact {
        page = 1
        self.noUser = true
            let escapedString = searchText.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            let trimming = searchText.trimmingCharacters(in: .whitespaces)
            
        globalSearchText  = escapedString!
        self.indicator.start()
            if trimming.count > 0 {
                VinviteFriendWebService.searchViteUsersToInvite(eventVar.eventID!, page: page, searchString: escapedString!, successBlock: { (viteusers) in
            if viteusers.count < 10 {
                self.noUser = true
            }else{
                self.noUser = false
            }
            self._UserArr = viteusers
            self.tableView.reloadData()
            self.indicator.stop()

            }) { (message) in
                self.indicator.stop()
                showAlert(controller: self, title: "", message: message)
        }
            } else {
             self.indicator.stop()
        }
        } else {
            page = 1
            self.noUser = true
            let escapedString = searchText.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            let trimming = searchText.trimmingCharacters(in: .whitespaces)
           
            globalSearchText  = escapedString!
            self.indicator.start()
            if trimming.count > 0 {
                viteUserDataController.getViteUsers(page, searchString: escapedString!, successBlock: { (viteusers,friendListInPrivateFriendModel) in
                if viteusers.count < 10 {
                                        self.noUser = true
                                    }else{
                                        self.noUser = false
                                    }
                                    self._UserArr = viteusers
                                    self.viteUserArrayInPrivateFriendModel = friendListInPrivateFriendModel
                                    self.tableView.reloadData()
                                    self.indicator.stop()

                }, failureBlock: { (message) in
                    self.indicator.stop()
                    showAlert(controller: self, title: "", message: message)
            })
         
         } else {
            self.indicator.stop()
        }
    }
    }
    
    
    //MARK:- Search Bar Delegates
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        self.searchShouldBegin = false

        let predicate = NSPredicate(format: "self contains %@",searchText)
        
        let filteredContactFriendsArray = originalContactArray.filter {
            predicate.evaluate(with: $0.name)
        }
        
        let filteredFriendsArray = originalFriendsArr.filter {
            predicate.evaluate(with: $0.fullName)
        }

        self.isSearching = true
        
        if searchText.isEmpty {
            //lblMessage.hidden = true
            self.friendsArr = originalFriendsArr
            self.contactArray = self.originalContactArray
            self.isSearching = false
            self._UserArr.removeAll()
            self.tableView.reloadData()
            self.indicator.stop()
        } else {
            self.friendsArr = filteredFriendsArray
            self.contactArray = filteredContactFriendsArray
            self.searchViteUsersToInvite(searchText: searchText)
        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool{
        self.searchShouldBegin = true
        self.tableView.reloadData()
        return true
    }
    
    func  searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
           self.searchBar.resignFirstResponder()
    }
    
    

    
    //MARK:- CONTACT List
    @available(iOS 9.0, *)
    func getContactsList(){
        var contactList =  [PhoneContact]()
        self.contactArray.removeAll()
        let contactStore = CNContactStore()
        do {
            let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactEmailAddressesKey, CNContactPhoneNumbersKey]
            try contactStore.enumerateContacts(with: CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])) {
                (contact, cursor) -> Void in
             
                if contact.isKeyAvailable(CNContactPhoneNumbersKey){
                    
                    for phoneNumber:CNLabeledValue in contact.phoneNumbers {
                        let num = phoneNumber.value
                        let fullName = contact.givenName + " " + contact.familyName
                        let number = num.stringValue
                        let contact = PhoneContact(name: fullName, number: number)
                        contactList.append(contact)
                        
                    }
                    self.contactArray = contactList.sorted(by: {$0.name <  $1.name})
                     self.originalContactArray = self.contactArray
                } else {
                    print("No phone numbers are available")
                }
                
            }
            self.tableView.reloadData()
        }
        catch{
            print("Error getting contacts")
        }
        
    }

}
