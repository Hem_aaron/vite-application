//
//  PaypalEmailViewController.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 4/25/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit
import Braintree
import BraintreeDropIn

class PaypalEmailViewController: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    var braintreeClient: BTAPIClient?
    var indicator = ActivityIndicator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    

    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Confirm(_ sender: Any) {
        postPaypalEmailToServer()
    }
    
    func postPaypalEmailToServer() {
        self.indicator.start()
        self.view.isUserInteractionEnabled = false
        let trimmedEmail = trimCharacter(comment: txtEmail.text!)
        if !isValidEmail(trimmedEmail) || trimmedEmail.isEmpty {
              self.view.isUserInteractionEnabled = true
            self.indicator.stop()
            showAlert(controller: self, title: localizedString(forKey: "INVALID_EMAIL"), message:localizedString(forKey: "ENTER_VALID_EMAIL"))
        } else {
            let parameter = [
                "email" : trimCharacter(comment: txtEmail.text!) as AnyObject
            ]
        PaypalWebService.addPaypalEmail(param: parameter, successBlock: { (msg) in
              self.view.isUserInteractionEnabled = true
               self.indicator.stop()
            self.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PayPalEmailAdded"), object: nil)
            })
        }) { (msg) in
             self.view.isUserInteractionEnabled = true
               self.indicator.stop()
            showAlert(controller: self, title: "Error", message: msg)
        }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

