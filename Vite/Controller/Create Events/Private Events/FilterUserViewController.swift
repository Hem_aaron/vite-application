//
//  FilterUserViewController.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 3/19/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit
import HTagView

protocol FilterUserDelegate {
    func getFilteredUserList(_ filteredDataArr: [PrivateFriend])
}


class FilterUserViewController: UIViewController, HTagViewDelegate, HTagViewDataSource {
    
    @IBOutlet weak var lblAll: UILabel!
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var lblFemale: UILabel!
    @IBOutlet weak var lblMinAge: UILabel!
    @IBOutlet weak var lblMaxAge: UILabel!
    @IBOutlet weak var intrestView: HTagView!
    @IBOutlet weak var ageRangeSlider: RangeSlider!
    @IBOutlet var btnGender: [UIButton]!
    var tagLists = [String]()
    let indicator = ActivityIndicator()
    var userFilterDataArr = [PrivateFriend]()
    var filteredUsersArrayCount = Int()
    var delegate: FilterUserDelegate?
    var eventID = String()
    var gender = String()
    var interest = [[String: AnyObject]]()
    var maxAge = Int()
    var minAge = Int()
    var isForPeopleView = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        intrestView.layer.cornerRadius = 10
        self.title = "Filters"
        getTagList()
        if DeviceType.IS_IPHONE_5 {
            changeFontForSmallScreenDevice(UIFont(name: "Avenir Medium", size: 14)!)
        }
        // set default value at first
        setDefaultValue()
        ageRangeSlider.addTarget(self, action: #selector(FilterUserViewController.rangeSliderValueChanged(_:)), for: .valueChanged)
        // false parameter is to clear  navigation background color
        setupNav(false)
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        setupNav(true)
    }
    
    // MARK: Private Methods
    func changeFontForSmallScreenDevice(_ font: UIFont){
        lblAll.font =  font
        lblMale.font = font
        lblFemale.font =  font
    }
    
    func setDefaultValue(){
        gender = "All"
        maxAge = Int(ageRangeSlider.upperValue)
        minAge = Int(ageRangeSlider.lowerValue)
        lblMinAge.text = "\(minAge)"
        lblMaxAge.text = "\(maxAge)+"
    }
    
    // Navigation setup
    func setupNav(_ clearNavColor: Bool) {
        if let nav = self.navigationController {
            nav.navigationBar.setBackgroundImage(clearNavColor ? UIImage() : UIImage(named:"purpleNav"), for: UIBarMetrics.default)
            nav.navigationBar.shadowImage                  = UIImage()
            if clearNavColor{
                nav.view.backgroundColor                       = UIColor.clear
            }
            self.navigationItem.hidesBackButton            = true
            nav.isNavigationBarHidden                        = false
            let navBar                                     = self.navigationController!.navigationBar
            
            navBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white , NSAttributedStringKey.font: UIFont(name: kFontName, size: kNavigationTitleFontSize)!]
            self.automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    @objc func rangeSliderValueChanged(_ rangeSlider: RangeSlider) {
        maxAge = Int(rangeSlider.upperValue)
        minAge = Int(rangeSlider.lowerValue)
        lblMinAge.text = "\(minAge)"
        lblMaxAge.text =  maxAge == 60 ? "\(maxAge)+" : "\(maxAge)"
    }
    
    
    // MARK: Service Call
    
    // get list of the interest tag from the server
    
    func getTagList(){
        VProfileWebService.getUserInterestList(successBlock: { (interestList) in
            self.tagLists = interestList
            self.intrestView.delegate = self
            self.intrestView.dataSource = self
        }) { (message) in
            showAlert(controller: self, title: "", message: message)
        }
    }
    
    // filter user
    func serviceCallfilterUser() {
        self.indicator.start()
        let parameter :[String: AnyObject] = [
            "entityId": eventID as AnyObject,
            "gender":  gender as AnyObject,
            "interests": interest as AnyObject,
            "maxAge": maxAge as AnyObject,
            "minAge": minAge as AnyObject
        ]
        VFilterWebService.filterUser(parameter, successBlock: { (userFilterList) in
            self.userFilterDataArr = userFilterList
            self.indicator.stop()
            self.delegate?.getFilteredUserList( self.userFilterDataArr)
            self.dismiss(animated: true, completion: nil)
        }) { (message) in
            self.indicator.stop()
            showAlert(controller: self, title: "", message: message)
        }
    }
    
    func serviceCallfilterPeopleInMasterView() {
        self.indicator.start()
        let parameter :[String: AnyObject] = [
            "gender":  gender as AnyObject,
            "interests": interest as AnyObject,
            "maxAge": maxAge as AnyObject,
            "minAge": minAge as AnyObject
        ]
        
        VFilterWebService.filterPeople(parameter, successBlock: { (peopleFilterList) in
            self.userFilterDataArr = peopleFilterList
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: "pushFilterPeople"), object: self.userFilterDataArr)
            UserDefaults.standard.set(true, forKey: "filterBool")
            self.dismiss(animated: true, completion: nil)
            self.indicator.stop()
        }) { (message) in
            self.indicator.stop()
            showAlert(controller: self, title: "", message: message)
        }
    }

    
    
    // MARK: - TagViewDataSource
    func numberOfTags( _ tagView: HTagView) -> Int {
        return tagLists.count
    }
    
    func tagView(_ tagView: HTagView, titleOfTagAtIndex index: Int) -> String {
        return  tagLists[index]
    }
    
    func tagView(_ tagView: HTagView, tagTypeAtIndex index: Int) -> HTagType {
        return .select
    }
    
    // MARK: - TagViewDelegate
    func tagView(_ tagView: HTagView, tagSelectionDidChange selectedIndices: [Int]) {
        interest.removeAll()
        for select in selectedIndices{
            interest.append([ "filterName" : tagLists[select] as AnyObject])
        }
    }
}

//MARK: IBActions

extension FilterUserViewController{
    
    @IBAction func back(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
   
    }
    
    @IBAction func applyFilters(_ sender: AnyObject) {
        if !isForPeopleView{
            serviceCallfilterUser()
        }else{
            serviceCallfilterPeopleInMasterView()
        }
       
    }
    
    @IBAction func selectGender(_ sender: AnyObject) {
        for btn in btnGender{
            btn.setImage(UIImage(named: "UntickAge"), for: UIControlState())
        }
        switch sender.tag{
        case 0 :
            gender = "All"
            btnGender[sender.tag].setImage(UIImage(named: "TickAge"), for: UIControlState())
        case 1 :
            gender = "Male"
            btnGender[sender.tag].setImage(UIImage(named: "TickAge"), for: UIControlState())
        case 2 :
            gender = "Female"
            btnGender[sender.tag].setImage(UIImage(named: "TickAge"), for: UIControlState())
        default:
            break
        }
    }
    
    
}

