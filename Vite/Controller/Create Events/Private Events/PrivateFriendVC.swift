 //
 //  PrivateFriendVC.swift
 //  Vite
 //
 //  Created by eepostIT5 on 6/7/16.
 //  Copyright © 2016 EeposIT_X. All rights reserved.
 //

 import UIKit
 import Koloda
 import SDWebImage

 enum OrganizerViteResponse:String {
    case Requested = "INVITED"
    case Rejected  = "ORGANIZER_REJECTED"
 }

class PrivateFriendVC: WrapperController, FilterUserDelegate {
    
        @IBOutlet weak var kolodaView: KolodaView!
        @IBOutlet weak var noUserLbl: UILabel!
        @IBOutlet weak var btnFilterUser: UIBarButtonItem!
        let indicator = ActivityIndicator()
        var userDataArr:[PrivateFriend]? = [PrivateFriend]()
        var userFbID = String()
        var eventID = String()
        var isFromMyEvent = Bool()
        var event:EventsOrganizer!
    
        override func viewDidLoad() {
            super.viewDidLoad()
            kolodaView.dataSource = self
            kolodaView.delegate = self
            getViteUsers()
            // Do any additional setup after loading the view.
        }
    
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    
         // MARK: Private Method
        func goToHomeViewController(){
            self.dismiss(animated: true, completion: nil)
        }
    
        func showNoUserMsg(){
            self.noUserLbl.text = localizedString(forKey: "NO_ONE_AROUND_YOU")
            self.noUserLbl.isHidden = false
        }
    
          // MARK: Service Call
        func getViteUsers() {
            self.indicator.start()
            VPrivateWebService.getPrivateViteUsers(eventID, { (list) in
                self.userDataArr = list
                if let userList  = self.userDataArr{
                    if userList.count == 0 {
                        self.showNoUserMsg()
                        self.btnFilterUser.isEnabled = false
                    } else {
                        self.btnFilterUser.isEnabled = true
                        self.kolodaView.reloadData()
                    }
                } else {
                    self.showNoUserMsg()
                    self.btnFilterUser.isEnabled = false
                }
                  self.indicator.stop()
            }) { (message) in
                self.indicator.stop()
                showAlert(controller: self, title: "", message: message)
            }
        }
    
    
    
        func serviceCallRespondToRequest(_ eventID : String, fbId : String, userResponse: OrganizerViteResponse){
    
            let parameters:[String:AnyObject] = [
                "viteStatus":userResponse.rawValue as AnyObject,
                ]
            var url:String = ""
    
            if !self.isFromMyEvent {
                url = kInviteForPrivateEvent + "\(eventID)" + "/" + "\(fbId)"
            }
            else{
                url = kRespondToRequestFromUser + "\(fbId)" + "/" + "\(eventID)"
            }
    
            WebService.request(method: .post, url: url, parameter: parameters, header: nil, isAccessTokenRequired: true, success: { (response) in
                guard let response = response.result.value as? [String:AnyObject] else {
                    print("Network Error!")
                    return
                }
                guard let success = response["success"] as? Bool else {
                    return
                }
                if success {
                    
                }
            }) { (msg, err) in
                showAlert(controller: self, title: "", message: msg)
            }
        }
    
        // MARK: Filter User delegate
        func getFilteredUserList(_ filteredDataArr: [PrivateFriend]) {
            if filteredDataArr.count == 0 {
                showNoUserMsg()
                self.kolodaView.clear()
            }else {
                self.noUserLbl.isHidden = true
                self.userDataArr = filteredDataArr
                self.kolodaView.reloadData()
            }
        }
     }
     // MARK: IBActions
    
     extension PrivateFriendVC{
    
        @IBAction func btnBackAction(_ sender: AnyObject) {
            self.dismiss(animated: true, completion: nil)
        }
    
        @IBAction func showFilterView(_ sender: AnyObject) {
            let controller = Route.filterUser
            controller.eventID = self.eventID
            controller.delegate = self
           self.present(controller, animated: true, completion: nil)
        }
    
    
    
     }
    
     //MARK: -  Koloda Delegates
    
     extension PrivateFriendVC: KolodaViewDelegate{
    
       func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
            let userResponse:OrganizerViteResponse = direction == SwipeResultDirection.right ? .Requested : .Rejected
            let userData = self.userDataArr?[Int(index)]
            if userData != nil{
                if let userFbID = userData!.facebookId{
                    self.serviceCallRespondToRequest(eventID, fbId: userFbID, userResponse: userResponse )
                }
            }
        }
    
       func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
              self.dismiss(animated: true, completion: nil)
        }
    
         func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
             //do something on tap of the card
            let storyboard = UIStoryboard(name: "Profile", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MyProfileVc") as! MyProfileViewController
            let user = self.userDataArr?[Int(index)]
            if user != nil{
                if let id = user!.facebookId {
                    controller.fbId = String(id)
                    controller.isForViewingOthersProfile = true
                    self.navigationController?.show(controller, sender: nil)
                }
            }
        }
    
     }
    
     //MARK: -  Koloda Datasource
    
     extension PrivateFriendVC: KolodaViewDataSource{
        func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
            let swipe = SwipeAttendee()
            let userData = self.userDataArr?[Int(index)]
            if userData != nil{
                let fbId = userData!.facebookId
                userFbID = fbId!
                swipe.lblName.text = userData!.fullName
                swipe.lblDescription.text = userData!.info
                if let image = userData!.profileImage{
                    let url = image.convertIntoViteURL()
                    swipe.imgProfile.sd_setImage(with: URL(string:url))
                }
            }
            swipe.layer.cornerRadius = 10.0
            swipe.clipsToBounds = true
            return swipe
        }
    
         func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
            return DragSpeed.moderate
        }
    
    
         func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
    
            if let arr = userDataArr {
                return arr.count
            }
            return 1
        }
    
    
      func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
            //return nil
             return Bundle.main.loadNibNamed("OverlayView", owner: self, options: nil)![0] as?OverlayView
        }
    
}



