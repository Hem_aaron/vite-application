//
//  FBFriendCell.swift
//  Vite
//
//  Created by Ujjwal Shrestha on 1/17/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class FBFriendCell: UITableViewCell {

    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var btnInvite: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        profileImg.layer.cornerRadius = 24.0
        profileImg.clipsToBounds = true
    }

}
