//
//  InviteFriendsCell.swift
//  Vite
//
//  Created by Hem Poudyal on 1/11/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class InviteFriendsCell: UITableViewCell {
    
    
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var lblPhoneNum: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
