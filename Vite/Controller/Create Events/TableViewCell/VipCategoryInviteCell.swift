//
//  VipCategoryInviteCell.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 5/15/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class VipCategoryInviteCell: UITableViewCell {
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    var groupCollectionDataSource :VipGroupInviteCollectionViewDatasource!
    
    @IBOutlet weak var btnInvite: UIButton!
    
    var invite: (() -> ())?
    
    
    @IBAction func inviteVIPCategory(_ sender: AnyObject) {
        if let inv = invite{
            inv()
        }
    }
    
}
