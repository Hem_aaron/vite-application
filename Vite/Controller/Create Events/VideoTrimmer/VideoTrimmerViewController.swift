//
//  ViewController.swift
//  PryntTrimmerView
//
//  Created by Henry on 27/03/2017.
//  Copyright © 2017 Prynt. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices
import PryntTrimmerView
import AVKit

/// A view controller to demonstrate the trimming of a video. Make sure the scene is selected as the initial
// view controller in the storyboard


protocol CreateEventVideoTrimDelegate{
    func sendVideoURL(vidUrl: URL)
}


class VideoTrimmerViewController: UIViewController {

    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var trimmerView: TrimmerView!
    var asset: AVAsset!
    var url: URL!
    var delegate: CreateEventVideoTrimDelegate?

    var player: AVPlayer?
    var playbackTimeCheckerTimer: Timer?
    var trimmerPositionChangedTimer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()
        trimmerView.handleColor = UIColor.white
        trimmerView.mainColor = UIColor.darkGray
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.async {
            if let asset = self.asset {
                self.loadVideoToTrim(asset)
            }
        }
        
    }

    @IBAction func play(_ sender: Any) {
        guard let player = player else { return }
        if !player.isPlaying {
            player.play()
            startPlaybackTimeChecker()
            
        } else {
            player.pause()
            stopPlaybackTimeChecker()
        }
    }
    
    @IBAction func back(_ sender: Any) {
       guard let player = player else { return }
        player.pause()
       self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func next(_ sender: Any) {
        guard let player = player else { return }
        player.pause()
        self.delegate?.sendVideoURL(vidUrl: url)
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadVideoToTrim(_ asset: AVAsset) {
        trimmerView.asset = asset
        trimmerView.delegate = self
        addVideoPlayer(with: asset, playerView: playerView)
    }

    private func addVideoPlayer(with asset: AVAsset, playerView: UIView) {
        let playerItem = AVPlayerItem(asset: asset)
        player = AVPlayer(playerItem: playerItem)

        NotificationCenter.default.addObserver(self, selector: #selector(itemFinishPlaying),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)

        let layer: AVPlayerLayer = AVPlayerLayer(player: player)
        layer.backgroundColor = UIColor.white.cgColor
        layer.frame = CGRect(x: 0, y: 0, width: playerView.frame.width, height: playerView.frame.height)
        layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerView.layer.sublayers?.forEach({$0.removeFromSuperlayer()})
        playerView.layer.addSublayer(layer)
    }
    


    @objc func itemFinishPlaying() {
        if let startTime = trimmerView.startTime {
            player?.seek(to: startTime)
        }
    }

    
    @IBAction func cropVideo(_ sender: Any) {
        crop(sourceURL1: url!, statTime: trimmerView.startTime!, endTime: trimmerView.endTime!)
    }
    

    func startPlaybackTimeChecker() {

        stopPlaybackTimeChecker()
        playbackTimeCheckerTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
                                                        selector:
            #selector(VideoTrimmerViewController.onPlaybackTimeChecker), userInfo: nil, repeats: true)
    }

    func stopPlaybackTimeChecker() {

        playbackTimeCheckerTimer?.invalidate()
        playbackTimeCheckerTimer = nil
    }

    @objc func onPlaybackTimeChecker() {

        guard let startTime = trimmerView.startTime, let endTime = trimmerView.endTime, let player = player else {
            return
        }

        let playBackTime = player.currentTime()
        trimmerView.seek(to: playBackTime)

        if playBackTime >= endTime {
            player.seek(to: startTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
            trimmerView.seek(to: startTime)
        }
    }
    
    func crop(sourceURL1: URL, statTime:CMTime, endTime:CMTime)
    {
         self.view.isUserInteractionEnabled = false
        let manager = FileManager.default
        guard let documentDirectory = try? manager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) else {return}
            let asset = AVAsset(url: sourceURL1)
            let length = Float(asset.duration.value) / Float(asset.duration.timescale)
            print("video length: \(length) seconds")
            var outputURL = documentDirectory.appendingPathComponent("output")
            do {
                try manager.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
                let name = "Exported"
                outputURL = outputURL.appendingPathComponent("\(name).mov")
            }catch let error {
                print(error)
            }
            //Remove existing file
            _ = try? manager.removeItem(at: outputURL)
            guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else {return}
            exportSession.outputURL = outputURL
            exportSession.outputFileType = AVFileType.mp4
            
            let timeRange = CMTimeRange(start: statTime, end: endTime)
            
            exportSession.timeRange = timeRange
            exportSession.exportAsynchronously{
                switch exportSession.status {
                case .completed:
                    self.trimmerView.handleColor = UIColor.white
                    self.trimmerView.mainColor = UIColor.darkGray
                    DispatchQueue.main.async {
                         self.url = outputURL
                         let asset = AVAsset(url: outputURL)
                         self.loadVideoToTrim(asset)
                         self.view.isUserInteractionEnabled = true
                    }
                case .failed:
                     self.view.isUserInteractionEnabled = true
                    self.view.makeToast(message: localizedString(forKey: "ERROR_TRIMMING_VIDEO"))
                    print("failed \(String(describing: exportSession.error))")
                    
                case .cancelled:
                     self.view.isUserInteractionEnabled = true
                    print("cancelled \(String(describing: exportSession.error) )")
                    
                default: break
                }
            }
        
        
    }
}

extension VideoTrimmerViewController: TrimmerViewDelegate {
    func positionBarStoppedMoving(_ playerTime: CMTime) {
        player?.seek(to: playerTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
        player?.play()
        startPlaybackTimeChecker()
    }

    func didChangePositionBar(_ playerTime: CMTime) {
        stopPlaybackTimeChecker()
        player?.pause()
        player?.seek(to: playerTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
        let duration = (trimmerView.endTime! - trimmerView.startTime!).seconds
        print(duration)
    }
}
