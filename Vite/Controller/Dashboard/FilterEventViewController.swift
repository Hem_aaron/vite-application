//
//  FilterEventViewController.swift
//  Vite
//
//  Created by Eeposit 01 on 8/22/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit
import HTagView
import MapKit


//@available(iOS 9.3, *)
class FilterEventViewController: WrapperController {
    
    var tagLists = [String]()
    var interest = [[String: AnyObject]]()
    var indicator = ActivityIndicator()
    var predictions = [Prediction]()
    var pageSize = 10
    var page = Int()
    
    @IBOutlet weak var interestView: HTagView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Filters"
        interestView.layer.cornerRadius = 10
        getTagList()
    }
    
    func getTagList(){
        VProfileWebService.getUserInterestList(successBlock: { (interestList) in
            self.tagLists = interestList
            self.interestView.delegate = self
            self.interestView.dataSource = self
        }) { (message) in
            showAlert(controller: self, title: "", message: message)
        }
        
    }
    
    func getEventListByLocation() {
        let parameter = [
            "interests": interest,
            "maxResults": pageSize,
            "pageNo": page,
            "searchString": ""
            
            ] as [String : Any]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "EventByLocation"), object: nil, userInfo: parameter)
        Route.goToHomeController()
        
    }
    
    
    
    @IBAction func back(sender: AnyObject) {
        Route.goToHomeController()
        
    }
    
    @IBAction func applyFilters(_ sender: Any) {
         getEventListByLocation()
    }
}

@available(iOS 9.3, *)
extension FilterEventViewController: HTagViewDelegate , HTagViewDataSource  {
    
    // MARK: - TagViewDataSource
    func numberOfTags( _ tagView: HTagView) -> Int {
        return tagLists.count
    }
    
    func tagView(_ tagView: HTagView, titleOfTagAtIndex index: Int) -> String {
        return  tagLists[index]
    }
    
    func tagView(_ tagView: HTagView, tagTypeAtIndex index: Int) -> HTagType {
        return .select
    }
    
    // MARK: - TagViewDelegate
    func tagView(_ tagView: HTagView, tagSelectionDidChange selectedIndices: [Int]) {
        interest.removeAll()
        for select in selectedIndices{
            interest.append([ "filterName" : tagLists[select] as AnyObject])
        }
    }
    
}






