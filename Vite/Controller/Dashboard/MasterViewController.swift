//
//  MasterViewController.swift
//  Vite
//
//  Created by Hem Poudyal on 4/18/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit
import MapKit
import SDWebImage
import Firebase
import FirebaseAuth
import pop
import Alamofire
import Stripe
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import Branch
import MIBadgeButton_Swift
import SlideMenuControllerSwift


class MasterViewController: WrapperController , PeopleViewDelegate {
    
    var count: Int?
    
    @IBOutlet weak var btnCreateEvent: UIButton!
    @IBOutlet weak var segmentView: UIView!
    @IBOutlet weak var btnPeople: UIButton!
    @IBOutlet weak var btnEvents: UIButton!
    @IBOutlet weak var btnViteFeed: UIButton!
    @IBOutlet weak var containerEvent: UIView!
    @IBOutlet weak var containerPeople: UIView!
    @IBOutlet weak var containerViteFeed: UIView!
    @IBOutlet weak var btnMyVites: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    //@IBOutlet weak var btnUserCount: CircularButton!
    @IBOutlet weak var btnUserCount: MIBadgeButton!
    @IBOutlet weak var btnFilterPeople: UIButton!
    @IBOutlet weak var btnContact: UIButton!
    @IBOutlet weak var btnFilterCameraWidth: NSLayoutConstraint!
    
    var fromEventFilter = true
    var isPeopleAnimated = false
    var isEventAnimated = false
    var forViteFeed = false
    var fullScreenButton: UIButton!
    var containers : [UIView] {
        return [self.containerEvent, containerPeople, containerViteFeed]
    }
    var nearViteListsArr = [VVite]()
    var eventForPostCount = Int()
    var containerButtons:[UIButton] {
        return [self.btnPeople, self.btnEvents, self.btnViteFeed]
    }
    
    override func viewDidLoad() {
        self.view.alpha = 0
        segmentView.layer.borderColor = UIColor.lightGray.cgColor
        segmentView.layer.cornerRadius = 18
        segmentView.layer.borderWidth = 1.0
        btnUserCount.badgeBackgroundColor = UIColor.clear
        btnUserCount.badgeEdgeInsets = UIEdgeInsetsMake(19, -10, 0, 0)
        btnUserCount.setImage(UIImage(named: "TallyOn"), for: .normal)
        btnUserCount.setImage(UIImage(named: "Tally_Off"), for: .highlighted)

        if UserDefaultsManager().isBusinessAccountActivated{
            self.btnEvents.isHidden = true
        } else {
            self.btnEvents.isHidden = false
        }

        showEvents()
        self.startObservingNotifications()
        self.authenticateInFirebase()
        self.setupNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(MasterViewController.updateInUI(notification:)), name: NSNotification.Name(rawValue: "KUpdateUi"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.switchAccount(notification:)), name: NSNotification.Name(rawValue: "switchAccount"), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 1, animations: {
            self.view.alpha = 1
        }, completion: {_ in
            self.showNotificationOnIcons()
            self.swappedUserList()
            
        })
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.removeObserver(self)
    }
  

    
    //MARK: - Methods
    
    @objc func switchAccount(notification: NSNotification) {
        //showNoEventScreen()
        if UserDefaultsManager().isBusinessAccountActivated{
            self.btnEvents.isHidden = true
        } else {
            self.btnEvents.isHidden = false
        }
    }
    
    // Notification and firebase setup
    func authenticateInFirebase() {
        
        if let token = FBSDKAccessToken.current()?.tokenString{
            let credential = FIRFacebookAuthProvider.credential(withAccessToken: token)
            
            FIRAuth.auth()?.signIn(with: credential) { (user, error) in
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "inviteVc" {
            let destinationController =  segue.destination as! InviteFriendsVC
            destinationController.isFromContact = true
        } else {
            if let destinationViewController = segue.destination as? PeopleViewController {
                destinationViewController.delegate = self
            }
            
        }
    }
    
    
    //Notification observer
    @objc func updateInUI(notification: NSNotification) {
        self.fullScreenButton = UIButton(frame: self.view.bounds)
        fullScreenButton.backgroundColor = UIColor.clear
        fullScreenButton.addTarget(self, action: #selector(fullScreenButtonAction), for:.touchUpInside)
        self.view.addSubview(fullScreenButton)
    }
    
    // fullScreenbutton Action
    @objc func fullScreenButtonAction(sender: AnyObject?) {
       // self.revealViewController().rightRevealToggle(sender)
        fullScreenButton.removeFromSuperview()
    }
    
    
    // people View Delegate
    func swappedUserList(){
        let swappedPeople = SwappedPeopleService.getPeopleList()
        btnUserCount.badgeString = swappedPeople.count > 99 ? "99+" : String(swappedPeople.count)
        //btnUserCount.setTitle("\(swappedPeople.count)", forState: .Normal)
    }
    
    func toggleNotificationButton(status:Bool) {
        let imageName = status ? "newMessages" : "MsgCenter_Off"
        self.btnMessage.setImage(UIImage(named: imageName), for: .normal)
    }
    
    func startObservingNotifications() {
        let user = UserDefaultsManager.getActiveUserForNotification()
        NotificationService().startNotificationObserver(user.uid) {
            (count) in
            
            if count > 0 {
                self.toggleNotificationButton(status: true)
            } else {
                self.toggleNotificationButton(status: false)
            }
        }
    }
    
    func setupNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.showNotificationOnIcons), name: NSNotification.Name(rawValue: kNotificationReceived), object: nil)
    }
    
    func locationUpdated() {
        print ("location updated")
    }
    
    
    //People, events and vite feed logic
    func showEvents() {
        self.forViteFeed = false
        self.fromEventFilter = true
        self.isPeopleAnimated = false
        self.highlightSelectedButton(button: self.btnEvents)
        self.showActiveContainer(container: self.containerEvent)
        if !isEventAnimated {
            UIView.animate(withDuration: 0.7, animations: {
                self.isEventAnimated = true
                self.btnFilterPeople.center.x -= self.view.bounds.width
                self.btnUserCount.isHidden = true
                self.btnContact.isHidden = true
                self.btnFilterPeople.isHidden = false
                self.btnFilterPeople.setImage(UIImage(named: "Filters"), for: .normal)
                self.btnFilterCameraWidth.constant = 25
            })
            
        }
    }
    
    func showPeople() {
        self.forViteFeed = false
        self.highlightSelectedButton(button: self.btnPeople)
        swappedUserList()
        self.showActiveContainer(container: self.containerPeople)
        self.fromEventFilter = false
        self.isEventAnimated = false
        if !isPeopleAnimated {
            UIView.animate(withDuration: 0.7, animations: {
                self.isPeopleAnimated = true
                self.btnUserCount.center.x -= self.view.bounds.width
                self.btnFilterPeople.center.x -= self.view.bounds.width
                self.btnUserCount.isHidden = false
                self.btnContact.isHidden = false
                self.btnFilterPeople.isHidden = false
                self.btnFilterPeople.setImage(UIImage(named: "Filters"), for: .normal)
                self.btnFilterCameraWidth.constant = 25
            })
        }
    }
    
    func showViteFeed() {
        self.fromEventFilter = false
        self.isPeopleAnimated = false
        self.isEventAnimated = false
        self.highlightSelectedButton(button: self.btnViteFeed)
        self.showActiveContainer(container: self.containerViteFeed)
        if !forViteFeed {
            UIView.animate(withDuration: 0.7, animations: {
                self.forViteFeed = true
                self.btnFilterPeople.center.x -= self.view.bounds.width
                self.btnUserCount.isHidden = true
                self.btnFilterPeople.setImage(UIImage(named: "cameraIcon"), for: .normal)
                self.btnFilterCameraWidth.constant = 30
                self.btnFilterPeople.isHidden = false
                self.btnContact.isHidden = true
            })
        }
        
    }
    
    func showActiveContainer(container:UIView) {
        for view in self.containers {
            view.isHidden = (view != container)
        }
    }
    
    func highlightSelectedButton(button:UIButton) {
        for btn in self.containerButtons {
            if btn == button {
                btn.setTitleColor(UIColor.purple, for: .normal)
            } else {
                btn.setTitleColor(UIColor.lightGray, for: .normal)
            }
        }
    }
    
    @objc func showNotificationOnIcons(){
        //Set Message button badge count and color
        let userDefaults = UserDefaults.standard
        btnMyVites.setImage(UIImage(named:(userDefaults.bool(forKey: UserDefaultKeys.myViteNotification)) ? "newVites" : "MyVite_Off"), for: UIControlState.normal)
        btnMessage.setImage(UIImage(named:(userDefaults.bool(forKey: UserDefaultKeys.msgCenterNotification)) ? "newMessages" : "MsgCenter_Off" ), for: UIControlState.normal)
    }
}

// MARK: - IBActions
extension MasterViewController {
    @IBAction func toggle(sender: AnyObject) {
       self.slideMenuController()?.openLeft()
        
    }
    
    @IBAction func peopleAction(sender: AnyObject) {
        self.showPeople()
    }
    
    @IBAction func eventsAction(sender: AnyObject) {
        self.showEvents()
    }
    
    @IBAction func viteFeedAction(sender: AnyObject) {
        self.showViteFeed()
    }
    
    
    @IBAction func contactAction(sender: AnyObject) {

    }
    
    @IBAction func myVitesAction(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: UserDefaultKeys.myViteNotification)
        self.slideMenuController()?.changeMainViewController(Route.myVitesNav, close: true)
    }
    
    @IBAction func createEventAction(_ sender: Any) {
        let swipedUsers = SwappedPeopleService.getPeopleList()
        if swipedUsers.count > 0 {
            
            let createEventOptions = Route.createEventOptions
            createEventOptions.modalPresentationStyle = .overCurrentContext
            createEventOptions.modalTransitionStyle = .crossDissolve
            self.present(createEventOptions, animated: true, completion: nil)
            createEventOptions.delegate = self
        } else {
      
            let nav = UINavigationController(rootViewController: Route.CreateEvent)
            //self.present(nav, animated: true, completion: nil)
            self.slideMenuController()?.changeMainViewController(nav, close: true)
        }
    }
    
    @IBAction func msgCenterAction(sender: AnyObject) {
        UserDefaults.standard.set(false, forKey: UserDefaultKeys.msgCenterNotification)
        let controller = Route.MessageCenter
        let nav = UINavigationController(rootViewController: controller)
        self.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func showFilterView(sender: AnyObject) {
        if fromEventFilter {
            if #available(iOS 9.3, *) {
                let controller = Route.eventFilterNav
                self.slideMenuController()?.changeMainViewController(controller, close: true)
            }
        } else if forViteFeed {
            self.getNearViteLists()
        } else {
            let controller = Route.filterUser
            controller.isForPeopleView = true
            let nav = UINavigationController(rootViewController: controller)
            self.present(nav, animated: true, completion: nil)
        }
    }
 
    @IBAction func showSwappedUserList(sender: AnyObject) {
         let nav = UINavigationController(rootViewController: Route.SwapPeople )
        self.show(nav, sender: nil)
    }
}
// MARK: - CreateEventOptionsDelegate
extension MasterViewController:CreateEventOptionsViewControllerDelegate {
    func selectedCreateEventsForUsers() {
        let controller = Route.CreateEvent
        controller.isForAddingUsersToInviteList = true
      let nav = UINavigationController(rootViewController: controller)
      self.slideMenuController()?.changeMainViewController(nav, close: true)
    }
    
    func selectedAddUsersToExistingEvent() {
    
         let controller = Route.MyEvents
        let eventController = controller.viewControllers[0] as! EventListTableViewController
        eventController.isFromPeopleList = true
        eventController.isFromCreateBtn = true
      self.slideMenuController()?.changeMainViewController(controller, close: true)
    }
    
    func selectedDecideLater() {
          let nav = UINavigationController(rootViewController: Route.CreateEvent)
          self.slideMenuController()?.changeMainViewController(nav, close: true) 
    }
}

//MARK: VITE FEED POSTING

extension MasterViewController {
    
    func getNearViteLists() {
        VPostMediaViteFeedService.getNearViteList(successBlock: { (lists) in
            self.nearViteListsArr = lists
            self.fetchEvents()
        }) { (msg) in
            print(msg)
        }
    }
    
    func fetchEvents() {
        EventServices.getEventListWithPaginationForPosting(1, pageSize: 10, successBlock: { [weak self]
            (eventList,totalCount)  in
            self?.eventForPostCount = totalCount.intValue
            if (self?.nearViteListsArr.count == 0 && self?.eventForPostCount == 0) {
                showAlert(controller: self!, title: "", message: localizedString(forKey: "NO_VITE_EVENT"))
            } else {
                let controller  = Route.showViteFeedMedia
                self?.present(controller, animated: true, completion: nil)
            }
            }, failureBlock:{[weak self] _ in ()
        })
    }
}
