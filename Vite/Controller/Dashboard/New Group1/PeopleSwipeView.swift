//
//  PeopleSwipeView.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 5/3/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import UIKit
import Koloda

class PeopleSwipeView: KolodaView{
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblGender_Age: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        Bundle.main.loadNibNamed("PeopleSwipe", owner: self, options: nil)
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("PeopleSwipe", owner: self, options: nil)
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    
}
