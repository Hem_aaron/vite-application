//
//  SwappedPeopleService.swift
//  Vite
//
//  Created by Prajeet Shrestha on 5/9/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class SwappedPeopleService {
    static private let key = "NSSwappedUser"
    static private var ud:UserDefaults {
        return UserDefaults.standard
    }
    
    static func addPeople(people:PrivateFriend) {
        var swappedPeopleArray = self.getPeopleList()
        swappedPeopleArray.append(people)
        let encodedData :NSData = NSKeyedArchiver.archivedData(withRootObject: swappedPeopleArray) as NSData
        ud.set(encodedData, forKey: key)
        ud.synchronize()
    }
    
    static func getPeopleList() -> [PrivateFriend] {
        if let  decoded = ud.object(forKey: key) as? NSData {
            let decodedPeopleList = NSKeyedUnarchiver.unarchiveObject(with: decoded as Data) as! [PrivateFriend]
            return decodedPeopleList
        } else {
            return [PrivateFriend]()
        }
    }
    
    static func filterPeopleOfSavedListFrom(newList peoples: [PrivateFriend], savedPeople:[PrivateFriend]) -> [PrivateFriend]{
    
        if savedPeople.count == 0 {
            return peoples
        } else {
            return peoples.filter { (people) -> Bool in
                let filteredArr = savedPeople.filter({ (savedPerson) -> Bool in
                    return people.facebookId == savedPerson.facebookId
                })
                return filteredArr.count == 0
            }
        }
    }
    
    static func deletePersonFromSwappedList(person:PrivateFriend) {
        let swappedPeopleArray = self.getPeopleList()
        let filteredArr = swappedPeopleArray.filter { (friend) -> Bool in
            return friend.facebookId != person.facebookId
        }
        let encodedData :NSData = NSKeyedArchiver.archivedData(withRootObject: filteredArr) as NSData
        ud.set(encodedData, forKey: key)
        ud.synchronize()
    }
    
    static func clearSwappedList() {
        ud.set(nil, forKey: key)
    }
}
