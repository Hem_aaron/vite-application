//
//  SwappedPeopleViewController.swift
//  Vite
//
//  Created by Prajeet Shrestha on 5/9/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class SwappedPeopleViewController: WrapperController {
    var peoples = [PrivateFriend]()
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var tbl: UITableView!
    @IBOutlet weak var btnAddUsers: UIButton!
    @IBOutlet weak var btnClearList: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.refreshList()
        self.title = "Swiped User List"
    }
    
    func refreshList() {
        self.peoples = SwappedPeopleService.getPeopleList()
        self.tbl.reloadData()
        
        if self.peoples.count == 0 {
            self.tbl.isHidden = true
            self.lblMessage.isHidden = false
            self.btnAddUsers.isEnabled = false
            self.btnClearList.isEnabled = false
        } else {
            self.tbl.isHidden = false
            self.lblMessage.isHidden = true
        }
    }
    
    @IBAction func addSelectedUsersToEvent(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "MyEvents", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "EventListNav") as! UINavigationController
        let eventController = controller.viewControllers[0] as! EventListTableViewController
        eventController.isFromPeopleList = true
        self.slideMenuController()?.changeMainViewController(controller, close: true)
    }
    
    @IBAction func back(sender: AnyObject) {
        self.navigationController!.popToRootViewController(animated: true)
    }
    
    @IBAction func clearPeopleList(sender: AnyObject) {
        SwappedPeopleService.clearSwappedList()
        self.refreshList()
        self.btnAddUsers.isEnabled = false
    }
}

extension SwappedPeopleViewController:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.peoples.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let person = self.peoples[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "SwappedPeopleCell") as! SwappedPeopleCell
        cell.lblName.text = person.fullName
        if let userImageUrl = person.profileImage?.convertIntoViteURL(), let fbId = person.facebookId {
            if isValidImage(url: userImageUrl){
                cell.imgUser.sd_setImage(with: URL(string: userImageUrl), placeholderImage: UIImage(named: "ProfilePlaceholder.png"))
            } else {
                let profileUrl = "http://graph.facebook.com/\(fbId)/picture?type=large"
                cell.imgUser.sd_setImage(with: URL(string: profileUrl), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
            }
        }
        var genderAndAgeString = ""
        if let gender = person.gender {
            genderAndAgeString = gender
        }
        
        if let age = person.age {
            genderAndAgeString = ", \(age)"
        }
        cell.lblGenderAndAge.text = genderAndAgeString
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            SwappedPeopleService.deletePersonFromSwappedList(person: self.peoples[indexPath.row])
            self.refreshList()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = self.peoples[indexPath.row]
        let controller                       = Route.profileViewController
        controller.fbId                      = user.facebookId
        controller.isForViewingOthersProfile = true
        self.navigationController?.show(controller, sender: nil)
    }
}
