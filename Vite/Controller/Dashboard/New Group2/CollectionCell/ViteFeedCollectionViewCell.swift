//
//  ViteFeedCollectionViewCell.swift
//  Vite
//
//  Created by Hem Poudyal on 4/26/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class ViteFeedCollectionViewCell: UICollectionViewCell {
    

    @IBOutlet weak var imageEvent: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblOrgName: UILabel!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnOrganizer: UIButton!
    
    override func awakeFromNib() {
        imageEvent.layer.cornerRadius = 10
        imgProfile.layer.cornerRadius = 20
        imgProfile.clipsToBounds = true
    }
}