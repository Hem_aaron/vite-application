//
//  ViteFeedViewController.swift
//  Vite
//
//  Created by Hem Poudyal on 4/23/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper
import AVFoundation

class ViteFeedViewController: WrapperController, UICollectionViewDataSource, CollectionViewWaterfallLayoutDelegate {
    
    @IBOutlet weak var placeHolderImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    var feedList = [ViteFeed]()
    var cardAmount = 20
    var pageNum = 1
    var currentPage = 1
    var pageRefreshing = false
    var count = 0{
        didSet {
            placeHolderImageView.isHidden = count>0 ? true : false
        }
    }
    let refresher = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.loadViteFeed(pageNumber: pageNum, cardAmount: cardAmount)
        self.feedList = [ViteFeed]()
        
        let layout = CollectionViewWaterfallLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
        layout.headerInset = UIEdgeInsetsMake(0, 0, 0, 0)
        layout.headerHeight = 0
        layout.footerHeight = 20
        layout.minimumColumnSpacing = 10
        layout.minimumInteritemSpacing = 10
        
        collectionView.collectionViewLayout = layout
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: CollectionViewWaterfallElementKindSectionHeader, withReuseIdentifier: "Header")
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: CollectionViewWaterfallElementKindSectionFooter, withReuseIdentifier: "Footer")
        self.collectionView.backgroundColor = UIColor.white
        
        self.collectionView!.alwaysBounceVertical = true
        refresher.tintColor = UIColor.gray
        refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        collectionView!.addSubview(refresher)
    }
    
    //    func switchAccount(notification: NSNotification) {
    //        // collectionView.hidden = User().isBusinessAccountActivated ? true : false
    //    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.collectionView.isUserInteractionEnabled = true
    }
    
    //MARK:- methods
    
    @objc func loadData()
    {
        //code to execute during refresher
        loadViteFeed(pageNumber: 1, cardAmount: 20)
    }
    
    func stopRefresher()
    {
        refresher.endRefreshing()
    }
    
    func loadViteFeed(pageNumber:Int ,cardAmount:Int ){
        WebService.request(method: .get, url: kAPILiveFeed + "/\(pageNumber)/\(cardAmount)", success: { (response) in
            guard let response = response.result.value as! [String: AnyObject]?,
                let success = response["success"] as? Bool else {
                    return
            }
            self.pageRefreshing = false
            self.view.hideToastActivity()
            
            if let totalCount = response["params"]?["count"] as? Int{
                self.count = Int(totalCount)
            }
            
            if pageNumber == 1{
                self.feedList.removeAll()
            }
            if success {
                let responeArray = response["params"]?["media"] as! [[String:AnyObject]]
                for obj in responeArray {
                    let feed = Mapper<ViteFeed>().map(JSON: obj)
                    if let f = feed {
                        self.feedList.append(f)
                    }
                }
            }
            self.collectionView.reloadData()
            self.stopRefresher()
        }) { (message, apiError) in
            if pageNumber>1{
                self.pageNum = self.pageNum - 1
            }
            showAlert(controller: self, title: "", message: message)
            self.stopRefresher()
        }
        
    }
    
    func showImageOrVideoScreen(feed:ViteFeed){
        let storyboard = UIStoryboard(name: "EventDetail", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "swipeFullScreenVC") as! SwipeFullScreenViewController
        controller.mediaURL = feed.mediaUrl!
        controller.mediaType = feed.fileType!
        controller.feedId = feed.feedId
        
        
        VEventWebService.getEventDetail(feed.eventId!, successBlock: { (eventDetail) in
            controller.detailEvent = eventDetail
            self.navigationController?.pushViewController(controller, animated: true)
            
        }) { (message) in
            showAlert(controller: self, title: " ", message: message)
        }
        
    }
    
    @objc func showOrganizer(sender: UIButton){
        let feed =  self.feedList[Int(sender.tag)]
        //add friends of user in server
        self.collectionView.isUserInteractionEnabled = false
        if feed.businessAccount!{
            // show business account
            let controller                       = Route.updateAccountForBusiness
            controller.isForViewingOthersBusinessProfile = true
            controller.businessID = String(feed.businessId!)
            self.navigationController?.show(controller, sender: nil)
        }
        else{
            let controller                       = Route.profileViewController
            controller.fbId                      = String(describing: feed.organizerId!)
            controller.isForViewingOthersProfile =  String(describing: feed.organizerId!) == UserDefaultsManager().userFBID! ? false : true
            self.navigationController?.show(controller, sender: nil)
        }
        
    }
    
    @objc func showFullScreen(sender: UIButton){
        
        let feed =  self.feedList[Int(sender.tag)]
        //add friends of user in server
        self.collectionView.isUserInteractionEnabled = false
        self.showImageOrVideoScreen(feed: feed)
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feedList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let feed = self.feedList[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath ) as! ViteFeedCollectionViewCell
        let mediaUrl = feed.mediaUrl?.convertIntoViteURL()
        
        if feed.fileType == "VIDEO"{
            cell.btnPlay.isHidden = false
            let thumbnailUrl = feed.thumbnailUrl?.convertIntoViteURL()
            cell.imageEvent.sd_setImage(with: URL(string:thumbnailUrl!), completed: nil)
            //cell.imageEvent.sd_setImageWithURL(NSURL(string:thumbnailUrl!))
            cell.btnPlay.tag = indexPath.row
            cell.btnPlay.addTarget(self, action: #selector(self.showFullScreen(sender:)), for: .touchUpInside)
        }else{
            cell.btnPlay.isHidden = true
            cell.imageEvent.sd_setImage(with: URL(string:mediaUrl!))
        }
        cell.btnOrganizer.tag = indexPath.row
        cell.btnOrganizer.addTarget(self, action: #selector(self.showOrganizer(sender:)), for: .touchUpInside)
        
        if let imgUrl = feed.profileImage?.convertIntoViteURL(), let fbId = feed.organizerId {
            if isValidImage(url: imgUrl){
                cell.imgProfile.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "ProfilePlaceholder.png"))
            } else {
                let profileUrl = "http://graph.facebook.com/\(fbId)/picture?type=large"
                cell.imgProfile.sd_setImage(with: URL(string: profileUrl), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
            }
        }
        cell.lblOrgName.text = feed.organizerName
        cell.title.text = feed.eventTitle
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var reusableView: UICollectionReusableView? = nil
        
        if kind == CollectionViewWaterfallElementKindSectionHeader {
            reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath)
            
            if let view = reusableView {
                view.backgroundColor = UIColor.white
            }
        }
        else if kind == CollectionViewWaterfallElementKindSectionFooter {
            reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Footer", for: indexPath)
            if let view = reusableView {
                view.backgroundColor = UIColor.white
            }
        }
        
        return reusableView!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let feed = self.feedList[indexPath.row]
        if feed.fileType == "IMAGE" {
            self.collectionView.isUserInteractionEnabled = false
            self.showImageOrVideoScreen(feed: feed)
        }
    }
    
    // MARK: WaterfallLayoutDelegate
    
    func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if feedList.count == 0 {
            return  CGSize(width: 0.0, height: 0.0)
        }
        
        let media = self.feedList[indexPath.row]
        let width = 130
        let height = ((CGFloat(truncating: media.mediaHeight!) / CGFloat(truncating: media.mediaWidth!)) * 130) + 100
        
        return CGSize(width: CGFloat(width), height: CGFloat(height))
    }
    
    
    func  scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            //you reached end of the table
            if self.pageNum * 20 < self.count{
                //page refreshing checks if the app is busy getting data from server
                if !pageRefreshing{
                    self.pageNum += 1
                    self.currentPage = self.pageNum
                    self.loadViteFeed(pageNumber: self.pageNum, cardAmount: cardAmount)
                    pageRefreshing = true
                }
            }
        }
    }
}
