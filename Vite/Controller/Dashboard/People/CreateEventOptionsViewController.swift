//
//  CreateEventPopUpViewController.swift
//  Vite
//
//  Created by Prajeet Shrestha on 5/15/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit
protocol CreateEventOptionsViewControllerDelegate: class {
    func selectedCreateEventsForUsers()
    func selectedAddUsersToExistingEvent()
    func selectedDecideLater()
}
class CreateEventOptionsViewController: UIViewController {
    
    weak var delegate:CreateEventOptionsViewControllerDelegate?
    @IBOutlet weak var lblUserCountMessage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblUserCountMessage.text = "You have \(SwappedPeopleService.getPeopleList().count) Users on your list"
    }

}

// MARK: - IBActions

extension CreateEventOptionsViewController {
    
    @IBAction func back(_ sender: Any) {
        if let del = delegate {
            self.dismiss(animated: false, completion: nil)
            del.selectedDecideLater()
            
        }
    }
    
    @IBAction func createEventForUsersAction(_ sender: Any) {
        if let del = delegate {
            self.dismiss(animated: false, completion: nil)
            del.selectedCreateEventsForUsers()
            
        }
    }
    
    @IBAction func addUsersToExistingEventAction(_ sender: AnyObject) {
        if let del = delegate {
            self.dismiss(animated: false, completion: nil)
            del.selectedAddUsersToExistingEvent()
            
        }
    }
}
