//
//  PeopleViewController.swift
//  Vite
//
//  Created by Hem Poudyal on 4/20/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//


import UIKit
import Koloda
import SDWebImage
import Hero
enum OrganizerPeopleResponse:String {
    case Requested = "ADDED"
    case Rejected  = "REMOVED"
}

protocol PeopleViewDelegate {
    func swappedUserList()
}

class PeopleViewController: WrapperController{
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var lblNoUser: UILabel!
    @IBOutlet weak var kolodaView: KolodaView!
    var peopleListArr = [PrivateFriend]()
    var facebookId = String()
    var delegate: PeopleViewDelegate?
    var isLoaded:Bool = false
    var pageNo = Int()
    var friendFound  = false

    override func viewDidLoad() {
        getPeopleList()
        NotificationCenter.default.addObserver(self,selector:#selector(self.getFilterList(notification:)), name: NSNotification.Name(rawValue: "pushFilterPeople"), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // To ensure kolada datasource and delegate is set only once after the view appears
        if !isLoaded {
            kolodaView.dataSource = self
            kolodaView.delegate = self
            isLoaded = true
        }
    }
    
    func filterMainPeopleListFromSwappedList(peoples: [PrivateFriend]){
        self.peopleListArr = SwappedPeopleService.filterPeopleOfSavedListFrom(newList: peoples, savedPeople: SwappedPeopleService.getPeopleList())
        if self.peopleListArr.count == 0 {
            showNoUserMsg()
        } else {
            backgroundView.isHidden = true
        }
    }
    
    func showNoUserMsg(){
        self.backgroundView.isHidden = false
    }
    
    //MARK: Get people List by Location
    
    func getPeopleList(){
        pageNo += 1
        //to check it is not the list from filter view
        UserDefaults.standard.set(false, forKey: "filterBool")
        VEventWebService.getPeopleList(pageNo, maxResult: 10, successBlock: { (peopleList) in
            if peopleList.count == 0 {
                 self.showNoUserMsg()
            }else {
                self.peopleListArr = peopleList
                self.filterMainPeopleListFromSwappedList(peoples: peopleList)
                self.kolodaView.resetCurrentCardIndex()
            }
        }) { (message) in
            showAlert(controller: self, title: "", message: message)
        }
    }
    
    @objc func getFilterList(notification: NSNotification){
        if let list =  notification.object{
            self.filterMainPeopleListFromSwappedList(peoples: list as! [PrivateFriend])
        }
        self.kolodaView.resetCurrentCardIndex()
    }
}

//MARK: -  Koloda Delegates

extension PeopleViewController: KolodaViewDelegate {
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        if direction == SwipeResultDirection.right {
            let people = self.peopleListArr[Int(index)]
            filterDublicatAddedPeopleFromKolodaSwapWithSaveSwappedPeople(people: people)
        }
    }
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        let  isFromFilterView = UserDefaults.standard.object(forKey: "filterBool") as! Bool
        if !isFromFilterView{
            getPeopleList()
        }else {
            self.showNoUserMsg()
        }
    }
  
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        let userData = self.peopleListArr[Int(index)]
        let controller                       = Route.profileViewController
        controller.fbId                      = userData.facebookId
        controller.isForViewingOthersProfile = true
        let nav = UINavigationController.init(rootViewController: controller)
        nav.hero.isEnabled = true
        self.show(nav, sender: nil)
    }
    
 
}

extension PeopleViewController: KolodaViewDataSource {
  
  
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return self.peopleListArr.count
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let swipe = PeopleSwipeView()
        let userData = self.peopleListArr[Int(index)]
        self.facebookId = userData.facebookId!
        swipe.imgProfile.hero.id = self.facebookId
        swipe.lblName.hero.id = "Name" + self.facebookId
        swipe.lblGender_Age.hero.id = "Gender" + self.facebookId
        swipe.lblDescription.hero.id = "Desc" + self.facebookId
        swipe.lblName.text = userData.fullName
        if (userData.info == "N/A") {
            swipe.lblDescription.text = ""
        } else {
            swipe.lblDescription.text = userData.info
        }
        swipe.lblGender_Age.text = userData.gender
        if let age =  userData.age{
            swipe.lblGender_Age.text = swipe.lblGender_Age.text! + ", " + age
        }
        //        if let profileImg = userData.profileImage{
        //            let url =  profileImg.convertIntoViteURL()
        //            swipe.imgProfile.sd_setImageWithURL(NSURL(string:url))
        //           }
        
        if let profileImg = userData.profileImage {
            let url = profileImg.convertIntoViteURL()
            if isValidImage(url: url){
                swipe.imgProfile.sd_setImage(with: URL(string:url))
            } else {
                let profileUrl = "http://graph.facebook.com/\(userData.facebookId!)/picture?type=large"
                swipe.imgProfile.sd_setImage(with: URL(string:profileUrl))
            }
        }
        
        swipe.layer.cornerRadius = 10.0
        swipe.clipsToBounds = true
        return swipe
    }
    

    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return DragSpeed.moderate
    }
    
    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
       //return nil
        return Bundle.main.loadNibNamed("OverlayView", owner: self, options: nil)![0] as?OverlayView
    }
    
}
extension PeopleViewController {

    func filterDublicatAddedPeopleFromKolodaSwapWithSaveSwappedPeople(people: PrivateFriend){
        let recentAddedPeople = people
        let savePeopleList  = SwappedPeopleService.getPeopleList()
        for savePeople in savePeopleList {
            if savePeople.facebookId == recentAddedPeople.facebookId {
                friendFound = true
            }
        }
        if friendFound {
            showAlert(controller: self, title: localizedString(forKey: "ALERT"), message: localizedString(forKey: "USER_ALREADY_ADDED"))
            
        } else {
            SwappedPeopleService.addPeople(people: recentAddedPeople)
            self.delegate?.swappedUserList()
        }
        
    }

}

