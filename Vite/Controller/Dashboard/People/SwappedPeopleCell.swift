//
//  SwappedPeopleCell.swift
//  Vite
//
//  Created by Prajeet Shrestha on 5/11/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class SwappedPeopleCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblGenderAndAge: UILabel!
    @IBOutlet weak var imgUser: CircleImage!
    
}
