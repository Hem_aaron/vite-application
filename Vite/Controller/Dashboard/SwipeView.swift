//
//  SwipeView.swift
//  Vite
//
//  Created by EeposIT_X on 2/9/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation
import UIKit
import Koloda
import AVFoundation

class SwipeView: KolodaView{
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var lblTimeStamp: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgPriceTag: UIImageView!
    @IBOutlet weak var celebStarLargeImgView: UIImageView!
    @IBOutlet weak var celebStarImgWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var horrizontalSpaceBetnTitleAndStar: NSLayoutConstraint!
    
    @IBOutlet weak var trailingSpace: NSLayoutConstraint!
    @IBOutlet weak var celebName: UILabel!
    @IBOutlet weak var celebStarImgView: UIImageView!
    var avPlayer:AVPlayerLayer?
    var player: AVPlayer?
    var viewProfile:(() -> ())?
    var shareEvent:(() -> ())?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        Bundle.main.loadNibNamed("SwipeView", owner: self, options: nil)
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    func playVideo() {
        player?.seek(to: kCMTimeZero)
        player?.play()
        self.avPlayer?.frame = self.imageView.bounds
        self.avPlayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.trailingSpace.constant = -20
        
    }
    
    init(frame: CGRect, videoUrl:String?) {
        super.init(frame: frame)
       Bundle.main.loadNibNamed("SwipeView", owner: self, options: nil)
        lblPrice.transform = CGAffineTransform(rotationAngle: CGFloat(-M_PI_4))
        view.frame = self.bounds
        self.addSubview(view)
        self.initializations()
        
        if let url = videoUrl {
            let videoURL = URL(string: url.convertIntoViteURL())
            let player = AVPlayer(url: videoURL!)
            let playerLayer = AVPlayerLayer(player: player)
            self.avPlayer = playerLayer
            self.player = player
            self.player?.isMuted = true
            
            //Notification Block for loop
            NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: nil, using: { (notification) in
                let t1 = CMTimeMake(3, 100)
                player.seek(to: t1)
                player.play()
            })
            self.imageView.layer.addSublayer(playerLayer)
        } else {
            self.trailingSpace.constant = 0
        }
        
    }
    
    
    func initializations()  {
        self.imgProfile.layer.cornerRadius = 30.0
        self.imgProfile.layer.borderWidth  = 2
        self.imgProfile.layer.borderColor  = UIColor.white.cgColor
        self.imgProfile.clipsToBounds      = true
        self.layer.cornerRadius            = 0.0
        self.clipsToBounds                 = true
    }
    
    @IBAction func viewProfileAction(sender: AnyObject) {
        if let viewProfile = viewProfile {
            viewProfile()
        }
    }
    
    @IBAction func shareEvent(sender: AnyObject) {
        if let shareEvent = shareEvent {
            shareEvent()
        }
    }
}
