//
//  ViewController.swift
//  Vite
//
//  Created by EeposIT_X on 2/1/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//
import UIKit
import Koloda
import MapKit
import SDWebImage
import Firebase
import FirebaseAuth
import pop
import Alamofire
import Stripe
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import Branch
import AVFoundation
import MIBadgeButton_Swift
import SlideMenuControllerSwift
import Hero

enum VersionError: Error {
    case invalidBundleInfo(String)
    case invalidResponse(String)
}


class ViewController: WrapperController, STPPaymentCardTextFieldDelegate, KolodaViewDataSource, KolodaViewDelegate {
   
   
    
    @IBOutlet weak var kolodaView: KolodaView!
    @IBOutlet weak var noEventsImage: UIImageView!
    @IBOutlet weak var btnCreateEvent: UIButton!
    @IBOutlet weak var btnMyVites: MIBadgeButton!
    @IBOutlet var btnMessage: MIBadgeButton!
    @IBOutlet weak var btnEventAndVip: UIButton!
    
    var message: String?
    let indicator = ActivityIndicator()
    var frame:CGRect?
    var revealToggle = false
    let paymentTextField = STPPaymentCardTextField()
    var userConnectedToStripe = false
    var countApi = 0
    var page = Int()
    var locationManager:LocationManager? = LocationManager()
    var user = UserDefaultsManager()
    
    
    var isRevealControllerON:Bool {
        get {
            return revealToggle
        } set {
            if newValue {
                self.kolodaView.isUserInteractionEnabled = false
            } else {
                self.kolodaView.isUserInteractionEnabled = true
            }
            self.revealToggle = newValue
        }
    }
    var events = [VEvent]()
    var eventsForRate = [VEvent]()
    var imageIndex = 0
    var imageArr = ["Screen1","Screen2","Screen3","Screen4","Screen5","Screen6"]
    
    
    //MARK: - Class methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkForUpdate()
        kolodaView.hero.id = "KOLODA"
        getNearVite()
        VServices.updateDeviceToken(nil, failureBlock: nil)
        setupNotifications()
        kolodaView.dataSource = self
        kolodaView.delegate   = self
        if !UserDefaultsManager().isBusinessAccountActivated{
            if FilterEventParameter.shared.filterEvent.count > 0  && UserDefaultsManager().userPremiumStatus == true {
                print(FilterEventParameter.shared.filterEvent)
                loadFilterEventByLocation()
            } else {
                self.makeInitialServiceRequests()
            }
        }
        self.getStripeAccountStatus()
        self.observeBranchNotification()
        NotificationCenter.default.addObserver(self, selector: #selector(self.switchAccount(notification:)), name: NSNotification.Name(rawValue: "switchAccount"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.sendDeviceToken), name: NSNotification.Name(rawValue: "kTokenReceived"), object: nil)
       
    }
    
    // to get the notification count and display it in badge of appicon
    
    func getBatchCount() {
        NotificationServices.getNotificationCount({ (count) in
            let application = UIApplication.shared
            application.applicationIconBadgeNumber = count
        }) { (msg) in
            print(msg)
        }
    }
    
    func getNearVite(){
        VPostMediaViteFeedService.getNearVite(successBlock: { (id, title) in
            self.user.nearViteId = id
            self.user.nearViteTitle = title
            // when there is no near vite
            if !id.isEmpty {
                if let cameraShownForPostMediaToNearVite = UserDefaultsManager().nearViteCameraShown {
                    if !cameraShownForPostMediaToNearVite {
                        let controller  = Route.showViteFeedMedia
                        self.present(controller, animated: true, completion: nil)
                    }
                }
                self.user.nearViteCameraShown = true
            } else {
                self.user.nearViteCameraShown = false
            }
        }) { (msg) in
            print(msg)
        }
    }
    
    @objc func sendDeviceToken(){
        VServices.updateDeviceToken(nil, failureBlock: nil)
    }
    
    @objc func switchAccount(notification: NSNotification) {
        //showNoEventScreen()
        print(self.events.count)
        self.noEventsImage.isHidden = UserDefaultsManager().isBusinessAccountActivated || self.events.count == 0 ? false : true
        kolodaView.isHidden = UserDefaultsManager().isBusinessAccountActivated || self.events.count == 0 ? true : false
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
           getBatchCount()
        showNoEventScreen()
        if countApi == 1{
            if self.eventsForRate.count > 0 {
                popUpRatingView()
            }
        }
        FacebookEventHelper().checkForFacebookEvents { [weak self] (status, message) in
            if status {
                self?.alertFbEvent()
            } else {
                print(message)
            }
            if UserDefaultsManager().isBusinessAccountActivated{
                self?.kolodaView.isHidden = true
                self?.noEventsImage.isHidden = false
            } else {
                self?.kolodaView.isHidden = false
            }
        
        }
        
        // Read the deep link paramters from the link
        let sessionParams = Branch.getInstance().getLatestReferringParams()
        //let itemId = sessionParams["item_id"]
        guard let isItemPresent = sessionParams?["isFromHome"] as? Bool else { return }
        if isItemPresent {
            // from here, you'd load the appropriate item from the item id
            //self.respondToRequestFromBranch(User().userFBID!, eventID: sessionParams["eventID"] as! String)
            showEventDetailFromBranch(eventID: sessionParams!["eventID"] as! String)
            //print("Deep linked to page from Branch link with item id: %@", sessionParams["item_id"])
        } else {
            showEventDetailFromBranch(eventID: sessionParams!["eventID"] as! String)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        //NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    func popUpRatingView(){
        let controller = Route.rateViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.eventForRate = self.eventsForRate
        self.present(controller, animated: true, completion: nil)
    }
    
    func observeBranchNotification(){
        
        let nc = NotificationCenter.default
        nc.addObserver(self,
                       selector: #selector(self.handleBranchEventShareNotification),
                       name: NSNotification.Name(rawValue: kNotificationBranchShare),
                       object: nil)
        nc.addObserver(self,
                       selector: #selector(self.handleBranchEventShareNotification),
                       name: NSNotification.Name(rawValue: kNotificationBranchEventShare),
                       object: nil)
    }
    
    @objc func handleBranchEventShareNotification(notification:NSNotification) -> Void {
        self.indicator.start()
        guard let userInfo = notification.userInfo,
            let eventID     = userInfo["eventID"]    as? String else {
                print("No userInfo found in notification")
                return
        }
        showEventDetailFromBranch(eventID: eventID)
    }
    
    func handleBranchShareNotification(notification:NSNotification) -> Void {
        self.indicator.start()
        guard let userInfo = notification.userInfo,
            let eventID     = userInfo["eventID"]    as? String else {
                print("No userInfo found in notification")
                return
        }
        
        self.respondToRequestFromBranch(fbID: UserDefaultsManager().userFBID!, eventID: eventID)
    }
    
    func showEventDetailFromBranch(eventID:String){
        VEventWebService.getEventDetail(eventID, successBlock: {[weak self] (eventDetail) in
            self?.indicator.stop()
            let controller = Route.EventDetailVC
            controller.eventDetail = eventDetail
            controller.isFromViteFeed = false
            self?.show(controller, sender: nil)
        }) { [weak self](message) in
            self?.indicator.stop()
            showAlert(controller: self!, title: localizedString(forKey: "NETWORK_ERROR"), message: message)
        }
        
    }
    
    func respondToRequestFromBranch(fbID:String,eventID:String){
        //Invite self to the shared event
        EventServices.respondToRequest(status: true,
                                            userFbID: fbID,
                                            eventID: eventID,
                                            completionBlock: {
                                               [weak self] (status) -> () in
                                                if status {
                                                    self?.indicator.stop()
                                                    } else {
                                                    self?.indicator.stop()
                                                    print("Failed to respond to request")
                                                }
        })
    }
    
    func showNoEventScreen(){
        imageIndex = Int(arc4random_uniform(UInt32(imageArr.count)))
        noEventsImage.image = UIImage(named: imageArr[imageIndex])
    }
    
    @IBAction func goToCreateEvent(_ sender: Any) {
        if imageIndex == 4 {
            return
        }

       let controller = Route.CreateEvent
        controller.isFromSideMenu = true
        controller.userConnectedToStripe = userConnectedToStripe
        let nav = UINavigationController.init(rootViewController: controller)
        self.slideMenuController()?.changeMainViewController(nav, close: true)
        
    }
    
    func goToAddVip(){
        self.navigationController?.show(Route.Vip, sender: nil)
    }
    
    
    func makeInitialServiceRequests() {
            if (UserDefaultsManager().userPremiumStatus != true){
                UserDefaultsManager.saveUserLocationToUserDefaults(locManager: self.locationHelper)
            }
            locationManager?.getUserLocation()
            locationManager?.fetchedUserLocation = {
                location in ()
                let user = UserDefaultsManager()
                user.userCurrentLatitude  = String(location.latitude)
                user.userCurrentLongitude = String(location.longitude)
            }
            
            self.loadFilteredEvents()
            self.authenticateUber()
    }
    
    func authenticateUber() {
        //To Do :
//        Alamofire.request("https://login.uber.com/oauth/v2/authorize?client_id=5sM95iho5DcCMM9GVtaUv2-bIAC1T9pH&response_type=pin", method: .get, parameters: nil, encoding: nil, headers: nil).responseString { (response) in
//
//        }
        //  OLd
//        Alamofire.request(.GET, "https://login.uber.com/oauth/v2/authorize?client_id=5sM95iho5DcCMM9GVtaUv2-bIAC1T9pH&response_type=pin").responseString() {
//            response in
//        }
    }
  
  func showProfileOfUser(fbID:String) {
    let controller                       = Route.profileViewController
    controller.fbId                      = fbID
    controller.isForViewingOthersProfile = true
    let nav = UINavigationController.init(rootViewController: controller)
     self.present(nav, animated: true, completion: nil)
  
}



  func showBusinessprofile(businessDetail: BusinessAccountUserModel){
    let controller                       = Route.updateAccountForBusiness
    controller.isForViewingOthersBusinessProfile = true
    controller.businessAccounts = businessDetail
    if let businessId = businessDetail.entityId{
        controller.businessID = businessId
    }
    let nav = UINavigationController.init(rootViewController: controller)
     self.present(nav, animated: true, completion: nil)
}
  
  
    //MARK: - Methods
    
    // Send request for event
    func userResponseToCard(event: VEvent, userResponse: UserViteResponse){
        
        VEventWebService.setUserResponseForEvent(event, userResponse: userResponse, successBlock: {
            if userResponse == .Requested { 
                let evnt     = NEvent(name: event.title!, uid: event.uid!, imageUrl: event.imageUrl!.convertIntoViteURL())
                let sender    = UserDefaultsManager.getActiveUserForNotification()
                let receiver = NReceiver(uid: String(describing: event.organizer!.uid!))
                if let businessUser = event.business {
                    let businessUser =  NReceiver(uid: String(businessUser.entityId!))
                    NotificationService().triggerNotificationWithEvent(.RequestReceived, event: evnt, sender: sender, receiver: businessUser)
                }
                NotificationService().triggerNotificationWithEvent(.RequestReceived, event: evnt, sender: sender, receiver: receiver)
            } else {
                print("REJECTED")
            }
            }, failureBlock: { (message) in
                print("Failed to respond to event: Error: \(message)")
        })
        
    }


    //parameter to check organizer's stripe status
    func getStripeAccountStatus(){
        let fbID = UserDefaultsManager().userFBID!
        
        WebService.request(method: .get, url: KAPICheckStripeStatus + fbID, parameter: nil, header: nil, isAccessTokenRequired: true, success: {  [weak self] (response) in
            guard let response = response.result.value as! [String: AnyObject]? else {
                                return
                            }
            
                            guard let success = response["success"] as? Bool else {
                                return
                            }
            
                            if(success){
                                if let cardStatus = response["params"]!["cardStatus"] as? Bool{
                                    if (cardStatus != false){
                                        self?.userConnectedToStripe = true
                                    }
                                }
                                let accountStatus = response["params"]!["accountStatus"] as! Bool
                                if accountStatus{
                                    UserDefaults.standard.set(true, forKey: UserDefaultKeys.isStripeConnected)
                                }
                            }
        }) { [weak self] (message, apiError) in
            showAlert(controller: self!, title: "", message: message)
        }
    }
    
    func getPaypalEmailStatus(event: VEvent) {
        guard let organizerFbId = event.organizer?.uid else {
            return
        }
        PaypalWebService.checkPaypalStatus(fbId: String(describing: organizerFbId), { (paypalEmailStatus) in
            if paypalEmailStatus {
                let controller = Route.makePaymentViewController
                controller.event = event
                self.present(controller, animated: true, completion: nil)
            } else {
                //when it is a paid event
                let controller = Route.paymentViewController
                controller.eventDetail = event
                let nav = UINavigationController(rootViewController: controller)
                self.show(nav, sender: nil)
            }
        }) { (msg) in
            print(msg)
        }
    }
    
    
    // alert to ask user if wants to add facebook event to vite
    func alertFbEvent(){
        let alert = UIAlertController(title: localizedString(forKey: "ADD_FB_EVENTS_Q"),
                                      message: localizedString(forKey: "ADD_FB_EVENTS"),
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Yes",
                                     style: .default,
                                     handler: {
                                       [weak self] (action:UIAlertAction) -> Void in
                                        self?.loadFbEventController()
        })
        
        let cancelAction = UIAlertAction(title: "No",
                                         style: .cancel) {
                                         (action: UIAlertAction) -> Void in
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        self.present(alert,
                                   animated: false,
                                   completion: nil)
    }
    
    //Show facebook event view controller if user accepts.
    func loadFbEventController(){
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "fbEventVc") as! UINavigationController
        controller.view.backgroundColor = UIColor.clear
        self.present(controller, animated: true, completion: nil)
    }
    
    func kolodaDisplayToggle() {
        if self.events.count > 0 {
            self.kolodaView.isHidden    = false
            self.noEventsImage.isHidden = true
        } else {
            self.kolodaView.isHidden    = true
            self.noEventsImage.isHidden = false
        }
    }
    
//    func revealController(revealController: SWRevealViewController!,  willMoveToPosition position: FrontViewPosition){
//        if(position == .Right) {
//            self.isRevealControllerON = true
//        } else {
//            self.isRevealControllerON   = false
//        }
//    }


    //MARK :- Koloda Datasource
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return self.events.count
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let vEvent = self.events[Int(index)]
        var swipe:SwipeView!
        
        if let profileVideoUrl = vEvent.profileVideoUrl, !profileVideoUrl.isEmpty {
            swipe = SwipeView(frame:CGRect.zero,videoUrl: profileVideoUrl )
            swipe.playVideo()
            
        } else {
            swipe = SwipeView(frame: CGRect.zero, videoUrl: nil)
        }
        
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "YYYY-MM-dd HH:mm:ss"
        
        let fullDate = dateFormatter.date(from: vEvent.date! + " " + vEvent.startTime!)
        swipe.lblTimeStamp.text = timeAgoSince(date: fullDate!)
        swipe.lblTitle.text = vEvent.title
        
        if let organizerProfileImage = (vEvent.business == nil ? vEvent.organizer?.profileImageUrl?.convertIntoViteURL() : vEvent.business?.profileImage?.convertIntoViteURL()) {
            swipe.imgProfile.sd_setImage(with: URL(string: organizerProfileImage), placeholderImage: UIImage(named: "ProfilePlaceholder.png"))
        }
        
        let imageUrl = vEvent.imageUrl?.convertIntoViteURL()
        swipe.imageView.sd_setImage(with: URL(string: imageUrl!), placeholderImage: nil)
        
        // For the paid Events
        if vEvent.paidEvent != nil{
            let paidEvent = vEvent.paidEvent!
            if (paidEvent){
                swipe.lblPrice.text = "$" + String(describing: vEvent.eventPrice!)
            } else {
                swipe.imgPriceTag.isHidden = true
                swipe.lblPrice.isHidden = true
            }
        } else {
            swipe.imgPriceTag.isHidden = true
            swipe.lblPrice.isHidden = true
        }
        
        swipe.viewProfile = {
            let uid = String(describing: vEvent.organizer!.uid!)
            if vEvent.business == nil {
                self.showProfileOfUser(fbID: uid)
            } else {
                self.showBusinessprofile(businessDetail: vEvent.business!)
            }
        }
        
        swipe.shareEvent = {
            if let eventTitle = vEvent.eventTitle{
                BranchShareHelper.ShareEventDetail(self, eventID: vEvent.uid!, eventTitle: eventTitle, eventDesc: vEvent.detail!, shareText: eventTitle, imageURL: imageUrl!)
            }
        }
        
        if  let latitude  = UserDefaultsManager().userCurrentLatitude,
            let longitude = UserDefaultsManager().userCurrentLongitude {
            let currentlocation    = CLLocation(latitude: Double(latitude)!, longitude:  Double(longitude)!)
            let eventLocation      = CLLocation(latitude: Double(vEvent.location!.latitude!)!, longitude:  Double(vEvent.location!.longitude!)!)
            swipe.lblDistance.text = String(getDistanceInMile(userCurrentLocation: currentlocation, eventLocation: eventLocation)) + " Miles Away"
        }
        
        swipe.celebName.text = vEvent.organizer?.fullName!
        swipe.celebName.textColor = UIColor.white
        swipe.lblTitle.textColor = UIColor.white
        swipe.celebStarImgView.isHidden = true
        swipe.celebStarLargeImgView.isHidden = true
        swipe.celebStarImgWidthConstraint.constant = 0
        swipe.horrizontalSpaceBetnTitleAndStar.constant = 0
        
        //Setting ogranizer/ business holder's name and images
        if vEvent.business == nil {
            // for verified celebrity
            if let verifiedCeleb = vEvent.organizer?.verificationStatus{
                swipe.celebName.text = vEvent.organizer?.fullName!
                if verifiedCeleb{
                    swipe.lblTitle.textColor = UIColor.colorFromRGB(rgbValue: 0xFBCB2E)
                    swipe.celebName.textColor = UIColor.colorFromRGB(rgbValue: 0xFBCB2E)
                    swipe.celebStarImgView.isHidden = false
                    swipe.celebStarLargeImgView.isHidden = false
                    swipe.celebStarImgWidthConstraint.constant = 16
                    swipe.horrizontalSpaceBetnTitleAndStar.constant = 8
                }
            }
            // for verified elite
            if let verifiedElite = vEvent.organizer?.eliteVerificationStatus {
                if verifiedElite{
                    swipe.celebStarLargeImgView.image = UIImage(named: "EliteEventCrown")
                    swipe.celebStarLargeImgView.isHidden = false
                    swipe.celebStarImgWidthConstraint.constant = 20
                    swipe.horrizontalSpaceBetnTitleAndStar.constant = 8
                }
            }
        } else {
            // for business account
            swipe.celebName.text = vEvent.business?.businessName
        }
        swipe.playVideo()
        return swipe
    }
    
    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
         return Bundle.main.loadNibNamed("OverlayView", owner: self, options: nil)![0] as? OverlayView
    }
    
    //MARK: - Koloda Delegates
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return DragSpeed.moderate
    }
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        let event = self.events[Int(index)]
                let userResponse:UserViteResponse = direction == SwipeResultDirection.right ? .Requested : .Rejected
                if userResponse == .Requested {
                    if (event.paidEvent == true){
                      getPaypalEmailStatus(event: event)
                    } else {// if it is not paid event
                        self.userResponseToCard(event: event, userResponse: userResponse)
                    }
                } else {
                    print("Rejected")
                }
    }
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        self.kolodaView.isHidden    = true
                self.noEventsImage.isHidden = false
                if FilterEventParameter.shared.filterEvent.count > 0  && UserDefaultsManager().userPremiumStatus == true {
                    loadFilterEventByLocation()
                }
    }
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        let event = self.events[Int(index)]
                let controller = Route.EventDetailVC
                controller.eventDetail = event
                controller.event = self.events
                let nav = UINavigationController(rootViewController: controller)
                nav.hero.isEnabled = true
                self.present(nav, animated: true, completion: nil)
    }
}


//MARK: Notifications
extension ViewController {
    
    func setupNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.getFilteredEvents), name: NSNotification.Name(rawValue: kNotificationViteNowReceived), object: nil)
        //notificationCenter.addObserver(self, selector: #selector(self.loadFilteredEvents), name: kNotificationViteNowReceived, object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.directToMessageCenter), name: NSNotification.Name(rawValue: kNotificationDirectToMsgCenter), object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.directToMyVites), name: NSNotification.Name(rawValue: kNotificationDirectToMyVites), object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.directToMyVip), name: NSNotification.Name(rawValue: kNotificationDirectToMyVip), object: nil)
         notificationCenter.addObserver(self, selector: #selector(self.directToAddMyVip), name: NSNotification.Name(rawValue: kNotificationDirectToAddMyVip), object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.directToMyAttendee), name: NSNotification.Name(rawValue: kNotificationDirectToMyAttendee), object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.kolodaAnimationFinished), name: NSNotification.Name(rawValue: "kKolodaAnimationFinished"), object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.directToMyComment), name: NSNotification.Name(rawValue: kNotificationDirectToComment), object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.getFilteredEvents), name: NSNotification.Name(rawValue: "ShowEventDetailAtHome"), object: nil)
        notificationCenter.addObserver(self, selector: #selector(ViewController.filterEventByLocation(notification:)), name: NSNotification.Name(rawValue: "EventByLocation"), object: nil)
        notificationCenter.addObserver(self, selector: #selector(ViewController.setMYCurrentLocation), name: NSNotification.Name(rawValue: "MyCurrentLocation"), object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.requestVite), name: NSNotification.Name(rawValue: "requestVite"), object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.directToMyProfile), name: NSNotification.Name(rawValue: kNotificationDirectToMyProfile), object: nil)
        
    }
    
    //MARK:  Filter event by location
    @objc func setMYCurrentLocation(){
        FilterEventParameter.shared.filterEvent = [String:AnyObject]()
        if !UserDefaultsManager().isBusinessAccountActivated{
            self.makeInitialServiceRequests()
        }
        
    }
    
    @objc func filterEventByLocation(notification : NSNotification){
        FilterEventParameter.shared.filterEvent = notification.userInfo as! [String:AnyObject]
        print(FilterEventParameter.shared.filterEvent)
        loadFilterEventByLocation()
        
    }
    
    @objc func requestVite() {
        self.view.isUserInteractionEnabled = false
        self.view.makeToast(message: localizedString(forKey: "REQUESTED_TO_EVENT"))
        loadFilteredEvents()
    }
    
    func loadFilterEventByLocation() {
        page = +1
        FilterEventParameter.shared.filterEvent["pageNo"] = page as AnyObject
        let param = FilterEventParameter.shared.filterEvent
        VEventWebService.getEventListWithLocation(parameter: param, successBlock: { [weak self] (eventList) in
            self?.events = eventList!
            self?.kolodaDisplayToggle()
            self?.kolodaView.resetCurrentCardIndex()
            
        }) {[weak self](message) in
            showAlert(controller: self!, title: localizedString(forKey: "ALERT"), message: message)
            
        }
    }
    
    
    func loadFilteredEvents(){
        VEventWebService.getFilteredEvents({ [weak self] (eventList , eventForRate) in
            self?.events = eventList
            self?.eventsForRate = eventForRate
            self?.kolodaDisplayToggle()
            self?.kolodaView.resetCurrentCardIndex()
            self?.apiCalled()
            self?.view.isUserInteractionEnabled = true
            }, failureBlock: {
           [weak self]     message in
                self?.view.isUserInteractionEnabled = true
                checkIfUserLoggedInFromAnotherDevice(msg: message, ctrl: self!)
        })
    }
    func apiCalled(){
        countApi = countApi + 1
        if countApi == 1 {
            viewDidAppear(true)
        }
        countApi = 0
    }
    
    func getEventDetail(){
        let userDefault = UserDefaults.standard
        let eventId = userDefault.object(forKey: "eventIdFromNotification") as? String
        VEventWebService.getEventDetail(eventId!, successBlock: { [weak self](eventDetail) in
            self?.pushNotificationViewed(eventID: eventId!)
            if eventDetail.eventsUsers?.requestedDate != nil {
                showAlert(controller: self!, title: "", message: localizedString(forKey: "ALREADY_SENT_REQUEST"))
            } else {
                self?.events = (self?.events.filter({ (event) -> Bool in
                    if event.uid == eventId {
                        return false
                    } else {
                        return true
                    }
                }))!
                self?.events.insert(eventDetail, at: 0)
                self?.kolodaDisplayToggle()
                self?.kolodaView.resetCurrentCardIndex()
            }
            
        }) { [weak self] (message) in
            showAlert(controller: self!, title: "", message: message)
        }
        
    }
    
    /*This function  to display event when user clicks the notification from vite now*/
    
    @objc func getFilteredEvents(){
        self.navigationController?.popToRootViewController(animated: true)
        VEventWebService.getFilteredEvents({ [weak self] (eventList,eventForRate) in
            self?.events = eventList
            self?.getEventDetail()
            }, failureBlock: {
              [weak self]  message in
                showAlert(controller: self!, title: "", message: message)
                
                // self.tinder.removeFromSuperview()
        })
        
    }
    
    func pushNotificationViewed(eventID: String){
        let url = KAPIPushNotificationViewed + eventID + "/" + UserDefaultsManager().userFBID!
        WebService.request(method: .get, url: url, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            
        }) { [weak self] (message, apiError) in
            showAlert(controller: self!, title: "", message: message)
        }

    }
    
    
    @objc func directToMyVites(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kNotificationDirectToMyVites), object: nil)
        UserDefaults.standard.set(false, forKey: UserDefaultKeys.myViteNotification)
        let nav = UINavigationController(rootViewController: Route.myVites)
        self.show(nav, sender: nil)
     }
    
    @objc func directToMyComment(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kNotificationDirectToComment), object: nil)
        UserDefaults.standard.set(false, forKey: UserDefaultKeys.myCommentNotification)
           if let eventId = UserDefaults.standard.object(forKey: "eventIdFromNotification") as? String,
            let mediaType = UserDefaults.standard.object(forKey: "mediaTypeFromNotification") as? String,
            let mediaUrl = UserDefaults.standard.object(forKey: "mediaUrlFromNotification") as? String,
            let feedId =   UserDefaults.standard.object(forKey:"feedIdFromNotification") as? String {
            VEventWebService.getEventDetail(eventId, successBlock: { [weak self] (eventDetail) in
                
                let commentController = Route.SwipeFullScreenViewController
                commentController.isFromNotification = true
                commentController.mediaURL = mediaUrl
                commentController.mediaType = mediaType
                commentController.feedId = feedId
                commentController.detailEvent = eventDetail
                let nav = UINavigationController(rootViewController: commentController)
                self?.show(nav, sender: nil)
            }) {[weak self] (message) in
               showAlert(controller: self!, title: " ", message: message)
            }
        }
    }
    
    @objc func directToMyVip(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kNotificationDirectToMyVip), object: nil)
        UserDefaults.standard.set(false, forKey: UserDefaultKeys.myVipNotification)
        let nav = UINavigationController(rootViewController: Route.Vip)
        self.slideMenuController()?.changeMainViewController(nav, close: true)
        
    }
    
    @objc func directToMyProfile(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kNotificationDirectToMyProfile), object: nil)
        let nav = UINavigationController(rootViewController: Route.profileViewController)
        self.show(nav, sender: nil)
        
    }
    
    
    @objc func directToAddMyVip(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kNotificationDirectToMyVip), object: nil)
        UserDefaults.standard.set(false, forKey: UserDefaultKeys.myVipNotification)
        let nav = UINavigationController(rootViewController: Route.addToVip)
        self.present(nav, animated: true, completion: nil)
        
    }
    
    @objc func directToMessageCenter(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kNotificationDirectToMsgCenter), object: nil)
        UserDefaults.standard.set(false, forKey: UserDefaultKeys.msgCenterNotification)
        let controller = Route.MessageCenter
        controller.isFromSideMenu = false
        let nav = UINavigationController(rootViewController: controller)
        self.slideMenuController()?.changeMainViewController(nav, close: true)
    }
    
    @objc func directToMyAttendee(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kNotificationDirectToMyAttendee), object: nil)
        let controller = Route.scanTicketViewController
        let userDefault = UserDefaults.standard
        
        if let eventIdFromNotification = userDefault.object(forKey: "eventIdFromScreenshotNotification") as? String {
            controller?.eventIdFromNotification = eventIdFromNotification
            controller?.isFromSideMenuToScanTicket = false
        }
        let nav = UINavigationController(rootViewController: controller!)
        self.slideMenuController()?.changeMainViewController(nav, close: true)
    }
    
    
    @objc func kolodaAnimationFinished() {
        self.kolodaView.isUserInteractionEnabled = !self.isRevealControllerON
    }
    
    func shareWithActivityController(eventTitle: String, eventImageView: UIView) {
        var sharingItems = [AnyObject]()
        sharingItems.append(eventTitle as AnyObject )
        let url = NSURL(string: "https://itunes.apple.com/us/app/vite-exclusive-events/id1087246453?mt=8")
        sharingItems.append(url!)
        
        //disable features for sharing the image of event on facebook
        
        let eventImageView = imageWithView(eventImageView)
        sharingItems.append(eventImageView)
        
        let activityVC = UIActivityViewController(activityItems: sharingItems, applicationActivities: nil)
        activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
        self.present(activityVC, animated: true, completion: nil)
    }
}

//MARK: FORCE UPDATE
extension ViewController {
    func isUpdateAvailable() throws -> (Bool,String) {
        guard let info = Bundle.main.infoDictionary,
            var currentVersion = info["CFBundleShortVersionString"] as? String,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
                throw VersionError.invalidBundleInfo("Invalid bundle info")
        }
        let data = try Data(contentsOf: url)
        guard let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
            throw VersionError.invalidResponse("Invalid Response")
        }
        
        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
            let versionToCompare = version.replacingOccurrences(of: ".", with: "")
            currentVersion = currentVersion.replacingOccurrences(of: ".", with: "")

            print("version in app store", versionToCompare,currentVersion);
            
            return (versionToCompare > currentVersion,version)
        }
        throw VersionError.invalidResponse("Invalid Response")
    }
    
    func checkForUpdate() {
        
        DispatchQueue.global().async {
            do {
                let update = try self.isUpdateAvailable()
                print("update",update)
                DispatchQueue.main.async {
                    
                    if update.0{ // update.0 checks if the current version is same as appstore version
                        self.popupUpdateDialogue(appStoreVersion: update.1); // update.1 gives the appstore version
                    }
                }
            } catch {
                print(error)
            }
        }
    }
    
    func popupUpdateDialogue(appStoreVersion: String){
        let alertMessage = "A new version \(appStoreVersion) of vite is available. Please update to the latest version."
        let alert = UIAlertController(title: "New Version Available", message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        let okBtn = UIAlertAction(title: "Update", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if let url = URL(string: "https://itunes.apple.com/us/app/vite-exclusive-events/id1087246453?mt=8"),
                UIApplication.shared.canOpenURL(url){
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        })
        alert.addAction(okBtn)
        self.present(alert, animated: true, completion: nil)
        
    }
}



