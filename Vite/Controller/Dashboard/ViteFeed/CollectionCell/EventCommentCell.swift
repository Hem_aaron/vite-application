//
//  EventCommentCell.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 8/2/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class EventCommentCell: UITableViewCell {

    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
}
