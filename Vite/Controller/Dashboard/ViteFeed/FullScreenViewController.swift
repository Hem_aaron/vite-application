//
//  FullScreenViewController.swift
//  Vite
//
//  Created by Hem Poudyal on 4/27/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import AVFoundation

class FullScreenViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    var mediaURL = String()
    var mediaType = String()
    var player: AVPlayer?
    
    override func viewDidLoad() {
        if mediaType == "VIDEO"{
                let videoURL = NSURL(string: mediaURL.convertIntoViteURL())
            self.player = AVPlayer(url: videoURL! as URL)
                let playerLayer = AVPlayerLayer(player: self.player)
                playerLayer.frame = self.imageView.bounds
            playerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
                self.imageView.layer.addSublayer(playerLayer)
            self.player?.isMuted = false
                self.player?.play()
                //self.player?.addObserver(self, forKeyPath: "status", options: [], context: nil)
            //Notification Block for loop
            NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem, queue: nil, using: { (notification) in
                let t1 = CMTimeMake(3, 100)
                self.player?.seek(to: t1)
                self.player?.play()
            })
            }
        else{
            self.imageView.sd_setImage(with: URL(string: mediaURL.convertIntoViteURL()))
        }
    }
    
//    override func  observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
//        if object?.isEqual(player) == true && keyPath == "status" {
//            if player?.status == AVplayer{
//                
//            }
//        }
//    }
    
    @IBAction func btnCrossAction(_ sender: AnyObject) {
        self.player?.isMuted = true
        self.player = nil
        self.dismiss(animated: true) { 
        }
    }
}
