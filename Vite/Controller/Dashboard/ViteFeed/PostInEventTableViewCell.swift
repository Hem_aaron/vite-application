//
//  PostInEventTableViewCell.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 5/10/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit

class PostInEventTableViewCell: UITableViewCell {

    @IBOutlet weak var lblEventTitle: UILabel!
    @IBOutlet weak var selectEvent: UIButton!
    var tickEvent: (() ->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func selectEvent(_ sender: Any) {
        if let te = tickEvent {
            te()
        }
    }
}
