//
//  PostInViteTableViewCell.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 5/10/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit

class PostInViteTableViewCell: UITableViewCell {

    @IBOutlet weak var selectVite: UIButton!
    @IBOutlet weak var lblViteTitle: UILabel!
    
    var tickVite: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func selectVite(_ sender: Any) {
        if let tv = tickVite {
            tv()
        }
    }
}
