//
//  SelectEventOrViteToPostViewController.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 5/10/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit
enum sectionIndex: Int {
    case vite = 0
    case event = 1
}

enum sectionTitle: String {
    case vite = "MY VITES"
    case event = "EVENTS"
}

class SelectEventOrViteToPostViewController: UIViewController {

    @IBOutlet weak var lblPostingTo: UILabel!
    @IBOutlet weak var tblPost: UITableView!
    var nearViteListsArr = [VVite]()
    var eventsArr = [VVite]()
    let indicator = ActivityIndicator()
    let cursor = VCursor()
    var mediaId : String?
    var eventOrViteId: String?
      @IBOutlet weak var progressView: UIProgressView!
    
    var caption = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        print(caption)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            guard let id = self.mediaId else {
                return
            }
            VMediaWebService.deleteMedia(id, successBlock: { (msg) in
                print(msg)
            }, failureBlock: { (msg) in
                print(msg)
            })
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
         self.fetchEvents(shouldReset: true)
    }
    
    
    @IBAction func postMedia(_ sender: Any) {
        guard let eventORViteId = eventOrViteId  else {
            showAlert(controller: self, title: "", message: localizedString(forKey: "SELECT_ONE_EVENT"))
            return
        }
         self.view.isUserInteractionEnabled = false
        indicator.start()
        if let id = mediaId{
            let parameter : [String: AnyObject] = [
                "mediaId" :  id as AnyObject,
                "eventId" : eventORViteId as AnyObject,
                "caption" : caption as AnyObject
            ]
            VPostMediaViteFeedService.postMediaToViteFeed(parameter, successBlock: { (successMsg) in
                self.view.window!.rootViewController?.dismiss(animated: false, completion: {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "postMediaSuccessMsg"), object: successMsg)
                    self.indicator.stop()
                    self.view.isUserInteractionEnabled = true
                })
            }, failureBlock: { (msg) in
                self.indicator.stop()
                self.view.isUserInteractionEnabled = true
                showAlert(controller: self, title: "", message: msg)
            })
        }
    }
    
    func setTableViewHeightWithContentSize(){
        if(self.tblPost.contentSize.height < self.tblPost.frame.height){
            var frame: CGRect = self.tblPost.frame;
            frame.size.height = self.tblPost.contentSize.height + 20;
            self.tblPost.frame = frame;
            self.tblPost.layer.cornerRadius = 10
            self.tblPost.clipsToBounds = true
        }
    }

}

//MARK: SERVICE CALL
extension SelectEventOrViteToPostViewController {
    func getNearViteLists() {
        VPostMediaViteFeedService.getNearViteList(successBlock: { (lists) in
            self.nearViteListsArr = lists
            self.tblPost.reloadData()
            self.setTableViewHeightWithContentSize()
        }) { (msg) in
            print(msg)
        }
    }
    
    func fetchEvents(shouldReset reset:Bool) {
        indicator.tintColor = UIColor.gray
        indicator.start()
        if reset {
            self.cursor.reset()
        }
        
        EventServices.getEventListWithPaginationForPosting(cursor.nextPage(), pageSize: cursor.pageSize, successBlock: { [weak self] (eventList,totalCount)  in
            if reset {
                self?.eventsArr.removeAll()
            }
            self?.eventsArr += eventList
            self?.cursor.totalCount = totalCount.intValue
            self?.cursor.totalLoadedDataCount = (self?.eventsArr.count)!
              self?.getNearViteLists()
            self?.indicator.stop()
            }, failureBlock:{[weak self] _ in ()
                self?.indicator.stop()
        })
    }
}


extension SelectEventOrViteToPostViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == sectionIndex.vite.rawValue {
            return sectionTitle.vite.rawValue
        }else {
            return sectionTitle.event.rawValue
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == sectionIndex.vite.rawValue {
            return nearViteListsArr.count
        }else {
            return eventsArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == sectionIndex.vite.rawValue {
            return nearViteListsArr.count == 0 ? 0 : 40
        }else {
            return eventsArr.count == 0 ? 0 : 40
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView()
        let headerLabel = UILabel(frame: CGRect(x: 15, y: 15, width: self.view.frame.width, height: 30))
        headerLabel.font = UIFont(name: "Avenir-Heavy", size: 12)
        vw.backgroundColor = UIColor.white
        headerLabel.textColor = UIColor.purple
        if (section == sectionIndex.vite.rawValue) {
            headerLabel.text = sectionTitle.vite.rawValue
        } else {
            headerLabel.text = sectionTitle.event.rawValue
        }
        headerLabel.sizeToFit()
        vw.addSubview(headerLabel)
        return vw
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == sectionIndex.vite.rawValue {
             return 20
        }
       return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == sectionIndex.vite.rawValue {
            let vite = nearViteListsArr[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "vitePostCell") as! PostInViteTableViewCell
            cell.lblViteTitle.text = vite.eventTitle!
            return cell
        } else {
            let event = eventsArr[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "eventPostCell") as! PostInEventTableViewCell
            cell.lblEventTitle.text = event.eventTitle!
            return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == sectionIndex.vite.rawValue {
            let vite = nearViteListsArr[indexPath.row]
            let cell = tableView.cellForRow(at: indexPath) as! PostInViteTableViewCell
            if cell.selectVite.isHidden == false {
                self.lblPostingTo.text = ""
                self.eventOrViteId = nil
                cell.selectVite.isHidden = true
            } else {
                self.lblPostingTo.text = vite.eventTitle!
                self.eventOrViteId = vite.eventId!
                cell.selectVite.isHidden = false
            }
             cell.selectionStyle = .none
        } else {
             let event = eventsArr[indexPath.row]
            let cell = tableView.cellForRow(at: indexPath) as! PostInEventTableViewCell
            if cell.selectEvent.isHidden == false {
                self.lblPostingTo.text = ""
                self.eventOrViteId = nil
                cell.selectEvent.isHidden = true
            } else {
                self.lblPostingTo.text = event.eventTitle!
                self.eventOrViteId = event.eventId!
                cell.selectEvent.isHidden = false
            }
             cell.selectionStyle = .none
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if indexPath.section == sectionIndex.vite.rawValue {
            let cell = tableView.cellForRow(at: indexPath) as! PostInViteTableViewCell
            cell.selectVite.isHidden = true
        } else {
            let cell = tableView.cellForRow(at: indexPath) as! PostInEventTableViewCell
            cell.selectEvent.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == sectionIndex.vite.rawValue   {
            if self.cursor.hasNextPage() && indexPath.row == (self.eventsArr.count - 1) {
                self.fetchEvents(shouldReset: false)
            }
        }
    }
    
}


