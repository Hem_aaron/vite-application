//
//  FullScreenSwipeViewController.swift
//  Vite
//
//  Created by Hem Poudyal on 5/2/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import AVFoundation
import IQKeyboardManagerSwift

class SwipeFullScreenViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var btnEventDetail: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var topSpace: NSLayoutConstraint!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var txtComment: UITextField!
    @IBOutlet weak var tblComment: UITableView!
    @IBOutlet weak var btnAddComment: UIButton!
    @IBOutlet weak var lblCaption: UILabel!
    
    var afterCommentAdded = false
    var commentState = true
    var mediaURL = String()
    var mediaType = String()
    var player: AVPlayer?
    var feedId: String?
    var caption: String?
    var detailEvent : VEvent?
    var isFromNotification = false
    let cursor = VCursor()
    let indicator = ActivityIndicator()
    var mediaCommentListsArr = [VComment]()
    var isVideoLoaded = false
    
    override func viewDidLoad() {
        print(self.navigationController ?? "NO Nav")
        getCommentList(shouldReset: true)
        IQKeyboardManager.sharedManager().enable = true
        // change color of comment text box place holder to white
        txtComment.attributedPlaceholder = NSAttributedString(string: "Write a comment....",
                                                              attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        //set height of tableView row according to content
        tblComment.estimatedRowHeight = 40
        tblComment.rowHeight = UITableViewAutomaticDimension
        NotificationCenter.default.addObserver(self, selector: #selector(self.ViewTopReached), name: NSNotification.Name(rawValue: "kReachedTableViewTop"), object: nil)
        addUpdownGesture()
        if let c = self.caption {
            lblCaption.text = c
        }
        if isFromNotification {
            self.view.gestureRecognizers?.removeAll()
            selectChat(select: true, show: 1)
            commentState = true
        } else {
            commentState = false
            selectChat(select: false, show: 1)
        }
       self.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if !isVideoLoaded{
            if mediaType == "VIDEO"{
                let videoURL = URL(string: mediaURL.convertIntoViteURL())
                self.player = AVPlayer(url: videoURL!)
                let playerLayer = AVPlayerLayer(player: self.player)
                playerLayer.frame = self.imageView.bounds
                playerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
                self.imageView.layer.addSublayer(playerLayer)
                self.player?.isMuted = false
                self.player?.play()
                
                //Notification Block for loop
                NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem, queue: nil, using: { (notification) in
                    let t1 = CMTimeMake(3, 100)
                    self.player?.seek(to: t1)
                    self.player?.play()
                })
            }
            else{
                self.imageView.sd_setImage(with: URL(string: mediaURL.convertIntoViteURL()))
            }
            isVideoLoaded = true
        }
    }
    
    
    //MARK: METHODS
    
    func addUpdownGesture() {
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(gesture:)))
        swipeUp.direction = UISwipeGestureRecognizerDirection.up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(gesture:)))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
                self.ViewTopReached()
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
                if UIDevice().userInterfaceIdiom == .phone {
                    switch UIScreen.main.nativeBounds.height {
                    case 2436:
                        print("iPhone X, Xs")
                        self.topSpace.constant = -(self.view.frame.size.height) - 34
                    case 2688:
                        print("iPhone Xs Max")
                        self.topSpace.constant = -(self.view.frame.size.height) - 34
                    default:
                        self.topSpace.constant = -(self.view.frame.size.height)
                    }
                }
                self.player?.pause()
                UIView.animate(withDuration: 0.6) {
                    self.view.layoutIfNeeded()
                }
            default:
                break
            }
        }
    }
    
    @objc func ViewTopReached() {
        self.topSpace.constant = 0
        UIView.animate(withDuration: 0.6) {
            self.view.layoutIfNeeded()
        }
    }
    
  
    
    func selectChat(select state: Bool, show alphaValue: CGFloat){
        commentView.alpha = alphaValue
        commentView.isHidden = !state
        btnEventDetail.isHidden = state
        tblComment.isHidden = !state
        lblCaption.isHidden = state
        btnComment.setImage(UIImage(named: state ? "commentOn" : "commentOff"), for: .normal)
    }
    
    func maintainTblViewHeight() {
        print(self.tblComment.contentSize.height)
        if self.tblComment.contentSize.height <= 280 {
            self.tblComment.isScrollEnabled = false
            self.tblHeight.constant = self.tblComment.contentSize.height
        } else {
            self.tblComment.isScrollEnabled = true
            self.tblHeight.constant = 280
        }
    }
    
    func addCommentToMedia() {
        let user = UserDefaultsManager()
        var businessId = String()
        self.btnAddComment.isEnabled = false
        if user.isBusinessAccountActivated == true {
            if let id = user.userBusinessAccountID{
                businessId = id
            }
        }else {
            businessId = ""
        }
        guard let id = feedId else {
            return
        }
        let parameter = [
            "comment": trimCharacter(comment: txtComment.text!),
            "businessId": businessId
        ]
        indicator.start()
        VCommentService.addMediaComment(feedId: id,param: parameter as [String : AnyObject], successBlock: { (msg) in
            self.getCommentList(shouldReset: true)
            self.txtComment.text! = ""
            self.btnAddComment.isEnabled = true
        }) { (msg) in
            self.indicator.stop()
            self.btnAddComment.isEnabled = true
            showAlert(controller: self, title: "", message: msg)
        }
    }

    
    //MARK: SERVICE CALL
    func getCommentList(shouldReset reset:Bool) {
        self.btnComment.isEnabled = false
        guard let id = feedId else {
            return
        }
        indicator.tintColor = UIColor.gray
        indicator.start()
        if reset {
            self.cursor.reset()
        }
     
        VCommentService.getMediaCommentList(feedId: id, pageNumber: cursor.nextPage(), pageSize: cursor.pageSize, successBlock: { (comments, commentCount) in
            if reset {
                self.mediaCommentListsArr.removeAll()
            }
            self.mediaCommentListsArr.append(contentsOf: comments)
            self.cursor.totalCount = commentCount
            self.cursor.totalLoadedDataCount = self.mediaCommentListsArr.count
            self.indicator.stop()
            self.btnComment.isEnabled = true
            self.tblHeight.constant = 280
            self.tblComment.reloadData()
            self.tblComment.layoutIfNeeded()
            self.maintainTblViewHeight()
            self.txtComment.resignFirstResponder()
        }) { (message) in
            self.indicator.stop()
            self.btnComment.isEnabled = true
            showAlert(controller: self, title: "", message: message)
        }
    }
  
    //MARK: SEGUE
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.player?.isMuted = true
        let controller = segue.destination as! EventDetailViewController
        controller.isFromViteFeed = true
        controller.eventDetail = self.detailEvent
    }
    
    //MARK: IBACTIONS
    
    @IBAction func showEventDetail(_ sender: Any) {
        self.topSpace.constant = -(self.view.frame.size.height)
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func btnCrossAction(_ sender: Any) {
        self.player?.isMuted = true
       self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showComment(_ sender: Any) {
        commentState = !commentState
        if commentState {
            self.view.gestureRecognizers?.removeAll()
            selectChat(select: true, show: 1)
        } else {
            addUpdownGesture()
            selectChat(select: false, show: 0)
        }
    }
    
    @IBAction func addComment(_ sender: Any) {
        if trimCharacter(comment: txtComment.text!).count > 0 {
             addCommentToMedia()
        }
    }
}

//MARK: TABLE VIEW DELEGATE AND DATASOURCE

extension SwipeFullScreenViewController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return mediaCommentListsArr.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let media = mediaCommentListsArr[indexPath.row]
   
        let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell") as! EventCommentCell
        cell.lblName.text = media.user?.fullName
        if let imageUrl = media.user?.profileImageUrl?.convertIntoViteURL(){
            cell.imgUser.sd_setImage(with: URL(string: imageUrl), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
        }
        cell.lblComment.text = media.comment
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.cursor.hasNextPage() && indexPath.row == (mediaCommentListsArr.count - 1) {
                        self.getCommentList(shouldReset: false)
                    }
    }

    

    //MARK: SCROLL VIEW DELEGATE
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
         fadeTopAndShowButtomCell()
    }
    
    //fade and show table view
    func fadeTopAndShowButtomCell(){
        // Fades out top and bottom cells in table view as they leave the screen
        var visibleCells = self.tblComment.visibleCells
        if  visibleCells.count != 0 {
            // Don't do anything for empty table view
            /* Get top and bottom cells */
            let topCell = visibleCells[0]
            let bottomCell = visibleCells.last!
            /* Make sure other cells stay opaque */
            // Avoids issues with skipped method calls during rapid scrolling
            for cell: UITableViewCell in visibleCells {
                cell.contentView.alpha = 1.0
            }
            /* Set necessary constants */
            let cellHeight = topCell.frame.size.height - 1
            // -1 To allow for typical separator line height
            let tableViewTopPosition = self.tblComment.frame.origin.y
            let tableViewBottomPosition = self.tblComment.frame.origin.y + self.tblComment.frame.size.height
            /* Get content offset to set opacity */
            var topCellPositionInTableView  = CGRect()
            var bottomCellPositionInTableView = CGRect()
            if let indexTopCell = self.tblComment.indexPath(for: topCell){
                topCellPositionInTableView = self.tblComment.rectForRow(at: indexTopCell)
            }
            if let indexButtomCell = self.tblComment.indexPath(for: bottomCell){
                bottomCellPositionInTableView = self.tblComment.rectForRow(at: indexButtomCell)
            }
            let topCellPosition: CGFloat = self.tblComment.convert(topCellPositionInTableView, to: self.tblComment.superview!).origin.y
            let bottomCellPosition: CGFloat = (self.tblComment.convert(bottomCellPositionInTableView, to: self.tblComment.superview!).origin.y + cellHeight)
            /* Set opacity based on amount of cell that is outside of view */
            let modifier: CGFloat = 2.5
            /* Increases the speed of fading (1.0 for fully transparent when the cell is entirely off the screen,
             2.0 for fully transparent when the cell is half off the screen, etc) */
            let topCellOpacity: CGFloat = (1.0 - ((tableViewTopPosition - topCellPosition) / cellHeight) * modifier)
            let bottomCellOpacity: CGFloat = (1.0 - ((bottomCellPosition - tableViewBottomPosition) / cellHeight) * modifier)
            /* Set cell opacity */
            if topCell == visibleCells[0] {
                topCell.contentView.alpha = topCellOpacity
            }
            if bottomCell == visibleCells.last! {
                bottomCell.contentView.alpha = bottomCellOpacity
            }
        }
    }
    
}
