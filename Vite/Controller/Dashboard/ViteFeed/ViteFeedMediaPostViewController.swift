//
//  PostImageViewController.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 3/13/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit
import MBCircularProgressBar
import AVFoundation
import IQKeyboardManagerSwift


class ViteFeedMediaPostViewController: UIViewController {
    
    @IBOutlet weak var gradientView: UIView!
    //MARK: OUTLETS
    @IBOutlet fileprivate var captureButton: UIButton!
    @IBOutlet weak var circularProgressbar: MBCircularProgressBarView!
    @IBOutlet fileprivate var capturePreviewView: UIView!
    @IBOutlet weak var capturedView: UIView!
    @IBOutlet weak var capturedImgView: UIImageView!
    @IBOutlet weak var btnPostMedia: UIButton!
    @IBOutlet weak var roundView: RoundView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var toggleCameraBtn: UIButton!
    @IBOutlet weak var txtCaption: UITextField!
   
    
    //MARK: VARIABLES USED
    var isImageCaptured =  false {
        didSet {
            if isImageCaptured {
                self.txtCaption.text = ""
            }
            captureButton.isHidden = isImageCaptured ? true : false
            toggleCameraBtn.isHidden = isImageCaptured ? true : false
            capturePreviewView.isHidden = isImageCaptured ? true : false
            capturedImgView.isHidden = isImageCaptured ? false : true
            circularProgressbar.isHidden = isImageCaptured ? true : false
            capturedView.isHidden = isImageCaptured ? true : false
            txtCaption.isHidden = isImageCaptured ? false : true
            btnPostMedia.isHidden = isImageCaptured ? false : true
            roundView.isHidden = isImageCaptured ? true : false
        }
    }
    var isVideoCaptured =  false {
        didSet {
            if isVideoCaptured {
                self.txtCaption.text = ""
            }
            captureButton.isHidden = isVideoCaptured ? true : false
            toggleCameraBtn.isHidden = isVideoCaptured ? true : false
            capturePreviewView.isHidden = isVideoCaptured ? true : false
            capturedImgView.isHidden = isVideoCaptured ? true  : false
            circularProgressbar.isHidden = isVideoCaptured ? true : false
            capturedView.isHidden = isVideoCaptured ? false : true
            txtCaption.isHidden = isVideoCaptured ? false : true
            btnPostMedia.isHidden = isVideoCaptured ? false : true
            roundView.isHidden = isVideoCaptured ? true : false
            progressView.isHidden = isVideoCaptured ? false : true
        }
    }
    let avPlayer = AVPlayer()
    var avPlayerLayer: AVPlayerLayer!
    let cameraController = CameraController()
    var vidUrl: URL!
    let indicator =  ActivityIndicator()
    var checkVideoRecording = false
    var yPositionStore = CGFloat()
    
    
    //MARK: IBACTIONS
    @IBAction func close(_ sender: Any) {
        if isImageCaptured || isVideoCaptured {
            isImageCaptured = false
            isVideoCaptured = false
           self.capturedImgView.image = UIImage()
           avPlayer.pause()
        } else {
            self.dismiss(animated: true, completion: nil)
        }
     
    }
    
    @IBAction func captureImage(_ sender: Any) {
        self.indicator.start()
        self.indicator.tintColor = UIColor.gray
        isImageCaptured = true
        cameraController.captureImage {(image, error) in
            guard let image = image else {
                print(error ?? "Image capture error")
                return
            }
            self.capturedImgView.image = image
            self.indicator.stop()
        }
    }
    @IBAction func postMedia(_ sender: Any) {
        self.view.isUserInteractionEnabled = false
        if isImageCaptured {
            if let img = self.capturedImgView.image {
                self.sendImageData(image: img)
            }
        } else {
            self.compressVideoAndPostToServer()
        }
    }
    
    @IBAction func switchCameras(_ sender: UIButton) {
        do {
            try cameraController.switchCameras()
        }
            
        catch {
            print(error)
        }
    }
    
}

extension ViteFeedMediaPostViewController: AVCaptureFileOutputRecordingDelegate{
    override func viewDidLoad() {
        txtCaption.layer.cornerRadius = 20
      //  txtCaption.clipsToBounds = true
        txtCaption.attributedPlaceholder = NSAttributedString(string: "Add Caption ...",
                                                               attributes: [NSAttributedStringKey.foregroundColor: UIColor.colorFromRGB(rgbValue: 0xB5B5B5)])
        let gradient = CAGradientLayer()
        gradient.frame = gradientView.bounds
        gradient.colors = [UIColor.black.cgColor, UIColor.clear.cgColor]
        gradient.locations = [0, 1]
        gradientView.layer.insertSublayer(gradient, at: 0)
        self.circularProgressbar.value = 0
        
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(singleTap(_:)))
        longGesture.minimumPressDuration = 1
        captureButton.addGestureRecognizer(longGesture)
        captureButton.addGestureRecognizer(tapGesture)
        configureCameraController()
      
    }
 
    override func viewDidAppear(_ animated: Bool) {
        self.progressView.progress = 0
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.avPlayer.pause()
    }
    
    //MARK: CONFIGURE FULL SCREEN CAMERA
    func configureCameraController() {
        self.indicator.start()
        self.view.isUserInteractionEnabled = false
        self.indicator.tintColor = UIColor.gray
        cameraController.prepare {(error) in
            if let error = error {
                print(error)
                self.view.isUserInteractionEnabled = true
            }
            try? self.cameraController.displayPreview(on: self.capturePreviewView)
            self.indicator.stop()
            self.view.isUserInteractionEnabled = true
        }
    }

    
    //MARK: SINGLE TAP
    
    @objc func singleTap(_ sender: UIGestureRecognizer){
        self.indicator.start()
        self.indicator.tintColor = UIColor.gray
        isImageCaptured = true
        cameraController.captureImage {(image, error) in
            guard let image = image else {
                print(error ?? "Image capture error")
                return
            }
            print("Image captured")
            self.capturedImgView.image = image
            self.indicator.stop()
        }
    }
    
    //MARK : LONG TAP GESTURE
    @objc func longTap(_ sender: UIGestureRecognizer){
        if sender.state == .ended {
            print("Ended Called")
            // checkVideoRecording is set to true because for some cases this end state is called before the video starts to record
            checkVideoRecording = true
            cameraController.stopRecording()
             self.indicator.stop()
            self.isVideoCaptured = true
            self.circularProgressbar.value = 0
        }
        else if sender.state == .began {
            print("Began Called")
            toggleCameraBtn.isHidden = true
            UIView.animate(withDuration: 10, animations: {
                self.circularProgressbar.value = 100
                self.cameraController.captureVideo(completion: { (url, movieOutput, err) in
                    self.vidUrl = url
                    movieOutput?.startRecording(to:self.vidUrl, recordingDelegate: self)
                })
            }, completion: nil)
        }
    }
    
    
    
    func playVideo() {
        avPlayerLayer = AVPlayerLayer(player: avPlayer)
        avPlayerLayer.frame = view.bounds
        avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
        capturedView.layer.insertSublayer(avPlayerLayer, at: 0)
        let playerItem = AVPlayerItem(url: vidUrl )
        avPlayer.replaceCurrentItem(with: playerItem)
        avPlayer.play()
        self.indicator.stop()
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.avPlayer.currentItem, queue: .main) { _ in
            self.avPlayer.seek(to: kCMTimeZero)
            self.avPlayer.play()
        }
    }
    
    //MARK : AVCaptureFileOutputRecordingDelegate Methods
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        self.indicator.start()
        self.indicator.tintColor = UIColor.gray
        if (error != nil) {
             self.indicator.stop()
                checkVideoRecording = false
        print("Error recording movie: \(error!.localizedDescription)")
        } else {
             playVideo()
            checkVideoRecording = false
          
        }
    }
    
    func fileOutput(_ output: AVCaptureFileOutput, didStartRecordingTo fileURL: URL, from connections: [AVCaptureConnection]) {
        // this bool is true only when the end state of long gesture button is called before this delegate method didStartRecordingto
        if checkVideoRecording {
            print("inside did start")
            cameraController.stopRecording()
        }
    }
}


//MARK: UPLOAD MEDIA AND POST MEDIA TO SERVER
extension ViteFeedMediaPostViewController {
    //MARK: Upload multiple images and videos to server
    func uploadVideoOrImage(_ mediaData: Data,thumbnailData: Data?, mediaName: String, mediaFormat: String, mediaType: String,mediaWidth: Int, mediaHeight:Int){
        self.indicator.start()
        self.indicator.tintColor = UIColor.gray
        VPostMediaViteFeedService.uploadMedia(mediaData, videoThumbnailData: thumbnailData!, mediaName: mediaName, mediaType: mediaType, mediaWidth: mediaWidth, mediaHeight:mediaHeight, mediaFormat: mediaFormat, progressBlock: { (progressResult) in
            progressResult.uploadProgress(closure: { (progress) in
                let percent = Int(100 * progress.fractionCompleted)
                DispatchQueue.main.async{
                    //                    self.progressPercentage.text = "\(percent)%"
                                        self.progressView.setProgress(Float(progress.fractionCompleted), animated: true)
                    
                }
            })
            
        }, successBlock: { (media) in
               self.avPlayer.pause()
            if let id = media.mediaId {
                let controller = Route.selectViteOrEventToPost
                controller.caption = self.txtCaption.text!
                controller.mediaId = id
                self.present(controller, animated: true, completion: nil)
                self.view.isUserInteractionEnabled = true
            }
            self.indicator.stop()
  
        }, failureBlock: { (message) in
            self.indicator.stop()
            self.avPlayer.pause()
            self.progressView.progress = 0
            self.view.isUserInteractionEnabled = true
            showAlert(controller: self, title: "", message: message)
        })
    }
    
    func compressVideoAndPostToServer() {
           self.indicator.start()
         var compressedFileData : Data? =  nil
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".mov")
        Base64.compressVideo(inputURL: self.vidUrl, outputURL: compressedURL, handler: { (_ exportSession: AVAssetExportSession?) -> Void in
            switch exportSession!.status {
            case .completed:
                print("Video compressed successfully")
                do {
                    compressedFileData = try Data(contentsOf: exportSession!.outputURL!)
                    self.sendVideoData(videoData: compressedFileData!)
                    // Call upload function here using compressedFileData
                } catch _ {
                    showAlert(controller: self, title: "", message: localizedString(forKey: "ERROR_COMPRESSING_VIDEO"))
                    self.indicator.stop()
                     self.view.isUserInteractionEnabled = true
                    print ("Error converting compressed file to Data")
                }
                
            default:
                 self.indicator.stop()
                self.view.isUserInteractionEnabled = true
                print("Could not compress video")
            }
        } )
    }
    
    func sendImageData(image: UIImage) {
        let imgData = Base64.getCompressedImageData(image)
        let imageName = String(UInt64(floor(Date().timeIntervalSince1970 * 1000))) + ".jpeg"
        print(imageName)
        let mediaFormat = "jpeg/png"
        //thumbnail data for the image is not used
        
        self.uploadVideoOrImage(imgData as Data, thumbnailData: imgData as Data,mediaName: imageName, mediaFormat: mediaFormat,mediaType: "IMAGE",mediaWidth: Int(image.size.width),mediaHeight: Int(image.size.height))
    }
    
    func sendVideoData(videoData: Data){
        //video format
        if (videoData.count) < 10000000 {
            let mediaFormat = "mov/mp4"
            let thumbNailImage = thumbnailForVideoAtURL(self.vidUrl)
            let thumbNailImgData = Base64.getCompressedImageData(thumbNailImage!)
            let videoFileName = String(UInt64(floor(Date().timeIntervalSince1970 * 1000))) + UserDefaultsManager().userFBID! + ".MOV"
            if let thumbNailheight = thumbNailImage?.size.height , let thumbNailWidth = thumbNailImage?.size.width{
                self.uploadVideoOrImage(videoData,thumbnailData: thumbNailImgData as Data, mediaName: videoFileName, mediaFormat: mediaFormat,mediaType: "VIDEO", mediaWidth: Int(thumbNailWidth),mediaHeight: Int(thumbNailheight))
            }
        } else {
             self.view.isUserInteractionEnabled = true
            showAlert(controller: self, title: localizedString(forKey: "ALERT"), message: localizedString(forKey: "UPLOAD_VIDEO"))
        }
    }
}
