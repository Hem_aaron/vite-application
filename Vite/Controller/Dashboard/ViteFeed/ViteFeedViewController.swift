//
//  ViteFeedViewController.swift
//  Vite
//
//  Created by Hem Poudyal on 4/23/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper
import AVFoundation
import SlideMenuControllerSwift

class ViteFeedViewController: WrapperController, UICollectionViewDataSource, CollectionViewWaterfallLayoutDelegate {
    
    @IBOutlet weak var placeHolderImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    var feedList = [ViteFeed]()
    var cardAmount = 20
    var pageNum = 1
    var currentPage = 1
    var pageRefreshing = false
    var count = 0{
        didSet {
            placeHolderImageView.isHidden = count>0 ? true : false
        }
    }
    let refresher = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.loadViteFeed(pageNumber: pageNum, cardAmount: cardAmount)
        self.feedList = [ViteFeed]()
        
        let layout = CollectionViewWaterfallLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
        layout.headerInset = UIEdgeInsetsMake(0, 0, 0, 0)
        layout.headerHeight = 0
        layout.footerHeight = 20
        layout.minimumColumnSpacing = 10
        layout.minimumInteritemSpacing = 10
        
        collectionView.collectionViewLayout = layout
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: CollectionViewWaterfallElementKindSectionHeader, withReuseIdentifier: "Header")
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: CollectionViewWaterfallElementKindSectionFooter, withReuseIdentifier: "Footer")
        self.collectionView.backgroundColor = UIColor.white
        
        self.collectionView!.alwaysBounceVertical = true
        refresher.tintColor = UIColor.gray
        refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        collectionView!.addSubview(refresher)
     
    }
    
    //    func switchAccount(notification: NSNotification) {
    //        // collectionView.hidden = User().isBusinessAccountActivated ? true : false
    //    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.collectionView.isUserInteractionEnabled = true
        self.loadViteFeed(pageNumber: pageNum, cardAmount: cardAmount)
        NotificationCenter.default.addObserver(self, selector: #selector(self.mediaPostSuccessMsg(notification:)), name: NSNotification.Name(rawValue: "postMediaSuccessMsg"), object: nil)
    }
    
    
    
    
    
    //MARK:- methods
    
    @objc func loadData()
    {
        //code to execute during refresher
        loadViteFeed(pageNumber: 1, cardAmount: 20)
    }

    // NOTIFICATION FROM VITE FEED POST MEDIA
    @objc func mediaPostSuccessMsg(notification: NSNotification) {
        if let msg = notification.object as? String {
            showAlert(controller: self, title: "Success", message: msg)
        }
    }
    
    @objc func callViteFeedAPI(notification: NSNotification) {
         self.loadViteFeed(pageNumber: pageNum, cardAmount: cardAmount)
    }
    
    func stopRefresher()
    {
        refresher.endRefreshing()
    }
    
    func loadViteFeed(pageNumber:Int ,cardAmount:Int ){
        WebService.request(method: .get, url: kAPILiveFeed + "/\(pageNumber)/\(cardAmount)", success: { [weak self] (response) in
            guard let response = response.result.value as! [String: AnyObject]?,
                let success = response["success"] as? Bool else {
                    return
            }
            print(response)
            self?.pageRefreshing = false
            self?.view.hideToastActivity()
            
            if let totalCount = response["params"]?["count"] as? Int{
                self?.count = Int(totalCount)
            }
            
            if pageNumber == 1{
                self?.feedList.removeAll()
            }
            if success {
                let responeArray = response["params"]?["media"] as! [[String:AnyObject]]
                for obj in responeArray {
                    let feed = Mapper<ViteFeed>().map(JSON: obj)
                    if let f = feed {
                        self?.feedList.append(f)
                    }
                }
            }
            self?.collectionView.reloadData()
            self?.stopRefresher()
        }) { [weak self](message, apiError) in
            if pageNumber>1{
                self?.pageNum = (self?.pageNum)! - 1
            }
            showAlert(controller: self!, title: "", message: message)
            self?.stopRefresher()
        }
        
    }
    
    func showImageOrVideoScreen(feed:ViteFeed){
        let controller = Route.SwipeFullScreenViewController
        controller.mediaURL = feed.mediaUrl!
        controller.mediaType = feed.fileType!
        controller.feedId = feed.feedId
        controller.caption = feed.caption
        
        VEventWebService.getEventDetail(feed.eventId!, successBlock: { [weak self] (eventDetail) in
            controller.detailEvent = eventDetail
            let nav  =  UINavigationController.init(rootViewController : controller)
            self?.present(nav, animated: true, completion: nil)
        }) {[weak self] (message) in
            showAlert(controller: self!, title: "", message: message)
        }
        
    }
    
    @objc func showOrganizer(sender: UIButton){
        let feed =  self.feedList[Int(sender.tag)]
        //add friends of user in server
        self.collectionView.isUserInteractionEnabled = false
        if feed.businessAccount!{
            // show business account
            let controller                       = Route.updateAccountForBusiness
            controller.businessID = String(feed.businessId!)
            controller.isFromViteFeed = true
              let nav  =  UINavigationController.init(rootViewController : controller)
              self.present(nav, animated: true, completion: nil)
        }
        else{
            let controller                       = Route.profileViewController
            controller.fbId                      = String(describing: feed.organizerId!)
            controller.isForViewingOthersProfile =  String(describing: feed.organizerId!) == UserDefaultsManager().userFBID! ? false : true
            let nav  =  UINavigationController.init(rootViewController : controller)
            self.present(nav, animated: true, completion: nil)
       
        }
        
    }
    @objc func showFullScreen(sender: UIButton){
        
        let feed =  self.feedList[Int(sender.tag)]
        //add friends of user in server
        self.collectionView.isUserInteractionEnabled = false
        self.showImageOrVideoScreen(feed: feed)
     }
    
  
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feedList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let feed = self.feedList[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath ) as! ViteFeedCollectionViewCell
        let mediaUrl = feed.mediaUrl?.convertIntoViteURL()
        
        if feed.fileType == "VIDEO"{
            cell.btnPlay.isHidden = false
            if let thumbnailUrl = feed.thumbnailUrl?.convertIntoViteURL() {
              cell.imageEvent.sd_setImage(with: URL(string:thumbnailUrl), completed: nil)
            }
            //cell.imageEvent.sd_setImageWithURL(NSURL(string:thumbnailUrl!))
            cell.btnPlay.tag = indexPath.row
            cell.btnPlay.addTarget(self, action: #selector(self.showFullScreen(sender:)), for: .touchUpInside)
        }else{
            cell.btnPlay.isHidden = true
            cell.imageEvent.sd_setImage(with: URL(string:mediaUrl!))
        }
        cell.btnOrganizer.tag = indexPath.row
        cell.btnOrganizer.addTarget(self, action: #selector(self.showOrganizer(sender:)), for: .touchUpInside)
        if let imgUrl = feed.profileImage?.convertIntoViteURL() {
            print(imgUrl)
           cell.imgProfile.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "ProfilePlaceholder.png"))
        }
        if let isPost = feed.postedBy {
            if isPost {
                cell.title.text = "Story from \(feed.eventTitle ?? "")"
                cell.buttomView.backgroundColor = UIColor.colorFromRGB(rgbValue: 0x6776FE)
            }  else {
                cell.title.text = feed.eventTitle
                cell.buttomView.backgroundColor = UIColor.colorFromRGB(rgbValue: 0xFE4C4D)
            }
        }
        cell.lblOrgName.text = feed.organizerName
        //cell.title.text = feed.eventTitle
        if let commentCount = feed.commentCount {
              cell.lblComment.text = commentCount > 1 ? "\(commentCount) comments" : "\(commentCount) comment"
        }
     
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var reusableView: UICollectionReusableView? = nil
        
        if kind == CollectionViewWaterfallElementKindSectionHeader {
            reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath)
            
            if let view = reusableView {
                view.backgroundColor = UIColor.white
            }
        }
        else if kind == CollectionViewWaterfallElementKindSectionFooter {
            reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Footer", for: indexPath)
            if let view = reusableView {
                view.backgroundColor = UIColor.white
            }
        }
        
        return reusableView!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let feed = self.feedList[indexPath.row]
        if feed.fileType == "IMAGE" {
            self.collectionView.isUserInteractionEnabled = false
            self.showImageOrVideoScreen(feed: feed)
        }
    }
    
    // MARK: WaterfallLayoutDelegate
    
    func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if feedList.count == 0 {
            return  CGSize(width: 0.0, height: 0.0)
        }
        
        let media = self.feedList[indexPath.row]
        let width = 130
        let height = ((CGFloat(truncating: media.mediaHeight!) / CGFloat(truncating: media.mediaWidth!)) * 130) + 140
        
        return CGSize(width: CGFloat(width), height: CGFloat(height))
    }
    
    
    func  scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            //you reached end of the table
            if self.pageNum * 20 < self.count{
                //page refreshing checks if the app is busy getting data from server
                if !pageRefreshing{
                    self.pageNum += 1
                    self.currentPage = self.pageNum
                    self.loadViteFeed(pageNumber: self.pageNum, cardAmount: cardAmount)
                    pageRefreshing = true
                }
            }
        }
    }
}
