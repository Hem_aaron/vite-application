//
//  ViteWebView.swift
//  Vite
//
//  Created by EeposIT_X on 3/8/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation


class ViteWebView:WrapperController,UIWebViewDelegate{

    
    @IBOutlet weak var webView: UIWebView!
     let indicator = ActivityIndicator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.delegate = self
        
        indicator.parentView = webView 
        indicator.tintColor = UIColor.gray
        indicator.start()


        let url = URL(string: "http://www.vite.city")
        let requestObj = URLRequest(url: url!)
        webView.loadRequest(requestObj as URLRequest)
        
    }
    
    @IBAction func back(sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        indicator.stop()
    }
}
