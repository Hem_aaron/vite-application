//
//  AttendingCollectionViewCell.swift
//  Vite
//
//  Created by Eeposit  on 1/25/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit

class AttendingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var attendingPeopleImg: CircleImage!
    
}
