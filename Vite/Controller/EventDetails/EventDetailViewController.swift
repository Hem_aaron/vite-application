//
//  ViewController.swift
//  ViteDesign
//
//  Created by sajjan giri on 11/9/16.
//  Copyright © 2016 sajjan giri. All rights reserved.
//

import UIKit
import MapKit
import AVFoundation
import Hero
class EventDetailViewController: WrapperController ,UIScrollViewDelegate, UICollectionViewDataSource , UICollectionViewDelegate{
  
  
    
    var reportSubjectArr = ["The Event has been cancelled.","The Event has been postponed.","The Event contains nudity.","The Event is a Spam.","Others"]
  var eventDetail:VEvent?
  var historyList = [GroupChatMessageModel]()
  var attendees : [MyEventAttendee]? = [MyEventAttendee]()
  var currentLocation = CLLocation()
  var userConnectedToStripe = false
  var cardIndex = Int()
  var event = [VEvent]()
  var isFromViteFeed = false
  var service = ChatHistoryService()
    // if first media is a video put value of the url in this variable
  var secondaryVideoUrl = String()

  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var friendsAttendingLabels: GrayLabel!
  @IBOutlet weak var eventImagesLabel: GrayLabel!
  @IBOutlet weak var backBtn: UIButton!
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var btnPlay: UIButton!
  @IBOutlet weak var eventImage: UIImageView!
  @IBOutlet weak var eventTitle: UILabel!
  @IBOutlet weak var organizerName: UILabel!
  @IBOutlet weak var organizerImg: CircleImage!
  @IBOutlet weak var eventDesc: UILabel!
  @IBOutlet weak var eventlocation: UILabel!
  @IBOutlet weak var eventTime: UILabel!
  @IBOutlet weak var attendesCount: UILabel!
  @IBOutlet weak var peopleAttendees: UILabel!
  @IBOutlet weak var attendeesCollectionView: UICollectionView!
  @IBOutlet weak var mediaFileCollectionView: UICollectionView!
  @IBOutlet weak var btnRequest: UIButton!
  @IBOutlet weak var lblPrice: UILabel!
  @IBOutlet weak var lblEventPrice: UILabel!
  @IBOutlet weak var btnBuyTicket: UIButton!
  @IBOutlet weak var mediaView: UIView!
  @IBOutlet weak var mediaViewTopSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var mediaCollectionViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var friendsAttendingViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var friendsAttendingViewTopSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var descViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var descViewTopSpaceConstriant: NSLayoutConstraint!
  @IBOutlet weak var descView: UIView!
  
  @IBOutlet weak var eventImageHeight: NSLayoutConstraint!
  @IBOutlet weak var btnReportEvent: UIButton!
  @IBOutlet weak var btnMessageOrganizer: UIButton!
  @IBOutlet weak var btnShareEvent: UIButton!
    @IBOutlet weak var backBtnTopSpace: NSLayoutConstraint!
    
    let notificationCenter = NotificationCenter.default
    
  override func viewDidLoad() {
    super.viewDidLoad()
    self.eventImage.hero.id = "KOLODA"
    // hide friends attending view if the count is zero
    self.scrollView.delegate = self
    self.getStripeAccountStatus()
    if eventDetail?.friendAttending?.count == 0 {
        hideShowFriendsAttendingView(heightValue: 0, topSpaceValue: 0, hiddenState: true)
    } else {
        hideShowFriendsAttendingView(heightValue: 127, topSpaceValue: 5, hiddenState: false)
    }
    currentLocation = CLLocation(latitude: Double(UserDefaultsManager().userCurrentLatitude!)!, longitude:  Double(UserDefaultsManager().userCurrentLongitude!)!)
    self.eventImage.clipsToBounds = true
    if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
        case 2436,2488:
            print("iPhone X , Xr")
            backBtnTopSpace.constant = 60
            break
        default:
            print("unknown")
        }
    }
   }

    
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
//    self.eventImage.roundCorners([.TopRight , .TopLeft], radius: 10)
    
    }
    
    override func viewDidAppear( _ animated: Bool)
    {
       
        
    }
  
    override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
//    if isFromViteFeed {
//        self.backBtn.isHidden = true
//    } else {
//        self.backBtn.isHidden = false
//    }
    loadEventDetail()
    self.navigationController?.isNavigationBarHidden  = true
  }
  
  //MARK: Private Methods
  
  func checkRequestViteStatus() -> Bool{
    guard let eventStatus = eventDetail?.eventsUsers else {
        return false
    }
    if eventStatus.requestedDate != nil {
        return true
    } else {
        return false
    }
  }
    

    
    func hidePrice(state: Bool){
        lblPrice.isHidden = state
        lblEventPrice.isHidden = state
        btnBuyTicket.isHidden =  checkRequestViteStatus() ? !state : state
        btnRequest.isHidden = !state
    }
    
 
  func loadEventDetail () {
    // image or video load logic-
    
   // print(eventDetail?.promoCode)
    if eventDetail?.fileType == "IMAGE" {
      self.btnPlay.isHidden = true
      self.eventImage.sd_setShowActivityIndicatorView(true)
    if let eventImage = eventDetail?.imageUrl {
        self.eventImage.sd_setImage(with: URL(string: eventImage.convertIntoViteURL()))
      }
    } else {
      self.btnPlay.isHidden = false
      self.eventImage.sd_setShowActivityIndicatorView(true)
      if let thumbnailUrl = self.eventDetail?.videoProfileThumbNail{
        self.eventImage.sd_setImage(with: URL(string: thumbnailUrl.convertIntoViteURL()))
      }
    }
    
    if isFromViteFeed {
        guard let media = eventDetail?.media else  {
            return
        }
        if media.count > 0 {
            if let mediaType = media[0].fileType, let url = media[0].mediaUrl {
                if mediaType == "IMAGE" {
                    self.btnPlay.isHidden = true
                    self.eventImage.sd_setImage(with: URL(string: url.convertIntoViteURL()))
                } else {
                    self.btnPlay.isHidden = false
                    self.eventImage.sd_setShowActivityIndicatorView(true)
                    if let thumbnailUrl = media[0].thumbNailUrl, let url = media[0].mediaUrl{
                        self.secondaryVideoUrl = url.convertIntoViteURL()
                        self.eventImage.sd_setImage(with: URL(string:thumbnailUrl.convertIntoViteURL()))
                    }
                }
            }
        }
    }
    if eventDetail?.paidEvent == true  {
        hidePrice(state: false)
        btnBuyTicket.isHidden =  checkRequestViteStatus() ? true : false
    } else {
        hidePrice(state: true)
        btnBuyTicket.isHidden = true
        btnRequest.isHidden = checkRequestViteStatus() ? true : false
    }
    if let userId = UserDefaultsManager().userFBID , let organiserId = eventDetail?.organizer?.uid {
        if userId == String(describing: organiserId) {
            btnRequest.isHidden = true
            btnBuyTicket.isHidden = true
            btnReportEvent.isHidden = true
            btnMessageOrganizer.isHidden = true
        }
    }
   
    
    if let price = eventDetail?.eventPrice {
        self.lblEventPrice.text = "$\(price)"
    }
    self.showMediaFileCollectionView()
    self.eventTitle.text = eventDetail?.eventTitle
    if let businessName = eventDetail?.business?.businessName, let businessImage = eventDetail?.business?.profileImage {
      self.organizerName.text = businessName
        self.organizerImg.sd_setImage(with: URL(string: businessImage.convertIntoViteURL()), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
    } else {
      self.organizerName.text = eventDetail?.organizer?.fullName
        if let organizerProfileUrl =  eventDetail?.organizer?.profileImageUrl?.convertIntoViteURL(), let organizerId = eventDetail?.organizer?.uid{
            print(organizerId)
            if isValidImage(url: organizerProfileUrl){
                self.organizerImg.sd_setImage(with: URL(string: (eventDetail?.organizer?.profileImageUrl)!.convertIntoViteURL()), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
            } else {
                let profileUrl = "http://graph.facebook.com/\(organizerId)/picture?type=large"
                self.organizerImg.sd_setImage(with: URL(string: profileUrl), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
            }
        }
    }
    if eventDetail?.detail?.count == 0 {
        descView.alpha = 0
        descViewTopSpaceConstriant.constant = 0
        descViewHeightConstraint.constant = 0
        descriptionLabel.text = ""
        scrollView.backgroundColor = UIColor.colorFromRGB(rgbValue: 0x6F2D6E)
    } else {
        self.eventDesc.text = eventDetail?.detail
        descView.alpha = 1
        descViewTopSpaceConstriant.constant = 5
        descViewHeightConstraint.constant = 130
        descriptionLabel.text = "DESCRIPTION"
        scrollView.backgroundColor = UIColor.white
    }
    
    let screenSize = UIScreen.main.bounds
    let screenHeight = screenSize.height
    eventImageHeight.constant = screenHeight - 333
    
    let eventLocation  = CLLocation(latitude: Double((eventDetail?.location?.latitude)!)!, longitude:  Double((eventDetail?.location?.longitude)!)!)
    self.eventlocation.text = String(round(getDistanceInMile(userCurrentLocation: currentLocation, eventLocation: eventLocation))) + " " + "miles away"
    let eventDate               = eventDetail?.date
    let eventStartTime          = eventDetail?.startTime
    let dateFormatter           = DateFormatter()
    dateFormatter.dateFormat    = "YYYY-MM-dd HH:mm:ss"
    let date = dateFormatter.date(from: eventDate! + " " + eventStartTime!)
    self.eventTime.text             = timeAgoSince(date: date!)
    if let friendCount = eventDetail?.friendAttending?.count {
      self.attendesCount.text = "\(friendCount)"
    }
  }
  
  // hide and show media collection view logic
  func showMediaFileCollectionView(){
    if let media =  eventDetail?.media {
        if isFromViteFeed {
            //Since the first media is shown in the main image, Media collection view is only load if the media count in greater than 1
            mediaViewTopSpaceConstraint.constant = media.count <= 1 ? 0 : 5
            mediaCollectionViewHeightConstraint.constant = media.count <= 1 ? 0 : 250
            mediaView.isHidden = media.count <= 1 ? true: false
            eventImagesLabel.text = media.count <= 1 ? "" : "EVENT IMAGES AND VIDEOS"
        } else {
            mediaViewTopSpaceConstraint.constant = media.count == 0 ? 0 : 5
            mediaCollectionViewHeightConstraint.constant = media.count == 0 ? 0 : 250
            mediaView.isHidden = media.count == 0 ? true: false
            eventImagesLabel.text = media.count == 0 ? "" : "EVENT IMAGES AND VIDEOS"
        }
    } else {
      mediaCollectionViewHeightConstraint.constant = 0
        mediaView.isHidden = true
    }
  }
  
    func goToViewController(eventName: String){
        notificationCenter.post(name: NSNotification.Name(rawValue: "requestVite"), object: nil)
        self.dismiss(animated: true, completion: nil)
  }
  
  func showReportEventPicker(){
    let alert = UIAlertController(title: nil,
                                  message: nil,
                                  preferredStyle: .actionSheet)
    alert.view.tintColor = UIColor.purple
    let cancelledEvent = UIAlertAction(title: "Cancelled",
                                       style: .default,
                                       handler: {
                                        (action:UIAlertAction) -> Void in
                                        if let id = self.eventDetail?.uid{
                                            self.goToMessagePage(subject: self.reportSubjectArr[0],eventId: id)
                                        }
                                        
    })
    
    let postponedEvent = UIAlertAction(title: "Postponed",
                                       style: .default,
                                       handler: {
                                        (action:UIAlertAction) -> Void in
                                        if let id = self.eventDetail?.uid{
                                            self.goToMessagePage(subject: self.reportSubjectArr[1],eventId: id)
                                        }
    })
    
    let nudityEvent = UIAlertAction(title: "Nudity",
                                    style: .default,
                                    handler: {
                                      (action:UIAlertAction) -> Void in
                                      if let id = self.eventDetail?.uid{
                                        self.goToMessagePage(subject: self.reportSubjectArr[2],eventId: id)
                                      }
    })
    let spamEvent = UIAlertAction(title: "Spam",
                                  style: .default,
                                  handler: {
                                    (action:UIAlertAction) -> Void in
                                    if let id = self.eventDetail?.uid{
                                        self.goToMessagePage(subject: self.reportSubjectArr[3],eventId: id)
                                    }
    })
    
    let others = UIAlertAction(title: "Others",
                               style: .default,
                               handler: {
                                (action:UIAlertAction) -> Void in
                                if let id = self.eventDetail?.uid{
                                  self.goToMessagePage(subject: self.reportSubjectArr[4],eventId: id)
                                }
    })
    
    
    
    let cancelAction = UIAlertAction(title: "Cancel",
                                     style: .cancel) {
                                      (action: UIAlertAction) -> Void in
    }
    
    alert.addAction(cancelledEvent)
    alert.addAction(postponedEvent)
    alert.addAction(nudityEvent)
    alert.addAction(spamEvent)
    alert.addAction(others)
    alert.addAction(cancelAction)
    
    present(alert,
                          animated: false,
                          completion: nil)
    
  }
  
  func goToMessagePage(subject: String, eventId: String) {
    let controller = Route.UserReasonViewController
    controller.subject = subject
    controller.eventId = eventId
    controller.isFromReportEvent = true
    self.navigationController?.show(controller, sender: nil)
  }
  
  func hideShowFriendsAttendingView(heightValue: CGFloat, topSpaceValue: CGFloat, hiddenState: Bool){
    friendsAttendingViewHeightConstraint.constant = heightValue
    friendsAttendingViewTopSpaceConstraint.constant = topSpaceValue
    attendesCount.isHidden = hiddenState
    friendsAttendingLabels.text = heightValue == 0 ? "" : "FRIENDS ATTENDING"
  }
  
  
  //MARK: IBActions
  
  // Play video in new view
    @IBAction func playVIdeo(_ sender: UIButton) {
        if secondaryVideoUrl.count > 0 {
            playVideo(secondaryVideoUrl, viewController: self)
        } else {
            if let url = eventDetail?.profileVideoUrl {
                playVideo(url.convertIntoViteURL(), viewController: self)
            }
        }
    }
   
  @IBAction func goBackEvent(sender: AnyObject) {
    self.dismiss(animated: true, completion: nil)
  }
    @IBAction func requestVite(_ sender: Any) {
        
        if !UserDefaultsManager().isBusinessAccountActivated {
        self.view.isUserInteractionEnabled = false
        let event = self.eventDetail
        if (event!.paidEvent == true) {
            //when it is a paid event
            getPaypalEmailStatus(event: self.eventDetail!)
        } else {// if it is not paid event
            self.userResponseToCard(event: event!)
        }
        }else {
            showAlert(controller: self, title: "", message: localizedString(forKey: "BUSINESS_ACC_REQ_FAIL"))
        }
    }
    
    func getPaypalEmailStatus(event: VEvent) {
        guard let organizerFbId = event.organizer?.uid else {
            return
        }
        PaypalWebService.checkPaypalStatus(fbId: String(describing: organizerFbId), { (paypalEmailStatus) in
               self.view.isUserInteractionEnabled = true
            if paypalEmailStatus {
                let controller = Route.makePaymentViewController
                controller.event = event
                self.present(controller, animated: true, completion: nil)
            } else {
                //when it is a paid event
                let controller = Route.paymentViewController
                controller.eventDetail = event
                if self.userConnectedToStripe{
                    //If the user is already connected to Stripe
                    controller.isUserNotConnectedToStripe = false
                } else {
                    //If the user is not connected to Stripe
                    controller.isUserNotConnectedToStripe = true
                }
                let nav = UINavigationController(rootViewController: controller)
                self.show(nav, sender: nil)
            }
        }) { (msg) in
            print(msg)
        }
    }
    
 
    @IBAction func reportEvent(_ sender: Any) {
        showReportEventPicker()
    }

  
  
  @IBAction func showOrganizerProfile(_ sender: AnyObject) {
    if let bg = eventDetail?.business {
        showBusinessprofile(businessDetail: bg)
    } else {
      showProfileOfUser()
    }
  }
  
    @IBAction func showMessageOrganizer(sender: AnyObject) {
        let chatBoard = UIStoryboard(name: "Chat", bundle: nil)
        let chatController      = chatBoard.instantiateViewController(withIdentifier: "ChatMainViewController") as! ChatMainViewController
        let sender             = ChatUser()
        sender.fbID            =  UserDefaultsManager().isBusinessAccountActivated ? UserDefaultsManager().userBusinessAccountID :UserDefaultsManager().userFBID
        sender.name            =  UserDefaultsManager().isBusinessAccountActivated ? UserDefaultsManager().userBusinessFullName : UserDefaultsManager().userFBFullName
        sender.profileImageUrl =  UserDefaultsManager().isBusinessAccountActivated ? UserDefaultsManager().userBusinessProfileImageUrl :"http://graph.facebook.com/\(UserDefaultsManager().userFBID!)/picture?type=large"
        let receiver             = ChatUser()
        if eventDetail?.business == nil {
            guard let receiverId = eventDetail?.organizer?.uid else {return}
            receiver.fbID            = receiverId.stringValue
            receiver.name            = eventDetail?.organizer?.fullName
            receiver.profileImageUrl = (eventDetail?.organizer?.profileImageUrl ?? "").convertIntoViteURL()
        } else {
            guard let receiverId = eventDetail?.business?.entityId else {return}
            receiver.fbID            = receiverId
            receiver.name            = eventDetail?.business?.businessName
            receiver.profileImageUrl = (eventDetail?.business?.profileImage ?? "").convertIntoViteURL()
        }
        chatController.receiver = receiver
        chatController.sender   = sender
        self.navigationController?.show(chatController, sender: nil)
     
    }
  
  func showProfileOfUser() {
    let controller                       = Route.profileViewController
    if let fbId = eventDetail?.organizer?.uid {
        controller.fbId                      = String(describing: fbId)
    }
    controller.isForViewingOthersProfile = true
    let nav  =  UINavigationController.init(rootViewController : controller)
    self.present(nav, animated: true, completion: nil)
  
  }
  
  func showBusinessprofile(businessDetail: BusinessAccountUserModel){
    let controller                       = Route.updateAccountForBusiness
    controller.isForViewingOthersBusinessProfile = true
    controller.businessAccounts = businessDetail
    let nav  =  UINavigationController.init(rootViewController : controller)
    self.present(nav, animated: true, completion: nil)
  }
  
  
  //MARK: Collection Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if collectionView == self.attendeesCollectionView{
      if let friendCount = self.eventDetail?.friendAttending?.count {
        return friendCount
      }
    } else {
      if let media = self.eventDetail?.media {
        return isFromViteFeed ? media.count - 1 : media.count
      }
    }
    return 0
  }
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if collectionView == self.attendeesCollectionView {
      let friendAttendee  =  eventDetail?.friendAttending![indexPath.row]
      let controller = Route.profileViewController
      if let friendFbId = friendAttendee?.facebookId {
        controller.fbId = "\(friendFbId)"
      }
      controller.isForViewingOthersProfile = true
        self.navigationController?.show(controller, sender: nil)
    }
  }
  
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if collectionView == attendeesCollectionView {
      let data = eventDetail?.friendAttending![indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "attendingPeople", for: indexPath as IndexPath) as! AttendingCollectionViewCell
      if let imageUrl = data?.profileImageUrl?.convertIntoViteURL(), let attendeeId = data?.facebookId {
        print(attendeeId)
        if isValidImage(url: imageUrl){
            cell.attendingPeopleImg.sd_setImage(with: URL(string: imageUrl.convertIntoViteURL()), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
        } else {
            let profileUrl = "http://graph.facebook.com/\(attendeeId)/picture?type=large"
            cell.attendingPeopleImg.sd_setImage(with: URL(string: profileUrl), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
        }
      }
      return cell
    }else {
      // for media collection view
        let mediaData = isFromViteFeed ? eventDetail?.media![indexPath.row + 1] : eventDetail?.media![indexPath.row]
      if mediaData?.fileType == "IMAGE" {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCVCell", for: indexPath as IndexPath  ) as! ImageEventDetailCollectionViewCell
        if let url = mediaData?.mediaUrl {
          cell.mediaImgView.sd_setShowActivityIndicatorView(true)
            cell.mediaImgView.sd_setImage(with: URL(string: url.convertIntoViteURL()))
        }
        return cell
      } else {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoCVCell", for: indexPath as IndexPath) as! VideoEventDetailCollectionViewCell
        if let url = mediaData?.thumbNailUrl {
          cell.vidThumbnailImgView.sd_setShowActivityIndicatorView(true)
            cell.vidThumbnailImgView.sd_setImage(with: URL(string: url.convertIntoViteURL()))
        }
        cell.play = {
          if let url = mediaData?.mediaUrl?.convertIntoViteURL(){
            playVideo(url, viewController: self)
            
          }
        }
        return cell
      }
    }
    
  }
  
  //MARK: Server API Call
  // Send request for event
  func userResponseToCard(event: VEvent){
    VEventWebService.setUserResponseForEvent(event, userResponse: UserViteResponse.Requested, successBlock: {
      let evnt     = NEvent(name: event.title!, uid: event.uid!, imageUrl: event.imageUrl!.convertIntoViteURL())
      let sender    = UserDefaultsManager.getActiveUserForNotification()
        let receiver = NReceiver(uid: String(describing: event.organizer!.uid!))
      if let businessUser = event.business {
        let businessUser =  NReceiver(uid: String(businessUser.entityId!))
        NotificationService().triggerNotificationWithEvent(.RequestReceived, event: evnt, sender: sender, receiver: businessUser)
      }
      NotificationService().triggerNotificationWithEvent(.RequestReceived, event: evnt, sender: sender, receiver: receiver)
        self.goToViewController(eventName: event.title!)
        self.view.isUserInteractionEnabled = true
      }, failureBlock: { (message) in
        self.view.isUserInteractionEnabled = true
        print("Failed to respond to event: Error: \(message)")
    })
  }
  
  
  //parameter to check organizer's stripe status
  func getStripeAccountStatus(){
    let fbID = UserDefaultsManager().userFBID!
    WebService.request(method: .get, url: KAPICheckStripeStatus + fbID, success: { (response) in
        guard let response = response.result.value as! [String: AnyObject]? ,let param = response["params"]  as? [String:AnyObject] else {
                    return
                  }
                  guard let success = response["success"] as? Bool else {
                    return
                  }
        if (success) {
             if let cardStatus = param["cardStatus"] as? Bool{
                      if (cardStatus != false){
                        self.userConnectedToStripe = true
                      }
                    }
                  }
        
    }) { (message, apiError) in
        
    }
  }
  
  
//  func scrollViewDidScroll(scrollView: UIScrollView) {
//    if isFromViteFeed {
//      if scrollView.contentOffset.y < -20 {
//        NSNotificationCenter.defaultCenter().postNotificationName("kReachedTableViewTop", object: nil)
//      }
//    }
//  }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        let actualPosition = scrollView.panGestureRecognizer.translation(in: scrollView.superview)
                if scrollView.contentOffset.y == -20 && actualPosition.y > 0 {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kReachedTableViewTop"), object: nil)
                }
            }
    }


//MARK: Share Event
extension EventDetailViewController{
    
    @IBAction func shareEvent(sender: AnyObject) {
        if let id = eventDetail?.uid, let eventTitle = eventDetail?.eventTitle , let desc = eventDetail?.detail, let imgUrl = eventDetail?.imageUrl?.convertIntoViteURL() {
            BranchShareHelper.ShareEventDetail(self, eventID: id, eventTitle: eventTitle, eventDesc: desc, shareText: eventTitle, imageURL: imgUrl)
        }

    }
    
}


