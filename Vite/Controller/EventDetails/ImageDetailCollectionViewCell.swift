//
//  ImageDetailCollectionViewCell.swift
//  Vite
//
//  Created by Eeposit  on 1/25/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit

class ImageDetailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgEvent: UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.imgEvent.layer.cornerRadius = 10
        self.imgEvent.clipsToBounds = true
    }
}
