//
//  VideoEventDetailCollectionViewCell.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 5/2/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class VideoEventDetailCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var vidThumbnailImgView: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    var play: (() -> ())?
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.vidThumbnailImgView.layer.cornerRadius = 10
        self.vidThumbnailImgView.clipsToBounds = true
    }

 
    @IBAction func playVideo(_ sender: Any) {
        if let p = play{
            p()
        }
    }
}
