//
//  FacebookEventViewController.swift
//  Vite
//
//  Created by eeposit2 on 5/15/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
protocol FBEventDataProvider {
    func getEventOfUser(successBlock:@escaping ((_ eventList: [FBUserEvent]) -> Void),
                        failureBlock:@escaping ((_ message:String) -> Void))
    
}

class FacebookEventViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    var eventArr : [FBUserEvent] = [FBUserEvent]()
    let indicator                        = ActivityIndicator()
    var eventCount: Int = 0
    var viteNow:Bool = false
    var fbEventDataProvider: FBEventDataProvider = FacebookEventHelper()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationController?.isNavigationBarHidden = true
        //        self.scrollView.layer.cornerRadius = 7
        //
        //        self.fbEventDataProvider.getEventOfUser({ (eventList) in
        //
        //            self.showEventList(eventList)
        //            self.eventCount = eventList.count
        //            self.pageControl.numberOfPages = self.eventCount
        //            self.pageControl.pageIndicatorTintColor = UIColor.lightGrayColor()
        //        }) { (message) in
        //            self.view.makeToast("Failed to load facebook events")
        //        }
        self.view.backgroundColor = UIColor.clear
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.indicator.start()
        self.fbEventDataProvider.getEventOfUser(successBlock: { (eventList) in
            if eventList.count == 0 {
                 self.indicator.stop()
                self.dismiss(animated: true, completion: nil)
            }else{
                self.indicator.stop()
                for view in self.scrollView.subviews{
                    view.removeFromSuperview()
                }
                self.indicator.tintColor = UIColor.black
                self.indicator.parentView = self.view
                self.pageControl.numberOfPages = 0
                self.scrollView.layer.cornerRadius = 7
                self.showEventList(eventList: eventList)
                self.eventCount = eventList.count
                self.pageControl.numberOfPages = self.eventCount
                self.pageControl.pageIndicatorTintColor = UIColor.lightGray
                           }
            
        }) { (message) in
            self.view.makeToast(message: localizedString(forKey: "FAILED_LOADING_FB_EVENTS"))
            self.indicator.stop()
        }
        
    }
    
    @IBAction func popBack(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        loadPageControl()
    }
    
    
    func loadPageControl(){
        let pageCount =  self.eventCount
        pageControl.currentPage = 0
        pageControl.numberOfPages = pageCount
        let pageWidth =  self.scrollView.frame.size.width
        let page = Int(floor( self.scrollView.contentOffset.x / (pageWidth)))
        pageControl.currentPage = page
    }
    
    func showEventList(eventList:[FBUserEvent]){
        var fbEventIdArr = [String]()
        eventArr = eventList
        for event in eventArr{
            fbEventIdArr.append(event.eventId)
        }
        
        UserDefaults.standard.set(fbEventIdArr, forKey : "fbEventIdUserDefault" )
        var i = 0;
        for event in eventList {
            let frame = CGRect(x: CGFloat(i) * self.scrollView.frame.width, y: 0, width: self.scrollView.frame.width, height: self.scrollView.frame.height)
            let eventView = FbEventView(frame: frame)
            eventView.btnAddEvent.tag = i
            eventView.btnAddEvent.addTarget(self, action: #selector(addFacebookEvent) , for: .touchUpInside)
            //eventView.btnCloseFbEvent.addTarget(self, action: #selector(closePopUp) , forControlEvents: .TouchUpInside)
            eventView.eventImage.layer.cornerRadius = 7
            eventView.eventDetailView.layer.cornerRadius = 7
            
            if event.imageUrl.isEmpty{
                eventView.eventImage.image = UIImage(named: "NoEventCover")
            } else{
                let imageURL = URL(string: event.imageUrl)
                eventView.eventImage.sd_setImage(with: imageURL)
            }
            eventView.eventName.text = event.eventName
            let dateTime = convertDateFormater(date: event.eventStartTime)
            eventView.eventDate.text = dateTime.0
            eventView.eventTime.text = dateTime.1
            self.scrollView.addSubview(eventView)
            i += 1
        }
        
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width * CGFloat(eventList.count), height: self.scrollView.frame.height)
    }
    
    
    
    // convert date format and splited to date and time separately from '2016-05-15T16:00:00+0545' to  ("May 15, 2016 ", " 10:15:00 AM")
    func convertDateFormater(date: String) -> (String,String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        guard let date = dateFormatter.date(from: date) else {
            assert(false, localizedString(forKey: "NO_DATE_STRING"))
            return ("","")
        }
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.timeStyle = .medium
        let dateString = dateFormatter.string(from: date)
        let dateTime:[String] = dateString.components(separatedBy: "at")
        return (dateTime[0],dateTime[1])
    }
    
    func convertDateFormaterForParams(date: String) -> (String,String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        guard let date = dateFormatter.date(from: date) else {
            assert(false, localizedString(forKey: "NO_DATE_STRING"))
            return ("","")
        }
        dateFormatter.dateFormat = "yyy-MM-dd'T'HH:mm:ss"
        let dateString = dateFormatter.string(from: date)
        let dateTime:[String] = dateString.components(separatedBy: "T")
        return (dateTime[0],dateTime[1])
    }
    
    func  closePopUp(sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func addFacebookEvent(sender : UIButton){
        let event = eventArr[sender.tag]
        let controller = Route.CreateEvent
        controller.isFromFbEvent = true
        controller.userFbEvent = event
        self.navigationController?.pushViewController(controller, animated: true)
        //        let coord =
        //            [
        //                "givenLocation":event.city + ", " + event.country,
        //                "longitude":String(event.longitude),
        //                "latitude" :String(event.latitude)
        //        ]
        //
        //        let dateTime = convertDateFormaterForParams(event.eventStartTime)
        //
        //        let parameters = [
        //            "imageUrl":event.imageUrl,
        //            "eventTitle":event.eventName,
        //            "location":coord,
        //            "eventDetail":event.eventDescription,
        //            "viteNow":false,
        //            "bringAFriend":event.canGuestsInvite,
        //            "eventDate":dateTime.0,
        //            "eventStartTime":dateTime.1,
        //            "facebookEventId": event.eventId
        //        ]
        //        print(parameters)
        //
        //        let alert = UIAlertController(title: "Would you like to use ViteNow?",
        //                                      message: "ViteNow will notify all Vite users in your area of the event via push notification",
        //                                      preferredStyle: .Alert)
        //        let okAction = UIAlertAction(title: "Yes",
        //                                     style: .Default,
        //                                     handler: { (action:UIAlertAction) -> Void in
        //                                        self.viteNow = true
        //                                        self.postEventWithParameter(parameters as! [String : AnyObject])
        //        })
        //
        //        let cancelAction = UIAlertAction(title: "No",
        //                                         style: .Default) { (action: UIAlertAction) -> Void in
        //                                            self.viteNow = false
        //                                            self.postEventWithParameter(parameters as! [String : AnyObject])
        //
        //        }
        //        alert.addAction(okAction)
        //        alert.addAction(cancelAction)
        //        presentViewController(alert,
        //                              animated: false,
        //                              completion: nil)
        
        
    }
    
    func postEventWithParameter(parameter:[String:AnyObject]) {
        
        //Update viteNow key
        var updatedParam:[String:AnyObject]!
        updatedParam = parameter
        updatedParam.updateValue(self.viteNow as AnyObject, forKey: "viteNow")
        
        guard let createEventAPI = kAPICreateEvent else {
            return
        }
        WebService.request(method: .post, url: createEventAPI, parameter: updatedParam, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as! [String: AnyObject]? else {
                                return
                            }
            
                            guard let status = response["success"] as? Bool else {
                                return
                
                            }
            
                            if status {
                                self.showToast(message: localizedString(forKey: "EVENT_CREATED_SUCCESSFULLY"))
                               
                            }
        }) { (message, apiError) in
            showAlert(controller: self, title: "", message: message)
        }

    }
    
    func showToast(message:String) {
        // self.back(message)
    }
    
    
    
    
}
