//
//  FbEventView.swift
//  Vite
//
//  Created by eeposit2 on 5/15/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class FbEventView: UIView {
    @IBOutlet var view: UIView!
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var eventTime: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var eventDetailView: UIView!
    @IBOutlet weak var btnAddEvent: UIButton!
    @IBOutlet weak var btnCloseFbEvent: UIButton!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        Bundle.main.loadNibNamed("FBEventView", owner: self, options: nil)
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("FBEventView", owner: self, options: nil)
        view.frame = self.bounds
        eventName.adjustsFontSizeToFitWidth = true
        self.addSubview(view)
    }
    
    
    
}
