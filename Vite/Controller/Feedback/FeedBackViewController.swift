//
//  FeedBackViewController.swift
//  Vite
//
//  Created by eeposit2 on 8/23/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class FeedBackViewController: WrapperController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func back(_ sender: Any) {
        Route.goToHomeController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
