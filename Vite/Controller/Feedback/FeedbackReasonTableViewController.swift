//
//  FeedbackReasonTableViewController.swift
//  Vite
//
//  Created by eeposit2 on 8/21/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class FeedbackReasonTableViewController: WrapperTableViewController {
    
    @IBOutlet var tblFeedback: UITableView!
    let indicator = ActivityIndicator()
    
    var subjectArr =  [VFeedback]()
    // MARK: - Table view data source
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         getFeedBackSubject()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
       
        
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subjectArr.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "feedbackReasonCellIdentifier"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ReasonTableViewCell
        let feedback = self.subjectArr[indexPath.row]
        cell.lblReason.text = feedback.subject
        cell.selectionStyle = .none
        cell.feedbackView.backgroundColor = UIColor.colorFromRGB(rgbValue: 0xd2d2d2)
        cell.feedbackView.layer.cornerRadius = 5
        cell.feedbackView.layer.masksToBounds = true
        cell.tickImgView.isHidden = true
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "reasonVc") as! UserReasonViewController
        let feedback = subjectArr[indexPath.row]
        if let subject  =  feedback.subject{
              controller.subject = subject
        }
        self.navigationController?.show(controller, sender: nil)
    }
    
    func getFeedBackSubject(){
        self.indicator.start()
        VFeedbackWebService.getFeedbackSubject({ (subjectList) in
            self.subjectArr = subjectList
            self.indicator.stop()
            self.tblFeedback.reloadData()
        }) { (message) in
            self.indicator.stop()
            self.view.makeToast(message: message)
        }
    }

}
