//
//  ReasonTableViewCell.swift
//  Vite
//
//  Created by eeposit2 on 8/21/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class ReasonTableViewCell: UITableViewCell {

    @IBOutlet weak var feedbackView: UIView!
    @IBOutlet weak var lblReason: UILabel!
    @IBOutlet weak var tickImgView: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool){
        if selected {
            feedbackView.backgroundColor = UIColor.colorFromRGB(rgbValue: 0x2cabe1)
            tickImgView.isHidden = false
        } else {
            feedbackView.backgroundColor = UIColor.colorFromRGB(rgbValue: 0xd2d2d2)
            tickImgView.isHidden = true
        }
    }
    
}
