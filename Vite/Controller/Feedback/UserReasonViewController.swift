//
//  UserReasonTableViewController.swift
//  Vite
//
//  Created by eeposit2 on 8/21/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class UserReasonViewController: WrapperController,  UITextViewDelegate {
    
    var subject = String()
    var isFromReportEvent = Bool()
    var eventId = String()
    let indicator = ActivityIndicator()
    @IBOutlet weak var textViewLbl: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        IQKeyboardManager.sharedManager().enable = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isFromReportEvent{
            self.navigationItem.title = localizedString(forKey: "REPORT_MSG")
        }
        textView.delegate = self        // to control navigation to be pushed upward by keyboard
        IQKeyboardManager.sharedManager().enable = false
    }
    
    //MARK: PRIVATE METHODS
    func submitFeedback(){
        let feedbackText = textView.text.replacingOccurrences(of: " ", with: "")
        if !feedbackText.isEmpty{
            self.indicator.start()
            let parameter: [String: AnyObject] =  [
                "feedbackSubject" : subject as AnyObject,
                "message" : textView.text as AnyObject
            ]
            
            VFeedbackWebService.submitFeedback(parameter, successBlock: { (message) in
                self.indicator.stop()
                let alert = UIAlertController(title: localizedString(forKey: "THANK_YOU"),
                                              message: localizedString(forKey: "FEEDBACK_SUBMITTED"),
                                              preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK",
                                             style: .default,
                                             handler: {
                                                (action:UIAlertAction) -> Void in
                                                Route.goToHomeController()
                })
                alert.addAction(okAction)
                self.present(alert, animated: false, completion: nil)
            }, failureBlock: { (message) in
                self.view.makeToast(message: message)
            })
            
        }else{
            self.view.makeToast(message: localizedString(forKey: "SUBMIT_REASON"))
        }
    }
    
    func reportEvent(){
        let reportText = textView.text.replacingOccurrences(of: " ", with: "")
        if !reportText.isEmpty{
            self.indicator.start()
            
            let parameter: [String: AnyObject] =  [
                "eventReportSubject" : subject as AnyObject,
                "message" : textView.text as AnyObject
            ]
            VFeedbackWebService.reportEvent(eventId, parameter: parameter, successBlock: { (message) in
                Route.goToHomeController()
                showAlert(controller: self, title: localizedString(forKey: "SUCCESS"), message:localizedString(forKey: "REPORT_SUBMITTED_SUCCESSFULLY"))
                self.indicator.stop()
                self.view.makeToast(message: message)
                }, failureBlock: { (message) in
                    self.indicator.stop()
                    self.view.makeToast(message: message)
            })
        }else{
            self.view.makeToast(message: localizedString(forKey: "SUBMIT_REASON"))
        }
    }
    
    
    //MARK: IBACTION METHODS
    @IBAction func submit(_ sender: AnyObject) {
        if isFromReportEvent{
            reportEvent()
        }else{
           submitFeedback()
        }
    }
    
    @IBAction func back(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK: TEXTVIEW DELEGATES
    func textViewDidBeginEditing(_ textView: UITextView) {
        textViewLbl.isHidden = true
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 10, bottom: 20, right: 20)
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.count == 0{
            textViewLbl.isHidden = false
        }
    }
    
   
}
