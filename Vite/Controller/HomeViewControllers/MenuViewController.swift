//
//  MenuViewController.swift
//  Vite
//
//  Created by Hem Poudyal on 12/29/17.
//  Copyright © 2017 Eepos IT. All rights reserved.
//

import FBSDKLoginKit
import UIKit
import SDWebImage
import ObjectMapper
import SlideMenuControllerSwift

enum TableViewIndex:Int {
    case MyProfile = 0
    case MyEvents  = 1
    case MyVips    = 2
    case Feedback  = 3
    case PaymentHistory = 4
    case BusinessAccount = 5
    case CreateEvent = 6
    case MyVites = 7
}

class MenuViewController: UIViewController {
    var userDefault = UserDefaultsManager()
    var primaryBusinessAccounts:BusinessAccountUserModel!
    var userMenuList = ["My Profile","My Events","My VIPs","Feedback", "Payment History","Business Accounts"]
    
    @IBOutlet weak var settingButton: UIButton!
    @IBOutlet weak var vitePlusAccountButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var scanTicketButton: UIButton!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var switchAccountButton: UIButton!
    @IBOutlet weak var vitePlusImg: UIImageView!
    @IBOutlet weak var userProfileBtn: UIButton!
    
    @IBOutlet weak var menuTableView: UITableView! {
        didSet {
            self.menuTableView.dataSource = self
            self.menuTableView.delegate   = self
            self.menuTableView.rowHeight  = 50.0
            self.menuTableView.backgroundColor = UIColor(rgb:AppProperties.viteTableThemeColor)
        }
    }
    
    @IBOutlet weak var profileImage: UIImageView! {
        didSet {
            self.profileImage.layer.cornerRadius = self.profileImage.frame.width / 2
            self.profileImage.layer.borderColor = UIColor.white.cgColor
            self.profileImage.clipsToBounds = true
        }
    }
    
    @IBAction func settingsBtnAction(_ sender: UIButton) {
        self.slideMenuController()?.changeMainViewController(Route.settingsNav, close: true)
    }
    @IBOutlet weak var businessProfileImage: UIImageView! {
        didSet {
            self.businessProfileImage.layer.cornerRadius = self.businessProfileImage.frame.width / 2
            self.businessProfileImage.layer.borderColor = UIColor.white.cgColor
            self.businessProfileImage.clipsToBounds = true
        }
    }
    
    var eventArr = [EventsOrganizer]()
    var eventCount =  Int()
    let indicator = ActivityIndicator()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        userDetail()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getUserInfo()
        geteventListForQr()
        if userDefault.isBusinessAccountActivated && self.userMenuList.count == 6 {
            self.userMenuList.removeLast()
        }
    }
    
    @IBAction func switchAccountAction(_ sender: Any) {
        animate()
        let businessAccount = UserAccountFetcher().fetchBusinessAccount()
        let userPersonalAccount = UserAccountFetcher().fetchpersonalAccount()
        
        if userDefault.isBusinessAccountActivated {
            userDefault.isBusinessAccountActivated = false
            self.makePersonalAccountPrimary(business: businessAccount, personal: userPersonalAccount)
            
        } else {
            self.makeBusinessAccountPrimary(business: businessAccount, personal: userPersonalAccount)
            userDefault.isBusinessAccountActivated = true
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "switchAccount"), object: nil)
        self.menuTableView.reloadData()
    }
    
    @IBAction func vitePlusAccount(_ sender: Any) {
        if let premStatus = userDefault.userPremiumStatus {
            if premStatus {
                    self.slideMenuController()?.changeMainViewController(Route.profileNav, close: true)
            } else {
                self.present(Route.premiumViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func scanTicket(_ sender: UIButton) {
        self.slideMenuController()?.changeMainViewController(Route.scanTicketNav, close: true)
    }
//    @IBAction func showProfile(_ sender: Any) {
//        self.slideMenuController()?.changeMainViewController(Route.profileNav, close: true)
//    }
    
}
//MARK: Private Function
extension MenuViewController {
    
    private func getUserInfo() {
        UserService.getUserInfo(successBlock: { (userInfo) in
            if  let name = userInfo.fullName ,
                let imageUrl = userInfo.profileImageUrl ,
                let userGender = userInfo.gender,
                let age  = userInfo.age,
                let premiumStatus = userInfo.premiumStatus
            {
                self.userDefault.userFBFullName = name
                self.userDefault.userFBProfileURL = imageUrl
                self.userDefault.userFBGender = userGender
                self.userDefault.userAge = age
                self.userDefault.userPremiumStatus = premiumStatus
                self.vitePlusImg.image = premiumStatus ? UIImage(named: "vitePlus") : UIImage(named: "GetVitePlus")
                
            }
            if let businessBlock = userInfo.business {
                self.primaryBusinessAccounts = businessBlock
                if let members = self.primaryBusinessAccounts.businessMembers {
                    let otherAdmins = members.filter({ (accounts) -> Bool in
                        return accounts.admin!
                    })
                    
                    if otherAdmins.count > 0 {
                        //someone else is owner of business account
                        self.userDefault.userBusinessOwnerId = String(describing: otherAdmins.first!.facebookId!)
                    } else {
                        //user is owner of business account
                        self.userDefault.userBusinessOwnerId = self.userDefault.userFBID
                    }
                } else {
                    //user is owner of business account
                    self.userDefault.userBusinessOwnerId = self.userDefault.userFBID
                }
                
                if let id = self.primaryBusinessAccounts.entityId {
                    self.userDefault.userBusinessAccountID = id
                }
                if let name =  self.primaryBusinessAccounts.businessName {
                    self.userDefault.userBusinessFullName = name
                }
                if let url = self.primaryBusinessAccounts.profileImage {
                    self.userDefault.userBusinessProfileImageUrl = url
                }
                self.showBusinessAccount()
            } else {
                self.businessProfileImage.isHidden = true
                self.switchAccountButton.isHidden = true
            }
            self.checkUserAccountAndBusinessAccount()
        }) { (message) in
            checkIfUserLoggedInFromAnotherDevice(msg: message, ctrl: self)
        }
    }
    
    func geteventListForQr() {
        EventServices.getEventListForQrScan(sucessBlock: { (eventList, eventCount) in
            self.eventArr = eventList
            self.eventCount = eventCount
            print(eventCount)
        }) { (message) in
            showAlert(controller: self, title:localizedString(forKey: "VITE_EXCLUSIVE_EVENT"), message: message)
            self.indicator.stop()

        }
    }
    
    func showBusinessAccount(){
        if let url = userDefault.userBusinessProfileImageUrl{
            self.businessProfileImage.sd_setImage(with:URL(string: (url.convertIntoViteURL())))
        }
        self.switchAccountButton.isHidden = false
        self.businessProfileImage.isHidden = false
    }
    
    func checkUserAccountAndBusinessAccount() {
        if userDefault.isBusinessAccountActivated {
            self.profileImage.sd_setImage(with:(URL(string: (userDefault.userBusinessProfileImageUrl?.convertIntoViteURL())!)))
            self.userNameLbl.text =   userDefault.userBusinessFullName
            self.genderLbl.text = ""
            self.vitePlusAccountButton.isHidden = true
            self.vitePlusImg.isHidden = true
            self.settingButton.isHidden = true
            self.businessProfileImage.sd_setImage(with:(URL(string: (userDefault.userFBProfileURL?.convertIntoViteURL())!)))
        } else {
            self.userDetail()
            if let imgUrl = userDefault.userBusinessProfileImageUrl {
                self.businessProfileImage.sd_setImage(with:(URL(string: (imgUrl.convertIntoViteURL()))))
            } else {
                self.businessProfileImage.image = UIImage(named: "ProfilePlaceholder")
            }
            self.profileImage.sd_setImage(with:(URL(string: (userDefault.userFBProfileURL?.convertIntoViteURL())!)!), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
            self.vitePlusAccountButton.isHidden = false
            self.vitePlusImg.isHidden = false
            self.settingButton.isHidden = false
            
        }
        self.menuTableView.reloadData()
    }
    
    func userDetail () {
        //if the characters of username is greater than 20 then the first name displays as the name
        if (userDefault.userFBFirstName != nil && ((userDefault.userFBFullName?.count)! > 15)) {
            userNameLbl.text = userDefault.userFBFirstName!
        } else {
            userNameLbl.text = userDefault.userFBFullName!
        }
        //if the user age or gender is nil, the text doesn't appear
        if(userDefault.userFBGender != nil && userDefault.userAge != nil) {
            genderLbl.text = "\(userDefault.userFBGender!), \(userDefault.userAge!)"
        } else {
            genderLbl.text = ""
        }
        profileButton.addTarget(self, action:#selector(self.showEditDetails), for: .touchUpInside)
        userProfileBtn.addTarget(self, action:#selector(self.showEditDetails), for: .touchUpInside)
        
    }
    
    func makeBusinessAccountPrimary(business:UserMiniModel,personal:UserMiniModel) {
        self.profileImage.sd_setImage(with: (URL(string: (business.profileImageURL.convertIntoViteURL()))))
        userNameLbl.text = business.fullName.uppercaseFirst
        genderLbl.text = ""
        self.vitePlusAccountButton.isHidden = true
        self.vitePlusImg.isHidden = true
        self.settingButton.isHidden = true
        if self.userMenuList.count == 6 {
        self.userMenuList.removeLast()
        }
        self.businessProfileImage.sd_setImage(with: (URL(string: (personal.profileImageURL.convertIntoViteURL()))))
    }
    
    func makePersonalAccountPrimary(business:UserMiniModel,personal:UserMiniModel) {
        self.userDetail()
        self.businessProfileImage.sd_setImage(with: (URL(string: (business.profileImageURL.convertIntoViteURL()))))
        self.profileImage.sd_setImage(with: (URL(string: (personal.profileImageURL.convertIntoViteURL()))))
        self.vitePlusAccountButton.isHidden = false
        self.vitePlusImg.isHidden = false
        self.settingButton.isHidden = false
        if self.userMenuList.count == 5 {
        self.userMenuList.append("Business Accounts")
        }
    }
    
    @objc func showEditDetails(){
        self.view.isUserInteractionEnabled = false
        if !userDefault.isBusinessAccountActivated {
            let controller = Route.profileNav
            self.slideMenuController()?.changeMainViewController(controller, close: true)
          } else {
            let controller = Route.updateAccountForBusiness
            controller.businessAccounts = self.primaryBusinessAccounts
            controller.isFromSideMenu = true
            let nav = UINavigationController.init(rootViewController: controller)
            self.slideMenuController()?.changeMainViewController(nav, close: true)
        }
    }

    func animate() {
        let xPositionProfile = profileImage.frame.origin.x
        let yPositionProfile = profileImage.frame.origin.y
        let heightprofile = profileImage.frame.size.height
        let widthProfile = profileImage.frame.size.width
        let xPositionBusiness = businessProfileImage.frame.origin.x
        let yPositionBusiness = businessProfileImage.frame.origin.y
        let heightBusiness = businessProfileImage.frame.size.height
        let widthBusiness = businessProfileImage.frame.size.width
        self.profileImage.frame = CGRect(x: xPositionBusiness, y: yPositionBusiness, width: widthBusiness, height: heightBusiness)
        UIView.animate(withDuration: 0.6, animations: {
            self.profileImage.frame = CGRect(x: xPositionProfile, y: yPositionProfile, width: widthProfile, height: heightprofile)
        }, completion: nil)
    }

}

//MARK: UITableViewDataSource
extension MenuViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userMenuList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "MenuTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MenuTableViewCell
        cell.menuListName.text = userMenuList[indexPath.row]
        return cell
    }
}
extension MenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRowAtIndexPath(indexPath, animated: true)
        //tableView.userInteractionEnabled = false
        switch indexPath.row {
        case TableViewIndex.MyProfile.rawValue:
            if !userDefault.isBusinessAccountActivated {
                self.slideMenuController()?.changeMainViewController(Route.profileNav, close: true)
            }else{
                let controller = Route.updateAccountForBusiness
                controller.businessAccounts = self.primaryBusinessAccounts
                controller.isFromSideMenu = true
                let nav = UINavigationController.init(rootViewController: controller)
                self.slideMenuController()?.changeMainViewController(nav, close: true)
            }
            
        case TableViewIndex.MyEvents.rawValue:
            self.slideMenuController()?.changeMainViewController(Route.MyEvents, close: true)
        case TableViewIndex.MyVips.rawValue:
            self.slideMenuController()?.changeMainViewController(Route.myVipNav, close: true)
        case TableViewIndex.PaymentHistory.rawValue:
            self.slideMenuController()?.changeMainViewController(Route.paymentNav, close: true)
        case TableViewIndex.Feedback.rawValue:
           self.slideMenuController()?.changeMainViewController(Route.feedbackNav, close: true)
        case TableViewIndex.BusinessAccount.rawValue:
            self.slideMenuController()?.changeMainViewController(Route.businessAccNav, close: true)
        default:
            break;
        }
        
    }
    
}
