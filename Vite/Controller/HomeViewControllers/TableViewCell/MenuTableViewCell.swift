//
//  MenuTableViewCell.swift
//  Vite
//
//  Created by Hem Poudyal on 1/2/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var menuListName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor(rgb:AppProperties.viteTableThemeColor)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
