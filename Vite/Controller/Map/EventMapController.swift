//
//  EventMapController.swift
//  TicketHub
//
//  Created by Eeposit1 on 1/26/16.
//  Copyright © 2016 eeposit. All rights reserved.
//

import UIKit
import MapKit

protocol HandleMapSearch {
    func dropPinZoomIn(placemark:MKPlacemark)
}

class EventMapController:UIViewController
, MKMapViewDelegate, UISearchControllerDelegate {
    
    var isForPremium = false
    var isFromMyEventMenu = false
    @IBOutlet weak var btnCurrentLocation: UIButton!
    @IBOutlet weak var lblInstruction: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    var currentLocation:CLLocationCoordinate2D?
    var selectedCoordinate:CLLocationCoordinate2D?
    var timer:Timer?
    var resultSearchController:CustomSearchController? = nil
    var currentCoordinateOfAnnotation:((_ coordinate:CLLocationCoordinate2D) -> ())?
    var locationManager:LocationManager? = LocationManager()
    //var selectedPin:MKPlacemark? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.mapView.delegate = self
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(mapViewDidTap))
        self.mapView.addGestureRecognizer(tapGestureRecognizer)
        
        //Current location of user is selected location initially by default
        self.selectedCoordinate = currentLocation!
        mapView.showsUserLocation = true
        
        let span = MKCoordinateSpanMake(0.01, 0.01)
        let region = MKCoordinateRegion(center: currentLocation!, span: span)
        mapView.setRegion(region, animated: true)
        
        //Set up the UISearchController, Set up the search results table
        if #available(iOS 9.3, *) {
            let newSearchTable = NewLocationSearchTable()
            newSearchTable.isFromMyEventMenu = isFromMyEventMenu
            resultSearchController = CustomSearchController(searchResultsController: newSearchTable)
            resultSearchController?.searchResultsUpdater = newSearchTable
            newSearchTable.mapView = mapView
            newSearchTable.handleMapSearchDelegate = self
        } else {
            let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "LocationSearchTable") as! LocationSearchTable
            resultSearchController = CustomSearchController(searchResultsController: locationSearchTable)
            resultSearchController?.searchResultsUpdater = locationSearchTable
            locationSearchTable.mapView = mapView
            locationSearchTable.handleMapSearchDelegate = self
        }
        
        
        //  Set up the search bar
        
        let searchBar = resultSearchController!.searchBar
        searchBar.sizeToFit()
        searchBar.barTintColor = UIColor.white
        searchBar.placeholder = "Search for places"
        navigationItem.titleView = resultSearchController?.searchBar
        
        // Configure the UISearchController appearance
        
        resultSearchController?.hidesNavigationBarDuringPresentation = false
        resultSearchController?.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        if isForPremium {
            btnCurrentLocation.isHidden = false
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(EventMapController.hideInstruction), userInfo: nil, repeats: false)
        self.lblInstruction.isHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
    }
    
    @objc func hideInstruction() {
        self.lblInstruction.isHidden = true
        timer?.invalidate()
    }
    
    func getAddress(fromCoordinate: CLLocationCoordinate2D, callback: @escaping (String?, String?) -> ()) {
        let location = CLLocation(latitude: fromCoordinate.latitude, longitude: fromCoordinate.longitude)
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
            guard let placemark = placemarks?.first else {
                callback(nil, nil)
                return
            }
            let name = placemark.name ?? ""
            let city = placemark.locality ?? ""
            let state = placemark.administrativeArea ?? ""
            callback(name, "\(city), \(state)")
        })
    }
    
    
    
    @objc func mapViewDidTap(gestureRecognizer: UITapGestureRecognizer) {
        removeAllAnnotations()
        addAnnotation(gestureRecognizer: gestureRecognizer)
    }
    
    func removeAllAnnotations() {
        mapView.removeAnnotations(mapView.annotations)
    }
    
    func addAnnotation(gestureRecognizer: UITapGestureRecognizer) {
        let touchPoint = gestureRecognizer.location(in: mapView)
        selectedCoordinate = mapView.convert(touchPoint, toCoordinateFrom: mapView)
        getAddress(fromCoordinate: selectedCoordinate!, callback: { name, cityAndState in
            let annotation = MKPointAnnotation()
            annotation.coordinate = self.selectedCoordinate!
            if let name = name, let cityAndState = cityAndState {
                annotation.title = name
                annotation.subtitle = cityAndState
            }
            self.mapView.addAnnotation(annotation)
            self.updateSearchBar(annotation.title ?? "", annotation.subtitle ?? "")
        })
    }
    
    @IBAction func getCurrentLocation(_ sender: Any) {
        mapView.showsUserLocation = true
        let span = MKCoordinateSpanMake(0.01, 0.01)
        locationManager?.getUserLocation()
        locationManager?.fetchedUserLocation = {
            location in ()
            let region = MKCoordinateRegion(center: location, span: span)
            self.mapView.setRegion(region, animated: true)
            self.addCurrentLocation(location: location)
        }
    }
    
    func addCurrentLocation(location: CLLocationCoordinate2D) {
        getAddress(fromCoordinate: location, callback: { name, cityAndState in
            let annotation = MKPointAnnotation()
            annotation.coordinate = location
            if let name = name, let cityAndState = cityAndState {
                annotation.title = name
                annotation.subtitle = cityAndState
            }
            self.mapView.addAnnotation(annotation)
            self.updateSearchBar(annotation.title ?? "", annotation.subtitle ?? "")
        })
    }

    
    func updateSearchBar(_ title: String, _ subtitle: String) {
        let searchBar = resultSearchController!.searchBar
        searchBar.sizeToFit()
        searchBar.barTintColor = UIColor.white
        print(title)
        print(subtitle)
        searchBar.text = "\(title), \(subtitle)"
        searchBar.placeholder = "\(title), \(subtitle)"
        navigationItem.titleView = resultSearchController?.searchBar
    }
    
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        for view in views {
            view.isDraggable = true
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var v : MKAnnotationView! = nil
        if let t = annotation.title, t == "Park here" {
            let ident = "greenPin"
            v = mapView.dequeueReusableAnnotationView(withIdentifier: ident)
            if v == nil {
                v = MKPinAnnotationView(
                    annotation:annotation, reuseIdentifier:ident)
                (v as! MKPinAnnotationView).pinTintColor =
                    MKPinAnnotationView.greenPinColor()
                v.canShowCallout = true
                v.isDraggable = true
            }
            v.annotation = annotation
        }
        return v
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        self.selectedCoordinate = mapView.centerCoordinate
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,didChange newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
        switch (newState) {
        case .starting:
            break
        //print("Dragging")
        case .ending, .canceling:
            guard let coordinate = view.annotation?.coordinate else {
                return
            }
            self.selectedCoordinate = coordinate
        default: break
        }
    }
    
    
    @IBAction func back(_ sender: Any) {
        self.pop()
    }
    
    func pop() {
        guard let nav = self.navigationController else {
            return
        }
        nav.popViewController(animated: true)
    }
    
    //    @IBAction func done(sender: AnyObject) {
    //
    //
    //    }
    @IBAction func done(_ sender: Any) {
        self.pop()
        guard let currentCoordinateOfAnnotation = currentCoordinateOfAnnotation else {
            return
        }
        currentCoordinateOfAnnotation(self.selectedCoordinate!)
    }
    
}

extension EventMapController: HandleMapSearch {
    func dropPinZoomIn(placemark:MKPlacemark){
        // cache the pin
        //selectedPin = placemark
        // clear existing pins
        removeAllAnnotations()
        self.selectedCoordinate = placemark.coordinate
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        annotation.title = placemark.name
        if let city = placemark.locality,let state = placemark.administrativeArea {
            annotation.subtitle = "\(city) \(state)"
        }
        let span = MKCoordinateSpanMake(0.01, 0.01)
        let region = MKCoordinateRegionMake(placemark.coordinate, span)
        mapView.setRegion(region, animated: true)
        mapView.addAnnotation(annotation)
        self.updateSearchBar(annotation.title ?? "", annotation.subtitle ?? "")
    }
}

class CustomSearchBar: UISearchBar {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setShowsCancelButton(false, animated: false)
    }
}

class CustomSearchController: UISearchController, UISearchBarDelegate {
    lazy var _searchBar: CustomSearchBar = {
        [unowned self] in
        let result = CustomSearchBar(frame: CGRect.zero)
        result.delegate = self
        
        return result
        }()
    
    override var searchBar: UISearchBar {
        get {
            return _searchBar
        }
    }
}

