
import Foundation
import UIKit
import MapKit

@available(iOS 9.3, *)
class NewLocationSearchTable : UITableViewController {
    
    var isFromMyEventMenu = true
    var matchingItems:[MKMapItem] = []
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    var mapView: MKMapView? = nil
    
    var handleMapSearchDelegate:HandleMapSearch? = nil
    
    var searchCompleter = MKLocalSearchCompleter()
    var searchResults = [MKLocalSearchCompletion]()
    var newView = UIView()
    var reftableView: UITableView!
    var indicator = ActivityIndicator()
    
    override func viewDidLoad() {
        addIndicatorView()
        searchCompleter.delegate = self
        reftableView = self.tableView
        self.view = newView
        newView.backgroundColor = UIColor.white
        self.view.addSubview(reftableView)
        reftableView.frame = isFromMyEventMenu ? self.view.frame.offsetBy(dx: 0, dy: 0) :  self.view.frame.offsetBy(dx: 0, dy: 80)
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    func addIndicatorView(){
        myActivityIndicator.center = CGPoint(x: view.center.x, y: view.center.y - 100)
        view.addSubview(myActivityIndicator)
    }
    
    func startIndicator(){
        self.myActivityIndicator.startAnimating()
        self.myActivityIndicator.isHidden = false
    }
    
    func stopIndicator(){
        self.myActivityIndicator.isHidden = true
        self.myActivityIndicator.startAnimating()
    }
    
    
    //Set up the Table View Data Source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchResult = searchResults[indexPath.row]
        let cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = searchResult.title
        cell.detailTextLabel?.text = searchResult.subtitle
        return cell
    }
    
    
    override  func tableView(_ tableView: UITableView, didSelectRowAt  indexPath: IndexPath) {
        let completion = searchResults[indexPath.row]
        let searchRequest = MKLocalSearchRequest(completion: completion)
        let search = MKLocalSearch(request: searchRequest)
        
        search.start { (response, error) in
            guard  let mark = response?.mapItems[0].placemark else {return}
            self.handleMapSearchDelegate?.dropPinZoomIn(placemark: mark)
        }
        dismiss(animated: true, completion: nil)
    }
}

@available(iOS 9.3, *)

extension NewLocationSearchTable: MKLocalSearchCompleterDelegate {
    
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        searchResults = completer.results
        stopIndicator()
        reftableView.reloadData()
    }
    
}


@available(iOS 9.3, *)
extension NewLocationSearchTable : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard
            let searchBarText = searchController.searchBar.text else { return }
        
        if searchBarText != " "{
            startIndicator()
            searchCompleter.queryFragment = searchBarText
            
        }
    }
}

