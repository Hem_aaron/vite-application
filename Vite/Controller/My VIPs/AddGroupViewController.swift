////
////  AddGroupViewController.swift
////  Vite
////
////  Created by Eeposit 01 on 4/4/17.
////  Copyright © 2017 EeposIT_X. All rights reserved.
////
//
import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class AddGroupViewController: WrapperController , UITextFieldDelegate , AddMyVipsToGroupViewControllerDelegate  , UITableViewDelegate , UITableViewDataSource {
    var titleTextField : UITextField! = UITextField(frame: CGRect(x: 0, y: 0, width: 200, height: 30))
    var btnDone = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    var btnAdd  = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    var btnBack = UIButton(frame: CGRect(x: 0, y: 0, width: 15, height: 25))
    var myVipList: [MyVIPFriend] = [MyVIPFriend]()
    var category : VipGroupCategory!
    let indicator     = ActivityIndicator()
    var isFromAddMyVip = false
    var listCategoryVip : VipGroupCategory!
    var vipFriends = [MyVIPFriend]()
    var isFromVipsViewController = Bool()
    var color = UIColor.colorFromRGB(rgbValue: 0x6d2b6b)
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var instructionImg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleTextField.delegate = self
        self.tableView.delegate = self
        self.tableView.dataSource = self
        if isFromAddMyVip {
            self.titleTextField.text = self.category.categoryName
        } else {
            setUpTitleTextfieldView()
        }
        goBackSetup()
        self.tableView.tableFooterView = UIView()
        self.titleTextField.addTarget(self, action: #selector(textFieldDidEndEditing), for: .editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if  self.isFromAddMyVip  || self.isFromVipsViewController {
            self.instructionImg.isHidden = true
            self.titleTextField.isEnabled = false
            self.vipFriends.removeAll()
            listVipByCategory()
        }else {
            self.instructionImg.isHidden = false
        }
    }
    
    //MARK: Private Function
    
    func doneRightBarButton () {
        
        self.btnDone.addTarget(self, action: #selector(rightDoneBarButtonAction), for: .touchUpInside)
        self.btnDone.setTitle("Done", for: UIControlState())
        btnDone.setTitleColor(color, for: UIControlState())
        self.navigationItem.rightBarButtonItem  = UIBarButtonItem(customView: self.btnDone)
    }
    
    func addRightBarButton (){
        
        self.btnAdd.addTarget(self, action: #selector(rightAddBarButtonAction), for: .touchUpInside)
        self.btnAdd.setBackgroundImage(UIImage(named: "AddVip"), for:UIControlState())
        self.navigationItem.rightBarButtonItem  = UIBarButtonItem(customView: self.btnAdd)
    }
    
    func goBackSetup(){
        self.btnBack.addTarget(self, action: #selector(goBackBtnAction), for: .touchUpInside)
        self.btnBack.setBackgroundImage(UIImage(named: "Back"), for:UIControlState())
        self.navigationItem.leftBarButtonItem  = UIBarButtonItem(customView: self.btnBack)
    }
    
    @objc func rightDoneBarButtonAction() {
        
        self.navigationItem.title     = self.titleTextField.text
        self.titleTextField.resignFirstResponder()
        addRightBarButton()
        if titleTextField.text?.count > 0 {
            setCategory()
        }
    }
    
    @objc func rightAddBarButtonAction() {
        if titleTextField.text?.count > 0  {
            let storyboard = UIStoryboard(name: "MyVip", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "addMyVips") as! AddMyVipsToGroupViewController
            controller.myVipList = self.myVipList
            controller.delegate = self
            controller.vipGroupCategoryFriend = self.vipFriends
            if let categoryId = self.category.entityId{
                controller.categoryId = categoryId
            }
            self.navigationController?.show(controller, sender: nil)
        }
    }
    
    
    @objc func goBackBtnAction() {
        let storyboard = UIStoryboard(name: "MyVip", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "myVipNav")
        self.slideMenuController()?.changeMainViewController(controller, close: true)
    }
    
    func setUpTitleTextfieldView() {
        let font = UIFont(name: "Avenir-Light", size: 19.0)
        self.navigationItem.titleView = self.titleTextField
        self.titleTextField.attributedPlaceholder =  NSAttributedString(string:"Enter Group Name", attributes:[NSAttributedStringKey.foregroundColor:  UIColor.lightGray,NSAttributedStringKey.font :font!])
        self.titleTextField.textAlignment = .center
        self.titleTextField.font = font
        self.titleTextField.tintColor =  color
        self.titleTextField.textColor =  color
        self.navigationItem.title     = ""
        if isFromVipsViewController  {
            self.titleTextField.text = category.categoryName
            self.titleTextField.isEnabled = false
            self.addRightBarButton()
        } else {
            self.titleTextField.isEnabled = true
            self.titleTextField.becomeFirstResponder()
            if titleTextField.text?.isEmpty == true {
                self.doneRightBarButton()
                btnDone.isEnabled = false
            }else {
                self.doneRightBarButton()
            }
        }
    }
    
    //MARK: TextFieldDelegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        // To Handel Space Characters Only
        let noSpace = textField.text!.replacingOccurrences(of: " ", with: "")
        if noSpace.count > 0 {
            self.btnDone.isEnabled = true
        }else {
            self.btnDone.isEnabled = false
        }
    }
    //MARK: AddMyVipsToGroupViewControllerDelegate
    func getVipCategoryId(_ categoryId: String, isfromAddMyVip: Bool) {
        self.isFromAddMyVip = isfromAddMyVip
    }
    
    //MARK: API call
    func listVipByCategory() {
        guard let categoryId = category.entityId else {
            showAlert(controller: self, title: "", message: localizedString(forKey: "LIST_NOT_FOUND"))
            return
        }
        
        VipGroupService.listVipByCategory(categoryId, successBlock: { (vipListByCategory) in
            self.listCategoryVip = vipListByCategory
            guard let vipFriends = self.listCategoryVip.friends else {
                return
            }
            for vipList in vipFriends {
                self.vipFriends.append(vipList)
            }

            if self.vipFriends.count == 0 {
                self.instructionImg.isHidden = false
            }else {
                self.instructionImg.isHidden = true
            }

            self.tableView.reloadData()

        }) { (message) in
            showAlert(controller: self, title: "", message: message)
        }
    }
    
    func setCategory() {
        self.indicator.start()
        guard let categoryName = titleTextField.text else {return}
        let noSpace = categoryName.replacingOccurrences(of: " ", with: "")
        
        self.btnAdd.isEnabled = false
        let parameter : [String:AnyObject] = [
            
            "categoryName" : categoryName as AnyObject
        ]
        if noSpace.count > 0  {
            VipGroupService.createGroupCategory(parameter, successBlock: { (category) in
                self.category = category
                self.btnAdd.isEnabled = true
                self.indicator.stop()
            }) { (message) in
                showAlert(controller: self, title: "", message: message)
                
                self.indicator.stop()
            }
        } else {
            self.titleTextField.text = ""
        }
    }
    
    @objc func deleteVipFromGroup(_ sender : UIButton) {
        indicator.start()
        self.view.isUserInteractionEnabled = false
        guard let categoryId = category.entityId else {return}
        let aFriend =  self.vipFriends[Int(sender.tag)]
        guard let friendsFbId = aFriend.facebookIdInt else {return}
        let userFriends = [String(describing: friendsFbId)]
        let parameters:[String:AnyObject] = [
            "fbIdList" :userFriends as AnyObject
        ]
        
        VipGroupService.deleteVipFromCategory(categoryId, parameter: parameters, successBlock: { (successMsg) in
            self.view.makeToast(message:successMsg)
            self.vipFriends.remove(at: Int(sender.tag))
            self.tableView.reloadData()
            self.indicator.stop()
            self.view.isUserInteractionEnabled = true
            
        }) { (message) in
            showAlert(controller: self, title: "", message: message)
            self.view.isUserInteractionEnabled = true
            self.indicator.stop()
        }
    }
    
}
extension AddGroupViewController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.vipFriends.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let vipFriendList  = self.vipFriends[indexPath.row]
        let cell = tableView.dequeueReusableCell( withIdentifier: "addGroupcell" , for: indexPath) as! AddGroupListTableViewCell
        cell.vipName.text = vipFriendList.fullName
        if let url = vipFriendList.profileImageUrl{
            cell.vipImg.sd_setImage(with: URL(string: url.convertIntoViteURL()), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
        }
        cell.btnReject.tag = indexPath.row
        cell.btnReject.addTarget(self, action: #selector(self.deleteVipFromGroup(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let controller = Route.profileViewController
        let vipFriends = self.vipFriends[indexPath.row]
        if let facebookId = vipFriends.facebookIdInt {
            controller.fbId = String(describing: facebookId)
        }
        controller.isForViewingOthersProfile = true
        let nav = UINavigationController.init(rootViewController: controller)
        self.navigationController?.show(nav, sender: nil)
     }
    
}

