////
////  AddMyVipsToGroupViewController.swift
////  Vite
////
////  Created by Eeposit 01 on 4/4/17.
////  Copyright © 2017 EeposIT_X. All rights reserved.
////
//
import UIKit

protocol AddMyVipsToGroupViewControllerDelegate {
  func getVipCategoryId (_ categoryId : String , isfromAddMyVip : Bool)
}

class AddMyVipsToGroupViewController: WrapperController {
  
  @IBOutlet weak var lblNoVipMsgTxt: UILabel!
  @IBOutlet weak var tableView: UITableView!
  
  var myVipList: [MyVIPFriend] = [MyVIPFriend]()
  var btnBack = UIButton(frame: CGRect(x: 0, y: 0, width: 15, height: 25))
  var categoryId : String?
  let indicator = ActivityIndicator()
  var selectedFriendsArr = [MyVIPFriend]()
  var delegate : AddMyVipsToGroupViewControllerDelegate!
  var vipGroupCategoryFriend :  [MyVIPFriend] = [MyVIPFriend]()
  var vipListFromCategory: [MyVIPFriend] = [MyVIPFriend]()
  var filteredVip: [MyVIPFriend] = [MyVIPFriend]()
  override func viewDidLoad() {
    super.viewDidLoad()
    vipListFromCategory = vipGroupCategoryFriend
    self.tableView.delegate = self
    self.tableView.dataSource = self
    self.navigationItem.title     = "Add VIPs"
    self.tableView.tableFooterView = UIView()
    goBackSetup()
    filteredVipList()
    showMessage()
  }
  
  func showMessage () {
    if filteredVip.count == 0 {
      self.tableView.isHidden = true
      self.lblNoVipMsgTxt.isHidden = false
    }else {
      self.lblNoVipMsgTxt.isHidden = true
      self.tableView.isHidden = false
    }
  }
  
  // Filtering viplist to show non-added vip list
  func filteredVipList () {
    for vipFriend in myVipList {
      let flag = vipListFromCategory.contains(where: { (categoryVipfriend) -> Bool in
        return vipFriend.facebookIdInt == categoryVipfriend.facebookIdInt
        
      })
      if flag {
        let index = myVipList.index(where: { (friend) -> Bool in
          return friend.facebookIdInt == vipFriend.facebookIdInt
        })
        myVipList.remove(at: index!)
      }
    }
    filteredVip = myVipList
    self.tableView.reloadData()
  }
  
  func goBackSetup(){
    self.btnBack.addTarget(self, action: #selector(goBackBtnAction), for: .touchUpInside)
    self.btnBack.setBackgroundImage(UIImage(named: "Back"), for:UIControlState())
    self.navigationItem.leftBarButtonItem  = UIBarButtonItem(customView: self.btnBack)
  }
  
    @objc func goBackBtnAction() {
    guard let nav = self.navigationController else {
      return
    }
    nav.popViewController(animated: true)
    if let delegate = self.delegate  , let categoryId =  self.categoryId{
      delegate.getVipCategoryId(categoryId, isfromAddMyVip: true)
    }
  }
  
  func checkIfVIPListed(_ myfriend:MyVIPFriend) -> Bool {
    for friend in self.selectedFriendsArr {
      if myfriend.facebookIdInt == friend.facebookIdInt {
        return true
      }
    }
    return false
  }
  
    @objc func addMyVipInGroup(_ sender: UIButton) {
    self.view.isUserInteractionEnabled = false
    guard let categoryId = self.categoryId else {return}
    let aFriend =  self.filteredVip[Int(sender.tag)]
    self.selectedFriendsArr.append(aFriend)
    guard let friendsFbId = aFriend.facebookIdInt else {return}
    let userFriends = [String(describing: friendsFbId)]
    let parameters:[String:AnyObject] = [
      "fbIdList" :userFriends as AnyObject
    ]
    
    self.indicator.start()
    VipGroupService.addVipToGroupCategory(categoryId, parameter: parameters, successBlock: { (successMsg) in
        self.view.makeToast(message: successMsg)
      self.filteredVip.remove(at: Int(sender.tag))
      self.tableView.reloadData()
      self.view.isUserInteractionEnabled = true
      self.indicator.stop()
    }) { (message) in
        showAlert(controller: self, title: "", message: message)
      self.view.isUserInteractionEnabled = true
      self.indicator.stop()
    }
    
    
  }
  
}
extension AddMyVipsToGroupViewController: UITableViewDelegate , UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return filteredVip.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let myVipFriend = filteredVip[indexPath.row]
    let cell = tableView.dequeueReusableCell(withIdentifier: "addMyVipsCell") as! AddMyVipsTableViewCell
    cell.peopleName.text = myVipFriend.fullName
    cell.selectionStyle = .none
    if let url = myVipFriend.profileImageUrl{
      cell.userImg.sd_setImage(with: URL(string: url.convertIntoViteURL()), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
    }
    if checkIfVIPListed(myVipFriend) {
      cell.addBtn.isEnabled = false
      cell.addBtn.setImage(UIImage(named:"InvitedToVIP"), for: UIControlState.disabled)
    } else {
      cell.addBtn.isEnabled = true
      cell.addBtn.tag = indexPath.row
      cell.addBtn.addTarget(self, action: #selector(self.addMyVipInGroup(_:)), for: .touchUpInside)
    }
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let storyboard = UIStoryboard(name: "Profile", bundle: nil)
    let controller = storyboard.instantiateViewController(withIdentifier: "MyProfileVc") as! MyProfileViewController
    let vipFriends = self.filteredVip[indexPath.row]
    if let facebookId = vipFriends.facebookIdInt {
        controller.fbId = String(describing: facebookId)
    }
    controller.isForViewingOthersProfile = true
    controller.isFromVip = true
    self.navigationController?.show(controller, sender: nil)
    
  }
  
}

