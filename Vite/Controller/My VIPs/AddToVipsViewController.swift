////
////  AddToVipsViewController.swift
////  Vite
////
////  Created by eeposit2 on 4/25/16.
////  Copyright © 2016 EeposIT_X. All rights reserved.
////
//
//
import UIKit
import ContactsUI
import AddressBook
import MessageUI

@available(iOS 9.0, *)
protocol  NonVIPDataManagerProtocol{
    func getUserNonVIPList(_ successBlock:@escaping (([MyVIPFriend], _ friendListInPrivateFriendModel:[PrivateFriend]) ->()) , failureBlock:@escaping ((_ message:String) -> ()))
}

struct PhoneContact {
    let name:String
    let number:String
    
    init(name:String, number:String) {
        self.name = name
        self.number = number
        
    }
}
class AddToVipsViewController: WrapperController, UITableViewDelegate, MFMessageComposeViewControllerDelegate, UISearchBarDelegate , UITableViewDataSource {
    lazy   var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
    @IBOutlet weak var tableView: UITableView!
    var timer: Timer? = nil
    var friendsArr:[MyVIPFriend]       = [MyVIPFriend]()
    var selectedFriendsArr:[MyVIPFriend] = [MyVIPFriend]()
    var selectedSearchedFriendsArr:[ViteUser] = [ViteUser]()
    var vipReqDataArr:[PrivateFriend] = [PrivateFriend]()
    
    let indicator                        = ActivityIndicator()
    var vipAdded                         = false
    let addedOnVip                       = 0
    let addToVipSection                  = 1
    let inviteFriendsSection             = 2
    let searchAnySection                 = 3
    var contactArray                     = [PhoneContact]()
    
    let VIPDataController             = VIPListService()
    let viteUserDataController = AllViteUsersService()
    fileprivate var _UserArr = [ViteUser]()
    
    // temporary container
    var originalContactArray             = [PhoneContact]()
    var originalFriendsArr:[MyVIPFriend] = [MyVIPFriend]()
    var originalVIPRequestData:[PrivateFriend] = [PrivateFriend]()
    
    var isSearching                      = false
    var searchShouldBegin                = false
    var page = Int()
    var globalSearchText = String()
    var noUser = false
    var refreshControl: UIRefreshControl!
    
    @IBOutlet weak var lblMessage: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getContactsList()
        lblMessage.text = "No Results Found"
        self.navigationItem.titleView = self.searchBar;
        searchBar.delegate = self
        self.getNonVipFriend()
        self.getVIPRequests()
        self.addRefreshControl()
    }
    func addRefreshControl(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(AddToVipsViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @objc func refresh(_ sender:AnyObject) {
        // Code to refresh table view
        getContactsList()
        self.getNonVipFriend()
        self.getVIPRequests()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.searchBar.resignFirstResponder()
     self.navigationController?.isNavigationBarHidden = true
    }
    
    func getVIPRequests(){
        WebService.request(method: .get, url: kAPIGetVIPRequests+(UserDefaultsManager().userFBID!), parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as! [String: AnyObject]? else {
                return
            }
            let param = response["params"] as? [String:AnyObject]
            
            
            if let responseArray =  param!["users"] as? [[String:AnyObject]] {
                self.vipReqDataArr = responseArray.flatMap(PrivateFriend.init)
                self.originalVIPRequestData = self.vipReqDataArr
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
            }
        }) { (message, apiError) in
            showAlert(controller: self, title: "", message: message)
            self.refreshControl.endRefreshing()
        }
    }
    
    
    
    @IBAction func loadMoreVip(_ sender: AnyObject) {
        page += 1
        self.viteUserDataController.getViteUsersForVip(page,searchString: globalSearchText,
                                                       successBlock:{ (viteusers) in
                                                        if viteusers.count < 10{
                                                            self.noUser = true
                                                        }
                                                        
                                                        self._UserArr.append(contentsOf: viteusers)
                                                        
                                                        self.tableView.reloadData()
        },
                                                       failureBlock:{ (message) in
                                                        
                                                        showAlert(controller: self, title: "", message: message)
        })
        
    }
    
    @IBAction func buttonTouched(_ sender: AnyObject) {
        
        print("Working")
    }
    
    func checkIfVIPListed(_ myfriend:MyVIPFriend) -> Bool {
        
        for friend in self.selectedFriendsArr {
            if myfriend.facebookIdInt == friend.facebookIdInt {
                return true
            }
        }
        return false
        
    }
    
    func checkIfVIPListedForAnyUser(_ myfriend:ViteUser) -> Bool {
        
        for friend in self.selectedSearchedFriendsArr {
            if myfriend.facebookId == friend.facebookId {
                return true
            }
        }
        return false
        
    }
    
    
    //MARK: PERFORM SEGUE
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if "showSearchAnyVC" == segue.identifier {
            // Nothing really to do here, since it won't be fired unless
            // shouldPerformSegueWithIdentifier() says it's ok. In a real app,
            // this is where you'd pass data to the success view controller.
            
            let destinationController = (segue.destination as! SearchAnyVC)
            destinationController.username = searchBar.text!
        }
    }
    
    //MARK: - API Events
    
    func getNonVipFriend(){
        self.indicator.start()
        self.VIPDataController.getUserNonVIPList({
            (friends , friendListInPrivateFriendModel) in
            self.friendsArr = friends
            self.originalFriendsArr =  self.friendsArr
            self.tableView.reloadData()
            self.indicator.stop()
            self.refreshControl.endRefreshing()
        })
        { (message) in
            self.indicator.stop()
            self.refreshControl.endRefreshing()
            showAlert(controller: self, title: "", message: message)
            
        }
        
    }
    
    //MARK: - Methods
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result.rawValue) {
        case MessageComposeResult.cancelled.rawValue:
            print("Message was cancelled")
            self.dismiss(animated: true, completion: nil)
        case MessageComposeResult.failed.rawValue:
            print("Message failed")
            self.dismiss(animated: true, completion: nil)
        case MessageComposeResult.sent.rawValue:
            print("Message was sent")
            self.dismiss(animated: true, completion: nil)
        default:
            break;
        }
    }
    
    
    
    @IBAction func goBack(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Actions
//    @IBAction func Back(_ sender: AnyObject) {
//        guard let nav = self.navigationController else {
//            return
//        }
//        nav.popViewController(animated: true)
//        self.dismiss(animated: true, completion: nil)
//    }
    
    @objc func acceptRequests(_ sender: UIButton){
        self.view.isUserInteractionEnabled = false
        var business = [String: AnyObject]()
        let friends =  self.vipReqDataArr[Int(sender.tag)]
        
        guard let friendFbId =  friends.facebookId else{
            return
        }
        guard let fbId = UserDefaultsManager().userFBID else {
            return
        }
        if let id = friends.businessAccountId{
            business =  [
                "entityId": id as AnyObject
            ]
        }
        
        let parameters:[String:AnyObject] = [
            "requestStatus":"ACCEPTED" as AnyObject,
            "business" : business as AnyObject
        ]
        WebService.request(method: .post, url: kAPIRespondVIPRequests + fbId + "/" + friendFbId, parameter: parameters, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as! [String: AnyObject]? else {
                return
            }
            guard let success = response["success"] as? Bool else{
                return
            }
            if(success){
                self.view.isUserInteractionEnabled = true
                self.getVIPRequests()
                
                let senderID = UserDefaultsManager().isBusinessAccountActivated ?UserDefaultsManager().userBusinessAccountID! : UserDefaultsManager().userFBID!
                let senderName = UserDefaultsManager().isBusinessAccountActivated ? UserDefaultsManager().userBusinessFullName! : UserDefaultsManager().userFBFullName!
                let senderImage = UserDefaultsManager().isBusinessAccountActivated ? UserDefaultsManager().userBusinessProfileImageUrl! : UserDefaultsManager().userFBProfileURL!
                let sender = NSender(uid: senderID, name: senderName, imageUrl: senderImage)
                let receiver = NReceiver(uid: friendFbId)
                NotificationService().sendVipNotification(sender, receiver: receiver, type: .VIPAccepted)
            }
            
        }) { (message, apiError) in
            self.view.isUserInteractionEnabled = true
            showAlert(controller: self, title: "", message: message)
        }
    }
    
    @objc func rejectRequests(_ sender: UIButton){
        self.view.isUserInteractionEnabled = false
        var business = [String: AnyObject]()
        let friends =  self.vipReqDataArr[Int(sender.tag)]
        guard let friendFbId =  friends.facebookId else{
            return
        }
        
        guard let fbId = UserDefaultsManager().userFBID else {
            return
        }
        if let id = friends.businessAccountId{
            business =  [
                "entityId": id as AnyObject
            ]
        }
        
        
        let parameters:[String:AnyObject] = [
            "requestStatus":"REJECTED" as AnyObject,
            "business" : business as AnyObject
        ]
        WebService.request(method: .post, url: kAPIRespondVIPRequests + fbId + "/" + friendFbId, parameter: parameters, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as! [String: AnyObject]? else {
                return
            }
            guard let success = response["success"] as? Bool else{
                return
            }
            if(success){
                self.getVIPRequests()
                self.vipReqDataArr.remove(at: sender.tag)
                self.tableView.reloadSections(NSIndexSet(index: self.addedOnVip) as IndexSet, with: .fade)
                
            }
            self.view.isUserInteractionEnabled = true
            
        }) { (message, apiError) in
            self.view.isUserInteractionEnabled = true
            showAlert(controller: self, title: "", message: message)
        }
    }
    
    
    
    @objc func addToVip(_ sender: UIButton){
        
        let addButton: UIButton = (sender )
        addButton.isEnabled = false
        
        let aFriend =  self.friendsArr[Int(sender.tag)]
        //add friends of user in server
        self.selectedFriendsArr.append(aFriend)
        guard let friendsFbId = aFriend.facebookIdInt else {return}
        let userFriends = [String(describing: friendsFbId)]
        let parameters:[String:AnyObject] = [
            "fbIdList" :userFriends as AnyObject
        ]
        //if businesss account is activated then create vip for business account
        let createVipUrl = UserDefaultsManager().isBusinessAccountActivated ?  kCreateBusinessVips + UserDefaultsManager().userBusinessAccountID! : kAPICreateVIPList!
        WebService.request(method: .post, url: createVipUrl, parameter: parameters, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as! [String: AnyObject]? else {
                return
            }
            guard let success = response["success"] as? Bool else{
                return
            }
            if success{
                self.vipAdded = true
            } else {
                self.vipAdded =  false
                self.view.makeToast(message: localizedString(forKey: "VIP_LIST_FULL_10"))
            }
            
            let indexPath = [IndexPath(row: sender.tag, section: self.addToVipSection)]
            self.tableView.reloadRows(at: indexPath, with: .automatic)
            
            addButton.isEnabled = true
            let senderID = UserDefaultsManager().isBusinessAccountActivated ? UserDefaultsManager().userBusinessAccountID! : UserDefaultsManager().userFBID!
            let senderName = UserDefaultsManager().isBusinessAccountActivated ? UserDefaultsManager().userBusinessFullName! : UserDefaultsManager().userFBFullName!
            let senderImage = UserDefaultsManager().isBusinessAccountActivated ? UserDefaultsManager().userBusinessProfileImageUrl! : UserDefaultsManager().userFBProfileURL!
            let sender = NSender(uid: senderID, name: senderName, imageUrl: senderImage)
            guard let facebookId = aFriend.facebookId else {return}
            let receiver = NReceiver(uid: facebookId)
            NotificationService().sendVipNotification(sender, receiver: receiver, type: .VIPRequested)
            
        }) { (message, apiError) in
            self.view.isUserInteractionEnabled = true
            showAlert(controller: self, title: "", message: message)
        }
    }
    
    
    @objc func addSearchUserToVip(_ sender: UIButton){
        sender.isEnabled = false
        let addButton: UIButton = (sender )
        addButton.isEnabled = false
        
        self.indicator.start()
        let friends =  self._UserArr[Int(sender.tag)]
        //add friends of user in server
        self.selectedSearchedFriendsArr.append(friends)
        guard let friendsFbId = friends.facebookId else {
            print("facebook Id not found")
            return
        }
        let userFriends = [String(describing: friendsFbId)]
        let parameters:[String:AnyObject] = [
            "fbIdList" :userFriends as AnyObject
        ]
        //if businesss account is activated then create vip for business account
        let createVipUrl = UserDefaultsManager().isBusinessAccountActivated ?  kCreateBusinessVips + UserDefaultsManager().userBusinessAccountID! : kAPICreateVIPList!
        WebService.request(method: .post, url: createVipUrl, parameter: parameters, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as! [String: AnyObject]? else {
                return
            }
            guard let success = response["success"] as? Bool else{
                return
            }
            if success{
                self.vipAdded = true
            } else {
                self.vipAdded =  false
                self.view.makeToast(message: localizedString(forKey: "VIP_LIST_FULL_20"))
            }
            self.indicator.stop()
            let indexPath = [IndexPath(row: sender.tag, section: 1)]
            self.tableView.reloadRows(at: indexPath, with: .automatic)
            addButton.isEnabled = true
            
        }) { (message, apiError) in
            showAlert(controller: self, title: "", message: message)
            addButton.isEnabled = true
            self.indicator.stop()
        }
    }
    
    
    @objc func inviteFriends(_ sender: UIButton)
    {
        //if user is in iphone
        if (UIDevice.current.userInterfaceIdiom == .phone )
        {
            let messageVC = MFMessageComposeViewController()
            messageVC.body = localizedString(forKey: "INVITE_TO_VIP_SMS")
            messageVC.recipients = [self.contactArray[sender.tag].number] // Optionally add some tel numbers
            messageVC.messageComposeDelegate = self
            // Open the SMS View controller
            if MFMessageComposeViewController.canSendText() {
                self.present(messageVC, animated: true, completion: nil)
            }
            
        }
        
        
    }
    
    
    //MARK: - Table View Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if friendsArr.count == 0 && self.contactArray.count == 0 && self._UserArr.count == 0 && self.vipReqDataArr.count == 0 {
            lblMessage.isHidden = false
            return 0
        }
        else {
            lblMessage.isHidden = true
            if section == addedOnVip{
                return self.vipReqDataArr.count
            }
            else if section == addToVipSection {
                if self.isSearching {
                    return self._UserArr.count + 1
                }
                return friendsArr.count
            }
            else {
                return self.contactArray.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case addToVipSection:
            return "Add To VIPs"
            
        case addedOnVip:
            return "VIP Requests"
            
        case searchAnySection:
            return "Load more"
            
        default:
            return "Invite To Vite"
        }
    }
    
    func  tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == addedOnVip{
            if self.vipReqDataArr.count == 0 || UserDefaultsManager().isBusinessAccountActivated{
                return  0
            }
            else if self.isSearching{
                return 0
            }
            else if self.searchShouldBegin{
                return 0
            }
            else{
                return 50
            }
        }
        else if section == addToVipSection{
            if self.isSearching {
                if self._UserArr.count == 0{
                    return 0
                }
                else{
                    return 50
                }
            }
            else{
                if self.friendsArr.count == 0{
                    return 0
                }
                else{
                    return 50
                }
            }
            
        }
        else{
            if self.contactArray.count == 0 {
                return 0
            }
            else{
                return 50
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: "Avenir-Heavy", size: 17)!
    }
    
    /*  func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
     
     var footerView : UIView?
     
     footerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 50))
     
     footerView?.backgroundColor = UIColor.clearColor()
     let btnSearch = UIButton(type: UIButtonType.System)
     
     btnSearch.backgroundColor = UIColor.clearColor()
     btnSearch.setTitle("Load More", forState: UIControlState.Normal)
     
     btnSearch.frame = CGRectMake(0, 0,tableView.frame.size.width, 50)
     btnSearch.titleLabel?.textAlignment = .Left
     btnSearch.addTarget(self, action: #selector(AddToVipsViewController.buttonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
     
     footerView?.addSubview(btnSearch)
     return footerView
     } */
    
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 && isSearching == true{
            return 0.0
        }
        return 0.0
        
    }
    
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.section == addToVipSection){
            if self.isSearching {
                if indexPath.row == _UserArr.count && noUser{
                    return 0
                }
                else{
                    return 60
                }
            }
            else{
                if indexPath.row == self.friendsArr.count{
                    return 0
                }
                else{
                    return 60
                }
            }
        } else if indexPath.section == addedOnVip{
            if self.isSearching {
                return 0
            }
            else if self.self.searchShouldBegin{
                return 0
            }
            return UserDefaultsManager().isBusinessAccountActivated ? 0 : 60
        }
        else{
            return 60
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
            
        case addedOnVip:
            let reqCell = tableView.dequeueReusableCell(withIdentifier: "AddedMeOnVIPCell") as! AddedMeOnVIPCell
            let userData = self.vipReqDataArr[indexPath.row]
            
            reqCell.lblTitle.text = userData.fullName
            if let imageUrl = userData.profileImage {
                reqCell.imgProfileImage.sd_setImage(with: URL(string:imageUrl.convertIntoViteURL()))
            }
            reqCell.btnAccept.tag = indexPath.row
            reqCell.btnReject.tag = indexPath.row
            reqCell.btnAccept.addTarget(self, action: #selector(self.acceptRequests), for: .touchUpInside)
            reqCell.btnReject.addTarget(self, action: #selector(self.rejectRequests), for: .touchUpInside)
            
            return reqCell
            
        case addToVipSection:
            //User can add their friends to VIP List
            if self.isSearching {
                if indexPath.row == _UserArr.count{
                    let loadCell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreVipCell") as! LoadMoreVipCell
                    loadCell.isHidden = noUser ? true : false
                    return loadCell
                }else{
                    let friends =  self._UserArr[indexPath.row]
                    let userCell = tableView.dequeueReusableCell(withIdentifier: "SearchVIPCell") as! SearchVIPCell
                    userCell.lblFullName.text = friends.fullName
                    userCell.imgProfile.sd_setImage(with: URL(string: (friends.profileImgUrl?.convertIntoViteURL())!))
                    if checkIfVIPListedForAnyUser(friends){
                        userCell.btnAdd.isEnabled = false
                        userCell.btnAdd.setImage(UIImage(named:"InvitedToVIP"), for: UIControlState.disabled)
                    }else{
                        userCell.btnAdd.isEnabled = true
                        userCell.btnAdd.setImage(UIImage(named:"AddToVIP"), for: UIControlState())
                        userCell.btnAdd.addTarget(self, action: #selector(self.addSearchUserToVip), for: .touchUpInside)
                    }
                    
                    userCell.btnAdd.tag = indexPath.row
                    return userCell
                }
            }
            else{
                let friends = self.friendsArr[indexPath.row]
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyVipCell") as! MyVipsTableViewCell
                cell.lblFullName.text = friends.fullName
                if let profileimg = friends.profileImageUrl {
                    cell.imgProfile.sd_setImage(with: URL(string: profileimg.convertIntoViteURL()))
                }
                if checkIfVIPListed(friends){
                    cell.btnAdd.isEnabled = false
                    cell.btnAdd.setImage(UIImage(named:"InvitedToVIP"), for: UIControlState.disabled)
                }else{
                    cell.btnAdd.isEnabled = true
                    cell.btnAdd.setImage(UIImage(named:"AddToVIP"), for: UIControlState())
                    cell.btnAdd.addTarget(self, action: #selector(self.addToVip), for: .touchUpInside)
                }
                cell.btnAdd.tag = indexPath.row
                return cell
            }
            
        default:
            //User can invite their friends to Vite
            let phoneContact      = self.contactArray[indexPath.row]
            let cell              = tableView.dequeueReusableCell(withIdentifier: "InviteCell") as! InviteTableViewCell
            cell.fullName.text    = phoneContact.name
            cell.lblPhoneNum.text = phoneContact.number
            cell.btnInvite.tag    = indexPath.row
            cell.btnInvite.addTarget(self, action: #selector(self.inviteFriends), for: .touchUpInside)
            return cell
            
        }
        
        
    }
    
    //MARK: Search VIPs
    func searchViteUser(_ searchText:String){
        page = 1
        self.noUser = true
        let escapedString = searchText.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        
        globalSearchText  = escapedString!
        self.indicator.start()
        
        self.viteUserDataController.getViteUsersForVip(page,searchString: escapedString!,
                                                       
                                                       successBlock:{ (viteusers) in
                                                        if viteusers.count < 10 {
                                                            self.noUser = true
                                                        }else{
                                                            self.noUser = false
                                                        }
                                                        self._UserArr = viteusers
                                                        self.tableView.reloadData()
                                                        self.indicator.stop()
        },
                                                       failureBlock:{ (message) in
                                                        self.indicator.stop()
                                                        showAlert(controller: self, title: "", message: message)
        })
    }
    
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(AddToVipsViewController.searchVip(_:)), userInfo: searchText, repeats: false)
    }
    
    //MARK:- Search Bar Delegates
    @objc func searchVip(_ timer: Timer){
        self.searchShouldBegin = false
        guard let searchText =  timer.userInfo as? String else{
            return
        }
        let predicate = NSPredicate(format: "self CONTAINS[cd] %@",searchText)
        
        let filteredContactsArray = originalContactArray.filter {
            predicate.evaluate(with: $0.name)
        };
        
        let filteredFriendsArray = originalFriendsArr.filter {
            predicate.evaluate(with: $0.fullName)
        };
        let filteredVIPRequest = originalVIPRequestData.filter {
            predicate.evaluate(with: $0.fullName)
        }
        self.isSearching = true
        if searchText.isEmpty {
            lblMessage.isHidden = true
            //when the request is empty set the original data to array
            self.contactArray = originalContactArray
            self.vipReqDataArr = originalVIPRequestData
            self.friendsArr = originalFriendsArr
            self.isSearching = false
            self._UserArr.removeAll()
            //            serviceCallGetNonVipFriends()
            
        } else {
            self.contactArray = filteredContactsArray
            self.friendsArr = filteredFriendsArray
            self.vipReqDataArr = filteredVIPRequest
            searchViteUser(searchText)
        }
        
        self.tableView.reloadData()
        
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool{
        self.searchShouldBegin = true
        self.tableView.reloadData()
        return true
    }
    
    //MARK:- CONTACT List
    @available(iOS 9.0, *)
    func getContactsList(){
        self.contactArray.removeAll()
        let contactStore = CNContactStore()
        do {
            try contactStore.enumerateContacts(with: CNContactFetchRequest(keysToFetch: [CNContactGivenNameKey as CNKeyDescriptor, CNContactFamilyNameKey as CNKeyDescriptor, CNContactEmailAddressesKey as CNKeyDescriptor, CNContactPhoneNumbersKey as CNKeyDescriptor])) {
                (contact, cursor) -> Void in
                
                if contact.isKeyAvailable(CNContactPhoneNumbersKey){
                    
                    for phoneNumber:CNLabeledValue in contact.phoneNumbers {
                        let num = phoneNumber.value
                        let fullName = contact.givenName + " " + contact.familyName
                        let number = num.stringValue
                        let contact = PhoneContact(name: fullName, number: number)
                        self.contactArray.append(contact)
                    }
                    self.originalContactArray = self.contactArray
                    
                } else {
                    print("No phone numbers are available")
                }
                
            }
            self.tableView.reloadData()
        }
        catch{
            print("Error getting contacts")
        }
        
    }
    
}

