//
//  AddGroupListTableViewCell.swift
//  Vite
//
//  Created by Eeposit 01 on 4/9/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class AddGroupListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnReject: UIButton!
    @IBOutlet weak var vipName: UILabel!
    @IBOutlet weak var vipImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.vipImg.layer.cornerRadius = vipImg.layer.frame.size.width/2
        self.vipImg.clipsToBounds = true
    }
    
}
