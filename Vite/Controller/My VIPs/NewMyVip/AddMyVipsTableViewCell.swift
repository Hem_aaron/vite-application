//
//  AddMyVipsTableViewCell.swift
//  Vite
//
//  Created by Eeposit 01 on 4/4/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class AddMyVipsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var peopleName: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var addBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userImg.layer.cornerRadius = userImg.layer.frame.size.width/2
        userImg.clipsToBounds = true
    }
}
