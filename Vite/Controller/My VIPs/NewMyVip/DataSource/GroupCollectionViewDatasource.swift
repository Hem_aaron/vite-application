////
////  GroupCollectionViewDatasource.swift
////  MyVipNewDesign
////
////  Created by Eeposit 01 on 4/2/17.
////  Copyright © 2017 Eeposit 01. All rights reserved.
////
//
import UIKit


class GroupCollectionViewDataSource : NSObject {
    
    fileprivate var collectionView : UICollectionView!
    var dataSource = [MyVIPFriend]()
    
    
    init(collectionView : UICollectionView , dataSource : [MyVIPFriend]) {
        
        self.collectionView = collectionView
        self.dataSource = dataSource
        super.init()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
}
extension GroupCollectionViewDataSource : UICollectionViewDataSource , UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = dataSource[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "groupCollectionCell", for: indexPath) as! GroupCollectionViewCell
        
        if let url = data.profileImageUrl {
        cell.userImg.sd_setImage(with: URL(string: url.convertIntoViteURL()), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
        }
        
        return cell
    }


}

