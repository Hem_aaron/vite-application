//
//  HeaderView.swift
//  MyVipNewDesign
//
//  Created by Eeposit 01 on 4/2/17.
//  Copyright © 2017 Eeposit 01. All rights reserved.
//

import UIKit

class HeaderView : UIView {

    @IBOutlet var mainView: UIView!

    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var titleBackView: UIView!
 
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)
        mainView.frame = self.bounds
        titleBackView.layer.cornerRadius = 15
        titleBackView.clipsToBounds = true
        self.addSubview(mainView)
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)
        mainView.frame = self.bounds
        titleBackView.layer.cornerRadius = 15
        titleBackView.clipsToBounds = true
        self.addSubview(mainView)
        
    }





}
