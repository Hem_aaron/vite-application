////
////  TableViewDataSource.swift
////
////  Created by Eeposit 01 on 4/2/17.
////  Copyright © 2017 Eeposit 01. All rights reserved.
////
//
import UIKit


protocol VIPListDataManagerProtocol {
    func getUserVIPList(_ successBlock:@escaping (([MyVIPFriend]) ->()) , failureBlock:@escaping ((_ message:String) -> ()))
    func deleteUserFromVIPList(_ parameters:[String:AnyObject], successBlock:@escaping (() ->()) , failureBlock:@escaping ((_ message:String) -> ()))
}

class TableViewDataSource : NSObject {
    
    var user = UserDefaultsManager()
    var sectionName  = ["REQUESTS" , "GROUP","PEOPLE"]
    var headerView : HeaderView!
    var headerViewFrame : CGRect!
    var buttonFrame : CGRect!
    var addGroupButton : UIButton!
    var vipReqDataArr:[PrivateFriend] = [PrivateFriend]()
    var listCategory : [VipGroupCategory] = [VipGroupCategory]()
    var isFromVipsViewController = false
    var lblNoVIP : UILabel!
    var view : UIView!
    var navigationController : UINavigationController!
    var apiCallCount = 0
    var viewController = WrapperController()
    let indicator = ActivityIndicator()
    let vipDataService:VIPListDataManagerProtocol = VIPListService()
    fileprivate var tableView : UITableView!
    fileprivate var friendsArr: [MyVIPFriend] = [MyVIPFriend]()
    
    enum tableSection : Int {
        case wantToBeMyVip  = 0
        case  group    = 1
        case  myVipList  = 2
    }
    
    
    init(tableView :UITableView, label :UILabel, navigationController :UINavigationController, view :UIView, viewController : WrapperController) {
        self.tableView = tableView
        self.lblNoVIP  = label
        self.navigationController = navigationController
        self.viewController = viewController
        self.view = view
        
        super.init()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.initiateRequests()
        self.lblNoVIP.isHidden = true
    }
    
    func initiateRequests() {
        self.view.isUserInteractionEnabled = false
        self.indicator.start()
        self.getUserVipLists()
        self.getListOfVipCategoryList()
        self.getVIPRequestsToBeMyVip()
    }
    
    // MARK: Private Function
    func addGroupButtonDesign () {
        let font = UIFont(name: "Avenir-Roman", size: 19.0)
        addGroupButton.titleLabel?.font =  font
        addGroupButton.setTitle("Add Group", for: UIControlState())
        addGroupButton.setTitleColor(UIColor(red: 6/255, green: 164/255, blue: 233/255, alpha: 1.0),for: UIControlState())
        
    }
    
    func changeMessage() {
        if user.isBusinessAccountActivated == true{
            let message = localizedString(forKey: "ADD_40_VIPS")
            self.lblNoVIP.text = message
            
        }else {
            let message = localizedString(forKey: "ADD_20_VIPS")
            self.lblNoVIP.text = message
        }
    }
    
    func getUserVipLists(){
        self.vipDataService.getUserVIPList({
            (friends) in
            self.friendsArr = friends
            self.updatedDataSource()
        })
        { (message) in
            self.indicator.stop()
            showAlert(controller: self, title: "", message: message)
        }
    }
    
    func deleteVipGroup(_ indexPath : IndexPath) {
        self.view.isUserInteractionEnabled = false
        let categoryList = listCategory[indexPath.row]
        guard let categoryId = categoryList.entityId else {return}
        self.indicator.start()
        VipGroupService.deleteVipCategory("\(categoryId)", successBlock: { (successMsg) in
            self.listCategory.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            self.tableView.reloadData()
            self.view.isUserInteractionEnabled = true
            self.indicator.stop()
        }, failureBlock: { (message) in
            showAlert(controller: self, title: "", message: message)
            self.view.isUserInteractionEnabled = true
            self.indicator.stop()
            
        })
    }
    
    func deleteMyVipList (_ indexPath :IndexPath) {
        
        let friend =  self.friendsArr[indexPath.row]
       
        guard let friendFbId = friend.facebookIdInt else {return}
        //delete VIP friend of user in server
        let userFriends = [friendFbId]
        let parameters:[String:AnyObject] = [
            "fbIdList" :userFriends as AnyObject
        ]
        
        self.indicator.start()
        AppDel.beginIgnoring()
        self.vipDataService.deleteUserFromVIPList(parameters, successBlock: {
            AppDel.endIgnoring()
            self.friendsArr.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            self.tableView.reloadData()
            self.indicator.stop()
        }, failureBlock: { (message) in
            AppDel.endIgnoring()
            self.view.makeToast(message: localizedString(forKey: "DELETE_EVENT_FAILED"))
            self.indicator.stop()
        })
    }
    
    func getVIPRequestsToBeMyVip(){
        WebService.request(method: .get, url: KAPIVipRequestForMe, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as! [String: AnyObject]? else {
                                return
                            }
                            let param = response["params"] as? [String:AnyObject]
                            if let responseArray =  param!["users"] as? [[String:AnyObject]] {
                                self.vipReqDataArr = responseArray.flatMap(PrivateFriend.init)
                                self.lblNoVIP.isHidden = self.vipReqDataArr.count == 0 && self.friendsArr.count == 0 ? false : true
                                self.updatedDataSource()
                            }
        }) { (message, apiError) in
            self.indicator.stop()
            showAlert(controller: self, title: "", message: message)
        }
    }
    
    func respondToVipRequest(_ senderTag : Int, requesterFbId: String) {
        self.view.isUserInteractionEnabled = false
        
        // sender tag determines if the user accepts or rejects the request. sender tag is set from story board and sent from closure of table view cell.
        let status = senderTag == 0 ? "REJECTED" : "ACCEPTED"
        let parameters:[String:AnyObject] = [
            "requestStatus": status as AnyObject
        ]
        WebService.request(method: .post, url: kAPIRespondToMyVipRequest + requesterFbId , parameter: parameters, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as! [String: AnyObject]? else {
                return
            }
            guard let success = response["success"] as? Bool else{
                                return
                            }
            if success{
                                self.initiateRequests()
                            }
                            self.view.isUserInteractionEnabled = true
        }) { (message, apiError) in
            self.indicator.stop()
                        self.view.isUserInteractionEnabled = true
            showAlert(controller: self, title: "", message: message)
        }
        
    }
    
    @objc func goAddGroupViewController () {
        self.view.isUserInteractionEnabled = false
        if user.isBusinessAccountActivated {
            if  listCategory.count >= 3{
                showAlert(controller: viewController, title: localizedString(forKey: "ALERT"), message: localizedString(forKey: "ADD_ONLY_3_GROUPS"))
            } else {
                goToAddGroupViewController()
            }
        } else {
            if  listCategory.count >= 2{
                showAlert(controller: viewController, title: localizedString(forKey: "ALERT"), message:localizedString(forKey: "ADD_ONLY_2_GROUPS"))
            } else {
                goToAddGroupViewController()
            }
        }
        self.view.isUserInteractionEnabled = true
    }
    
    func goToAddGroupViewController () {
        let storyboard = UIStoryboard(name: "MyVip", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "addGroup") as! AddGroupViewController
        controller.myVipList = friendsArr
        self.navigationController?.show(controller, sender: nil)
        
    }
    // get all list of category
    func getListOfVipCategoryList () {
        VipGroupService.listVipCategory({ (listVipCategory) in
            self.listCategory = listVipCategory
            self.updatedDataSource()
        }) { (message) in
            self.indicator.stop()
            showAlert(controller: self, title: "" , message: message)
        }
        
    }
    
    func updatedDataSource() {
        self.apiCallCount = self.apiCallCount + 1
        if self.apiCallCount == 3 {
            self.apiCallCount = 0
            self.indicator.stop()
            self.tableView.reloadData()
            self.view.isUserInteractionEnabled = true
            
        }
    }
    
}
extension TableViewDataSource : UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        headerViewFrame  = CGRect(x: 0, y: 0, width: tableView.frame.width , height: 20)
        headerView = HeaderView(frame: headerViewFrame)
        headerView.titleName.text = sectionName[section]
        if section == tableSection.group.rawValue {
            self.buttonFrame = CGRect(x: self.tableView.frame.size.width / 2  + 40, y: 8, width: 100, height: 34)
            self.addGroupButton     = UIButton(frame: buttonFrame)
            self.addGroupButtonDesign()
            addGroupButton.addTarget(self, action:#selector(goAddGroupViewController), for: .touchUpInside)
            self.headerView.addSubview(addGroupButton)
        }
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == tableSection.wantToBeMyVip.rawValue{
            return vipReqDataArr.count == 0 ? 0 : 50
        }else if section == tableSection.group.rawValue{
            return 50
        } else {
            return friendsArr.count == 0 ? 0 : 50
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case tableSection.wantToBeMyVip.rawValue:
            return vipReqDataArr.count
        case tableSection.group.rawValue:
            return listCategory.count
        case tableSection.myVipList.rawValue:
            return friendsArr.count
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if  indexPath.section == tableSection.wantToBeMyVip.rawValue {
            let requesterData           = self.vipReqDataArr[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "requestCell") as! RequestsTableViewCell
            cell.selectionStyle = .none
            cell.userName.text = requesterData.fullName
            let url = requesterData.profileImage!.convertIntoViteURL()
            cell.userImg.sd_setImage(with: URL(string: url), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
            cell.respondRequest = {(tag) in
                if let fbId = requesterData.facebookId{
                    self.respondToVipRequest(tag,requesterFbId: fbId)
                }
            }
            return cell
        }else if indexPath.section == tableSection.group.rawValue {
            let category = self.listCategory[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "groupCell") as! GroupTableViewCell
            cell.titleName.text = category.categoryName
            if let friendData  = category.friends {
                cell.groupCollectionDataSource = GroupCollectionViewDataSource(collectionView: cell.collectionView, dataSource: friendData)
            }
            return cell
        } else {
            
            let friends           = self.friendsArr[indexPath.row]
            let cell              = tableView.dequeueReusableCell(withIdentifier: "peopleCell") as! PeopleTableViewCell
            cell.peopleName.text  = friends.fullName
            if let url = friends.profileImageUrl {
                cell.userImg.sd_setImage(with: URL(string: url.convertIntoViteURL()), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == tableSection.myVipList.rawValue{
            let storyboard = UIStoryboard(name: "Profile", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MyProfileVc") as!MyProfileViewController
            let vipFriends = self.friendsArr[indexPath.row]
            controller.fbId = String(describing: vipFriends.facebookIdInt!)
            controller.isForViewingOthersProfile = true
            let nav = UINavigationController.init(rootViewController: controller)
            self.viewController.present(nav, animated: true, completion: nil)
        }
        if indexPath.section == tableSection.group.rawValue {
            self.isFromVipsViewController = true
            let category = listCategory[indexPath.row]
            let storyboard = UIStoryboard(name: "MyVip", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "addGroup") as! AddGroupViewController
            controller.myVipList = friendsArr
            controller.titleTextField.text = category.categoryName
            controller.category = category
            controller.isFromVipsViewController = self.isFromVipsViewController
            self.navigationController?.show(controller, sender: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            switch indexPath.section {
            case tableSection.myVipList.rawValue:
                self.deleteMyVipList(indexPath)
            case tableSection.group.rawValue:
                self.deleteVipGroup(indexPath)
            default:
                break
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}

