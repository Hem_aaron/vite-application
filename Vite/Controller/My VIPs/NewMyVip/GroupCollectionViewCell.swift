//
//  GroupCollectionViewCell.swift
//  MyVipNewDesign
//
//  Created by Eeposit 01 on 4/2/17.
//  Copyright © 2017 Eeposit 01. All rights reserved.
//

import UIKit

class GroupCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var userImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.userImg.layer.cornerRadius = self.userImg.layer.frame.size.height / 2
        self.userImg.clipsToBounds = true
    }
    
    
}
