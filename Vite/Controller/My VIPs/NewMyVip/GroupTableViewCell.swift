//
//  GroupTableViewCell.swift
//
//  Created by Eeposit 01 on 4/2/17.
//  Copyright © 2017 Eeposit 01. All rights reserved.
//

import UIKit

class GroupTableViewCell: UITableViewCell {

    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    var groupCollectionDataSource :GroupCollectionViewDataSource!
    
}
