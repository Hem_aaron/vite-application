//
//  PeopleTableViewCell.swift
//
//  Created by Eeposit 01 on 4/2/17.
//  Copyright © 2017 Eeposit 01. All rights reserved.
//

import UIKit

class PeopleTableViewCell: UITableViewCell {
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var peopleName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userImg.layer.cornerRadius = userImg.layer.frame.size.width/2
        userImg.clipsToBounds = true
        
    }
    
    
}
