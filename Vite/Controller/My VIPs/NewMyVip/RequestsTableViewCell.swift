//
//  RequestsTableViewCell.swift
//
//  Created by Eeposit 01 on 4/2/17.
//  Copyright © 2017 Eeposit 01. All rights reserved.
//

import UIKit

class RequestsTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userName: UILabel!
    var respondRequest:((_ tag: Int)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userImg.layer.cornerRadius = userImg.layer.frame.size.width/2
        userImg.clipsToBounds = true
        
    }
    
    @IBAction func requestRespond(_ sender: AnyObject) {
        if let respond = respondRequest{
            respond(sender.tag)
        }
        
    }
}
