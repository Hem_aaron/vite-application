//
//  SearchAnyVC.swift
//  Vite
//
//  Created by eepostIT5 on 5/22/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class SearchAnyVC: UIViewController {

    @IBOutlet weak var tblSearch: UITableView!
    var vipAdded                         = false
    let viteUserDataController = AllViteUsersService()
    fileprivate var _UserArr = [ViteUser]()
    var username = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchViteUser(username)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackAction(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    func searchViteUser(_ searchText:String){
        
        print("search"+searchText)
        self.viteUserDataController.getViteUsers(1,searchString: searchText,
                                                 successBlock:{ (viteusers , friendInPrivateFriendModel) in
                                                    self._UserArr = viteusers
                                                    print(self._UserArr)
                                                    self.tblSearch.reloadData()
            },
                                                 failureBlock:{ (message) in
                                                    
                                                    showAlert(controller: self, title: "", message: message)
        })
    }
    

    @objc func addToVip(_ sender: UIButton){
        
        let friends =  self._UserArr[Int(sender.tag)]
        //add friends of user in server
        
        let userFriends = [String(describing: friends.facebookId)]
        let parameters:[String:AnyObject] = [
            "fbIdList" :userFriends as AnyObject
        ]
        
//        WebService.postAt(kAPICreateVIPList!, parameters: parameters, successBlock:{
//            (response) -> Void in
//            guard let success = response!["success"] as? Bool else{
//                return
//            }
//            if success{
//                self.vipAdded = true
//            } else {
//                self.vipAdded =  false
//                self.view.makeToast("Your VIP list is full. You can add up to 10 people.")
//            }
//
//            let indexPath = [IndexPath(row: sender.tag, section: 0)]
//            self.tblSearch.reloadRows(at: indexPath, with: UITableViewRowAnimation.none)
//
//
//            }, failureBlock: {
//                (message) -> Void in
//                showAlert(self, title: "", message: message)
//        })
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - Table View Delegate Methods
    
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self._UserArr.count
    }
    
    func tableView(_ tableView: UITableView!, titleForHeaderInSection section: Int) -> String {
            return "ADD TO VIPs"
    }

    
    func tableView(_ tableView: UITableView,
                   heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
            //User can add their friends to VIP List
            let friends =  self._UserArr[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyVipCell") as! MyVipsTableViewCell
            cell.lblFullName.text = friends.fullName
        
        let url = friends.profileImgUrl?.convertIntoViteURL()
        cell.imgProfile.sd_setImage(with: URL(string: url!), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))

            cell.btnAdd.setImage(UIImage(named:(self.vipAdded ? "InvitedToVIP" : "AddToVIP")), for:UIControlState())
            cell.btnAdd.addTarget(self, action: #selector(self.addToVip), for: .touchUpInside)
            cell.btnAdd.tag = indexPath.row
            return cell
        
    }


}
