//
//  AddToBeMyVipTableViewCell.swift
//  Vite
//
//  Created by eeposit2 on 1/30/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class AddToBeMyVipTableViewCell: UITableViewCell {
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    var respondRequest:((_ tag: Int)->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        imgProfile.layer.cornerRadius = imgProfile.layer.frame.size.width/2
        imgProfile.clipsToBounds = true
    }
    
    @IBAction func respondToRequest(_ sender: AnyObject) {
        if let respond = respondRequest{
            respond(sender.tag)
        }
    }
    

}
