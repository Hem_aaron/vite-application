//
//  AddedMeOnVIPCell.swift
//  Vite
//
//  Created by EeposIT_X on 6/15/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class AddedMeOnVIPCell: UITableViewCell {
    @IBOutlet weak var imgProfileImage: UIImageView!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    @IBOutlet weak var lblTitle: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgProfileImage.layer.cornerRadius = 24
        imgProfileImage.clipsToBounds = true
    }

}
