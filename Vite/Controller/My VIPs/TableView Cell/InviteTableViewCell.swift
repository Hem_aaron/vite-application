//
//  InviteTableViewCell.swift
//  Vite
//
//  Created by eeposit2 on 4/25/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class InviteTableViewCell: UITableViewCell {

    
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var lblPhoneNum: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
