//
//  SearchVIPCell.swift
//  Vite
//
//  Created by Ujjwal Shrestha on 29/05/2016.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class SearchVIPCell: UITableViewCell {

    @IBOutlet var btnAdd: UIButton!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblFullName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgProfile.layer.cornerRadius = 24.0
        imgProfile.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
