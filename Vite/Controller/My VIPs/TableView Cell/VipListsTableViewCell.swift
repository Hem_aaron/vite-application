//
//  VipListsTableViewCell.swift
//  Vite
//
//  Created by eeposit2 on 4/26/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class VipListsTableViewCell: UITableViewCell {
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnAdd: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imgProfile.layer.cornerRadius = 24.0
        imgProfile.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
