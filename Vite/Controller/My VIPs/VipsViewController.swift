////
////  VipsViewController.swift
////  Vite
////
////  Created by Eeposit 01 on 4/3/17.
////  Copyright © 2017 EeposIT_X. All rights reserved.
////
//
import UIKit

class VipsViewController: WrapperController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblNoVip: UILabel!
    var tableViewDatasource:TableViewDataSource!
    var fromNotification = false

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        self.tableViewDatasource = TableViewDataSource(tableView: self.tableView, label: lblNoVip, navigationController: self.navigationController!, view: self.view,viewController: self)
    }
    @IBAction func goBack(_ sender: AnyObject) {
        if !fromNotification {
        let controller = Route.masterViewController
        self.slideMenuController()?.changeMainViewController(controller, close: true)
        } else {
            guard let nav = self.navigationController else {
                return
            }
            nav.popViewController(animated: true)
        }
    }


}

