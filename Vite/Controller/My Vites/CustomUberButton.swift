//
//  CustomUberButton.swift
//  Vite
//
//  Created by Hem Poudyal on 4/28/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
////
//import UberRides
//import Foundation
//let sourceString = "button"
//class CustomUberButton: UIButton {
//    var rideParameters: RideParameters!
//    var requestBehavior: RideRequesting!
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        self.addTarget(self, action: #selector(CustomUberButton.tappedUber), for: .touchUpInside)
//    }
//
//    @objc func tappedUber() {
//        self.rideParameters.source = sourceString
//
//        self.requestBehavior.requestRide(parameters: self.rideParameters)
//    }
//}

