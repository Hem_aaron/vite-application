//
//  GetDirectionViewController.swift
//  Vite
//
//  Created by eeposit2 on 2/23/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import MapKit

class GetDirectionViewController: UIViewController,CLLocationManagerDelegate,MKMapViewDelegate {
    var vites:MyVites?
    
    @IBOutlet weak var mapVIew: MKMapView!
    @IBOutlet weak var btnShowInstruction: UIButton!
    
    var stepArr = [MKRouteStep]()
    var longitude:String! = "-0.127758"
    var latitude:String! = "51.507351"
    var address:String!
    var annotationTitle:String!
    var locationManager:CLLocationManager!
    var distance:Double = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        btnShowInstruction.isEnabled = false
        longitude = vites?.longitude
        latitude = vites?.latitude
        
        
        self.navigationController!.setNavigationBarHidden(false, animated:true)
        let myBackButton:UIButton = UIButton(type:UIButtonType.custom) as UIButton
        myBackButton.addTarget(self, action: #selector(self.popToRoot(_:)), for: UIControlEvents.touchUpInside)
        myBackButton.setImage(UIImage(named: "Back"), for: UIControlState())
        myBackButton.sizeToFit()
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBackButton)
        self.navigationItem.leftBarButtonItem  = myCustomBackButtonItem
        
        
        self.initiateLocationManager()
        showAnnotation()
        let swipUp = UISwipeGestureRecognizer(target: self, action: #selector(GetDirectionViewController.swipeUpAction(_:)))
        swipUp.direction = UISwipeGestureRecognizerDirection.up
        self.view.addGestureRecognizer(swipUp)
    }
    
    @objc func swipeUpAction(_ gesture: UIGestureRecognizer) {
        showInstructionPage()
    }
    
    func showInstructionPage(){
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "showInstructionVc") as! StepWiseDirectionViewController
        controller.instructionArr = stepArr
        self.present(controller, animated: true, completion: nil)
        
    }
    
    
    func initiateLocationManager() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        self.mapVIew.delegate = self
    }
    
    @objc func popToRoot(_ sender:UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    
    func createMapItem(_ longitude:Double, latitude:Double, placemarkName:String) -> MKMapItem {
        let placemark = MKPlacemark(coordinate: CLLocationCoordinate2DMake(latitude, longitude), addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = placemarkName
        return mapItem
    }
    
    func plotCoordinate() {
        let annotation = MKPointAnnotation()
        var coordinate = CLLocationCoordinate2D()
        //        var latitude = eventList.latitude
        //        var longitude = eventList.longitude
        
        coordinate.latitude  = Double(latitude!)!
        coordinate.longitude = Double(longitude!)!
        
        annotation.coordinate = coordinate
        self.mapVIew.addAnnotation(annotation)
        
        annotation.title =  annotationTitle
        self.mapVIew.centerCoordinate = annotation.coordinate;
        let viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 1500, 1500)
        self.mapVIew.setRegion(viewRegion, animated: false)
        let destination = createMapItem(Double(longitude!)!, latitude: Double(latitude!)!, placemarkName: "Event")
        plotRoute(destination)
    }
    
    
    func plotRoute(_ destination:MKMapItem ) {
        let req = MKDirectionsRequest()
        req.source = MKMapItem.forCurrentLocation()
        req.destination = destination
        let dir = MKDirections(request:req)
        
        dir.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else { return }
             self.btnShowInstruction.isEnabled = true
            
            for route in unwrappedResponse.routes {
                self.mapVIew.add(route.polyline)
                self.mapVIew.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
                for step in route.steps {
                    self.distance += step.distance
                }
            }
        }
    }
    
    // To Show Annotation
    
    func showAnnotation()
    {
        //        let destinationLocation = CLLocationCoordinate2DMake(Double(eventList.latitude!)!, Double(eventList.longitude!)!)
        //
        let destinationLocation = CLLocationCoordinate2DMake(Double(latitude)!, Double(longitude)!)
        // Drop a pin
        let dropPin = MKPointAnnotation()
        dropPin.coordinate = destinationLocation
        //        dropPin.title = eventList.eventTitle
        //        dropPin.subtitle = eventList.eventDetail
        dropPin.title = vites?.eventTitle
        dropPin.subtitle = ""
        mapVIew.addAnnotation(dropPin)
    }
    
    
    //MARK: MapView Delegate
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        polylineRenderer.strokeColor = UIColor.blue
        polylineRenderer.fillColor = UIColor.red
        polylineRenderer.lineWidth = 2
        return polylineRenderer
        
        
    }
    
    
    func mapView(_ mapView: MKMapView,
                 viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation {
            //return nil so map view draws "blue dot" for standard user location
            return nil
        }
        
        let reuseId = "pin"
        
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView!.canShowCallout = true
            pinView!.animatesDrop = true
            pinView!.pinTintColor = UIColor.purple
        }
        else {
            pinView!.annotation = annotation
        }
        
        return pinView
    }
    
    
    //MARK: Location delegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        LocationManager.plotPinAt(location: locValue, mapView: self.mapVIew, title: "Current Location", subTitle: "")
        plotCoordinate()
        locationManager.delegate = nil
        locationManager.stopUpdatingLocation()
    }
    
    @IBAction func showInstruction(_ sender: AnyObject) {
        showInstructionPage()
    }
    
}
