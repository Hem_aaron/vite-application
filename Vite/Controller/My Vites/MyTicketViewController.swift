//
//  MYTicketViewController.swift
//  Vite
//
//  Created by sajjan giri on 10/20/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class MyTicketViewController: UIViewController {
    
    var vite : MyVites?
    
    
    @IBOutlet weak var userImage: ImageViewWithRoundedBorder!
    @IBOutlet weak var qrImage: UIImageView!
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var eventLocation: UILabel!
    @IBOutlet weak var personName: UILabel!
    @IBOutlet weak var genderType: UILabel!
    @IBOutlet weak var leftRoundView: UIView!
    @IBOutlet weak var rightRoundView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadTicketInfo()
        desingRoundView()
        //set clear Nav color to false
        setupNav(false)
        NotificationCenter.default.addObserver(self, selector: #selector(screenShotTaken), name: NSNotification.Name.UIApplicationUserDidTakeScreenshot, object: nil)
        //screenShotTaken()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //set clear Nav color to true
        setupNav(true)
    }
    
    //MARK: Private Methods
    func loadTicketInfo() {
        if let imagePath = vite?.qrImagePath{
            self.qrImage.sd_setImage(with: URL(string: imagePath.convertIntoViteURL()))
        }
        self.eventName.text = vite?.eventTitle
        self.eventLocation.text = vite?.eventGivenLocation
        self.personName.text = UserDefaultsManager().userFBFullName
        if let gender = UserDefaultsManager().userFBGender{
            self.genderType.text = gender
        }
        if let imgUrl = UserDefaultsManager().userFBProfileURL{
            self.userImage.sd_setImage(with: URL(string: imgUrl.convertIntoViteURL()))
        }
    }
    
    func desingRoundView() {
        self.leftRoundView.layer.cornerRadius = 15
        self.leftRoundView.clipsToBounds = true
        self.rightRoundView.layer.cornerRadius = 15
        self.rightRoundView.clipsToBounds = true
    }
    
    // Navigation setup
    func setupNav(_ clearNavColor: Bool) {
        if let nav = self.navigationController {
            nav.navigationBar.setBackgroundImage(clearNavColor ? UIImage() : UIImage(named:"purpleNav"), for: UIBarMetrics.default)
            nav.navigationBar.shadowImage                  = UIImage()
            if clearNavColor{
                nav.view.backgroundColor                       = UIColor.clear
            }
            nav.isNavigationBarHidden                        = false
            let navBar                                     = self.navigationController!.navigationBar
            navBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: clearNavColor ? UIColor.purple : UIColor.white , NSAttributedStringKey.font: UIFont(name: kFontName, size: kNavigationTitleFontSize)!]
            self.automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    //MARK: ScreenShot Actions and API's
    @objc func screenShotTaken(){
        guard let eventId = self.vite?.eventID else {
            return
        }
        VitesWebservice.screenShotDetail(eventId, successBlock: { (message) in
            print(message)
        }) { (message) in
            showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: message)
        }

    }
    
}

extension MyTicketViewController{
    
    @IBAction func backButton(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
