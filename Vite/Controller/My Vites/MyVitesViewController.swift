//
//  MyVitesViewController.swift
//  Vite
//
//  Created by Eeposit1 on 2/16/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import EventKit
import ObjectMapper


class MyVitesViewController: WrapperController,UITableViewDataSource,VitesTableViewCellDelegate, UITableViewDelegate {
    let message = localizedString(forKey: "NO_VITES")
    let lblMessage = UILabel()
    var fbId: String!
    //var isFromVip : Bool = false
    var vipName = String()
    var vipViteCount = Int()
    var isForViewingOthersProfile: Bool =  false
    var vitesArr = [MyVites]()
    let indicator = ActivityIndicator()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingView: UIView!
     let cursor = VCursor()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.isUserInteractionEnabled = false
        UserDefaults.standard.set(false, forKey: "myViteNotification")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.ServiceCallMyVites(shouldReset: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        indicator.stop()
    }
    
    func ServiceCallMyVites(shouldReset reset:Bool){
        if reset {
            self.cursor.reset()
            self.initiateLoading(true)
        } else {
            indicator.start()
        }
        WebService.request(method: .get, url: kMyVitesWithPagination + "\(cursor.nextPage())/\(cursor.pageSize)", success: { (response) in
            if reset {
                self.vitesArr.removeAll()
            }
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                self.lblMessage.isHidden = false
                setUpLblMsgs(self.message, view: self.view, lblMessage: self.lblMessage)
                self.indicator.stop()
                self.initiateLoading(false)
                self.view.isUserInteractionEnabled = true
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let count = param["count"] as? Int {
                    self.cursor.totalCount = count
                }
                if let viteArray = param["vites"] as? NSArray {
                    for vite in viteArray {
                        let vites = MyVites()
                        let vite = vite as! [String:AnyObject]
                        vites.acceptedDate    = vite["acceptedDate"] as? String
                        vites.qrImagePath    = vite["qrImagePath"] as? String
                        vites.rated           =  vite["rated"] as? Bool
                        if let event = vite["event"] as? [String:AnyObject] {
                            vites.eventID         = event["entityId"] as? String
                            vites.eventTitle      = event["eventTitle"] as? String
                            vites.imageUrl        = event["imageUrl"] as? String
                            vites.eventDate       = event["eventDate"] as? String
                            vites.eventStartTime  = event["eventStartTime"] as? String
                            vites.privateEvent    = event["privateEvent"] as? Bool
                            if let videoUrl = event["profileVideoUrl"] as? String{
                                vites.eventVideoUrl = videoUrl
                            }
                            if let type = event["fileType"] as? String{
                                vites.fileType = type
                            }
                            if let thumbnail = event["videoProfileThumbnail"] as? String{
                                vites.videoProfileThumbNail = thumbnail
                            }
                            if let location = event["location"] as? [String:AnyObject] {
                                vites.latitude        = location["latitude"] as? String
                                vites.longitude       = location["longitude"] as? String
                                vites.eventGivenLocation   = location["givenLocation"] as? String
                            }
                            
                            if let organizer = event["organizer"] as? [String:AnyObject] {
                                vites.fullName        = organizer["fullName"] as? String
                                vites.profileImageUrl = organizer["profileImageUrl"] as? String
                                vites.isCelebrity     = organizer["celebrity"] as? Bool
                                if let fbID = organizer["facebookId"] {
                                    vites.organizerFBID   = String(describing: fbID)
                                }
                            }
                            vites.eventDetail     = event["eventDetail"] as? String
                            if let businessDetail = event["business"] as? [String:AnyObject] {
                                vites.businessName = businessDetail["businessName"] as? String
                                vites.businessImageUrl =  businessDetail["profileImage"] as? String
                                vites.businessID = businessDetail["entityId"] as? String
                            }
                            if let mediaData = event["media"] as? [[String: AnyObject]]{
                                var mediaDataArr = [VMedia]()
                                for obj in mediaData {
                                    let media = Mapper<VMedia>().map(JSON: obj)
                                    if let m = media {
                                        mediaDataArr.append(m)
                                    }
                                }
                                vites.media = mediaDataArr
                            }
                        }
                        vites.viteStatus      = vite["viteStatus"] as? String
                        
                        self.vitesArr.append(vites)
                    }
                }
                self.cursor.totalLoadedDataCount = self.vitesArr.count
                if self.vitesArr.count == 0 {
                    self.lblMessage.isHidden = false
                    setUpLblMsgs(self.message, view: self.view, lblMessage: self.lblMessage)
                } else {
                    self.lblMessage.isHidden = true
                }
              
                self.addVitesToCalendar()
                self.tableView.reloadData()
            }
            self.indicator.stop()
            self.initiateLoading(false)
             self.view.isUserInteractionEnabled = true
            
        }) { (msg, err) in
            showAlert(controller: self, title: "", message: msg)
            self.initiateLoading(false)
             self.view.isUserInteractionEnabled = true
            self.indicator.stop()
        }
    }
    
    @IBAction func backToHome(_ sender: AnyObject) {
            Route.goToHomeController()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Mark: TableView Delegates and DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vitesArr.count
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.cursor.hasNextPage() && indexPath.row == self.vitesArr.count - 1 {
            self.ServiceCallMyVites(shouldReset: false)
        }
    }

    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ViteCell") as! VitesTableViewCell
//        if isFromVip{
//            cell.btnDeleteVite.isHidden = true
//        }
        let vites = self.vitesArr[indexPath.row]
        var url = String()
        if vites.fileType == "IMAGE"{
            if let url = vites.imageUrl{
                cell.backImage.sd_setImage(with: URL(string: url.convertIntoViteURL()))
            }
        } else {
            if let thumbNailUrl = vites.videoProfileThumbNail{
                cell.backImage.sd_setImage(with: URL(string: thumbNailUrl.convertIntoViteURL()))
            }
        }
        cell.lblTitle.text = vites.eventTitle
        if let name  =  vites.businessName, let imgUrl = vites.businessImageUrl{
            cell.lblInvitedBy.text = "Invited by \(name)"
            url = imgUrl.convertIntoViteURL()
        }else{
            cell.lblInvitedBy.text = "Invited by \(vites.fullName!)"
            url = vites.profileImageUrl!.convertIntoViteURL()
        }
        cell.lblDescription.text = vites.eventDetail
        cell.profileImageView.sd_setImage(with: URL(string: url), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
        cell.delegate = self
        cell.row = indexPath.row
        if (vites.qrImagePath) != nil{
            cell.btnShowTicket.isHidden = false
        }else{
            cell.btnShowTicket.isHidden = true
        }
        cell.showTicket = {
            [weak self] in
            self?.showTicketController((self?.vitesArr[indexPath.row])!)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller   = Route.viteDetail
        self.navigationController?.show(controller, sender: nil)
        controller.vites =  self.vitesArr[indexPath.row]
    }
    

    
    
    
    func showTicketController(_ vite: MyVites){
        let controller = Route.myTicket
        controller.vite = vite
        self.navigationController?.show(controller, sender: nil)
    }
    
    //Cell Delegate
    func deleteTableAtRow(_ index: Int) {
        self.promptConfirmationForDelete(index)
    }
    
    func promptConfirmationForDelete(_ index:Int) {
        
        let alert = UIAlertController(title: "",
                                      message: localizedString(forKey: "DELETE_THIS_VITE"),
                                      preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok",
                                     style: .default,
                                     handler: {
                                        (action:UIAlertAction) -> Void in
                                        self.deleteAction(index)
        })
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .default) { (action: UIAlertAction) -> Void in
                                            
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        present(alert,
                animated: false,
                completion: nil)
    }
    
    func deleteAction(_ index:Int) {
        let vite = self.vitesArr[index]
        let deleteURL = kAPIDeleteMyVite + "/" + vite.eventID! + "/" + UserDefaultsManager().userFBID!
        self.indicator.start()
        WebService.request(method: .post, url: deleteURL, parameter: nil, header: nil, isAccessTokenRequired: false, success: {
            (response) -> Void in
            print(response)
            guard let response = response.result.value as? [String:AnyObject] else {
                return
            }
            if (response["success"] as? Bool)! {
                self.vitesArr.remove(at: index)
                let indexPath = IndexPath(row: index, section: 0)
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
                self.indicator.stop()
                showAlert(controller: self, title: localizedString(forKey: "SUCCESS"), message: localizedString(forKey: "DELETE_SUCCESSFULLY"))
            }
        }, failure:{ (message, APIError) -> Void in
            self.view.makeToast(message: localizedString(forKey: "DELETE_EVENT_FAILED"))
            self.indicator.stop()
        })
    }
    
    
    @IBAction func showTicket(_ sender: AnyObject) {
        
    }
    
   
    
    func initiateLoading(_ state: Bool){
        if state{
            self.view.isUserInteractionEnabled = false
            loadViteLoadingGif(loadingView)
        }else{
              self.view.isUserInteractionEnabled = true
            loadingView.isHidden = true
        }
        
        self.tableView.isHidden = state
    }
    
    // Creates an event in the EKEventStore. The method assumes the eventStore is created and
    // accessible
    func createEvent(_ eventStore: EKEventStore, title: String, startDate: Date, endDate: Date) {
        let event = EKEvent(eventStore: eventStore)
        event.title = title
        event.startDate = startDate
        event.endDate = endDate
        event.calendar = eventStore.defaultCalendarForNewEvents
        do {
            try eventStore.save(event, span: .thisEvent)
        } catch {
            print("Error occured while adding event to calendar.")
        }
    }
    
    // asks for user permission for calendar
    func addEvent(_ startDate: Date, endDate: Date, eventTitle: String){
        let eventStore = EKEventStore()
        if (EKEventStore.authorizationStatus(for: .event) != EKAuthorizationStatus.authorized) {
            eventStore.requestAccess(to: .event, completion: {
                granted, error in
                self.createEvent(eventStore, title: eventTitle, startDate: startDate, endDate: endDate)
            })
        } else {
            createEvent(eventStore, title: eventTitle, startDate: startDate, endDate: endDate)
        }
    }
    
    func addVitesToCalendar(){
        let eventStore = EKEventStore()
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "YYYY-MM-dd HH:mm:ss"
            for vites in vitesArr{
                if let eventDate = vites.eventDate, let eventStartTime = vites.eventStartTime, let eventTitle = vites.eventTitle{
                    if let eventStartDate = dateFormatter.date(from: eventDate + " " + eventStartTime){
                        let eventEndDate = eventStartDate.addingTimeInterval(300 * 60) // 5 hr after event start date i.e 5 hr  = 300 min
                        if !checkIfEventExistsInCalendar(eventStartDate, endDate: eventEndDate, eventStore: eventStore, eventTitle: eventTitle){
                            addEvent(eventStartDate, endDate: eventEndDate, eventTitle: eventTitle)
                        }
                    }
                }
            }
    }
    
    func checkIfEventExistsInCalendar(_ startDate: Date, endDate: Date, eventStore: EKEventStore, eventTitle: String) -> Bool{
        var count = 0
        let predicate = eventStore.predicateForEvents(withStart: startDate, end: endDate, calendars: nil)
        let existingEvents = eventStore.events(matching: predicate)
        for singleEvent in existingEvents {
            if singleEvent.title == eventTitle{
                // count increased if the event title  matches with calendar title
                count += 1
            }
        }
        // return false if the event is not in calendar
        return count == 0 ? false: true
    }
    
    
}
