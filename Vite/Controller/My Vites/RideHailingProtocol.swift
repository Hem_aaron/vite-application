//
//  RideHailingProtocol.swift
//  Vite
//
//  Created by Hem Poudyal on 4/28/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation
import CoreLocation

protocol RideHailingProtocol {
    func callRideFor(_ dropOffLocation:CLLocation,pickupLocation:CLLocation,
                     completionBlock:((_ status:Bool, _ response:AnyObject?) -> ()))
    
    
    func callRideForCurrentLocationTo(_ dropOffLocation:CLLocation,
                                      presentingViewController:UIViewController,
                                      completionBlock:(() -> ())?,
                                      failureBlock:((_ message:String) -> ())?)
}
