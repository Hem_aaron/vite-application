//
//  StepWiseDirectionCell.swift
//  Vite
//
//  Created by eeposit2 on 8/31/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class StepWiseDirectionCell: UITableViewCell {
    @IBOutlet weak var lblNo: UILabel!
    @IBOutlet weak var lblDirection: UILabel!
}
