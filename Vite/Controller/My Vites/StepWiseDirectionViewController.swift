//
//  StepWiseDirectionViewController.swift
//  Vite
//
//  Created by eeposit2 on 8/31/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import MapKit

class StepWiseDirectionViewController: WrapperController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    var instructionArr =  [MKRouteStep]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 60.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    
    
    //Mark: TableView Delegates and DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return instructionArr.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StepCell") as! StepWiseDirectionCell
        cell.lblNo.text = "\(indexPath.row+1)."
        let step =  instructionArr[indexPath.row]
        var text = String()
        text =  step.distance == 0.0 ? "\(step.instructions)" : "After \(Int(step.distance)) metres: \(step.instructions)"
        cell.lblDirection.text = text
        return cell
    }
    
    
    @IBAction func back(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    
    
}
