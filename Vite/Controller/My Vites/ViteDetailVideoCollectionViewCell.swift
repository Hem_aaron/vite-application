//
//  ViteDetailVideoCollectionViewCell.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 5/2/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class ViteDetailVideoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var thumbNailImgView: UIImageView!
    
    var playVideo: (() -> ())?
    
    @IBOutlet weak var btnPlay: UIButton!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.thumbNailImgView.layer.cornerRadius = 10
        self.thumbNailImgView.clipsToBounds = true
    }
    
    @IBAction func playVid(_ sender: AnyObject) {
        if let p = playVideo{
            p()
        }
    }
}
