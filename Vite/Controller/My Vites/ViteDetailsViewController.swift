      //
      //  ViteDetailsViewController.swift
      //  Vite
      //
      //  Created by Eeposit1 on 2/16/16.
      //  Copyright © 2016 EeposIT_X. All rights reserved.
      //
      
      import UIKit
      import MapKit
      import Social
      import MessageUI
      import UberRides
      import FBSDKCoreKit
      import FBSDKShareKit
      import FBSDKLoginKit
      import GoogleMapsKit
      import AVFoundation
      
      class ViteDetailsViewController: UIViewController, CLLocationManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
        var vites:MyVites?
        var viteIdFromNotification = String()
        
     
        //MARK: Outlets
    
        @IBOutlet weak var lblOrganizer: GrayLabel!
        @IBOutlet weak var btnUber: UIButton!
        @IBOutlet weak var containerView: UIView!
        @IBOutlet weak var btnShare: UIButton!
        @IBOutlet weak var imgEvent: UIImageView!
        @IBOutlet weak var lblEventTitle: UILabel!
        @IBOutlet weak var lblEventDescription: UILabel!
        @IBOutlet weak var lblAddress: UILabel!
        @IBOutlet weak var imgProfileImage: ImageViewWithWhiteRoundedBorder!
        @IBOutlet weak var lblLocation: UILabel!
        @IBOutlet weak var organizerName: UILabel!
        @IBOutlet weak var eventDateAndTime: UILabel!
        @IBOutlet weak var btnGetDirectionHeightConstraint: NSLayoutConstraint!
        @IBOutlet weak var btnGetDirectionTopSpaceConstraint: NSLayoutConstraint!
        @IBOutlet weak var mediaCollectionViewHeightConstraint: NSLayoutConstraint!
        @IBOutlet weak var mediaView: UIView!
        @IBOutlet weak var btnPlay: UIButton!
        @IBOutlet weak var btnMsg: UIButton!
        @IBOutlet weak var mediaCollectionView: UICollectionView!
        @IBOutlet weak var btnRateOrganizer: UIButton!
        
        //MARK: Private Variables
        var rideHailingService:RideHailingProtocol!
        let indicator = ActivityIndicator()
        var locationManager:CLLocationManager!
        var userLocation:CLLocation!
        var destinationLocation:CLLocation!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            if let myVite = self.vites {
                getViteDetailFromServer(viteId: myVite.eventID!)
            } else {
                getViteDetailFromServer(viteId: viteIdFromNotification)
            }
            self.initiateLocationManager()
            //LYFT CONFIGURATION
            getLyftDetailForUser()
             NotificationCenter.default.addObserver(self, selector: #selector(screenShotTaken), name: NSNotification.Name.UIApplicationUserDidTakeScreenshot, object: nil)
               NotificationCenter.default.addObserver(self, selector: #selector(getViteDetailAfterRating), name: NSNotification.Name(rawValue: "getViteDetailFromServer"), object: nil)
        }
        
        override func viewWillAppear(_ animated: Bool) {
            self.btnMsg.isEnabled = true
        }
        
        //MARK: Private Methods
        
        func getLyftDetailForUser() {
            guard let userCurrentLat = UserDefaultsManager().userCurrentLatitude , let userCurrentLong = UserDefaultsManager().userCurrentLongitude, let eventLat = vites?.latitude, let eventLongitude = vites?.longitude else { return }
            let pickup = CLLocationCoordinate2D(latitude: Double(userCurrentLat)!, longitude: Double(userCurrentLong)!)
            let destination = CLLocationCoordinate2D(latitude: Double(eventLat)!, longitude: Double(eventLongitude)!)
           // btnLyft.configure(rideKind: LyftSDK.RideKind.Standard, pickup: pickup, destination: destination)
        }
        
        @objc func getViteDetailAfterRating() {
            if let myVite = self.vites {
                getViteDetailFromServer(viteId: myVite.eventID!)
            } else {
                getViteDetailFromServer(viteId: viteIdFromNotification)
            }
        }
   
        
         func getViteDetailFromServer(viteId: String) {
            self.indicator.start()
            self.lblEventTitle.text = ""
            lblEventDescription.text = ""
            imgProfileImage.image = nil
            lblAddress.text = ""
            VitesWebservice().getViteDetail(viteId, successBlock: { (vite) -> () in
                self.vites = vite
                self.populateUI()
                self.indicator.stop()
            }, failure: { (message) -> () in
                self.indicator.stop()
            })
        }
        
        func initiateLocationManager() {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        
        override func viewDidLayoutSubviews() {
            super.viewDidLayoutSubviews()
            self.imgEvent.roundCorners([.topRight , .topLeft], radius: 10)
        }
        
        
        func showMediaFileCollectionView(){
            if let media =  vites?.media{
                mediaCollectionViewHeightConstraint.constant = media.count == 0 ? 0 : 250
                mediaView.isHidden = media.count == 0 ? true : false
                mediaCollectionView.reloadData()
            }else{
                mediaCollectionViewHeightConstraint.constant = 0
                mediaView.isHidden = true
            }
            
        }
        
        func populateUI() {
            var url = String()
            if let name = vites?.businessName, let imgUrl = vites?.businessImageUrl{
                lblOrganizer.text = "ORGANIZATION"
                lblOrganizer.kern(kerningValue: 2)
                organizerName.text = name
                url = imgUrl.convertIntoViteURL()
            }else{
                lblOrganizer.text = "ORGANIZER"
                lblOrganizer.kern(kerningValue: 2)
                url = vites!.profileImageUrl!.convertIntoViteURL()
                organizerName.text = vites?.fullName
            }
            lblEventTitle.text = vites?.eventTitle
            lblEventDescription.text = vites?.eventDetail
            imgProfileImage.sd_setImage(with: URL(string: url), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
            if let eventDate = vites?.eventDate, let eventTime = vites?.eventStartTime{
                let dateFormatter           = DateFormatter()
                dateFormatter.dateFormat    = "YYYY-MM-dd HH:mm:ss"
                if let date = dateFormatter.date(from: eventDate + " " + eventTime){
                    dateFormatter.dateStyle = DateFormatter.Style.long
                    dateFormatter.timeStyle = .short
                    let dateString = dateFormatter.string(from: date)
                    eventDateAndTime.text = dateString
                }
            }
            if let eventGivenLocation =  vites?.eventGivenLocation{
                self.lblAddress.text = eventGivenLocation
            }
            self.setupUber()
            if let isPrivateEvent = vites?.privateEvent, let isCeleb = vites?.isCelebrity{
                if isPrivateEvent || isCeleb{
                hideShowBtnGetDirection(0, topSpace: 0)
                }else{
                hideShowBtnGetDirection(50, topSpace: 5)
                }
            }
            
            if vites?.fileType == "IMAGE"{
                self.btnPlay.isHidden = true
                self.imgEvent.sd_setShowActivityIndicatorView(true)
                if let imgUrl = self.vites?.imageUrl{
                     self.imgEvent.sd_setImage(with: URL(string: imgUrl.convertIntoViteURL()))
                }
            } else {
                self.btnPlay.isHidden = false
                self.imgEvent.sd_setShowActivityIndicatorView(true)
                if let thumbnailUrl = self.vites?.videoProfileThumbNail{
                    self.imgEvent.sd_setImage(with: URL(string: thumbnailUrl.convertIntoViteURL()))
                }
            }
            if let isRated = vites?.rated {
                btnRateOrganizer.isHidden = isRated ? true : false
            }
            showMediaFileCollectionView()
        }
        
        func hideShowBtnGetDirection(_ height: CGFloat, topSpace: CGFloat){
            btnGetDirectionHeightConstraint.constant = height
            btnGetDirectionTopSpaceConstraint.constant = topSpace
        }
        
        func getEventLocation () -> CLLocation? {
            if let vite = self.vites,
                let latitude = vite.latitude,
                let longitude = vite.longitude {
                let lat = Double(latitude)!
                let lon = Double(longitude)!
                return CLLocation(latitude: lat, longitude:lon)
            } else {
                return nil
            }
        }
        
        func setupUber() {
            let dropOffLocation = self.getEventLocation()!
            self.rideHailingService = UberHailingService()
            self.rideHailingService.callRideForCurrentLocationTo(dropOffLocation,
                                                                 presentingViewController: self,
                                                                 completionBlock:nil, failureBlock: {
                                                                    message in
            })
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        func shareWithActivityController() {
            guard let eventId = self.vites?.eventID, let eventTitle = self.vites?.eventTitle, let eventDetail = self.vites?.eventDetail, let eventImageUrl  = self.vites?.imageUrl else {
                showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: localizedString(forKey: "PROBLEM_WITH_NETWORK"))
                return
            }
            BranchShareHelper.ShareEventDetail(self, eventID: eventId, eventTitle: eventTitle , eventDesc: eventDetail, shareText: eventTitle, imageURL: eventImageUrl.convertIntoViteURL())
        }
        
        
        func messsageOrganiser() {
            //NEED_TO_BE_FIXED
            let chatController      = Route.chatMain
            let messageSender             = ChatUser()
            messageSender.fbID            = UserDefaultsManager().userFBID
            messageSender.name            = UserDefaultsManager().userFBFullName
            messageSender.profileImageUrl = UserDefaultsManager().userFBProfileURL
            
            let receiver             = ChatUser()
            
            if let name = vites?.businessName, let imgUrl = vites?.businessImageUrl{
                receiver.fbID            = String(describing: self.vites!.businessID)
                receiver.name            = name
                receiver.profileImageUrl = imgUrl
                chatController.receiver = receiver
                chatController.sender   = messageSender
                self.navigationController?.show(chatController, sender: nil)
                
            }else{
                receiver.fbID            = String(self.vites!.organizerFBID!)
                receiver.name            = self.vites?.fullName
                receiver.profileImageUrl = self.vites?.profileImageUrl
                chatController.receiver = receiver
                chatController.sender   = messageSender
                self.navigationController?.show(chatController, sender: nil)
            }
        }
        
        func messageEventGroup() {
            let groupController = Route.groupChat
            groupController.currentUser = UserDefaultsManager.getCurrentChatUser()
            let event = NEvent(name: (self.vites?.eventTitle)!, uid: (self.vites?.eventID)!, imageUrl: (self.vites?.imageUrl)!)
            groupController.event = event
            AttendeeService.getAllPartiesOfEvent(eventId: (self.vites?.eventID)!, successBlock: { (users) in
                groupController.users = users
                self.navigationController?.show(groupController, sender: nil)
                }, failureBlock: { (message) in
            })
        }
        
        func featuresHide(_ state: Bool){
            btnShare.isHidden = state
            
        }
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            self.userLocation = manager.location!
        }
        
        
        //MARK: ScreenShot API's
        
        @objc func screenShotTaken(){
            guard let eventId = self.vites?.eventID else {
                return
            }
            VitesWebservice.screenShotDetail(eventId, successBlock: { (message) in
            }) { (message) in
                showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: message)
            }
        }
      }
      
//      extension ViteDetailsViewController : RideRequestViewControllerDelegate {
//        func rideRequestViewController(_ rideRequestViewController: RideRequestViewController, didReceiveError error: NSError) {
//            print(error.description)
//           // let errorType = RideRequestViewErrorType(rawValue: error.code)
////            // Handle error here
////            switch errorType {
////            case .:
////                print("Access Token Missing")
////                break
////            // No AccessToken saved
////            case .AccessTokenExpired:
////                print("Access Token Expired")
////                break
////            // AccessToken expired / invalid
////            case .NetworkError:
////                print("Network error")
////                break
////            // A network connectivity error
////            case .NotSupported:
////                print("Not Supported")
////                break
////            // The attempted operation is not supported on the current device
////            case .Unknown:
////                print("Unknown")
////                break
////                // Other error
////            }
//        }
//      }
      
      
      //MARK: UBER DEEPLINKING
      extension ViteDetailsViewController {
        
        @IBAction func uberAction(_ sender: Any) {
            rideWithUber()
        }
        
        
        
        
        func rideWithUber() {
            let builder = RideParametersBuilder()
            guard let userCurrentLat = UserDefaultsManager().userCurrentLatitude , let userCurrentLong = UserDefaultsManager().userCurrentLongitude, let eventLat = vites?.latitude, let eventLongitude = vites?.longitude, let eventLocation = vites?.eventGivenLocation, let eventName = vites?.eventTitle else { return }
            let pickupLocation = CLLocation(latitude: Double(userCurrentLat)!, longitude: Double(userCurrentLong)!)
            let dropoffLocation = CLLocation(latitude: Double(eventLat)!, longitude: Double(eventLongitude)!)
            builder.pickupLocation = pickupLocation
            builder.dropoffLocation = dropoffLocation
            builder.dropoffNickname = eventName
            builder.dropoffAddress = eventLocation
            let rideParameters = builder.build()
            let deeplink = RequestDeeplink(rideParameters: rideParameters, fallbackType: .mobileWeb)
            deeplink.execute()
        }
      }
      
      
      //MARK: IBACTIONS
      extension ViteDetailsViewController{
        
        @IBAction func share(_ sender: AnyObject) {
            self.shareWithActivityController()
        }
        
        @IBAction func rateEvent(_ sender: Any) {
            let controller = Route.rateViewController
            controller.modalPresentationStyle = .overCurrentContext
            controller.isFromMyVite = true
            controller.myVite = vites
            self.present(controller, animated: true, completion: nil)
        }
        
        @IBAction func back(_ sender: AnyObject) {
            guard let nav = self.navigationController else {
                return
            }
            
            nav.popViewController(animated: true)
        }
        
        @IBAction func btnGetDirectionAction(_ sender: AnyObject) {
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                GoogleMapsKit.showMapWithDirections(forStartingPointCoordinate: CLLocationCoordinate2DMake(self.userLocation.coordinate.latitude, self.userLocation.coordinate.longitude), endPointCoordinate: CLLocationCoordinate2DMake(Double(self.vites!.latitude!)!, Double(self.vites!.longitude!)!), directionsMode:.driving)
            } else {
                print("Can't use comgooglemaps://");
                self.performSegue(withIdentifier: "showMapId", sender: nil)
            }
            
        }
        
        override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
            if segue.identifier == "showMapId"
            {
                let destinationVC = segue.destination as! GetDirectionViewController
                destinationVC.vites = self.vites
            }
        }
        
        @IBAction func showTicket(_ sender: AnyObject) {
            let controller = Route.myTicket
            controller.vite = vites
            self.navigationController?.show(controller, sender: nil)
        }
        
        
        @IBAction func openProfile(_ sender: Any) {
            let controller   = Route.profileViewController
            if let fbId = vites?.organizerFBID{
                controller.fbId = fbId
            }
            controller.isForViewingOthersProfile = true
            let nav = UINavigationController.init(rootViewController: controller)
            self.present(nav, animated: true, completion: nil)
        }
        
        
        
        //MARK: Messaging
        @IBAction func messageOrganiser(_ sender: AnyObject) {
            let alert = UIAlertController(title: nil,
                                          message: nil,
                                          preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.purple
            
            
            let messageOrganiser = UIAlertAction(title: localizedString(forKey: "MESSAGE_ORGANIZER"),
                                                 style: .default,
                                                 handler: {
                                                    (action:UIAlertAction) -> Void in
                                                    self.btnMsg.isEnabled = false
                                                    self.messsageOrganiser()
                                                    
            })
            
            let messageToGroup = UIAlertAction(title: localizedString(forKey: "MESSAGE_EVENT_GROUP"),
                                               style: .default) {
                                                (action: UIAlertAction) -> Void in
                                                self.btnMsg.isEnabled = false
                                                self.messageEventGroup()
            }
            
            
            let cancel = UIAlertAction(title: "Cancel",
                                       style: .cancel) {
                                        (action: UIAlertAction) -> Void in
                                        
            }
            alert.addAction(messageOrganiser)
            alert.addAction(messageToGroup)
            alert.addAction(cancel)
            present(alert,
                                  animated: false,
                                  completion: nil)
            
        }
        
        @IBAction func playVid(_ sender: AnyObject) {
            if let url = vites?.eventVideoUrl?.convertIntoViteURL(){
                playVideo(url, viewController: self)
            }
        }

        
        //MARK: Collection View Delegate And DataSource
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            if let media = vites?.media{
                return media.count
            }
            return 0
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let mediaData = vites?.media![indexPath.row]
            if mediaData?.fileType == "IMAGE"{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCVCell", for: indexPath) as! ViteDetailImageCollectionViewCell
                if let url = mediaData?.mediaUrl{
                    cell.mediaImgView.sd_setShowActivityIndicatorView(true)
                    cell.mediaImgView.sd_setImage(with: URL(string: url.convertIntoViteURL()))
                }
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoCVCell", for: indexPath) as! ViteDetailVideoCollectionViewCell
                if let url = mediaData?.thumbNailUrl{
                    cell.thumbNailImgView.sd_setShowActivityIndicatorView(true)
                    cell.thumbNailImgView.sd_setImage(with: URL(string: url.convertIntoViteURL()))
                }
                cell.playVideo = {
                        if let mediaUrl = mediaData?.mediaUrl?.convertIntoViteURL(){
                        playVideo(mediaUrl, viewController: self)
                    }
                }
                return cell
            }
        }
        
        
        
      }
      
      
