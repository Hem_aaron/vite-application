//
//  VitesTableViewCell.swift
//  Vite
//
//  Created by Eeposit1 on 2/16/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
protocol VitesTableViewCellDelegate: class {
    func deleteTableAtRow(_ index:Int)
}
class VitesTableViewCell: UITableViewCell {

    @IBOutlet weak var btnShowTicket: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var btnDeleteVite: UIButton!
    @IBOutlet weak var lblInvitedBy: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    weak var delegate:VitesTableViewCellDelegate?
    var row:Int?
    var showTicket:(()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.width / 2
        self.profileImageView.layer.borderColor = UIColor.white.cgColor
        self.profileImageView.layer.borderWidth = 2.0
        self.profileImageView.clipsToBounds = true
    }


    @IBAction func deleteViteAction(_ sender: AnyObject) {
        if let del = self.delegate {
            del.deleteTableAtRow(self.row!)
        }
    }
    
    @IBAction func showTicketAction(_ sender: AnyObject) {
        if let show = showTicket{
            show()
        }
    }
    
}
