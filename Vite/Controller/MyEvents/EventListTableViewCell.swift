//
//  EventListTableViewCell.swift
//  Vite
//
//  Created by EeposIT_X on 2/15/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import Hero
class EventListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var img_cellBackgroundImage: UIImageView!
    @IBOutlet weak var img_transparent: UIImageView!
    @IBOutlet weak var lbl_EventTitle: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    @IBOutlet weak var lbl_eventType: UILabel!
    
    var pushBlock:(()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        img_transparent.layer.cornerRadius = 10.0
        img_cellBackgroundImage.layer.cornerRadius = 10.0
        img_cellBackgroundImage.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //    @IBAction func deleteEvent(sender: AnyObject) {
    //        if let block = deleteBlock {
    //            block()
    //        }
    //    }
    @IBAction func pushNotificationSetting(_ sender: AnyObject) {
        if let block = pushBlock {
            block()
        }
    }
}
