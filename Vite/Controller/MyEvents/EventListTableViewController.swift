//
//  EventListTableViewController.swift
//  Vite
//
//  Created by EeposIT_X on 2/15/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import MapKit
import ViewAnimator
import Hero

class EventListTableViewController: WrapperTableViewController {
    var eventsArr = [EventsOrganizer]()
    var selectedEvent:EventsOrganizer?
    var currentLocation = CLLocation()
    var isFromPeopleList = false
    var isFromCreateBtn = false
    var showAnim:Bool?
    let indicator = ActivityIndicator()
    let message = localizedString(forKey: "NO_EVENT")
    let lblMessage = UILabel()
    let cursor = VCursor()
    @IBOutlet weak var openRearView: UIBarButtonItem!
}

//MARK: - View Life Cycles
extension EventListTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "My Events"
        self.showAnim = !self.isFromPeopleList
        //self.view.animateRandom()
        if let lat = UserDefaultsManager().userCurrentLatitude, let long = UserDefaultsManager().userCurrentLongitude {
            if let lat = Double( lat) , let long = Double(long) {
                currentLocation = CLLocation(latitude: lat, longitude: long)
            }
        }
        indicator.tintColor = UIColor.gray
        indicator.start()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.fetchEvents(shouldReset: true)
    }
}
//MARK: - IBActions
extension EventListTableViewController {
    
    @IBAction func back(_ sender: AnyObject) {
        if self.isFromPeopleList {
            if isFromCreateBtn{
                Route.goToHomeController()
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            self.tableView.animate(animations: [AnimationsList.fromBottom], reversed: true, initialAlpha: 1, finalAlpha: 0, delay: 0, duration: 0.8, completion: {
                Route.goToHomeController()
            })
        }
    }
    
    @IBAction func createEvent(_ sender: AnyObject) {
        let swipedUsers = SwappedPeopleService.getPeopleList()
        if swipedUsers.count > 0 {
            let createEventOptions = Route.createEventOptions
            createEventOptions.modalPresentationStyle = .overCurrentContext
            createEventOptions.modalTransitionStyle = .crossDissolve
            self.present(createEventOptions, animated: true, completion: nil)
            createEventOptions.delegate = self
        } else {
            let createEventController = Route.CreateEvent
            createEventController.isFromMyEventMenu = true
            self.navigationController?.show(createEventController, sender: nil)
        }
    }
    
    @IBAction func createNewEvent(_ sender: AnyObject) {
        
    }
}

// MARK: - Table view data source and Delegate
extension EventListTableViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if eventsArr.count == 0 {
            self.lblMessage.isHidden = false
        }
        return eventsArr.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 119
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "EventListCellIdentifier"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! EventListTableViewCell
        let events = self.eventsArr[indexPath.row]
        if events.fileType == "IMAGE" {
            cell.img_cellBackgroundImage.sd_setImage(with: URL(string:events.imageUrl!.convertIntoViteURL()))
        } else {
            if let thumbNailUrl = events.videoProfileThumbNail{
                cell.img_cellBackgroundImage.sd_setImage(with: URL(string:thumbNailUrl.convertIntoViteURL()))
            }
        }
        cell.lbl_EventTitle.text = events.eventTitle
        cell.pushBlock = { [weak self] in
            self?.showPicker(indexPath,event: events)
        }
        
        if events.isPrivateEvent!{
            cell.lbl_eventType.text = "Private"
        }
        else if events.isPaidEvent!{
            cell.lbl_eventType.text = "Paid"
        }
        else{
            cell.lbl_eventType.text = " "
        }
        cell.img_cellBackgroundImage.hero.id = events.entityId
        
        if let  lat = events.location?.latitude , let long = events.location?.longitude  {
            let eventLocation = CLLocation(latitude: Double(lat)!, longitude: Double(long)!)
            cell.lbl_distance.text = String(round(getDistanceInMile(userCurrentLocation: currentLocation, eventLocation: eventLocation)))
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = Bundle.main.loadNibNamed("EventListHeader", owner: nil, options: nil)?[0] as! UIView
        return header
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if isFromPeopleList {
            return 40
        }
        return 0
    }
    
    override  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let event = self.eventsArr[indexPath.row]
        if isFromPeopleList {
            self.showSendViteToSwappedPeopleConfirmation((event.entityId)!)
        } else {
            let controller = Route.showEventVC
            controller.event = event
            let nav = UINavigationController(rootViewController: controller)
            nav.hero.isEnabled = true
            self.present(nav, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.cursor.hasNextPage() && indexPath.row == (self.eventsArr.count - 1) {
            self.fetchEvents(shouldReset: false)
        }
    }
}

//MARK: - Private Methods
extension EventListTableViewController {
    func fetchEvents(shouldReset reset:Bool) {
        indicator.tintColor = UIColor.gray
        indicator.start()
        if reset {
            self.cursor.reset()
        }
        
        EventServices.getEventListWithPagination(cursor.nextPage(), pageSize: cursor.pageSize, successBlock: { [weak self] (eventList, eventListEventOrg, totalCount)  in
            guard let eventList = eventListEventOrg else {
                showAlert(controller: self!, title: "", message: (self?.message)!)
                self?.indicator.stop()
                return
            }
            
            if reset {
                self?.eventsArr.removeAll()
            }
            
            self?.eventsArr += eventList
            self?.cursor.totalCount = totalCount.intValue
            self?.cursor.totalLoadedDataCount = (self?.eventsArr.count)!
            self?.tableView.reloadData()
            if let anim = self?.showAnim{
                self?.showAnim = false
                self?.tableView.animate(animations: [AnimationsList.fromBottom], reversed: false, initialAlpha: 0, finalAlpha: 1, delay: 0, duration: 1, completion: nil)
            }
            if self?.eventsArr.count == 0 {
                self?.lblMessage.isHidden = false
                setUpLblMsgs((self?.message)!, view: (self?.view)!, lblMessage: (self?.lblMessage)!)
            } else {
                self?.lblMessage.isHidden = true
            }
            self?.indicator.stop()
            }, failureBlock:{[weak self] _ in ()
                self?.indicator.stop()
        })
    }
    
    func gotoHomeController() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showSendViteToSwappedPeopleConfirmation(_ eventId:String) {
        let alert = UIAlertController(title: "",
                                      message: localizedString(forKey: "SEND_VITE_TO_SWIPED_PEOPLE"),
                                      preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok",
                                     style: .default,
                                     handler: { (action:UIAlertAction) -> Void in
                                        self.inviteAllSwappedUsers(eventId)
        })
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .default) { (action: UIAlertAction) -> Void in
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        present(alert,
                animated: false,
                completion: nil)
    }
    
    func inviteAllSwappedUsers(_ eventId:String) {
        VEventWebService.inviteUsers(SwappedPeopleService.getPeopleList(),
                                     withEventId:eventId,
                                     successBlock: {
                                        SwappedPeopleService.clearSwappedList()
                                        self.gotoHomeController()
        }, failureBlock: {
            message in
            showAlert(controller: self, title: "", message: localizedString(forKey: "FAILED_SENDING_VITE"))
        })
    }
    
    func deleteEvent(_ indexPath:IndexPath, eventID:String) {
        
        let alert = UIAlertController(title: "",
                                      message: localizedString(forKey: "SURE_DELETE_EVENT"),
                                      preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok",
                                     style: .default,
                                     handler: { (action:UIAlertAction) -> Void in
                                        self.indicator.start()
                                        WebService.request(method: .post, url: getDeleteEventAPIForEvent(eventID: eventID), parameter: nil, header: nil, isAccessTokenRequired: false, success: {
                                            (response) -> Void in
                                            guard let response = response.result.value as? [String:AnyObject] else {
                                                return
                                            }
                                            
                                            if (response["success"] as? Bool)! {
                                                self.eventsArr.remove(at: indexPath.row)
                                                self.tableView.deleteRows(at: [indexPath], with: .none)
                                                let delay = 0.5 * Double(NSEC_PER_SEC)
                                                let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
                                                
                                                DispatchQueue.main.asyncAfter(deadline: time, execute: {
                                                    self.tableView.reloadData() })
                                                
                                                self.indicator.stop()
                                            }
                                            
                                        }, failure:{ (message, APIError) -> Void in
                                            self.view.makeToast(message: localizedString(forKey: "DELETE_EVENT_FAILED"))
                                            self.indicator.stop()
                                        })
                                        
        })
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .default) { (action: UIAlertAction) -> Void in
                                            
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        present(alert,
                animated: false,
                completion: nil)
        
    }
    
    func showPicker(_ indexPath: IndexPath,event: EventsOrganizer){
        let alert = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.purple
        
        guard let pushNotificationStatus = event.sendPushNotification, let eventId = event.entityId else {
            showAlert(controller: self, title: "", message:localizedString(forKey: "ERROR_WITH_EVENT"))
            return
        }
        
        let pushNotificationMessage = event.sendPushNotification! ? localizedString(forKey: "TURN_OFF_PUSH_NOTIFICATION") : localizedString(forKey: "TURN_ON_PUSH_NOTIFICATION")
        let turnONNotification = UIAlertAction(title: pushNotificationMessage,
                                               style: .default,
                                               handler: {
                                                (action:UIAlertAction) -> Void in
                                                self.serviceCallForEventPushNotification(eventId,pushNotificationStatus: !pushNotificationStatus)
        })
        
        
        func addInviteFriendsAction() {
            let inviteFriends = UIAlertAction(title: localizedString(forKey: "INVITE_TO_EVENT"),
                                              style: .default,
                                              handler: {
                                                (action:UIAlertAction) -> Void in
                                                let newProgramVar = CreateEventModel(eventID: eventId, eventTitle: event.eventTitle, eventDetail: event.eventDetail, imageUrl:event.imageUrl?.convertIntoViteURL())
                                                
                                                let inviteVC = Route.inviteVC
                                                inviteVC.eventVar = newProgramVar
                                                self.present(inviteVC, animated: true, completion: nil)
                                                
                                                
            })
            alert.addAction(inviteFriends)
            
        }
        
        
        let deleteEvent = UIAlertAction(title: localizedString(forKey: "DELETE_EVENT"),
                                        style: .default,
                                        handler: {
                                            (action:UIAlertAction) -> Void in
                                            self.deleteEvent(indexPath, eventID: eventId)
        })
        
        func addDeleteAllSessionForRecurringAction() {
            let deleteAllSession = UIAlertAction(title: localizedString(forKey: "DELETE_ALL_SESSIONS"),
                                                 style: .default,
                                                 handler: {
                                                    (action:UIAlertAction) -> Void in
                                                    self.deleteAllRecurringEventSessions(event.scheduledEventId!)
            })
            alert.addAction(deleteAllSession)
            self.tableView.reloadData()
        }
        
        
        func addReccuringAction() {
            let recurringEvent = UIAlertAction(title: localizedString(forKey: "MAKE_RECURRING_EVENT"),
                                               style: .default,
                                               handler: {
                                                (action:UIAlertAction) -> Void in
                                                let createEventVC = Route.CreateEvent
                                                createEventVC.isEditable = true
                                                createEventVC.isFromMyEventMenu = true
                                                createEventVC.event = event
                                                self.navigationController?.show(createEventVC, sender: nil)
            })
            alert.addAction(recurringEvent)
        }
        // For recurring to delete all recurring event session
        
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel) {
                                            (action: UIAlertAction) -> Void in
        }
        
        alert.addAction(turnONNotification)
        
        func addRecommendToUserAction() {
            let recommendEvent = UIAlertAction(title: localizedString(forKey: "RECOMMEND_USERS"),
                                               style: .default,
                                               handler: {
                                                (action:UIAlertAction) -> Void in
                                                self.reccomendUsersToEvent(eventId)
            })
            alert.addAction(recommendEvent)
        }
        var isPaid = Bool()
        var isExpired = Bool()
        if let paid = event.isPaidEvent{
            isPaid = paid
        }
        if let expired = event.eventExpired{
            isExpired = expired
        }
        if !isExpired {
            if !isPaid{
                addInviteFriendsAction()
                addRecommendToUserAction()
            }
        }
        
        if let isRecurring = event.recurringEvent {
            if isRecurring{
                addDeleteAllSessionForRecurringAction()
            }
        }
        alert.addAction(deleteEvent)
        alert.addAction(cancelAction)
        
        present(alert,
                animated: false,
                completion: nil)
    }
    
    func deleteAllRecurringEventSessions(_ scheduledEventId: String){
        
        let alert = UIAlertController(title: "",
                                      message: localizedString(forKey: "DELETE_SESSIONS_OF SCHEDULED_EVENT"),
                                      preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok",
                                     style: .default,
                                     handler: { (action:UIAlertAction) -> Void in
                                        self.indicator.start()
                                        
                                        EventServices().deleteAllSessionOfScheduledEvent(scheduledEventId, successBlock: { (msg) in
                                            self.indicator.stop()
                                            showAlert(controller: self, title: "", message: msg)
                                        }, failureBlock: { (msg) in
                                            self.indicator.stop()
                                            showAlert(controller: self, title: "", message: msg)
                                        })
        })
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .default) { (action: UIAlertAction) -> Void in
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        present(alert,
                animated: false,
                completion: nil)
    }
    
    func reccomendUsersToEvent(_ eventId: String) {
        let controller = Route.Private
        controller.eventID = eventId
        controller.isFromMyEvent = true
        let targetNavigationController = UINavigationController(rootViewController: controller)
        self.present(targetNavigationController, animated: true, completion: nil)
    }
    
    func serviceCallForEventPushNotification(_ eventID: String, pushNotificationStatus: Bool) {
        let url  = kAPIEventPUshNotification + eventID + "/\(pushNotificationStatus)"
        self.indicator.start()
        
        WebService.request(method: .post, url: url, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else{
                return
            }
            if success {
                self.fetchEvents(shouldReset: true)
            }
            
        }, failure: { (message, error) in
            self.view.makeToast(message: localizedString(forKey: "ERROR_PUSH_NOTIFICATION_UPDATE"))
            self.indicator.stop()
        })
    }
}

// MARK: - CreateEventOptionsDelegate

extension EventListTableViewController:CreateEventOptionsViewControllerDelegate {
    func selectedCreateEventsForUsers() {
        let controller = Route.CreateEvent
        controller.isForAddingUsersToInviteList = true
        self.navigationController?.show(controller, sender: nil)
    }
    
    func selectedAddUsersToExistingEvent() {
        let storyboard = UIStoryboard(name: "MyEvents", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "EventListNav") as! UINavigationController
        let eventController = controller.viewControllers[0] as! EventListTableViewController
        eventController.isFromPeopleList = true
        self.slideMenuController()?.changeMainViewController(controller, close: true)
    }
    
    func selectedDecideLater() {
        self.navigationController?.show(Route.CreateEvent, sender: nil)
    }
}


