//
//  MyEventDetailImageCollectionViewCell.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 5/8/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class MyEventDetailImageCollectionViewCell: UICollectionViewCell {

     @IBOutlet weak var mediaImgView: UIImageView!
    override func layoutSubviews() {
        super.layoutSubviews()
        self.mediaImgView.layer.cornerRadius = 10
        self.mediaImgView.clipsToBounds = true
    }

}
