//
//  RefundAlertViewController.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 7/24/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

protocol attendeeListDelegate {
    func reloadAttendeeList()
}

class RefundAlertViewController: WrapperController {
    
    var delegate: attendeeListDelegate?
    var attendee: MyEventAttendee?
    var eventId: String?
    
    @IBOutlet weak var lblSure: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateAttendeeDetail()
    }
    
    func populateAttendeeDetail(){
        if let name = attendee?.fullName, let amt = attendee?.amount{
            lblSure.text = "Are you sure you want to refund $\(amt) to \(name)?"
        }
    }
    
    func refundAttendeeTicket(_ attendeeFbId: String){
        AttendeeService.refundTicket(eventId!, attendeeId: attendeeFbId, successBlock: { [weak self] (message) in
            self?.delegate?.reloadAttendeeList()
            self?.dismiss(animated: true, completion: nil)
        }) { [weak self](message) in
            showAlert(controller: self!, title: "", message: message)
            self?.dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func refundTicket(_ sender: Any) {
        if let attendeeId = attendee?.facebookId{
            refundAttendeeTicket(String(describing: attendeeId))
        }
    }
    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
