//
//  RefundAttendeeListCell.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 7/2/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class RefundAttendeeListCell: UITableViewCell {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblAttendeeName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblAcceptedDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgProfile.layer.cornerRadius = 25.0
        imgProfile.clipsToBounds = true
    }

    
}
