//
//  RefundViewController.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 7/2/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class RefundViewController: WrapperController, attendeeListDelegate {
    var userArray:[MyEventAttendee]? = [MyEventAttendee]()
    let indicator = ActivityIndicator()
    var eventId = String()
    var isForMessagingUser = false
    @IBOutlet weak var attendeeTableView: UITableView!
    @IBOutlet weak var noAttendeeLabel: UILabel!
    @IBOutlet weak var refundAllView: UIView!
    @IBOutlet weak var refundAllViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnRefundAll: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ServiceCall(eventId)
        messageUserLogic()
    }
    
    func reloadAttendeeList() {
        ServiceCall(eventId)
        self.view.makeToast(message: localizedString(forKey: "TICKET_REFUNDED"))
    }
    
    func messageUserLogic() {
        if isForMessagingUser {
            self.navigationItem.title = "Attendee List"
            refundAllViewHeight.constant = 0
            btnRefundAll.isHidden = true
        } else {
            self.navigationItem.title = "Refund User"
            refundAllViewHeight.constant = 39
            btnRefundAll.isHidden = false
        }
    }
    
    //MARK: Service Call
    
    func ServiceCall(_ id: String){
        self.userArray?.removeAll()
        self.indicator.start()
        self.view.isUserInteractionEnabled = false
        AttendeeService.getAllAttendeeList(eventId: id, successBlock: { [unowned self] (users) in
            if users.count != 0 {
                self.view.isUserInteractionEnabled = true
                for user in users{
                    self.userArray?.append(user)
                }
                self.attendeeTableView.isHidden = false
                self.noAttendeeLabel.isHidden = true
            }else{
                self.attendeeTableView.isHidden = true
                self.refundAllViewHeight.constant = 0
                self.btnRefundAll.isHidden = true
                self.noAttendeeLabel.isHidden = false
            }
            self.attendeeTableView.reloadData()
            self.indicator.stop()
        }) { [unowned self] (message) in
            self.view.isUserInteractionEnabled = true
            showAlert(controller: self, title: "", message: message)
            self.indicator.stop()
        }
    }
    
    // MARK: Table View DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let arr = userArray {
            return arr.count
        }else{
            return 0
        }
    }
    
    // MARK: Table view Delegates
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttendeesCell") as! RefundAttendeeListCell
        let attendee = self.userArray![indexPath.row]
        cell.lblAttendeeName.text = attendee.fullName!
        if let profileUrl = attendee.profileImageUrl{
            let url = profileUrl.convertIntoViteURL()
            cell.imgProfile.sd_setImage(with: URL(string: url), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
        }
        if let amt = attendee.amount {
            cell.lblAmount.text = "$\(amt)"
        } else {
            cell.lblAmount.isHidden = true
        }
        cell.lblAmount.isHidden = self.isForMessagingUser ? true : false
        if let invitedDate = attendee.acceptedDate {
            let date = Date(timeIntervalSince1970: invitedDate.doubleValue / 1000)
            cell.lblAcceptedDate.text = agoDates(date)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        let attendee = self.userArray![indexPath.row]
        if !isForMessagingUser {
            let alertController = Route.refundAlertVC
            alertController.attendee = attendee
            alertController.eventId = eventId
            alertController.delegate = self
            self.present(alertController, animated: true, completion: nil)
        } else {
            messsageUser(attendee)
        }
    }
    
    func messsageUser(_ userData: MyEventAttendee) {
        //ChatMainViewController
        let chatBoard = UIStoryboard(name: "Chat", bundle: nil)
        let chatController      = chatBoard.instantiateViewController(withIdentifier: "ChatMainViewController") as! ChatMainViewController
        let messageSender             = ChatUser()
        if UserDefaultsManager().isBusinessAccountActivated {
            messageSender.fbID            = UserDefaultsManager().userBusinessOwnerId
            messageSender.name            = UserDefaultsManager().userBusinessFullName
            messageSender.profileImageUrl = UserDefaultsManager().userBusinessProfileImageUrl
            
        }else {
            messageSender.fbID            = UserDefaultsManager().userFBID
            messageSender.name            = UserDefaultsManager().userFBFullName
            messageSender.profileImageUrl = UserDefaultsManager().userFBProfileURL
        }
        let receiver             = ChatUser()
        receiver.fbID            = String(describing: userData.facebookId!)
        receiver.name            = userData.fullName
        receiver.profileImageUrl = userData.profileImageUrl
        chatController.receiver = receiver
        chatController.sender   = messageSender
        self.navigationController?.show(chatController, sender: nil)
    }
    
    func alertRefundAll(){
        let alert = UIAlertController(title: "",
                                      message: localizedString(forKey: "REFUND_ALL_ATTENDEE"),
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Yes",
                                     style: .default,
                                     handler: { [unowned self] (action:UIAlertAction) -> Void in
                                        AttendeeService.refundAllTicket(self.eventId, successBlock: { (message) in
                                            self.dismiss(animated: true, completion: nil)
                                            showAlert(controller: self, title: "", message: message)
                                        }) { (message) in
                                            self.dismiss(animated: true, completion: nil)
                                            showAlert(controller: self, title: "", message: message)
                                        }
                                        
        })
        
        let cancelAction = UIAlertAction(title: "No",
                                         style: .default) {
                                            (action: UIAlertAction) -> Void in
                                            
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert,
                animated: false,
                completion: nil)
    }
    
    //MARK: IBACTIONS
    @IBAction func back(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func refundAll(_ sender: AnyObject) {
        alertRefundAll()
    }
    
}
