//
//  ShowEventViewController.swift
//  Vite
//
//  Created by EeposIT_X on 2/16/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import MapKit
import MessageUI
import Social
import AVFoundation
import Hero
class ShowEventViewController: WrapperController, UICollectionViewDelegate, UICollectionViewDataSource, CreateEventsViewControllerDelegate  {
    
    let indicator = ActivityIndicator()
    var privateEvent = false
    var event:EventsOrganizer?
    var currentLocation = CLLocation()
    var eventLocation = CLLocation()
    
    @IBOutlet weak var organizerFullName: UILabel!
    @IBOutlet weak var organizerImage: CircleImage!
    @IBOutlet weak var eventView: UIView!
    @IBOutlet weak var btnEditEvent: UIButton!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventDescription: UILabel!
    @IBOutlet weak var eventTime: UILabel!
    @IBOutlet weak var eventDistance: UILabel!
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var viteNow: UIButton!
    @IBOutlet weak var viteNowHeight: NSLayoutConstraint!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var mediaCollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mediaView: UIView!
    @IBOutlet weak var mediaCollectionView: UICollectionView!
    @IBOutlet weak var descView: UIView!
    @IBOutlet weak var descViewHeight: NSLayoutConstraint!
    @IBOutlet weak var descTopSpaceConstraint: NSLayoutConstraint!
    // Revenue Constraints
    @IBOutlet weak var revenueView: UIView!
    @IBOutlet weak var lblTicketSold: UILabel!
    @IBOutlet weak var lblTotalRevenue: UILabel!
    @IBOutlet weak var lblPaymentToVite: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblPriceAmount: UILabel!
    @IBOutlet weak var revenueTopSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var revenueHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnRefundTicket: UIButton!
    @IBOutlet weak var btnEmailGrp: UIButton!
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var btnViteAll: UIButton!
    //Refund Ticket and Message User
    var RefundTicket = 0
    var MessageUser = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.eventImage.hero.id = event?.entityId
        self.populateData()
        setBtnDesign(btnViteAll)
        setBtnDesign(viteNow)
        if UserDefaultsManager().userPremiumStatus! {
            self.btnViteAll.isEnabled = true
            self.btnViteAll.isHidden = false
        }
    }
    func setBtnDesign(_ btn:UIButton){
        btn.layer.borderColor = UIColor.white.cgColor
        btn.layer.borderWidth = 2.0
    }
    @IBAction func showAttendee(_ sender: UIBarButtonItem) {
        
        let scanTicketVC = Route.scanTicketViewController
        scanTicketVC?.event = self.event
        scanTicketVC?.isFromMyEvents = true
        self.navigationController?.show(scanTicketVC!, sender: nil)
        
    }
    
    @IBAction func openProfile(_ sender: Any) {
        let ctrl = Route.profileViewController
        let nav = UINavigationController.init(rootViewController: ctrl)
        self.present(nav, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        guard let eventId = event?.entityId else {
            return
        }
        
        EventServices.getEventDetail(eventID: eventId, successBlock: { [weak self] (e) -> () in
            self?.event = e
            self?.populateData()
            self?.mediaCollectionView.reloadData()
        })
        self.viteNow.isEnabled = true
    }
    
    func showMediaFileCollectionView() {
        if let media =  event?.media {
            mediaCollectionViewHeightConstraint.constant = media.count == 0 ? 0 : 250
            mediaView.isHidden = media.count == 0 ? true : false
        } else {
            mediaCollectionViewHeightConstraint.constant = 0
            mediaView.isHidden = true
        }
    }
    
    //MARK: Revenue Logic
    func hideRevenueView(_ height: CGFloat, topSpace: CGFloat, alphaValue: CGFloat){
        self.revenueTopSpaceConstraint.constant = topSpace
        self.revenueHeightConstraint.constant = height
        self.revenueView.alpha = alphaValue
    }
    
    func hideDescView(_ alpha: CGFloat, height: CGFloat, topSpace: CGFloat){
        descView.alpha = alpha
        descViewHeight.constant = height
        descTopSpaceConstraint.constant = topSpace
    }
    
    func hideLogic(logicFor logic: String){
        switch logic {
        case "Paid":
            self.btnRefundTicket.isHidden = false
            self.lblPrice.isHidden = false
            self.lblPriceAmount.isHidden = false
            self.btnEmailGrp.isHidden = false
            self.btnInvite.isHidden = true
        case "FeaturedOrNormal":
            btnInvite.isHidden = false
            btnRefundTicket.isHidden = true
            btnEmailGrp.isHidden = true
        case "FeaturedAndPaid":
            self.btnRefundTicket.isHidden = false
            self.lblPrice.isHidden = false
            self.lblPriceAmount.isHidden = false
            self.btnEmailGrp.isHidden = false
            self.btnInvite.isHidden = true
        case "StartedEvent":
            btnInvite.isHidden = true
            btnRefundTicket.isHidden = true
            btnEmailGrp.isHidden = true
        default:
            break
        }
    }
    
    func populateData() {
        
        if let lat = UserDefaultsManager().userCurrentLatitude, let long = UserDefaultsManager().userCurrentLongitude {
            if let lat = Double( lat) , let long = Double(long) {
                currentLocation            = CLLocation(latitude: lat, longitude:  long)
                eventLocation              = CLLocation(latitude: Double((event!.location?.latitude)!)!, longitude:  Double((event!.location?.longitude)!)!)
            }
        }
        
        self.eventTitle.text       = event?.eventTitle
        if event?.eventDetail?.count == 0 {
            hideDescView(0, height: 0, topSpace: 0)
        } else {
            hideDescView(1, height: 130, topSpace: 5)
        }
        self.eventDescription.text = event?.eventDetail
        self.organizerFullName.text = UserDefaultsManager().userFBFullName
        self.organizerImage.sd_setImage(with: (URL(string: (UserDefaultsManager().userFBProfileURL?.convertIntoViteURL())!)))
        privateEvent = (event?.isPrivateEvent)!
        if let isEventExpired = event?.eventExpired {
            if privateEvent || isEventExpired {
                self.viteNow.isHidden = true
                self.viteNowHeight.constant = 0
            } else {
                self.viteNow.isHidden = false
                self.viteNowHeight.constant = 50
            }
        }
        if let paid = event?.isPaidEvent, let featured = event?.featuredEvent {
            if paid{
                hideLogic(logicFor: "Paid")
                hideRevenueView(300, topSpace: 5, alphaValue: 1)
            } else if featured {
                hideLogic(logicFor: "FeaturedOrNormal")
                hideRevenueView(300, topSpace: 5, alphaValue: 1)
            } else if paid && featured {
                hideLogic(logicFor: "FeaturedAndPaid")
                hideRevenueView(300, topSpace: 5, alphaValue: 1)
            } else {
                hideLogic(logicFor: "FeaturedOrNormal")
                hideRevenueView(0, topSpace: 0, alphaValue: 0)
            }
        }
        if let price = event?.eventPrice{
            self.lblPriceAmount.text = "$\(price)"
        }
        self.eventDistance.text    = String(round(getDistanceInMile(userCurrentLocation: currentLocation, eventLocation: eventLocation)) ) + " miles away"
        let eventDate               = event?.eventDate
        let eventStartTime          = event?.eventStartTime
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "YYYY-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: eventDate! + " " + eventStartTime!)
        eventTime.text             = timeAgoSince(date: date!)
        if eventTime.text == "Already started!" {
            hideLogic(logicFor: "StartedEvent")
        }
        
        if event?.fileType == "IMAGE" {
            btnPlay.isHidden = true
            self.eventImage.sd_setShowActivityIndicatorView(true)
            self.eventImage.sd_setImage(with: URL(string:(event?.imageUrl)!.convertIntoViteURL()))
        } else {
            btnPlay.isHidden = false
            self.eventImage.sd_setShowActivityIndicatorView(true)
            if let thumbNailUrl = event?.videoProfileThumbNail {
                self.eventImage.sd_setImage(with: URL(string:thumbNailUrl.convertIntoViteURL()))
            }
        }
        showMediaFileCollectionView()
        if let revenue = event?.revenue{
            lblTicketSold.text = "\(Int(revenue.noOfTickets!))"
            lblTotalRevenue.text = "\(revenue.totalRevenue!)"
            lblPaymentToVite.text = "\(revenue.vitePayment!)"
        }
    }
    
    func showToast(_ message:String) {
        // self.back(message)
    }
    
    //MARK: CreateEventsViewControllerDelegate
    func editedEvent(_ eventID: String) {
        EventServices.getEventDetail(eventID: eventID, successBlock: { [unowned self] (e) -> () in
            self.event = e
            self.populateData()
            self.mediaCollectionView.reloadData()
        })
    }
    
    func alertEmailGroup(){
        let alert = UIAlertController(title: "",
                                      message: localizedString(forKey: "SEND_EMAIL_TO_ATTENDEES"),
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Yes",
                                     style: .default,
                                     handler: { [unowned self] (action:UIAlertAction) -> Void in
                                        self.indicator.start()
                                        self.view.isUserInteractionEnabled = false
                                        guard let eventId = self.event?.entityId else {
                                            return
                                        }
                                        WebService.request(method: .post, url: KAPIEmailGroup, success: {
                                            (response) in
                                            guard let response = response.result.value as? [String : AnyObject] else {
                                                return
                                            }
                                            if let msg = response["message"] as? String {
                                                showAlert(controller: self, title: localizedString(forKey: "ALERT"), message: msg)
                                            }
                                            self.indicator.stop()
                                            self.view.isUserInteractionEnabled = true
                                        }, failure: {
                                            (message, error) in
                                            self.indicator.stop()
                                            showAlert(controller: self, title: localizedString(forKey: "FAILURE"), message: message)
                                            self.view.isUserInteractionEnabled = true
                                        })
        })
        
        let cancelAction = UIAlertAction(title: "No",
                                         style: .default) {
                                            (action: UIAlertAction) -> Void in
                                            
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert,
                animated: false,
                completion: nil)
    }
    
    func shareWithActivityController() {
        var sharingItems = [AnyObject]()
        
        if let title = event?.eventTitle {
            sharingItems.append(title as AnyObject)
        }
        
        let url = URL(string: "https://itunes.apple.com/us/app/vite-exclusive-events/id1087246453?mt=8")
        sharingItems.append(url! as AnyObject)
        
        let eventImageView = imageWithView(eventView)
        sharingItems.append(eventImageView)
        
        let activityVC = UIActivityViewController(activityItems: sharingItems, applicationActivities: nil)
        
        activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
        
        self.present(activityVC, animated: true, completion: nil)
    }
    
    func showActionSheet() {
        
        let alert = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: .actionSheet)
        
        let facebook = UIAlertAction(title:localizedString(forKey: "SHARE_WITH_FACEBOOK"),
                                     style: .default,
                                     handler: {
                                        [unowned self] (action:UIAlertAction) -> Void in
                                        self.fbShare()
                                        FacebookShareHelper.shareWithShareDialog(self, urlString: "www.google.com")
        })
        
        let twitter = UIAlertAction(title: localizedString(forKey: "SHARE_WITH_TWITTER"),
                                    style: .default) {[unowned self]
                                        (action: UIAlertAction) -> Void in
                                        self.twitterShare(self)
                                        
        }
        
        
        let cancel = UIAlertAction(title: "Cancel",
                                   style: .cancel) {
                                    (action: UIAlertAction) -> Void in
                                    
        }
        alert.addAction(facebook)
        alert.addAction(twitter)
        alert.addAction(cancel)
        present(alert,
                animated: false,
                completion: nil)
        
    }
    
    //Branch FB Share
    func shareWithBranch(){
        BranchShareHelper.ShareEventDetail(self, eventID: (self.event?.entityId)!, eventTitle: (self.event?.eventTitle)!, eventDesc: (self.event?.eventDetail)!, shareText: (self.event?.eventTitle)!, imageURL: (self.event?.imageUrl?.convertIntoViteURL())!)
    }
    
    //Facebook Share Action
    func fbShare(){
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) {
            let facebookSheet:SLComposeViewController = SLComposeViewController (forServiceType: SLServiceTypeFacebook)
            facebookSheet.setInitialText("Share on Facebook")
            facebookSheet.add(URL(string: "http://www.eeposit.com"))
            self.present(facebookSheet, animated: true, completion: nil)
        } else {
            let alert = UIAlertController (title: localizedString(forKey: "ACCOUNTS"), message:localizedString(forKey: "LOGIN_TO_FB"), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction (UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //Twitter Share Action
    func twitterShare(_ sender: AnyObject) {
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter){
            let twitterSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            twitterSheet.setInitialText("Share on Twitter")
            twitterSheet.add(URL(string: "www.vite.com"))
            self.present(twitterSheet, animated: true, completion: nil)
        } else {
            let alert = UIAlertController (title: localizedString(forKey: "LOGIN_TO_FB"), message:localizedString(forKey: "LOGIN_TO_TWITTER"), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction (UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        if segue.identifier == "attendeesList" {
            let destinationVC = segue.destination as! AttendeesViewController
            destinationVC.event = self.event
        }
    }
    
    //MARK: Collection View Delegate And DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let media = event?.media {
            return media.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let mediaData = event?.media![indexPath.row]
        if mediaData?.fileType == "IMAGE" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCVCell", for: indexPath) as! MyEventDetailImageCollectionViewCell
            if let url = mediaData?.mediaUrl {
                cell.mediaImgView.sd_setShowActivityIndicatorView(true)
                cell.mediaImgView.sd_setImage(with: URL(string: url.convertIntoViteURL()))
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoCVCell", for: indexPath) as! MyEventDetailVideoCollectionViewCell
            if let url = mediaData?.thumbNailUrl {
                cell.thumbNailImgView.sd_setShowActivityIndicatorView(true)
                cell.thumbNailImgView.sd_setImage(with: URL(string: url.convertIntoViteURL()))
            }
            cell.playVideo = { [unowned self] in
                if let mediaUrl = mediaData?.mediaUrl?.convertIntoViteURL() {
                    playVideo(mediaUrl, viewController: self)
                }
            }
            return cell
        }
    }
}

//MARK: - IBActions
extension ShowEventViewController {
    
    @IBAction func playVid(_ sender: UIButton) {
        if let url = event?.eventVideoUrl {
            playVideo(url.convertIntoViteURL(), viewController: self)
        }
    }
    
    @IBAction func goBack(_ sender: UIBarButtonItem) {
       self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func shareEvent(_ sender: UIButton) {
        shareWithBranch()
    }
    
    @IBAction func viteNow(_ sender: UIButton) {
        self.indicator.start()
        self.viteNow.isEnabled = false
        
        WebService.request(method: .post, url: kAPISendViteNow + (event?.entityId)! + "/EVENT_UPDATE", success: {  [unowned self] (response) -> Void in
            
            guard let response = response.result.value as! [String: AnyObject]? else {
                return
            }
            
            guard let status = response["success"] as? Bool else {
                return
            }
            
            if status {
                self.navigationController?.view.makeToast(message: localizedString(forKey: "VIEWNOW_NOTIFIED_ALL"))
                if UserDefaultsManager().userPremiumStatus! {
                    self.viteNow.isEnabled = true
                }
                self.indicator.stop()
            }
            
            }, failure: {
                [unowned self]   (message, error) -> Void in
                showAlert(controller: self, title: "", message: message)
                self.viteNow.isEnabled = true
                self.indicator.stop()
        })
    }
    
    @IBAction func editEvent(_ sender: UIButton) {
        let editEventVC = Route.CreateEvent
        editEventVC.isEditable = true
        editEventVC.delegate = self
        editEventVC.event = self.event
        self.navigationController?.show(editEventVC, sender: nil)
        
    }
    
    @IBAction func refundTicketAndMessageUser(_ sender: UIButton) {
        
        let refundController = Route.refundViewController
        if let eventID = event?.entityId{
            refundController.eventId = eventID
            refundController.isForMessagingUser = sender.tag == MessageUser ? true : false
        }
        self.navigationController?.show(refundController, sender: nil)
    }
    
    @IBAction func inviteFriend(_ sender: UIButton) {
        let inviteFriendController = Route.inviteVC
        guard let eventId = event?.entityId, let title = event?.eventTitle, let detail = event?.eventDetail, let image = event?.imageUrl else {
            return
        }
        let newProgramVar = CreateEventModel(eventID: eventId, eventTitle: title, eventDetail: detail, imageUrl: image)
        inviteFriendController.eventVar = newProgramVar
        self.present(inviteFriendController, animated: true, completion: nil)
        
    }
    
    @IBAction func emailGroup(_ sender: UIButton) {
        alertEmailGroup()
    }
    
    @IBAction func viteAll(_ sender: UIButton) {
        print("Viteall")
        self.indicator.start()
        self.btnViteAll.isEnabled = false
        
        WebService.request(method: .post, url: kAPIInviteAll + (event?.entityId)!, success: {  [unowned self] (response) -> Void in
            
            guard let response = response.result.value as! [String: AnyObject]? else {
                return
            }
            
            guard let status = response["success"] as? Bool else {
                return
            }
            
            if status {
                self.navigationController?.view.makeToast(message: localizedString(forKey: "VITEALL_NOTIFIED"))
                if UserDefaultsManager().userPremiumStatus! {
                    self.btnViteAll.isEnabled = true
                }
                self.indicator.stop()
            }
            
            }, failure: {
                [unowned self]   (message, error) -> Void in
                showAlert(controller: self, title: "", message: message)
                self.btnViteAll.isEnabled = true
                self.indicator.stop()
        })
    }
}
