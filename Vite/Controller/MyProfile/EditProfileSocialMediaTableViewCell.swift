//
//  EditProfileSocialMediaTableViewCell.swift
//  Vite
//
//  Created by eeposit2 on 1/15/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class EditProfileSocialMediaTableViewCell: UITableViewCell {
    @IBOutlet weak var imgSocialNetwork: UIImageView!
    
    @IBOutlet weak var lblSocialNetwork: UILabel!

    @IBOutlet weak var switchSocialNetwork: UISwitch!
}
