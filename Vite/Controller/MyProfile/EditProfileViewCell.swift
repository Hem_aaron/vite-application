//
//  EditProfileViewCell.swift
//  Vite
//
//  Created by eeposit2 on 2/17/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class EditProfileViewCell: UICollectionViewCell {
    
    @IBOutlet weak var deleteImage: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var addImage: UIImageView!
    
}