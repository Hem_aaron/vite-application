//
//  EditProfileViewController.swift
//  Vite
//
//  Created by eeposit2 on 2/16/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import TwitterKit
import HTagView


protocol GetUserInfoDelegate {
    func getUserInformation(_ userinfo: UserInfo)
}

class EditProfileViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UITextViewDelegate,FBImagePickControllerDelegate, UITableViewDelegate, UITableViewDataSource, HTagViewDelegate, HTagViewDataSource {
    
    @IBOutlet weak var btnBack: UIBarButtonItem!
    @IBOutlet weak var btnDone: UIBarButtonItem!
    @IBOutlet weak var maleCheckBox: UIImageView!
    @IBOutlet weak var femaleCheckBox: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var userDesc: UITextView!
    @IBOutlet weak var userAge: UITextField!
    @IBOutlet weak var userOccupation: UITextField!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var lblAboutMeCount: UILabel!
    @IBOutlet weak var tblNetwork: UITableView!
    @IBOutlet weak var intrestView: HTagView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    var tagLists = [String]()
    var delegate: GetUserInfoDelegate?
    var userInfoForEdit: UserInfo!
    let indicator = ActivityIndicator()
    var gender = " "
    var imageHelper:ImageHelper!
    var layoutSubviewsCalled = false
    var userImageLists       = [[String: AnyObject]]()
    var previousInterests    = [[String: AnyObject]]()
    var interests            = [[String: AnyObject]]()
    var cellWidth:CGFloat!
    var selectedIndexPath: IndexPath!
    
    // For social Network
    var user = UserDefaultsManager()
    var networkArr =  [Network]()
    var networkTypeArr =  [String]()
    var fbIndex =  0
    var twitterIndex = 1
    var instaIndex = 2
    var linkedInIndex = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageHelper                  = ImageHelper(controller: self)
        scrollView.layer.cornerRadius     = 10.0
        collectionView.layer.cornerRadius = 10.0
        populateUserInfo()
        getAllNetworkType()
        getTagList()
        NotificationCenter.default.addObserver(self, selector: #selector(textViewChanged), name: NSNotification.Name.UITextViewTextDidChange, object:self.userDesc)
        NotificationCenter.default.addObserver(self, selector: #selector(EditProfileViewController.instaLogin(_:)), name:NSNotification.Name(rawValue: "InstaTokenNotification"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getUserNetworkStatus()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.indicator.stop()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if layoutSubviewsCalled {
            let collectionViewWidth            = self.collectionView.frame.width
            self.cellWidth                     = (collectionViewWidth - 20) / 3
            self.collectionViewHeight.constant = (self.cellWidth * 2) + 30
            self.collectionView.delegate       = self
            self.collectionView.reloadData()
            return
        }
        layoutSubviewsCalled = true
    }
    
    //MARK: Private Methods
    
    
    func preEditParameter() -> [String: AnyObject]{
        
        let email = userInfoForEdit.email ?? ""
        let gender = userInfoForEdit.gender ?? ""
        let info = userInfoForEdit.info ?? ""
        let occupation = userInfoForEdit.occupation ?? ""
        let fullName = userInfoForEdit.fullName ?? ""
        let userImageLists = userInfoForEdit.userImageList ?? []
        
        let parameters: [String:AnyObject] = [
            "facebookId": UserDefaultsManager().userFBID! as AnyObject,
            "email": email as AnyObject,
            "gender": gender as AnyObject,
            "info": info as AnyObject,
            "occupation": occupation as AnyObject,
            "fullName":fullName as AnyObject,
            "location": [
                "latitude": UserDefaultsManager().userCurrentLatitude ?? "",
                "longitude":UserDefaultsManager().userCurrentLongitude ?? "",
                "givenLocation":""
            ] as AnyObject,
            "userImages" : self.userImageLists as AnyObject,
            "interests" : self.previousInterests as AnyObject
        ]
        return parameters
        
    }
    
    func checkValueChangeInEditParameter() -> Bool {
        let preParameter = preEditParameter()
        let postParameter = UserParameter()
        // check if this method is called when back button is pressed i.e not when edit done button is clicked
        if NSDictionary(dictionary: preParameter).isEqual(to: postParameter){
            return true
        }else{
            return false
        }
    }
    
    @objc func deleteProfileImage(_ sender:UIButton) {
        if userImageLists.count > 1 {
            let i : Int = (sender.layer.value(forKey: "index")) as! Int
            self.userImageLists.remove(at: i)
            self.collectionView!.reloadData()
        }else{
            self.view.makeToast(message: localizedString(forKey: "PHOTO_REQUIRED"))
        }
    }
    
    @objc func textViewChanged(_ notification:Foundation.Notification) {
        let object = notification.object as! UITextView
        if object.text.count >= 151  {
            object.text =  object.text.substring(to: object.text.index(before: object.text.endIndex))
        }
        self.lblAboutMeCount.text = "\(150 - object.text.count)"
    }
    
    func setGenderState(_ gender:String?) {
        if let ge = gender {
            self.gender = "\(ge)"
        }
        if self.gender == "Male"{
            self.maleCheckBox.image   = UIImage(named: "Check")
            self.femaleCheckBox.image = UIImage(named: "Uncheck")
        } else if self.gender == "Female" {
            self.femaleCheckBox.image = UIImage(named: "Check")
            self.maleCheckBox.image   = UIImage(named: "Uncheck")
        } else {
            self.femaleCheckBox.image = UIImage(named: "Uncheck")
            self.maleCheckBox.image   = UIImage(named: "Uncheck")
        }
    }
    
    func populateUserInfo(){
        let userGender = userInfoForEdit.gender
        self.setGenderState(userGender)
        let age = userInfoForEdit.age
        if let a = age {
            self.userAge.text = "\(a)"
        }
        self.userDesc.text = userInfoForEdit.info
        self.lblAboutMeCount.text = "\(150 - self.userDesc.text.count)"
        let email = userInfoForEdit.email
        if let usermail  = email{
            self.userEmail.text = "\(usermail)"
        }
        self.userOccupation.text = userInfoForEdit.occupation
        /// get multiple images of user from server and added to userImageLists
        if let userImage = userInfoForEdit.userImageList{
            for imagelist in userImage
            {
                self.userImageLists.append(imagelist)
            }
            
        }
        
        if let userInterests = userInfoForEdit.interests{
            self.previousInterests = userInterests
            self.interests = userInterests
        }
        
    }
    
    func UserParameter()->[String : AnyObject]{
        self.getFilteredImageLists()
        let parameters: [String:AnyObject] = [
            "facebookId": UserDefaultsManager().userFBID! as AnyObject,
            "email": userEmail.text! as AnyObject,
            "gender": self.gender as AnyObject,
            "info": userDesc.text as AnyObject,
            "occupation": userOccupation.text! as AnyObject,
            "fullName":UserDefaultsManager().userFBFullName! as AnyObject,
            "location": [
                "latitude": UserDefaultsManager().userCurrentLatitude ?? "",
                "longitude":UserDefaultsManager().userCurrentLongitude ?? "",
                "givenLocation":""
            ] as AnyObject,
            "userImages" :self.userImageLists as AnyObject,
            "interests" : self.interests as AnyObject
        ]
        return parameters
    }
    
    func getFilteredImageLists() -> [[String: AnyObject]]{
        var count = 0
        for  (var userImage) in self.userImageLists {
            if let _ = userImage["profileImageUrl"] as? UIImage {
                userImage["profileImageUrl"] = Base64.getCompressedBase64Image(userImage["profileImageUrl"] as! UIImage) as AnyObject
                self.userImageLists[count] = userImage
            }
            count += 1
        }
        return self.userImageLists
    }
    
    func getImage(_ userImage:[String:AnyObject]) -> (imageURL:String?,image:UIImage?){
        if  let imageType = userImage["imageType"] as? String, imageType == "FB"{
            return (userImage["profileImageUrl"] as? String, nil)
        } else {
            if let image = userImage["profileImageUrl"] as? UIImage {
                return (nil, image)
            } else  if let image = (userImage["profileImageUrl"] as? String) {
                return (kBaseURL + image , nil)
            }
        }
        return(nil,nil)
    }
    
    
    func pickPhotosFromFacebook() {
        
        
        FacebookPhotoHelper.getAllFacebookAlbumAndPhoto({ (albumList) in
            let imagePickerController = self.storyboard?.instantiateViewController(withIdentifier: "FBImagePickController") as! FBImagePickController
            let albumListController = self.storyboard?.instantiateViewController(withIdentifier: "FacebookAlbumListController") as! FacebookAlbumListController
            albumListController.albumList = albumList
            albumListController.photoPickerController = imagePickerController
            imagePickerController.delegate = self
            let navController = UINavigationController(rootViewController: albumListController)
            self.present(navController, animated: true, completion: nil)
        }) { (message) in
             self.view.makeToast(message: localizedString(forKey: "NO_FB_IMAGES"))
        }
    }
    
    func showImagePickerSheet() {
        
        let alert = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.purple
        alert.view.backgroundColor = UIColor.white
        
        let facebookPhotos = UIAlertAction(title: "Facebook photos",
                                           style: .default,
                                           handler: {
                                            (action:UIAlertAction) -> Void in
                                            self.pickPhotosFromFacebook()
        })
        
        let gallery = UIAlertAction(title: "Photos from gallery",
                                    style: .default) {
                                        (action: UIAlertAction) -> Void in
                                        self.imageHelper.getImageController(.photoLibrary )
                                        self.imageHelper.imagePicked = {
                                            image in
                                            self.pushCustomImageToSelectedArray(image)
                                        }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel) {
                                            (action: UIAlertAction) -> Void in
        }
        
        alert.addAction(facebookPhotos)
        alert.addAction(gallery)
        alert.addAction(cancelAction)
        
        present(alert,
                              animated: false,
                              completion: nil)
        
    }
    
    func pushFBImageToSelectedArray(_ urlString:String) {
        let userImage:[String:AnyObject] =
            [
                "profileImageUrl": urlString as AnyObject,
                "priority": self.selectedIndexPath.row as AnyObject,
                "imageType": "FB" as AnyObject
        ]
        self.userImageLists.append(userImage)
        collectionView.reloadData()
    }
    
    func pushCustomImageToSelectedArray(_ image:UIImage) {
        let userImage =
            [
                "profileImageUrl": image,
                "priority": self.selectedIndexPath.row,
                ] as [String : Any]
        self.userImageLists.append(userImage as [String : AnyObject])
        collectionView.reloadData()
    }
    
    //MARK: FBImagePickController Delegate
    func selectedImageWithURL(_ urlString:String) {
        self.pushFBImageToSelectedArray(urlString)
    }
    
    //MARK: Picker Delegate
    // returns the number of 'columns' to display.
    func numberOfComponentsInPickerView(_ pickerView: UIPickerView!) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    
    //MARK: Service call
    func serviceCallToUpdateUserInfo(){
        self.indicator.start()
        self.btnBack.isEnabled = false
        self.btnDone.isEnabled = false
        WebService.request(method: .post, url: kApiUpdateUserInfo, parameter: UserParameter(), header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let params = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            
            if success {
                if let userDetail = params["users"] as? [String:AnyObject] {
                       self.userInfoForEdit.imageURL = userDetail["profileImageUrl"] as? String
                     self.delegate?.getUserInformation(self.userInfoForEdit)
                }
                self.indicator.stop()
                self.btnDone.isEnabled = true
                self.btnBack.isEnabled = true
                self.navigationController?.popViewController(animated: true)
            }
        }) { (message, err) in
            self.view.makeToast(message: localizedString(forKey: "UPDATE_ERROR"))
            self.btnDone.isEnabled = true
            self.indicator.stop()
        }
    }
    
}

extension EditProfileViewController  {
    //MARK: Collection View Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! EditProfileViewCell
        cell.layer.cornerRadius = 10.0
        //check indexpath and profile images count
        if indexPath.row < self.userImageLists.count {
            var userImage = self.userImageLists[indexPath.row]
            userImage["priority"] = indexPath.row as AnyObject
            
            self.userImageLists[indexPath.row] = userImage
            let image = self.getImage(self.userImageLists[indexPath.row])
            if let url = image.imageURL {
                cell.profileImage.sd_setImage(with: URL(string: url ))
            } else {
                cell.profileImage.image = image.image
            }
            
            cell.addImage.isHidden    = true
            cell.deleteImage.isHidden = false
            
        } else {
            cell.profileImage.image = nil
            cell.addImage.isHidden    = false
            cell.deleteImage.isHidden = true
        }
        
        cell.deleteImage?.layer.setValue(indexPath.row, forKey: "index")
        cell.deleteImage?.addTarget(self, action: #selector(self.deleteProfileImage(_:)), for: UIControlEvents.touchUpInside)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! EditProfileViewCell
        if cell.profileImage.image == nil {
            self.selectedIndexPath = indexPath
            self.showImagePickerSheet()
        }
    }
    
    //Use for size
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.cellWidth, height: self.cellWidth)
    }
}

extension EditProfileViewController {
    //MARK: Actions
    @IBAction func backButton(_ sender: AnyObject) {
        self.navigationController!.popToRootViewController(animated: true)
    }
    
    @IBAction func checkMale(_ sender: AnyObject) {
        maleCheckBox.image   = UIImage(named: "Check")
        femaleCheckBox.image = UIImage(named: "Uncheck")
        gender               = "Male"
    }
    
    @IBAction func checkFemale(_ sender: AnyObject) {
        femaleCheckBox.image = UIImage(named: "Check")
        maleCheckBox.image   = UIImage(named: "Uncheck")
        gender               = "Female"
    }
    
    @IBAction func updateUser(_ sender: AnyObject) {
        if gender == " " {
            self.view.makeToast(message: localizedString(forKey: "GENDER_BLANK"))
        }else{
            if userEmail.text!.isEmpty || isValidEmail(userEmail.text!) {
                if !checkValueChangeInEditParameter(){
                    serviceCallToUpdateUserInfo()
                }else{
                    self.navigationController?.popViewController(animated: true)
                }
            } else {
                self.view.makeToast(message: localizedString(forKey: "PROPER_EMAIL_REQUIRED"))
            }
        }
    }
    
    
    //MARK: TABLE VIEW DELEGATES
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return networkArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "socialMediaCell") as! EditProfileSocialMediaTableViewCell
        let network = networkArr[indexPath.row]
        cell.imgSocialNetwork.image = UIImage(named: network.networkType!)
        cell.lblSocialNetwork.text = " Show in \(network.networkType!)"
        cell.switchSocialNetwork.tag = indexPath.row
        cell.switchSocialNetwork.isEnabled = true
        switch indexPath.row{
        case fbIndex:
            cell.switchSocialNetwork.isOn = networkTypeArr.contains("Facebook") ? true : false
        case twitterIndex:
            cell.switchSocialNetwork.isOn = networkTypeArr.contains("Twitter") ? true : false
        case instaIndex:
            cell.switchSocialNetwork.isOn = networkTypeArr.contains("Instagram") ? true : false
        case linkedInIndex:
            cell.switchSocialNetwork.isOn = networkTypeArr.contains("LinkedIn") ? true : false
        default:
            cell.switchSocialNetwork.isOn = false
        }
        cell.switchSocialNetwork.addTarget(self, action: #selector(EditProfileViewController.stateChanged(_:)), for: UIControlEvents.valueChanged)
        return cell
    }
    
    
    
    //MARK: Social Network Api's
    
    func getAllNetworkType()  {
        VSocialNetworkWebService.getAllNetworkType({ (networkList) in
            if let list  = networkList{
                self.networkArr = list
            }
            self.tblNetwork.isScrollEnabled = false
            self.getUserNetworkStatus()
            self.tblNetwork.reloadData()
        }) { (message) in
            showAlert(controller: self, title: "", message: message)
        }
    }
    
    func getUserNetworkStatus() {
        guard let fbId = UserDefaultsManager().userFBID else{
            self.view.isUserInteractionEnabled = true
            return
        }

        VSocialNetworkWebService.getUserNetworkStatus(fbId, successBlock: { (networkStatusList) in
            if let list = networkStatusList{
                self.getStatus(list)
            }
            self.view.isUserInteractionEnabled = true
            self.tblNetwork.reloadData()
        }) { (message) in
            showAlert(controller: self, title: "", message: message)
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func getStatus( _ networkArr : [NetworkStatus]) -> [String] {
        networkTypeArr.removeAll()
        for network in networkArr{
            if let networkType =  network.networkType, network.networkConnectStatus == true {
                networkTypeArr.append(networkType)
            }
        }
        return networkTypeArr
    }
    
    //MARK: UISWITCH SWITCH CHANGE
    @objc func stateChanged(_ switchState: UISwitch) {
        // user interaction is disabled when user connects or disconnects to any social media
        self.view.isUserInteractionEnabled = false
        switch switchState.tag {
        case 0:
            if switchState.isOn {
                connectWithSocial(UserDefaultsManager().userFBID!, networkUserName: UserDefaultsManager().userFBFirstName!, networkName: "Facebook", networkProfileUrl: "https://www.facebook.com/\(UserDefaultsManager().userFBID!)")
            } else {
                disconnectWithSocial("Facebook")
            }
        case 1:
            if switchState.isOn {
                loginWithTwitter()
            } else {
                TwitterApiService().clearTwitterSession()
                disconnectWithSocial("Twitter")
            }
        case 2:
            if switchState.isOn {
                self.performSegue(withIdentifier: "InstaSegue", sender: nil)
            } else {
                disconnectWithSocial("Instagram")
            }
        case 3:
            if switchState.isOn {
                if UIApplication.shared.canOpenURL(URL(string: "linkedin://")!) {
                    initiateLinkedinLogin()
                } else {
                    self.view.isUserInteractionEnabled = true
                    showAlert(controller: self, title: localizedString(forKey: "ALERT"), message: localizedString(forKey: "INSTALL_LINKEDIN_APP"))
                    //doOAuthLinkedin()
                }
            }
            else{
                disconnectWithSocial("LinkedIn")
            }
        default:
            if switchState.isOn {
            } else {
            }
        }
        self.tblNetwork.reloadData()
        
    }
    
    //MARK:- LINKED IN LOGIN
    func initiateLinkedinLogin() {
        self.indicator.start()
        if MGLinkedinSDKWrapper.isSessionValid() {
            self.getProfileUrlLinkedIn()
        } else {
            MGLinkedinSDKWrapper.initSession({ (state) in
                self.getProfileUrlLinkedIn()
                }, error: {
                    error in
                    self.indicator.stop()
            })
        }
    }
    
    func doOAuthLinkedin(){
        self.indicator.start()
        self.view.isUserInteractionEnabled = true
        LinkedInOAuthWebService().doOAuthLinkedin({ (credentials) in
            self.tblNetwork.reloadData()
            self.getProfileLinkedin()
        }) { (error) in
            showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: localizedString(forKey: "TRY_AGAIN"))
            self.view.isUserInteractionEnabled = true
            self.indicator.stop()
        }
    }
    
    func getProfileUrlLinkedIn() {
        MGLinkedinUserProfileService().getUserProfileUrl({ (userResponse) in
            if let linkedInId =  userResponse["id"] as? String{
                self.user.linkedInId = linkedInId
            }
            if let urlData = userResponse["siteStandardProfileRequest"] as? [String:AnyObject] {
                if let url = urlData["url"] as? String {
                    self.user.linkedInUserName = url
                }
            }
            self.connectWithSocial(self.user.linkedInId!,networkUserName: self.user.linkedInUserName!, networkName: "LinkedIn", networkProfileUrl:self.user.linkedInUserName!)
            
        }) { (error) in
            self.view.isUserInteractionEnabled = true
            showAlert(controller: self, title: localizedString(forKey: "ERROR"), message:  localizedString(forKey: "TRY_AGAIN"))
            self.indicator.stop()
        }
    }
    
    func getProfileLinkedin() {
        LinkedInOAuthWebService().getLinkedInProfile({ (userResponse) in
            if let linkedInId =  userResponse["id"] as? String{
                self.user.linkedInId = linkedInId
            }
            if let profileRequest = userResponse["siteStandardProfileRequest"] as? [String: String]{
                if let url = profileRequest["url"]{
                    self.user.linkedInUserName = url
                }
            }
            self.connectWithSocial(self.user.linkedInId!,networkUserName: self.user.linkedInUserName! ,networkName: "LinkedIn", networkProfileUrl :self.user.linkedInUserName!)
        }) { (error) in
            self.view.isUserInteractionEnabled = true
            showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: localizedString(forKey: "TRY_AGAIN"))
            self.indicator.stop()
            
        }
    }
    
    //MARK: TWITTER LOGIN
    func loginWithTwitter(){
        self.indicator.start()
        Twitter.sharedInstance().logIn { (session, error) in
            if (session != nil) {
                self.indicator.stop()
                self.getTwitterProfileData()
            } else {
                self.indicator.stop()
            }
        }
    }
    
    func getTwitterProfileData(){
        TwitterApiService().getTwitterprofile({ (userResponse) in
            self.user.twitterUserName = userResponse["name"] as? String
            self.user.twitterID = userResponse["id_str"] as? String
            self.user.twitterScreenName = userResponse["screen_name"] as? String
            if let twitterId =   self.user.twitterID, let twitterScreenName = self.user.twitterScreenName{
                let userName = twitterScreenName.replacingOccurrences(of: " ", with: "")
                self.connectWithSocial(twitterId,networkUserName: userName,networkName: "Twitter" ,networkProfileUrl: "https://twitter.com/\(userName)" )
            }
            
        }) { (error) in
            self.indicator.stop()
            self.view.isUserInteractionEnabled = true
            showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: error as! String)
        }
    }
    
    //MARK: INSTAGRAM LOGIN
    
    @objc func instaLogin(_ notification: Foundation.Notification){
        let defaults = UserDefaults.standard
        user.instaUserName = defaults.string(forKey: "kInstaUserName")!
        user.instaId = defaults.string(forKey: "InstaId")!
        if let userName =   user.instaUserName,let id =   user.instaId {
            self.connectWithSocial(id,networkUserName: userName,networkName: "Instagram",networkProfileUrl:  "https://www.instagram.com/\(userName)")
        }
    }
    
    
    //MARK: CONNECT WITH SOCIAL - SEND FRIENDS TO SERVER
    func connectWithSocial(_ networkID: String,networkUserName:String, networkName: String, networkProfileUrl: String){
        self.indicator.start()
        let parameter :[String: AnyObject] = [
            "networkId":networkID as AnyObject,
            "networkType":networkName as AnyObject,
            "networkUserName":networkUserName as AnyObject,
            "networkProfileUrl": networkProfileUrl as AnyObject
        ]
        VSocialNetworkWebService.connectWithSocial(parameter, successBlock: { (message) in
            self.getUserNetworkStatus()
            self.indicator.stop()
        }) { (message) in
            if let msg = message{
                self.view.isUserInteractionEnabled = true
                self.indicator.stop()
                showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: msg)
            }
        }
    }
    
    //MARK: DISCONNECT WITH SOCIAL - SEND FRIENDS TO SERVER
    func disconnectWithSocial(_ networkName: String){
        self.indicator.start()
        let parameter :[String: AnyObject] = [
            "networkType":networkName as AnyObject,
            ]
        
        VSocialNetworkWebService.disconnectWithSocial(parameter, successBlock: { (message) in
            self.emptySocialMediaUserDefaults(networkName)
            self.getUserNetworkStatus()
            self.indicator.stop()
        }) { (message) in
            if let msg = message{
                self.indicator.stop()
                self.view.isUserInteractionEnabled = true
                showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: msg)
            }
        }
    }
    
    
    func emptySocialMediaUserDefaults(_ network: String){
        switch network {
        case "Twitter":
            user.twitterUserName = ""
            user.twitterID = ""
        case "Instagram":
            break
        case "LinkedIn":
            user.linkedInId = ""
            user.linkedInUserName = ""
        default:
            break
        }
    }
    
    // MARK: - TagViewDataSource
    func numberOfTags( _ tagView: HTagView) -> Int {
        return tagLists.count
        
    }
    
    func tagView(_ tagView: HTagView, titleOfTagAtIndex index: Int) -> String {
        return  tagLists[index]
    }
    
    func tagView(_ tagView: HTagView, tagTypeAtIndex index: Int) -> HTagType {
        return .select
    }
    
    // MARK: - TagViewDelegate
    func tagView(_ tagView: HTagView, tagSelectionDidChange selectedIndices: [Int]) {
        interests.removeAll()
        for select in selectedIndices{
            interests.append([ "filterName" : tagLists[select] as AnyObject])
        }
    }
    
    // get list of the interest tag from the server
    
    func getTagList(){
        VProfileWebService.getUserInterestList(successBlock: { (interestList) in
            self.tagLists = interestList
            self.intrestView.delegate = self
            self.intrestView.dataSource = self
            self.populateInterests(self.userInfoForEdit.interests!)
            }) { (message) in
                showAlert(controller: self, title: "", message: message)
        }
    }
    

    func populateInterests(_ interests: [[String: AnyObject]]){
        for i in interests {
            for tagIndex in 0 ..< tagLists.count {
                if String(describing: i["filterName"]!) == tagLists[tagIndex] {
                    //intrestView.reloadData()
                   intrestView.selectTagAtIndex(tagIndex)
                }
            }
        }
    }
}






