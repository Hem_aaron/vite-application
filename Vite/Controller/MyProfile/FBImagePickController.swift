//
//  FBImagePickController.swift
//  Vite
//
//  Created by Hem Poudyal on 3/29/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
protocol FBImagePickControllerDelegate {
    func selectedImageWithURL(_ urlString:String)
}

class FBImagePickController: WrapperController,UICollectionViewDelegate,UICollectionViewDataSource {
    var fbImageList:[String]!
    var delegate:FBImagePickControllerDelegate?
    
    @IBOutlet weak var fbImageCollectionView: UICollectionView!
    
    override func viewDidAppear(_ animated: Bool) {
        self.fbImageCollectionView.reloadData()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.fbImageList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FBImageCell", for: indexPath) as! FBImageCell
        cell.FBImage.sd_setImage(with: URL(string: self.fbImageList[indexPath.row]))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.navigationController?.dismiss(animated: true, completion: nil)
        self.delegate?.selectedImageWithURL(self.fbImageList[indexPath.row])
    }
    @IBAction func back(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
        //self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //Use for size
    func collectionView(_ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
            // 4 is space between cell 
            // 4 is number of cell to be contained in a row.
            let width = (UIScreen.main.bounds.size.width - 12 - 32) / 4
            return CGSize(width: width, height: width)
    }
}
