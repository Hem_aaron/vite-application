//
//  FacebookAlbumCell.swift
//  Vite
//
//  Created by Hem Poudyal on 3/29/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class FacebookAlbumCell: UITableViewCell {
    @IBOutlet weak var albumCover: UIImageView!
    @IBOutlet weak var lblAlbumTitle: UILabel!
    @IBOutlet weak var lblAlbumPhotosCount: UILabel!
    @IBOutlet weak var rightArrowImageView: UIImageView!
}
