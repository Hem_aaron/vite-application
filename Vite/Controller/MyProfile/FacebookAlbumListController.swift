//
//  FacebookAlbumList.swift
//  Vite
//
//  Created by Hem Poudyal on 3/29/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class FacebookAlbumListController: WrapperController {
    var albumList:[FacebookAlbum]!
    var photoPickerController:FBImagePickController!
    @IBOutlet weak var tableView: UITableView!
    
}

extension FacebookAlbumListController:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.albumList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell                      = tableView.dequeueReusableCell(withIdentifier: "FacebookAlbumCell") as! FacebookAlbumCell
        let album                     = self.albumList[indexPath.row];
        cell.lblAlbumPhotosCount.text = album.count! + " photos"
        cell.lblAlbumTitle.text       = album.name
        if album.coverPhoto != nil{
            cell.albumCover.sd_setImage(with: URL(string: album.coverPhoto!)!)
        }
        else{
            let size = CGSize(width: 100,height: 100)
            cell.albumCover.image = getImageWithColor(color: UIColor.gray, size: size)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let album = self.albumList[indexPath.row]
        self.view.isUserInteractionEnabled = false
        
        FacebookPhotoHelper.getAllPhotosFromAlbum(album.id!, successBlock: {
            (imageList) -> Void in
            
            //controller.delegate = self
            self.photoPickerController.fbImageList = imageList
            self.photoPickerController.title = album.name
            self.navigationController?.show(self.photoPickerController, sender: nil)
            self.view.isUserInteractionEnabled = true
            }) { (message) -> Void in
                print("Error pulling photos from album")
                self.view.isUserInteractionEnabled = true
        }
        
    }
    @IBAction func back(_ sender: AnyObject) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}
