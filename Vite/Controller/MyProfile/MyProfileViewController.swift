//
//  MyProfileViewController.swift
//  Vite
//
//  Created by EeposIT_X on 2/15/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import Alamofire
import HTagView
import Cosmos
import MapKit
import Hero
class MyProfileViewController: WrapperController, GetUserInfoDelegate, UIScrollViewDelegate, UICollectionViewDelegate , UICollectionViewDataSource, HTagViewDataSource{
    
    // outlets used
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblMutualFriendsCount: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnBack: UIBarButtonItem!
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var lblConnections: UILabel!
    @IBOutlet weak var btnEditProfile: UIBarButtonItem!
    @IBOutlet weak var userDesc: UILabel!
    @IBOutlet weak var viteCount: UILabel!
    @IBOutlet weak var viteImgView: UIImageView!
    @IBOutlet weak var connectionImgView: UIImageView!
    @IBOutlet weak var profileScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var userGenderAge: UILabel!
    @IBOutlet weak var oldView: UIView!
    @IBOutlet weak var intrestView: HTagView!
    @IBOutlet weak var ratingView: CosmosView!

  
    // Constraints outlets
    
    @IBOutlet weak var oldViewHeightContant: NSLayoutConstraint!
    @IBOutlet weak var oldViewTopSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var mutualFriendsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var peopleAttendingViewTopSpaceConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ratingViewHeight: NSLayoutConstraint!
    
    // Constraint Outlets for social media connections
    
    @IBOutlet weak var socialMediaViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnFbWidth: NSLayoutConstraint!
    @IBOutlet weak var btnTwitterWidth: NSLayoutConstraint!
    @IBOutlet weak var btnInstagramWidth: NSLayoutConstraint!
    @IBOutlet weak var btnLinkedInWidth: NSLayoutConstraint!
    @IBOutlet weak var horizontalSpaceBtnFbAndTwit: NSLayoutConstraint!
    @IBOutlet weak var horizontalSpaceBtnTwitAndInsta: NSLayoutConstraint!
    @IBOutlet weak var horrizontalSpaceBtnInstaAndLinkedIn: NSLayoutConstraint!
    @IBOutlet weak var intrestViewHeightConstraint: NSLayoutConstraint!
    
    
    // SocialMediaView  outlets
    @IBOutlet weak var btnFb: SocialMediaCustomButton!
    @IBOutlet weak var btnTwitter: SocialMediaCustomButton!
    @IBOutlet weak var btnInsta: SocialMediaCustomButton!
    @IBOutlet weak var btnLinkedIn: SocialMediaCustomButton!
    @IBOutlet weak var socialMediaViewTopSpaceConstraint: NSLayoutConstraint!
    
    
    
    
    @IBOutlet weak var celebVerifiedImgView: UIImageView!
    @IBOutlet weak var userDetailView: UIView!
    
    // button Outlets
    @IBOutlet weak var btnMsgUser: UIButton!
    @IBOutlet weak var btnAddToMyVIps: UIButton!
    @IBOutlet weak var btnBlockUser: UIButton!
    @IBOutlet weak var lblNoMutualFriend: UILabel!
    @IBOutlet weak var btnAddMeToYourVip: UIButton!
    
    // switch Outlets
    // premium Outlets
    @IBOutlet weak var darkModeHideAgeStackView: UIStackView!
    @IBOutlet weak var goDarkModeView: UIView!
    @IBOutlet weak var hideAgeView: UIView!
    @IBOutlet weak var darkModeSwitch: UISwitch!
    @IBOutlet weak var hideAgeSwitch: UISwitch!
    @IBOutlet weak var premiumView: UIView!
    @IBOutlet weak var btnGoPremium: UIButton!
    @IBOutlet weak var imgTickPremium: UIImageView!
    @IBOutlet weak var btnPassport: UIButton!
    @IBOutlet weak var btnViteAll: UIButton!
    @IBOutlet weak var btnViteNow: UIButton!
    @IBOutlet weak var passportView: UIView!
    @IBOutlet weak var viteNowView: UIView!
    @IBOutlet weak var viteAllView: UIView!
    
    @IBOutlet weak var lblViteAllMonth: UILabel!
    @IBOutlet weak var lblViteNowMonth: UILabel!
    
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var premiumViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var goDarkModeHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var btnMgUserTopconstantWithInterView: NSLayoutConstraint!
    
    //rate an comment
    @IBOutlet weak var btnViewAllComment: UIButton!
    @IBOutlet weak var commentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblRateAndComment: UITableView!
    @IBOutlet weak var tblCommentHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblRateComment: UITableView!
    @IBOutlet weak var lblCommentCount: UILabel!
    
    // variables used
    var connectionStatusArr = [Bool]()
    var fbId: String!
    var isForViewingOthersProfile: Bool =  false
    var isFromChatVc: Bool =  false
    var isFromVip: Bool = false
    var mutualFriendsList = [VUser]()
    var businessDetail: BusinessAccountUserModel?
    var isForViewingBusinessProfile = Bool()
    let indicator = ActivityIndicator()
    var userInfo: UserInfo!
    var isMyVip = false
    var amIVip = false
    var addToMyVipRequestSent = false
    var addMeToYourVipRequestSent = false
    var isFromPeopleList = false
    var isFromAttendeeRequest = false
    var tagLists = [String]()
    let defaults = UserDefaults.standard
    let cursor = VCursor()
    var rateAndCommentArr = [VRateComment]()
    
     var locationManager:LocationManager? = LocationManager()
     var eventLocation:CLLocationCoordinate2D?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setHeroId()
        self.ratingView.isUserInteractionEnabled = false
        self.ratingView.isHidden = true
        self.ratingView.settings.fillMode = .half
        resetData()
        btnEditProfile.isEnabled = false
        viewProfile()
        btnGoPremium.layer.cornerRadius = 10.0
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.getUserProfileAfterPremiumActivated), name: NSNotification.Name(rawValue: "premiumNotification"), object: nil)
    }
    
    @objc func getUserProfileAfterPremiumActivated() {
        self.btnEditProfile.isEnabled = false
        ServiceCall(URL: kAPIViewProfile!)
        self.view.makeToast(message: localizedString(forKey: "VITEPLUS_USER"))
    }
   
    func maintainTblViewHeight() {
        self.tblCommentHeightConstraint.constant = self.tblRateAndComment.contentSize.height
        self.commentViewHeightConstraint.constant = self.tblRateAndComment.contentSize.height + 85 // 38 is the height of comment(200) lbl and the top space of the label plus the top space of the table view  and the viewallcomment view height
    }
    
    func setHeroId () {
        if self.fbId != nil {
        self.profileImage.hero.id = self.fbId
        self.fullName.hero.id = "Name" + self.fbId
        self.userGenderAge.hero.id = "Gender" + self.fbId
        self.userDesc.hero.id = "Desc" + self.fbId
        }
    }
    func viewProfile(){
        if isForViewingOthersProfile {
            self.goDarkModeView.isHidden = true
            self.premiumView.isHidden = true
            self.hideAgeView.isHidden = true
            self.btnEditProfile.image = nil
            self.passportView.isHidden = true
            self.viteAllView.isHidden = true
            self.viteNowView.isHidden = true
            // set height value of the views to their respective heights.
            hideViews(btnHeightConstant: 50, mutualFriendViewConstant: 148, hiddenStatus:false)
            socialMediaViewHeightConstraint.constant = 100
            self.peopleAttendingViewTopSpaceConstraint.constant = 6
            let url = KAPIViewAttendeeProfile + fbId
            ServiceCall(URL: url)
            self.btnMgUserTopconstantWithInterView.constant = 8
        }else{
            // set height value of the views to 0
            hideViews(btnHeightConstant: 0, mutualFriendViewConstant: 0 , hiddenStatus: true)
            socialMediaViewHeightConstraint.constant = 0
            self.peopleAttendingViewTopSpaceConstraint.constant = 0
            ServiceCall(URL: kAPIViewProfile!)
            self.btnMgUserTopconstantWithInterView.constant = 0
            self.navigationItem.title = "My Profile"
        }
    }
    
    
    //MARK: Private Methods
    
    func hideViews(btnHeightConstant:CGFloat, mutualFriendViewConstant: CGFloat,hiddenStatus: Bool) {
        self.mutualFriendsViewHeightConstraint.constant = mutualFriendViewConstant
        self.btnMsgUser.isHidden = hiddenStatus
        self.btnBlockUser.isHidden = hiddenStatus
        self.btnAddToMyVIps.isHidden = hiddenStatus
        self.btnAddMeToYourVip.isHidden = hiddenStatus
    }
    
    
    
    func addMessageBarButton() {
        let btnName = UIButton()
        btnName.setImage(UIImage(named: "chat_home"), for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
      //  btnName.addTarget(self, action: #selector(MyProfileViewController.message), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    // func to call mutual friends
    func getMutualfriendsList () {
        VProfileWebService.getMutualFriends(friendFbId: fbId, successBlock: { (users) in
            self.mutualFriendsList = users
            self.lblMutualFriendsCount.text = "\(self.mutualFriendsList.count)"
            if self.mutualFriendsList.count == 0 {
                self.lblNoMutualFriend.isHidden = false
                self.lblMutualFriendsCount.isHidden = true
                self.collectionView.isHidden = true
            }else{
                self.lblNoMutualFriend.isHidden = true
                self.lblMutualFriendsCount.isHidden = false
                self.collectionView.isHidden = false
                self.collectionView.reloadData()
            }
        }) { (message) in
            showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: message)
        }
    }
    
    
    func message() {
        let chatBoard = UIStoryboard(name: "Chat", bundle: nil)
        let chatController      = chatBoard.instantiateViewController(withIdentifier: "ChatMainViewController") as! ChatMainViewController
        let sender             = ChatUser()
        sender.fbID            =  UserDefaultsManager().isBusinessAccountActivated ? UserDefaultsManager().userBusinessAccountID : UserDefaultsManager().userFBID
        sender.name            =  UserDefaultsManager().isBusinessAccountActivated ? UserDefaultsManager().userBusinessFullName : UserDefaultsManager().userFBFullName
        sender.profileImageUrl = UserDefaultsManager().isBusinessAccountActivated ? UserDefaultsManager().userBusinessProfileImageUrl :"http://graph.facebook.com/\(UserDefaultsManager().userFBID!)/picture?type=large"
        
        let receiver             = ChatUser()
        receiver.fbID            = String(describing: self.userInfo.fbId!)
        receiver.name            = self.userInfo.fullName!
        receiver.profileImageUrl = self.userInfo.imageURL!
        
        chatController.receiver = receiver
        chatController.sender   = sender
        self.navigationController?.show(chatController, sender: nil)
    }
    
    
    func swipeProfileImage(userImageListDetail: [[String: AnyObject]]){
        for view in self.profileScrollView.subviews {
            view.removeFromSuperview()
        }
        loadPageControl()
        if userImageListDetail.count == 0{
            profileImage.isHidden = false
            profileScrollView.isHidden = true
            pageControl.isHidden = true
        } else{
            pageControl.isHidden = false
            for i in 0  ..< userImageListDetail.count  {
                let imageView: UIImageView = UIImageView()
                imageView.sd_setShowActivityIndicatorView(true)
                if String(describing: userImageListDetail[i]["imageType"]!) == "CUSTOM" {
                    imageView.sd_setImage(with: NSURL(string: kBaseURL + String(describing: userImageListDetail[i]["profileImageUrl"]!))! as URL)
                    
                } else {
                    imageView.sd_setImage(with: NSURL(string: String(describing: userImageListDetail[i]["profileImageUrl"]!))! as URL)
                }
                
                imageView.contentMode = .scaleAspectFill
                imageView.clipsToBounds = true
              
                imageView.frame = CGRect(x: CGFloat(i) * self.profileScrollView.frame.width, y: 0, width: self.profileScrollView.frame.width, height: self.profileScrollView.frame.height)
                self.profileScrollView.addSubview(imageView)
            }
            self.profileScrollView.contentSize = CGSize(width: self.profileScrollView.frame.width * CGFloat(userImageListDetail.count) , height: self.profileScrollView.frame.height)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        loadPageControl()
    }
    
    func loadPageControl(){
        if let imageList = userInfo.userImageList{
            pageControl.numberOfPages = imageList.count
            let pageWidth =  self.profileScrollView.frame.size.width
            let page = Int(floor( self.profileScrollView.contentOffset.x / (pageWidth)))
            pageControl.currentPage = page
        }
        
    }
    
    
    //hide label and views for attendee list
    func hideViewsAndLabels(value: Bool){
        viteImgView.isHidden = value
        connectionImgView.isHidden = value
        lblConnections.isHidden = value
        viteCount.isHidden = value
    }
    
    //populate user info when the information is updated
    func getUserInformation(_ userinfo: UserInfo){
        var ud              = UserDefaultsManager()
        ud.userFBProfileURL = userinfo.imageURL
        self.btnEditProfile.isEnabled = false
        ServiceCall(URL: kAPIViewProfile!)
        self.view.makeToast(message: localizedString(forKey: "USER_PROFILE_UPDATED"))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? EditProfileViewController {
            destinationViewController.delegate = self
            destinationViewController.userInfoForEdit = self.userInfo
        }
    }
    
    func resetData(){
        self.viteCount.text = nil
        self.lblConnections.text = nil
        self.userDesc.text = nil
        self.userGenderAge.text = nil
        self.fullName.text = nil
        self.lblMutualFriendsCount.text = nil
    }
    
    // populate user information when the page is loaded for the first time
    
    func populateUserinfo() {
        self.fullName.text = self.userInfo.fullName
        if let imgUrl = userInfo.imageURL{
            self.profileImage.sd_setImage(with: NSURL(string:imgUrl)! as URL)
        }
        if let userDesc =  self.userInfo.info{
            self.userDesc.text = userDesc
        }
        if userInfo.info == ""{
            self.userDesc.text = userInfo.occupation
        }
        if let userGender = userInfo.gender , let userAge = userInfo.age {
            self.userGenderAge.text = userGender + ", \(userAge)"
        } else if let userGender = userInfo.gender {
            self.userGenderAge.text = userGender
        }
        
        if let countVite = self.userInfo.countVite{
            self.viteCount.text = String(countVite) + " vites"
        }
        if let imageLists = userInfo.userImageList{
            swipeProfileImage(userImageListDetail: imageLists)
        }
        if let interestList = self.userInfo.interests{
            self.getInterests(interests: interestList)
        }
        
        if let rating = userInfo.rating {
            if rating == 0 {
                self.ratingViewHeight.constant = 0
                self.ratingView.isHidden = true
            } else {
                self.ratingView.rating = rating
                self.ratingView.isHidden = false
                self.ratingViewHeight.constant = 24
            }
        }
        self.indicator.stop()
    }
    
    //MARK: Service Call
    
    // get user profile
    func ServiceCall(URL: String){
        //taglist is removed everytime when service is called as it is recieved from server everytime
        self.tagLists.removeAll()
        self.indicator.start()
        self.view.isUserInteractionEnabled = false
        VProfileWebService.getUserProfile(url: URL, successBlock: { (users) in
            self.userInfo = users
            let ud = UserDefaultsManager()
            ud.userPremiumStatus  = users.premiumAccount!
            self.populateUserinfo()
            if let isUserVip = self.userInfo.myVip{
                self.isMyVip = isUserVip
            }
            if let amIUsersVip = self.userInfo.iAmVip{
                self.amIVip = amIUsersVip
            }
            if let requestStatus = self.userInfo.addToMyVipRequestSent{
                self.addToMyVipRequestSent = requestStatus
            }
            if let requestStatus = self.userInfo.addMeToYourVipRequestSent{
                self.addMeToYourVipRequestSent = requestStatus
            }
            if self.userInfo.premiumAccount == true {
                self.imgTickPremium.isHidden = false
                self.btnGoPremium.isEnabled = false
                self.btnGoPremium.setTitle("Vite Plus", for:  .normal)
                self.btnPassport.isHidden = true
                self.btnViteAll.isHidden = true
                self.btnViteNow.isHidden = true
                self.lblLocation.isHidden = false
                if let location =  UserDefaults.standard.string(forKey: "userpremiumLocation") {
                    self.lblLocation.text = location
                }
                self.btnLocation.isHidden = false
                self.lblViteAllMonth.isHidden = false
                self.lblViteNowMonth.isHidden = false
                if let viteAll = users.viteAllCount {
                    self.lblViteAllMonth.text = "\(viteAll)/month"
                }
                if let viteNow =  users.viteNowCount {
                 self.lblViteNowMonth.text = "\(viteNow)/month"
                }
                self.premiumViewHeightConstant.constant = 65
                self.btnGoPremium.titleEdgeInsets.left = -20
            } else {
        
                self.btnGoPremium.isEnabled = true
                self.imgTickPremium.isHidden = true
                self.premiumViewHeightConstant.constant = 140
                self.btnGoPremium.setTitle("Get Plus", for:  .normal)
                self.btnPassport.isHidden = false
                self.btnViteAll.isHidden = false
                self.btnViteNow.isHidden = false
                self.lblLocation.isHidden = true
                self.btnLocation.isHidden = true
                self.lblViteAllMonth.isHidden = true
                self.lblViteNowMonth.isHidden = true
                self.btnGoPremium.titleEdgeInsets.left = 0
            }

            if self.userInfo.darkMode == true {
                self.darkModeSwitch.isOn = true
            } else {
                self.darkModeSwitch.setOn(false, animated: true)
            }
            
            if self.userInfo.hideAge == true {
                self.hideAgeSwitch.isOn = true
            } else {
                self.hideAgeSwitch.setOn(false, animated: true)
            }
            self.btnEditProfile.isEnabled = true
            if self.isForViewingOthersProfile{
                self.btnEditProfile.isEnabled = false
                self.navigationItem.title = self.userInfo.fullName!
                self.getMutualfriendsList()
                self.getUserNetworkDetail(userNetworks: self.userInfo.userNetworks)
                if let isUserVerifiedCelebrity = self.userInfo.verificationStatus{
                    self.checkIfUserIsCelebrity(isUserVerifiedCelebrity: isUserVerifiedCelebrity)
                }
                if self.isMyVip ||  self.addToMyVipRequestSent {
                    self.btnAddToMyVIps.isHidden = true
                  }else{
                    self.btnAddToMyVIps.isHidden = false
                }
                if self.amIVip ||  self.addMeToYourVipRequestSent {
                    self.btnAddMeToYourVip.isHidden = true
                }else{
                    self.btnAddMeToYourVip.isHidden = false
                 }
                
            }
            self.fetchComments(shouldReset: true)
            self.view.isUserInteractionEnabled = true
        }) { (message) in
            showAlert(controller: self, title: "", message: message)
            self.indicator.stop()
            self.view.isUserInteractionEnabled = true
        }
        
        
    }
    // To show PremiumWalkthroughViewController
    func showPremiumWalkthroughViewController() {
        let controller = Route.premiumViewController
        controller.isFromProfile = true
        self.present(controller, animated: true, completion: nil)
    }
    
    
    //MARK: CollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  self.mutualFriendsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let mutualFriend = self.mutualFriendsList[indexPath.row]
        let controller = Route.profileViewController
        controller.fbId = String(describing: mutualFriend.uid!)
        controller.isForViewingOthersProfile = true
        self.navigationController?.show(controller, sender: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = self.mutualFriendsList[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mutualFriendsCell", for: indexPath) as! MutualFriendCollectionViewCell
        if let  profileImgUrl = data.profileImageUrl?.convertIntoViteURL(), let fbId = data.uid {
            if isValidImage(url: profileImgUrl){
                cell.profileImg.sd_setImage(with: NSURL(string:(profileImgUrl))! as URL)
            } else {
                let profileUrl = "http://graph.facebook.com/\(fbId)/picture?type=large"
                cell.profileImg.sd_setImage(with: NSURL(string: profileUrl)! as URL, placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
            }
        }
        return cell
    }
    
    

    
    @IBAction func goToPremiumWalkthrough(_ sender: Any) {
        showPremiumWalkthroughViewController()
    }
    
    // MARK: switch toggle action
    @IBAction func darkModeAction(_ sender: Any) {
        if let status = UserDefaultsManager().userPremiumStatus {
            if status{
                 enableDarkMode()
            } else {
                alertVerifyPremiumDark()
            }
        }
      
    }
    
    @IBAction func hideAgeAction(_ sender: Any) {
        if let status = UserDefaultsManager().userPremiumStatus {
            //if status == "true"{
            if status{
               hideAge()
            } else {
                alertVerifyPremiumAge()
            }
        }
    }
    
    func hideAge(){
        VProfileWebService.onHideAge(successBlock: { (successMsg) in
            }, failureBlock: { (message) in
               // self.hideAgeSwitch.setOn(false, animated: true)
                showAlert(controller: self, title: localizedString(forKey: "ALERT"), message: message)
        })

    }
    
    func enableDarkMode(){
        VProfileWebService.onDarkMode(successBlock: { (successMsg) in
            }, failureBlock: { (message) in
                self.darkModeSwitch.setOn(false, animated: true)
                showAlert(controller: self, title: localizedString(forKey: "ALERT"), message: message)
        })

    }
    
    func alertVerifyPremiumDark() {
       let popController = Route.passportPop
        popController.premiumType = "dark"
        self.darkModeSwitch.isOn = false
        self.darkModeSwitch.tintColor = UIColor.white
         popController.isProfile = true
        self.present(popController, animated: true, completion: nil)
    }
    
    func alertVerifyPremiumAge() {
        let popController = Route.passportPop
        popController.premiumType = "age"
        self.hideAgeSwitch.isOn = false
        self.hideAgeSwitch.tintColor = UIColor.white
        popController.isProfile = true
        self.present(popController, animated: true, completion: nil)
    }
    
    
    //MARK: Social Media Methods
    
    func getUserNetworkDetail(userNetworks: [[String: AnyObject]]?){
        self.connectionStatusArr.removeAll()
        btnFbWidth.constant = 0
        horizontalSpaceBtnFbAndTwit.constant = 0
        btnTwitterWidth.constant = 0
        horizontalSpaceBtnTwitAndInsta.constant = 0
        btnInstagramWidth.constant = 0
        horrizontalSpaceBtnInstaAndLinkedIn.constant = 0
        btnLinkedInWidth.constant = 0
        horrizontalSpaceBtnInstaAndLinkedIn.constant = 0
        if let networks = userNetworks{
            for net in networks{
                if let networkType =  net["networkType"] as? String, let networkConnectStatus =  net["networkConnectStatus"] as? Bool{
                    self.connectionStatusArr.append(networkConnectStatus)
                    switch networkType {
                    case "Facebook":
                        if networkConnectStatus{
                            btnFbWidth.constant = 46
                            horizontalSpaceBtnFbAndTwit.constant = 17
                        }else{
                            btnFbWidth.constant = 0
                            horizontalSpaceBtnFbAndTwit.constant = 0
                        }
                        if let networkId = net["networkId"] as? String, let profileUrl = net["networkProfileUrl"] as? String{
                            btnFb.networkId = networkId
                            btnFb.networkUrl = profileUrl
                            btnFb.addTarget(self, action: #selector(MyProfileViewController.btnFBAction(sender:)), for: .touchUpInside)
                        }
                    case "Twitter":
                        if networkConnectStatus{
                            btnTwitterWidth.constant = 46
                            horizontalSpaceBtnTwitAndInsta.constant = 17
                        }else{
                            btnTwitterWidth.constant = 0
                            horizontalSpaceBtnTwitAndInsta.constant = 0
                        }
                        if let userName = net["networkUserName"] as? String{
                            btnTwitter.networkUserName = userName
                            btnTwitter.addTarget(self, action: #selector(MyProfileViewController.btnTwitterAction(sender:)), for: .touchUpInside)
                        }
                    case "Instagram":
                        if networkConnectStatus{
                            btnInstagramWidth.constant = 46
                            horrizontalSpaceBtnInstaAndLinkedIn.constant = 17
                        }else{
                            btnInstagramWidth.constant = 0
                            horrizontalSpaceBtnInstaAndLinkedIn.constant = 0
                        }
                        if let userName = net["networkUserName"] as? String{
                            btnInsta.networkUserName = userName
                            btnInsta.addTarget(self, action: #selector(self.btnInstaAction(sender:)), for: .touchUpInside)
                        }
                    case "LinkedIn":
                        if networkConnectStatus{
                            btnLinkedInWidth.constant = 46
                        }else{
                            btnLinkedInWidth.constant = 0
                        }
                        if let userName = net["networkUserName"] as? String{
                            btnLinkedIn.networkUserName = userName
                            btnLinkedIn.addTarget(self, action: #selector(self.btnLinkedInAction(sender:)), for: .touchUpInside)
                        }
                    default:
                        break
                    }
                }
            }
            hideSocialMediaView(hiddenStatus: connectionStatusArr.contains(true) ? false : true, constantHeight: connectionStatusArr.contains(true)  ? 100 : 0, constantTopSpace: connectionStatusArr.contains(true) ? 6 : 0)
        }else{
            hideSocialMediaView(hiddenStatus: true, constantHeight: 0, constantTopSpace: 0)
        }
    }
    
    // to hide or show social media view
    func hideSocialMediaView(hiddenStatus: Bool, constantHeight: CGFloat, constantTopSpace: CGFloat){
        btnFb.isHidden = hiddenStatus
        btnInsta.isHidden = hiddenStatus
        btnTwitter.isHidden = hiddenStatus
        btnLinkedIn.isHidden = hiddenStatus
        socialMediaViewHeightConstraint.constant = constantHeight
        socialMediaViewTopSpaceConstraint.constant = constantTopSpace
        
    }
    
    
    @objc func btnFBAction(sender: SocialMediaCustomButton) {
        if let fbUrl =  sender.networkUrl{
            UIApplication.shared.open(NSURL(string: fbUrl)! as URL, options: ["":""], completionHandler: nil)
        }
    }
    
     @objc func btnTwitterAction(sender: SocialMediaCustomButton) {
        if let twitUserName =  sender.networkUserName{
            let userName = twitUserName.replacingOccurrences(of: " ", with: "")
            if UIApplication.shared.canOpenURL(NSURL(string: "twitter://")! as URL) {
                UIApplication.shared.open(NSURL(string: "twitter://user?screen_name=\(userName)")! as URL, options: ["":""], completionHandler: nil)
            } else {
                 UIApplication.shared.open(NSURL(string: "https://twitter.com/\(userName)")! as URL, options: ["":""], completionHandler: nil)
            }
        }
    }
    
     @objc func btnInstaAction(sender: SocialMediaCustomButton) {
        if let instaUserName =  sender.networkUserName{
            if UIApplication.shared.canOpenURL(NSURL(string: "instagram://user?username=johndoe")! as URL) {
                UIApplication.shared.open(NSURL(string: "instagram://user?username=\(instaUserName)")! as URL, options: ["":""], completionHandler: nil)
            } else {
                UIApplication.shared.open(NSURL(string: "https://www.instagram.com/\(instaUserName)")! as URL, options: ["":""], completionHandler: nil)
            }
        }
    }
    
     @objc func btnLinkedInAction(sender: SocialMediaCustomButton) {
        if let linkedInId =  sender.networkUserName{
               UIApplication.shared.open(NSURL(string: "\(linkedInId)")! as URL, options: ["":""], completionHandler: nil)
        }
    }
    
    
    //MARK: Celeb Methods
    
    
    func checkIfUserIsCelebrity(isUserVerifiedCelebrity: Bool){
        if isUserVerifiedCelebrity{
            celebVerifiedImgView.isHidden = false
            fullName.textColor = UIColor.white
            userGenderAge.textColor = UIColor.white
            userDesc.textColor = UIColor.white
            userDetailView.backgroundColor = UIColor.colorFromRGB(rgbValue: 0x6d2b6b)
            //self.navForCeleb()
        }else{
            celebVerifiedImgView.isHidden = true
            fullName.textColor = UIColor.colorFromRGB(rgbValue: 0x6d2b6b)
            userGenderAge.textColor = UIColor.colorFromRGB(rgbValue: 0x6d2b6b)
            userDesc.textColor = UIColor.colorFromRGB(rgbValue: 0x6d2b6b)
            userDetailView.backgroundColor = UIColor.colorFromRGB(rgbValue: 0xF2F2F2)
            
        }
    }
    
    func navForCeleb(){
        // Only execute the code if there's a navigation controller
        if self.navigationController == nil {
            return
        }
        
        // Create a navView to add to the navigation bar
        let navView = UIView()
        
        // Create the label
        let label = UILabel()
        if let  name =  userInfo.fullName{
            label.text = name
        }
        
        //label.sizeToFit()
        label.center = navView.center
        label.textAlignment = NSTextAlignment.center
        
        // Create the image view
        let image = UIImageView()
        image.image = UIImage(named: "celeb_verified_tick")
        // To maintain the image's aspect ratio:
        let imageAspect = image.image!.size.width/image.image!.size.height
        // Setting the image frame so that it's immediately before the text:
        image.frame = CGRect(x: label.frame.origin.x-label.frame.size.height*imageAspect, y: label.frame.origin.y, width: label.frame.size.height*imageAspect, height: label.frame.size.height)
        image.contentMode = UIViewContentMode.scaleAspectFit
        
        // Add both the label and image view to the navView
        navView.addSubview(label)
        navView.addSubview(image)
        
        // Set the navigation bar's navigation item's titleView to the navView
        self.navigationItem.titleView = navView
        
        // Set the navView's frame to fit within the titleView
        navView.sizeToFit()
    }
    
    
    // MARK : ADD ME TO YOUR VIP
    
    
    
}

//MARK: IBACTIONS

extension MyProfileViewController{
    
    // when user clicks message user button
    @IBAction func messageUser(_ sender: Any) {
        message()
    }
    
    // when user clicks add to vip button
    @IBAction func addToMyVips(_ sender: Any) {
        let  parameters: [String: AnyObject] = [
            "fbIdList" : [fbId] as AnyObject
        ]
        self.btnAddToMyVIps.isEnabled = false
        self.indicator.start()
        self.view.isUserInteractionEnabled = false
        
        
        WebService.request(method: .post, url:  kAPICreateVIPList!, parameter: parameters, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success{
                self.indicator.stop()
                self.view.makeToast(message: "You have sent VIP request to \(self.fullName.text!).")
            }
            //self.btnAddToMyVIps.isEnabled = true
            self.view.isUserInteractionEnabled = true
        }) { (msg, err) in
            self.indicator.stop()
            self.view.isUserInteractionEnabled = true
            self.btnAddToMyVIps.isEnabled = true
            showAlert(controller: self, title: "", message: msg)
        }
    }
    
    
    // to block vite user
    @IBAction func blockUser(_ sender: Any) {
        if self.isMyVip{
            showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: localizedString(forKey: "CANNOT_BLOCK_VIP_USER"))
        }else{
            self.view.isUserInteractionEnabled = false
            let parameter:[String: AnyObject] =  [
                
                "fbIdList" : [fbId] as AnyObject
            ]
            self.indicator.start()
            
            WebService.request(method: .post, url: KAPIBlockUser + UserDefaultsManager().userFBID!, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
                guard let response = response.result.value as? [String:AnyObject] else {
                    print("Network Error!")
                    return
                }
                guard let success = response["success"] as? Bool else{
                    return
                }
                if success{
                    self.indicator.stop()
                    Route.goToHomeController()
                    showAlert(controller: self, title: localizedString(forKey: "SUCCESS"), message: "\(self.fullName.text!) has been blocked successfully.")
                }
                self.view.isUserInteractionEnabled = true
            }, failure: { (message, err) in
                self.indicator.stop()
                self.view.isUserInteractionEnabled = true
                showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: message)
            })
        }
    }
    
    
    @IBAction func viewAllComments(_ sender: Any) {
        let controller = Route.rateCommentList
        controller.fbId = isForViewingOthersProfile ? self.fbId : UserDefaultsManager().userFBID!
        self.present(controller, animated: true, completion: nil)
    }
    
    
    @IBAction func backToHome(_ sender: Any) {
        if isForViewingOthersProfile{
            if isFromVip{
                guard let nav = self.navigationController else {
                    return
                }
                nav.popViewController(animated: true)
                
            }else{
                if isFromPeopleList || isFromAttendeeRequest  {
                    self.navigationController?.popViewController(animated: true)
                } else  {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        } else if isFromChatVc{
            self.dismiss(animated: true, completion: nil)
        } else {
            Route.goToHomeController()
        }
    }
    
    
    @IBAction func addMeToUsersVip(_ sender: Any) {
        self.indicator.start()
        btnAddMeToYourVip.isEnabled = false
        self.view.isUserInteractionEnabled = false
        
        WebService.request(method: .post, url: kAPIAddMeToYourVip + fbId, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else{
                return
            }
            if success{
                self.indicator.stop()
                self.view.makeToast(message: localizedString(forKey: "VIP_REQUEST_SENT"))
            }
            self.view.isUserInteractionEnabled = true
        }) { (message, err) in
            self.indicator.stop()
            self.view.isUserInteractionEnabled = true
            self.btnAddMeToYourVip.isEnabled = true
            showAlert(controller: self, title: "", message: message)
        }
    }
    
    
    @IBAction func passportAction(_ sender: Any) {
       showPremiumWalkthroughViewController()
    }
    
    @IBAction func viteAllAction(_ sender: Any) {
          showPremiumWalkthroughViewController()
    }
    
    @IBAction func viteNowAction(_ sender: Any) {
         showPremiumWalkthroughViewController()
    }
    
    @IBAction func openMapForPremium(_ sender: Any) {
        
        if let latitude =  UserDefaults.standard.string(forKey: "locationLatitudePremium"), let longitude =  UserDefaults.standard.string(forKey: "locationLongitudePremium") {
          let coordinate = CLLocationCoordinate2DMake(Double(latitude)!, Double(longitude)!)
            self.getEventLocation(eventLocation: coordinate)
        } else {
            locationManager?.getUserLocation()
            locationManager?.fetchedUserLocation = {
                location in ()
                self.getEventLocation(eventLocation: location)
            }
        }
    }
    
    func getEventLocation (eventLocation: CLLocationCoordinate2D){
        let controller = Route.mapController
        controller.isForPremium = true
        controller.currentCoordinateOfAnnotation = {
            coordinate in
            self.eventLocation = coordinate
            let indicator = ActivityIndicator()
            indicator.parentView = self.lblLocation
            indicator.tintColor = UIColor.gray
            indicator.start()
            LocationManager.isForPremiumLocation = true
            LocationManager.getLocationStringFromCoordinate(coordinate: coordinate,
                                                            completionBlock: {
                                                                (location) -> Void in
                                                                self.lblLocation.text = location ?? ""
                                                                self.updateUserLocation(userLocation: self.eventLocation!)
                                                                UserDefaults.standard.set(location, forKey:  "userpremiumLocation")
                                                                 UserDefaults.standard.set(coordinate.latitude, forKey:  "locationLatitudePremium")
                                                                 UserDefaults.standard.set(coordinate.longitude, forKey:  "locationLongitudePremium")
                                                                indicator.stop()

            })
        }
        controller.currentLocation = eventLocation
        self.show(controller, sender: nil)
    }
    
    func updateUserLocation(userLocation: CLLocationCoordinate2D) {
        let parameter:[String: AnyObject] =
            [
                "latitude": userLocation.latitude as AnyObject,
                "longitude":userLocation.longitude  as AnyObject
        ]
        print(parameter)

        //Update user location every time the we get the user location on device.
        WebService.request(method: .post, url: kAPIUpdateUserLocation + UserDefaultsManager().userFBID!, parameter: parameter, header: nil, isAccessTokenRequired: false, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let Rmessage = response["success"] as? Int else{
                return
            }
            if Rmessage == 1 {
                //print("User location successfully updated")
            } else {
                //print("User location update failed")
            }
        }) { (message, err) in
            print("User location update failed")
        }
      //  FNode.lastUpdatedLocation.child(User().userFBID!).setValue(parameter)
    }

    
}


//MARK : Interest View Methods
extension MyProfileViewController{
    
    func getInterests(interests: [[String: AnyObject]]){
        for i in interests{
            tagLists.append(String(describing: i["filterName"]!))
        }
        self.intrestView.dataSource = self
        if tagLists.count > 0 {
            self.intrestViewHeightConstraint.constant = 65
      }else {
            self.intrestViewHeightConstraint.constant = 0
        }
        
    }
    
    // MARK: - TagViewDataSource
    func numberOfTags( _ tagView: HTagView) -> Int {
        return tagLists.count
        
    }
    
    func tagView(_ tagView: HTagView, titleOfTagAtIndex index: Int) -> String {
        return  tagLists[index]
    }
    
    func tagView(_ tagView: HTagView, tagTypeAtIndex index: Int) -> HTagType {
        return .select
    }
}

extension MyProfileViewController: UITableViewDataSource, UITableViewDelegate {
    
    func fetchComments(shouldReset reset:Bool) {
        indicator.tintColor = UIColor.gray
        indicator.start()
        if reset {
            self.cursor.reset()
        }
        VRateCommentWebService.getRateAndCommentList(fbId:  isForViewingOthersProfile ? fbId : UserDefaultsManager().userFBID!,  cursor.nextPage(), pageSize: cursor.pageSize, successBlock: { (commentList, totalCount) in
            
            if reset {
                self.rateAndCommentArr.removeAll()
            }
            for list in commentList! {
                // to display only 2 comments  in the design
                if self.rateAndCommentArr.count < 2 {
                   self.rateAndCommentArr.append(list)
                }
            }
            self.cursor.totalCount = totalCount.intValue
            self.lblCommentCount.text = "COMMENTS (\(totalCount.intValue))"
            self.cursor.totalLoadedDataCount = self.rateAndCommentArr.count
            if self.rateAndCommentArr.count == 0 {
                self.commentViewHeightConstraint.constant = 0
                self.btnViewAllComment.isHidden = true
            } else {
                self.tblRateComment.reloadData()
                self.tblRateComment.layoutIfNeeded()
                self.maintainTblViewHeight()
            }
            self.indicator.stop()
        }) { (msg) in
            print(msg)
            self.indicator.stop()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rateAndCommentArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rateComment = rateAndCommentArr[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "rateAndCommentCell", for: indexPath) as! RateAndCommentTableViewCell
        cell.lblOrganizerName.text = rateComment.fullName
        cell.lblComment.text = rateComment.comment
        cell.rating.rating =  rateComment.rating!
        return cell
    }
}

