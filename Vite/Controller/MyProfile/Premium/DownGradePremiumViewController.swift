//
//  DownGradePremiumViewController.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 11/28/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class DownGradePremiumViewController: UIViewController {

    @IBOutlet weak var btnDownGrade: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnDownGrade.layer.cornerRadius = 2
        btnDownGrade.clipsToBounds = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func continuePremium(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func downGradePremium(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        let url  = "https://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/manageSubscriptions"
        UIApplication.shared.open(NSURL(string: url)! as URL, options: ["":""], completionHandler: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
