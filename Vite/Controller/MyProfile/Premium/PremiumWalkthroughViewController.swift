

//
//  PremiumWalkthroughViewController.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 10/13/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit
import StoreKit

protocol premiumDelegate {
    func serviceCallGetUserProfile()
}

class PremiumWalkthroughViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SKProductsRequestDelegate, SKPaymentTransactionObserver{
    var walkthroughArr = ["passport", "godark", "age", "premiumViteAll", "premiumViteNow"]
    var cellColorArr = [0x1A96E7, 0x302C44, 0x1AC28D, 0xFF3159, 0xB35D9B]
    
    @IBOutlet weak var wtCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var btnUpgrade: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var subscriptionView: UIView!
    
    
    var isFromProfile = false
    var delegate: premiumDelegate?
    var product_id: NSString?
    
    let defaults = UserDefaults.standard
    let receiptURL = Bundle.main.appStoreReceiptURL
    var ud = UserDefaultsManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.isHidden = true
        product_id = "com.eeposit.Vite.premium"
        SKPaymentQueue.default().add(self)
        btnUpgrade.layer.cornerRadius = 2
        btnUpgrade.clipsToBounds = true
        subscriptionView.layer.cornerRadius = 5
        subscriptionView.clipsToBounds = true

        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        SKPaymentQueue.default().remove(self)
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: COLLECTION VIEW DELEGATE AND DATASOURCE
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return walkthroughArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let wtImg = walkthroughArr[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wtCVCell", for: indexPath) as! premiumWTCollectionViewCell
        cell.wtImgView.image = UIImage(named: wtImg)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         return CGSize(width: self.view.frame.width, height: CGFloat(307))
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cellbgColor = cellColorArr[indexPath.item]
        pageControl.currentPage = indexPath.item
        UIView.animate(withDuration: 1.0) {
            self.wtCollectionView.backgroundColor = UIColor.colorFromRGB(rgbValue: UInt(cellbgColor))
            self.view.backgroundColor = UIColor.colorFromRGB(rgbValue: UInt(cellbgColor))
        }
    }
    
    @IBAction func showPrivacyPolicy(_ sender: Any) {
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "subsVc") as! SubscriptionViewController
        
        UserDefaults.standard.set("Privacy Policy", forKey: "urlTypeSubs")
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func showTermsOfUse(_ sender: Any) {
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "subsVc") as! SubscriptionViewController
        UserDefaults.standard.set("Terms of Use", forKey: "urlTypeSubs")
        self.show(controller, sender: self)
    }
    

    
    
    @IBAction func upgradePremium(_ sender: Any) {
        self.upgradePremiumToServer()
    }
    
    
    func premiumAccountSubscription() {
        let alert = UIAlertController(title: "",
                                      message: localizedString(forKey: "UPGRADE_TO_PREMIUM"),
                                      preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok",
                                     style: .default,
                                     handler: { (action:UIAlertAction) -> Void in
                                        self.upgradePremiumToServer()
                                        
        })
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .default) { (action: UIAlertAction) -> Void in
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        present(alert,
                              animated: false,
                              completion: nil)
    }
    
    func upgradePremiumToServer() {
        self.view.isUserInteractionEnabled = false
        print("About to fetch the products")
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        // We check that we are allow to make the purchase.
        if (SKPaymentQueue.canMakePayments()) {
            let productID:NSSet = NSSet(object: self.product_id!);
            print(productID)
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
            print(productsRequest)
            productsRequest.delegate = self;
            productsRequest.start();
            print("Fetching Products");
        } else {
            print("can't make purchases");
        }
    }
    
    func buyProduct(product: SKProduct) {
        print("Sending the Payment Request to Apple");
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
    
    
    func sendPremiumDetailToServer() {
        
        guard let url = receiptURL,  let receipt = NSData(contentsOf: url) else {
            return
        }
        let receiptString = receipt.base64EncodedString(options: [])
        VProfileWebService.upgradeToPremium(receiptData: receiptString, successBlock: { (successMsg) in
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
            self.ud.userPremiumStatus = true
            self.dismiss(animated: true, completion: nil)
            if self.isFromProfile {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "premiumNotification"), object: nil)
            } else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "premiumNotificationForUserMenu"), object: nil)
            }
        }) { (message) in
            showAlert(controller: self, title: "", message: message)
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
        }
    }
    
}

extension PremiumWalkthroughViewController {
    func productsRequest (_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        let count : Int = response.products.count
        print(response.products.count)
        if (count>0) {
            let validProduct: SKProduct = response.products[0]
            guard let prodId = self.product_id else {
                return
            }
            if (validProduct.productIdentifier == prodId as String) {
                print(validProduct.localizedTitle)
                print(validProduct.localizedDescription)
                print(validProduct.price)
                buyProduct(product: validProduct);
            } else {
                print(validProduct.productIdentifier)
            }
        } else {
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
            self.view.isUserInteractionEnabled = true
            print("nothing")
        }
    }
    
    func request(request: SKRequest, didFailWithError error: NSError) {
        print("Error Fetching product information");
        self.activityIndicator.isHidden = true
        self.activityIndicator.stopAnimating()
    }
    
    
    
    func paymentQueue(_ queue: SKPaymentQueue,
                      updatedTransactions transactions: [SKPaymentTransaction])   {
        print("Received Payment Transaction Response from Apple");
        
        for transaction:AnyObject in transactions {
            if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction{
                print(trans.transactionState.rawValue)
                switch trans.transactionState {
                case .purchased:
                    print("Product Purchased");
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    self.view.isUserInteractionEnabled = true
                    sendPremiumDetailToServer()
                    break;
                case .failed:
                    if let errosMsg = trans.error?.localizedDescription{
                        showAlert(controller: self, title: localizedString(forKey: "TRANSACTION_FAILED"), message: (errosMsg))
                    }
                    print("Purchased Failed");
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    self.activityIndicator.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.activityIndicator.stopAnimating()
                    break;
                case .restored:
                    print("Already Purchased");
                    SKPaymentQueue.default().restoreCompletedTransactions()
                    self.activityIndicator.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    self.activityIndicator.stopAnimating()
                default:
                    break;
                }
            }
        }
    }
}
