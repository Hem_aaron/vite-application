//
//  SubscriptionViewController.swift
//  Vite
//
//  Created by Hem Poudyal on 1/9/18.
//  Copyright © 2018 EeposIT_X. All rights reserved.
//

import Foundation
import UIKit

class SubscriptionViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    var urlType = ""
    @IBOutlet weak var nameTitle: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        urlType = UserDefaults.standard.object(forKey: "urlTypeSubs") as! String
        var webURL = ""
        nameTitle.text = urlType
        
        if urlType == "Privacy Policy"{
            webURL = kAPIPrivacyPolicyUrl
        } else {
            webURL = kAPITermsAndConditionUrl
        }
        print(webURL)
        
        let url = NSURL(string: webURL)
        webView.loadRequest(NSURLRequest(url: url! as URL) as URLRequest)
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
