//
//  passportPopViewController.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 11/14/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit



class premiumPopViewController: UIViewController {

    
    @IBOutlet weak var btnUpgrade: UIButton!
    @IBOutlet weak var imgPremium: UIImageView!
    
    var isProfile = false
    var premiumType = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAsPremiumType(premType: premiumType)
        btnUpgrade.layer.cornerRadius = 4
        btnUpgrade.clipsToBounds = true
        // Do any additional setup after loading the view.
    }
    
    func setupAsPremiumType(premType: String){
        switch premiumType {
        case "passport":
            imgPremium.image = UIImage(named: "passport_pop")
        case "viteAll":
             imgPremium.image = UIImage(named: "viteall_pop")
        case "age":
             imgPremium.image = UIImage(named: "age_pop")
        case "dark":
             imgPremium.image = UIImage(named: "godark_pop")
        default:
            break
        }
    }
    
    @IBAction func goToWalkThrough(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Profile", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "PremiumWalkthroughViewController") as! PremiumWalkthroughViewController
        controller.isFromProfile = isProfile
        self.present(controller, animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)
    }
    

    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
