//
//  RateAndCommentListViewController.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 4/6/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit

class RateAndCommentListViewController: WrapperController, UITableViewDelegate, UITableViewDataSource {
     let cursor = VCursor()
    var rateAndCommentArr = [VRateComment]()
    @IBOutlet weak var lblMessage: UILabel!
    let indicator = ActivityIndicator()
     let message = localizedString(forKey: "NOT_RATED")
    @IBOutlet weak var tblRateAndComment: UITableView!
    var fbId: String?
    var businessId : String?
    @IBOutlet weak var tblRateComment: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // if there is a value in business id which means user is trying to get the list of business account comments.
        if let _ = businessId {
            self.fetchCommentsForBusiness(shouldReset: true)
        } else {
             self.fetchComments(shouldReset: true)
        }
        
    }
    
    func fetchComments(shouldReset reset:Bool) {
        indicator.tintColor = UIColor.gray
        indicator.start()
        if reset {
            self.cursor.reset()
        }
        VRateCommentWebService.getRateAndCommentList(fbId: fbId!,  cursor.nextPage(), pageSize: cursor.pageSize, successBlock:{ (commentList, totalCount) in
            if reset {
                self.rateAndCommentArr.removeAll()
            }
            
            self.rateAndCommentArr += commentList!
            self.cursor.totalCount = totalCount.intValue
            self.cursor.totalLoadedDataCount = self.rateAndCommentArr.count
            self.tblRateComment.reloadData()
             self.indicator.stop()
            if self.rateAndCommentArr.count == 0 {
                self.lblMessage.isHidden = false
            } else {
                self.lblMessage.isHidden = true
            }
        }) { (msg) in
            print(msg)
            self.indicator.stop()
        }
    }
    
    func fetchCommentsForBusiness(shouldReset reset:Bool) {
        indicator.tintColor = UIColor.gray
        indicator.start()
        if reset {
            self.cursor.reset()
        }
        VRateCommentWebService.getRateAndCommentListForBusinessAccount(businessAccountId: businessId!,  cursor.nextPage(), pageSize: cursor.pageSize, successBlock:{ (commentList, totalCount) in
            if reset {
                self.rateAndCommentArr.removeAll()
            }
            
            self.rateAndCommentArr += commentList!
            self.cursor.totalCount = totalCount.intValue
            self.cursor.totalLoadedDataCount = self.rateAndCommentArr.count
            self.tblRateComment.reloadData()
            self.indicator.stop()
            if self.rateAndCommentArr.count == 0 {
                self.lblMessage.isHidden = false
            } else {
                self.lblMessage.isHidden = true
            }
        }) { (msg) in
            print(msg)
            self.indicator.stop()
        }
    }

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rateAndCommentArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rateComment = rateAndCommentArr[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "rateAndCommentCell", for: indexPath) as! RateAndCommentTableViewCell
        cell.lblOrganizerName.text = rateComment.fullName
        cell.lblComment.text = rateComment.comment
        cell.rating.rating = rateComment.rating!
        return cell
    }
    
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.cursor.hasNextPage() && indexPath.row == (self.rateAndCommentArr.count - 1) {
            self.fetchComments(shouldReset: false)
        }
    }
    
}
