//
//  RateAndCommentTableViewCell.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 4/6/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit
import Cosmos

class RateAndCommentTableViewCell: UITableViewCell {

    @IBOutlet weak var rating: CosmosView!
    @IBOutlet weak var lblOrganizerName: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        rating.settings.fillMode = .half
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
