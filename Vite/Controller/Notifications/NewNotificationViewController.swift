//
//  NewNotificationViewController.swift
//  Vite
//
//  Created by Eeposit 01 on 4/25/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit
import ViewAnimator

enum userNotificationTypes:String {
    case requestReceived  =  "REQUEST_RECEIVED"
    case requestAccepted  =  "REQUEST_ACCEPTED"
    case eventCreated     =  "EVENT_CREATED"
    case eventEdited      =  "EVENT_EDITED"
    case vipRequested     =  "VIP_REQUESTED"
    case vipAccepted      =  "VIP_ACCEPTED"
    case feedComment      =  "FEED_COMMENT"
    case organizerRated   =  "RATED"
}

enum  NotificationTypes : String{
    case eventType    = "EVENT_TYPE"
    case userType     = "USER_TYPE"
    case businessType  = "BUSINESS_TYPE"
}

class NewNotificationViewController: WrapperController, UIGestureRecognizerDelegate  {
    var notificationList = [Notification]()
    var showAnim: Bool?
    let indicator = ActivityIndicator()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Setup our animaiton view
        showAnim = true
        self.indicator.start()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.navigationItem.title = "Notifications"
        //swipe back
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if self.showAnim! {
        self.showAnim = false
        self.fetchNotificationList()
        }
    }
    
    func fetchNotificationList () {
        NotificationServices.listMessageAndNotificationForUser({ [weak self](listOfNotificationMsg) in
                self?.notificationList = listOfNotificationMsg
                self?.tableView.reloadData()
                self?.tableView.animate(animations: [AnimationsList.fromBottom], reversed: false, initialAlpha: 0, finalAlpha: 1, delay: 0, duration: 1, completion: {
                    self?.indicator.stop()
                })
            })
       { (message) in
            showAlert(controller: self, title: "", message: message)
        }
    }
    
    @IBAction func back(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension  NewNotificationViewController : UITableViewDelegate ,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let notification = notificationList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationTableViewCell
        cell.lblNotification.text = notification.message?.html2String
        if let createDate = notification.createdDate {
            let date = Date(timeIntervalSince1970: createDate.doubleValue / 1000)
            cell.time.text = agoDates(date)
        }
        if let imageURL = notification.imageUrl?.convertIntoViteURL() {
            cell.notificationImage.sd_setImage(with: URL(string: imageURL))
        }
        
        if notification.seenStatus == true {
            cell.backgroundColor = UIColor.white
        } else {
            cell.backgroundColor = UIColor(rgb: AppProperties.viteAppThemeColor).withAlphaComponent(0.1)
        }
        cell.onDelete = {[unowned self] in
            self.deleteNotificationMsg(notification.messageId!, indexPath: indexPath)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let notification = notificationList[indexPath.row]
        if notification.seenStatus == false {
            makeNotificationSeen(notification, indexPath: indexPath)
        }
        viewNotificationDetailWithNotification(notification)
    }
}
extension NewNotificationViewController {
    // MARK: Api call
    func makeNotificationSeen(_ notification : Notification , indexPath : IndexPath) {
        NotificationServices.makeNotifiactionSeeen(notification.messageId!, successBlock: { [unowned self] (status) in
            if status {
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
                self.fetchNotificationList()
            }
            }, failureBlock: { [unowned self] (message) in
                showAlert(controller: self, title: "", message : message)
        })
    }
    
    func deleteNotificationMsg (_ messageId:String,indexPath : IndexPath) {
        
        NotificationServices.deleteParticularNotification(messageId, successBlock: { [unowned self] (successMsg) in
            self.view.makeToast(message: successMsg)
            self.notificationList.remove(at: indexPath.row)
            self.tableView.reloadData()
        }) { [unowned self] (message) in
            showAlert(controller: self, title: "", message : message)
        }
    }
    //MARK: Private Function
    func viewNotificationDetailWithNotification(_ notification:Notification) {
        guard let userNType = notification.userNotificationType else {
            return
        }
        switch userNType{
        case userNotificationTypes.requestReceived.rawValue:
            self.showAttendeeController(notification)
            break
        case userNotificationTypes.requestAccepted.rawValue:
            self.showViteDetailController(notification)
            break
        case userNotificationTypes.eventCreated.rawValue:
            self.showHomeController(notification)
            break
        case userNotificationTypes.eventEdited.rawValue:
            self.showViteDetailController(notification)
            break
        case userNotificationTypes.vipRequested.rawValue:
            self.showVIPController(notification)
            break
        case userNotificationTypes.vipAccepted.rawValue:
            self.showVIPController(notification)
            break
        case userNotificationTypes.feedComment.rawValue:
            showImageOrVideoScreen(notification)
            break
        case userNotificationTypes.organizerRated.rawValue:
            showProfileController(notification)
            break
        default :
            break
        }
    }
    
    func showVIPController(_ notification:Notification) {
        if notification.userNotificationType ==  userNotificationTypes.vipRequested.rawValue{
            let controller = Route.addToVip
            controller.automaticallyAdjustsScrollViewInsets = false
            self.show(controller, sender: nil)
        } else {
            let controller = Route.Vip
            controller.automaticallyAdjustsScrollViewInsets = false
            controller.fromNotification = true
            self.show(controller, sender: nil)
        }
    }
    
    func showAttendeeController(_ notification:Notification) {
        AppDel.beginIgnoring()
        EventServices.getVEventDetail(eventID: notification.eventId!, successBlock: {
            [unowned self]   (eventDetail) in
            let controller = Route.attendeeRequest
            controller.eventDetail = eventDetail
            self.show(controller, sender: nil)
            AppDel.endIgnoring()
        })
    }
    
    func showProfileController(_ notification:Notification) {
         self.show(Route.profileViewController, sender: nil)
    }
    
    func showViteDetailController(_ notification:Notification) {
        let controller = Route.viteDetail
        controller.viteIdFromNotification = notification.eventId!
        self.show(controller, sender: nil)
    }
    
    func showHomeController(_ notification:Notification) {
        UserDefaults.standard.set(notification.eventId, forKey: "eventIdFromNotification")
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: "ShowEventDetailAtHome"), object: nil)
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    func showImageOrVideoScreen(_ feed:Notification){
        let swipeFullScreenVC = Route.SwipeFullScreenViewController
        if let imageUrl = feed.imageUrl {
            swipeFullScreenVC.mediaURL = imageUrl
        }
        swipeFullScreenVC.feedId = feed.feedId
        swipeFullScreenVC.isFromNotification = true
        VEventWebService.getEventDetail(feed.eventId!, successBlock: { [unowned self] (eventDetail) in
            swipeFullScreenVC.detailEvent = eventDetail
            self.navigationController?.pushViewController(swipeFullScreenVC, animated: true)
        }) { [unowned self] (message) in
            showAlert(controller: self, title: " ", message: message)
        }
    }
}
//String.Encoding.utf8.rawValue
extension String {
    var html2AttributedString: NSAttributedString? {
        guard
            let data = data(using: String.Encoding.utf8)
            else { return nil }
        do {
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
