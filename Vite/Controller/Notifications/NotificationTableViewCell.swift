//
//  NotificationTableViewCell.swift
//  Vite
//
//  Created by Eeposit1 on 6/22/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet var time: UILabel!
    @IBOutlet var notificationImage: UIImageView!
    @IBOutlet var lblNotification: UILabel!
    var onDelete:(() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func deleteActions(_ sender: UIButton) {
        if let del = onDelete {
            del()
        }
    }
    
}
