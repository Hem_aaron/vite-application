//
//  PaymentHistoryAttendeeTableViewCell.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 7/25/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class PaymentHistoryAttendeeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblAttendeeName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblAcceptedDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
