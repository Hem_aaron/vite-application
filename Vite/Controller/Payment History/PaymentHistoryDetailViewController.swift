//
//  PaymentHistoryDetailViewController.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 7/25/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class PaymentHistoryDetailViewController: UIViewController {
    
    @IBOutlet weak var eventRevenue: UILabel!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    @IBOutlet weak var attendeeTblView: UITableView!
    var event: VEvent?
    var userArray:[MyEventAttendee]? = [MyEventAttendee]()
    let cursor = VCursor()
    let indicator = ActivityIndicator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateData()
        fetchAttendeesWithRevenue(shouldReset: true)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func populateData() {
        if let name = event?.title {
            eventTitle.text = name
        }
        if let date = event?.date {
            eventDate.text = convertDate(date)
        }
        //TODO:
                if let price = event?.totalRevenue {
                    eventRevenue.text = "$\(price)"
                }
    }
    
    @IBAction func back(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func fetchAttendeesWithRevenue(shouldReset reset:Bool) {
        guard let id = event?.uid else {
            return
        }
        indicator.tintColor = UIColor.gray
        indicator.start()
        if reset {
            self.cursor.reset()
        }
        
        VPaymentHistoryWebService().getAllAttendeeListWithRevenue(id, pageNo: cursor.nextPage(), pageSize: cursor.pageSize, successBlock: { (users, totalCount) in
            if reset {
                self.userArray?.removeAll()
            }
            self.userArray?.append(contentsOf: users)
            self.cursor.totalCount = totalCount
            self.cursor.totalLoadedDataCount = self.userArray!.count
            self.indicator.stop()
            self.attendeeTblView.reloadData()
        }) { (message) in
            self.indicator.stop()
            showAlert(controller: self, title: "", message: message)
        }
        
    }
    
    func convertDate(_ date: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let nsDate = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MMM d yyyy"
        return  dateFormatter.string(from: nsDate!)
    }
}

extension PaymentHistoryDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let arr = self.userArray {
            return arr.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let attendee = self.userArray![indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "paymentDetailAttendeeCell") as!PaymentHistoryAttendeeTableViewCell
        cell.lblAttendeeName.text = attendee.fullName!
        if let profileUrl = attendee.profileImageUrl{
            let url = profileUrl.convertIntoViteURL()
            cell.imgProfile.sd_setImage(with: URL(string: url), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
        }
        if let amt = attendee.chargeAmount {
            cell.lblAmount.text = "$\(amt)"
        }
        if let invitedDate = attendee.createdDate {
            let date = Date(timeIntervalSince1970: invitedDate.doubleValue / 1000)
            cell.lblAcceptedDate.text = agoDates(date)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if self.cursor.hasNextPage() && indexPath.row == (userArray!.count - 1) {
            self.fetchAttendeesWithRevenue(shouldReset: false)
            
        }
    }
}
