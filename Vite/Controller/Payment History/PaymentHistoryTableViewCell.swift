//
//  PaymentHistoryTableViewCell.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 7/25/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class PaymentHistoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var eventPrice: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.cornerRadius = 10
        mainView.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
