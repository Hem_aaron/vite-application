//
//  PaymentHistoryViewController.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 7/25/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit
import ViewAnimator
class PaymentHistoryViewController: UIViewController {
    
    @IBOutlet weak var lblNoPaidEvents: UILabel!
    @IBOutlet weak var revenueView: UIView!
    @IBOutlet weak var totalRevenue: UILabel!
    @IBOutlet weak var paymentTblView: UITableView!
    var payment: VPayment?
    var eventRevenueList = [VEvent]()
    let cursor = VCursor()
    let indicator = ActivityIndicator()
    var showAnim: Bool?
    override func viewDidLoad() {
        super.viewDidLoad()
        showAnim = true
        setupNav(false)
        getRevenueList(shouldReset: true)
        // Do any additional setup after loading the view.
        totalRevenue.alpha = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 1.2, animations: {
            self.totalRevenue.alpha = 1
        }, completion: nil)
    }
    func getRevenueList(shouldReset reset:Bool) {
        indicator.tintColor = UIColor.gray
        indicator.start()
        if reset {
            self.cursor.reset()
        }
        
        VPaymentHistoryWebService().getEventsRevenueList(cursor.nextPage(), pageSize: cursor.pageSize, successBlock: {
            (payment) in
            self.payment = payment
            
            if reset {
                self.eventRevenueList.removeAll()
            }
            
            if let event = payment?.eventList {
                self.eventRevenueList.append(contentsOf: event)
                if self.eventRevenueList.count == 0 {
                    self.hideView(hiddenState: true, alphaValue: 0)
                } else {
                    self.hideView(hiddenState: false, alphaValue: 1)
                }
            }
            self.cursor.totalCount = (payment?.eventCount)!
            self.cursor.totalLoadedDataCount = self.eventRevenueList.count
            if let total = self.payment?.totalRevenue{
                self.totalRevenue.text = "$ \(total)"
            }
            self.indicator.stop()
            self.paymentTblView.reloadData()
            if self.showAnim! {
                self.showAnim = false
                self.paymentTblView.animate(animations: [AnimationsList.fromBottom], reversed: false, initialAlpha: 0, finalAlpha: 1, delay: 0, duration: 0.8, completion: nil)
            }
            
        }, failureBlock: {
            (message) in
            self.indicator.stop()
            showAlert(controller: self, title: "", message: message)
        })
        
    }
        
    @IBAction func back(_ sender: AnyObject) {
        UIView.animate(withDuration: 0.8, animations: {
            self.totalRevenue.alpha = 0
        }, completion: nil)
        self.paymentTblView.animate(animations: [AnimationsList.fromBottom], reversed: true, initialAlpha: 1, finalAlpha: 0, delay: 0, duration: 0.8, completion: {
            Route.goToHomeController()
        })
        }
    
    // Navigation setup
    func setupNav(_ clearNavColor: Bool) {
        if let nav = self.navigationController {
            nav.navigationBar.setBackgroundImage(clearNavColor ? UIImage() : UIImage(named:"purpleNav"), for: UIBarMetrics.default)
            nav.navigationBar.shadowImage                  = UIImage()
            if clearNavColor{
                nav.view.backgroundColor                       = UIColor.clear
            }
            self.navigationItem.hidesBackButton            = true
            nav.isNavigationBarHidden                        = false
            let navBar                                     = self.navigationController!.navigationBar
            navBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: clearNavColor ? UIColor.purple  : UIColor.white , NSAttributedStringKey.font: UIFont(name: AppProperties.viteFontName, size: AppProperties.viteNavigationTitleFontSize)!]
            self.automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    func hideView(hiddenState state: Bool, alphaValue: CGFloat) {
        revenueView.alpha = alphaValue
        paymentTblView.isHidden = state
        lblNoPaidEvents.isHidden = !state
    }
}

extension PaymentHistoryViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.eventRevenueList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let eventRevenue = self.eventRevenueList[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "paymentHistoryTvCell") as! PaymentHistoryTableViewCell
        if let date = eventRevenue.date {
            cell.eventDate.text = convertDate(date)
        }
        if let name = eventRevenue.title {
            cell.eventName.text = "\(name)"
        }
        
        
        if let revenue = eventRevenue.totalRevenue {
            cell.eventPrice.text = "$\(revenue)"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let eventRevenue = self.eventRevenueList[indexPath.row]
        let detailController = Route.paymentHistoryDetails
        detailController.event = eventRevenue
        self.navigationController?.show(detailController, sender: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let eventList = self.payment?.eventList{
            if self.cursor.hasNextPage() && indexPath.row == (eventList.count - 1) {
                self.getRevenueList(shouldReset: false)
            }
        }
    }
    
    func convertDate(_ date: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let nsDate = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MMM d yyyy"
        return  dateFormatter.string(from: nsDate!)
    }
    
    
}
