//
//  FeaturePaymentViewController.swift
//  Vite
//
//  Created by Eeposit 01 on 5/24/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit
import Stripe
protocol FeaturePaymentDelegate {
    func featurePaymentPassParameter(_ parameter: [String:AnyObject],isFromFeaturedView: Bool)
}

class FeaturePaymentViewController: WrapperController ,STPPaymentCardTextFieldDelegate  {
    
    let paymentTextField = STPPaymentCardTextField()
    let indicator = ActivityIndicator()
    @IBOutlet weak var lblEventName: UILabel!
    
    @IBOutlet weak var lblFeeTable: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var payBtn: UIButton!
    @IBOutlet weak var btnChangeCard: UIButton!
    @IBOutlet weak var lblCardNum: UILabel!
    @IBOutlet weak var imgCard: UIImageView!
    @IBOutlet weak var viewPayment: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    var isEligibleToPay = false
    var changeBtnToPayBtn = false
    var eventName: String?
    var parameter: [String:AnyObject]?
    var evenType = String()
    var delegate: FeaturePaymentDelegate?
    var isFromEditView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.isEnabled = true
        payBtn.isEnabled = false
        payBtn.isHidden = true
        lblCardNum.text = ""
        self.btnChangeCard.isHidden = true
        self.viewPayment.isHidden = true
        viewPayment.layer.cornerRadius = 5
        btnChangeCard.layer.cornerRadius = 5
        self.title = localizedString(forKey: "FEATURED_EVENT")
        if let name = eventName {
            self.lblEventName.text = name
        }
        getStripeInfo()
        getStripeAccountStatus()
        getPaymentInfo()
    }
    
    //MARK: Actions
    @IBAction func payBtn(_ sender: AnyObject) {
        indicator.start()
        btnChangeCard.isEnabled = false
        self.view.isUserInteractionEnabled = false
        if isEligibleToPay {
            if let del = self.delegate {
                del.featurePaymentPassParameter(self.parameter!, isFromFeaturedView: isFromEditView)
                indicator.stop()
            }
        } else {
            if paymentTextField.cardNumber == nil {
                showAlert(controller: self, title: "", message: localizedString(forKey: "ENTER_YOUR_CARD_DETAILS"))
                btnChangeCard.isEnabled = true
                indicator.stop()
            }
            let card = paymentTextField.cardParams
            #if DEVELOPMENT
                //For test only
                self.updateStripeToken(TestTokens[String(card.last4()!)]!)
                self.indicator.stop()
                self.view.isUserInteractionEnabled = true
                self.btnBack.isEnabled = true
                self.btnChangeCard.isEnabled = true
            
            #else
                STPAPIClient.shared().createToken(withCard: card) { [weak self] (token, error) -> Void in
                    if let error = error  {
                        print (error)
                        
                        showAlert(controller: self!, title: localizedString(forKey: "INVALID_CARD"), message: localizedString(forKey: "USE_ANOTHER_CARD"))
                    } else if let token = token {
                        self?.updateStripeToken(String(describing: token))
                    }
                    self?.indicator.stop()
                    self?.view.isUserInteractionEnabled = true
                    self?.btnBack.isEnabled = true
                    self?.btnChangeCard.isEnabled = true
                }
            #endif
            }
        btnBack.isEnabled = false
    }
    
    @IBAction func changeCard(_ sender: AnyObject) {
        if changeBtnToPayBtn {
            self.payBtn("" as AnyObject)
        } else {
            let alert = UIAlertController(title: localizedString(forKey: "CHANGE_CARD"),
                                          message: localizedString(forKey: "PROCEED_WITH_NEWCARD"),
                                          preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Yes",
                                         style: .default,
                                         handler: {
                                            (action:UIAlertAction) -> Void in
                                            
                                            self.btnChangeCard.setTitle("Pay", for: UIControlState())
                                            self.changeBtnToPayBtn = true
                                            self.viewPayment.isHidden = true
                                            self.isEligibleToPay = false
                                            self.showStripeCard()
            })
            
            let cancelAction = UIAlertAction(title: "No",
                                             style: .cancel) {
                                                (action: UIAlertAction) -> Void in
            }
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            self.present(alert,
                         animated: false,
                         completion: nil)
        }
    }
    
    @IBAction func goBack(_ sender: AnyObject) {
        guard let nav = self.navigationController else {
            return
        }
        nav.popViewController(animated: true)
    }
    
    //MARK: Privat Method
    func showStripeCard(){
        self.paymentTextField.frame = CGRect(x: 15, y: ScreenSize.SCREEN_HEIGHT - 120 , width: (self.view.frame).width - 30, height: 44)
        self.paymentTextField.delegate = self
        self.view.addSubview(self.paymentTextField)
    }
    
    func updateStripeToken(_ token: String) {
        indicator.stop()
        self.btnBack.isEnabled = false
        VStripeWebService.updateStripeToken(token: token, { (success) in
            self.view.isUserInteractionEnabled = false
            let alert = UIAlertController(title: localizedString(forKey: "CONGRATULATIONS"), message: localizedString(forKey: "CARD_ADDED_SUCCESSFULLY"),preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK",
                                         style: .default,
                                         handler: {
                                            (action:UIAlertAction) -> Void in
                                            self.indicator.start()
                                            if let del = self.delegate {
                                                del.featurePaymentPassParameter(self.parameter!,isFromFeaturedView: self.isFromEditView)
                                            }
            })
            alert.addAction(okAction)
            self.present(alert,
                         animated: true,
                         completion: nil)
        }) { (msg) in
            showAlert(controller: self, title: "", message: msg)
            self.indicator.stop()
        }
    }
    
    func getStripeInfo(){
        VStripeWebService.getStripeInfo({ (card) in
            self.isEligibleToPay = true
            self.indicator.stop()
            self.viewPayment.isHidden = false
            if let cardlast4 = card.lastFour {
                self.lblCardNum.text = "*****" + cardlast4
                self.btnChangeCard.isHidden = false
                self.payBtn.isHidden = false
                self.payBtn.isEnabled = true
            } else {
                self.showStripeCard()
            }
            if let cardType = card.cardType{
                self.imgCard.image = UIImage(named: "\(cardType)")
            }
        }) { (message) in
            self.indicator.stop()
            if message == localizedString(forKey: "NO_CARD_MSG"){
                self.viewPayment.isHidden = true
                self.showStripeCard()
                
            } else {
                showAlert(controller: self, title: "", message: message)
            }
        }
    }
    
    func getPaymentInfo(){
        indicator.start()
        VStripeWebService.getPaymentInfo({ (fee) in
            self.lblFeeTable.text = ("$\(fee)")
            self.lblTotalAmount.text = ("$\(fee)")
            self.indicator.stop()
        }) { (message) in
            self.indicator.stop()
            showAlert(controller: self, title: "", message: message)
            self.indicator.stop()
        }
    }
    
    func getStripeAccountStatus(){
        VStripeWebService.checkStripeStatus({ (cardStatus) in
            if !cardStatus{
                self.btnChangeCard.setTitle("Pay", for: UIControlState())
                self.btnChangeCard.isHidden = false
            } else {
                self.changeBtnToPayBtn = false
            }
        }) { (message) in
            print(message)
        }
    }
    
    //MARK: STPPayment Delegate
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        // Toggle navigation, for example
        payBtn.isEnabled = textField.isValid
    }
}

