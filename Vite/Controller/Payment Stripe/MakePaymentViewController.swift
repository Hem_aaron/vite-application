//
//  MakePaymentViewController.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 4/25/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit

class MakePaymentViewController: UIViewController {

    var event:VEvent?
   let indicator = ActivityIndicator()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var payWithCard: UIButton!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func payWithCard(_ sender: Any) {
        let controller = Route.paymentViewController
        controller.eventDetail = event
        let nav = UINavigationController(rootViewController: controller)
        self.show(nav, sender: nil)
    }
    
    @IBAction func payWithPaypal(_ sender: Any) {
        let controller = Route.paymentViewController
        controller.eventDetail = event
        controller.isForPaypal = true
        let nav = UINavigationController(rootViewController: controller)
        self.show(nav, sender: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.indicator.stop()
    }
    
    @IBAction func cancel(_ sender: Any) {
       Route.goToHomeController()
    }
}


