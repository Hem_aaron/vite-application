//
//  PaymentView.swift
//  Vite
//
//  Created by EeposIT_X on 6/27/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class PaymentView: UIView {
  @IBOutlet var view: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        Bundle.main.loadNibNamed("PaymentView", owner: self, options: nil)
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("PaymentView", owner: self, options: nil)
        view.frame = self.bounds
       // eventName.adjustsFontSizeToFitWidth = true
        self.addSubview(view)
    }

}
