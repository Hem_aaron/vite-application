//
//  PaymentViewController.swift
//  Vite
//
//  Created by EeposIT_X on 6/26/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import Stripe
import ObjectMapper
import Braintree
import BraintreeDropIn


class PaymentViewController: WrapperController, STPPaymentCardTextFieldDelegate {
    
    let paymentTextField = STPPaymentCardTextField()
    @IBOutlet weak var payBtn: UIButton!
    @IBOutlet weak var lblCardNum: UILabel!
    @IBOutlet weak var viewPayment: UIView!
    @IBOutlet weak var btnChangeCard: UIButton!
    @IBOutlet weak var imgCard: UIImageView!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblPriceTable: UILabel!
    @IBOutlet weak var lblFeeTable: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var promoTextField: UITextField!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblPayWith: UILabel!
    //Views in Stack View
    @IBOutlet weak var promoCodeView: UIView!
    @IBOutlet weak var promoInfoView: UIView!
    @IBOutlet weak var discountView: UIView!
    @IBOutlet weak var subTotalView: UIView!
    @IBOutlet weak var feeView: UIView!
    @IBOutlet weak var btnProceedPaypal: UIButton!
    

    @IBOutlet weak var paymentViewHeightConstraint: NSLayoutConstraint!
    
    let indicator = ActivityIndicator()
    var eventDetail:VEvent?
    var isUserNotConnectedToStripe = false
    var isEligibleToPay = false
    var changeBtnToPayBtn = false
    var promoCodeAavilable = false
    var promoCodeVerified = false
    var isForPaypal = false
    var braintreeClient: BTAPIClient?
    override func viewDidLoad() {
        super.viewDidLoad();
        self.payBtn.isEnabled = false
        self.payBtn.isHidden = true
        self.lblCardNum.text = ""
        self.getStripeInfo()
        self.viewPayment.layer.cornerRadius = 5
        self.btnChangeCard.layer.cornerRadius = 5
        self.btnChangeCard.isHidden = true
        self.viewPayment.isHidden = true
        self.indicator.start()
        self.title = "Purchase Ticket"
        if let status = eventDetail?.promoCodeStatus {
            promoCodeAavilable = status
        }
        self.promoCodeView.isHidden = !promoCodeAavilable
        self.promoInfoView.isHidden = !promoCodeAavilable
        self.discountView.isHidden = !promoCodeAavilable
        self.subTotalView.isHidden = !promoCodeAavilable
        lblEventName.text = eventDetail?.eventTitle
        getPaymentInfo()
        getStripeAccountStatus()
        if isForPaypal {
            paypalLogic()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.indicator.stop()
    }
    
    @IBAction func sendPromoCode(_ sender: AnyObject) {
        if let promoCode = promoTextField.text{
            //remove white space
            let trimmedPromo = promoCode.trimmingCharacters(in: CharacterSet.whitespaces)
            
            if !trimmedPromo.isEmpty{
                self.indicator.start()
                VStripeWebService.getEventFeeWithPromoCode(trimmedPromo, (eventDetail?.uid)!, { (event) in
                    self.promoCodeVerified = true
                    self.promoTextField.isUserInteractionEnabled = false
                    self.lblDiscount.text = "$ " + String(event.discount!)
                    self.lblSubTotal.text = "$ " + String(event.subTotal!)
                    self.lblTotalAmount.text = "$ " + String(event.total!)
                    self.lblFeeTable.text = "$" + String(event.fee!)
                    self.indicator.stop()
                }, failureBlock: { (message) in
                    self.indicator.stop()
                    showAlert(controller: self, title: "", message: message)
                })
            }
        }
    }
    
    @IBAction func pay(_ sender: AnyObject?) {
        self.view.isUserInteractionEnabled = false
        if self.isEligibleToPay {
            self.indicator.start()
            self.userResponseToCard(eventDetail!, userResponse: .Requested)
        }else{
            if self.paymentTextField.cardNumber == nil {
                showAlert(controller: self, title: "", message: localizedString(forKey: "ENTER_YOUR_CARD_DETAILS"))
            }else{
                let cardParams = paymentTextField.cardParams
               
                #if DEVELOPMENT
                //For test only
                self.updateStripeToken(TestTokens[String(cardParams.last4()!)]!)
                self.indicator.start()
                
                #else
                STPAPIClient.shared().createToken(withCard: cardParams) { token, error in
                    guard let stripeToken = token else {
                        print(error)
                        return
                    }
                    self.updateStripeToken(String(describing: stripeToken))
                    self.indicator.start()
                }
                #endif
            }
            self.view.isUserInteractionEnabled = true
        }
    }
    

    
    func showStripeCard(){
        self.paymentTextField.frame = CGRect(x: 15, y: ScreenSize.SCREEN_HEIGHT - 120 , width: (self.view.frame).width - 30, height: 44)
        self.paymentTextField.delegate = self
        self.view.addSubview(self.paymentTextField)
    }
    
    func getPaymentInfo(){
        VStripeWebService.getEventFee((eventDetail?.uid)!, { (event) in
            self.indicator.stop()
            self.lblPriceTable.text = "$ " + String (event.price!)
            self.lblFeeTable.text = "$ " + String (event.fee!)
            self.lblTotalAmount.text = "$ " + String (event.total!)
            self.lblSubTotal.text = "$ " + String (event.price!)
        }) { (message) in
            self.indicator.stop()
            
            print(message)
            showAlert(controller: self, title: "", message: message)
        }
    }
    
    //parameter to check organizer's stripe status
    func getStripeAccountStatus(){
        VStripeWebService.checkStripeStatus({ (cardStatus) in
            if !cardStatus{
                self.btnChangeCard.setTitle("Pay", for: UIControlState())
                self.btnChangeCard.isHidden = false
            }
            else{
                self.changeBtnToPayBtn = false
            }
            
        }) { (message) in
            print(message)
        }
    }
    
    func getStripeInfo(){
        
        VStripeWebService.getStripeInfo({ (card) in
            if let cardLastFour = card.lastFour {
                self.viewPayment.isHidden = false
                self.lblCardNum.text = "*****" + cardLastFour
                self.btnChangeCard.isHidden = false
                self.payBtn.isHidden = false
                self.payBtn.isEnabled = true
                self.isEligibleToPay = true
            }
            else{
                self.showStripeCard()
            }
            
            if let cardType = card.cardType {
                self.imgCard.image = UIImage(named: "\(cardType)")
            }
            
        }) { (message) in
            self.indicator.stop()
            
            print(message)
            if message == localizedString(forKey: "NO_CARD_MSG"){
                self.viewPayment.isHidden = true
                self.showStripeCard()
                
            }
            else{
                showAlert(controller: self, title: "", message: message)
            }
        }
        
    }
    
    func updateStripeToken(_ token: String) {
        VStripeWebService.updateStripeToken(token: token, { (success) in
            let alert = UIAlertController(title: localizedString(forKey: "CONGRATULATIONS"), message: localizedString(forKey: "CARD_ADDED_SUCCESSFULLY"),preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK",
                                         style: .default,
                                         handler: {
                                            (action:UIAlertAction) -> Void in
                                            self.indicator.start()
                                            self.userResponseToCard(self.eventDetail!, userResponse: .Requested)
            })
            alert.addAction(okAction)
            self.present(alert,
                         animated: true,
                         completion: nil)
            self.indicator.stop()
        }) { (message) in
            showAlert(controller: self, title: "", message: message)
            self.indicator.stop()
        }
    }
    
    @IBAction func changeCard(_ sender: AnyObject) {
        
        if changeBtnToPayBtn {
            self.pay( nil )
        }else{
            let alert = UIAlertController(title: localizedString(forKey: "CHANGE_CARD"),
                                          message: localizedString(forKey: "PROCEED_WITH_NEWCARD"),
                                          preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Yes",
                                         style: .default,
                                         handler: {
                                            (action:UIAlertAction) -> Void in
                                            self.isEligibleToPay = false
                                            self.btnChangeCard.setTitle("Pay", for: UIControlState())
                                            self.changeBtnToPayBtn = true
                                            self.viewPayment.isHidden = true
                                            self.showStripeCard()
                                            
            })
            
            let cancelAction = UIAlertAction(title: "No",
                                             style: .cancel) {
                                                (action: UIAlertAction) -> Void in
            }
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            self.present(alert,
                         animated: false,
                         completion: nil)
            
        }
    }
    
    // Send request for event
    func userResponseToCard(_ event: VEvent, userResponse: UserViteResponse){
        
        if promoCodeVerified{
            VEventWebService.setUserResponseForEventPayment(event, userResponse: userResponse, successBlock: {
                self.view.isUserInteractionEnabled = true
                if userResponse == .Requested {
                    self.indicator.stop()
                    let evnt     = NEvent(name: event.title!, uid: event.uid!, imageUrl: event.imageUrl!.convertIntoViteURL())
                    let sender    = UserDefaultsManager.getActiveUserForNotification()
                    let receiver = NReceiver(uid: String(describing: event.organizer!.uid!))
                    NotificationService().triggerNotificationWithEvent(.RequestReceived, event: evnt, sender: sender, receiver: receiver)
                    
                    let alert = UIAlertController(title: "Congratulations", message: "Your payment is successful! ",preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK",
                                                 style: .default,
                                                 handler: {
                                                    (action:UIAlertAction) -> Void in
                                                    Route.goToHomeController()
                    })
                    alert.addAction(okAction)
                    self.present(alert,
                                 animated: true,
                                 completion: nil)
                    
                } else {
                    print("REJECTED")
                }
            }, failureBlock: { (message) in
                self.indicator.stop()
                print("Failed to respond to event: Error: \(message)")
            })
            
        }
        else {
            VEventWebService.setUserResponseForEvent(event, userResponse: userResponse, successBlock: {
                self.view.isUserInteractionEnabled = true
                if userResponse == .Requested {
                    self.indicator.stop()
                    let evnt     = NEvent(name: event.title!, uid: event.uid!, imageUrl: event.imageUrl!.convertIntoViteURL())
                    let sender    = UserDefaultsManager.getActiveUserForNotification()
                    let receiver = NReceiver(uid: String(describing: event.organizer!.uid!))
                    NotificationService().triggerNotificationWithEvent(.RequestReceived, event: evnt, sender: sender, receiver: receiver)
                    
                    let alert = UIAlertController(title: localizedString(forKey: "CONGRATULATIONS"), message: localizedString(forKey: "PAYMENT_SUCCESSFUL"),preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK",
                                                 style: .default,
                                                 handler: {
                                                    (action:UIAlertAction) -> Void in
                                                    Route.goToHomeController()
                    })
                    alert.addAction(okAction)
                    self.present(alert,
                                 animated: true,
                                 completion: nil)
                    
                } else {
                    print("REJECTED")
                }
            }, failureBlock: { (message) in
                self.indicator.stop()
                print("Failed to respond to event: Error: \(message)")
                self.view.isUserInteractionEnabled = true
            })
        }
    }
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        // Toggle navigation, for example
        payBtn.isEnabled = textField.isValid
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        //    self.navigationController!.popToRootViewController(animated: true)
        Route.goToHomeController()
    }
    
    //MARK: PAYPAL PAYMENT
    
    @IBAction func proceedPaypalPayment(_ sender: Any) {
        startCheckout()
    }
    
    func paypalLogic() {
        viewPayment.isHidden = true
        paymentViewHeightConstraint.constant = 0
        lblPayWith.isHidden = true
        lblCardNum.isHidden = true
        imgCard.isHidden = true
        btnProceedPaypal.isHidden = false
        btnChangeCard.isHidden = true
        paymentTextField.isHidden = true
    }
    
    func startCheckout() {
        self.indicator.start()
        self.view.isUserInteractionEnabled = false
        #if DEVELOPMENT
            braintreeClient = BTAPIClient(authorization: "sandbox_y55vhwdt_rh6yvbsrq8q26gvr")!
        #else
            braintreeClient = BTAPIClient(authorization: "production_4ptkhv79_v3h36x7d6mg5wzt5")!
        #endif
        let payPalDriver = BTPayPalDriver(apiClient: braintreeClient!)
        payPalDriver.viewControllerPresentingDelegate = self
        payPalDriver.appSwitchDelegate = self // Optional
        let request = BTPayPalRequest()
        request.billingAgreementDescription = "Your agremeent description" //Displayed in customer's PayPal account
        
        payPalDriver.requestBillingAgreement(request) { (tokenizedPayPalAccount, error) -> Void in
            self.indicator.stop()
            self.view.isUserInteractionEnabled = true
            if let tokenizedPayPalAccount = tokenizedPayPalAccount {
                print("Got a nonce: \(tokenizedPayPalAccount.nonce)")
                self.sendViteRequestPaypal(paymentNonce: tokenizedPayPalAccount.nonce, self.eventDetail!, userResponse: .Requested)
                print("Got a nonce: \(tokenizedPayPalAccount.nonce)")
                // Send payment method nonce to your server to create a transaction
            } else if let error = error {
                // Handle error here...
            } else {
                // Buyer canceled payment approval
            }
        }
    }
    
    func sendViteRequestPaypal(paymentNonce: String, _ event: VEvent, userResponse: UserViteResponse) {
        self.indicator.start()
        if promoCodeVerified {
            // if user enters promocode
            VEventWebService.setUserResponseForEventForPaypalWithPromo(nonce: paymentNonce, event, userResponse: userResponse, successBlock: {
                self.view.isUserInteractionEnabled = true
                if userResponse == .Requested {
                    self.indicator.stop()
                    let evnt     = NEvent(name: event.title!, uid: event.uid!, imageUrl: event.imageUrl!.convertIntoViteURL())
                    let sender    = UserDefaultsManager.getActiveUserForNotification()
                    let receiver = NReceiver(uid: String(describing: event.organizer!.uid!))
                    NotificationService().triggerNotificationWithEvent(.RequestReceived, event: evnt, sender: sender, receiver: receiver)
                    let alert = UIAlertController(title: localizedString(forKey: "CONGRATULATIONS"), message: localizedString(forKey: "PAYMENT_SUCCESSFUL"),preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK",
                                                 style: .default,
                                                 handler: {
                                                    (action:UIAlertAction) -> Void in
                                                    Route.goToHomeController()
                    })
                    alert.addAction(okAction)
                    self.present(alert,
                                 animated: true,
                                 completion: nil)
                    
                } else {
                    self.indicator.stop()
                    print("REJECTED")
                }
            }, failureBlock: { (message) in
                self.indicator.stop()
                print("Failed to respond to event: Error: \(message)")
                self.view.isUserInteractionEnabled = true
            })
        } else {
            // Normal payment without promocode
            VEventWebService.setUserResponseForEventForPaypal(nonce: paymentNonce, event, userResponse: userResponse, successBlock: {
                self.view.isUserInteractionEnabled = true
                if userResponse == .Requested {
                    self.indicator.stop()
                    let evnt     = NEvent(name: event.title!, uid: event.uid!, imageUrl: event.imageUrl!.convertIntoViteURL())
                    let sender    = UserDefaultsManager.getActiveUserForNotification()
                    let receiver = NReceiver(uid: String(describing: event.organizer!.uid!))
                    NotificationService().triggerNotificationWithEvent(.RequestReceived, event: evnt, sender: sender, receiver: receiver)
                    let alert = UIAlertController(title: localizedString(forKey: "CONGRATULATIONS"), message: localizedString(forKey: "PAYMENT_SUCCESSFUL"),preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK",
                                                 style: .default,
                                                 handler: {
                                                    (action:UIAlertAction) -> Void in
                                                    Route.goToHomeController()
                    })
                    alert.addAction(okAction)
                    self.present(alert,
                                 animated: true,
                                 completion: nil)
                    
                } else {
                    self.indicator.stop()
                    print("REJECTED")
                }
            }, failureBlock: { (message) in
                self.indicator.stop()
                print("Failed to respond to event: Error: \(message)")
                self.view.isUserInteractionEnabled = true
            })
        }
        
    }
}

extension PaymentViewController: BTAppSwitchDelegate, BTViewControllerPresentingDelegate {
    func appSwitcherWillPerformAppSwitch(_ appSwitcher: Any) {
        
    }
    
    func appSwitcher(_ appSwitcher: Any, didPerformSwitchTo target: BTAppSwitchTarget) {
        
    }
    
    func appSwitcherWillProcessPaymentInfo(_ appSwitcher: Any) {
        
    }
    
    func paymentDriver(_ driver: Any, requestsPresentationOf viewController: UIViewController) {
        present(viewController, animated: true, completion: nil)
    }
    
    func paymentDriver(_ driver: Any, requestsDismissalOf viewController: UIViewController) {
        dismiss(animated: true, completion: nil)
    }
}


