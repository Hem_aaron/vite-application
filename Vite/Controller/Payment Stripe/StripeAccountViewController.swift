//
//  StripeAccountViewController.swift
//  Vite
//
//  Created by EeposIT_X on 6/30/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import Stripe

class StripeAccountViewController: WrapperController,UIWebViewDelegate {
    
    let indicator = ActivityIndicator()
    var connectedWithStripe = false
    
    @IBOutlet weak var webView: UIWebView!
  
    #if DEVELOPMENT
    let stripeAccountForm = "https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_8j8X3AWtIRC0Rn55CHQ2PSCgXpUrM6E2&scope=read_write"
    #else
    let stripeAccountForm = "https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_8j8XLaNZ7pK1bmVzyVPrLJgNNXemQ0fv&scope=read_write"
    #endif
    
    override func viewDidLoad() {
        super.viewDidLoad();
        webView.delegate = self
        
        let url = URL(string: stripeAccountForm)
        webView.loadRequest(URLRequest(url: url!))
        
        if let nav = self.navigationController {
            nav.navigationBar.isTranslucent                  = false
            nav.view.backgroundColor                       = UIColor.white
        }
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.indicator.stop()
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.indicator.start()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        
        self.indicator.stop()
        let currentURL : NSString = (webView.request?.url!.absoluteString)! as NSString
        print(currentURL)
        
        if currentURL as String != stripeAccountForm{
            let url : URL = URL(string: currentURL as String)!
            print (currentURL)
            print(url.query)
            
            let params = url.query;
            let splitParams = params!.components(separatedBy: "=")
            let count = splitParams.count
            if splitParams[count - 2] == "read_write&code"{
                if connectedWithStripe == false{
                    self.connectedWithStripe = true
                    VStripeWebService.connectWithStripe(splitParams[count - 1], { (success) in
                        let alert = UIAlertController(title: localizedString(forKey: "CONGRATULATIONS"),
                                                      message: localizedString(forKey: "STRIPE_ACC_DONE"),
                                                      preferredStyle: .alert)
                        
                        let okAction = UIAlertAction(title: "OK",
                                                     style: .default,
                                                     handler: {
                                                        (action:UIAlertAction) -> Void in
                                                        self.navigationController?.popViewController(animated: true)
                                                        
                        })
                        
                        alert.addAction(okAction)
                        self.present(alert,
                                     animated: false,
                                     completion: nil)
                        
                        
                    }, failureBlock: { (message) in
                        showAlert(controller: self, title: "", message: message)
                    })
                    
                }
                
            }
        }
    }
    
    @IBAction func backBtn(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
}

