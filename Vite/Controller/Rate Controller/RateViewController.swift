//
//  RateViewController.swift
//  Vite
//
//  Created by Eeposit 01 on 6/1/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit
import Cosmos

class RateViewController: UIViewController {
  
  var eventForRate: [VEvent]?
  var ratingValue = String()
  let indicator = ActivityIndicator()
  
  @IBOutlet weak var blackView: UIView!
  @IBOutlet weak var popView: UIView!
  @IBOutlet weak var lblEventDetail: UILabel!
  @IBOutlet weak var cosmosView: CosmosView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setUpView()
    tapGestureSetUp()
    setUpComosView()
    showEventInPopUpView()
  }
  //MARK: Private Method
  func showEventInPopUpView() {
    guard let eventRate = eventForRate else {return}
    let event = eventRate.last
    lblEventDetail.text = "Let us Know how the '\(event!.eventTitle!)' went by rating " + (event!.organizer?.fullName)!
  }
  
  func setUpComosView() {
    cosmosView.settings.updateOnTouch = true
    cosmosView.didFinishTouchingCosmos = { rating in
      self.ratingValue = String(rating)
    }
  }
  
  func setUpView() {
    self.popView.layer.cornerRadius = 15
    self.popView.clipsToBounds = true
  }
  
  func tapGestureSetUp() {
    let tap = UITapGestureRecognizer(target: self, action: #selector(RateViewController.handelTapGesture))
    self.blackView.addGestureRecognizer(tap)
  }
  
    @objc func handelTapGesture(_ gestureRecognizer: UIGestureRecognizer) {
    dismiss(animated: true, completion: nil)
  }
  
  //MARK: Rating api call
  func eventRatingApiCall(_ eventId:String, parameter:[String:AnyObject]){
    VRateEventService.rateEvent(eventId, parameter: parameter, successBlock: { (successMsg) in
      self.indicator.stop()
      self.dismiss(animated: true, completion: nil)
    }) { (message) in
      self.indicator.stop()
    }
  }
  
  
  //MARK: UIButton Action
  @IBAction func noThanks(_ sender: AnyObject) {
    indicator.start()
    guard let event = eventForRate?.last else {return}
    guard let eventId = event.uid else {return}
    
    let parameter: [String:AnyObject] = [
      "rating" : "0" as AnyObject
    ]
    eventRatingApiCall(eventId , parameter: parameter)
  }
  
  @IBAction func saveRating(_ sender: AnyObject) {
    indicator.start()
    guard let event = eventForRate?.last else {return}
    guard let eventId = event.uid else {return}
    
    let parameter: [String:AnyObject] = [
      "rating" : self.ratingValue as AnyObject
    ]
   eventRatingApiCall(eventId, parameter: parameter)
  }
}

