//
//  RateViewController.swift
//  Vite
//
//  Created by Eeposit 01 on 6/1/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit
import Cosmos

class RateViewController: UIViewController, UITextViewDelegate {
  
  
  var eventForRate: [VEvent]?
  var myVite: MyVites?
  var isFromMyVite = false
  var ratingValue = String()
  let indicator = ActivityIndicator()
  
  @IBOutlet weak var lblPlaceHolder: UILabel!
  @IBOutlet weak var blackView: UIView!
  @IBOutlet weak var popView: UIView!
  @IBOutlet weak var lblEventDetail: UILabel!
  @IBOutlet weak var cosmosView: CosmosView!
  @IBOutlet weak var txtViewComment: UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if isFromMyVite {
        showMyViteInPopUpView()
    } else {
      showEventInPopUpView()
      setUpView()
    }
    txtViewComment.delegate = self
    tapGestureSetUp()
    setUpComosView()
   
  }
  //MARK: Private Method
  func showEventInPopUpView() {
    guard let eventRate = eventForRate else {return}
    let event = eventRate.last
    lblEventDetail.text = "Let us Know how the '\(event!.eventTitle!)' went by rating " + (event!.organizer?.fullName)!
  }
    
    func showMyViteInPopUpView() {
        guard let vite = myVite else {return}
        lblEventDetail.text = "Let us Know how the '\(vite.eventTitle!)' went by rating \(vite.fullName!)"
    }
  
  func setUpComosView() {
    cosmosView.settings.updateOnTouch = true
    cosmosView.didFinishTouchingCosmos = { rating in
      self.ratingValue = String(rating)
    }
  }
  
  func setUpView() {
    self.popView.layer.cornerRadius = 15
    self.popView.clipsToBounds = true
  }
  
  func tapGestureSetUp() {
    let tap = UITapGestureRecognizer(target: self, action: #selector(RateViewController.handelTapGesture))
    self.blackView.addGestureRecognizer(tap)
  }
  
    @objc func handelTapGesture(gestureRecognizer: UIGestureRecognizer) {
    dismiss(animated: true, completion: nil)
  }
  
  //MARK: Rating api call
  func eventRatingApiCall(eventId:String, parameter:[String:AnyObject]){
    VRateEventService.rateEvent(eventId, parameter: parameter, successBlock: { (successMsg) in
      self.indicator.stop()
         self.view.isUserInteractionEnabled = true
        self.dismiss(animated: true, completion: {
            if self.isFromMyVite{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getViteDetailFromServer"), object: nil)
            }
        })
    }) { (message) in
         self.view.isUserInteractionEnabled = true
      self.indicator.stop()
    }
  }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        lblPlaceHolder.isHidden = true
    }
  
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtViewComment.text.count == 0 {
            lblPlaceHolder.isHidden = false
        }
    }
  
  //MARK: UIButton Action
  @IBAction func noThanks(_ sender: AnyObject) {
    self.view.isUserInteractionEnabled = false
    guard let eventId = isFromMyVite ? myVite?.eventID : eventForRate?.last?.uid else {
        self.view.isUserInteractionEnabled = true
        return
    }
     indicator.start()
    let parameter: [String:AnyObject] = [
        "rating" : "0" as AnyObject,
        "comment" : trimCharacter(comment: txtViewComment.text)  as AnyObject
    ]
    eventRatingApiCall(eventId: eventId , parameter: parameter)
  }
  
  @IBAction func saveRating(_ sender: AnyObject) {
    self.view.isUserInteractionEnabled = false
    guard let eventId = isFromMyVite ? myVite?.eventID : eventForRate?.last?.uid else {
        self.view.isUserInteractionEnabled = true
        return
    }
    let comment = trimCharacter(comment: txtViewComment.text)
    if self.ratingValue.isEmpty || comment.isEmpty {
        showAlert(controller: self, title: localizedString(forKey: "REQUIRED"), message: localizedString(forKey: "PLEASE_RATE"))
         self.view.isUserInteractionEnabled = true
    } else {
         indicator.start()
        let parameter: [String:AnyObject] = [
            "rating" : self.ratingValue as AnyObject,
            "comment" : comment  as AnyObject
        ]
        eventRatingApiCall(eventId: eventId, parameter: parameter)
    }
  }
}

