 //
 //  ScanEventListViewController.swift
 //  Vite
 //
 //  Created by eeposit2 on 10/28/16.
 //  Copyright © 2016 EeposIT_X. All rights reserved.
 //
 
 import UIKit
 import ObjectMapper
 
 class ScanEventListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    let indicator = ActivityIndicator()
    var eventsList:[EventsOrganizer]? = [EventsOrganizer]()
    
    @IBOutlet weak var tblEventList: UITableView!
    let message = localizedString(forKey: "NO_EVENTS_TO_SCAN");
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getEventListForQr()
        tblEventList.dataSource = self
        tblEventList.delegate = self
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor =  UIColor.colorFromRGB(rgbValue: 0x71a1ed)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    
    @IBAction func back(_ sender: AnyObject) {
        Route.goToHomeController()
    }
    /*Table View Delegates*/
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let arr = self.eventsList {
            return arr.count
        } else {
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScanEventListCell") as! ScanEventTableViewCell
        let event =  self.eventsList![indexPath.row]
        cell.lblEventName.text = event.eventTitle?.uppercaseFirst
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let event =  self.eventsList![indexPath.row]
        goToScanner(event)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func goToScanner(_ event: EventsOrganizer){
        let controller = Route.scanTicketViewController
        controller?.event = event
        controller?.isFromSideMenuToScanTicket = true
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    
    func getEventListForQr(){
        self.indicator.start()
        // if businesss account is activated then get list of business events
        let url = UserDefaultsManager().isBusinessAccountActivated ?  kListBusinessEvents + UserDefaultsManager().userBusinessAccountID! : kAPIGetEventListsForQrScan
        
        WebService.request(method: .get, url: url!, success: { [unowned self] (response) -> Void in
            guard let response = response.result.value as! [String: AnyObject]? else {
                return
            }
            
            guard let success = response["success"] as? Bool else {
                return
                
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            if success {
                if let responseArray =  param["events"] as? [[String:AnyObject]] {
                    for obj in responseArray {
                        let event = Mapper<EventsOrganizer>().map(JSON: obj)
                        if let e = event {
                            self.eventsList?.append(e)
                        }
                    }
                    self.tblEventList.reloadData()
                    self.indicator.stop()
                }
            }}, failure: {
               [unowned self] (message, APIError) -> Void in
                self.view.makeToast(message: localizedString(forKey: "ERROR_UPDATING_EVENT_DISTANCE"))
            })
        }
        
 }
 
