//
//  ScanTicket.swift
//  Vite
//
//  Created by sajjan giri on 10/19/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class ScanTicketViewController: WrapperController ,AttendeeDelegate
{
    
    @IBOutlet weak var scannerContainer: UIView!
    @IBOutlet weak var attendeeContainer: UIView!
    var isFromMyEvents = Bool()
    var event: EventsOrganizer?
    var eventIdFromNotification: String?
    var isAfterQrScanned = Bool()
    var isFromSideMenuToScanTicket = Bool()
    let navToggle         = ToggleButton(frame: CGRect(x: 0,y: 0,width: 200,height: 34),isInRight: true)
   
    override func viewDidLoad() {
        super.viewDidLoad()
        if let attendeeCount = event?.attendeeCount{
            self.displayAttendeeInNavigation(Int(truncating: attendeeCount))
        }
        self.setupNavigation()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }

    @IBAction func backButton(_ sender: AnyObject) {
        if isFromMyEvents{
            self.navigationController?.popViewController(animated: true)
           
        }else {
            self.slideMenuController()?.changeMainViewController(Route.scanTicketNav, close: true)
    }
    }
    
    func instantiateScannerView() {
        self.attendeeContainer.isHidden = true
        self.scannerContainer.isHidden = false
    }
    
    func instantiateAttendeeView() {
        self.attendeeContainer.isHidden = false
        self.scannerContainer.isHidden = true
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "attendee" {
            let attendeeController = segue.destination as! AttendeesViewController
            attendeeController.delegate = self
            attendeeController.event = event
            // incase of if any user takes  screenshot, server provides eventId on pushnotification
            if let id = eventIdFromNotification{
                attendeeController.eventId = id
            }else if let id = event?.entityId {
                attendeeController.eventId = id
            }

        } else if segue.identifier == "scanner" {
            let scannerController = segue.destination as! ScannerViewController
            //scannerController.event = event
            //scannerController.isFromSideMenuToScanner = isFromSideMenuToScanTicket

        }
    }
    
    func setupNavigation() {
        navToggle.delegate                  = self
     
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.titleView       = navToggle
        self.navToggle.lblFirstHalf.text         = "Scanner"
        self.navToggle.lblSecondHalf.text       = "Checked In"
        // this code is executed if it is not viewed from side menu or is executed after qr is scanned
        if !isFromSideMenuToScanTicket || isAfterQrScanned{
            self.instantiateAttendeeView()
                   }else{
            instantiateScannerView()
            self.navToggle.toggleState(toRight: false)
        }
               self.navToggle.realignLabels()
        
    }

    
    // Delegate from attendeeListVc
    func displayAttendeeCount(_ count: Int) {
        displayAttendeeInNavigation(count)
    }
    
    func displayAttendeeInNavigation(_ count: Int){
        let attendeeView = UIView(frame: CGRect(x: 30, y: 10, width: 47, height: 25))
        attendeeView.layer.cornerRadius = 10
        attendeeView.clipsToBounds = true
        attendeeView.backgroundColor = UIColor.colorFromRGB(rgbValue: 0x09ADEC)
        let attendeeCountLabel = UILabel(frame: CGRect(x: 2, y: 2, width: 42, height: 21))
        attendeeCountLabel.font = attendeeCountLabel.font.withSize(11)
        attendeeCountLabel.textColor = UIColor.white
        attendeeCountLabel.text = "\(count)"
        attendeeCountLabel.textAlignment = NSTextAlignment.center
        attendeeView.addSubview(attendeeCountLabel)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: attendeeView)
    }
}

extension ScanTicketViewController: ToggleButtonDelegate {
    func toggleIndex(index:Int) {
        switch index {
        case 0:
            self.instantiateScannerView()
            break
        case 1:
            self.instantiateAttendeeView()
            break
        default:
            break
        }
    }
}
