//
//  ScannerViewController.swift
//  Vite
//
//  Created by sajjan giri on 10/19/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class ScannerViewController: UIViewController {
    /*
    //let scanner = QRCode()
    var event : EventsOrganizer?
    var isFromSideMenuToScanner = Bool()
    @IBOutlet weak var qrlblView: UIView!
    @IBOutlet weak var scannerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.qrlblView.layer.cornerRadius =  15
        self.qrlblView.clipsToBounds = true
        scan()
    }
    
    func scan () {
        //scanner.prepareScan(scannerView) { [unowned self] (stringValue) -> () in
          //  self.sendQRCodeToServer(stringValue)
        }
    }
    
    func sendQRCodeToServer(_ qrCode: String){
        
        guard let id = kevent?.entityId else{
            return
        }
        
        let param: [String: AnyObject] = [
            "qrCode" : qrCode as AnyObject
        ]
        WebService.request(method: .put, url: kAPIDecodeQrcode + "\(id)", parameter: param, header: nil, isAccessTokenRequired: true, success: {[unowned self] (response) in
            
            guard let response = response.result.value as? [String : AnyObject] else {
                return
            }
            
            guard let success = response["success"] as? Bool else{
                return
            }
            if success {
                 self.redirectToAttendee(self.event)
            }
        }
            , failure: {
              [unowned self]  (message, apiError) in
                
                showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: message)
               // self.scanner.clearDrawLayer()
                //self.scanner.stopScan()
                //self.scanner.startScan()
        })
        
        
        
        
        //        WebService.putAt(kAPIDecodeQrcode + "\(id)", parameters: param as [String : AnyObject], successBlock: { (response) in
        //
        //          guard let success = response!["success"] as? Bool else{
        //                return
        //            }
        //            if success {
        //                    self.redirectToAttendee(self.event)
        //
        //            }
        //
        //        }) { (message) in
        //             showAlert(self, title: "Error", message: message)
        //            self.scanner.clearDrawLayer()
        //            self.scanner.stopScan()
        //            self.scanner.startScan()
        //        }
    }
    
    
    
    func redirectToAttendee(_ event: EventsOrganizer?) {
        let controller = Route.scanTicketViewController
        controller?.event = event
        controller?.isAfterQrScanned = true
        controller?.isFromSideMenuToScanTicket = isFromSideMenuToScanner
        self.navigationController?.show(controller!, sender: nil)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //scanner.scanFrame = view.bounds
        // start scan
        //scanner.startScan()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
 */
    
}
