//
//  ScanEventTableViewCell.swift
//  Vite
//
//  Created by eeposit2 on 10/28/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class ScanEventTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblEventName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
