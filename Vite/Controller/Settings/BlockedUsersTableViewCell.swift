//
//  BlockedUsersTableViewCell.swift
//  Vite
//
//  Created by sajjan giri on 12/13/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class BlockedUsersTableViewCell: UITableViewCell {
    
    var unblockAction:(() -> ())?

    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func btnUnblockUser(_ sender: AnyObject) {
        if let unblockAction = self.unblockAction {
            unblockAction();
        }

        
    }
}
