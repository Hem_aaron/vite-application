//
//  BlockedUsersViewController.swift
//  Vite
//
//  Created by sajjan giri on 12/13/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class BlockedUsersViewController: WrapperController , UITableViewDelegate , UITableViewDataSource{
    
    var blockedUsersList : [VUser]?
    let indicator = ActivityIndicator()
    var fbId : String!
    
    @IBOutlet weak var lblNoBlockedUser: UILabel!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getBlockedUsersList()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getBlockedUsersList () {
        indicator.start()
        VProfileWebService.getBlockedUserList(successBlock: { [unowned self] (blockedUsers) in
            self.blockedUsersList = blockedUsers
            self.hideShowNoUserListLbl()
            for fbid in self.blockedUsersList! {
                self.fbId = String(describing: fbid.uid)
            }
            self.indicator.stop()
            self.tableView.reloadData()
            
        }) { [unowned self] (message) in
            showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: message)
            self.indicator.stop()
        }
        
    }
    
    func unBlockedUser(_ fbId:String, index: Int, userFullName: String) {
        self.view.isUserInteractionEnabled = false
        let parameter: [String: AnyObject] = [
            
            "fbIdList" : [fbId] as AnyObject
        ]
        self.indicator.start()
        VSettingsWebService.unblockUser(param: parameter, { [unowned self] (msg) in
            self.indicator.stop()
            self.view.makeToast(message: "\(userFullName) has been unblocked successfully")
            self.blockedUsersList?.remove(at: index)
            self.hideShowNoUserListLbl()
            self.tableView.reloadData()
        }) { [unowned self] (message) in
            self.indicator.stop()
            showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: message)
            self.view.isUserInteractionEnabled = true

        }
   }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let blockedUsers = self.blockedUsersList {
            return blockedUsers.count
       }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let blockedUsers = blockedUsersList![indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "blockUsersCell") as! BlockedUsersTableViewCell
        cell.userName.text = blockedUsers.fullName
        if let profileImg = blockedUsers.profileImageUrl{
            cell.userImg.sd_setImage(with: URL(string: (profileImg).convertIntoViteURL()))
        }
        
        cell.unblockAction = {
            [unowned self] in 
            if let fbId = blockedUsers.uid {
                self.unBlockedUser(String(describing: fbId),index: indexPath.row, userFullName: blockedUsers.fullName!)
            }
       }
        return cell
    }
    
    @IBAction func back(_ sender: AnyObject) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    // hide table view and show no block user label if there are no blocked user in the list.
    func hideShowNoUserListLbl(){
        if self.blockedUsersList?.count == 0 {
            self.tableView.isHidden = true
            self.lblNoBlockedUser.isHidden = false
        }else{
            self.tableView.isHidden = false
            self.lblNoBlockedUser.isHidden = true
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
