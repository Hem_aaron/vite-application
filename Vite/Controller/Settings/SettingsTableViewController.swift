//
//  SettingsTableViewController.swift
//  Vite
//
//  Created by eeposit2 on 2/21/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SlideMenuControllerSwift

class SettingsTableViewController: WrapperTableViewController {
    
    @IBOutlet weak var sliderLbl: UILabel!
    @IBOutlet weak var distanceSlider: UISlider!
    @IBOutlet weak var viteNowSwitch: UISwitch!
    @IBOutlet weak var pushNotificationSwitch: UISwitch!
    let viteNowKey = "kViteNowState"
    let pushNotificationKey = "kPushNotificationState"
    let indicator = ActivityIndicator()
    
    @IBOutlet weak var btnLogOut: UIButton!
    var userLoggedOut: Bool = false
    var termsAndCondnSection = 2
    var privacySection = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //get distanceSlider value from user default
        viteNowSwitch.setOn(UserDefaults.standard.bool(forKey: viteNowKey), animated: false)
        pushNotificationSwitch.setOn(UserDefaults.standard.bool(forKey: pushNotificationKey), animated: false)
        getPushNotificationServiceCall()
        self.navigationItem.title = "Settings"
        btnLogOut.layer.borderWidth = 2.0
        btnLogOut.layer.borderColor = UIColor.purple.cgColor
        self.navigationController?.navigationBar.isTranslucent = false
        serviceCallGetEventVisibleDistance()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //print(NSUserDefaults.standardUserDefaults().integerForKey("slider_value"))
        let sliderValue = UserDefaults.standard.integer(forKey: "slider_value")
        if sliderValue == 0 {
            sliderLbl.text = "\(50)" +  " miles"
            self.distanceSlider.setValue(50, animated: true)
        }else{
            sliderLbl.text = "\(sliderValue)" +  " miles"
            self.distanceSlider.setValue(Float(sliderValue), animated: true)
        }
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        if(!self.userLoggedOut){
            serviceCallSetEventVisibleDistance()
        }
    }
    
    @IBAction func backToHome(_ sender: AnyObject) {
        Route.goToHomeController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logOut(_ sender: AnyObject)
    {
        let alert = UIAlertController(title: localizedString(forKey: "ARE_YOU_SURE"),
                                      message: "",
                                      preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel",
                                     style: .default,
                                     handler: { (action:UIAlertAction) -> Void in
                                        })
        let okAction = UIAlertAction(title: "Log Out",
                                         style: .default) { (action: UIAlertAction) -> Void in
                                            self.indicator.start()
                                            self.serviceCallLogOut()
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        present(alert,
                              animated: false,
                              completion: nil)
        
        
    }
    
    func serviceCallLogOut(){
        VSettingsWebService.logOut({ (msg) in
            self.indicator.stop()
            self.userLoggedOut = true
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
            FBSDKLoginManager().logOut()
            AppState.sharedInstance.popOverShown = false
            UIApplication.shared.applicationIconBadgeNumber = 0
            let controller = Route.signUpViewController
            SlideMenuController().changeMainViewController(controller, close: false)
            AppDel.delegate?.window??.rootViewController = controller
        }) { (msg) in
            self.indicator.stop()
            showAlert(controller: self, title: "", message: msg)
        }
    }
   
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == termsAndCondnSection{
            let controller = Route.termsConditionAndPrivacy
            controller.isFromSettings = true
            controller.url =  kAPITermsAndConditionUrl
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if indexPath.section == privacySection{
            let controller = Route.termsConditionAndPrivacy
            controller.isFromSettings = true
            controller.url =  kAPIPrivacyPolicyUrl
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 7
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
            return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }   
    
    @IBAction func viteNowNotification(_ sender: UISwitch) {
        updatePushNotificationServiceCall()
    }
    
    @IBAction func push(_ sender: UISwitch) {
        updatePushNotificationServiceCall()
    }
    
    func updatePushNotificationServiceCall(){
        VSettingsWebService.updatePushNotification(param: pushNotificationParameter(), { (msg) in
    
        }) { (msg) in
            self.view.makeToast(message: localizedString(forKey: "UPDATE_ERROR"))
        }
    }
    
    func pushNotificationParameter() -> [String: AnyObject]{
        let userDef                         = UserDefaults.standard
        userDef.set(pushNotificationSwitch.isOn, forKey: pushNotificationKey)
        userDef.set(viteNowSwitch.isOn, forKey: viteNowKey)
        var parameter:[String: AnyObject]   = [String: AnyObject]()
        parameter["viteNow"]                = (viteNowSwitch.isOn ? true : false) as AnyObject
        parameter["pushNotificationStatus"] = (pushNotificationSwitch.isOn ? "ALL" : "NONE") as AnyObject
        return parameter
    }
    
    
    func getPushNotificationServiceCall(){
        VSettingsWebService.getPushNotification({ (pushNotificationDetail) in
             let userDef = UserDefaults.standard
            if let status = pushNotificationDetail.pushNotificationStatus {
                if status == "ALL" {
                    self.pushNotificationSwitch.setOn(true, animated: false)
                    userDef.set(true, forKey: self.pushNotificationKey)
                } else {
                    self.pushNotificationSwitch.setOn(false, animated: false)
                    userDef.set(false, forKey: self.pushNotificationKey)
                }
            }
            if let viteNow = pushNotificationDetail.viteNow {
                if viteNow {
                    self.viteNowSwitch.setOn(true, animated: false)
                    userDef.set(true, forKey: self.viteNowKey)
                } else{
                    self.viteNowSwitch.setOn(false, animated: false)
                    userDef.set(false, forKey: self.viteNowKey)
                }
            }
            
        }) { (msg) in
        }
    }
    
    //When sliderValue is changed this method is called
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        self.sliderLbl.text = "\(currentValue)" + " miles"
        UserDefaults.standard.set(currentValue, forKey: "slider_value")
    }
    
    /* slider parameter */
    func sliderParameter() -> [String: AnyObject]{
        let parameter = [
            "facebookId" : UserDefaultsManager().userFBID!,
            "eventVisibleDistance": UserDefaults.standard.integer(forKey: "slider_value")
        ] as [String : Any]
        return parameter as [String : AnyObject]
    }
    
    
    //Api to set event visible distance
    func  serviceCallSetEventVisibleDistance(){
        VSettingsWebService.setEventDistance(param: sliderParameter(), { (msg) in
        }) { (msg) in
            self.view.makeToast(message: localizedString(forKey: "ERROR_UPDATING_EVENT_DISTANCE"))
        }
    }
    
    //Api to get event visible distance
    func serviceCallGetEventVisibleDistance(){
        VSettingsWebService.getEventVisibleDistance({ (distanceDetail) in
            if let sliderValue = distanceDetail.eventVisibleDistance, sliderValue != 0  {
                self.distanceSlider.setValue(Float(sliderValue), animated: true)
                self.sliderLbl.text = "\(sliderValue)" + " miles"
                UserDefaults.standard.set(sliderValue, forKey: "slider_value")
            } else {
                self.distanceSlider.setValue(50, animated: true)
                self.sliderLbl.text = "\(50)" + " miles"
                UserDefaults.standard.set(50, forKey: "slider_value")
            }
        }) { (msg) in
            self.view.makeToast(message: localizedString(forKey: "ERROR_UPDATING_EVENT_DISTANCE"))
        }
    }
}
