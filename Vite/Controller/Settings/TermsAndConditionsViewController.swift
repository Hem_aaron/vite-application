//
//  TermsAndConditionsViewController.swift
//  Vite
//
//  Created by eeposit2 on 2/23/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit



class TermsAndConditionsViewController: UIViewController, UIWebViewDelegate{
    var isFromSettings :Bool = false
    var url = String()
    var indicator = ActivityIndicator()
    
    @IBOutlet weak var termWebView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let webLink = URL(string: url)
        let requestObj = URLRequest(url: webLink!)
        termWebView.loadRequest(requestObj)
    }
    
    @IBAction func closeTermsAndConditions(_ sender: AnyObject) {
        if isFromSettings{
            self.navigationController!.popViewController(animated: true)
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getHtmltxtinLabel()  {
        
        let url = Bundle.main.url(forResource: "terms", withExtension:"html")
        let request = URLRequest(url: url!)
        termWebView.loadRequest(request)
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.indicator.start()
    }
    
    func  webViewDidFinishLoad(_ webView: UIWebView) {
        self.indicator.stop()
    }
    
}
