//
//  SignUpViewController.swift
//  Vite
//
//  Created by Hem Poudyal on 12/20/17.
//  Copyright © 2017 Eepos IT. All rights reserved.
//

import UIKit
import MapKit
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import Firebase
import FirebaseAuth

class SignUpViewController: UIViewController {
    var window : UIWindow?
    var fbResponse:FBLoginResponse!
    var userLocation:CLLocationCoordinate2D?
    var isFromCelebrityLogin = false
    var isFromEliteLogin = false
    var locationHelper = LocationManager()
    
    @IBOutlet var aIndicator: UIActivityIndicatorView!
    @IBOutlet weak var SignInBtn: UIButton!
    @IBOutlet weak var viewForIndicator: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationHelper.getUserLocation()
        self.locationHelper.fetchedUserLocation =
            {
                location in
                self.userLocation = location
                UserDefaultsManager().userCurrentLongitude = String(describing: self.userLocation!.longitude)
                UserDefaultsManager().userCurrentLatitude = String(describing: self.userLocation!.latitude)
        }
        
       
    }
    //MARK: UIButton Method
    @IBAction func signUpWithFacebook(_ sender: Any) {
        isFromEliteLogin = false
        isFromCelebrityLogin = false
        initiateFacebookLogin()
    }
    
    @IBAction func celebrityLoginWithFacebook(_ sender: Any) {
        isFromCelebrityLogin = true
        isFromEliteLogin = false
        initiateFacebookLogin()
    }
    
    @IBAction func eliteLoginWithFacebook(_ sender: Any) {
        isFromEliteLogin = true
        isFromCelebrityLogin = false
        initiateFacebookLogin()
    }
}
//MARK: Private Method
extension SignUpViewController {
    
    func initiateFacebookLogin(){
        self.aIndicator.startAnimating()
        FacebookLoginService.login(controller: self) {
            (status, response, token, id) -> Void in
            guard status else {
                self.aIndicator.stopAnimating()
                let message = "Facebook Login failed! Response:\(String(describing: response))"
                ErrorLogService.saveErrorWithCode(code: ErrorCode.FBLoginFailure, message: ErrorLogService.getCodeLog(message: message), completionBlock:nil)
                return
            }
            self.fbResponse = FBLoginResponse.mapper(dictionary: response)
            self.fbResponse.fbID = id
            self.fbResponse.accessToken = token
            self.fbResponse.description()
            
            let credential = FIRFacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
            
            FIRAuth.auth()?.signIn(with: credential) { (user, error) in
                if error != nil {
                    print("Firebase authentication error")
                    let message = "Firebase authentication error Login failed! Response:\(String(describing: error?.localizedDescription))"
                    ErrorLogService.saveErrorWithCode(code: ErrorCode.FBLoginFailure, message: ErrorLogService.getCodeLog(message: message), completionBlock:nil)
                } else {
                    self.loginToServer()
                }
            }
        }
    }
    
    func loginToServer() {
        self.getprofileImageListParameters() {
            (userImages) -> Void in
            self.initiateRequestToServer(userImageUrl: userImages)
        }
    }
    
    func getprofileImageListParameters(completionBlock :@escaping (_ userImages: [[String: AnyObject]]?) -> Void ){
        var imageParameter = [[String : AnyObject]]()
        FacebookPhotoHelper.getFacebookPhoto(
            successBlock: {
                (imageList) -> Void in
                for i in 0  ..< imageList!.count  {
                    let imagesDictionary  =
                        [
                            "profileImageUrl": imageList![i],
                            "imageType": "FB"
                    ]
                    imageParameter.append(imagesDictionary as [String : AnyObject])
                }
                completionBlock(imageParameter)
                
        }) {
            (message) -> Void in
            ErrorLogService.saveErrorWithCode(code: ErrorCode.FBPhotoFetchFailure,
                                              message: ErrorLogService.getCodeLog(message: message),
                                              completionBlock: nil)
            
            completionBlock(nil)
        }
    }   
    
    func initiateRequestToServer(userImageUrl : [[String:AnyObject]]?) {
        
        self.getSignLoginAPIParameter { (param) in
            var parameter = param
            if let images = userImageUrl {
                parameter!["userImages"] = images
            }
            // Facebook login request
            WebService.request(method: .post, url: kAPIFbLogin, parameter: parameter! as [String : AnyObject], header: nil,isAccessTokenRequired: false ,success: { (response) in
                let headers = response.response?.allHeaderFields
                let accessToken = headers?["accessToken"]
                let premiumStatus = headers?["premiumStatus"]
                let celebVerificationStatus = headers?["celebrityVerificationStatus"]
                let eliteVerificationStatus = headers?["eliteVerificationStatus"]
                if(accessToken != nil){
                    let user = UserDefaultsManager()
                    if let premiumStat = premiumStatus {
                        user.userPremiumStatus = (premiumStat as? Bool)
                    }
                    user.userAccessToken = String(describing: accessToken!)
                    if let celebStatus = celebVerificationStatus, let eliteStatus = eliteVerificationStatus {
                        user.celebVerificationStatus = String(describing: celebStatus)
                        user.eliteVerificationStatus = String(describing: eliteStatus)
                    }
                }
                self.aIndicator.stopAnimating()
                UserDefaultsManager.saveUserFBResponse(fbResponse: self.fbResponse)
                guard let response = response.value as? [String:AnyObject],
                    let status = response["success"] as? Bool else {
                        ErrorLogService.saveErrorWithCode(code: ErrorCode.LoginPostRequestFailure,
                                                          message: ErrorLogService.getCodeLog(message: "Invalid response"),
                                                           completionBlock: nil)
                        return
                }
                
                if status {
                    UserDefaultsManager.saveUserFriendsToUserDefaults()
                    guard let _ = self.userLocation else {
                    let controller = Route.noLocationViewController
                    self.show(controller, sender: nil)
                      return
                   }
                    self.enterHomeScreen()
                }
              }) { (messageString, error) in
                self.aIndicator.stopAnimating()
                showAlert(controller: self, title: "", message: messageString)
            }
        }
    }
    
    
    func enterHomeScreen() {
        let user = UserDefaultsManager()
        if self.isFromCelebrityLogin {
            if let eliteStatus = user.eliteVerificationStatus{
                // User cannot apply for elite and celebrity at the same time
                if eliteStatus == "null" {
                    if let status = user.celebVerificationStatus {
                        if status  == "null" {
                            // the status is set to null if the user have not sent email for celeb verification
                            self.performSegue(withIdentifier: "CelebrityEliteLoginSegue", sender: nil)
                        } else if status == "false" {
                            // the status is set to false if the user have sent email for celeb verification but is not verified
                            showAlert(controller: self, title: localizedString(forKey: "LOGIN_FAILED"), message: localizedString(forKey: "NOT_VERFIED_AS_CELEBRITY") )
                        } else {
                            // the status is set to true if the user have sent email for celeb verification and is  verified
                            user.loginSuccessStatus = true
                            showRootVC()
                           // self.performSegue(withIdentifier: "LoginSegue", sender: nil)
                        }
                    }
                } else {
                          showAlert(controller: self, title:localizedString(forKey: "CELEBRITY_LOGIN_FAILED"), message: localizedString(forKey: "ELITE_VERIFICATION"))
                }
            } else {
                
            }
        } else if self.isFromEliteLogin{
            if let celebStatus = user.celebVerificationStatus{
                // User cannot apply for elite and celebrity at the same time
                if celebStatus == "null" {
                    if let status = user.eliteVerificationStatus {
                        if status  == "null" {
                            // the status is set to null if the user have not sent email for celeb verification
                            self.performSegue(withIdentifier: "CelebrityEliteLoginSegue", sender: nil)
                        } else if status == "false" {
                            // the status is set to false if the user have sent email for celeb verification but is not verified
                        
                            showAlert(controller: self, title: localizedString(forKey: "LOGIN_FAILED"), message: localizedString(forKey: "LOGIN_WITH_FACEBOOK"))
                        } else {
                            // the status is set to true if the user have sent email for celeb verification and is  verified
                            user.loginSuccessStatus = true
                            showRootVC()
                            //self.performSegue(withIdentifier: "LoginSegue", sender: nil)
                        }
                    }
                    
                } else {
                    showAlert(controller: self, title: localizedString(forKey: "ELITE_LOGIN_FAILED"), message: localizedString(forKey: "ELITE_LOGIN_FAILED_MSG"))
                }
            }
        }
        else {
            // this userdefault is set to check if the user has succedded on signing from facebook however this value is set to true  if the user sign in successfully.
            user.loginSuccessStatus = true
            showRootVC()
           // self.performSegue(withIdentifier: "LoginSegue", sender: nil)
        }
    }
        
        
    
    func getSignLoginAPIParameter(completionBlock:(_ parameter:[String:Any]?) -> Void) {
        
        //If user location is extracted send that location ot the server
        // Else send (0,0) since the location is made mandatory in the API.
        
        var location = [String:String]()
        if let loc = self.userLocation {
            location =
                [
                    "latitude":String(loc.latitude),
                    "longitude":String(loc.longitude)
            ]
            
        } else {
            location =
            [
                    "latitude":"23.0",
                    "longitude":"23.0"
            ]
        }
        var userDevice = [String: AnyObject]()
        if let uid = UIDevice.current.identifierForVendor?.uuidString{
            userDevice = [ "uuid": uid as AnyObject,
                           "family":UIDevice.current.systemName as AnyObject,
                               "brand":UIDevice.current.modelName as AnyObject ]
        }
        // If we can get the image with the facebook URL then send parameter with facebook image
        // Else send parameter without facebook image.
        
        let profileImgUrl = "http://graph.facebook.com/\(self.fbResponse.fbID!)/picture?type=large"
        let parameter:[String:Any] =
            [
                "facebookId": self.fbResponse.fbID ?? ""  ,
                "fbToken": self.fbResponse.accessToken ?? "",
                "email" : self.fbResponse.email ?? "",
                "dateOfBirth": self.fbResponse.birthday ?? "",
                "gender" : self.fbResponse.gender ?? "",
                "info": self.fbResponse.aboutMe ?? "",
                "fullName":self.fbResponse.fullName ?? "",
                "occupation": self.fbResponse.work ?? "",
                "profileImageUrl":profileImgUrl,
                "userDevice": [userDevice],
                "location": location,
                "celebrity": isFromCelebrityLogin ? true : false,
                "facebookURL": self.fbResponse.fbURL ?? ""
        ]
        print(parameter)
        completionBlock(parameter)
    }
   
    func showRootVC()  {
        self.view.endEditing(true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.configureInitialViewController()
    }
}

