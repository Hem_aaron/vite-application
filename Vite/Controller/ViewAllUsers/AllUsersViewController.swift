//
//  AllUsersViewController.swift
//  Vite
//
//  Created by EeposIT_X on 5/18/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

protocol AllUserMangaer {
    
    func getViteUsers(_ page: Int, searchString: String, successBlock: @escaping (([ViteUser], _ friendListInPrivateFriendModel: [PrivateFriend]) -> ()), failureBlock: ((String) -> ()))
  }

class AllUsersViewController: WrapperController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate, UIGestureRecognizerDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblNoUsers: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    let viteUserDataController:AllUserMangaer = AllViteUsersService()
    fileprivate var _viteUserArr = [ViteUser]()
    var userInfo: UserInfo!
    var noUser = false
    
    var searchTextGlobal = String()
    var pageGlobal = Int()
    
    var viteUserArr:[ViteUser] {
        get {
            return self._viteUserArr
        } set {
            //self.lblNoVIP.hidden = newValue.count == 0 ? false : true
            self._viteUserArr = newValue
        }
    }
    func message(_ selectedUser:ViteUser) {
        let chatBoard = UIStoryboard(name: "Chat", bundle: nil)
        
        let chatController      = chatBoard.instantiateViewController(withIdentifier: "ChatMainViewController") as! ChatMainViewController
        let sender             = ChatUser()
        sender.fbID            =  UserDefaultsManager().isBusinessAccountActivated ?  UserDefaultsManager().userBusinessAccountID! :  UserDefaultsManager().userFBID!
        sender.name            =   UserDefaultsManager().isBusinessAccountActivated ?  UserDefaultsManager().userBusinessFullName! :  UserDefaultsManager().userFBFullName!
        sender.profileImageUrl =  UserDefaultsManager().isBusinessAccountActivated ?  UserDefaultsManager().userBusinessProfileImageUrl :"http://graph.facebook.com/\(UserDefaultsManager().userFBID!)/picture?type=large"
        
        let receiver             = ChatUser()
        receiver.fbID            = String(describing: selectedUser.facebookId!)
        
        print(receiver.fbID)
        receiver.name            = selectedUser.fullName!
        receiver.profileImageUrl = "http://graph.facebook.com/\(receiver.fbID!)/picture?type=large"

        chatController.receiver = receiver
        chatController.sender   = sender
        self.navigationController?.show(chatController, sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.tableView.tableFooterView = UIView()
        //self.lblNoVIP.text = message
        searchBar.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
          //swipe back
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
    }
    
    func setupNavigationBar() {
        if let nav = self.navigationController {
            nav.navigationBar.isTranslucent = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationBar()
        
    }
    
    @IBAction func backToHome(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Table View Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //  return friendsArr.count
        
        if viteUserArr.count == 0 || searchTextGlobal.isEmpty {
            return 0
        }
        return viteUserArr.count + 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let viteUserData           = self.viteUserArr[indexPath.row]
        message(viteUserData)
    }
    
   func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return (indexPath.row == viteUserArr.count) && noUser ? 0 : 60

    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.row == viteUserArr.count) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "loadMoreCell") as! LoadMoreTableViewCell
            
            cell.isHidden = noUser ? true : false
            return cell
        } else{
            let viteUserData           = self.viteUserArr[indexPath.row]
            
            let cell              = tableView.dequeueReusableCell(withIdentifier: "VipListsCell") as! VipListsTableViewCell
            cell.lblFullName.text = viteUserData.fullName
            let url = viteUserData.profileImgUrl?.convertIntoViteURL()
            cell.imgProfile.sd_setImage(with: URL(string: url!), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
            return cell
            
        }
    }
    
    
    
    @IBAction func loadMoreCell(_ sender: AnyObject) {
        
        
        self.pageGlobal += 1
        self.viteUserDataController.getViteUsers(pageGlobal,searchString: searchTextGlobal,
                                                 successBlock:{ (viteusers , friendListInPrivateFriendModel) in
                                                    if viteusers.count < 10{
                                                        self.noUser = true
                                                    }
                                                    self.viteUserArr.append(contentsOf: viteusers)
                                                    self.tableView.reloadData()
            },
                                                 failureBlock:{ (message) in
                                                    
                                                    showAlert(controller: self, title: "", message: message)
        })
        
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        self.viteUserArr.removeAll()
        self.tableView.reloadData()
    }
    
    
    //MARK: - Search Bar Delegates
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        
        // let predicate = NSPredicate(format: "self CONTAINS[cd] %@",searchText)
         pageGlobal = 1
        self.noUser = true
        let escapedString = searchText.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        searchTextGlobal = escapedString!
        if escapedString!.isEmpty  {
            self.viteUserArr.removeAll()
            self.tableView.reloadData()
        } else {
            self.viteUserDataController.getViteUsers(pageGlobal,searchString: escapedString!,
                                                     successBlock:{ (viteusers, friendListInPrivateFriendModel) in
                                                        if viteusers.count < 10{
                                                             self.noUser = true
                                                        }else{
                                                            self.noUser = false
                                                        }
                                                        self.viteUserArr = viteusers
                                                        self.tableView.reloadData()
                },
                                                     failureBlock:{ (message) in
                                                        
                                                        showAlert(controller: self, title: "", message: message)
            })
            
        }
        
    }
    
}
