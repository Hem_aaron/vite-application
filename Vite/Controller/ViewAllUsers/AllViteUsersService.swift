//
//  AllViteUsersService.swift
//  Vite
//
//  Created by EeposIT_X on 5/18/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper

struct AllViteUsersService:AllUserMangaer {
    
    func getViteUsers(_ page: Int, searchString: String, successBlock: @escaping (([ViteUser],_ friendListInPrivateFriendModel: [PrivateFriend]) -> ()), failureBlock: ((String) -> ())) {
        WebService.request(method: .get, url: kAPISearchViteUsers + "\(page)/10/\(searchString)/" + "\(UserDefaultsManager().userFBID!)", parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
           
            guard let response = response.result.value as? [String: AnyObject],
                            let success = response["success"] as? Bool,
                            let param = response["params"] as? [String:AnyObject], success == true
                            else {
                                return
                        }
            
                        if let responseArray =  param["users"] as? [[String:AnyObject]] {
                            let viteUsers = Mapper<ViteUser>().mapArray(JSONArray: responseArray)
                             let filterViteUser = responseArray.flatMap(PrivateFriend.init)
                            successBlock(viteUsers, filterViteUser)
                        }
        }) { (message, apiError) in
          
            
        }
    }
    
    
    func getViteUsersForVip(_ page: Int , searchString:String, successBlock: @escaping (([ViteUser]) ->()), failureBlock:((String) -> ())) {
        
        var businessUrl = String()
        if let businessId = UserDefaultsManager().userBusinessAccountID{
               businessUrl = KAPISearchUsersForBusinessVip + "/\(businessId)/" + "\(page)/10/\(searchString)/"
        }
        let userUrl = kAPISearchViteUsersForVip + "\(page)/10/\(searchString)/" + "\(UserDefaultsManager().userFBID!)"
        let url = UserDefaultsManager().isBusinessAccountActivated ? businessUrl : userUrl
        
        WebService.request(method: .get, url: url, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String: AnyObject],
                            let success = response["success"] as? Bool,
                            let param = response["params"] as? [String:AnyObject], success == true
                            else {
                                return
                        }
            
                        if let responseArray =  param["users"] as? [[String:AnyObject]] {
                            let viteUsers = Mapper<ViteUser>().mapArray(JSONArray: responseArray)
                            successBlock(viteUsers)
                        }
        }) { (message, apiError) in
            
        }
        
    }
}
