//
//  WalkthroughCollectionViewCell.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 11/17/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class WalkthroughCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var wtImgView: UIImageView!
}
