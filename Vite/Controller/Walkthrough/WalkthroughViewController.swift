//
//  WalkthroughViewController.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 11/17/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class WalkthroughViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var walkthroughCollectionView: UICollectionView!
    var walkthroughArr = ["wt_intro", "wt_events", "wt_people", "wt_vip","wt_paid","wt_payment"]
    let defaults = UserDefaults.standard
     var window: UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnStart.layer.cornerRadius = 10
        btnStart.clipsToBounds = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func startVite(_ sender: Any) {
        let controller = Route.signUpViewController
        self.show(controller, sender: nil)
        defaults.set(true , forKey: "walkthroughSeen")
    }
    
    //MARK: COLLECTION VIEW DELEGATE
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return walkthroughArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let wtImg = walkthroughArr[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WalkthroughCVCell", for: indexPath) as! WalkthroughCollectionViewCell
        cell.wtImgView.image = UIImage(named: wtImg)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height:  self.view.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
         pageControl.currentPage = indexPath.item
    }
}
