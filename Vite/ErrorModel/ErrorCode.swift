//
//  ErrorCode.swift
//  Vite
//
//  Created by Hem Poudyal on 12/21/17.
//  Copyright © 2017 Eepos IT. All rights reserved.
//

import Foundation

struct ErrorCode {
    
    //MARK: Signin Errors
    static let FBLoginFailure = "100"
    static let FirebaseLoginFailure = "101"
    static let FBPhotoFetchFailure = "102"
    static let LoginPostRequestFailure = "103"
}
