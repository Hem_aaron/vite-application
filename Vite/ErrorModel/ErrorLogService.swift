//
//  ErrorLogService.swift
//  Vite
//
//  Created by Hem Poudyal on 12/21/17.
//  Copyright © 2017 Eepos IT. All rights reserved.
//

import FirebaseDatabase
import FirebaseAuth
import Firebase
import Foundation


struct ErrorLogService {
    
    static func getCodeLog(message: String,
                           function: String = #function,
                           file: String = #file,
                           line: Int = #line) -> String {
        return "Message \"\(message)\" (File: \(file), Function: \(function), Line: \(line))"
    }
    
    static var rootRef:FIRDatabaseReference = FIRDatabase.database().reference()
    
    static func saveErrorWithCode(code:String, message:String, completionBlock:((_ status:Bool) -> Void)?){
        let errorData =
            [
                "message":message,
                "timestamp": FIRServerValue.timestamp()
                ] as [String : Any]
        self.rootRef.child("verrors/\(code)").childByAutoId().setValue(errorData) {
            (error, ref) in
            if error != nil {
                print(error ?? "")
            } else {
                print(ref)
            }
            
        }
    }
}

