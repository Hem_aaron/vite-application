//
//  AuthenticationService.swift
//  Vite
//
//  Created by Eeposit1 on 6/26/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation
import FirebaseAuth
import Firebase
import FBSDKLoginKit
import FBSDKCoreKit

typealias SuccessBlock = (_ response:AnyObject) -> ()


protocol AuthenticationService {
    func login(_ email:String, password:String, success:SuccessBlock, failure:FailureBlock)
    func register(_ email:String, password:String, successBlock:SuccessBlock)
    func loginWithFacebook(_ controller:UIViewController, successBlock:SuccessBlock)
    func getCurrentUser()
    func getUserProfile()
    func updateUserProfile()
    func setUserEmail()
    func setUserPassword()
    func sendPasswordResetEmail(_ email:String)
    func deleteCurrentUser()
    func logout()
}

/*
struct FirebaseAuthenticationService:AuthenticationService {
    //Login Existing user
    func login(email:String, password:String, success:SuccessBlock, failure:FailureBlock) {
        FIRAuth.auth()?.signInWithEmail(email, password: password) {
            (user, error) in
            if error != nil {
                failure(message:"Login error \(error?.description)")
                return
            }
            success(response:user!)
            
            
        }
    }
    
    //Create new user
    
    func register(email:String, password:String, successBlock:SuccessBlock) {
        FIRAuth.auth()?.createUserWithEmail(email, password: password) {
            (user, error) in
            if error != nil {
                print("Error creating user login")
                return
            }
            successBlock(response: user!)
        }
    }
    
    func loginWithFacebook(controller:UIViewController, successBlock:SuccessBlock) {
        
        FacebookLoginHelper.login(controller) {
            (status, response, token, id) -> Void in
            guard status else {
                print("Facebook login failed!")
                return
            }
            
            let credential = FIRFacebookAuthProvider.credentialWithAccessToken(FBSDKAccessToken.currentAccessToken().tokenString)
            
            FIRAuth.auth()?.signInWithCredential(credential) { (user, error) in
                successBlock(response: response!)
            }
            
        }
        
    }
    
    func getCurrentUser() {
        FIRAuth.auth()?.addAuthStateDidChangeListener { auth, user in
            if let user = user {
                // User is signed in.
            } else {
                // No user is signed in.
            }
        }
    }
    
    func getUserProfile() {
        if let user = FIRAuth.auth()?.currentUser {
            let name = user.displayName
            let email = user.email
            let photoUrl = user.photoURL
            let uid = user.uid;  // The user's ID, unique to the Firebase project.
            // Do NOT use this value to authenticate with
            // your backend server, if you have one. Use
            // getTokenWithCompletion:completion: instead.
        } else {
            // No user is signed in.
        }
    }
    
    func getProviderSpecificUserData() {
        if let user = FIRAuth.auth()?.currentUser {
            for profile in user.providerData {
                let providerID = profile.providerID
                let uid = profile.uid;  // Provider-specific UID
                let name = profile.displayName
                let email = profile.email
                let photoURL = profile.photoURL
            }
        } else {
            // No user is signed in.
        }
    }
    //PhotoUrl displayName
    func updateUserProfile() {
        let user = FIRAuth.auth()?.currentUser
        if let user = user {
            let changeRequest = user.profileChangeRequest()
            
            changeRequest.displayName = "Jane Q. User"
            changeRequest.photoURL =
                NSURL(string: "https://example.com/jane-q-user/profile.jpg")
            changeRequest.commitChangesWithCompletion { error in
                if let error = error {
                    // An error happened.
                } else {
                    // Profile updated.
                }
            }
        }
        
    }
    
    /*
     To update email users should be logged in recently
     */
    
    func setUserEmail() {
        let user = FIRAuth.auth()?.currentUser
        
        user?.updateEmail("user@example.com") { error in
            if let error = error {
                // An error happened.
            } else {
                // Email updated.
            }
        }
    }
    
    
    /*
     To update password users should be logged in recently
     */
    func setUserPassword() {
        let user = FIRAuth.auth()?.currentUser
        let newPassword = getRandomSecurePassword()
        
        user?.updatePassword(newPassword) { error in
            if let error = error {
                // An error happened.
            } else {
                // Password updated.
            }
        }
    }
    
    func sendPasswordResetEmail(email:String) {
        
        FIRAuth.auth()?.sendPasswordResetWithEmail(email) { error in
            if let error = error {
                // An error happened.
                print("Coudlnt' reset password")
            } else {
                // Password reset email sent.
                print("Email sent")
            }
        }
    }
    
    func deleteCurrentUser() {
        let user = FIRAuth.auth()?.currentUser
        
        user?.deleteWithCompletion { error in
            if let error = error {
                // An error happened.
            } else {
                // Account deleted.
            }
        }
    }
    
    
    /*
     func reauthenticateUser() {
     let user = FIRAuth.auth()?.currentUser
     var credential: FIRAuthCredential
     
     
     // Prompt the user to re-provide their sign-in credentials
     
     user?.reauthenticateWithCredential(credential) { error in
     if let error = error {
     // An error happened.
     } else {
     // User re-authenticated.
     }
     }
     }
     
     */
    
    func getRandomSecurePassword() -> String {
        return "randomNewPassword"
    }
    
    
    func logout() {
        try! FIRAuth.auth()!.signOut()
    }
    
}
 */
