//
//  ChatHistoryService.swift
//  Vite
//
//  Created by Eeposit1 on 6/19/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import ObjectMapper
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ChatHistoryService {
    var normalChatHistory = [GroupChatMessageModel]()
    var groupChatHistory = [GroupChatMessageModel]()
    var combinedChatHistory = [GroupChatMessageModel]()
    var isDataLoadedOnce = false
    var flag = 0
    var failureFlag = 0
    var timer:Timer!
    
    func getChatHistory(_ userId:String, completionBlock:@escaping ((_ status:Bool, _ chatHistoryList:[GroupChatMessageModel]?) -> ())) {
        FNode.messagesMetaNode.child(userId).queryOrdered(byChild: LMKey.timestamp).observe(.value, with: {
            snapshot in
            var groupChatMessageHistory = [GroupChatMessageModel]()
            if let historyValues = snapshot.value as? [String:AnyObject] {
                for (_,value) in historyValues {
                    if let groupMeta = Mapper<GroupChatMessageModel>().map(JSON: value as! [String : Any]){
                        groupChatMessageHistory.append(groupMeta)
                    }
                }
                groupChatMessageHistory.sort(by: {$0.timeString?.doubleValue > $1.timeString?.doubleValue})
                
                completionBlock(true, groupChatMessageHistory)
            } else {
                completionBlock(true, [GroupChatMessageModel]())
            }
        })
    }
    
    func deleteChatHistory(_ sender:String, receiver:String) {
        let node = "\(sender)/\(receiver)"
        FNode.messagesNode.child(node).setValue(nil)
        FNode.messagesMetaNode.child(node).setValue(nil)
    }
    
    
    
    func getMergedChatHistoryForCurrentUser(_ completionBlock:@escaping ((_ status:Bool, _ chatHistoryList:[GroupChatMessageModel]?) -> ())) {
        let id = UserDefaultsManager().isBusinessAccountActivated ?UserDefaultsManager().userBusinessAccountID :UserDefaultsManager().userFBID
        ChatHistoryService().getChatHistory(id!) { [](status, chatHistoryList) in
            if status {
                self.normalChatHistory = chatHistoryList!
                self.combinedChatHistory = self.normalChatHistory + self.groupChatHistory
            } else {
                self.failureFlag = self.failureFlag + 1
            }
            self.flag = self.flag + 1
            if self.flag == 2   || self.isDataLoadedOnce {
                self.isDataLoadedOnce = true
                self.combinedChatHistory.sort(by: {$0.timeString?.doubleValue > $1.timeString?.doubleValue})
                completionBlock(true, self.combinedChatHistory)
            } else if self.failureFlag == 2 {
                self.failureFlag = 0
                completionBlock(false, nil)
            } else {
                print("TEST")
            }

        }
        
        GroupChatService.getChatHistory(id!) { (status, groupChatHistoryList) in
            if status {
                self.groupChatHistory = groupChatHistoryList!
                self.combinedChatHistory = self.normalChatHistory + self.groupChatHistory
            } else {
                self.failureFlag = self.failureFlag + 1
            }
            self.flag = self.flag + 1
            
            if self.flag == 2   || self.isDataLoadedOnce {
                self.isDataLoadedOnce = true
                self.combinedChatHistory.sort(by: {$0.timeString?.doubleValue > $1.timeString?.doubleValue})
                completionBlock(true, self.combinedChatHistory)
            } else if self.failureFlag == 2 {
                self.failureFlag = 0
                completionBlock(false, nil)
            } else {
                print("TEST")
            }
        }
    }
}
