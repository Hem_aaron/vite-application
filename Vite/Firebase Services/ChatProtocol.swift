//
//  ChatProtocol.swift
//  Vite
//
//  Created by Eeposit1 on 6/20/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//
protocol ChatControllerServiceProtocol {
    init(sender:ChatUser, receiver:ChatUser, delegate:ChatServiceDelegate?)
    func sendMessage(_ message:MessageModel)
    func sendBroadcastMessage(_ message:MessageModel)
    func deleteMessage(_ message:MessageModel)
    func senderIsWritingMessage()
    func updateLastMessageCount()
}

protocol ChatServiceDelegate {
    func messageAdded(_ message:MessageModel)
    func messageDeleted(_ message:MessageModel)
    func receiverIsWritingMessage()
    func receiverHasGoneOffline()
    func receiverHasComeOnline()
    func messageIsDelivered(_ message:MessageModel)
    func messageFailedToDeliver()
}

protocol GroupChatServiceProtocol {
    init(delegate:GroupChatServiceDelegate?)
    func sendMessage(_ message:MessageModel)
    func deleteMessage(_ message:MessageModel)
}

protocol GroupChatServiceDelegate {
    func messageAdded(_ message:GroupChatMessageModel)
}
