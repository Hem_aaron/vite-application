//
//  ChatService.swift
//  Vite
//
//  Created by Hem Poudyal on 6/17/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth
import Firebase

class ChatService:ChatControllerServiceProtocol {
    var sender:ChatUser
    var receiver:ChatUser
    var delegate:ChatServiceDelegate?
    var refHandle:FIRDatabaseHandle!
    
    required init(sender:ChatUser, receiver:ChatUser, delegate:ChatServiceDelegate?) {
        self.sender = sender
        self.receiver = receiver
        self.delegate = delegate
        self.setObserverForMessageAdded()
    }
    
    deinit {
        FNode.messagesNode.removeObserver(withHandle: self.refHandle)
    }
    
    func sendMessage(_ message:MessageModel) {
        //Nodes
        var mVar = message
        let m:[String:AnyObject] = [
            MKey.message : message.message as AnyObject,
            MKey.timestamp : FIRServerValue.timestamp() as AnyObject,
            MKey.senderId  : message.sender.fbID! as AnyObject,
            MKey.receiverId : message.receiver.fbID! as AnyObject ,
            MKey.isMedia    : message.isMedia as AnyObject,
            MKey.mediaImageUrl : message.mediaImageUrl as AnyObject,
            MKey.mediaVideoUrl : message.mediaVideoUrl as AnyObject,
            MKey.isVideo       : message.isVideo as AnyObject
        ]
        
        var flag:Int = 0 //Flag value will be 2 when the message save is successful in both senderNode and receiverNode
        //Save in sender node
       
        self.senderMessagesNode.childByAutoId().setValue(m, withCompletionBlock: {
            (error, ref) in
            mVar.uniqueId = ref.key
            flag = flag + 1
            if error == nil {
                self.saveSenderMeta(mVar)
            }
            
            if flag == 2 {
                self.delegate?.messageIsDelivered(mVar)
            }
        })
        
        //Save in receiver node
        self.receiverMessagesNode.childByAutoId().setValue(m, withCompletionBlock: {
            (error, ref) in
            
            flag = flag + 1
            mVar.uniqueId = ref.key
            if error == nil {
                self.saveReceiverMeta(mVar)
                
            }
            
            if flag == 2 {
                self.delegate?.messageIsDelivered(mVar)
            }
        })
        
        self.sendPushNotificationToReceiverIfHeIsOffline(message)
    }
    
    func sendBroadcastMessage(_ message:MessageModel) {
        //Nodes
        var mVar = message
        let m:[String:AnyObject] = [
            MKey.message : message.message as AnyObject,
            MKey.timestamp : FIRServerValue.timestamp() as AnyObject,
            MKey.senderId  : message.sender.fbID! as AnyObject,
            MKey.receiverId : message.receiver.fbID! as AnyObject
        ]
        
        var flag:Int = 0 //Flag value will be 2 when the message save is successful in both senderNode and receiverNode
        //Save in sender node
        
        self.senderMessagesNode.childByAutoId().setValue(m, withCompletionBlock: {
            (error, ref) in
            mVar.uniqueId = ref.key
            flag = flag + 1
            if error == nil {
                //self.saveSenderMeta(mVar)
            }
            
            if flag == 2 {
                self.delegate?.messageIsDelivered(mVar)
            }
        })
        
        //Save in receiver node
        self.receiverMessagesNode.childByAutoId().setValue(m, withCompletionBlock: {
            (error, ref) in
            
            flag = flag + 1
            mVar.uniqueId = ref.key
            if error == nil {
                self.saveReceiverMeta(mVar)
                
            }
            
            if flag == 2 {
                self.delegate?.messageIsDelivered(mVar)
            }
        })
        
        self.sendPushNotificationToReceiverIfHeIsOffline(message)
    }
    
    func sendPushNotificationToReceiverIfHeIsOffline(_ message:MessageModel) {
        func send() {
            let parameter =
                [
                    "fbIdList":[message.receiver.fbID!],
                    "message": message.sender.name + ": " + message.message
            ] as [String : AnyObject]
            
            WebService.request(method: .post, url: kAPISendPushNotification, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
            }) { (message, apiError) in
            }
        }
        
        FNode.presenceNode.child(message.receiver.fbID!).observeSingleEvent(of: .value, with: {
            snapshot in
            if snapshot.exists() {
                if let presence = snapshot.value as? Bool, presence == true {
                    
                } else {
                    send()
                }
                
            } else {
                send()
            }
        })
    }
    
    func senderIsWritingMessage() {
        
    }
    
    func updateLastMessageCount() {
        self.senderMetaMessagesNode.observeSingleEvent(of: .value, with: {
            snapshot in
            if let _ = snapshot.value as? [String:AnyObject] {
                self.senderMetaMessagesNode.updateChildValues([LMKey.unseenCount:0])
            }
        })
    }
    
    func deleteMessage(_ message:MessageModel) {
        
        //Delete chat data the user selected
        self.senderMessagesNode.child(message.uniqueId).setValue(nil,withCompletionBlock: {
            (error, ref) in
            //Should delete chat meta too but condition is should have deleted the message node data first
            //Query the last message from messages node and if the snapshot exists update the metanode accordingly
            //Else set meta value to nil
            
            self.senderMessagesNode.queryLimited(toLast: 1).observeSingleEvent(of: .value, with: { (snapshot) -> Void in
                if snapshot.exists() {
                    
                    guard let lastMessageDictionary = snapshot.value as? [String:AnyObject] else {
                        print("Format of the last messages might be incorrect!!!")
                        return
                    }
                    
                    for (key,value) in lastMessageDictionary {
                        
                        if message.uniqueId != key {
                            return
                        }
                        
                        let meta:[String:AnyObject] = [
                            
                            "lastMessage" : value["message"] as! String as AnyObject,
                            "time" : String(describing: value["time"] as! NSNumber) as AnyObject,
                            "unseenCount" : 0 as AnyObject,
                            "messageID" : key as AnyObject
                        ]
                        self.senderMetaMessagesNode.setValue(meta)
                    }
                    
                } else {
                    self.senderMetaMessagesNode.setValue(nil)
                }
            })
        })
    }
}


//MARK: Nodes
extension ChatService {
    
    var senderMessagesNode:FIRDatabaseReference {
        get {
            return FNode.messagesNode.child("\(sender.fbID!)/\(receiver.fbID!)")//"
        }
    }
    
    var receiverMessagesNode:FIRDatabaseReference {
        get {
            return FNode.messagesNode.child("\(receiver.fbID!)/\(sender.fbID!)")
            
        }
    }
    
    var senderMetaMessagesNode:FIRDatabaseReference {
        get {
           return FNode.messagesMetaNode.child("\(sender.fbID!)/\(receiver.fbID!)")
        }
    }
    
    var receiverMetaMessagesNode:FIRDatabaseReference {
        get {
        return FNode.messagesMetaNode.child("\(receiver.fbID!)/\(sender.fbID!)")
        }
    }
    
}
//MARK:  Private Methods
extension ChatService {
    fileprivate func saveSenderMeta(_ message:MessageModel) {
        
        let meta:[String:AnyObject] = [
            LMKey.message : message.message as AnyObject,
            LMKey.timestamp: FIRServerValue.timestamp() as AnyObject,
            LMKey.unseenCount : 0 as AnyObject,
            LMKey.messageID: message.uniqueId as AnyObject,
            LMKey.userProfileURL :message.receiver.profileImageUrl as AnyObject,
            LMKey.userDisplayName : message.receiver.name as AnyObject,
            LMKey.userFBID : message.receiver.fbID!  as AnyObject
        ]
        
        self.senderMetaMessagesNode.setValue(meta)
    }
    
    fileprivate func saveReceiverMeta(_ message:MessageModel) {
        
        var meta:[String:AnyObject] = [
            LMKey.message : message.message as AnyObject,
            LMKey.timestamp : FIRServerValue.timestamp() as AnyObject,
            LMKey.messageID : message.uniqueId as AnyObject,
            LMKey.userProfileURL : message.sender.profileImageUrl as AnyObject,
            LMKey.userDisplayName : message.sender.name as AnyObject,
            LMKey.userFBID  : message.sender.fbID! as AnyObject
            
        ]
        NotificationService().updateNotificationMetaNode(message.receiver.fbID, notificationType: .UnseenMessages)
        
        self.receiverMetaMessagesNode.observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
            //If user had set the message meta before get the unseeCount
            if let messageMeta = snapshot.value as? [String : AnyObject] {
                let unseenValue = messageMeta[LMKey.unseenCount] as! Int
                meta[LMKey.unseenCount] = unseenValue + 1 as AnyObject
                
            } else {
                meta[LMKey.unseenCount] = 1 as AnyObject
            }
            
            self.receiverMetaMessagesNode.setValue(meta)
        })
    }
    
    fileprivate func setObserverForMessageAdded() {
        //If the user has viewed the messages then set unseen count to 0
        refHandle = self.senderMessagesNode.observe(.childAdded, with: { (snapshot) in
            let messageValue = snapshot.value as! [String:AnyObject]
            
            let timestamp    = String(describing: messageValue[MKey.timestamp] as! NSNumber)
            let message      = messageValue[MKey.message] as! String
            let senderId     = messageValue[MKey.senderId] as! String
            let isMedia      = messageValue[MKey.isMedia] as? Bool ?? false
            let isVideo      = messageValue[MKey.isVideo] as? Bool ?? false
            let mediaImageUrl = messageValue[MKey.mediaImageUrl] as? String ?? ""
            let mediaVideoUrl = messageValue[MKey.mediaVideoUrl] as? String ?? ""
            
            //let receiverId   = messageValue[MKey.receiverId] as! String
            
            var messageSender = ChatUser()
            var messageReceiver = ChatUser()
            
            if senderId == self.sender.fbID {
                messageSender = self.sender
                messageReceiver = self.receiver
            } else {
                messageSender = self.receiver
                messageReceiver = self.sender
            }
            
            let m = MessageModel(
                sender: messageSender,
                receiver: messageReceiver,
                message:message,
                timestamp:timestamp ,
                uniqueId: snapshot.key,
                mediaImageUrl:mediaImageUrl,
                mediaVideoUrl: mediaVideoUrl,
                isMedia: isMedia,
                isVideo: isVideo
            )
            self.delegate?.messageAdded(m)
        })
    
        /*
        refHandle = self.senderMessagesNode.observeEventType(.ChildChanged, withBlock: { (snapshot) -> Void in
            print(snapshot)
            let messageValue = snapshot.value as! [String:AnyObject]
            
            print(messageValue)
            let timestamp    = String(messageValue[MKey.timestamp] as! NSNumber)
            let message      = messageValue[MKey.message] as! String
            let senderId     = messageValue[MKey.senderId] as! String
            let isMedia      = messageValue[MKey.isMedia] as? Bool ?? false
            let isVideo      = messageValue[MKey.isVideo] as? Bool ?? false
            let mediaImageUrl = messageValue[MKey.mediaImageUrl] as? String ?? ""
            let mediaVideoUrl = messageValue[MKey.mediaVideoUrl] as? String ?? ""
            
            //let receiverId   = messageValue[MKey.receiverId] as! String
            
            var messageSender = ChatUser()
            var messageReceiver = ChatUser()
            
            if senderId == self.sender.fbID {
                messageSender = self.sender
                messageReceiver = self.receiver
            } else {
                messageSender = self.receiver
                messageReceiver = self.sender
            }
            
            let m = MessageModel(
                sender: messageSender,
                receiver: messageReceiver,
                message:message,
                timestamp:timestamp ,
                uniqueId: snapshot.key,
                mediaImageUrl:mediaImageUrl,
                mediaVideoUrl: mediaVideoUrl,
                isMedia: isMedia,
                isVideo: isVideo
            )
            
            self.delegate?.messageAdded(m)
            
        })
 */
    }
}

