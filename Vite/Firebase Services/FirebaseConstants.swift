//
//  FirebaseConstants.swift
//  Vite
//
//  Created by Eeposit1 on 6/20/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//
import FirebaseDatabase
import FirebaseAuth
import Firebase
struct MKey {
    static let senderId   = "senderId"
    static let receiverId = "receiverId"
    static let message    = "message"
    static let timestamp  = "timestamp"
    static let uniqueId   = "uniqueId"
    static let mediaImageUrl  = "mediaImageUrl"
    static let mediaVideoUrl  = "mediaVideoUrl"
    static let isMedia    = "media"
    static let isVideo    = "video"
}

struct GroupChatKey {
    static let senderId   = "userid"
    static let message    = "message"
    static let timestamp  = "timestamp"
    static let uniqueId   = "uniqueId"
}

struct LMKey {
    static let message         = "message"
    static let timestamp       = "timestamp"
    static let messageID       = "messageID"
    static let unseenCount     = "unseenCount"
    static let userDisplayName = "userDisplayName"
    static let userProfileURL  = "userProfileURL"
    static let userFBID        = "userFBID"
}

struct NotificationKey {
    static let uid            = "uid"
    static let type           = "type"
    static let seenStatus     = "seenStatus"
    static let timestamp      = "timestamp"
    
    static let senderUid      = "senderUid"
    static let senderFullName = "senderFullName"
    static let senderImageUrl = "senderImageUrl"
    
    static let eventUid       = "eventUid"
    static let eventName      = "eventName"
    static let eventImageUrl  = "eventImageUrl"
}

struct NotificationMetaKey {
    static let viteCount    = "viteCount"
    static let messageCount = "messageCount"
    static let eventCount   = "eventCount"
}

struct FNode {
    
    #if DEVELOPMENT
    static var rootRef:FIRDatabaseReference = FIRDatabase.database().reference().child("test")
    #else
    static var rootRef:FIRDatabaseReference = FIRDatabase.database().reference()
    #endif
    
    
    static var userTypingNode:FIRDatabaseReference {
        return self.rootRef.child("typingIndicator")
    }
    static var messagesNode:FIRDatabaseReference {
        return self.rootRef.child("messages")
    }
    
    static var messagesMetaNode:FIRDatabaseReference {
        return self.rootRef.child("messagesMeta")
    }
    
    static var presenceNode:FIRDatabaseReference {
        return self.rootRef.child("presence")
    }
    
    static var groupChatNode:FIRDatabaseReference {
        return self.rootRef.child("groupChat")
    }
    
    static var groupMetaNode:FIRDatabaseReference {
        return self.rootRef.child("groupMeta")
    }

    
    static var notificationNode:FIRDatabaseReference {
        return self.rootRef.child("notifications")
    }
    
    static var forceUpdateNode:FIRDatabaseReference {
        return self.rootRef.child("forceUpdateIOS")
    }
    
    
    
    static var notificationMetaNode:FIRDatabaseReference {
        return self.rootRef.child("notificationsMeta")
    }
    
    //Latest location update from user's device
    static var lastUpdatedLocation:FIRDatabaseReference {
        return self.rootRef.child("userLocation")
    }
    
    /**
     TEST NOTIFIATION NODES
     **/
    
    static var iosTest:FIRDatabaseReference = FIRDatabase.database().reference().child("testios")
    
    static var testNotificationNode:FIRDatabaseReference {
        return self.iosTest.child("notifications")
    }
    
    static var testNotificationMetaNode:FIRDatabaseReference {
        return self.iosTest.child("notificationsMeta")
    }
}
