//
//  GroupChatService.swift
//  Vite
//
//  Created by Prajeet Shrestha on 2/16/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
import ObjectMapper

class  GroupChatService {
    var delegate:GroupChatServiceDelegate?
    var refHandle:FIRDatabaseHandle!
    var event:NEvent
    
    static func sendMessage(_ message:GroupChatMessageModel, event:NEvent, usersInGroup:[String]) {
        let msg = message
        let node = FNode.groupChatNode.child(event.uid)
        var users = usersInGroup
        users.append(message.userFBID!)
        
      //  print(message.toJSON())
        node.childByAutoId().setValue(message.toJSON(), withCompletionBlock: {
            (error, ref) in
            if error != nil {
                print(error?.localizedDescription)
            }
            msg.messageID = ref.key
            updateGroupMetaWithEvent(msg, event: event, usersInGroup:users)
        })
        for user in usersInGroup {
            if user == UserDefaultsManager.getCurrentChatUser().fbID! {
                continue
            }
            sendPushNotificationToReceiverIfHeIsOffline(message, userId: user)
        }
        
    }
    
    static func sendPushNotificationToReceiverIfHeIsOffline(_ message:GroupChatMessageModel, userId:String) {
        func send() {
            let parameter =
                [
                    "fbIdList":[userId],
                    "message": message.userDisplayName! + ": " + message.message!
            ] as [String : AnyObject]
            WebService.request(method: .post, url: kAPISendPushNotification, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
                
            }) { (message, error) in
                
            }
            
        }
        
        FNode.presenceNode.child(message.userFBID!).observeSingleEvent(of: .value, with: {
            snapshot in
            if snapshot.exists() {
                if let presence = snapshot.value as? Bool, presence == true {
                    
                } else {
                    send()
                }
                
            } else {
                send()
            }
        })
    }
    
    static func deleteChatHistoryForEvent(_ eventId:String, userId:String) {
        FNode.groupMetaNode.child(userId).child(eventId).setValue(nil)
    }
    
    func updateLastMessageCount(_ userId:String) {
        let node = FNode.groupMetaNode.child(userId).child(event.uid)
        node.observeSingleEvent(of: .value, with: {
            snapshot in
            if let _ = snapshot.value as? [String:AnyObject] {
                node.updateChildValues([LMKey.unseenCount:0])
            }
        })
    }
    
    static func updateGroupMetaWithEvent( _ message:GroupChatMessageModel, event:NEvent, usersInGroup:[String]) {
        
        for user in usersInGroup {
            let node = FNode.groupMetaNode.child(user).child(event.uid)
            let msg           = message
            msg.eventName     = event.name
            msg.eventImageUrl = event.imageUrl
            
            node.observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                //If user had set the message meta before get the unseeCount
                let id =  UserDefaultsManager().isBusinessAccountActivated ? UserDefaultsManager().userBusinessAccountID : UserDefaultsManager().userFBID
                if user != id {
                    if let messageMeta = snapshot.value as? [String : AnyObject] {
                        let unseenValue = messageMeta[LMKey.unseenCount] as! Int
                        msg.unseenCount = unseenValue + 1
                    } else {
                        msg.unseenCount = 1
                    }
                } else {
                    msg.unseenCount = 0
                }
                node.setValue(msg.toJSON())
            })
        }
    }
    
    //static func getGroupMessages()
    
    required init(delegate:GroupChatServiceDelegate?, event:NEvent) {
        self.delegate = delegate
        self.event = event
        self.setObserverForMessageAdded(event.uid)
    }
    
    deinit {
        FNode.groupChatNode.removeObserver(withHandle: self.refHandle)
    }
    
    fileprivate func setObserverForMessageAdded(_ eventId:String) {
        //If the user has viewed the messages then set unseen count to 0
        let node = FNode.groupChatNode.child(eventId)
        
        refHandle = node.observe(.childAdded, with: { (snapshot) -> Void in
            let messageValue = snapshot.value as! [String:AnyObject]
            let model = Mapper<GroupChatMessageModel>().map(JSON: messageValue)
            self.delegate?.messageAdded(model!)
        })
    }
    
    static func getChatHistory(_ userId:String, completionBlock:@escaping ((_ status:Bool, _ groupChatHistoryList:[GroupChatMessageModel]?) -> ())) {
        FNode.groupMetaNode.child(userId).queryOrdered(byChild: LMKey.timestamp).observe(.value, with: {
            snapshot in
            var groupChatMessageHistory = [GroupChatMessageModel]()
            if let historyValues = snapshot.value as? [String:AnyObject] {
                for (k,value) in historyValues {
                    if let groupMeta = Mapper<GroupChatMessageModel>().map(JSON: value as! [String : Any]) {
                        let groupChatMeta = groupMeta
                        groupChatMeta.eventId = k
                        if let _ = groupChatMeta.eventName {
                            groupChatMessageHistory.append(groupChatMeta)
                        }
                       
                    }
                }
                groupChatMessageHistory.sort(by: {($0.timeString?.intValue)! > ($1.timeString?.intValue)!})
                
                completionBlock(true, groupChatMessageHistory)
            } else {
                completionBlock(true, [GroupChatMessageModel]())
            }
        })      }
    
    static func deleteChatHistory(_ sender:String, receiver:String) {
        let node = "\(sender)/\(receiver)"
        FNode.messagesNode.child(node).setValue(nil)
        FNode.messagesMetaNode.child(node).setValue(nil)
    }
    
}
