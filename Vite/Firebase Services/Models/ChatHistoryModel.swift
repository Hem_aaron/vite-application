//
//  ChatHistoryModel.swift
//  Vite
//
//  Created by Eeposit1 on 6/22/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation
struct ChatHistoryModel {
    var user:String
    var lastMessage:String
    var timestamp:String
    var unseenCount:Int
    var messageUniqueID:String
    var displayName:String
    var profileImageURL:String
    var userFBID:String
    
}