//
//  User.swift
//  FirebaseTest
//
//  Created by Eeposit1 on 3/6/16.
//  Copyright © 2016 Prajeet Shrestha. All rights reserved.
//

import UIKit

class ChatUser: NSObject{
    var name:String!
    var fbID:String!
    var profileImageUrl:String!
    var lastMessage:String!
    var lastMessageTime:String!
    var unseenMessagesCount:Int = 0
  
   
}
