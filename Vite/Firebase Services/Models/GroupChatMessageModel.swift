//
//  GroupChatMessaageModel.swift
//  Vite
//
//  Created by Prajeet Shrestha on 2/14/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper
import FirebaseDatabase
import Firebase

class GroupChatMessageModel:Mappable {
    var messageID:String?
    var userDisplayName:String?
    var userFBID:String?
    var userProfileURL:String?
    var message:String?
    var timestamp:[AnyHashable: Any]? = FIRServerValue.timestamp()
    var timeString: NSNumber?
    var unseenCount:Int?
    var mediaImageUrl: String?
    var mediaVideoUrl: String?
    var isMedia: Bool?
    var isVideo: Bool?
    
    
    //If EventData is not nil then it's GroupChat
    var eventId:String?
    var eventName:String?
    var eventImageUrl:String?
    
    required init?(map: Map) {
        
    }
    
    init(userName:String,
         userId:String,
         userImage:String,
         message:String,
         mediaImageUrl: String,
         mediaVideoUrl: String,
         isMedia:Bool,
         isVideo:Bool) {
        self.userDisplayName = userName
        self.userFBID = userId
        self.message = message
        self.userProfileURL = userImage
        self.mediaImageUrl = mediaImageUrl
        self.mediaVideoUrl = mediaVideoUrl
        self.isVideo = isVideo
        self.isMedia = isMedia
    }
    
    func mapping(map:Map) {
        messageID        <- map["messageID"]
        userDisplayName  <- map["userDisplayName"]
        userFBID         <- map["userFBID"]
        userProfileURL   <- map["userProfileURL"]
        message          <- map["message"]
        timestamp        <- map["timestamp"]
        timeString       <- map["timestamp"]
        unseenCount      <- map["unseenCount"]
        mediaVideoUrl    <- map["mediaVideoUrl"]
        mediaImageUrl    <- map["mediaImageUrl"]
        isMedia          <- map["media"]
        isVideo          <- map["video"]
        
        eventName        <- map["eventName"]
        eventImageUrl    <- map["eventImageUrl"]
        
    }
}
