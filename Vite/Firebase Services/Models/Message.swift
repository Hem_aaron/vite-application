//
//  Message.swift
//  FirebaseTest
//
//  Created by Eeposit1 on 3/9/16.
//  Copyright © 2016 Prajeet Shrestha. All rights reserved.
//

import UIKit

class Message: NSObject {
    var receiver:String!
    var user:String!
    var message:String!
    var time:String!
    var sender:String!
}
