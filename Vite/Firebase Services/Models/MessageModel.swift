//
//  MessageModel.swift
//  Vite
//
//  Created by Eeposit1 on 6/20/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

struct MessageModel {
    var sender:ChatUser
    var receiver:ChatUser
    var message:String
    var timestamp:String
    var uniqueId:String
    var mediaImageUrl:String
    var mediaVideoUrl:String
    var isMedia: Bool
    var isVideo: Bool
    
    init(sender:ChatUser, receiver:ChatUser, message:String, timestamp:String, uniqueId: String,mediaImageUrl: String , mediaVideoUrl:String , isMedia:Bool, isVideo:Bool) {
        self.sender    = sender
        self.receiver  = receiver
        self.message   = message
        self.timestamp = timestamp
        self.uniqueId  = uniqueId
        self.mediaImageUrl  = mediaImageUrl
        self.mediaVideoUrl  = mediaVideoUrl
        self.isVideo       = isVideo
        self.isMedia       = isMedia
    }
    
}