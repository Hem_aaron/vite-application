 //
 //  NotificationModel.swift
 //  Vite
 //
 //  Created by Eeposit1 on 6/20/16.
 //  Copyright © 2016 EeposIT_X. All rights reserved.
 //
 
 import Foundation
 import Firebase
 import FirebaseDatabase
 import ObjectMapper
 
 enum NotificationType:String {
    //When a user sends request to organiser's event.
    case RequestReceived = "RequestReceived"
    
    //When organiser accepts a user request to event.
    case RequestAccepted = "RequestAccepted"
    
    //When organiser creates event near user's periphery
    case EventCreated = "EventCreated"
    
    //When organiser edits event that user is attending.
    case EventEdited = "EventEdited"
    
    //When a user is requested to be a VIP
    case VIPRequested = "VIPRequested"
    
    //When a user acceptes a VIP Request
    case VIPAccepted = "VIPAccepted"
    
    case UnseenMessages = "UnseenMessages"
    
 }
 
 struct NEvent {
    var name:String
    var uid:String
    var imageUrl:String
    
    init(name: String, uid: String, imageUrl:String) {
        self.name     = name
        self.uid      = uid
        self.imageUrl = imageUrl
    }
    
    init(event:VEvent) {
        self.imageUrl = event.imageUrl!
        self.name  = event.title!
        self.uid = event.uid!
    }
 }
 
 struct NReceiver {
    var uid:String
 }
 
 struct NSender {
    var uid:String
    var name:String
    var imageUrl:String
 }
 
 struct NMeta {
    var unseenCount:String
 }
 
 struct NotificationModel:Mappable {
    //UID and timestamp are server value
    var uid:String?
    var timestamp:NSNumber?
    var seenStatus:Bool = false
    var type:NotificationType?
    
    var receiverUid:String?
    
    var senderUid:String?
    var senderFullName:String?
    var senderImageUrl:String?
    
    //Event Info not applicable to VIPRequested and VIPAccepted events
    var eventUid:String?
    var eventName:String?
    var eventImageUrl:String?
    var timestampObj = FIRServerValue.timestamp()
    
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        uid             <- map[NotificationKey.uid]
        timestamp       <- map[NotificationKey.timestamp]
        seenStatus      <- map[NotificationKey.seenStatus]
        type            <- map[NotificationKey.type]
        
        senderUid       <- map[NotificationKey.senderUid]
        senderFullName  <- map[NotificationKey.senderFullName]
        senderImageUrl  <- map[NotificationKey.senderImageUrl]
        
        eventUid        <- map[NotificationKey.eventUid]
        eventName       <- map[NotificationKey.eventName]
        eventImageUrl   <- map[NotificationKey.eventImageUrl]
        timestampObj    <- map[NotificationKey.timestamp]
        
    }
    //For Other Notifications
    init(type:NotificationType, receiver:NReceiver, sender:NSender, event:NEvent) {
        self.type           = type
        self.receiverUid    = receiver.uid
        self.senderUid      = sender.uid
        self.senderFullName = sender.name
        self.senderImageUrl = sender.imageUrl
        self.eventUid       = event.uid
        self.eventName      = event.name
        self.eventImageUrl  = event.imageUrl
    }
    //For Vite type event
    init(type:NotificationType, receiver:NReceiver, sender:NSender) {
        self.type           = type
        self.receiverUid    = receiver.uid
        self.senderUid      = sender.uid
        self.senderFullName = sender.name
        self.senderImageUrl = sender.imageUrl
    }
 }
