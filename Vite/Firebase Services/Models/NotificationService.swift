//
//  NotificationService.swift
//  Vite
//
//  Created by Eeposit1 on 6/20/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation
import FirebaseDatabase
import Firebase
import ObjectMapper

protocol NotificationServiceProtocol {
    func getNotifications()
}


class NotificationService {
    //Handle for fetchNotification Observer
    var allNotificationsHandler:UInt = 0
    var notificationUserNode:FIRDatabaseReference!
    
    func fetchAllNotifications(_ completionBlock:@escaping ((_ notifications:[NotificationModel])->Void)) {
        let userID = UserDefaultsManager.getActiveUserForNotification().uid
        
        self.notificationUserNode = FNode.notificationNode.child(userID)
        
        print(self.notificationUserNode)
        
        allNotificationsHandler =  notificationUserNode.queryLimited(toLast: 40).observe(.value, with: {
            snapshot in
            var notifications = [NotificationModel]()
            if snapshot.exists() {
                for (key,value) in snapshot.value as! [String:AnyObject]{
                    var event = Mapper<NotificationModel>().map(JSON: value as! [String : Any])
                    event?.uid = key
                    notifications.append(event!)
                }
            } else {
                print("NO NOTIFICATION")
            }
            
            //Sort the notifications by timestamp
            notifications.sort(by: {$0.timestamp!.doubleValue > $1.timestamp!.doubleValue})
            completionBlock(notifications)
        })
    }
    
    func resetNotificationMetaNode(_ userID:String, completionBlock:(_ status:Bool) -> Void) {
        FNode.notificationMetaNode.child(userID).setValue(nil)
    }
    
    func makeSeenStatusOn(_ userID:String, notificationID:String, completionBlock:@escaping (_ status:Bool) -> Void) {
        
        FNode.notificationNode.child(userID).child(notificationID).updateChildValues([NotificationKey.seenStatus: true], withCompletionBlock: {
            (error, ref) in
            if error == nil {
                completionBlock(true)
            } else {
                completionBlock(false)
            }
        })
    }
    
    func removeObserverFromNotificationNode() {
        self.notificationUserNode.removeObserver(withHandle: self.allNotificationsHandler)
    }
    
    func triggerNotificationWithEvent(_ type: NotificationType, event:NEvent, sender:NSender, receiver:NReceiver) {
        let notification = NotificationModel(type: type, receiver: receiver, sender: sender, event: event)
        FNode.notificationNode.child(receiver.uid).childByAutoId().setValue(notification.toJSON()) {
            (error, ref) in
            self.updateNotificationMetaNode(receiver.uid, notificationType:type)
        }
    }
    
    func triggerNotification(_ type:NotificationType, sender:NSender, receiver:NReceiver) {
        let notification = NotificationModel(type: type, receiver: receiver, sender: sender)
        FNode.notificationNode.child(receiver.uid).childByAutoId().setValue(notification.toJSON()) {
            (error, ref) in
            self.updateNotificationMetaNode(receiver.uid, notificationType:type)
        }
    }
    
    //MARK: Notification Meta
    func updateNotificationMetaNode(_ receiverUid:String, notificationType:NotificationType) {
        let userMetaNode = FNode.notificationMetaNode.child(receiverUid)
        self.retrieveNotificationMetaData(receiverUid) {
            (metaData) in
            if let data = metaData {
                if let unseenNotificationsCount = data[notificationType.rawValue] as? Int {
                    userMetaNode.updateChildValues([notificationType.rawValue : unseenNotificationsCount + 1])
                } else {
                    userMetaNode.updateChildValues([notificationType.rawValue : 1])
                }
            } else {
                userMetaNode.updateChildValues([notificationType.rawValue : 1])
            }
        }
    }
    
    func deleteNotificationWithID(_ notificationID:String) {
        let userID = UserDefaultsManager.getActiveUserForNotification().uid
        
        FNode.notificationNode.child(userID).child(notificationID).setValue(nil)
    }
    
    func resetUnseenMessageCount(_ receiver:String) {
        FNode.notificationMetaNode.child(receiver).child(NotificationType.UnseenMessages.rawValue).removeValue()
    }
    
    func resetNotificationMetaNode(_ receiverUid:String, notificationType:NotificationType) {
        FNode.notificationMetaNode.child(receiverUid).child(notificationType.rawValue).setValue(nil)
    }
    
    func resetAllNotificationMetaNode(_ receiverUid:String) {
        FNode.notificationMetaNode.child(receiverUid).setValue(nil)
    }
    
    func retrieveNotificationMetaData(_ receiverUid:String, completionBlock:@escaping (_ metaData:[String:AnyObject]?) -> Void) {
        let userMetaNode = FNode.notificationMetaNode.child(receiverUid)
        userMetaNode.observeSingleEvent(of: .value, with: {
            snapshot in
            if snapshot.exists() {
                if let notificationMeta = snapshot.value as? [String:AnyObject] {
                    completionBlock(notificationMeta)
                } else {
                    completionBlock(nil)
                }
            } else {
                completionBlock(nil)
            }
        })
    }
    
    func sendEditNotificationToAttendeeOfEvent(_ event:NEvent, sender:NSender) {
        EventServices.getEventAttendees(eventID: event.uid)
        {
            (attendeeList) -> () in
            if let attendeeArray = attendeeList {
                for attendee in attendeeArray  {
                    let id = String(describing: attendee.facebookId!)
                    let receiver = NReceiver(uid : id)
                    self.triggerNotificationWithEvent(.EventEdited, event: event, sender: sender, receiver: receiver)
                }
            }
            
        }
    }
    
    func sendNotificationToUsersInPeripheryOfEventCreated(_ event:NEvent, sender:NSender) {
        VServices.retrieveAllUserInPeriphery(event.uid, successBlock: {
            (users) in
            for user in users {
                let receiver = NReceiver(uid:String(describing: user.uid!))
                self.triggerNotificationWithEvent(.EventCreated, event: event, sender: sender, receiver: receiver)
            }
            }, failureBlock: {
                message in
        })
    }
    
    
    
    func startNotificationObserver(_ userId:String, completionBlock:@escaping ((_ count:Int) -> ())) {
        FNode.notificationMetaNode.child(userId).observe(.value, with: {
            snapshot in
            
            guard snapshot.exists() else {
                completionBlock(0)
                return
            }
            
            guard let v = snapshot.value as? [String:AnyObject] else {
                completionBlock(0)
                return
            }
            
            var count = 0
            
            for (_,value) in v {
                let num = value as? NSNumber
                count = count + num!.intValue
            }
            
            completionBlock(count)
        })
    }
    
    func removeNotificationObserverFromMetaNode() {
        FNode.notificationMetaNode.removeAllObservers()
    }
    
    /**
     VIP NOTIFICATIONS
     **/
    
    func sendVipNotification(_ sender:NSender, receiver:NReceiver, type:NotificationType) {
        let notification = NotificationModel(type: type, receiver: receiver, sender: sender)
        FNode.notificationNode.child(receiver.uid).childByAutoId().setValue(notification.toJSON()) {
            (error, ref) in
            self.updateNotificationMetaNode(receiver.uid, notificationType: type)
        }
    }
    
}
