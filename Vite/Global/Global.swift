//
//  Global.swift
//  Vite
//
//  Created by Hem Poudyal on 12/19/17.
//  Copyright © 2017 Eepos IT. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import AVFoundation
import AVKit
import ViewAnimator

let kNotificationLocationUpdated = "LocationUpdated"
let kNotificationViteNowReceived = "ViteNowReceived"
let kNotificationDirectToMsgCenter = "DirectToMsgCenter"
let kNotificationDirectToMyVites = "DirectToMyVites"
let kNotificationDirectToMyVip = "DirectToMyVip"
let kNotificationDirectToMyProfile = "DirectToMyProfile"
let kNotificationDirectToAddMyVip = "DirectToAddMyVip"
let kNotificationDirectToMyAttendee = "DirectToMyAttendee"
let kNotificationDirectToComment = "DirectToComment"
let kNotificationReceived = "customNotificationReceived"
let kNotificationBranchShare = "BranchShareNotification"
let kNotificationBranchEventShare = "BranchEventShareNotification"
let kNavigationTitleFontSize:CGFloat = 19
let kFontName = "Brown-Bold"
let kNotificationNoInternet = "NOInternetConnection"

var TestTokens = [
    "4242" : "tok_visa",
    "5556" : "tok_visa_debit",
    "4444" : "tok_mastercard",
    "8210" : "tok_mastercard_debit",
    "5100" : "tok_mastercard_prepaid",
    "0005" : "tok_amex",
    "1117" : "tok_discover",
    "5904" : "tok_diners",
    "0000" : "tok_jcb"
]

struct AppProperties {
   static let viteAppThemeColor:UInt = 0x6D2BC6
   static let viteTableThemeColor:UInt = 0x232323
   static let viteCardBackgroundColor:UInt = 0xD8D7CE
   static let viteMenuBottomViewColor:UInt = 0x363436
   static let viteNavigationTitleFontSize:CGFloat = 19
   static let viteFontName = "Brown-Bold"
}

func thumbnailForVideoAtURL(_ url: URL) -> UIImage? {
    
    let asset = AVAsset(url: url)
    let assetImageGenerator = AVAssetImageGenerator(asset: asset)
    assetImageGenerator.appliesPreferredTrackTransform = true
    var time = asset.duration
    time.value = min(time.value, 0)
    
    do {
        let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
        return UIImage(cgImage: imageRef)
    } catch {
        print("error")
        return nil
    }
}
struct Global {
    
    
   // let VITELocalizedString = [[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:@"AQStrings"]
    
    static func formatDate(date:String) -> String {
        let originalDateFormatter           = DateFormatter()
        originalDateFormatter.dateFormat    = "MM/dd/yyyy"
        
        let requiredDateFormatter = DateFormatter()
        requiredDateFormatter.dateFormat = "yyyy-MM-dd"
        
        if let date = originalDateFormatter.date(from: date) {
            return requiredDateFormatter.string(from: date)
        } else {
            return ""
        }
    }
    
}

struct UserDefaultsKey {
    //For storing device token got from app delegate after registering for push notification
    static let deviceToken = "DeviceToken"
    static let savedUserExternalID = "savedUserExternalID"
}

struct AppDel {
    static let delegate = UIApplication.shared.delegate
    static func beginIgnoring () -> Void {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    static func endIgnoring () -> Void {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        UIApplication.shared.endIgnoringInteractionEvents()
    }
}
struct UserDefaultKeys {
    static let myViteNotification    = "myViteNotification"
    static let msgCenterNotification = "msgCenterNotification"
    static let myVipNotification = "myVipNotification"
    static let myCommentNotification = "myCommentNotification"
    static let myAttendeeScreenshotNotification = "myAttendeeScreenshotNotification"
    static let isStripeConnected = "kStripeConnected"
}
func showAlert(controller:AnyObject,title:String?,message:String) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertController.addAction(defaultAction)
    controller.present(alertController, animated: false, completion: nil)
}

func getDistanceInMile(userCurrentLocation: CLLocation, eventLocation: CLLocation) -> Double{
    let distanceToEvent =  userCurrentLocation.distance(from: eventLocation)
    return round(distanceToEvent * 0.000621371)
}

func loadViteLoadingGif(_ loadingView: UIView){
    loadingView.isHidden = false
    let loadingGIF = UIImage.gifImageWithName("viteLoading")
    let imageView = UIImageView(image: loadingGIF)
    imageView.frame = CGRect(x: 0, y: 0, width: 100.0, height: 100.0)
    loadingView.addSubview(imageView)
}

func getImageWithColor(color: UIColor, size: CGSize) -> UIImage {
    let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
    UIGraphicsBeginImageContextWithOptions(size, false, 0)
    color.setFill()
    UIRectFill(rect)
    let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return image
}

func timeAgoSince(date: Date) -> String {
    
    let currentDate = Date()
   
    let components = Set<Calendar.Component>([.second, .minute, .hour, .day, .month, .year])
    let differenceOfDate = Calendar.current.dateComponents(components, from: currentDate, to: date)
    
    if let diff = differenceOfDate.year {
        if diff >= 2 {
            return "\(diff) years to go"
        }
    }
    
    if let diff = differenceOfDate.year {
        if diff >= 1 {
            return "Next year"
        }
    }
    
    if let diff = differenceOfDate.month {
        if diff >= 2 {
            return "\(diff) months to go"
        }
    }
    if let diff = differenceOfDate.month {
        if diff >= 1 {
            return "Next month"
        }
    }
    if let diff = differenceOfDate.weekOfYear {
        if diff >= 2 {
            return "\(diff) weeks to go"
        }
    }
    
    if let diff = differenceOfDate.weekOfYear {
        if diff >= 1 {
            return "Next week"
        }
    }
    
    if let diff = differenceOfDate.day {
        if diff >= 2 {
            return "\(diff) days to go"
        }
    }
    
    if let diff = differenceOfDate.day {
        if diff >= 1 {
            return "\(diff) day and " + "\((differenceOfDate.hour)!) hours to go"
        }
    }
 
    if let diff = differenceOfDate.hour {
        if diff >= 2 {
            return "\(diff) hours to go"
        }
    }
    
    if let diff = differenceOfDate.hour {
        if diff >= 1 {
            return "An hour to go"
        }
    }
    
    if let diff = differenceOfDate.minute {
        if diff >= 2 {
            return "\(diff) minutes to go"
        }
    }
    
    if let diff = differenceOfDate.minute {
        if diff >= 1 {
            return "A minute to go"
        }
    }
    
    if let diff = differenceOfDate.second {
        if diff >= 2 {
            return "\(diff) seconds to go"
        }
    }
    
    return "Already started!"
    
}

func agoDates(_ date:Date) -> String {
    let currentDate = Date()
    let components = Set<Calendar.Component>([.second, .minute, .hour, .day, .month, .year])
    let differenceOfDate = Calendar.current.dateComponents(components, from: date, to: currentDate)
    if let diff = differenceOfDate.year {
        if diff >= 2 {
            return "\(diff) years ago"
        }
    }
    
    if let diff = differenceOfDate.year {
        if diff >= 1 {
            return "Last year"
        }
    }
    
    if let diff = differenceOfDate.month {
        if diff >= 2 {
            return "\(diff) months ago"
        }
    }
    if let diff = differenceOfDate.month {
        if diff >= 1 {
            return "Last month"
        }
    }
    if let diff = differenceOfDate.weekOfYear {
        if diff >= 2 {
            return "\(diff) weeks ago"
        }
    }
    
    if let diff = differenceOfDate.weekOfYear {
        if diff >= 1 {
            return "Last week"
        }
    }
    
    if let diff = differenceOfDate.day {
        if diff >= 2 {
            return "\(diff) days ago"
        }
    }
    
    if let diff = differenceOfDate.day {
        if diff >= 1 {
            return "\(diff) day and " + "\((differenceOfDate.hour)!) hours ago"
        }
    }
    
    if let diff = differenceOfDate.hour {
        if diff >= 2 {
            return "\(diff) hours ago"
        }
    }
    
    if let diff = differenceOfDate.hour {
        if diff >= 1 {
            return "An hour ago"
        }
    }
    
    if let diff = differenceOfDate.minute {
        if diff >= 2 {
            return "\(diff) minutes ago"
        }
    }
    
    if let diff = differenceOfDate.minute {
        if diff >= 1 {
            return "A minute ago"
        }
    }
    
    if let diff = differenceOfDate.second {
        if diff >= 2 {
            return "\(diff) seconds ago"
        }
    }
    
    return "Just now"
}

func checkIfUserLoggedInFromAnotherDevice(msg: String, ctrl: UIViewController){
    if msg == "Invalid access token" {
        let controller = Route.signUpViewController
        //SlideMenuController().changeMainViewController(controller, close: false)
        AppDel.delegate?.window??.rootViewController = controller
        showAlert(controller: ctrl, title: "", message: localizedString(forKey: "LOGIN_AGAIN"))
    } else {
         showAlert(controller: ctrl, title: "", message: msg)
    }
}

func imageWithView(_ view : UIView) -> UIImage
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
    view.layer.render(in: UIGraphicsGetCurrentContext()!)
    
    var img = UIImage()
    img = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return img
    
}

func playVideo(_ url: String,viewController: UIViewController){
    if let videoURL = URL(string: url){
        let player = AVPlayer(url: videoURL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        viewController.present(playerViewController, animated: true, completion: nil)
    }
}

func setUpLblMsgs(_ text:String, view:UIView, lblMessage: UILabel) {
    //let lblMessage = UILabel()
    lblMessage.text      = text
    lblMessage.textColor = UIColor.lightGray
    lblMessage.sizeToFit()
    lblMessage.center    = view.center
    lblMessage.font      = UIFont.systemFont(ofSize: 17)
    //lblMessage.hidden    = true
    view.addSubview(lblMessage)
}

// remove unrequired white spaces
func trimCharacter(comment: String) -> String{
    let trimmedString =  comment.trimmingCharacters(in: .whitespaces)
    return trimmedString
}

func presentVideoInNewView(_ fileType: String, videoUrl: String, viewController: UIViewController){
    let controller = Route.fullScreenViewController
    controller.mediaURL = videoUrl
    controller.mediaType = fileType
    viewController.present(controller, animated: true, completion: nil)
}

func isValidEmail(testStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}

func convertDateFormaterFromFb(_ date: String) -> (String,String) {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    guard let date = dateFormatter.date(from: date) else {
        assert(false, "no date from string")
        return ("","")
    }
    dateFormatter.dateFormat = "yyy-MM-dd'T'HH:mm"
    let dateString = dateFormatter.string(from: date)
    let dateTime:[String] = dateString.components(separatedBy: "T")
    return (dateTime[0],dateTime[1])
}

func convertStringToNSDate(_ date: String, format: String) -> Date?{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    let nsDate =  dateFormatter.date(from: date)
    return nsDate!
}


enum UIUserInterfaceIdiom : Int
{
    case unspecified
    case phone
    case pad
}


struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct AnimationsList {
    static  let fromBottom = AnimationType.from(direction: .bottom, offset: 30.0)
    static  let fromRight = AnimationType.from(direction: .right , offset: 30)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
}


 func localizedString(forKey key: String) -> String {
    var result = Bundle.main.localizedString(forKey: key, value: nil, table: "ViteStrings")
    return result
}
