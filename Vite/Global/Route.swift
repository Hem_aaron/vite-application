//
//  Route.swift
//  Vite
//
//  Created by Hem Poudyal on 12/27/17.
//  Copyright © 2017 Eepos IT. All rights reserved.
//

import Foundation
import UIKit

class Route {
    static func goToHomeController() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.configureInitialViewController()
    }
    
    static private var  mainBoard  = UIStoryboard(name: "Main", bundle: nil)
    static private var  homeBoard  = UIStoryboard(name:"Home", bundle: nil)
    static private var  settingsBoard  = UIStoryboard(name:"Settings", bundle: nil)
    static private var  dashBoard  = UIStoryboard(name:"Dashboard", bundle: nil)
    static private var  messageBoard  = UIStoryboard(name:"MessageCenter", bundle: nil)
    static private var  profileBoard  = UIStoryboard(name:"Profile", bundle: nil)
    static private var myEventsBoard = UIStoryboard(name: "MyEvents", bundle: nil)
    static private var paymentHistoryBoard = UIStoryboard(name: "PaymentHistory", bundle: nil)
    static private var businessAccountBoard = UIStoryboard(name: "BusinessAccount", bundle: nil)
    static private var mapBoard =  UIStoryboard(name: "Map", bundle: nil)
    static private var feedbackBoard =  UIStoryboard(name: "Feedback", bundle: nil)
    static private var myVipBoard =  UIStoryboard(name: "MyVip", bundle: nil)
    static private var chatBoard =  UIStoryboard(name: "Chat", bundle: nil)
    static private var createEventBoard =  UIStoryboard(name: "CreateEvent", bundle: nil)
    static private var notificationBoard =  UIStoryboard(name: "Notification", bundle: nil)
    static private var stripeBoard =  UIStoryboard(name: "PaymentStripe", bundle: nil)
    static private let swapPeopleBoard  = UIStoryboard(name: "SwappedPeopleStoryboard", bundle: nil)
    static private let myViteBoard  = UIStoryboard(name: "MyVites", bundle: nil)
    static private var scanTicketBoard =  UIStoryboard(name: "ScanTicket", bundle: nil)
    static private var eventDetailBoard = UIStoryboard(name: "EventDetails", bundle: nil)
    static private var filterBoard = UIStoryboard(name: "Filter", bundle: nil)
    static private var walkthroughBoard = UIStoryboard(name: "Walkthrough", bundle: nil)
    static private var rateViewBoard = UIStoryboard(name: "RateView", bundle: nil)
    
    
    
    
    
    static var signUpViewController: SignUpViewController {
        let controller = mainBoard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        return controller
    }
    
    static var celebSuccess: CelebritySuccessViewController {
        let controller = mainBoard.instantiateViewController(withIdentifier: "celebSuccessVc") as! CelebritySuccessViewController
        return controller
    }
    
    static var noLocationViewController: NoLocationViewController {
        let controller = mainBoard.instantiateViewController(withIdentifier: "NoLocationViewController") as! NoLocationViewController
        return controller
    }
    
    static var celebEliteNav: UINavigationController {
        let controller = mainBoard.instantiateViewController(withIdentifier: "celebEliteNav") as! UINavigationController
        return controller
    }
    
    static var celebrityLoginViewController: CelebrityLoginViewController {
        let controller = mainBoard.instantiateViewController(withIdentifier: "CelebrityLoginViewController") as! CelebrityLoginViewController
        return controller
    }
    
    static var celebritySuccessViewController: CelebritySuccessViewController {
        let controller = mainBoard.instantiateViewController(withIdentifier: "CelebritySuccessViewController") as! CelebritySuccessViewController
        return controller
    }
    
    static var menuViewController: MenuViewController {
        let controller = homeBoard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        return controller
    }
    
    static var settingsTableViewController: SettingsTableViewController {
        let controller = settingsBoard.instantiateViewController(withIdentifier: "SettingsTableViewController") as! SettingsTableViewController
        return controller
    }
    
    static var settingsNav: UINavigationController {
        let controller = settingsBoard.instantiateViewController(withIdentifier: "settingsNav") as! UINavigationController
        return controller
    }
    
    static var blockedListViewController: BlockedUsersViewController {
        let controller = settingsBoard.instantiateViewController(withIdentifier: "BlockedListVc") as! BlockedUsersViewController
        return controller
    }
    
    static var termsConditionAndPrivacy: TermsAndConditionsViewController {
       let controller = settingsBoard.instantiateViewController(withIdentifier: "termsAndConditions") as! TermsAndConditionsViewController
        return controller
   }
    
    static var masterViewController: MasterViewController {
        let controller = dashBoard.instantiateViewController(withIdentifier: "MasterView") as! MasterViewController
        return controller
    }
    
    static var MessageCenter:MessageCenterTableViewController {
        return messageBoard.instantiateViewController(withIdentifier: "messageCenterVC") as! MessageCenterTableViewController
    }
    
    static var attendeeRequest:Attendee_RequestViewController {
        return messageBoard.instantiateViewController(withIdentifier: "Attendee_RequestViewController") as! Attendee_RequestViewController
    }
    static var broadCastVC:BroadcastViewController {
        return messageBoard.instantiateViewController(withIdentifier: "BroadcastViewController") as! BroadcastViewController
    }
    
    
    static var MyEvents: UINavigationController {
        let controller = myEventsBoard.instantiateViewController(withIdentifier: "EventListNav") as! UINavigationController
        return controller
     }
    
    static var refundAlertVC: RefundAlertViewController {
        let controller = myEventsBoard.instantiateViewController(withIdentifier: "RefundAlertVc") as! RefundAlertViewController
        return controller
    }
    static var showEventVC: ShowEventViewController {
        let controller = myEventsBoard.instantiateViewController(withIdentifier: "ShowEventViewController") as! ShowEventViewController
        return controller
    }
    
    static var refundViewController: RefundViewController {
        let controller = myEventsBoard.instantiateViewController(withIdentifier: "RefundVc") as! RefundViewController
        return controller
    }
  
    static var eventListTableViewController: EventListTableViewController {
        let controller = myEventsBoard.instantiateViewController(withIdentifier: "eventListIdentifier") as! EventListTableViewController       
        return controller
    }
    
    static var AllUser:AllUsersViewController {
        return messageBoard.instantiateViewController(withIdentifier: "AllUsersViewController") as! AllUsersViewController
    }
    
    static var rateViewController:RateViewController {
        return rateViewBoard.instantiateViewController(withIdentifier: "rateView") as! RateViewController
    }
   
    
    // EventDetail
    static var EventDetailVC:EventDetailViewController {
        return eventDetailBoard.instantiateViewController(withIdentifier: "eventDetail") as! EventDetailViewController
    }
    
    static var EventDetailNav:UINavigationController {
        return eventDetailBoard.instantiateViewController(withIdentifier: "eventDetailNav") as! UINavigationController
    }
    
    static var SwipeNav:UINavigationController {
        return eventDetailBoard.instantiateViewController(withIdentifier: "swipeFullScreenNav") as! UINavigationController
    }
    
    static var SwipeFullScreenViewController:SwipeFullScreenViewController {
        
        return eventDetailBoard.instantiateViewController(withIdentifier: "swipeFullScreenVC") as! SwipeFullScreenViewController
    }
    
    static var fullScreenViewController:FullScreenViewController {
        return eventDetailBoard.instantiateViewController(withIdentifier: "fullScreenVC") as! FullScreenViewController
    }
 
    // vip
    static var Vip:VipsViewController {
        return myVipBoard.instantiateViewController(withIdentifier: "VipVc") as! VipsViewController
    }

    static var addToVip:AddToVipsViewController {
        return myVipBoard.instantiateViewController(withIdentifier: "AddVipController") as! AddToVipsViewController
    }

    
    static var myVipNav: UINavigationController {
        let controller = myVipBoard.instantiateViewController(withIdentifier: "myVipNav") as! UINavigationController
        return controller
    }
    
    
    //Swap
    static var SwapPeople:SwappedPeopleViewController {
        return swapPeopleBoard.instantiateViewController(withIdentifier: "SwappedPeopleViewController") as! SwappedPeopleViewController
    }
    
    static var createEventOptions: CreateEventOptionsViewController{
        return swapPeopleBoard.instantiateViewController(withIdentifier: "CreateEventOptionsViewController") as! CreateEventOptionsViewController
    }
    
    //chatView
    static var imagePreview:ImagePreviewViewController {
        return chatBoard.instantiateViewController(withIdentifier: "imageVc") as! ImagePreviewViewController
    }
    
    static var chatMain:ChatMainViewController {
        let chatController      = chatBoard.instantiateViewController(withIdentifier: "ChatMainViewController") as! ChatMainViewController
       return chatController
    }
    
    static var groupChat:GroupChatViewController {
        let groupChatController      = chatBoard.instantiateViewController(withIdentifier: "GroupChatViewController") as! GroupChatViewController
        return groupChatController
    }
   
    
    //MARK: PROFILE VIEW CONTROLLER
    static var profileViewController: MyProfileViewController {
        let controller = profileBoard.instantiateViewController(withIdentifier: "MyProfileVc") as! MyProfileViewController
        return controller
    }
    
    static var profileNav: UINavigationController {
        let controller = profileBoard.instantiateViewController(withIdentifier: "ProfileViewNav") as! UINavigationController
        return controller
    }
    
    static var premiumViewController: PremiumWalkthroughViewController {
        let controller = profileBoard.instantiateViewController(withIdentifier: "PremiumWalkthroughViewController") as! PremiumWalkthroughViewController
        return controller
    }
    
    static var passportPop:premiumPopViewController {
        return profileBoard.instantiateViewController(withIdentifier: "passportPopVc") as! premiumPopViewController
    }
    
    static var rateCommentList: RateAndCommentListViewController {
        let controller = profileBoard.instantiateViewController(withIdentifier: "rateCommentVc") as! RateAndCommentListViewController
        return controller
    }
    
    //MARK:PAYMENT HISTORY BOARD VCs
    static var paymentHistoryDetails: PaymentHistoryDetailViewController {
        let controller = paymentHistoryBoard.instantiateViewController(withIdentifier: "paymentDetailVc") as! PaymentHistoryDetailViewController
        return controller
    }
    
    static var paymentNav: UINavigationController {
        let controller = paymentHistoryBoard.instantiateViewController(withIdentifier: "paymentNav") as! UINavigationController
        return controller
    }
    
    //MARK: BUSINESS ACCOUNT
    
    static var businessAccNav: UINavigationController {
        let controller = businessAccountBoard.instantiateViewController(withIdentifier: "ManageAcNav") as! UINavigationController
        return controller
    }
    
    static var createBusinessAccount: CreateBusinessAccountVC {
        let controller = businessAccountBoard.instantiateViewController(withIdentifier: "createAccount") as! CreateBusinessAccountVC
        return controller
    }
    
    static var updateAccountForBusiness:UpdateBusinessAccount{
        return businessAccountBoard.instantiateViewController(withIdentifier: "updateBusinessAccountVc") as! UpdateBusinessAccount
    }
    
    //MARK: MAP
    static var mapController: EventMapController {
        let controller = mapBoard.instantiateViewController(withIdentifier: "EventMapController") as! EventMapController
        return controller
    }
    
    //MARK: FEEDBACK
    static var feedbackNav: UINavigationController {
        let controller = feedbackBoard.instantiateViewController(withIdentifier: "feedBackNav") as! UINavigationController
        return controller
    }
    // Report Reason Feedback
    static var UserReasonViewController:UserReasonViewController {
        return feedbackBoard.instantiateViewController(withIdentifier: "reasonVc") as! UserReasonViewController
    }
    //MARK: CREATE EVENT
    
    static var CreateEvent:CreateEventsViewController {
        return createEventBoard.instantiateViewController(withIdentifier: "CreateEventController") as! CreateEventsViewController
    }

    static var CreateEventPopup:CreateEventPopViewController {
        return createEventBoard.instantiateViewController(withIdentifier: "CreateEventPopViewController") as! CreateEventPopViewController
    }
    
    static var Private:PrivateFriendVC {
        return createEventBoard.instantiateViewController(withIdentifier: "PrivateFriendVC") as! PrivateFriendVC
    }
    
    static var inviteVC: InviteFriendsVC {
        return createEventBoard.instantiateViewController(withIdentifier: "inviteVc") as! InviteFriendsVC
    }
    static var paypalEmailVc: PaypalEmailViewController {
        return createEventBoard.instantiateViewController(withIdentifier: "paypalEmailVc") as! PaypalEmailViewController
    }
    
  
    
    //MARK: STRIPE
    
    static var featurePayment:FeaturePaymentViewController {
        return stripeBoard.instantiateViewController(withIdentifier: "featureVc") as! FeaturePaymentViewController
    }
    
    //MARK: Payment Stripe
    
    static var paymentViewController:PaymentViewController {
        return stripeBoard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentViewController
    }
    
    static var makePaymentViewController:MakePaymentViewController {
        return stripeBoard.instantiateViewController(withIdentifier: "MakePaymentVC") as! MakePaymentViewController
    }
    
 
    
    //Mark: NotificationNav
    static var notificationNav: UINavigationController {
        let controller = notificationBoard.instantiateViewController(withIdentifier: "notificationNav") as! UINavigationController
        return controller
    }
    
    static var notificationVC: NewNotificationViewController {
        let controller = notificationBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NewNotificationViewController
        return controller
    }
    
    //MARK: MY VITES
    static var myTicket: MyTicketViewController {
         let controller = myViteBoard.instantiateViewController(withIdentifier: "myTicketVc") as! MyTicketViewController
        return controller
    }
    
    static var viteDetail:ViteDetailsViewController {
        return myViteBoard.instantiateViewController(withIdentifier: "ViteDetailsVC") as! ViteDetailsViewController
    }
    
    static var myVites:MyVitesViewController {
        return myViteBoard.instantiateViewController(withIdentifier: "myVitesController") as! MyVitesViewController
    }
    
    static var myVitesNav:UINavigationController {
        return myViteBoard.instantiateViewController(withIdentifier: "myVitesNav") as! UINavigationController
    }
    
    //Mark: ScanTicketNav
    static var scanTicketNav: UINavigationController {
        let controller = scanTicketBoard.instantiateViewController(withIdentifier: "ScanTicketNav") as! UINavigationController
        return controller
    }
    
    static weak var scanTicketViewController: ScanTicketViewController? {
        let controller = scanTicketBoard.instantiateViewController(withIdentifier: "ScanTicketViewController") as! ScanTicketViewController
        return controller
    }
    
    //MARK: WALKTHROUGH
    static var walkthrough: WalkthroughViewController {
        let controller =   walkthroughBoard.instantiateViewController(withIdentifier: "WalkthroughVc") as! WalkthroughViewController
        return controller
    }
    
    //MARK: FILTER EVENT AND USER
    static var eventFilterNav: UINavigationController {
        let controller = filterBoard.instantiateViewController(withIdentifier: "filterNav") as! UINavigationController
        return controller
    }
    
    static var filterUser: FilterUserViewController {
        let controller = filterBoard.instantiateViewController(withIdentifier: "filterUserVc") as! FilterUserViewController
        return controller
    }
    
  //MARK: FULL SCREEN CAMERA VIEW FOR IMAGE POST BY ATTENDEE
    static var showViteFeedMedia: ViteFeedMediaPostViewController {
        let controller = dashBoard.instantiateViewController(withIdentifier: "showViteFeedMedia") as! ViteFeedMediaPostViewController
        return controller
    }
    
    static var selectViteOrEventToPost: SelectEventOrViteToPostViewController {
        let controller = dashBoard.instantiateViewController(withIdentifier: "selectEventViteVc") as! SelectEventOrViteToPostViewController
        return controller
    }

    
}
