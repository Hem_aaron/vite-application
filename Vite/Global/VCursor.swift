//
//  VCursor.swift
//  Vite
//
//  Created by Prajeet Shrestha on 5/8/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
class VCursor {
    private var currentPage:Int = -1
    var totalCount:Int = 0
    var totalLoadedDataCount:Int = 0
    var pageSize:Int = 10
    
    func nextPage() -> Int {
        self.currentPage = self.currentPage + 1
        return currentPage
    }
    
    func reset() {
        self.currentPage = 0
        self.totalCount = 0
        self.totalLoadedDataCount = 0
    }
    
    func hasNextPage() -> Bool {
        return totalLoadedDataCount < totalCount
    }
}