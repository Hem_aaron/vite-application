//
//  ActivityIndicator.swift
//  Vite
//
//  Created by Eeposit  on 1/16/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//


/*
 USAGE:
 
 let indicator = ActivityIndicator() //Starts activity in default position which is at the center of the screen
 indicator.start()
 indicator.stop()
 
 
 let indicator = ActivityIndicator()
 indicator.parentView = aView// Reference of a parentView where indicator will start spinning
 
 */

import Foundation
import UIKit

class ActivityIndicator {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    lazy var parentView = (UIApplication.shared.windows.last?.rootViewController!.view)!
    var color:UIColor?
    
    //    /*
    //     If user has set the tintColor, the activityIndicator will have tint color as user set,
    //     If not, the default tintColor will be set as blackColor
    //     */
    
    var tintColor:UIColor? {
        get {
            guard let color = self.color else  {
                return UIColor.gray
            }
            return color
            
        } set {
            self.color = newValue
        }
    }
    
    /*
     If user has not provided view where activity indicator should pop up.
     The default view will be view of the current rootViewController of window.
     */
    
    var activityIndicatorView:UIActivityIndicatorView? {
        get {
            guard let indicatorView = self.parentView.viewWithTag(1024) as? UIActivityIndicatorView else {
                let activityIndicator              = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                activityIndicator.frame            = CGRect.zero
                activityIndicator.center           = self.parentView.center
                activityIndicator.hidesWhenStopped = true
                activityIndicator.tag              = 1024
                activityIndicator.color            = self.tintColor
                activityIndicator.startAnimating()
                self.parentView.addSubview(activityIndicator)
                //
                return activityIndicator
            }
            return indicatorView
        }
    }

    func start() {
        //self.setLayoutConstraintForView(self.activityIndicatorView!)
        //Get reference of activity indicator
        self.activityIndicatorView?.startAnimating()
        self.parentView.isUserInteractionEnabled = false
    }
    
    func stop() {
        self.activityIndicatorView?.stopAnimating()
        self.activityIndicatorView?.removeFromSuperview()
        self.parentView.isUserInteractionEnabled = true
        
    }
    
    
    //    func setLayoutConstraintForView(view:UIView) {
    //        view.translatesAutoresizingMaskIntoConstraints   = false
    //        let centerVertically   = NSLayoutConstraint(item: view, attribute: .CenterY, relatedBy: .Equal, toItem: view.superview, attribute: .CenterY, multiplier: 1, constant: 0)
    //        //Sets X-Position
    //        let centerHorizontally = NSLayoutConstraint(item: view, attribute: .CenterX, relatedBy: .Equal, toItem:view.superview , attribute: .CenterX, multiplier: 1, constant: 0)
    //        view.superview?.addConstraints([centerVertically,centerHorizontally])
    //    }
}
