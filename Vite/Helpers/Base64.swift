//
//  Base64.swift
//  Vite
//
//  Created by Eeposit1 on 2/21/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import AVFoundation
struct Base64 {
    static func stringFromImage(_ image:UIImage) -> String? {
        //Now use image to create into NSData format
        let imageData = UIImagePNGRepresentation(image)
        let base64String = imageData?.base64EncodedString(options: .lineLength76Characters)
        return base64String
    }
    
    static func stringFromDataImage(_ image:Data) -> String? {
        let base64String = image.base64EncodedString(options: .lineLength76Characters)
        return base64String
    }
    
    static func imageFromString(_ base64String:String) -> UIImage {
        let decodedData = Data(base64Encoded: base64String, options: Data.Base64DecodingOptions(rawValue: 0))
        let decodedimage = UIImage(data: decodedData!)
        return decodedimage!

    }
    
    static func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov 
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    static func getCompressedImageData(_ image:UIImage) -> Data {
        var smallImage:UIImage = image
        
        while true {
            let imgData: Data = NSData(data: UIImageJPEGRepresentation((smallImage), 1)!) as Data
            let sizeInKB = imgData.count / 1024
            if sizeInKB <= 512 {
                
                return imgData
            }
            let newWidth = smallImage.size.width * (80/100)
            smallImage = resizeImage(smallImage, newWidth: newWidth)
        }
    }
    
    
   static func resizeImage(_ image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: floor(newWidth), height: floor(newHeight)))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    static func getCompressedBase64Image(_ image:UIImage) -> String {
        let compressedImageDatas = [
            image.uncompressedPNGData,
            image.highestQualityJPEGNSData,
            image.highQualityJPEGNSData,
            image.lowQualityJPEGNSData,
            image.lowestQualityJPEGNSData
        ]
        
        for imageData in compressedImageDatas {
            
            let sizeInKB = imageData.length / 1024
//            print(sizeInKB)
            if sizeInKB <= 512 {
                return Base64.stringFromDataImage(imageData as Data)!
            }
        }
        let img = UIImage(data: image.lowestQualityJPEGNSData as Data)!
        return Base64.stringFromImage(img)!
    }
}
    
