//
//  CircleImage.swift
//  Vite
//
//  Created by Eeposit  on 1/25/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit

class CircleImage : UIImageView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
    }
    
}
