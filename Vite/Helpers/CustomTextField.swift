//
//  CustomTextField.swift
//  Vite
//
//  Created by Eeposit1 on 2/14/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class CustomTextField: UITextField,UITextFieldDelegate {
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    self.delegate = self
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    self.layer.cornerRadius = 5
    self.clipsToBounds = true
  }
  
  func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    self.resignFirstResponder()
    return true
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    self.resignFirstResponder()
    return true
  }
  
  let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10);
  
  override func textRect(forBounds bounds: CGRect) -> CGRect {
    return UIEdgeInsetsInsetRect(bounds, padding)
  }
  
  override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
    return UIEdgeInsetsInsetRect(bounds, padding)
  }
  
  override func editingRect(forBounds bounds: CGRect) -> CGRect {
    return UIEdgeInsetsInsetRect(bounds, padding)
  }
  
  
}
