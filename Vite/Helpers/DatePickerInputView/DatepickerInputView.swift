//
//  DatepickerInputView.swift
//  Vite
//
//  Created by Eeposit1 on 5/31/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//
import UIKit
class DatepickerInputView: UIView {
    
    var doneHandler:(()->())?
    
    var cancelHandler:(()->())?
    
    @IBOutlet var view: UIView!
    
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */
    @IBAction func doneAction(_ sender: AnyObject) {
        if let handler = self.doneHandler {
            handler()
        }
    }
    
    
    @IBAction func cancelAction(_ sender: AnyObject) {
        if let handler = self.cancelHandler {
            handler()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        Bundle.main.loadNibNamed("DatepickerInputView", owner: self, options: nil)
        self.view.frame = self.bounds
        self.addSubview(view)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        Bundle.main.loadNibNamed("DatepickerInputView", owner: self, options: nil)
        self.addSubview(view)
    }
    
}
