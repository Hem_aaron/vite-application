//
//  DatePickerText.swift
//  DatePicker
//
//  Created by Prajeet Shrestha on 4/11/16.
//

import UIKit

class DatePickerText: CustomTextField {
    var textFieldTag = Int()
    fileprivate let datePicker = UIDatePicker(frame: CGRect.zero)
    fileprivate var dateFormatter = DateFormatter()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setDatepickerAsInputView()
        self.datePicker.addTarget(self, action: #selector(DatePickerText.datePickerChanged(_:)), for: UIControlEvents.valueChanged)
        self.dateFormatter.dateFormat = "YYYY-MM-dd HH:mm"
        self.datePicker.datePickerMode = .dateAndTime
        self.datePicker.minimumDate = Date()

       
        
    }
    
    func setDatepickerAsInputView() {
        self.inputView = self.datePicker
        self.delegate = self
        let inputAccessory = DatepickerInputView(frame: CGRect(x: 0,y: 0,width: 100, height: 60))
        inputAccessory.cancelHandler = {
            self.resignFirstResponder()
        }
        inputAccessory.doneHandler = {
            self.resignFirstResponder()
            //for default time and date
            if self.textFieldTag == 0 {
                self.dateFormatter.dateFormat = "YYYY-MM-dd HH:mm"
            }else{
                //for recurring event start date
                self.dateFormatter.dateFormat = "YYYY-MM-dd"
            }
            let strDate = self.dateFormatter.string(from: self.datePicker.date)
            self.text = strDate
        }
        self.inputAccessoryView = inputAccessory
    }
    
    @objc func datePickerChanged(_ sender:AnyObject) {
        let strDate = dateFormatter.string(from: datePicker.date)
        self.text = strDate
    }
    
     func textFieldDidBeginEditing(_ textField: UITextField) {
        textFieldTag = textField.tag
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//         dateFormatter.dateFormat = "YYYY-MM-dd HH:mm"
//        let strDate = dateFormatter.stringFromDate(datePicker.date)
//        self.text = strDate
        
    }
    
}
