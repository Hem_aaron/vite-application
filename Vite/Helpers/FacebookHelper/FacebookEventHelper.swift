//
//  FacebookEventHelper.swift
//  Vite
//
//  Created by eeposit2 on 5/15/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit

struct FacebookEventHelper:FBEventDataProvider {
    
    
    func getEventOfUser(successBlock:@escaping ((_ eventList: [FBUserEvent]) -> Void),
                        failureBlock: @escaping ((_ message:String) -> Void)) {
        FBSDKGraphRequest(graphPath: "me/events/created",
                          parameters: ["fields":"id,attending_count,can_guests_invite,category,cover,description,end_time,start_time,name,place,type,updated_time"]).start(completionHandler: {
                (connection, result, error) -> Void in
                if error != nil {
                    failureBlock(localizedString(forKey: "FAILED_TO_GET_EVENTLIST"))
                    return
                }
                            guard let result = result as? [String:AnyObject] else {
                                return
                            }
                if let data = result["data"] as? [[String:AnyObject]] {
                    let eventList = data.flatMap(FBUserEvent.init)
                    guard let apiURl = KAPIGetFaceBookEventList else {
                        return
                    }
                    WebService.request(method: .get, url: apiURl , success: { (response) in
                        guard let response = response.result.value as? [String:AnyObject] else  {
                            return
                        }
                        if let params = response["params"] as? [String:AnyObject],
                                                    let eventsArray = params["eventIdList"] as? [AnyObject] {
                                                    let mapped = eventsArray.map({
                                                        String(describing: $0)
                                                    })
                                                    let filteredEvent = eventList.filter({
                                                        !mapped.contains($0.eventId)
                                                    })
                            UserDefaults.standard.set(filteredEvent.count, forKey: "fbEventListCount")
                            successBlock(filteredEvent)
                                                }
                    }, failure: { (messsage, apiError) in
                        failureBlock(messsage)
                    })

                }
            })
    }
    
    
    func checkForFacebookEvents(completionBlock:@escaping ((_ status:Bool, _ message:String) -> ())){
        var fbEventIdArr              = [String]()
        FacebookEventHelper().getEventOfUser(successBlock: { (eventList) in
            
            //Only append event id's from facebookEvents
            for event in eventList{
                fbEventIdArr.append(event.eventId)
            }
            let sortedFbEventIdArr = fbEventIdArr.sorted()
            
            let defaultKey = "fbEventIdUserDefault"
            let extraKey = "extra"
            let userDefault = UserDefaults.standard
            
            if fbEventIdArr.count != 0 {
                if let fbEventIdUserDefault = userDefault.object (forKey: defaultKey) as? [String] {
                    
                    if let e = userDefault.object(forKey: extraKey) as? [String], e == sortedFbEventIdArr {
                        return
                    } else {
                        userDefault.set(sortedFbEventIdArr, forKey: extraKey)
                    }
                    
                    if fbEventIdUserDefault.sorted() != sortedFbEventIdArr && (!Set(fbEventIdArr).isSubset(of: Set(fbEventIdUserDefault)))  {
                        completionBlock(true, "")
                    } else {
                        completionBlock(false, "No facebook events")
                    }
                }
                else {
                    userDefault.set(fbEventIdArr, forKey: defaultKey)//setset(fbEventIdArr, forKey: defaultKey)
                    completionBlock(true, "")
                }
            }
            } ,failureBlock: {
                message in
                completionBlock(false, message)
        })
    }
}
