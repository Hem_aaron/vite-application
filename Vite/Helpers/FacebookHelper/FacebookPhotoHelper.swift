//
//  FacebookPhotoHelper.swift
//  Vite
//
//  Created by Hem Poudyal on 12/21/17.
//  Copyright © 2017 Eepos IT. All rights reserved.
//

import Foundation
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit

class FacebookPhotoHelper {
    class func getFacebookPhoto(successBlock:@escaping ((_ imageList: [String]?) -> Void),
                                failureBlock:@escaping ((_ message:String) -> Void)) {
        
        FBSDKGraphRequest(graphPath: "me",
                          parameters: ["fields":"albums.fields(id,name,count,cover_photo)"]).start(completionHandler: {
                            (connection, result, error) -> Void in
                            
                            if error != nil {
                                failureBlock(localizedString(forKey: "FAILED_TO_GET_PROFILE_IMAGE"))
                                return
                                
                            }
                            guard let result = result as? [String:AnyObject] else {
                                return
                            }
                            // If result has albums continue else return
                            guard let album = result["albums"] as? [String:AnyObject]  else {
                                failureBlock(localizedString(forKey: "FAILED_TO_GET_PROFILE_IMAGE"))
                                return
                            }
                            // If result has albums data else return
                            guard let albumData = album["data"] as? [[String:AnyObject]] else {
                                
                                failureBlock(localizedString(forKey: "FAILED_TO_GET_ALBUMN_DATA"))
                                
                                return
                            }
                            
                            let album_data = albumData
                            
                            var count = 0
                            for album in album_data {
                                
                                if let name = album["name"] as? String, name == "Profile Pictures" {
                                    let id = album["id"] as! String
                                    
                                    self.getPhotosFromAlbum(id: id, successBlock: {
                                        photolist in
                                        successBlock(photolist)
                                        
                                    } ,failureBlock : {
                                        message in
                                        failureBlock(message)
                                    })
                                    break
                                }
                                count += 1
                            }
                            //If user doesn't have profile images in fb
                            
                            /*
                             If we couldn't find albume named Profile Pictures in user albums.
                             Then initiate failure block
                             */
                            
                            if count == album_data.count {
                                failureBlock(localizedString(forKey: "ALBUMN_NAME_ERROR"))
                            }
                            
                          })
    }
    
    class func getAllFacebookAlbumAndPhoto(_ successBlock:@escaping ((_ albumList: [FacebookAlbum]?) -> Void),
                                           failureBlock:@escaping ((_ message:String) -> Void)) {
        FBSDKGraphRequest(graphPath: "me",
                          parameters: ["fields":"albums.fields(id,name,count,cover_photo.fields(picture))"]).start(completionHandler: {
                            (connection, result, error) -> Void in
                            
                            if error != nil {
                                failureBlock(localizedString(forKey: "FAILED_TO_GET_PROFILE_IMAGE"))
                                return
                                
                            }
                            guard let results = result as? [String:AnyObject] else {
                                return
                            }
                            // If result has albums continue else return
                            guard let album = results["albums"] as? NSDictionary else {
                                failureBlock(localizedString(forKey: "FAILED_TO_GET_PROFILE_IMAGE"))
                                return
                            }
                            // If result has albums data else return
                            guard let albumData = album["data"] else {
                                return
                            }
                            
                            var facebookAlbumArray = [FacebookAlbum]()
                            for data in albumData as! NSArray {
                                let fdata = data as? [String: Any]
                                if fdata!["count"] as? Int != 0 {
                                    let fbAlbum        = FacebookAlbum()
                                    fbAlbum.count      = String(describing: fdata!["count"])
                                    if let coverPhoto = fdata!["cover_photo"]  as? [String: AnyObject]{
                                        fbAlbum.coverPhoto = coverPhoto["picture"] as? String
                                    }
                                    fbAlbum.id         = fdata!["id"] as? String
                                    fbAlbum.name       = fdata!["name"] as? String
                                    facebookAlbumArray.append(fbAlbum)
                                }
                            }
                            if facebookAlbumArray.count > 0 {
                                successBlock(facebookAlbumArray)
                            } else {
                                failureBlock("No albums")
                            }
                          })
    }
    
    class func getAllPhotosFromAlbum(_ id:String,
                                     successBlock:@escaping ((_ imageList: [String]?) -> Void),
                                     failureBlock:@escaping ((_ message:String) -> Void)) {
        FBSDKGraphRequest(graphPath: "\(id)",
            parameters: ["fields":"photos.fields(source,id,name,picture)"]).start(
                completionHandler: {
                    (connection, result, error) -> Void in
                    if error == nil {
                        
                        var imageArray = [String]()
                        guard let results = result as? [String:AnyObject] else {
                            return
                        }
                        if let photos = results["photos"]{
                            let images = photos["data"] as? NSArray
                            if images?.count != nil{
                                for imageDictionary in images!{
                                    let imagedata = imageDictionary as? [String: Any]
                                    let imageUrl = imagedata!["source"] as! String
                                    imageArray.append(imageUrl)
                                }
                            }
                            
                        }
                        successBlock(imageArray)
                    } else{
                        failureBlock(localizedString(forKey: "FAILED_TO_GET_PHOTOS"))
                    }
            })
    }
    
    class func getPhotosFromAlbum(id:String,
                                  successBlock:@escaping ((_ imageList: [String]?) -> Void),
                                  failureBlock:@escaping ((_ message:String) -> Void)) {
        FBSDKGraphRequest(graphPath: "\(id)",
            parameters: ["fields":"photos.fields(source,id,name,picture)"]).start(
                completionHandler: {
                    (connection, result, error) -> Void in
                    if error == nil {
                        guard let result = result as? [String:AnyObject] else {
                            return
                        }
                        if let photos = result["photos"] as? [String: AnyObject]{
                            var imageArray = [String]()
                            let images = photos["data"] as? [[String:AnyObject]]
                            for imageDictionary in images!{
                                let imageUrl = imageDictionary["source"] as! String
                                
                                imageArray.append(imageUrl)
                                if imageArray.count >= 6 {
                                    successBlock(imageArray)
                                    return
                                    
                                }
                            }
                            
                            if images!.count  == imageArray.count {
                                successBlock(imageArray)
                            }
                            
                        }else{
                            failureBlock(localizedString(forKey: "FAILED_TO_GET_PHOTOS"))
                        }
                        
                    } else{
                        failureBlock(localizedString(forKey: "FAILED_TO_GET_PHOTOS"))
                    }
            })
    }
    
}
