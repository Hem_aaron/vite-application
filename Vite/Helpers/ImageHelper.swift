//
//  ImageHelper.swift
//  Vite
//
//  Created by Eeposit1 on 2/15/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices
import AVFoundation

class ImageHelper:NSObject,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    var controller:UIViewController!
    var imagePicked:((_ image:UIImage) -> ())?
    var videoPath: ((_ video:URL) -> ())?
    
    init(controller:UIViewController) {
        super.init()
        self.controller = controller
        self.determineStatus {
            status in
        }
    }
    
    func determineStatus(_ completionHandler:(Bool)->()) {
        let status = PHPhotoLibrary.authorizationStatus()
        
        switch status {
        case .authorized:
            
            completionHandler(true)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization() {_ in}
            
        case .restricted:
            completionHandler(false)
            
        case.denied:
            let alert = UIAlertController(
                title: localizedString(forKey: "NEED_AUTHORIZATION"),
                message: localizedString(forKey: "AUTHORIZE_TO_USE_PHOTOS"),
                preferredStyle: .alert)
            
            let noAction = UIAlertAction(title: "No", style: .cancel, handler: { _ in })
            
            alert.addAction(noAction)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: { _ -> Void in
                let url = URL(string:UIApplicationOpenSettingsURLString)!
                UIApplication.shared.openURL(url)
                }
            )
            alert.addAction(okAction)
            self.controller.present(alert, animated: true, completion: nil)
            completionHandler(false)
        }
    }
    
    
    func getImageController(_ source:UIImagePickerControllerSourceType, allowEdit: Bool = false, isForBusinessAccount: Bool = false) {
        
        let ok = UIImagePickerController.isSourceTypeAvailable(source)
        if !ok {
            print("Source is not available")
            return
        }
        let arr = UIImagePickerController.availableMediaTypes(for: source)
        
        if arr == nil {
            print("no available types")
            return
        }
        
        let picker = UIImagePickerController()
        picker.sourceType = source
        picker.mediaTypes =  isForBusinessAccount ? ["public.image"] : arr!
        picker.delegate = self
        picker.allowsEditing = allowEdit
        controller.present(picker, animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let url = info[UIImagePickerControllerMediaURL] as? URL
        
        var im = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        let edim = info[UIImagePickerControllerEditedImage] as? UIImage
        
        if edim != nil {
            im = edim
        }
        controller.dismiss(animated: true) {
            
            let type = info[UIImagePickerControllerMediaType] as? String
            if type != nil {
                
                switch type! {
                case kUTTypeImage as NSString as String:
                    if im != nil {
                        self.imagePicked!(im!)
                    }
                    
                case kUTTypeMovie as NSString as String:
                    if url != nil {
                        self.videoPath!(url!)
                    }
                default:
                    print("Default")
                }
            }
        }
    }
}








