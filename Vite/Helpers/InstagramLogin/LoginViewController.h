//
//  LoginViewController.h
//  InstagramUnsignedAuthentication
//
//  Created by user on 11/13/14.
//  Copyright (c) 2014 Neuron. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UIWebViewDelegate>
{
    __weak IBOutlet UIWebView *loginView;
    IBOutlet UILabel *loadingLabel;
    __weak IBOutlet UIActivityIndicatorView *loginIndicate;
}
@property(strong,nonatomic)NSString *typeOfAuthentication;
@end
