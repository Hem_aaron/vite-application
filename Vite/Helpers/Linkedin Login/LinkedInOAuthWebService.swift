//
//  LinkedInOAuthWebService.swift
//  Mesh
//
//  Created by eeposit2 on 11/9/16.
//  Copyright © 2016 eepostIT. All rights reserved.
//

import Foundation
import OAuthSwift

class LinkedInOAuthWebService {
    let oauthswift = OAuth1Swift(
        consumerKey:    "818ivqapo9vlv4",
        consumerSecret: "sTeNCoKzOqZyE138",
        requestTokenUrl: "https://api.linkedin.com/uas/oauth/requestToken",
        authorizeUrl:    "https://api.linkedin.com/uas/oauth/authenticate",
        accessTokenUrl:  "https://api.linkedin.com/uas/oauth/accessToken"
    )
    
    func doOAuthLinkedin(_ success:@escaping ((_ credentials: OAuthSwiftCredential) -> Void), failure:@escaping ((_ error:AnyObject) -> Void)) {
        
        oauthswift.authorize(withCallbackURL: URL(string: "vite://oauth-callback/linkedin")!, success: { (credential, response, parameters) in
            
           success(credential)
        }, failure: { (error) in
            
            failure(error as AnyObject)
            print(error.localizedDescription)
        })

        
//        oauthswift.authorizeWithCallbackURL( , success: {
//            credential, response, parameters in
//            success(credentials: credential)
//            }, failure: { error in
//                failure(error: error)
//        })
    }
    
    
    func getLinkedInProfile(_ success:@escaping ((_ userResponse: [String: AnyObject]) -> Void), failure:@escaping ((_ error:AnyObject) -> Void)) {
     
        oauthswift.client.get("https://api.linkedin.com/v1/people/~?format=json", success: { (response) in
            do {
                let json = try   JSONSerialization.jsonObject(with: response.data, options: [])
                print(json)
                success(json as! [String : AnyObject])
            } catch let jsonError as NSError {
                failure(jsonError as AnyObject)
            }
        }) { (error) in
            failure(error as AnyObject)
        }
        
//        oauthswift.client.get("https://api.linkedin.com/v1/people/~?format=json", parameters: [:],
//                              success: {
//                                data, response in
//                                do {
//                                    let json = try NSJSONSerialization.JSONObjectWithData(data, options: [])
//                                    success(userResponse: json as! [String : AnyObject])
//                                } catch let jsonError as NSError {
//                                    failure(error: jsonError)
//                                }
//            }, failure: { error in
//                print(error)
//              failure(error: error)
//        })

    }
    
    
}
