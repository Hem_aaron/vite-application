//
//  MGLinkedinHelper.swift
//  LiSw
//
//  Created by Prajeet Shrestha on 11/7/16.
//  Copyright © 2016 Prajeet Shrestha. All rights reserved.
//

import Foundation
import UIKit

struct LiAPIConstants {
    static let permissions = ["r_basicprofile", "r_emailaddress"]
    static let people =  "https://www.linkedin.com/v1/people/~?format=json"
}

class MGLinkedinSDKWrapper {
    class func isSessionValid() -> Bool {
        return LISDKSessionManager.sharedInstance().session.isValid()
    }
    class func initSession(_ success:@escaping ((_ state:String) -> Void), error:((_ error:Any)-> Void)) {
        LISDKSessionManager.createSession(withAuth: LiAPIConstants.permissions, state: "Some state", showGoToAppStoreDialog: true, successBlock: { (returnState) in
            success(returnState!)
        }) { (error) in
            print(error)
        }
    }
    class func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        if (LISDKCallbackHandler.shouldHandle(url)) {

            return LISDKCallbackHandler.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        return true
    }
    
    class func GET(_ url:String, success:@escaping ((LISDKAPIResponse) -> Void), error:@escaping ((LISDKAPIError) -> Void)) {
        LISDKAPIHelper.sharedInstance().getRequest(url, success: { (response) in
            success(response!)
        }) { (err) in
            error(err!)
        }
        
    }
    
    class func POST(_ url:String, body:Data, success: @escaping ((LISDKAPIResponse) -> Void), error: @escaping ((LISDKAPIError) -> Void)) {
        LISDKAPIHelper.sharedInstance().postRequest(url, body: (body as NSData!) as Data!, success: { (response) in
            success(response!)
        }) { (err) in
            error(err!)
        }
    }

    class func POST(_ url:String, stringBody:String, success:@escaping ((LISDKAPIResponse) -> Void), error:@escaping ((LISDKAPIError) -> Void)) {
        LISDKAPIHelper.sharedInstance().postRequest(url, stringBody: stringBody, success: { (response) in
            success(response!)
        }) { (err) in
            error(err!)
        }
    }
    
    
    
}
