
//
//  MGLinkedinUserProfileService.swift
//  LiSw
//
//  Created by Prajeet Shrestha on 11/7/16.
//  Copyright © 2016 Prajeet Shrestha. All rights reserved.
//

import Foundation

class MGLinkedinUserProfileService {
    func getUserProfileUrl(_ success:@escaping ((_ userResponse: [String: AnyObject]) -> Void), error:@escaping ((_ error:AnyObject) -> Void)) {
        
        MGLinkedinSDKWrapper.GET(LiAPIConstants.people, success: { (response) in
            if let data = response.data.convertStringToDictionary(){
             success(data)
                }
        }) { (err) in
            error(err)
        }
    }
    
    func clearSession(){
        LISDKSessionManager.clearSession()
    }
}
