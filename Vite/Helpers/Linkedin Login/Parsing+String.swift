//
//  Parsing+String.swift
//  LiSw
//
//  Created by Prajeet Shrestha on 11/7/16.
//  Copyright © 2016 Prajeet Shrestha. All rights reserved.
//

import Foundation
extension String {
    func convertStringToDictionary() -> [String:AnyObject]? {
        if let data = self.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
}
