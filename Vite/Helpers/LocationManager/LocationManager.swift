//
//  LocationManager.swift
//  Vite
//
//  Created by Hem Poudyal on 12/20/17.
//  Copyright © 2017 Eepos IT. All rights reserved.
//
import Foundation
import UIKit
import MapKit
class LocationManager:NSObject, CLLocationManagerDelegate {
    //Instantiate Location manager
    let locationManager = CLLocationManager()
    static var isForPremiumLocation = false
    
    var fetchedUserLocation: ((_  location:CLLocationCoordinate2D) -> Void)?
    
    override init () {
        super.init()
        
    }
    
    func getUserLocation() {
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    
    @objc func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        //print("locations = \(locValue.latitude) \(locValue.longitude)")
        manager.delegate = nil
        manager.stopUpdatingLocation()
        self.fetchedUserLocation!(locValue)
        //self.fetchedUserLocation!(locValue)
    }
    
    class func plotPinAt(location:CLLocationCoordinate2D, mapView:MKMapView, title:String? , subTitle:String?) {
        
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        
        let annotation = MapPin(coordinate: location, title: title!, subtitle: subTitle!)
        mapView.addAnnotation(annotation)
    }
    
    class func getLocationStringFromCoordinate(coordinate:CLLocationCoordinate2D,
                                               completionBlock:@escaping ((_ location:String?) -> Void)) {
        
        let location =
            CLLocation (
                latitude: coordinate.latitude,
                longitude: coordinate.longitude
        )
        CLGeocoder().reverseGeocodeLocation(location,
                                            completionHandler: {
                                                (placemarks, error) -> Void in
                                                
                                                
                                                if error != nil {
                                                    print("Reverse geocoder failed with error" + error!.localizedDescription)
                                                    completionBlock(nil)
                                                    return
                                                }
                                                
                                                if placemarks!.count > 0 {
                                                    let pm = placemarks![0]
                                                    var locationDetail = ""
                                                    if !isForPremiumLocation {
                                                        if let name = pm.name {
                                                            locationDetail += name
                                                        }
                                                        if let  locality = pm.locality {
                                                            locationDetail += ", \(locality)"
                                                        }
                                                        if let  country = pm.country {
                                                            locationDetail += ", \(country)"
                                                        }
                                                    } else {
                                                        if let  locality = pm.locality {
                                                            locationDetail += "\(locality)"
                                                        }
                                                        if let  country = pm.country {
                                                            if pm.locality == nil {
                                                                locationDetail += "\(country)"
                                                            } else {
                                                                locationDetail += ", \(country)"
                                                            }
                                                        }
                                                    }
                                                    isForPremiumLocation = false
                                                    if let countryCode = pm.isoCountryCode {
                                                        let identifier = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: countryCode])
                                                        let _ = NSLocale(localeIdentifier:"en_US").displayName(forKey: NSLocale.Key.identifier, value: identifier)
                                                    }
                                                    
                                                    completionBlock(locationDetail)
                                                }
                                                else {
                                                    completionBlock(nil)
                                                    print("Problem with the data received from geocoder")
                                                }
        })
    }
    
    
    
}
