//
//  MapPin.swift
//  Vite
//
//  Created by Hem Poudyal on 12/20/17.
//  Copyright © 2017 Eepos IT. All rights reserved.
//

import MapKit
import UIKit

class MapPin : NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
}

