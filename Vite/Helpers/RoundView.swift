//
//  RoundedView.swift
//  Vite
//
//  Created by Eeposit1 on 4/3/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class RoundView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        let width = self.bounds.size.width
        self.layer.cornerRadius = width / 2
        self.clipsToBounds = true
    }
    
}
