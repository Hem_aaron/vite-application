//
//  TwitterApiService.swift
//  Mesh
//
//  Created by eeposit2 on 11/8/16.
//  Copyright © 2016 eepostIT. All rights reserved.
//

import Foundation
import TwitterKit

class TwitterApiService {
    
    var tClient: TWTRAPIClient? {
        get {

            let id = Twitter.sharedInstance().sessionStore.session()?.userID
            guard let userID = id else {
                return nil
            }
            return TWTRAPIClient(userID: userID)
            
        }
    }

    
    func getTwitterprofile(_ success:@escaping ((_ userResponse: [String: AnyObject]) -> Void), error:@escaping ((_ error:AnyObject) -> Void)) {
        let statusesShowEndpoint = "https://api.twitter.com/1.1/account/verify_credentials.json"
        let id = Twitter.sharedInstance().sessionStore.session()?.userID
        let params = ["id":id!]
        var clientError : NSError?
        
        guard let client = tClient else {
            return
        }
        let request = client.urlRequest(withMethod: "GET", url: statusesShowEndpoint, parameters: params, error: &clientError)
        
        client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
            if connectionError != nil {
                print("Error: \(connectionError)")
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: [])
                success(json as! [String : AnyObject])
                
            } catch let jsonError as NSError {
                error(jsonError)
            }
        }
    }
    
    func clearTwitterSession(){
        
        if let id = Twitter.sharedInstance().sessionStore.session()?.userID{
            Twitter.sharedInstance().sessionStore.logOutUserID(id)
        }
    }
}

