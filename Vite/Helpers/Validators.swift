//
//  Validators.swift
//  Royal
//
//  Created by Eeposit1 on 2/9/16.
//  Copyright © 2016 Prajeet Shrestha. All rights reserved.
//

import Foundation
//Email Validation Method
func isValidEmail(_ emailTxt:String) -> Bool	 {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    let emailCheck = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailCheck.evaluate(with: emailTxt)
}

func isRequiredStringSet(_ stringValue:String) -> Bool {
    return  stringValue.isEmpty
}

func isValidURL (_ stringURL : NSString) -> Bool {
    let urlRegEx = "(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/?)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))*(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’])*)"
    let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[urlRegEx])
    return predicate.evaluate(with: stringURL)
}
