//
//  ViewWithRoundedBorder.swift
//  Vite
//
//  Created by Eeposit1 on 2/14/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class ViewWithRoundedBorder: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
    }

}
