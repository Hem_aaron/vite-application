//
//  ViewWithRoundedCorner.swift
//  Vite
//
//  Created by Prajeet Shrestha on 5/15/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class ViewWithRoundedCorner: UIView {

    override func layoutSubviews() {
         super.layoutSubviews()
        self.layer.cornerRadius = 10.0
        self.clipsToBounds = true
    }
}
