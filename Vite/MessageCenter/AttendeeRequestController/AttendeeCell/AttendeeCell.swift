//
//  AttendeeTableViewCell.swift
//  Vite
//
//  Created by Eeposit1 on 3/14/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class AttendeeCell: UITableViewCell {

    @IBOutlet weak var userImage: CircleImage!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userInfo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
