//
//  AttendeeRequestCell.swift
//  Vite
//
//  Created by Eeposit1 on 3/14/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class RequestCell: UITableViewCell {
    @IBOutlet weak var userImage: CircleImage!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userInfo: UILabel!
    var rejectBlock:(() -> ())?
    var acceptBlock:(() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.increaseLineSpacing()
    }
    func increaseLineSpacing() {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1
        
        if let text = self.userInfo.text {
            let attrString = NSMutableAttributedString(string: text)
            //TODO attrString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(0, attrString.length))
            //attrString.addAttribute(NSAttributedStringKey.paragraphStyleNSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
            self.userInfo.attributedText = attrString
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func rejectRequest(_ sender: UIButton) {
        if let rejectBlock = self.rejectBlock {
            rejectBlock()
        }
    }
   
    
    @IBAction func acceptRequest(_ sender: UIButton) {
        if let acceptBlock = self.acceptBlock {
            acceptBlock()
        }
    }
   
}
