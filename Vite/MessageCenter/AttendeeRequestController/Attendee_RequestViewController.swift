//
//  Attendee&RequestViewController.swift
//  Vite
//
//  Created by Eeposit1 on 3/13/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import ObjectMapper

class Attendee_RequestViewController: UIViewController,BroadCastMessageScreenDelegate, UIGestureRecognizerDelegate   {
    let noAttendeeMessage = "There are no attendees for this event!"
    let noRequestsMessage = "There are no requests for this event!"
    @IBOutlet weak var btnBroadCast: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topSpaceLblMessage: NSLayoutConstraint!
    @IBOutlet weak var msgAllAttendeeLblHeight: NSLayoutConstraint!
    @IBOutlet weak var tblViewTopSpave: NSLayoutConstraint!
    
    
    var navToggleInitiated = false
    let lblMessage = UILabel()
    
    var user = UserDefaultsManager()
    var eventDetail : VEvent!
    var event:EventsOrganizer!
    var isForRequest:Bool = true
    var requesterList     = [RequestedUser]()
    var attendeeList      = [MyEventAttendee]()
    let navToggle         = ToggleButton(frame: CGRect(x: 0, y: 0, width: 220, height: 34), isInRight: false)
    
    let indicator         = ActivityIndicator()
    let broadcastView     = BroadCastMessageScreen(frame:UIScreen.main.bounds)
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigation()
        self.tableView.tableFooterView = UIView()
        self.broadcastView.delegate    = self
        // self.btnBroadCast.hidden       = true
        hideBroadCastBtn(height: 0,topSpace: 0)
        self.addLabelWithMessage(message: "");
        self.addRefreshControl()
          //swipe back
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getRequestAndAttendees()
    }
    
    func addRefreshControl(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refreshHandle(sender:)), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
    }
    func refreshHandle(sender:AnyObject) {
        self.getRequestAndAttendees()
    }
    func getRequestAndAttendees(){
        if self.navToggle.isInRight {
            self.reloadTableForAttendees()
        }
        else{
            self.reloadTableForRequest()
        }
        
    }
    
    func hideBroadCastBtn(height: CGFloat, topSpace : CGFloat){
        msgAllAttendeeLblHeight.constant = height
        tblViewTopSpave.constant = topSpace
        topSpaceLblMessage.constant = topSpace
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        guard let nav = self.navigationController else {
            return
        }
        nav.popViewController(animated: true)
    }
    
    //MARK: Broadcast Message Delegate Method
    func sendMessage( _ message:String) {
        
        let msg     = message.trimmingCharacters(in: .whitespaces)
        if msg.isEmpty {
            return
        }
        
        let currentUser             = ChatUser()
        currentUser.fbID            = user.isBusinessAccountActivated ? user.userBusinessAccountID! : user.userFBID!
        currentUser.name            = user.isBusinessAccountActivated ? user.userBusinessFullName! : user.userFBFullName!
        currentUser.profileImageUrl = user.isBusinessAccountActivated ? user.userBusinessProfileImageUrl :"http://graph.facebook.com/\(user.userFBID!)/picture?type=large"
        
        let message = GroupChatMessageModel(userName: currentUser.name!, userId:currentUser.fbID! , userImage: currentUser.profileImageUrl, message: msg ,mediaImageUrl: "",
                                            mediaVideoUrl: "",isMedia: false,isVideo: false)
        AttendeeService.getAllPartiesOfEvent(eventId: self.eventDetail.uid!, successBlock: { [weak self] (users) in
            let event = NEvent(event: (self?.eventDetail)!)
            GroupChatService.sendMessage(message, event:event, usersInGroup: users )
            }, failureBlock: { (message) in
        })
  }

    @IBAction func broadCast(_ sender: UIButton) {
        let controller                    = Route.broadCastVC
        controller.view.backgroundColor   = UIColor.clear
        controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
        /*
         self.broadcastView.frame = UIScreen.mainScreen().bounds
         
         
         print(UIApplication.sharedApplication().windows.last?.rootViewController!.view)
         UIApplication.sharedApplication().windows.last?.rootViewController!.view.addSubview(self.broadcastView)
         */
    }
  
    
    func setupNavigation() {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = true
        navToggle.delegate                  = self
        self.navigationItem.titleView       = navToggle
        if let requestCount = self.eventDetail.requestCount,
            let attendeeCount =  self.eventDetail.attendeeCount {
            self.navToggle.lblFirstHalf.text         = "Requests (\(requestCount))"
            self.navToggle.lblSecondHalf .text       = "Attendees (\(attendeeCount))"
        }
        self.navToggle.realignLabels()
        
        self.reloadTableForRequest()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK: ToggleButton Delegate
extension Attendee_RequestViewController: ToggleButtonDelegate {
    func toggleIndex(index:Int) {
        switch index {
        case 0:
            self.reloadTableForRequest()
            break
        case 1:
            self.reloadTableForAttendees()
            break
        default:
            break
        }
    }
}

//MARK: Private Methods
extension Attendee_RequestViewController {
    
    func reloadTableForRequest() {
        //btnBroadCast.hidden = true
        hideBroadCastBtn(height: 0, topSpace: 0)
        AppDel.beginIgnoring()
        self.isForRequest   = true
        self.indicator.start()
        self.view.isUserInteractionEnabled = false
        guard let eventId = self.eventDetail.uid else {
            showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: localizedString(forKey: "PROBLEM_WITH_EVENT"))
            AppDel.endIgnoring()
            self.indicator.stop()
            guard let nav = self.navigationController else {
                return
            }
            nav.popViewController(animated: true)
            return
        }
       
        EventServices().getEventRequesters(eventId) { [weak self] (requesterList) in
            if let list        = requesterList {
                                if(list.count == 0)
                                {
                                    self?.addLabelWithMessage(message: (self?.noRequestsMessage)!)
                                    self?.lblMessage.isHidden = false
                                    self?.tableView.reloadData()
                                }
                                else{
                                    self?.lblMessage.isHidden = true
                                    self?.requesterList = list
                                    self?.tableView.reloadData()
                                }
                
                self?.view.isUserInteractionEnabled = true
                
                            } else {
                self?.requesterList.removeAll()
                self?.addLabelWithMessage(message: (self?.noRequestsMessage)!)
                                print("No event requesters")
                self?.view.isUserInteractionEnabled = true
                
                            }
            self?.view.isUserInteractionEnabled = true
            self?.indicator.stop()
            self?.refreshEventDetail()
                            AppDel.endIgnoring()
        }
    }
    
    func reloadTableForAttendees() {
        self.isForRequest   = false
        self.indicator.start()
        AppDel.beginIgnoring()
        
        self.view.isUserInteractionEnabled = false
        guard let eventId = self.eventDetail.uid else {
            return
        }
        EventServices.getEventAttendees(eventID: eventId)
        {
         [weak self]   (attendeeList) -> () in
            if let list = attendeeList {
                if(list.count == 0)
                {
                    self?.addLabelWithMessage(message: (self?.noAttendeeMessage)!)
                    self?.lblMessage.isHidden = false
                }
                else{
                    self?.lblMessage.isHidden = true
                    self?.attendeeList = list
                }
                self?.tableView.reloadData()
                self?.view.isUserInteractionEnabled = true
                if self?.attendeeList.count == 0 {
                    //self.btnBroadCast.hidden = true
                    self?.hideBroadCastBtn(height: 0,topSpace: 0)
                } else {
                    //self.btnBroadCast.hidden = false
                    self?.hideBroadCastBtn( height: 29,topSpace: 16)
                }
            } else {
                self?.attendeeList.removeAll()
                print("No event attendees")
                self?.view.isUserInteractionEnabled = true
            }
            self?.indicator.stop()
            self?.refreshControl.endRefreshing()
            AppDel.endIgnoring()
            self?.refreshEventDetail()
        }
    }
    
    func addLabelWithMessage(message:String) {
        lblMessage.text      = message
        lblMessage.textColor = UIColor.lightGray
        lblMessage.sizeToFit()
        let midX = UIScreen.main.bounds.midX
        let midY = UIScreen.main.bounds.midY
    
        lblMessage.center    = CGPoint(x: midX, y: midY - 64)
            //CGPointMake(x,y - 64)
        lblMessage.font      = UIFont.systemFont(ofSize: 17)
        if self.view.subviews .contains(self.lblMessage) {
            return
        } else {
            view.addSubview(lblMessage)
        }
    }
}

//MARK: TableView delegate and data source
extension Attendee_RequestViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isForRequest {
            return self.requesterList.count
        } else {
            return self.attendeeList.count
        }
    }
    
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isForRequest {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RequestCell") as! RequestCell
            if self.requesterList.count > 0 {
                let requester      = self.requesterList[indexPath.row]
                cell.userName.text = requester.eventUserFullName
                cell.userInfo.text = requester.info
                if let profileUrl = requester.userProfileImageUrl {
                    let url = profileUrl.convertIntoViteURL()
             
                    cell.userImage.sd_setImage(with: URL(string: url), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
                 
                }
              
                cell.rejectBlock = { [weak self] in
                    self?.respondToEvent(responseStatus: false, fbID: String(requester.facebookId!),indexPath: indexPath)
                }
                cell.acceptBlock = {[weak self] in
                    self?.respondToEvent(responseStatus: true, fbID: String( requester.facebookId!),indexPath: indexPath)
                }
            }
            else{
                self.addLabelWithMessage(message: self.noAttendeeMessage)
            }
            
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AttendeeCell") as! AttendeeCell
            let attendee        = self.attendeeList[indexPath.row]
            cell.userName.text  = attendee.fullName
            if let profileImage = attendee.profileImageUrl {
                cell.userImage.sd_setImage(with:URL(string: profileImage.convertIntoViteURL()))
            }
            cell.userInfo.text = attendee.info;
            
            return cell
        }
    }
    func respondToEvent(responseStatus:Bool,fbID:String, indexPath:IndexPath) {
        self.indicator.start()
        AppDel.beginIgnoring()
        guard let eventId = self.eventDetail.uid else {
            return
        }
        EventServices.respondToRequest(status: responseStatus,
                                            userFbID: fbID,
                                            eventID: eventId,
                                            completionBlock: {
                                            [weak self]    (status) -> () in
                                                if status {
                                                    
                                                    if responseStatus {
                                                        self?.sendNotificationToAcceptedUser(indexPath: indexPath as NSIndexPath)
                                                    }
                                                    
                                                    CATransaction.begin()
                                                    CATransaction.setCompletionBlock({
                                                        self?.tableView.reloadData()
                                                        self?.refreshEventDetail()
                                                        self?.indicator.stop()
                                                        AppDel.endIgnoring()
                                                    })
                                                    self?.tableView.beginUpdates()
                                                    self?.requesterList.remove(at: indexPath.row)
                                                    self?.tableView.deleteRows(at: [indexPath], with: .automatic)
                                                    self?.tableView.endUpdates()
                                                    CATransaction.commit()
                                                    if(self?.requesterList.count == 0){
                                                        self?.addLabelWithMessage(message: (self?.noRequestsMessage)!)
                                                        self?.lblMessage.isHidden = false
                                                    }
                                                    
                                                } else {
                                                    print("Failed to respond to request")
                                                }
        })
    }
    
    func sendNotificationToAcceptedUser(indexPath:NSIndexPath) {
        let requester = self.requesterList[indexPath.row]
        let evnt      = NEvent(name: self.event.eventTitle!, uid: self.event.entityId!, imageUrl: self.event.imageUrl!.convertIntoViteURL())
        let sender    = UserDefaultsManager.getActiveUserForNotification()
        let receiver  = NReceiver(uid: String(requester.facebookId!)) // entityId == facebookId
        NotificationService().triggerNotificationWithEvent(.RequestAccepted, event: evnt, sender: sender, receiver: receiver)
    }
    
    func refreshEventDetail() {
        guard let eventId = self.eventDetail.uid else {
            return
        }
        EventServices.getEventDetail(eventID: eventId, successBlock: {[weak self] (event) in
            guard let e = event else {
                return
            }
            self?.event = e
            if let requestCount = self?.event.requestCount,
                let attendeeCount =  self?.event.attendeeCount {
                self?.navToggle.lblFirstHalf.text         = "Requests (\(requestCount))"
                self?.navToggle.lblSecondHalf .text       = "Attendees (\(attendeeCount))"
            }
            self?.navToggle.realignLabels()
        })
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       let controller = Route.profileViewController

        if self.isForRequest {
            let requester                        = self.requesterList[indexPath.row]
            controller.fbId                      = String(requester.facebookId!)
            controller.isForViewingOthersProfile = true
            controller.isFromAttendeeRequest     = true
        } else {
            let attendee                         = self.attendeeList[indexPath.row]
            controller.fbId                      = String(describing: attendee.facebookId!)
            controller.isForViewingOthersProfile = true
            controller.isFromAttendeeRequest     = true
        }
        self.navigationController?.show(controller, sender: nil)
    }
}
