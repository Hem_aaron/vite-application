//
//  BroadCastMessageScreen.swift
//  Vite
//
//  Created by Eeposit1 on 3/15/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class BroadCastMessageScreen: UIView {
    @IBOutlet weak var innerView: UIView!
    var delegate:BroadCastMessageScreenDelegate?
    @IBOutlet weak var message: UITextView!
    @IBOutlet var view: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("BroadcastMessageScreen", owner: self, options: nil)
        self.view.frame                   = self.bounds
        self.innerView.layer.cornerRadius = 10.0
        self.innerView.clipsToBounds      = true
        self.addSubview(self.view)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        Bundle.main.loadNibNamed("BroadcastMessageScreen", owner: self, options: nil)
        self.view.frame = self.bounds
        self.addSubview(self.view)
        
    }
    
    @IBAction func remove(_ sender: AnyObject) {
        self.message.text = ""
        self.removeFromSuperview()
    }
    
    @IBAction func hideKeyboard(_ sender: AnyObject) {
        self.message.resignFirstResponder()
    }
    
    @IBAction func broadCastMessage(_ sender: AnyObject) {
        if self.message.text == "" {
            return
        }
        self.delegate?.sendMessage(self.message.text)
        
    }
}
