//
//  BroadcastViewController.swift
//  Vite
//
//  Created by Eeposit1 on 6/1/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
protocol BroadCastMessageScreenDelegate : class{
    func sendMessage(_ message:String)
}

class BroadcastViewController: UIViewController {

    @IBOutlet var txtMessage: UITextView!
    @IBOutlet var viewBackground: UIView!
   weak var delegate:BroadCastMessageScreenDelegate?
    
    @IBAction func tapDismis(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeAction(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func broadcastAction(_ sender: AnyObject) {
        if let del = self.delegate {
            del.sendMessage(self.txtMessage.text)
            self.dismiss(animated: true, completion: nil)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.2) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
            
        }
        
    }
}
