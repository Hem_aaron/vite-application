//
//  MCConversationCell.swift
//  Vite
//
//  Created by Eeposit1 on 3/10/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class MCConversationCell: UITableViewCell {

    @IBOutlet weak var userImage: CircleImage!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserLastMessage: UILabel!
    @IBOutlet var lblMsgCount: UILabel!
    
    @IBOutlet var countContainer: RoundView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func cellSelected(sender: AnyObject) {
        print("Tapped")
    }

}
