//
//  MCConversationViewController.swift
//  Vite
//
//  Created by Eeposit1 on 2/28/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import Firebase
import ViewAnimator

class MCConversationViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var conversationUsersTable: UITableView!
    var historyList = [GroupChatMessageModel]()
    var flag = 0
    var blockedUsersList : [VUser]?
    var blockedId = [String]()
    var dataIsLoadedOnce = false
    var service = ChatHistoryService()
    var shouldShowChatViewController = true
    var showAnim:Bool?
    var indicator = ActivityIndicator()
    override func viewDidLoad() {
        super.viewDidLoad()
        showAnim = true
        conversationUsersTable.dataSource = self
        conversationUsersTable.delegate   = self
        indicator.start()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getBlockedUsersList()
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.historyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let history = self.historyList[indexPath.row]
      
        let cell                     = tableView.dequeueReusableCell(withIdentifier: "MCConversationCell") as! MCConversationCell
        
        var title = ""
        var imageUrl = ""
        var message = ""
        
        if let _ = history.eventId , let name = history.eventName , let image = history.eventImageUrl , let msg = history.message , let userDisplayName = history.userDisplayName {
            title = name
            imageUrl = image
            message = userDisplayName + " : " + msg
        } else {
            title = history.userDisplayName ?? ""
            imageUrl = history.userProfileURL ?? ""
            message = history.message ?? ""
        }
        cell.lblUserName.text        = title
        cell.lblUserLastMessage.text = message
        
        if history.unseenCount == 0 {
            cell.countContainer.isHidden = true
            
        } else {
            cell.countContainer.isHidden = false
            cell.lblMsgCount.text        = "\(history.unseenCount!)"
        }
        
        let url = imageUrl.convertIntoViteURL()
        cell.userImage.sd_setImage(with: URL(string: url), placeholderImage:(UIImage(named:"ProfilePlaceholder.png")))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !self.shouldShowChatViewController {
            return
        }
        self.shouldShowChatViewController = false
        
        let chatBoard = UIStoryboard(name: "Chat", bundle: nil)
        let history = self.historyList[indexPath.row]
        let sender             = ChatUser()
        sender.fbID            =  UserDefaultsManager().isBusinessAccountActivated ? UserDefaultsManager().userBusinessAccountID! : UserDefaultsManager().userFBID!
        sender.name            =  UserDefaultsManager().isBusinessAccountActivated ? UserDefaultsManager().userBusinessFullName! : UserDefaultsManager().userFBFullName!
        sender.profileImageUrl =  UserDefaultsManager().isBusinessAccountActivated ? UserDefaultsManager().userBusinessProfileImageUrl :"http://graph.facebook.com/\(UserDefaultsManager().userFBID!)/picture?type=large"
      
        let receiver             = ChatUser()
        receiver.fbID            = history.userFBID!
        receiver.name            = history.userDisplayName!
        receiver.profileImageUrl = (history.userProfileURL ?? "").convertIntoViteURL()
       
        if let eventid = history.eventId {
            let groupController = chatBoard.instantiateViewController(withIdentifier: "GroupChatViewController") as! GroupChatViewController
            groupController.currentUser = sender
            
            let event = NEvent(name: history.eventName ?? "" , uid: history.eventId ?? "", imageUrl: history.eventImageUrl ?? "")
            groupController.event = event
            
            AttendeeService.getAllPartiesOfEvent(eventId: eventid, successBlock: { (users) in
                groupController.users = users
                self.shouldShowChatViewController = true
                
                self.navigationController?.show(groupController, sender: nil)
                
                }, failureBlock: { (message) in
                      self.shouldShowChatViewController = true
                    showAlert(controller: self, title: "", message: message)
            })
            
        } else {
            self.shouldShowChatViewController = true
            let chatController      = chatBoard.instantiateViewController(withIdentifier: "ChatMainViewController") as! ChatMainViewController
            chatController.receiver = receiver
            chatController.sender   = sender
            self.navigationController?.show(chatController, sender: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let history = self.historyList[indexPath.row]
            
            if let eventid = history.eventId {
                GroupChatService.deleteChatHistoryForEvent(eventid, userId: UserDefaultsManager.getCurrentChatUser().fbID!)
            } else {
                ChatHistoryService().deleteChatHistory(UserDefaultsManager().isBusinessAccountActivated ? UserDefaultsManager().userBusinessAccountID! : UserDefaultsManager().userFBID!, receiver: history.userFBID!)
                    self.viewDidAppear(true)
            }
        }
    }
    
    @IBAction func messageAnyOneAction(_ sender: AnyObject) {
        self.navigationController?.show(Route.AllUser, sender: nil)
    }
    
    func getBlockedUsersList () {
        VProfileWebService.getBlockedUserList(successBlock: { [unowned self] (blockedUsers) in
            self.blockedUsersList = blockedUsers
            for fbid in self.blockedUsersList! {
                self.blockedId.append(String(describing: (fbid.uid!)))

            }
            //TODO:
            self.sortHistorylist()
            
        }) { [unowned self] (message) in
            showAlert(controller: self, title: localizedString(forKey: "ERROR"), message: message)
        }
    }
    
    func removeNilChat (){
       // print(self.blockedId)
        self.historyList = self.historyList.filter({$0.userFBID != nil})
        
    }
    
    func filterBlockedId () {
        for blockedUser in self.blockedId {
            self.historyList = self.historyList.filter({$0.userFBID! != blockedUser })
        }
    }
    
    func sortHistorylist() {
        self.service.getMergedChatHistoryForCurrentUser { (status, chatHistoryList) in
            if status {
                self.historyList = chatHistoryList!
                self.historyList.sort(by: {
                    
                    guard let firstT = $0.timeString, let secT =  $1.timeString else  {
                        return false
                    }
                    return  firstT.doubleValue > secT.doubleValue
                })
                
                self.removeNilChat()
                self.filterBlockedId()
                self.indicator.stop()
                self.conversationUsersTable.reloadData()
                if self.showAnim! {
                    self.showAnim = false
                    self.conversationUsersTable.animate(animations: [AnimationsList.fromRight])
                }
                
            } else {
                //TODO:Handle Error
                //self.historyList = []
                //self.conversationUsersTable.reloadData()
            }
        }
        
    }
    
}


