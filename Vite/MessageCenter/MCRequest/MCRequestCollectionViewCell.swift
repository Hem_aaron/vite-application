//
//  MCRequestCollectionViewCell.swift
//  Vite
//
//  Created by Eeposit1 on 2/28/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class MCRequestCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventImage: CircleImage!
    
    @IBOutlet weak var requestContainer: RoundView!
    @IBOutlet weak var lblRequestCount: UILabel!
}
