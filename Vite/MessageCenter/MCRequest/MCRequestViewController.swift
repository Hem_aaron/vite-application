//
//  RequestViewController.swift
//  Vite
//
//  Created by Eeposit1 on 2/28/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import ViewAnimator
class MCRequestViewController: UIViewController {
    
    @IBOutlet weak var lblRequestsCount: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    var showAnim : Bool?
    var events = [VEvent]()
    var indicator = ActivityIndicator()
    let cursor = VCursor()
    override func viewDidLoad() {
        super.viewDidLoad()
        showAnim = true
        self.lblRequestsCount.text     = "My Events "
        self.collectionView.dataSource = self
        self.collectionView.delegate   = self
      //  self.automaticallyAdjustsScrollViewInsets = false
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.fetchEvents(shouldReset: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.indicator.stop()
    }
}

extension MCRequestViewController {
    func fetchEvents(shouldReset reset:Bool) {
        indicator.tintColor = UIColor.gray
        indicator.parentView = view
        indicator.start()
        
        if reset {
            self.cursor.reset()
        }
        
        EventServices.getEventListWithPagination(cursor.nextPage(), pageSize: cursor.pageSize, successBlock: { (eventList, eventListEventOrg, totalCount)  in
            self.indicator.stop()
            guard let eventList = eventList else {
                showAlert(controller: self, title: "", message: localizedString(forKey: "COULDN'T_LOAD_EVENTS"))
                
                return
            }
            
            if reset {
                self.events.removeAll()
            }
            
            self.events.append(contentsOf: eventList)
            self.cursor.totalCount = totalCount.intValue
            self.cursor.totalLoadedDataCount = self.events.count
            
           
            self.collectionView.reloadData()
           
            if self.showAnim! {
                self.collectionView.animate(animations: [AnimationsList.fromRight])
                self.showAnim = false
            }
            
        }) { (message) in
            showAlert(controller: self, title: "", message: message)
            self.indicator.stop()
        }
        
    }
}

extension MCRequestViewController:UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return events.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MCRequestCollectionViewCell", for: indexPath) as! MCRequestCollectionViewCell
        
        let event                 = events[indexPath.row]
        cell.eventTitle.text      = event.eventTitle
        cell.eventImage.image     = nil
        cell.lblRequestCount.text = "\(event.requestCount!)"
        
        if event.requestCount!.int32Value == 0 {
            cell.requestContainer.isHidden = true
        } else {
            cell.requestContainer.isHidden = false
        }
        
        if event.fileType == "IMAGE" {
            let url = event.imageUrl!.convertIntoViteURL()
            cell.eventImage.sd_setImage(with: URL(string: url))
        } else {
          if let eventVideoProfileThumbNail = event.videoProfileThumbNail {
            let url = eventVideoProfileThumbNail.convertIntoViteURL()
            cell.eventImage.sd_setImage(with: URL(string: url))
          }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = Route.attendeeRequest
        controller.eventDetail = self.events[indexPath.row]
        self.navigationController?.show(controller, sender: nil)
        
      //  let controller = Controller.AttendeeRequest
//        controller.eventDetail = self.events[indexPath.row]
//
//        self.navigationController?.show(controller, sender: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.cursor.hasNextPage() && indexPath.row == (self.events.count - 1) {
            self.fetchEvents(shouldReset: false)
        }
    }
}

extension MCRequestViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 82, height: 122)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 8, 0, 8)
    }
}
