//
//  MCVitesTableViewCell.swift
//  Vite
//
//  Created by Eeposit1 on 2/28/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class MCVitesTableViewCell: UITableViewCell {
    let activeColor = UIColor(rgb: 0x62ADED)
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var eventOrganizer: UILabel!
    @IBOutlet weak var eventTime: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.containerView.layer.cornerRadius = 4
        self.containerView.layer.borderWidth = 2
        self.containerView.layer.borderColor = activeColor.cgColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            self.containerView.backgroundColor = activeColor
            self.eventTime.textColor = UIColor.white
            self.eventOrganizer.textColor = UIColor.white
            //self.btnDelete.tintColor = UIColor.whiteColor()
        } else {
            self.containerView.backgroundColor = UIColor.white
            self.eventOrganizer.textColor = activeColor
            self.eventTime.textColor = activeColor
            
            //self.btnDelete.tintColor = activeColor
        }
    }
    @IBAction func deleteAction(_ sender: AnyObject) {
    }
    
}
