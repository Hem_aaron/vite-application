//
//  MCVItesViewController.swift
//  Vite
//
//  Created by Eeposit1 on 2/28/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import ObjectMapper

class MCVitesViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var lblVitesCount: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var vitesArray = [MCVite]()
    
    var isLoadedAlready:Bool = false
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.fetchVites()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.activityIndicator.stopAnimating()  
    }
    
    func setUpComingSoonLbl(text:String) {
        let lblVite = UILabel()
        lblVite.text = text
        lblVite.textColor = UIColor.lightGray
        lblVite.sizeToFit()
        lblVite.center = self.view.center
        lblVite.font = UIFont.systemFont(ofSize: 14)
        self.view.addSubview(lblVite)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        self.lblVitesCount.text = "Vites (\(self.vitesArray.count))"
        return self.vitesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell  {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MCVitesTableViewCell") as! MCVitesTableViewCell
        let vite = self.vitesArray[indexPath.row]
        cell.eventOrganizer.text = vite.eventTitle
        cell.eventTime.text = vite.createdDate
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Need to work on it
//        let storyboard    = UIStoryboard(name: "MyVites", bundle: nil)
//        let controller    = storyboard.instantiateViewControllerWithIdentifier("ViteDetailsVC") as! ViteDetailsViewController
//        let vite          = self.vitesArray[indexPath.row]
//        controller.viteID = vite.entityID
//        self.navigationController?.showViewController(controller, sender: nil)
    }
}

extension MCVitesViewController {
    func fetchVites() {
        
        if !isLoadedAlready {
            isLoadedAlready = true
            self.activityIndicator.startAnimating()
        }
        
        WebService.request(method: .get, url: kAPIMCVite!, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            var vitesArr = [MCVite]()
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            
            if let vitesObject = param["vites"] as? [[String:AnyObject]] {
                for viteData in vitesObject {
                    if let viteInfo = Mapper<MCVite>().map(JSON: viteData){
                         vitesArr.append(viteInfo)
                    }
                }
                
            }
    }) { (message, ApiErrorMsg) in
            self.activityIndicator.stopAnimating()
        }
    }
}
