//
//  MessageCenterTableViewController.swift
//  Vite
//
//  Created by Eeposit1 on 2/28/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import MIBadgeButton_Swift

class MessageCenterTableViewController: WrapperTableViewController {
    
    var isFromSideMenu: Bool = true
    var notificationCount:Int?
    
    @IBOutlet var notificationButton: MIBadgeButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Message Center"
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        let user = UserDefaultsManager.getActiveUserForNotification()
        NotificationService().resetUnseenMessageCount(user.uid)
        notificationButton.badgeEdgeInsets = UIEdgeInsetsMake(15, 15, 1, 15)
        notificationButton.badgeBackgroundColor = UIColor.orange
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getNotificationCount()
        self.view.isUserInteractionEnabled = false
        super.viewWillAppear(animated)
        self.view.isUserInteractionEnabled = true
        self.notificationButton.isEnabled = true
    }
    
    //Mark: NotificationCount Api Call
    func getNotificationCount() {
        NotificationServices.getNotificationCount({ (count) in
            self.notificationCount = count
            if count > 0 {
                if let count = self.notificationCount{
                    self.notificationButton.badgeString = String(count)
                }
            } else {
                self.notificationButton.badgeString = nil
            }
        }) { (message) in
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UserDefaults.standard.set(false, forKey: "msgCenterNotification")
        self.title = "Message Center"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 175
        } else if indexPath.row == 1 {
            return 0
            
        } else {
            let screenHeight = self.tableView.frame.height
            return screenHeight - 200
        }
    }
    @IBAction func back(_ sender: AnyObject) {
        if isFromSideMenu {
            self.dismiss(animated: true, completion: nil)
        } else {
            Route.goToHomeController()
        }
    
    }
    
    @IBAction func searchAnyoneForMsg(_ sender: AnyObject) {
        self.notificationListViewed()
        self.view.isUserInteractionEnabled = false
        self.notificationButton.isEnabled = false
        self.navigationController?.pushViewController(Route.notificationVC, animated: true)
    }
    
    func notificationListViewed () {
        
        NotificationServices.notificaltionListViewed({ (successMsg) in
        }) { (messgae) in
            showAlert(controller: self, title: "", message: messgae)
        }
    }
}
