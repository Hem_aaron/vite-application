//
//  MessageCenterWebService.swift
//  Vite
//
//  Created by Hem Poudyal on 3/12/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import ObjectMapper

class MessageCenterWebService {
    func getUserInfoWithFBId(fbIDList:[String],
                             successBlock:@escaping (([ChatUser]) -> ()),
                             failure:@escaping  ((_ message:String) -> ())) {
        var chatUsers = [ChatUser]()
        let parameters:[String:AnyObject] =
            [
                "fbIdList":fbIDList as AnyObject
            ]
        WebService.request(method: .post, url: kAPIGetUserInfoWithFBID, parameter: parameters , header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] ,
            let params = response["params"] as? [String:AnyObject] ,
                  let success  = response["success"] as? Bool else {
                return
            }
            let users = params["users"] as? NSArray
            if success {
                
                for dictionary in users! {
                    let dictionary = dictionary as! [String:AnyObject]
                    let chatUser = ChatUser()
                    chatUser.profileImageUrl = dictionary["profileImageUrl"] as! String
                    chatUser.name            = dictionary["fullName"] as! String
                    chatUser.fbID            = String(describing: dictionary["facebookId"]!)
                    chatUsers.append(chatUser)
                }
                
                successBlock(chatUsers)

            } else {
                failure(localizedString(forKey: "FAILED_TO_GET_USERS"))

            }
          
        }) { (message, Error) in
            failure(localizedString(forKey: "FAILED_TO_GET_USERS"))
        }
    }
}
