//
//  MCRequest.swift
//  Vite
//
//  Created by Eeposit1 on 2/28/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class MCRequest: NSObject {
    var eventId:String?
    var eventTitle:String?
    var imageUrl:String?
    var requestCount:String?
}
/*
Optional({
message = "Requests have been retrieved successfully";
params =     {
events =         (
{
entityId = "1456644810005_99";
eventTitle = asdfasdf;
imageUrl = "/bucket/Events/Event_1456644809656.jpeg";
requestCount = 1;
}
);
};
success = 1;
})
}

*/