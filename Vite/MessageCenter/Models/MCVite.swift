//
//  MCVites.swift
//  Vite
//
//  Created by Eeposit1 on 2/29/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import ObjectMapper

class MCVite: Mappable {
    
    var entityID: String?
    var createdDate:String?
    var eventTitle: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        entityID             <- map["entityId"]
        createdDate          <- map["createdDate"]
        eventTitle           <- map["eventTitle"]
      }
}
