//
//  ToggleSwitch.swift
//  TestToggle
//
//  Created by Hem Poudyal on 4/7/16.
//  Copyright © 2016 Prajeet Shrestha. All rights reserved.
//

import UIKit
protocol ToggleButtonDelegate : class{
    func toggleIndex(index:Int)
}
class ToggleButton: UIView {
    var backImage:UIImage    = UIImage(named: "SlideOn.png")!
    var movableImage:UIImage = UIImage(named: "SlideOff.png")!
    
    var movableImageView:UIImageView!
    var isInRight:Bool
    var lblFirstHalf:UILabel!
    var lblSecondHalf:UILabel!
  weak  var delegate:ToggleButtonDelegate?
    
    init(frame: CGRect, isInRight:Bool) {
        self.isInRight = isInRight
        super.init(frame: frame)
        self.setupImageViews()
        self.setupLabels()
        self.setupTransparentButton()
    
    }
    
    func setupTransparentButton() {
        let btn = UIButton(frame: self.bounds)
        btn.addTarget(self, action: #selector(tappedButton), for: .touchUpInside)
        self.addSubview(btn)
    }
    
    @objc func tappedButton() {
        self.isInRight = !self.isInRight
        self.toggleState(toRight: self.isInRight)
    }
    
    func toggleState(toRight:Bool) {
        self.isInRight = toRight
        if toRight {
            
            UIView.animate(withDuration: 0.1) {
              [weak self]  () -> Void in
                self?.movableImageView.frame  = (self?.getFrameForMovableImageView(statusRight: true))!
                self?.lblFirstHalf.textColor  = UIColor.white
                self?.lblSecondHalf.textColor = UIColor.purple
                self?.layoutIfNeeded()
            }
        } else {
            
            UIView.animate(withDuration: 0.1) {
             [weak self]   () -> Void in
                self?.movableImageView.frame  = (self?.getFrameForMovableImageView(statusRight: false))!
                self?.lblFirstHalf.textColor  = UIColor.purple
                self?.lblSecondHalf.textColor = UIColor.white
                self?.layoutIfNeeded()
            }
        }
        
        if let del = self.delegate {
            if toRight {
                del.toggleIndex(index: 1)
            } else {
                del.toggleIndex(index: 0)
            }
        }
    }
    
    
    func setupImageViews() {
        let backImageView     = UIImageView(frame: self.bounds)
        backImageView.image   = self.backImage
        
        self.movableImageView = UIImageView(frame: self.getFrameForMovableImageView(statusRight: self.isInRight))
        movableImageView.image = self.movableImage
        self.addSubview(backImageView)
        self.addSubview(movableImageView)
    }
    
    func getFrameForMovableImageView(statusRight:Bool) -> CGRect {
        let width             = self.bounds.size.width / 2.1044776119
        let height            = self.bounds.size.height / 1.4117647059
        let yPosition         = (self.bounds.height / 2) - height / 2
        
        var xPosition:CGFloat
        if !statusRight {
            xPosition = 8;
        } else {
            xPosition = width
        }
        return CGRect(x: xPosition, y: yPosition, width: width, height: height)
        
    }
    
    func realignLabels() {
        
        self.lblFirstHalf.sizeToFit()
        self.lblSecondHalf.sizeToFit()
        let frame1                = self.getFrameForMovableImageView(statusRight: false)
        self.lblFirstHalf.center  = CGPoint(x:frame1.midX , y: frame1.midY)
        let frame2                = self.getFrameForMovableImageView(statusRight: true)
        self.lblSecondHalf.center = CGPoint(x:frame2.midX , y: frame2.midY)
        
        if isInRight {
            self.lblFirstHalf.textColor  = UIColor.white
            self.lblSecondHalf.textColor = UIColor.purple
        } else {
            
            self.lblFirstHalf.textColor  = UIColor.purple
            self.lblSecondHalf.textColor = UIColor.white
            
        }
    }
    
    let offset:CGFloat = 20
    func setupLabels() {
        let size = CGSize(width: self.bounds.width / 2, height: self.bounds.size.height)
        
        let firstHalf             = UIView(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        self.lblFirstHalf         = self.formatLabel()
        self.lblFirstHalf.text    = "Requests"
        self.lblFirstHalf.center  = CGPoint(x: firstHalf.center.x-offset, y: firstHalf.center.y)
         self.lblFirstHalf.sizeToFit()
        
        let secondHalf            = UIView(frame: CGRect(x: self.center.x, y: 0, width: size.width, height: size.height))
        self.lblSecondHalf        = self.formatLabel()
        self.lblSecondHalf.text   = "Attendees"
        self.lblSecondHalf.center = CGPoint(x: secondHalf.center.x-offset, y: secondHalf.center.y)
        self.lblSecondHalf.sizeToFit()
        
        self.addSubview(lblFirstHalf)
        self.addSubview(lblSecondHalf)
    }
    
    func formatLabel() -> UILabel {
        let lbl  = UILabel(frame: CGRect.zero)
        lbl.text = "-----"
        lbl.textAlignment = NSTextAlignment.center
        
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.sizeToFit()
        return lbl
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.isInRight = true
        super.init(coder: aDecoder)
    }
    
}
