//
//  AllViteUsers.swift
//  Vite
//
//  Created by EeposIT_X on 5/18/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper

struct ViteUser:Mappable {
    
    var facebookId : NSNumber?
    var entityId   : String?
    var fullName   : String?
    var profileImgUrl : String?
    
    
    init?(map: Map) {
    
    }
    
    mutating func mapping(map: Map) {
        self.facebookId       <- map["facebookId"]
        self.fullName         <- map["fullName"]
        self.entityId         <- map["entityId"]
        self.profileImgUrl    <- map["profileImageUrl"]
    }
    
}

