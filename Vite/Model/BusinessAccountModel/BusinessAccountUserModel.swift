//
//  BusinessModel.swift
//  Vite
//
//  Created by Hem Poudyal on 1/2/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation
import ObjectMapper

struct BusinessAccountUserModel:Mappable {
    var entityId:String?
    var businessName:String?
    var description:String?
    var webUrl:String?
    var profileImage:String?
    var location: LocationModel?
    var businessImages:  [BusinessImageModel]?
    var businessMembers: [BusinessMemberModel]?
    var isPrimaryBusinessAccount: Bool?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        entityId                <- map["entityId"]
        businessName            <- map["businessName"]
        description             <- map["description"]
        profileImage            <- map["profileImage"]
        webUrl                  <- map["webUrl"]
        location                <- map["location"]
        description             <- map["description"]
        businessImages          <- map["businessImages"]
        businessMembers         <- map["businessMembers"]
        isPrimaryBusinessAccount <- map["primaryAccount"]
    }
}
