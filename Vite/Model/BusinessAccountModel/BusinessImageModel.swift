//
//  BusinessImageModel.swift
//  Vite
//
//  Created by Hem Poudyal on 1/2/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation
import ObjectMapper

struct BusinessImageModel:Mappable {
    var entityId:String?
    var businessImageUrl:String?
    var priority:Int?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        entityId                <- map["entityId"]
        businessImageUrl        <- map["businessImageUrl"]
        priority                <- map["priority"]
    }
}
