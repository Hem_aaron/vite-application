//
//  BusinessMemberModel.swift
//  Vite
//
//  Created by Hem Poudyal on 1/2/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation
import ObjectMapper

struct BusinessMemberModel:Mappable {
    var facebookId: NSNumber?
    var fullName:String?
    var profileImageUrl:String?
    var admin: Bool?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        facebookId                  <- map["member.facebookId"]
        fullName                    <- map["member.fullName"]
        profileImageUrl             <- map["member.profileImageUrl"]
        admin                       <- map["member.admin"]
    }
}

