//
//  PaymentStripe.swift
//  Vite
//
//  Created by Hem Poudyal on 8/3/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

struct Card:Mappable {
    var  lastFour: String?
    var  cardType: String?
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        lastFour                   <- map["last4"]
        cardType                  <- map["cardType"]
    }
}
