//
//  EventOrganizerModel.swift
//  Vite
//
//  Created by Hem Poudyal on 1/5/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit
import ObjectMapper

class EventsOrganizer: Mappable {
    
    var entityId : String?
    var eventDetail : String?
    var eventTitle : String?
    var imageUrl : String?
    var eventVideoUrl : String?
    var location : LocationModel?
    var createdDate : Double?
    var bringAFriend : Bool?
    var attendeeCount : NSNumber?
    var requestCount : NSNumber?
    var eventDate : String?
    var eventStartTime : String?
    var sendPushNotification: Bool?
    var isPrivateEvent: Bool?
    var isPaidEvent: Bool?
    var interests: [[String: AnyObject]]?
    var minAge: Int?
    var maxAge: Int?
    var eventPrice : NSNumber?
    var maxNumAttendant: NSNumber?
    //reocurrence Attributes
    var repeatType : String?
    var eventDays : [Int]?
    var repeatOnOccurence: Int?
    var endType: String?
    var endValue: String?
    var recurringEvent: Bool?
    var scheduledEventId: String?
    var media: [VMedia]?
    var videoProfileThumbNail: String?
    var fileType: String?
    var eventExpired: Bool?
    var featuredEvent: Bool?
    var revenue: RevenueModel?
    var promoCode: String?
    var discountAmount: Double?
    var discountPercent: Double?
    var eventCount: Int?

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        entityId               <- map["entityId"]
        eventDetail            <- map["eventDetail"]
        eventTitle             <- map["eventTitle"]
        eventDate              <- map["eventDate"]
        imageUrl               <- map["imageUrl"]
        eventVideoUrl          <- map["profileVideoUrl"]
        location               <- map["location"]
        createdDate            <- map["createdDate"]
        bringAFriend           <- map["bringAFriend"]
        attendeeCount          <- map["attendeeCount"]
        requestCount           <- map["requestCount"]
        eventStartTime         <- map["eventStartTime"]
        sendPushNotification   <- map["sendPush"]
        isPrivateEvent         <- map["privateEvent"]
        isPaidEvent            <- map["paidEvent"]
        interests              <- map["interests"]
        minAge                 <- map["minAge"]
        maxAge                 <- map["maxAge"]
        eventPrice             <- map["eventPrice"]
        maxNumAttendant        <- map["maxNumAttendant"]
        recurringEvent         <- map["recurringEvent"]
        scheduledEventId       <- map["scheduleEventId"]
        videoProfileThumbNail  <- map["videoProfileThumbnail"]
        fileType               <- map["fileType"]
        eventExpired           <- map["eventExpired"]
        featuredEvent          <- map["featured"]
        promoCode              <- map["promoCode"]
        discountAmount         <- map["discountAmount"]
        discountPercent        <- map["discountPercent"]
        repeatOnOccurence      <- map["repeatOnOccurrence"]
        media                  <- map["media"]
        revenue                <- map["revenue"]
        repeatType             <- map["repeatType"]
        endType                <- map["endType"]
        endValue               <- map["endValue"]
        eventDays              <- map["eventDays"]
        eventCount             <- map["count"]
    }
    
}
