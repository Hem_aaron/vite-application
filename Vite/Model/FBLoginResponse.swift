//
//  FBLoginResponse.swift
//  Vite
//
//  Created by Hem Poudyal on 12/19/17.
//  Copyright © 2017 Eepos IT. All rights reserved.
//

import Foundation

struct FBLoginResponse {
    var birthday:String?
    var email:String?
    var firstName:String?
    var lastName:String?
    var gender:String?
    var fbID:String?
    var pictureURL:String?
    var aboutMe:String?
    var fullName:String?
    var accessToken:String?
    var work:String?
    var fbURL: String?
    
    static func mapper(dictionary:AnyObject?) -> FBLoginResponse {
        var fbLoginResponse = FBLoginResponse()
        if let dictionary = dictionary as? [String:AnyObject] {
            
            if  let picture = dictionary["picture"] as? [String:AnyObject],
                let pictureData = picture["data"] as? [String:AnyObject],
                let pictureUrl = pictureData["url"] as? String {
                fbLoginResponse.pictureURL = pictureUrl
            }
            if let gender = dictionary["gender"] as? String {
                fbLoginResponse.gender = gender
                //The gender should be either Male or Female -- Case Sensitive
                fbLoginResponse.gender = fbLoginResponse.gender!.uppercaseFirst
            }
            
            if let bday = dictionary["birthday"] as? String {
                fbLoginResponse.birthday = Global.formatDate(date: bday)
            }
            
            fbLoginResponse.email = dictionary["email"] as? String
            fbLoginResponse.aboutMe = dictionary["about"] as? String
            if fbLoginResponse.aboutMe == nil {
                fbLoginResponse.aboutMe = ""
            }
            fbLoginResponse.firstName = dictionary["first_name"] as? String
            fbLoginResponse.lastName = dictionary["last_name"] as? String
            if let url =  dictionary["link"] as? String {
                fbLoginResponse.fbURL = url
            }
            if let fName = fbLoginResponse.firstName,
                let lName = fbLoginResponse.lastName {
                
                
                fbLoginResponse.fullName = "\(fName) \(lName)"
            }
            
            fbLoginResponse.work  = self.fetchWorkInfo(dictionary: dictionary)
        }
        return fbLoginResponse
    }
    
    func description() {
        print("\n ---------FBRESPONSE----------")
        print("\n Birthday  : \(String(describing: self.birthday))")
        print("\n Email  : \(String(describing: self.email))")
        print("\n FirstName  : \(String(describing: self.firstName))")
        print("\n LastName  : \(String(describing: self.lastName))")
        print("\n Gender  : \(String(describing: self.gender))")
        print("\n FBId  : \(String(describing: self.fbID))")
        print("\n Picture URL  : \(String(describing: self.pictureURL))")
        print("\n About Me : \(String(describing: self.aboutMe))")
        print("\n Full Name : \(String(describing: self.fullName))")
        print("\n <<<<<<<<<<FBRESPONSE>>>>>>>>>>>")
    }
    
    static func fetchWorkInfo(dictionary:[String:AnyObject]) -> String{
        var workInfo:String?
        let workResponseArray = dictionary["work"] as? [[String:AnyObject]]
        var latestWorkOfUser:AnyObject?
        var employer : [String:AnyObject]?
        var employerName:String?
        if let response = workResponseArray, response.count > 0 {
            latestWorkOfUser = response[0] as AnyObject
            employer = latestWorkOfUser!["employer"] as? [String:AnyObject]
            employerName = employer!["name"] as? String
            if
                let position = latestWorkOfUser!["position"] as? [String:AnyObject] ,
                let posName = position["name"] as? String {
                
                workInfo =   posName + ", " +  employerName!
                return workInfo!
            }
        }
        return " "
    }
}

