//
//  FBUserEvent.swift
//  Vite
//
//  Created by eeposit2 on 5/15/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation

struct FBUserEvent {
    let eventId: String
    let eventName: String
    let eventDescription: String
    let eventStartTime: String
    let canGuestsInvite :Bool
    let imageUrl: String
    let city: String
    let country: String
    let latitude: Double
    let longitude: Double
    
    init?(json:[String:AnyObject]) {
        
        
        func checkPastDate(_ eventDate: String) -> Bool {
            let currentDate = Date()
            if (convertFbEventDateToNSDate(eventDate) as NSDate).earlierDate(currentDate) == convertFbEventDateToNSDate(eventDate){
                return true
            }else{
                return false
            }
        }
        
        func convertFbEventDateToNSDate(_ date: String) -> Date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            let finaldate = dateFormatter.date(from: date)
            return finaldate!
        }

        
        
        guard let id = json["id"] as? String,
            let name   = json["name"] as? String,
            let startTime   = json["start_time"] as? String else{
            return nil
        }
        let eventStartTime = json["start_time"] as? String
        
        if checkPastDate(eventStartTime!){
            return nil
        }
        
        
        var desc   = json["description"] as? String ?? ""
        if desc.characters.count > 90 {
           desc = String(desc.prefix(89))
        }
        let invite = json["can_guests_invite"] as? Bool ?? false
        let cover = json["cover"] as? [String: AnyObject] ?? ["":"" as AnyObject]
        let place   = json["place"] as? [String: AnyObject] ?? ["":"" as AnyObject]
        let location = place["location"] as? [String: AnyObject] ?? ["":"" as AnyObject]
        self.eventId = id
        self.city = location["city"] as? String ?? ""
        self.country = location["country"] as? String ?? ""
        self.latitude = location["latitude"] as? Double ?? 0
        self.longitude = location["longitude"] as? Double ?? 0
        self.eventName = name
        self.eventDescription = desc
        self.eventStartTime = startTime
        self.canGuestsInvite = invite
        self.imageUrl = cover["source"] as? String ?? ""
        
        
        
    }
    

    
    

}
