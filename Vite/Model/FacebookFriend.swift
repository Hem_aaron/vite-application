//
//  FacebookFriend.swift
//  Vite
//
//  Created by Eeposit 01 on 12/8/17.
//  Copyright © 2017 Eepos IT. All rights reserved.
//
import UIKit
import ObjectMapper

class FacebookFriend: Mappable {
    var fbID:String?
    var name:String?
    
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        fbID         <- map["id"]
        name         <- map["name"]
     }
}

