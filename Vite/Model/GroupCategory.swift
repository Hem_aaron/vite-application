//
//  GroupCategory.swift
//  Vite
//
//  Created by Eeposit 01 on 4/4/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//
import ObjectMapper
import UIKit

struct VipGroupCategory:Mappable {
    
    var categoryName:String?
    var entityId:String?
    var friends: [MyVIPFriend]?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        categoryName             <- map["categoryName"]
        entityId                 <- map["entityId"]
        friends                  <- map["friends"]
    }
}
