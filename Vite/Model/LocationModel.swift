//
//  LocationModel.swift
//  Vite
//
//  Created by Hem Poudyal on 1/2/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation
import ObjectMapper

struct LocationModel:Mappable {
    var uid:String?
    var givenLocation:String?
    var latitude:String?
    var longitude:String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        uid             <- map["entityId"]
        givenLocation   <- map["givenLocation"]
        latitude        <- map["latitude"]
        longitude       <- map["longitude"]
    }
}
