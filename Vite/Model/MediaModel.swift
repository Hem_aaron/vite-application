//
//  MediaModel.swift
//  Vite
//
//  Created by Hem Poudyal on 1/5/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation

import UIKit
import ObjectMapper

struct MediaModel :Mappable {
    
    var mediaId : String?
    var fileType: String?
    var mediaUrl: String?
    var thumbNailUrl: String?
    var height: Int?
    var width: Int?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        mediaId             <- map["mediaId"]
        fileType            <- map["fileType"]
        mediaUrl            <- map["mediaUrl"]
        thumbNailUrl        <- map["thumbnailUrl"]
        height              <- map["height"]
        width               <- map["width"]
    }
}
