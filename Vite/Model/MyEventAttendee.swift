//
//  MyEventAttendee.swift
//  Vite
//
//  Created by eeposit2 on 7/4/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//



import Foundation
import ObjectMapper

struct MyEventAttendee:Mappable {
    
    var facebookId : NSNumber?
    var fullName : String?
    var profileImageUrl : String?
    var info: String?
    var pushNotificationStatus : Bool?
    var qrScanned : Bool?
    var manualScanned: Bool?
    var screenShotTaken: Bool?
    var qrScannedDate: NSNumber?
    var amount: Double?
    var acceptedDate: NSNumber?
    // for new payment history detail attendee API
    var chargeAmount: Double?
    var createdDate: NSNumber?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        facebookId              <- map["facebookId"]
        fullName                <- map["fullName"]
        info                    <- map["info"]
        profileImageUrl         <- map["profileImageUrl"]
        pushNotificationStatus  <- map["pushNotificationStatus"]
        qrScanned               <- map["qrScanned"]
        manualScanned           <- map["manuallyScanned"]
        screenShotTaken         <- map["screenshotTaken"]
        qrScannedDate           <- map["qrScannedDate"]
        amount                  <- map["amount"]
        chargeAmount            <- map["chargeAmount"]
        acceptedDate            <- map["acceptedDate"]
        createdDate             <- map["createdDate"]

    }
}

