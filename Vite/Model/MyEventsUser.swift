//
//  MyEventsUser.swift
//  Vite
//
//  Created by Hem Poudyal on 1/29/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit
import ObjectMapper

class MyEventsUser: Mappable {
    //Facebook Id of attendee
    var entityId : String?
    var eventUserFullName : String?
    var eventDetail : String?
    var eventTitle : String?
    var imageUrl : String?
    var latitude : String?
    var longitude : String?
    var organizerProfileImageUrl : String?
    var organizerFbId : String?
    var organizerFullName : String?
    var userDescription:String?
    var status : String?
    var createdDate: Double?
    var eventDate : String?
    var eventStartTime : String?
    
    
    required init?(map: Map) {
    
    }
    
    func mapping(map: Map) {
        entityId                     <- map["facebookId"]
        eventUserFullName            <- map["fullName"]
        eventDetail                  <- map[""]
        eventTitle                   <- map[""]
        imageUrl                     <- map["profileImageUrl"]
        latitude                     <- map[""]
        longitude                    <- map[""]
        organizerProfileImageUrl     <- map[""]
        organizerFbId                <- map[""]
        organizerFullName            <- map[""]
        userDescription              <- map["info"]
        status                       <- map[""]
        createdDate                  <- map[""]
        eventDate                    <- map[""]
        eventStartTime               <- map[""]
    }
}
