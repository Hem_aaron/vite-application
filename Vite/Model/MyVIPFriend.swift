//
//  MyFriends.swift
//  Vite
//
//  Created by EeposIT_X on 5/4/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import ObjectMapper


struct MyVIPFriend: Mappable {
    var facebookIdInt : NSNumber?
    var facebookId : String?
    var fullName : String?
    var profileImageUrl : String?
    var vipViteCount : Int?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        facebookIdInt            <- map["facebookId"]
        facebookId               <- map["facebookId"]
        fullName                 <- map["fullName"]
        profileImageUrl          <- map["profileImageUrl"]
        vipViteCount            <- map["countVite"]
    }
    
}
