//
//  MyVites.swift
//  Vite
//
//  Created by EeposIT_X on 2/29/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class MyVites: NSObject {
    var eventID : String?
    var acceptedDate : String?
    var eventTitle : String?
    var eventDetail : String?
    var imageUrl : String?
    var eventVideoUrl: String?
    var latitude : String?
    var longitude : String?
    var fullName : String?
    var profileImageUrl : String?
    var viteStatus : String?
    var organizerFBID:String?
    var eventGivenLocation: String?
    var eventDate : String?
    var eventStartTime : String?
    var qrImagePath: String?
    var businessName: String?
    var businessImageUrl: String?
    var businessID : String?
    var privateEvent: Bool?
    var isCelebrity: Bool?
    var media: [VMedia]?
    var videoProfileThumbNail: String?
    var fileType: String?
    var rated: Bool?
    
}
