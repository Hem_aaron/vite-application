//
//  Network.swift
//  Vite
//
//  Created by eeposit2 on 1/16/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper

struct Network: Mappable {
    var networkType:String?
    var entityId:String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        networkType          <- map["networkType"]
        entityId             <- map["entityId"]
        
        
    }
}

