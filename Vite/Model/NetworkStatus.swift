//
//  NetworkStatus.swift
//  Vite
//
//  Created by eeposit2 on 1/16/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit
import ObjectMapper

struct NetworkStatus: Mappable {
    
    var networkId:String?
    var networkType:String?
    var networkConnectStatus:Bool?
    var networkUserName: String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        networkId                        <- map["networkId"]
        networkType                      <- map["networkType"]
        networkUserName                  <- map["networkUserName"]
        networkConnectStatus             <- map["networkConnectStatus"]
        
        
    }
    
}

