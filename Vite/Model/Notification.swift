//
//  Notification.swift
//  Vite
//
//  Created by Hem Poudyal on 1/31/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit
import ObjectMapper

struct Notification: Mappable {
    
    var imageUrl : String?
    var eventId  : String?
    var seenStatus : Bool?
    var createdDate :NSNumber?
    var message : String?
    var notificationType : String?
    var userNotificationType : String?
    var messageId : String?
    var feedId: String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        imageUrl                 <- map["imageUrl"]
        eventId                  <- map["eventId"]
        seenStatus               <- map["seenStatus"]
        createdDate              <- map["createdDate"]
        message                  <- map["message"]
        notificationType         <- map["notificationType"]
        userNotificationType     <- map["userNotificationType"]
        messageId                <- map["messageId"]
        feedId                   <- map["feedId"]
    }
}

