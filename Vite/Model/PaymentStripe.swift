//
//  PaymentStripe.swift
//  Vite
//
//  Created by Hem Poudyal on 8/3/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

struct PaymentStripe:Mappable {
    
    var  discount: Double?
    var  eventName: String?
    var  fee: Double?
    var  price: Double?
    var  total: Double?
    var  subTotal: Double?
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        discount               <- map["discount"]
        eventName              <- map["eventName"]
        fee                    <- map["fee"]
        price                  <- map["price"]
        total                  <- map["total"]
        subTotal               <- map["subTotal"]
    }
}
