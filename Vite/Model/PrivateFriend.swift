//
//  PrivateFriend.swift
//  Vite
//
//  Created by eepostIT5 on 6/12/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class PrivateFriend : NSObject,NSCoding  {
    
    var facebookId : String?
    var fullName : String?
    var email : String?
    var entityId: String?
    var gender : String?
    var age: String?
    var profileImage: String?
    var info: String?
    var businessAccountId: String?
    
    init?(json:[String:AnyObject]) {
        guard
            let facebookId   = json["facebookId"],
            let fullName     = json["fullName"] as? String else {
                return nil
        }
         let profileImage = json["profileImageUrl"] as? String
         let email        = json["email"] as? String
         let info         = json["info"] as? String
         let gender       = json ["gender"] as? String
         let businessId   = json ["businessId"] as? String
        let entityId     = json["entityId"] as? String
        
        
        self.facebookId      = String(describing: facebookId)
        self.fullName        = fullName
        self.entityId         = entityId
        self.profileImage = profileImage
        self.email = email
        self.info = info
        self.gender = gender
        self.businessAccountId = businessId
    }
    
    init(facebookId: String?, fullName: String?, email: String?, entityId: String?, gender: String?, age: String?, profileImage: String?, info: String?) {
        self.facebookId = facebookId
        self.fullName = fullName
         self.email = email
         self.entityId = entityId
        self.gender = gender
         self.age = age
         self.profileImage = profileImage
         self.info = info
    }
    required convenience init(coder aDecoder: NSCoder) {
        let facebookId = aDecoder.decodeObject(forKey: "facebookId") as? String
        let fullName = aDecoder.decodeObject(forKey: "fullName") as? String
        let email = aDecoder.decodeObject(forKey: "email") as? String
        let entityId = aDecoder.decodeObject(forKey: "entityId")  as? String
        let gender = aDecoder.decodeObject(forKey: "gender")  as? String
        let age = aDecoder.decodeObject(forKey: "age")  as? String
        let profileImage = aDecoder.decodeObject(forKey: "profileImage") as? String
        let info = aDecoder.decodeObject(forKey: "info") as? String
        self.init(facebookId: facebookId, fullName: fullName, email: email, entityId: entityId, gender: gender, age: age, profileImage: profileImage, info: info)
    }
    
    func encode(with aCoder: NSCoder){
        aCoder.encode(facebookId, forKey: "facebookId")
        aCoder.encode(fullName, forKey: "fullName")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(entityId, forKey: "entityId")
        aCoder.encode(gender, forKey: "gender")
        aCoder.encode(age, forKey: "age")
        aCoder.encode(profileImage, forKey: "profileImage")
        aCoder.encode(info, forKey: "info")
    }
    
}
