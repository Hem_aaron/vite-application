//
//  RequestedUser.swift
//  Vite
//
//  Created by Hem Poudyal on 1/29/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit
import ObjectMapper

struct RequestedUser: Mappable {
  
    var facebookId : Int64?
    var eventUserFullName : String?
    var userProfileImageUrl : String?
    var info : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        facebookId             <- map["facebookId"]
        eventUserFullName                 <- map["fullName"]
        userProfileImageUrl                  <- map["profileImageUrl"]
        info                    <-             map["info"]
    }
   
}

