//
//  RevenueModel.swift
//  Vite
//
//  Created by Hem Poudyal on 1/5/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation
import ObjectMapper

struct RevenueModel: Mappable {
    var noOfTickets: Double?
    var totalRevenue: Double?
    var vitePayment: Double?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        noOfTickets             <- map["noOfTickets"]
        totalRevenue            <- map["totalRevenue"]
        vitePayment            <- map["vitePayment"]
    }
}
