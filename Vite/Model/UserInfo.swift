//
//  UserInfo.swift
//  Vite
//
//  Created by eeposit2 on 2/29/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation

class UserInfo: NSObject{
    var fbId : NSNumber?
    var fullName : String?
    var age : Int?
    var gender : String?
    var email : String?
    var info : String?
    var occupation : String?
    var education : String?
    var countVite : Int?
    var imageURL: String?
    var userImageList: [[String: AnyObject]]?
    var myVip: Bool?
    var iAmVip: Bool?
    var addToMyVipRequestSent: Bool?
    var addMeToYourVipRequestSent: Bool?
    var givenLocation:String?
    var latitude:String?
    var longitude:String?
    var userNetworks: [[String: AnyObject]]?
    var verificationStatus : Bool?
    var interests: [[String: AnyObject]]?
    var rating: Double?
    var hideAge: Bool?
    var darkMode:Bool?
    var premiumAccount: Bool?
    var viteAllCount: Int?
    var viteNowCount: Int?
  
    init?(json:[String:AnyObject]) {
        guard
            let fullName     = json["fullName"] as? String,
            let fbId         = json["facebookId"] as? NSNumber else {
                return nil
        }
        let age                     = json["age"] as? Int
        let gender                  = json["gender"] as? String
        let email                   = json["email"] as? String
        let info                    = json["info"] as? String
        let occupation              = json["occupation"] as? String
        let education               = json["education"] as? String
        let countVite               = json["countVite"] as? Int
        let imageURL                = json["profileImageUrl"] as? String
        let userImageList           = json["userImages"] as? [[String : AnyObject]]
        let myVip                   = json["myVip"] as? Bool
        let addToMyVipRequestSent   = json["myVipRequestStatus"] as? Bool
        let addMeToYourVipRequestSent  = json["iAmVipRequestStatus"] as? Bool
        let iAmVip                  = json["iAmVip"] as? Bool
        let givenLocation           = json["location"]?["givenLocation"] as? String
        let latitude                = json["location"]?["latitude"] as? String
        let longitude               = json["location"]?["longitude"] as? String
        let userNetworks            = json["userNetworks"] as? [[String : AnyObject]]
        let verificationStatus      = json["verificationStatus"] as? Bool
        let interests               = json["interests"] as? [[String : AnyObject]]
        let rating                  = json["rating"] as? Double
        let hideAge                 = json["hideAge"] as? Bool
        let darkMode                = json["darkMode"] as? Bool
        let premiumAccount          = json["premiumAccount"] as? Bool
        let viteAllCount            = json["viteAllCount"] as? Int
        let viteNowCount            = json["viteNowCount"] as? Int
        
        
        self.fbId                   = fbId
        self.fullName               = fullName
        self.age                    = age
        self.gender                 = gender
        self.email                  = email
        self.info                   = info
        self.occupation             = occupation
        self.education              = education
        self.countVite              = countVite
        self.imageURL               = imageURL
        self.userImageList          = userImageList
        self.myVip                  = myVip
        self.iAmVip                 = iAmVip
        self.addToMyVipRequestSent  = addToMyVipRequestSent
        self.addMeToYourVipRequestSent  = addMeToYourVipRequestSent
        self.givenLocation          = givenLocation
        self.latitude               = latitude
        self.longitude              = longitude
        self.userNetworks           = userNetworks
        self.verificationStatus     = verificationStatus
        self.interests              = interests
        self.rating                 = rating
        self.hideAge                = hideAge
        self.darkMode               = darkMode
        self.premiumAccount         = premiumAccount
        self.viteNowCount           = viteNowCount
        self.viteAllCount           = viteAllCount
    }
}

