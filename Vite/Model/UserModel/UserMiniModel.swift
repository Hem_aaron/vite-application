//
//  UserMiniModel.swift
//  Vite
//
//  Created by Hem Poudyal on 1/3/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation

struct UserMiniModel {
    var id:String
    var profileImageURL:String
    var fullName:String
}
