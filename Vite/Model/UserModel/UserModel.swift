//
//  UserModel.swift
//  Vite
//
//  Created by Hem Poudyal on 1/2/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit
import ObjectMapper

struct UserModel: Mappable{
    
    var facebookId: Int?
    var fullName: String?
    var profileImageUrl :String?
    var gender: String?
    var age: Int?
    var interest: [[String:AnyObject]]?
    var premiumStatus: Bool?
    var pushNotificationStatus: String?
    var business: BusinessAccountUserModel?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        fullName               <- map["fullName"]
        profileImageUrl        <- map["profileImageUrl"]
        gender                 <- map["gender"]
        age                    <- map["age"]
        facebookId             <- map["facebookId"]
        interest               <- map["interests"]
        premiumStatus          <- map["premiumAccount"]
        pushNotificationStatus <- map["pushNotificationStatus"]
        business               <- map["business"]
    }
}
