//
//  VComment.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 8/3/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper

struct VComment:Mappable {
    
    var entityId : String?
    var user: VUser?
    var comment: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        entityId                  <- map["entityId"]
        user                      <- map["user"]
        comment                   <- map["comment"]
       }

}
