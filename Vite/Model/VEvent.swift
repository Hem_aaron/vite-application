//
//  VEvent.swift
//  Vite
//
//  Created by Eeposit  on 1/26/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit
import ObjectMapper

struct VEvent :Mappable {

    var uid : String?
    // Date when the event will start
    var date : String?
    
    // Time when the event will start
    var startTime : String?
    
    // Date that the event was created in
    var createdDate : String?
    
    var detail : String?
    
    var title : String?
    
    var imageUrl : String?
    
    var location : VLocation?
    
    var organizer : VUser?
    
    //Shows wither the event is active or inactive - ACTIVE / INACTIVE
    var status : String?
    
    var eventPrice : NSNumber?
    
    var bringAFriend : Bool?
    
    var paidEvent : Bool?
    
    var requestedDate: NSNumber?
    
    var eventsUsers : VEventUsers?
    
    var attendeeCount : NSNumber?
    
    var requestCount : NSNumber?
    
    var eventTitle : String?
    
    var business: BusinessAccountUserModel?
    
    var friendAttending : [VFriendAttending]?
    
    var profileVideoUrl : String?
    
    var media: [VMedia]?
    
    var videoProfileThumbNail: String?
    
    var fileType: String?
    
    var featureEvent: Bool?
    
    var totalRevenue: Double?
    
    var promoCodeStatus: Bool?
    
    var promoCode: String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        uid                             <- map["entityId"]
        date                            <- map["eventDate"]
        startTime                       <- map["eventStartTime"]
        createdDate                     <- map["createdDate"]
        detail                          <- map["eventDetail"]
        title                           <- map["eventTitle"]
        imageUrl                        <- map["imageUrl"]
        bringAFriend                    <- map["bringAFriend"]
        status                          <- map["status"]
        location                        <- map["location"]
        organizer                       <- map["organizer"]
        paidEvent                       <- map["paidEvent"]
        eventPrice                      <- map["eventPrice"]
        eventsUsers                     <- map["eventsUsers.0"]
        requestCount                    <- map["requestCount"]
        attendeeCount                   <- map["attendeeCount"]
        eventTitle                      <- map["eventTitle"]
        business                        <- map["business"]
        friendAttending                 <- map["friendsAttending"]
        profileVideoUrl                 <- map["profileVideoUrl"]
        media                           <- map["media"]
        fileType                        <- map["fileType"]
        videoProfileThumbNail           <- map["videoProfileThumbnail"]
        featureEvent                    <- map["featured"]
        totalRevenue                    <- map["totalRevenue"]
        promoCode                       <- map["promoCode"]
        promoCodeStatus                 <- map["promoCodeStatus"]
        // requestedDate   <- map["eventsUsers.0.requestedDate"]
    }
}
