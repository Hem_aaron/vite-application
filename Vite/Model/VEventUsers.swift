//
//  VEventUsers.swift
//  Vite
//
//  Created by Eeposit  on 1/26/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation
import ObjectMapper

struct VEventUsers:Mappable {
    
    var requestedDate: NSNumber?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        requestedDate <- map["requestedDate"]
    }
}
