//
//  VFeedback.swift
//  Vite
//
//  Created by eeposit2 on 8/24/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import ObjectMapper

struct VFeedback:Mappable {
    
    var uid : String?
    
    var subject: String?
    
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        uid             <- map["entityId"]
        subject            <- map["feedBackSubject"]
    }
}
