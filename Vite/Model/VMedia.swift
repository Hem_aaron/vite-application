//
//  VMedia.swift
//  Vite
//
//  Created by Eeposit  on 1/26/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit
import ObjectMapper

struct VMedia :Mappable {
    var mediaId : String?
    var fileType: String?
    var mediaUrl: String?
    var thumbNailUrl: String?
    var height: Int?
    var width: Int?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        mediaId             <- map["mediaId"]
        fileType            <- map["fileType"]
        mediaUrl            <- map["mediaUrl"]
        thumbNailUrl        <- map["thumbnailUrl"]
        height              <- map["height"]
        width               <- map["width"]
    }
}
