//
//  VPayment.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 7/25/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

struct VPayment:Mappable {
    
    var  totalRevenue: Double?
    var  eventCount: Int?
    var  eventList: [VEvent]?
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        eventCount               <- map["count"]
        totalRevenue             <- map["totalRevenue"]
        eventList                <- map["events"]
    }
}
