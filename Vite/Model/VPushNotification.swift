//
//  VFeedback.swift
//  Vite
//
//  Created by eeposit2 on 8/24/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import ObjectMapper

struct VPushNotification:Mappable {
    
    var pushNotificationStatus : String?
    var viteNow: Bool?
    var eventVisibleDistance :Int?
    
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        pushNotificationStatus               <- map["pushNotificationStatus"]
        viteNow                              <- map["viteNow"]
        eventVisibleDistance                 <- map["eventVisibleDistance"]
    }
}
