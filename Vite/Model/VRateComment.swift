//
//  VRateComment.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 4/6/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

struct VRateComment:Mappable {
    
    var  comment: String?
    var  rating: Double?
    var  createdDate: NSNumber?
    var  facebookId: NSNumber?
    var  fullName: String?
    var  imageUrl: String?
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        comment                 <- map["comment"]
        rating                  <- map["rating"]
        createdDate             <- map["createdDate"]
        facebookId              <- map["facebookId"]
        fullName                <- map["fullName"]
        imageUrl                <- map["imageUrl"]
    }
}

