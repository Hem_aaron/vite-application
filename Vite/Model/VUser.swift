//
//  VUser.swift
//  Vite
//
//  Created by Eeposit  on 1/26/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation
import ObjectMapper

struct VUser:Mappable {
    var uid:NSNumber?
    var email:String?
    var fullName:String?
    var age:Int?
    var education:String?
    var gender:String?
    var info:String?
    var occupation:String?
    var profileImageUrl:String?
    var location:VLocation?
    var verificationStatus : Bool?
    var eliteVerificationStatus: Bool?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        uid                       <- map["facebookId"]
        email                     <- map["email"]
        fullName                  <- map["fullName"]
        age                       <- map["age"]
        education                 <- map["education"]
        gender                    <- map["gender"]
        info                      <- map["info"]
        occupation                <- map["occupation"]
        profileImageUrl           <- map["profileImageUrl"]
        location                  <- map["age"]
        verificationStatus        <- map["verificationStatus"]
        eliteVerificationStatus   <- map["eliteVerificationStatus"]
        
        
    }
}
