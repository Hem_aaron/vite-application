//
//  VVites.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 5/15/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

struct VVite:Mappable {
    
    var  eventId: String?
    var  eventTitle: String?
    var isSelected = false 
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        eventId                 <- map["eventId"]
        eventTitle                  <- map["eventTitle"]
    }
}
