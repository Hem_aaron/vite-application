//
//  VfriendAttending.swift
//  Vite
//
//  Created by Eeposit  on 1/26/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit
import ObjectMapper

struct VFriendAttending : Mappable {
    
    var entityId : String?
    var facebookId : NSNumber?
    var fullName : String?
    var profileImageUrl : String?
    var pushNotificationStatus : String?
    
    init?(map: Map) {
    
    }
    
    mutating func mapping(map: Map) {
        entityId                   <- map["entityId"]
        facebookId                 <- map["facebookId"]
        fullName                   <- map["fullName"]
        profileImageUrl            <- map["profileImageUrl"]
        pushNotificationStatus     <- map["pushNotificationStatus"]
        // requestedDate   <- map["eventsUsers.0.requestedDate"]
    }
    
}
