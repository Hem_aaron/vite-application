//
//  ViteFeed.swift
//  Vite
//
//  Created by Eeposit  on 1/22/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import UIKit
import ObjectMapper

struct ViteFeed:Mappable {
    
    var mediaId : String?
    var feedId: String?
    var fileType : String?
    var mediaUrl : String?
    var eventId : String?
    var eventTitle : String?
    var organizerName : String?
    var profileImage : String?
    var mediaHeight : NSNumber?
    var mediaWidth : NSNumber?
    var thumbnailUrl : String?
    var businessAccount : Bool?
    var organizerId : NSNumber?
    var businessId : String?
    var commentCount: Int?
    var postedBy: Bool?
    var caption: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        mediaId         <- map["mediaId"]
        feedId         <- map["feedId"]
        fileType        <- map["fileType"]
        mediaUrl        <- map["mediaUrl"]
        eventId         <- map["eventId"]
        eventTitle      <- map["eventTitle"]
        organizerName   <- map["organizerName"]
        profileImage    <- map["profileImage"]
        mediaHeight     <- map["height"]
        mediaWidth      <- map["width"]
        thumbnailUrl    <- map["thumbnailUrl"]
        businessAccount <- map["businessAccount"]
        organizerId     <- map["organizerId"]
        businessId      <- map["businessId"]
        commentCount    <- map["commentCount"]
        postedBy        <- map["postedBy"]
        caption        <- map["caption"]
    }
}

