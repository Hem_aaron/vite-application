//
//  APIList.swift
//  Vite
//
//  Created by Hem Poudyal on 12/22/17.
//  Copyright © 2017 Eepos IT. All rights reserved.
//

import Foundation
import Alamofire

// Local
//let kBaseURL = "http://192.168.0.128:8080/vite-web/"


#if DEVELOPMENT
//let kBaseURL = "http:/192.168.0.119:8080/vite-web/"
  let kBaseURL = "http://app21.eeposit.com:8080/vite-web/"
#else
let kBaseURL = "https://api-vite.eeposit.com/"
#endif

let kAPIFbLogin                = kBaseURL + "client/login_with_facebook"
let kViewEvents_Users          = kBaseURL + "client/get_filtered_events/"
let kGetUserMiniInfo           = kBaseURL + "client/get_user_min_info/"
let kListEvents                = kBaseURL + "/organizer/find_events/"
let kListEventsWithPagination  = kBaseURL + "/organizer/find_paginated_events/"
let kListEventsForPosting      = kBaseURL + "/organizer/find_paginated_events_for_posting/"


let kAPIEditEvent              = kBaseURL + "/organizer/edit_event"
let kUserRequestForEvent       = kBaseURL + "/client/request_to_be_a_participant/"
let kUserRequestForEventPayment = kBaseURL + "/client/request_to_be_a_participant_with_promo_code/"
let kMyVites                   = kBaseURL + "client/get_my_vites/"
let kMyVitesWithPagination    = kBaseURL + "/client/get_my_vites_with_pagination/"


let kAPIDeleteMyVite           = kBaseURL + "client/delete_my_vite/"

let kAllPartyList              = kBaseURL + "client/list_of_fbIds_of_all_parties_in_event/"
let kAttendeeList              = kBaseURL + "organizer/find_event_attendees/"
let kAttendeeNewRequests       = kBaseURL + "organizer/find_new_requests/"
let kApiUpdateUserInfo         = kBaseURL + "/client/update_user_profile"
let kRespondToRequestFromUser  = kBaseURL + "/organizer/respond_to_request/"
let kAPIDeleteEvent            = kBaseURL + "organizer/delete_event/"
let KAPIViewAttendeeProfile    = kBaseURL + "client/get_user_profile/"
let kAPIUpdateDeviceToken      = kBaseURL + "/client/update_device_info/"
let kAPIGetUserInfoWithFBID    = kBaseURL + "/client/get_message_center_conversions"
let kAPIGetEventDetail         = kBaseURL + "/organizer/get_event_detail/"
let kAPISendPushNotification   = kBaseURL + "/client/send_chat_push_notification"
let kAPIGetViteDetail          = kBaseURL + "/client/get_my_vite_detail"
let kAPISetEventDistance       = kBaseURL + "/client/set_event_visible_distance"
let kAPIGetEventDistance       = kBaseURL + "/client/get_event_visible_distance/"
let kAPILogOut                 = kBaseURL + "/client/logout/"
let KAPIDarkModeOn             = kBaseURL + "/user/toggle_dark_mode"
let KAPICheckDarkMode          = kBaseURL + "/user/dark_mode_activated_ids"
let KAPIHideAge                = kBaseURL + "/user/toggle_hide_age"
let KAPIListEventByLocation    = kBaseURL + "/client/list_of_paginated_events_by_location"

//Vip list
let kAPIAddFriendList          = kBaseURL + "/client/add_users_friends/"
let kAPIGetFriendList          = kBaseURL + "/client/get_users_friends/"
let kAPIGetVIPList             = kBaseURL + "client/get_users_vips/"
let kAPIRemoveVIPList          = kBaseURL + "organizer/remove_vip/"
let kAPIGetVIPRequests         = kBaseURL + "client/get_VIP_requests/"
let kAPIRespondVIPRequests     = kBaseURL + "client/respond_to_VIP_Request/"

//Vip Group Api
let KAPICreateVipGroup         = kBaseURL + "/user/create_VIP_category/"
let KAPIListVipCategory        = kBaseURL + "/user/list_VIP_category_by_organizer"
let KAPIDeleteVipCategory      = kBaseURL + "/user/remove_VIP_category/"
let KAPIAddVipToGroup          = kBaseURL + "/user/add_VIP_to_category/"
let KAPIListVipByCategory      = kBaseURL + "/user/list_VIP_by_category/"
let KAPIDeleteVipFromCategory  = kBaseURL + "/user/remove_VIP_from_category/"

// Vip Business Group Api
let KAPICreateBusinessVipGroup          = kBaseURL + "/business/create_VIP_category/"
let KAPIListBusinessVipCategory         = kBaseURL + "/business/list_VIP_category_by_business/"
let KAPIDeleteBusinessVipCategory       = kBaseURL + "/business/remove_VIP_category/"
let KAPIAddBusinessVipToCategory        = kBaseURL + "/business/add_VIP_to_category/"
let KAPIListBusinessVipByCategory       = kBaseURL + "//business/list_VIP_by_category/"
let KAPIDeleteBusinessVipFromCategory   = kBaseURL + "/business/remove_VIP_from_category/"

// Refund Attendee
let KAPIRefundAttendeeTicket                    = kBaseURL + "/organizer/refund_ticket/"
let KAPIRefundAllTicket                         = kBaseURL + "/organizer/refund_all_tickets/"

//Comment Media
let KAPICommentList                      = kBaseURL + "/feed/list_comment/"
let KAPIAddComment                       = kBaseURL + "/feed/save_comment/"




//Search Users
let kAPISearchViteUsers        = kBaseURL + "client/searchViteUsers/"
let kAPISearchViteUsersForVip  = kBaseURL + "client/searchViteUsersForVIP/"
//Organisers
let kAPISendViteNow            = kBaseURL + "organizer/send_vite_now/"
let kAPIInviteUsersToEvent     = kBaseURL + "organizer/invite_users/"

//Invite All People Around
let kAPIInviteAll            = kBaseURL + "organizer/vite_all/"
// Feature Event Fee
let KAPIFeatureEventFee      = kBaseURL + "/organizer/get_featured_event_fee"
//Private Event
let kAPIGetPrivateEventUsers   = kBaseURL + "organizer/get_users_for_private_event/"
let kInviteForPrivateEvent     = kBaseURL + "organizer/invite_user_for_private_event/"
let kAPIEventPUshNotification  = kBaseURL + "organizer/set_notification_status_for_event/"
//Paid Event
let kAPIUpdateStripeToken      = kBaseURL + "/client/update_stripe_token/fbId/"
let kAPIConnectWithStripe      = kBaseURL + "/organizer/connect_with_stripe/"
let kAPIGetStripeInfo          = kBaseURL + "/client/get_client_stripe_info/fbId/"
let kAPIGetEventFee            = kBaseURL + "/client/event_fee_info/"
let KAPICheckStripeStatus      = kBaseURL + "/organizer/check_for_stripe_account_status/"
let kAPIGetEventFeeWithPromoCode = kBaseURL + "/client/event_fee_info_with_promo_code/"
let KAPICheckPaypalStatus      = kBaseURL + "/organizer/check_for_paypal_email_status/"
let KAPIAddPaypalEmail         = kBaseURL + "/user/add_paypal_email"

// feedback list
let kAPIGetFeedbackSubjectList =  kBaseURL + "/client/list_feedback_subjects"
//privacy policy
let kAPIPrivacyPolicyUrl       = kBaseURL + "/privacy_policy"
let kAPITermsAndConditionUrl   = kBaseURL + "/terms_and_conditions"

// peopleList
let kAPIListPeople             = kBaseURL + "user/list_users_by_location/"

let KAPIPushNotificationViewed  = kBaseURL + "/client/mark_push_notification_viewed/"
let kAPIDecodeQrcode            = kBaseURL + "/organizer/decode_scanned_qrcode/"
let kAPIChangeQrScanStatus      = kBaseURL + "/organizer/chage_qr_code_scanned_status/"
//report Event
let kAPIReportEvent             = kBaseURL + "client/report_event/"

//MARK:- BUSINESS ACCOUNT
let kGetAllBusinessAccount              = kBaseURL + "business/get_all_business_accounts/"
let kCreateBusinessAccount              = kBaseURL + "business/add_business_account/"
let kUpdateBusinessAccount              = kBaseURL + "business/update_business_account/"
let kDeleteBusinessAccount              = kBaseURL + "business/delete_business_account/"
let KListAllUser                        = kBaseURL + "user/get_all_users"
let kListBusinessEvents                 = kBaseURL + "business/list_business_events/"
let kListBusinessEventsPaginated        = kBaseURL + "business/list_business_events_paginated/"
let kListBusinessVips                   = kBaseURL + "business/get_business_vips/"
let kCreateBusinessVips                 = kBaseURL + "business/create_business_vip/"
let kAPIRemoveBusinessVIPList           = kBaseURL + "business/remove_business_vip/"
let kAPITogglePrimaryBusinessAccount    = kBaseURL + "business/toggle_primary_business_account/"
let KAPIDeleteMemberBusinessAccount     = kBaseURL + "business/remove_member_from_business_account/"
let KAPIGetBUsinessProfile              = kBaseURL + "business/get_business_profile/"
let KAPISearchUsersForBusinessVip       = kBaseURL + "business/search_users_for_business_VIPs/"
let kAPIGetUserFriendListForBusiness    = kBaseURL + "/business/get_users_friends_for_business/"
let KAPIGetMutualFriends                = kBaseURL + "/user/get_mutual_friends/"
let KAPIBlockUser                       = kBaseURL + "user/block_users/"
let KAPIBlockedUserList                 = kBaseURL + "/user/get_blocked_users/"
let KAPIUnBlockeUser                    = kBaseURL + "user/unblock_users/"


//MARK:- Social Network
let KAPIGetNetworkTypes                 = kBaseURL + "/user/get_network_types/"
let  KAPIConnectSocialNetwork           = kBaseURL + "/user/connect_with_social_network/"
let  KAPIDisconnectSocialNetwork        = kBaseURL + "/user/disconnect_social_network/"
let  KAPIGetUserNetworkStatus           = kBaseURL + "/user/user_network_status/"

// ScreenShot
let KAPIScreenShotTaken                  = kBaseURL + "/client/screenshot_taken/"

let KAPISendEmailForCelebVerification    = kBaseURL + "/user/send_celebrity_verification_email/"
let KAPISendEmailForEliteVerification    = kBaseURL + "/user/send_elite_verification_email/"

// AddMeToYourVip

let kAPIAddMeToYourVip                   = kBaseURL + "/user/request_to_be_VIP/"
let kAPIRespondToMyVipRequest            = kBaseURL + "/user/respond_to_want_to_be_VIP_Request/"
let KAPIVipRequestForMe                  = kBaseURL + "/user/get_VIP_requests_for_me/"


// InviteFriendsOnEvent
let kAPIGetFriendListToInvite          = kBaseURL + "/organizer/get_users_friends_for_event_invitation/"
let kAPIBusinessGetFriendListToInvite  = kBaseURL + "/business/get_users_friends_for_business_event_invitation/"
let kAPISearchFriendToInvite           = kBaseURL + "organizer/search_users_for_event_invitation/"
let KAPIInviteVIPCategory              = kBaseURL + "/organizer/invite_VIPs_by_category/"

// Filter Users
let KAPIListFilterTags                  = kBaseURL + "/filter/list_all_filters/"
let KAPIGetFilteredUser                 = kBaseURL + "/user/get_users_for_event_by_filter/"
let KAPIGetFilteredPeople               = kBaseURL + "/user/get_users_by_filter/"

//Upload Video
let KAPIUploadVideo                = kBaseURL + "upload_file/"

//delete Video
let kAPIDeleteMedia                = kBaseURL + "/delete_file/"
let kAPIDeleteMultipleMedia        = kBaseURL + "/delete_multiple_files/"

//Payment History
let kAPIRevenueListPaginated           = kBaseURL + "/event/revenue_of_organizer/"
let kAPIBusinessRevenueListPaginated   = kBaseURL + "/event/revenue_of_business/"
let KAPIEventRevenue                   = kBaseURL + "/event/revenue_of_event/"

//Notification
let KAPIListMessageAndNotificationForUser  = kBaseURL + "/message/list_messages"
let KAPIMakeNotificationSeen               = kBaseURL + "/message/make_message_seen/"
let KAPIDeleteParticularNotification       = kBaseURL + "/message/delete_message/"
let KAPINotificationListViewed             = kBaseURL + "/message/set_notification_list_viewed"
let KAPINotificationCount                  = kBaseURL + "/message/count_new_notifications"

//Rate Event
let KAPIRateEvent                        = kBaseURL + "/event/save_rating/"


//Premium
let kAPIUpgradePremium                    =  kBaseURL + "/user/upgrade_to_premium_account/"
let kAPICheckAutoRenewStatus              =  kBaseURL + "/user/check_receipt_auto_renew_status/"

// Email group
let KAPIEmailGroup                        = kBaseURL + "/organizer/send_group_email_to_attendees/"

//Post Media Vite Feed
let KAPIGetNearEvent                      = kBaseURL + "/feed/nearest_event"
let KAPIGetNearViteList                   = kBaseURL + "/event/list_vites_for_posting/"
let KAPIPostMediaToFeed                   = kBaseURL + "/feed/post_media_to_event"

//Rate and comment Organizer
let kGetRateAndCommentList                          = kBaseURL + "/event/list_comments/"
let kGetRateAndCommentListForBusinessAccount        = kBaseURL + "/event/list_business_comments/"
//Recurring Event

var kAPICreateRecurringEvent:String? {
    get {
        guard let fbID = UserDefaultsManager().userFBID else {
            return nil
        }
        return kBaseURL + "scheduled_events/save_event/" +  fbID
    }
}
let kAPIEditRecurringEvent     = kBaseURL + "/scheduled_events/update_scheduled_event/"
let kAPIDeleteRecurringEvent   = kBaseURL + "/scheduled_events/delete_scheduled_event/"
let kAPILiveFeed               = kBaseURL + "/feed/list_feeds"




func getDeleteEventAPIForEvent(eventID:String) -> String {
    return kAPIDeleteEvent + eventID
}

//Update User location
let kAPIUpdateUserLocation = kBaseURL + "/client/update_user_location/"

//let userID = (UserDefault().userFBID)
// var kAPIViewProile : = kBaseURL + "client/get_user_profile/\(userID)",


var kAPICreateEvent:String? {
    get {
        guard let fbID = UserDefaultsManager().userFBID else {
            return nil
        }
        return kBaseURL + "organizer/save_event/" +  fbID
    }
}


var kAPIViewProfile:String? {
    get {
        guard let fbID = UserDefaultsManager().userFBID else {
            return nil
        }
        return kBaseURL + "client/get_user_profile/" +  fbID
    }
}

//Message Center APIs
var kAPIMCVite:String? {
    get {
        guard let fbID = UserDefaultsManager().userFBID else {
            return nil
        }
        return kBaseURL + "client/get_message_center_vites/" +  fbID
    }
}

var kAPIMCRequest:String? {
    get {
        guard let fbID = UserDefaultsManager().userFBID else {
            return nil
        }
        return kBaseURL + "organizer/get_message_center_request/" +  fbID
    }
}

//Push Notificatopn API
var kAPIUpdatePushNotification:String? {
    get {
        guard let fbID = UserDefaultsManager().userFBID else {
            return nil
        }
        return kBaseURL + "/client/update_push_notification_setting/" +  fbID
    }
}

var KAPIGetPushNotification:String? {
    get {
        guard let fbID = UserDefaultsManager().userFBID else {
            return nil
        }
        return kBaseURL + "/client/get_push_notification_setting/" +  fbID
    }
}

//Facebook Event List

var KAPIGetFaceBookEventList:String? {
    get {
        guard let fbID = UserDefaultsManager().userFBID else {
            return nil
        }
        return kBaseURL + "/organizer/get_facebook_event_Id_list/" +  fbID
    }
}

//Create VIP List
var kAPICreateVIPList:String? {
    get {
        guard let fbID = UserDefaultsManager().userFBID else {
            return nil
        }
        return kBaseURL + "/organizer/create_vip/" +  fbID
    }
}

// Submit feedback
var KAPISubmitFeedback:String? {
    get {
        guard let fbID = UserDefaultsManager().userFBID else {
            return nil
        }
        return kBaseURL + "/client/send_feedback/" +  fbID
    }
}


var kAPIGetEventListsForQrScan:String? {
    get {
        guard let fbID = UserDefaultsManager().userFBID else {
            return nil
        }
        return kBaseURL + "/organizer/get_events_for_qr_code/" +  fbID
    }
}



