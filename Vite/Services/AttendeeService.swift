//
//  AttendeeService.swift
//  Vite
//
//  Created by Hem Poudyal on 1/29/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation
import ObjectMapper

struct AttendeeService {
    
    static func getAllAttendeeList(eventId:String, successBlock:@escaping (_ users:[MyEventAttendee]) -> Void, failureBlock:@escaping (_ message:String) -> Void) {
        
        WebService.request(method: .get, url: kAttendeeList + eventId, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String: AnyObject],
                let success = response["success"] as? Bool,
                let param = response["params"] as? [String:AnyObject], success == true
                else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                    return
            }
            
            if let responseArray =  param["users"] as? [[String:AnyObject]] {
                var attendeeList = [MyEventAttendee]()
                for user in responseArray {
                    if let u = Mapper<MyEventAttendee>().map(JSON: user) {
                        attendeeList.append(u)
                        print(user)
                    }
                }
                successBlock(attendeeList)
            } else {
                failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
            }
        }) { (message, error) in
            failureBlock(message)
        }
        
    }
   
    static func getAllPartiesOfEvent(eventId:String, successBlock:@escaping (_ useridArray:[String]) -> Void, failureBlock:@escaping (_ message:String) -> Void) {
    
        WebService.request(method: .get, url:  kAllPartyList + eventId, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String: AnyObject],
                let success = response["success"] as? Bool,
                let param = response["params"] as? [String:AnyObject], success == true
                else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                    return
            }
            let userIds = param["userFbIds"] as! [NSNumber]
            successBlock(userIds.map({ (num) -> String in
                return String(describing: num)
            }))
        }) { (message, error) in
             failureBlock(message)
        }
    }
    
    static func refundAllTicket(_ eventId:String, successBlock:@escaping (_ message:String) -> Void, failureBlock:@escaping (_ message:String) -> Void) {
     
        WebService.request(method: .post, url: KAPIRefundAllTicket + eventId, success: {
            (response) -> Void in
            guard let response = response.result.value as? [String: AnyObject],
                let success = response["success"] as? Bool,
                let msg = response["message"] as? String
                else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                    return
            }
            if success {
                successBlock(msg)
            } else {
                failureBlock(msg)
            }
        }, failure: { (message, error) in
            print(message)
        }
        )
 }
    
    static func refundTicket(_ eventId:String,attendeeId: String, successBlock:@escaping (_ message:String) -> Void, failureBlock:@escaping (_ message:String) -> Void) {
        WebService.request(method: .post, url: KAPIRefundAttendeeTicket + eventId + "/\(attendeeId)", success: {
            (response) -> Void in
            guard let response = response.result.value as? [String: AnyObject],
                let success = response["success"] as? Bool,
                let msg = response["message"] as? String
                else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                    return
            }
            if success {
                successBlock(msg)
            } else {
                failureBlock(msg)
            }
        }, failure: { (message, error) in
            print(message)
        }
        )
    }
}
