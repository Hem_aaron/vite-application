//
//  BaseWebService.swift
//  NHS
//
//  Created by Prajeet Shrestha on 8/4/17.
//  Copyright © 2017 Prajeet Shrestha. All rights reserved.
//

import Foundation
import Alamofire

public enum APIError:Error {
    case networkError, wrongResponse
}

enum networkStatus: Int{
    case success = 200
    case failure = 417
    case badRequest = 400
    case unAuthorized = 401
}

enum ErrorMessage:String {
    case generic = "There is some problem with your network. Please try again."
}

public typealias FailureBlock = (String, APIError) -> Void

class WebService {
    
    static func request(method:HTTPMethod,
                        url:String,
                        parameter:[String:AnyObject]? = nil,
                        header:HTTPHeaders? = nil,isAccessTokenRequired:Bool = true ,
                        success:@escaping (_ response:DataResponse<Any>) -> Void,
                        failure: @escaping FailureBlock) {
        
        print("\(method.rawValue) request at: \(url)")
        let param = alteredParameter(parameter: parameter) ?? nil
        let encoding:ParameterEncoding
        if method == .get {
            encoding = URLEncoding.default
        } else {
            encoding = JSONEncoding.default
        }
        var hdr:[String:String]?
        
        if isAccessTokenRequired {
            hdr = alteredHeader(header: header)
        }
        
        let request = Alamofire.request(url,
                                        method: method,
                                        parameters: param,
                                        encoding: encoding,
                                        headers: hdr)
        request.responseJSON { response in
            let statusCode = response.response?.statusCode
            let headers = response.response?.allHeaderFields
            let errorMsg = headers?["errorMessage"]
            if statusCode == networkStatus.success.rawValue {
                success(response)
            } else if statusCode == networkStatus.failure.rawValue {
                failureBlockHandler(genericErrorMessage: (errorMsg as? String)!,
                                    res: response,
                                    failure: failure)
            } else {
                failureBlockHandler(genericErrorMessage: ErrorMessage.generic.rawValue,
                                    res: response,
                                    failure: failure)
            }
        }
    }
    
   
    
    private static func alteredHeader(header:HTTPHeaders?) -> HTTPHeaders? {
        let user = UserDefaultsManager()
        var altHeader = [String:String]()
        if let hdr = header  {
            altHeader = hdr
        }
        if let userAccessToken = user.userAccessToken {
            altHeader["accessToken"] = userAccessToken
        }
        
        return altHeader
        
    }
    
    private static func alteredParameter(parameter:[String:AnyObject]?) -> [String:AnyObject]? {
        guard let param = parameter else {
            return parameter
        }
        return param
    }
    
    private static func failureBlockHandler(genericErrorMessage:String, res: DataResponse<Any>,
                                            failure:@escaping FailureBlock)  {
        if res.response?.statusCode == networkStatus.badRequest.rawValue || res.response?.statusCode == networkStatus.unAuthorized.rawValue {
            
            if res.response?.statusCode == networkStatus.unAuthorized.rawValue {
                /* if user accesstoken is expired then redirect to the login controller */
                //UIApplication.shared.keyWindow?.rootViewController = AppRouter.loginController
                return
            }
            
            if let errMessage = res.result.value as? [String:AnyObject] {
                let err = errMessage["error"] as? [String:AnyObject]
                let message = err?["message"] as? String
                failure((message ?? genericErrorMessage), .wrongResponse)
            } else {
                failure(genericErrorMessage, .wrongResponse)
            }
        } else if res.response?.statusCode == networkStatus.failure.rawValue {
            failure(genericErrorMessage, .wrongResponse)
        } else {
            let message:[String: String] = ["message": res.result.error?.localizedDescription ?? genericErrorMessage]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationNoInternet), object: nil, userInfo: message)
           // failure("NETWORK ERROR", .networkError)
        }
    }
    
    static func cancelPreviousRequests(completionBlock:@escaping () -> Void) {
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach { $0.cancel() }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() }
            completionBlock()
        }
    }
    /* This method is for download */
    static func requestData(
        url:String,
        parameter:[String:AnyObject]? = nil,
        header:HTTPHeaders? = nil,
        success:@escaping (_ response:Data?) -> Void,
        failure: @escaping FailureBlock) {
        
        let param = alteredParameter(parameter: parameter) ?? nil
        let hdr = alteredHeader(header: header) ?? nil
        let request = Alamofire.request(url,
                                        parameters: param,
                                        encoding: URLEncoding.default,
                                        headers: hdr)
        
        Alamofire.download(url).responseData { response in
            if let data = response.result.value {
                print(data)
                
                
            }
        }
        
        request.responseData { response in
            
            if response.response?.statusCode == networkStatus.success.rawValue {
                success(response.result.value)
            } else {
                if let e = response.error, e._code == -999 {
                    
                } else {
                    failure("Can't read PDF", APIError.wrongResponse)
                }
            }
        }
    }
    
    // MARK: Upload multiple images and videos
    
    static func uploadMedia(_ mediaData: Data, videoThumbnailData: Data, mediaName: String, mediaFormat: String, apiURL: String, successBlock:@escaping ((_ response:AnyObject?) -> Void),
                           progressBlock:@escaping ((_ progressData:UploadRequest) -> Void),
                           failureBlock:@escaping ((_ message:String) -> Void)){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            //thumbnail data is sent incase for video
            if mediaFormat == "mov/mp4"{
                var thumbNailFileNameArr = mediaName.split{$0 == "."}.map(String.init)
                let thumbNailFileName: String = thumbNailFileNameArr[0] + ".jpg"
                multipartFormData.append(videoThumbnailData, withName: "thumbnail", fileName: thumbNailFileName, mimeType: "jpeg/png")
            }
            multipartFormData.append(mediaData, withName: "file", fileName: mediaName, mimeType: mediaFormat)
            
        }, to: apiURL) { (result) in
            switch result {
            case .success(let upload, _, _):
                progressBlock(upload)
                upload.responseJSON { response in
                    
                    let statusCode = response.response?.statusCode
                    let headers = response.response?.allHeaderFields
                    let errorMsg = headers?["errorMessage"]
                    guard statusCode == 200 else {
                        //SVProgressHUD.dismiss()
                        if statusCode == 417 && errorMsg != nil{
                            failureBlock((errorMsg as? String)!)
                        }
                        else{
                            print(statusCode)
                              print(errorMsg)
                            failureBlock(ErrorMessage.generic.rawValue)
                        }
                        return
                    }
                    
                    if let httpError = response.result.error {
                        //failureBlock("Serialization Error! \(httpError.code)")
                        return
                    }
                    //SVProgressHUD.dismiss()
                    successBlock( response.result.value as AnyObject)
                    
                    
                }
            case .failure(let encodingError):
                // failureBlock(String(encodingError))
                print(encodingError.localizedDescription)
            }
        }
    }
}
