//
//  BranchShareHelper.swift
//  Vite
//
//  Created by Ujjwal Shrestha on 1/5/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import Branch


var branchUniversalObject: BranchUniversalObject!


class BranchShareHelper {
    
    //Share with Branch 

    class func ShareWithBranchActivity(_ controller:UIViewController,eventID:String,eventTitle:String,eventDesc:String,shareText:String,imageURL:String){
        

        branchUniversalObject = BranchUniversalObject(canonicalIdentifier: "item_id_12345")
        branchUniversalObject.title = eventTitle
        branchUniversalObject.contentDescription = eventDesc
        branchUniversalObject.imageUrl = imageURL
        branchUniversalObject.addMetadataKey("userID", value: UserDefaultsManager().userFBID!)
        branchUniversalObject.addMetadataKey("eventID", value: eventID)
        branchUniversalObject.userCompletedAction(BNCRegisterViewEvent)
        
        let linkProperties: BranchLinkProperties = BranchLinkProperties()
        linkProperties.feature = "sharing"

        // Show the share sheet for the content you want the user to share. A link will be automatically created and put in the message.
        branchUniversalObject.showShareSheet(with: linkProperties,
                                                               andShareText: eventTitle,
                                                               from: controller,
                                                               completion: { (activityType, completed) in
                                                                if (completed) {
                                                                    // This code path is executed if a successful share occurs
                                                                }
        })
        
    }


    
    
    //Share with Branch
    class func ShareEventDetail(_ controller:UIViewController,eventID:String,eventTitle:String,eventDesc:String,shareText:String,imageURL:String){
        branchUniversalObject = BranchUniversalObject(canonicalIdentifier: "item_id_12345")

        branchUniversalObject.title = eventTitle
        branchUniversalObject.contentDescription = eventDesc
        branchUniversalObject.imageUrl = imageURL
        branchUniversalObject.addMetadataKey("userID", value: UserDefaultsManager().userFBID!)
        branchUniversalObject.addMetadataKey("eventID", value: eventID)
        branchUniversalObject.addMetadataKey("isFromHome", value: "true")
        branchUniversalObject.userCompletedAction(BNCRegisterViewEvent)
        
        let linkProperties: BranchLinkProperties = BranchLinkProperties()
        linkProperties.feature = "sharing"

        
        // Show the share sheet for the content you want the user to share. A link will be automatically created and put in the message.
        branchUniversalObject.showShareSheet(with: linkProperties,
                                                               andShareText: "\(eventTitle)\n",
                                                               from: controller,
                                                               completion: { (activityType, completed) in
                                                                if (completed) {
                                                                    // This code path is executed if a successful share occurs
                                                                }
        })
        
    }
    
    
}
