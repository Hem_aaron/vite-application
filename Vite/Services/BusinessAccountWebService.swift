//
//  BusinessAccountWebService.swift
//  Vite
//
//  Created by sajjan giri on 11/13/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper

class BusinessAccountWebService {
    func getUserVIPList(_ page:Int,successBlock:@escaping ((_ friendArray:[MyVIPFriend])->()) , failureBlock:@escaping ((_ message:String) -> ())) {
        
        WebService.request(method: .get, url: KListAllUser + "\(page)/10", success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let responseArray =  param["users"] as? [[String:AnyObject]] {
                    var friendList = [MyVIPFriend]()
                    for friend in responseArray {
                        if let u = Mapper<MyVIPFriend>().map(JSON: friend) {
                            friendList.append(u)
                        }
                    }
                    successBlock(friendList)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    func getBusinessAccountList(_ successBlock:@escaping ((_ businessAccountList:[BusinessAccountUserModel])->()) , failureBlock:@escaping ((_ message:String) -> ())) {
        
        let fbID = UserDefaultsManager().userFBID!
        
        WebService.request(method: .get, url: kGetAllBusinessAccount + fbID, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let responseArray =  param["businessAccounts"] as? [[String:AnyObject]] {
                    var businessAccountList = [BusinessAccountUserModel]()
                    for business in responseArray {
                        if let u = Mapper<BusinessAccountUserModel>().map(JSON: business) {
                            businessAccountList.append(u)
                        }
                    }
                    successBlock(businessAccountList)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    func getBusinessProfile(_ businessId: String, successBlock:@escaping ((_ businessAccount:BusinessAccountUserModel)->()) , failureBlock:@escaping ((_ message:String) -> ())) {
        
        WebService.request(method: .get, url: KAPIGetBUsinessProfile + businessId , success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let responseArray =  param["business"] as? [String:AnyObject] {
                    if let u = Mapper<BusinessAccountUserModel>().map(JSON: responseArray) {
                        successBlock(u)
                    }
                    
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    
    func createBusinessAccount(_ param: [String: AnyObject] , successBlock:@escaping ((_ message: String)->()) , failureBlock:@escaping ((_ message:String) -> ())) {
        WebService.request(method: .post, url: kCreateBusinessAccount + UserDefaultsManager().userFBID!, parameter: param, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let msg = response["message"] as? String  {
                    successBlock(msg)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    
    func updateBusinessAccount(_ param: [String: AnyObject] , successBlock:@escaping ((_ message: String)->()) , failureBlock:@escaping ((_ message:String) -> ())) {
        WebService.request(method: .post, url: kUpdateBusinessAccount, parameter: param, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let msg = response["message"] as? String  {
                    successBlock(msg)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    
    
    func deleteBusinessAccount(_ businessId: String , successBlock:@escaping ((_ message: String)->()) , failureBlock:@escaping ((_ message:String) -> ())) {
        
        WebService.request(method: .delete, url: kDeleteBusinessAccount + businessId, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let msg = response["message"] as? String  {
                  successBlock(msg)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    func deleteMemberFromBusinessAccount( _ businessId: String ,_ param: [String: AnyObject] , successBlock:@escaping ((_ message: String)->()) , failureBlock:@escaping ((_ message:String) -> ())) {
        WebService.request(method: .post, url: KAPIDeleteMemberBusinessAccount + businessId, parameter: param, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let msg = response["message"] as? String  {
                    successBlock(msg)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    
    func makeBusinessAccountPrimary( _ businessId: String ,_ status: Bool, successBlock:@escaping ((_ message: String)->()) , failureBlock:@escaping ((_ message:String) -> ())) {
          let url = kAPITogglePrimaryBusinessAccount +  "\(businessId)" + "/\(UserDefaultsManager().userFBID!)" + "/\(status)"
        
        WebService.request(method: .post, url: url, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let msg = response["message"] as? String  {
                    successBlock(msg)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
}
