//
//  EventServices.swift
//  Vite
//
//  Created by Hem Poudyal on 1/5/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation
import ObjectMapper

struct EventServices {
    static var userDefault = UserDefaultsManager()
    
    static func getEventListForQrScan(sucessBlock: @escaping ((_ eventList : [EventsOrganizer], _ count:Int) -> ()),failureBlock:@escaping ((String)) -> ()) {
        let url = userDefault.isBusinessAccountActivated ?  kListBusinessEvents + userDefault.userBusinessAccountID! : kAPIGetEventListsForQrScan
        WebService.request(method: .get, url: url!, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            var events = [EventsOrganizer]()
            var counts = Int()
            if success{
                if let eventData = param["events"] as? [[String:AnyObject]] {
                    for eventList in eventData {
                        if let eventInfo  = Mapper<EventsOrganizer>().map(JSON: eventList) {
                            events.append(eventInfo)
                        }
                    }
                }
                if let count = param["count"] as? Int {
                    counts = count
                }
                sucessBlock(events, counts)
            }
            
            
        }) { (message, error) in
            failureBlock(message)
        }
    }
    
    
    static func getEventAttendees(eventID:String,
                                  completionBlock:@escaping ((_ attendeeList:[MyEventAttendee]?) -> ())) {
        
        WebService.request(method: .get, url: kAttendeeList + eventID, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            //print(response)
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                var eventAttendees = [MyEventAttendee]()
                if let userList = param["users"] as? [[String:AnyObject]] {
                    for user in userList {
                        if let eventUser = Mapper<MyEventAttendee>().map(JSON: user) {
                            eventAttendees.append(eventUser)
                        }
                    }
                }
                completionBlock(eventAttendees)
            } else {
                completionBlock(nil)
            }
        }) { (message, error) in
            completionBlock(nil)
            print(message)
        }
    }
    
    static func respondToRequest(status:Bool,userFbID:String, eventID:String,completionBlock:@escaping ((_ status:Bool) -> ())) {
        
        var responseStatus = String()
        if status {
            responseStatus = "INVITED"
        } else {
            responseStatus = "ORGANIZER_REJECTED"
        }
        
        let parameters:[String:AnyObject] =
            [
                "viteStatus":responseStatus as AnyObject,
                ]
        
        WebService.request(method: .post, url: kRespondToRequestFromUser + userFbID + "/" + eventID, parameter: parameters, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as! [String: AnyObject]? else {
                completionBlock(false)
                return
            }
            guard let success = response["success"] as? Bool else {
                completionBlock(false)
                return
            }
            
            if success {
                completionBlock(true)
            } else {
                completionBlock(false)
            }
            
        }) { (messageString, error) in
            completionBlock(false)
        }
    }
    
    static func getEventDetail(eventID:String,
                               successBlock:@escaping ((_ event:EventsOrganizer?) -> ())) {
        WebService.request(method: .get, url: kAPIGetEventDetail + eventID, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                successBlock(nil)
                return
            }
            
            guard let params = response["params"] as? [String:AnyObject] else {
                return
            }
            
            if let eventDetail = params["eventDetail"] as? [String:AnyObject] {
                let eventInfo = Mapper<EventsOrganizer>().map(JSON: eventDetail)
                successBlock(eventInfo)
            }
            
        }) { (message, error) in
            successBlock(nil)
        }
        
    }
    
    static func getVEventDetail(eventID:String,
                               successBlock:@escaping ((_ event:VEvent?) -> ())) {
        WebService.request(method: .get, url: kAPIGetEventDetail + eventID, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                successBlock(nil)
                return
            }
            
            guard let params = response["params"] as? [String:AnyObject] else {
                return
            }
            
            if let eventDetail = params["eventDetail"] as? [String:AnyObject] {
                let eventInfo = Mapper<VEvent>().map(JSON: eventDetail)
                successBlock(eventInfo)
            }
            
        }) { (message, error) in
            successBlock(nil)
        }
        
    }
    
    static func getEventListWithPagination(_ pageNumber:Int, pageSize: Int, successBlock:@escaping ((_ eventList:[VEvent]?, _ eventListEventOrg:[EventsOrganizer]? , _ totalCount:NSNumber) -> ()), failureBlock:@escaping (_ message:String) -> Void) {
        // if businesss account is activated then get list of business events
        var url = ""
        
        if UserDefaultsManager().isBusinessAccountActivated {
            url = kListBusinessEventsPaginated + UserDefaultsManager().userBusinessAccountID!
        } else {
            url = kListEventsWithPagination + UserDefaultsManager().userFBID!
        }
        url = url + "/\(pageNumber)/\(pageSize)"
        
        WebService.request(method: .get, url: url, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as! [String: AnyObject]?,
                let success = response["success"] as? Bool else {
                    return
            }
            if success {
                let responeArray = response["params"]?["events"] as! [[String:AnyObject]]
                
                print(response)
                var eventList = [VEvent]()
                print(eventList)
                var eventListOrg = [EventsOrganizer]()
                for obj in responeArray {
                    let event = Mapper<VEvent>().map(JSONObject: obj)
                    let eventOrg = Mapper<EventsOrganizer>().map(JSONObject: obj)
                  
                    if let e = event, let eOrg = eventOrg {
                        eventList.append(e)
                        eventListOrg.append(eOrg)
                    }
                }
                let totalCount = response["params"]?["count"] as! NSNumber
                successBlock(eventList, eventListOrg, totalCount)
            }
        }) { (message, apiError) in
            failureBlock(message)
        }
    }
    
    static func getEventListWithPaginationForPosting(_ pageNumber:Int, pageSize: Int, successBlock:@escaping ((_ eventList:[VVite], _ totalCount:NSNumber) -> ()), failureBlock:@escaping (_ message:String) -> Void) {
        // if businesss account is activated then get list of business events
        let url = kListEventsForPosting + "\(pageNumber)/\(pageSize)"
        
        WebService.request(method: .get, url: url, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as! [String: AnyObject]?,
                let success = response["success"] as? Bool else {
                    return
            }
            if success {
                let responeArray = response["params"]?["events"] as! [[String:AnyObject]]
                var eventList = [VVite]()
                for obj in responeArray {
                    let event = Mapper<VVite>().map(JSONObject: obj)
                    if let e = event{
                        eventList.append(e)
                    }
                }
                let totalCount = response["params"]?["count"] as! NSNumber
                successBlock(eventList, totalCount)
            }
        }) { (message, apiError) in
            failureBlock(message)
        }
    }
    
    
    func deleteAllSessionOfScheduledEvent(_ scheduledEventId: String,successBlock:@escaping ((_ msg: String) -> ()),failureBlock:@escaping ((_ msg: String) -> ())){
        
        WebService.request(method: .delete, url: kAPIDeleteRecurringEvent + scheduledEventId, success: { (response) in
            guard let response = response.result.value as? [String: AnyObject] else {
                return
            }
            
            guard let success = response["success"] as? Bool else {
            failureBlock(localizedString(forKey: "ERROR_DELETING_SCHEDULED_EVENT"))
            return
        }
        if success{
            if let successMsg = response["message"] as? String{
                successBlock(successMsg)
            }else {
                successBlock(localizedString(forKey: "EVENT_DELTED"))
            }
        }
        }, failure: {(message, apiError) in
            failureBlock(message)
        })
        }

    
    func getEventRequesters(_ eventID:String,
                            completionBlock:@escaping ((_ requesterList:[RequestedUser]?) -> ()))
    {
        WebService.request(method: .get, url: kAttendeeNewRequests + eventID, success: {
            (response) -> Void in
            guard let response = response.result.value as? [String : AnyObject] else {
                completionBlock(nil)
                return
            }
            print(response)
            guard let success = response["success"] as? Bool else {
                completionBlock(nil)
                return
            }
            if success {

                let responeArray = response["params"]?["users"]  as! [[String:AnyObject]]

                var requesterArray = [RequestedUser]()
                for userData in responeArray {
                    
                    let user = Mapper<RequestedUser>().map(JSONObject: userData)
                    
                    if let userInfo = user {
                        requesterArray.append(userInfo)
                    }
                }
                completionBlock(requesterArray)
            } else {
                completionBlock(nil)
            }
            
        }, failure: {
            (message, err) -> Void in
            completionBlock(nil)
            
        })
      
    }
}


