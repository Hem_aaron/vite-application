//
//  LoginService.swift
//  Vite
//
//  Created by Eeposit 01 on 12/5/17.
//  Copyright © 2017 Eepos IT. All rights reserved.
//

import Foundation
import ObjectMapper
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit


class FacebookLoginService {
    
        // Response will return Dictionary of User profile.
        // Token is facbook Token string
        // id is unique fbID for the user
        class func login(controller:UIViewController, successBlock:@escaping (_ status:Bool,_ response:AnyObject?, _ token:String?, _ id:String?) -> Void) {
            
            
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            loginManager.logIn(withReadPermissions: ["public_profile","email","user_friends","user_photos","user_birthday","user_events","user_link",], from: controller) {
                (result,error ) -> Void in
                
                if (error != nil) {
                    successBlock(false, nil, nil,nil )
                    return
                }
                
                if (result?.isCancelled)! {
                    successBlock(false, nil , nil,nil)
                } else {
                    let tokenString = result?.token.tokenString
                    FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,first_name,last_name,picture.width(500).height(500),birthday,gender,events,link"]).start(completionHandler: { (connection, result, error) -> Void in
                        guard let result  = result as? [String : Any] else {
                            print("result error")
                            return
                        }
                        if error != nil {
                            successBlock(false, nil, nil,nil)
                            
                        } else {
                            print(result)
                            // id , name
                            successBlock(true, result as AnyObject, tokenString ,result["id"] as? String)
                            
                        }
                        
                    })
                }
            }
        }
        
        class func friendListOfUser(completionHandler:@escaping ([FacebookFriend]?) -> ()) {
            
            FBSDKGraphRequest(graphPath: "me",
                              parameters: ["fields":"friends"]).start(
                                completionHandler: {
                                    (connection, result, error) -> Void in
                                    if error == nil {
                                        guard let results  = result as? [String : Any] else {
                                            return
                                        }
                                        let friends = results["friends"] as? [String:Any]
                                        let friendData = friends!["data"] as? [String:Any]
                                        guard let friendsArray = friendData else {
                                            return
                                        }
                                        var arrayOfFriends:[FacebookFriend] = [FacebookFriend]()
                                       
                                        for friendDictionary in friendsArray {
                                            if let friends = Mapper<FacebookFriend>().map(JSONObject: friendDictionary) {
                                                arrayOfFriends.append(friends)
                                            }
                                        }
                                        completionHandler(arrayOfFriends)
                                        
                                    } else {
                                        completionHandler(nil)
                                        print("Error Getting Friends \(String(describing: error))");
                                        
                                    }
                              })
            
        }
}
