//
//  FacebookSharingHelper.swift
//  Royal
//
//  Created by Eeposit1 on 2/8/16.
//  Copyright © 2016 Prajeet Shrestha. All rights reserved.
//

//Reference : https://developers.facebook.com/docs/sharing/ios

import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import Foundation


class FacebookShareHelper {
    /*
    1. contentURL - The link to be shared
    2. contentTitle - Represents the title of the content in the link
    3. imageURL - the URL of thumbnail image that appears on the post
    4. contentDescription - of the content, usually 2-4 sentences
    
    If your app share links to the iTunes or Google Play stores, we do not post any images or descriptions that you specify in the share. Instead we post some app information we scrape from the app store directly with the Webcrawler. This may not include images. To preview a link share to iTunes or Google Play, enter your URL into the URL Debugger.
    
    */
    static var contentURL:String?
    var contentTitle:String?
    var imageURL:String?
    var contentDescription:String?
    
    class func  sharelink() {
        let linkContent = FBSDKShareLinkContent()
        linkContent.contentURL = URL(string: contentURL!)
    }
    
    /*
    Photos must be less than 12MB in size
    People will need native facebook app installed
    */
    
    func sharePhoto() {
        
    }
    
    //MARK: Sharing Methods
    class func addLikeButton(_ view:UIView) {
        let button = FBSDKLikeControl()
        button.objectID = "https://www.facebook.com/FacebookDevelopers"
        view.addSubview(button)
    }
    
    class func addShareButton(_ view:UIView) {
        let button = FBSDKShareButton()
        let linkContent = FBSDKShareLinkContent()
        linkContent.contentURL = URL(string: "https://www.facebook.com/FacebookDevelopers")
        button.shareContent = linkContent
        view.addSubview(button)
    }
    
    class func addSendButton(_ view:UIView) {
        let button = FBSDKSendButton()
        let linkContent = FBSDKShareLinkContent()
        linkContent.contentURL = URL(string: "https://www.facebook.com/FacebookDevelopers")
        button.shareContent = linkContent
        view.addSubview(button)
    }
    
    class func shareWithShareDialog(_ controller:UIViewController, urlString:String) {
        let content : FBSDKShareLinkContent = FBSDKShareLinkContent()
        content.contentURL = URL(string: urlString)
        FBSDKShareDialog.show(from: controller, with: content, delegate: nil)
    }
    
    /*
    class func shareWithMessenger() {
        let content : FBSDKShareLinkContent = FBSDKShareLinkContent()
        content.contentURL = URL(string: "https://www.facebook.com/FacebookDevelopers")
        content.contentTitle = "MyApp"
        content.contentDescription = "No Description"
        FBSDKMessageDialog.show(with: content, delegate: nil)
    }
    */
    
    func shareFB(_ sender: AnyObject){
        
        
    }
    
    
    class func fbShare(_ controller:UIViewController, urlString:String, imageURL:String, title:String) {
        let content : FBSDKShareLinkContent = FBSDKShareLinkContent()
        content.contentURL = URL(string: urlString)
        //content.imageURL = URL(string: imageURL)
       // content.contentTitle  = title
        FBSDKShareDialog.show(from: controller, with: content, delegate: nil)
        
    }
}
