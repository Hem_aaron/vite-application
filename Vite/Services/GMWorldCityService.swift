//
//  GMWorldCityService.swift
//  Vite
//
//  Created by Prajeet Shrestha on 8/25/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class Prediction: Mappable {
    
    var des:String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        des <- map["description"]
    }
}

class GoogleMapPredictionResponseModel:Mappable {
    
    var predictions:[Prediction]?
    var status:String?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        predictions <- map["predictions"]
        status <- map["status"]
    }
}

class GMWorldCityService {
    func getWorldCityServiceWithSearchString(searchString:String,
                                             successBlock:@escaping ((_ predictions:[Prediction]) -> Void),
                                             failureBlock:@escaping ((_ message:String) -> Void)) {
        
        let mapApiUrl = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
        let googleMapKey = "AIzaSyBquEZhJfRpqSiDQinmNZT4PV-iZo09X-A"
        let parameters: [String:Any] = [
            "input": searchString,
            "types": "(cities)",
            "key": googleMapKey
        ]
        WebService.request(method: .get, url: mapApiUrl, parameter: parameters as [String : AnyObject], header: nil, isAccessTokenRequired: false, success: { (response) in
            if let res = response.result.value as? [String:AnyObject] {
                                    if let predicitonResponseModel = Mapper<GoogleMapPredictionResponseModel>().map(JSON: res) {
                                        successBlock(predicitonResponseModel.predictions ?? [Prediction]())
                                    } else {
                                        failureBlock(localizedString(forKey: "FETCH_LOCATION_ERROR"))
                                    }
                                } else {
                failureBlock(localizedString(forKey: "FETCH_LOCATION_ERROR"))
                                }
        }) { (messaage, apiError) in
            
        }
    }
}

