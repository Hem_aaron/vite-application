//
//  UserAccountFetcher.swift
//  Vite
//
//  Created by Hem Poudyal on 1/4/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation
struct UserAccountFetcher {
    var userDefault = UserDefaultsManager()
    
    func fetchBusinessAccount() -> UserMiniModel {
        let id = userDefault.userBusinessAccountID
        let fullName = userDefault.userBusinessFullName
        let profileURL = userDefault.userBusinessProfileImageUrl ?? ""
        return UserMiniModel(id: id!, profileImageURL: profileURL, fullName: fullName!)
    }
    
    func saveBusinessAccount(userInfo:UserMiniModel) {
        userDefault.userBusinessAccountID = userInfo.id
        userDefault.userBusinessFullName = userInfo.fullName
        userDefault.userBusinessProfileImageUrl = userInfo.profileImageURL
    }
    
    func fetchpersonalAccount() -> UserMiniModel {
        let id = userDefault.userFBID
        let fullName = userDefault.userFBFullName
        let profileURL = userDefault.userFBProfileURL ?? ""
        return UserMiniModel(id: id!, profileImageURL: profileURL, fullName: fullName!)
    }
}
