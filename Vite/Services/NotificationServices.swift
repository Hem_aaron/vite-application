//
//  NotificationServices.swift
//  Vite
//
//  Created by Eeposit 01 on 4/25/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit
import ObjectMapper

class NotificationServices {
    
    
    static func listMessageAndNotificationForUser(_ successBlock:@escaping (_ listOfNotificationMsg :[Notification]) -> Void, failureBlock:@escaping ((_ message:String) -> Void)){
        let indicator = ActivityIndicator()
        indicator.start()
        WebService.request(method: .get, url: KAPIListMessageAndNotificationForUser, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] ,
                let success = response["success"] as? Bool else {return}
            if success {
                var notificationValue = [Notification]()
                guard  let responseParam = response["params"] as? [String:AnyObject] ,
                    let responseData  = responseParam["messages"] as? [[String:AnyObject]] else {
                        
                        return
                }
                for message in responseData {
                    if let msgData = Mapper<Notification>().map(JSON: message){
                        notificationValue.append(msgData)
                    }
                }
                indicator.stop()
                successBlock(notificationValue)
            }
        }) { (message, apiError) in
            indicator.stop()
            failureBlock(message)
        }
        
    }
    
    static func makeNotifiactionSeeen(_ messageId : String,successBlock:@escaping (_ status :Bool) -> Void, failureBlock:@escaping ((_ message:String) -> Void)){
        let indicator = ActivityIndicator()
        indicator.start()
        WebService.request(method: .put, url: KAPIMakeNotificationSeen + messageId, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] ,
                let success = response["success"] as? Bool else {return}
            if success {
                successBlock(success)
                indicator.stop()
            }
        }) { (message, apiError) in
            failureBlock(message)
            indicator.stop()
        }
    }
    
    static func deleteParticularNotification(_ messageId : String,successBlock:@escaping (_ successMsg :String) -> Void, failureBlock:@escaping ((_ message:String) -> Void)){
        
        let indicator = ActivityIndicator()
        WebService.request(method: .delete, url: KAPIDeleteParticularNotification + messageId, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject],
                let success = response["success"] as? Bool else {return}
            if success {
                guard let message = response["message"] as? String else {return}
                successBlock(message)
                indicator.stop()
            }
        }) { (message, apiError) in
            failureBlock(message)
            indicator.stop()
        }
    }
    static func notificaltionListViewed (_ successBlock : @escaping (_ successMsg : String) -> Void , failureBlock:@escaping (_ messgae:String) -> Void) {
        
        WebService.request(method: .put, url: KAPINotificationListViewed, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response  = response.result.value as? [String:AnyObject],
                let success = response["success"] as? Bool else {return}
            if success {
                guard let message = response["message"] as? String else {return}
                successBlock(message)
            }
        }) { (message, apiError) in
            failureBlock(message)
        }
    }
    
    static func getNotificationCount(_ successBlock: @escaping (_ count: Int) -> Void , failureBlock : @escaping (_ message: String) -> Void) {
        WebService.request(method: .get, url: KAPINotificationCount, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject],
                let success  = response["success"] as? Bool else {return}
            if success {
                guard let param = response["params"] as? [String:AnyObject],
                    let count = param["count"] as? Int else {return}
                successBlock(count)
            }
        }) { (message, apiError) in
            failureBlock(message)
        }
    }
}







