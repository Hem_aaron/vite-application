//
//  PaypalWebService.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 4/25/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//


import Foundation
import ObjectMapper

class PaypalWebService{
    
    static func addPaypalEmail(param: [String: AnyObject],  successBlock: @escaping ((_ msg: String) ->()), failureBlock: @escaping ((_ msg: String) ->())){
        WebService.request(method: .post, url: KAPIAddPaypalEmail, parameter: param, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String: AnyObject],
                let success = response["success"] as? Bool,
                let successMsg = response["message"] as? String, success == true
                else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                    return
            }
            successBlock(successMsg)
        }) { (message, apiError) in
            failureBlock(message)
        }
    }

    static func checkPaypalStatus(fbId: String, _ successBlock:@escaping ((_ paypalStatus: Bool) -> Void), failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .get, url: KAPICheckPaypalStatus + fbId, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            guard let param = response["params"] as?  [String:AnyObject] else {
                return
            }
            print(response)
            if success {
                if  let paypalStatus = param["status"] as? Bool {
                    successBlock(paypalStatus)
                }
                
            }
            
        }) { (msg, err) in
            failureBlock(msg)
        }
    }

    
}


