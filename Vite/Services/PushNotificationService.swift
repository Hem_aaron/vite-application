//
//  PushNotificationService.swift
//  Vite
//
//  Created by Hem Poudyal on 3/24/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit

class PushNotificationService: NSObject {
    
    class func sendPushNotificationsWithParameter(parameter: [String: AnyObject]) {
        WebService.request(method: .post, url: kAPISendPushNotification, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
        }) { (message, apiError) in
        }
    }
}
