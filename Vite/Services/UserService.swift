//
//  UserService.swift
//  Vite
//
//  Created by Hem Poudyal on 1/2/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation
import ObjectMapper
import UIKit

struct UserService {
    static var fbId = UserDefaultsManager().userFBID!
    static func getUserInfo(successBlock:@escaping ((_ userInfo:UserModel) ->()) , failureBlock:((String)) -> ()) {
        
        WebService.request(method: .get, url: kGetUserMiniInfo + fbId, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            if let userData = param["user"] as? [String:AnyObject] {
                let userInfo = Mapper<UserModel>().map(JSON: userData)
                successBlock(userInfo!)
            }
        }) { (message, error) in
            
        }
     }
}
