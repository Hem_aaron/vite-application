//
//  VCommentService.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 8/3/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//
import Foundation
import ObjectMapper

class VCommentService: NSObject {
    
    static func getMediaCommentList(feedId: String,pageNumber:Int, pageSize: Int, successBlock:@escaping ((_ comments:[VComment] , _ commentCount: Int) -> ()), failureBlock:@escaping ((_ message:String)->())) {
        
        let commentListUrl = KAPICommentList + "\(feedId)/" + "\(pageNumber)/\(pageSize)"
        WebService.request(method: .get, url: commentListUrl, success: { (response) in
            guard let response = response.result.value as? [String: AnyObject],
                            let success = response["success"] as? Bool,
                          let param = response["params"] as? [String:AnyObject], success == true
                            else {
                                failureBlock("Response not in proper format")
                                return
                        }
            if let responseArray =  param["comments"] as? [[String:AnyObject]],let count = param["count"] as? Int {
                                var commentList = [VComment]()
                                for user in responseArray {
                                    if let u = Mapper<VComment>().map(JSON: user) {
                                        commentList.append(u)
                                    }
                                }
                
                successBlock(commentList, count)
                            } else {
                failureBlock("Response not in proper format")
                            }
        }) { (message, apiError) in
            failureBlock(message)
        }
    }
    
    static func addMediaComment(feedId: String, param: [String: AnyObject],  successBlock: @escaping ((_ msg: String) ->()), failureBlock: @escaping ((_ msg: String) ->())){
        
        let addCommentUrl = KAPIAddComment + feedId
        WebService.request(method: .post, url: addCommentUrl, parameter: param, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String: AnyObject],
                            let success = response["success"] as? Bool,
                let successMsg = response["message"] as? String, success == true
                            else {
                                failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                                return
                        }
            successBlock(successMsg)
        }) { (message, apiError) in
            failureBlock(message)
        }
    }
    
}

