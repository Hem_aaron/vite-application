//
//  VCreateEventService.swift
//  Vite
//
//  Created by Hem Poudyal on 6/23/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation
struct VCreateEventService {
    static func editEvent(_ parameter:[String:AnyObject], successBlock:@escaping ((_ event:AnyObject) -> Void), failureBlock: @escaping ((_ message:String) -> Void)) {
        
        WebService.request(method: .put, url: kAPIEditEvent, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
        guard let param = response["params"] as? [String:AnyObject] else {
            return
        }
        guard let success = response["success"] as? Bool else {
            return
        }
        if success {
            if let event = param["event"] as? [String:AnyObject] {
                successBlock(event as AnyObject)
            }
        }
        }) { (msg, err) in
             failureBlock(msg)
        }
    }
    
    static func createEvent(_ parameter:[String:AnyObject], successBlock:@escaping (_ eventID:String, _ imageUrl:String) -> Void, failureBlock:@escaping ((_ message:String) -> Void)) {
        guard let createEventAPI = kAPICreateEvent else {
            return
        }
        WebService.request(method: .post, url: createEventAPI, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
            
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if  let params    = response["params"] as? [String:AnyObject],
                    let event     = params["event"] as? [String:AnyObject] {
                    let eventID   = event["entityId"] as! String
                    let imageUrl  = event["imageUrl"] as! String
                    successBlock(eventID, imageUrl)
                }
            } else {
                failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func createRecurringEvent(_ parameter:[String:AnyObject], successBlock:@escaping (_ eventID:String, _ imageUrl:String) -> Void, failureBlock:@escaping ((_ message:String) -> Void)) {
        guard let createRecurringEventAPI = kAPICreateRecurringEvent else {
            return
        }
        WebService.request(method: .post, url: createRecurringEventAPI, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if  let params    = response["params"] as? [String:AnyObject],
                    let event     = params["event"] as? [String:AnyObject] {
                    let eventID   = event["entityId"] as! String
                    let imageUrl  = event["imageUrl"] as! String
                    successBlock(eventID, imageUrl)
                }
            } else {
                failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func editRecurringEvent(_ eventId: String, parameter:[String:AnyObject], successBlock:@escaping (_ imageUrl:String) -> Void, failureBlock:@escaping ((_ message:String) -> Void)) {
        
        WebService.request(method: .post, url: kAPIEditRecurringEvent, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if  let params    = response["params"] as? [String:AnyObject],
                    let event     = params["scheduledEvent"] as? [String:AnyObject] {
                    let imageUrl  = event["imageUrl"] as! String
                      successBlock(imageUrl)
                }
            } else {
                failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func sendViteNow(_ eventId: String, successBlock:@escaping (_ success:Bool) -> Void, failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .post, url: kAPISendViteNow + eventId + "/EVENT_CREATE", success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                successBlock(true)
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
}
