//
//  VEventWebService.swift
//  Vite
//
//  Created by Eeposit1 on 6/20/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper
struct VEventWebService {
    static func getFilteredEvents(_ successBlock:@escaping ((_ eventList:[VEvent], _ eventForRate:[VEvent])->()), failureBlock:@escaping ((_ message:String)->())) {
        let fbID = UserDefaultsManager().userFBID!
        WebService.request(method: .get, url: kViewEvents_Users + fbID, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let responseArray =  param["events"] as? [[String:AnyObject]], let eventRate =  param["eventsToRate"] as? [[String:AnyObject]] {
                    var eventList = [VEvent]()
                    var eventForRate  = [VEvent]()
                    for obj in responseArray {
                        let event = Mapper<VEvent>().map(JSON: obj)
                        if let e = event {
                            eventList.append(e)
                        }
                    }
                    for obj in eventRate {
                        let eventsRate = Mapper<VEvent>().map(JSON: obj)
                        if let e = eventsRate{
                            eventForRate.append(e)
                            
                        }
                    }
                    successBlock(eventList ,eventForRate)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func getEventList(_ successBlock:@escaping ((_ eventList:[VEvent]?)->()), failureBlock:@escaping ((_ message:String)->())) {
        //let fbID = User().userFBID!
        
        // if businesss account is activated then get list of business events
        let url = UserDefaultsManager().isBusinessAccountActivated ?  kListBusinessEvents + UserDefaultsManager().userBusinessAccountID! : kListEvents + UserDefaultsManager().userFBID!
        
        WebService.request(method: .get, url: url, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let responseArray =  param["events"] as? [[String:AnyObject]]{
                    var eventList = [VEvent]()
                    for obj in responseArray {
                        let event = Mapper<VEvent>().map(JSON: obj)
                        if let e = event {
                            eventList.append(e)
                        }
                    }
                    successBlock(eventList)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func getEventListWithPagination(_ pageNumber:Int, pageSize: Int, successBlock:@escaping ((_ eventList:[VEvent]?, _ totalCount:NSNumber) -> ()), failureBlock:@escaping (_ message:String) -> Void) {
        // if businesss account is activated then get list of business events
        var url = ""
        
        if UserDefaultsManager().isBusinessAccountActivated {
            url = kListBusinessEventsPaginated + UserDefaultsManager().userBusinessAccountID!
        } else {
            url = kListEventsWithPagination + UserDefaultsManager().userFBID!
        }
        url = url + "/\(pageNumber)/\(pageSize)"
        
        
        WebService.request(method: .get, url: url, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let responseArray =  param["events"] as? [[String:AnyObject]]{
                    var eventList = [VEvent]()
                    for obj in responseArray {
                        let event = Mapper<VEvent>().map(JSON: obj)
                        if let e = event {
                            eventList.append(e)
                        }
                    }
                    let totalCount = param["count"] as! NSNumber
                    successBlock(eventList, totalCount)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func getEventDetail(_ eventID: String, successBlock:@escaping ((_ eventDetail:VEvent)->()), failureBlock:@escaping ((_ message:String)->())) {
        
        WebService.request(method: .get, url: kAPIGetEventDetail + eventID, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let responseArray =  param["eventDetail"] as? [String:AnyObject]{
                    if let event = Mapper<VEvent>().map(JSON: responseArray){
                        successBlock(event)
                    }
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    
    static func setUserResponseForEventPayment(_ event : VEvent, userResponse : UserViteResponse, successBlock:@escaping (() -> Void), failureBlock:@escaping ((_ message:String) -> Void)){
        let eventUID = event.uid!
        let parameters:[String:AnyObject] = [
            "viteStatus":userResponse.rawValue as AnyObject,
            ]
        
        let fbID = UserDefaultsManager().userFBID!
        
        WebService.request(method: .post, url: kUserRequestForEventPayment + fbID + "/" + eventUID, parameter: parameters, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                successBlock()
            } else {
                failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    
    static func setUserResponseForEvent(_ event : VEvent, userResponse : UserViteResponse, successBlock:@escaping (() -> Void), failureBlock:@escaping ((_ message:String) -> Void)){
        let eventUID = event.uid!
        let parameters:[String:AnyObject] = [
            "viteStatus":userResponse.rawValue as AnyObject,
            ]
        
        let fbID = UserDefaultsManager().userFBID!
        
        WebService.request(method: .post, url: kUserRequestForEvent + fbID + "/" + eventUID, parameter: parameters, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                successBlock()
            } else {
                failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func getPeopleList(_ pageNo: Int, maxResult: Int, successBlock:@escaping ((_ peopleList:[PrivateFriend])->()), failureBlock:@escaping ((_ message:String)->())) {
        let url = kAPIListPeople + "/\(pageNo)" +  "/\(maxResult)"
        
        WebService.request(method: .get, url: url, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let responseArray =  param["users"] as? [[String:AnyObject]]{
                    var peopleLists = [PrivateFriend]()
                    peopleLists = responseArray.flatMap(PrivateFriend.init)
                    successBlock(peopleLists)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func inviteUsers(_ users:[PrivateFriend], withEventId eventId:String,
                            successBlock:@escaping (() -> Void),
                            failureBlock:@escaping ((_ message:String) -> Void)) {
        let userIds = users.map { (friend) -> String in
            return friend.facebookId!
        }
        let parameters = ["fbIdList": userIds]
        let url = kAPIInviteUsersToEvent + "\(eventId)"
        
        WebService.request(method: .post, url: url, parameter: parameters as [String : AnyObject], header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                successBlock()
            } else {
                failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func getEventListWithLocation(parameter : [String:AnyObject] , successBlock:@escaping ((_ eventList:[VEvent]?) -> ()), failureBlock:@escaping (_ message:String) -> Void) {
        WebService.request(method: .post, url: KAPIListEventByLocation, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as! [String: AnyObject]?,
                            let success = response["success"] as? Bool else {
                                return
                        }
            
            
                        if success {
                            let responeArray = response["params"]?["events"] as! [[String:AnyObject]]
                            var eventList = [VEvent]()
            
                            for obj in responeArray {
                                let event = Mapper<VEvent>().map(JSON: obj)
                                if let e = event {
                                    eventList.append(e)
            
                                }
                            }
                            successBlock(eventList)
                        }
        }) { (message, apiError) in
            failureBlock(message)
        }
    }
    
    static func setUserResponseForEventForPaypal(nonce: String, _ event : VEvent, userResponse : UserViteResponse, successBlock:@escaping (() -> Void), failureBlock:@escaping ((_ message:String) -> Void)){
        let eventUID = event.uid!
        let parameters:[String:AnyObject] = [
            "viteStatus":userResponse.rawValue as AnyObject,
            "paypalNonce": nonce as AnyObject
            ]
        
        let fbID = UserDefaultsManager().userFBID!
        WebService.request(method: .post, url: kUserRequestForEvent + fbID + "/" + eventUID, parameter: parameters, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                successBlock()
            } else {
                failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func setUserResponseForEventForPaypalWithPromo(nonce: String, _ event : VEvent, userResponse : UserViteResponse, successBlock:@escaping (() -> Void), failureBlock:@escaping ((_ message:String) -> Void)){
        let eventUID = event.uid!
        let parameters:[String:AnyObject] = [
            "viteStatus":userResponse.rawValue as AnyObject
        ]
        
        let fbID = UserDefaultsManager().userFBID!
        WebService.request(method: .post, url: kUserRequestForEventPayment + fbID + "/" + eventUID, parameter: parameters, header: ["nonce" : nonce], isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                successBlock()
            } else {
                failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
}
