//
//  VFeedbackWebService.swift
//  Vite
//
//  Created by eeposit2 on 8/24/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper
import Foundation

struct VFeedbackWebService {
    
    static func getFeedbackSubject(_ successBlock:@escaping ((_ subjectList:[VFeedback]) -> Void), failureBlock: @escaping ((_ message:String) -> Void)) {
        
        WebService.request(method: .get, url: kAPIGetFeedbackSubjectList, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let responseArray =  param["feedbackSubjects"] as? [[String:AnyObject]] {
                    var subjectList = [VFeedback]()
                    for obj in responseArray {
                        if let u = Mapper<VFeedback>().map(JSON: obj) {
                            subjectList.append(u)
                        }
                    }
                    successBlock(subjectList)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func submitFeedback(_ parameter:[String:AnyObject], successBlock:@escaping (_ message :String) -> Void, failureBlock:@escaping ((_ message:String) -> Void)) {
        guard let feedBackAPI = KAPISubmitFeedback else {
            return
        }
        WebService.request(method: .post, url: feedBackAPI, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if  let message    = response["message"] as? String {
                    successBlock(message)
                }
            } else {
                failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func reportEvent(_ eventId: String , parameter:[String:AnyObject], successBlock:@escaping (_ message :String) -> Void, failureBlock:@escaping ((_ message:String) -> Void)) {
        
        let reportEventUrl = kAPIReportEvent + UserDefaultsManager().userFBID! + "/\(eventId)"
        
        WebService.request(method: .post, url: reportEventUrl, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if  let message    = response["message"] as? String {
                    successBlock(message)
                }
            } else {
                failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
}
