//
//  VFeedbackWebService.swift
//  Vite
//
//  Created by eeposit2 on 8/24/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper
import Foundation

struct VFilterWebService {
    
    static func filterPeople(_ parameter:[String:AnyObject], successBlock:@escaping (_ peopleFilterList : [PrivateFriend]) -> Void, failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .post, url: KAPIGetFilteredPeople, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let responseArray =  param["userList"] as? [[String:AnyObject]]{
                    var filterPeopleList = [PrivateFriend]()
                    filterPeopleList = responseArray.flatMap(PrivateFriend.init)
                    successBlock(filterPeopleList)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func filterUser(_ parameter:[String:AnyObject], successBlock:@escaping (_ userFilterList : [PrivateFriend]) -> Void, failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .post, url: KAPIGetFilteredUser, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let responseArray =  param["userList"] as? [[String:AnyObject]]{
                    var filterUserList = [PrivateFriend]()
                    filterUserList = responseArray.flatMap(PrivateFriend.init)
                    successBlock(filterUserList)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
}
