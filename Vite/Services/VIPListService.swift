//
//  VIPListService.swift
//  Vite
//
//  Created by Hem Poudyal on 5/17/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//
import Foundation
import ObjectMapper
struct VIPListService:VIPListDataManagerProtocol, NonVIPDataManagerProtocol {
    
    
    
    func getUserVIPList(_ successBlock:@escaping (([MyVIPFriend]) ->()) , failureBlock:@escaping ((_ message:String) -> ())) {
        // if businesss account is activated then get list of business vips
        let listVipUrl = UserDefaultsManager().isBusinessAccountActivated ?  kListBusinessVips + UserDefaultsManager().userBusinessAccountID! : kAPIGetVIPList + UserDefaultsManager().userFBID!
        WebService.request(method: .get, url: listVipUrl, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            print(response)
            guard
                let response   = response.result.value as? [String: AnyObject],
                let success    = response["success"] as? Bool,
                let params     = response["params"] as? [String:AnyObject],
                let friendList = params["friends"] as? [[String:AnyObject]], success == true else {
                    failureBlock(localizedString(forKey: "INVALID_RESPONSE"))
                    return
            }
            let friendsArray = Mapper<MyVIPFriend>().mapArray(JSONArray: friendList)
            successBlock(friendsArray)
        }) { (message, apiError) in
            failureBlock(message)
        }
    }
    
    func getUserNonVIPList(_ successBlock:@escaping (([MyVIPFriend],_ friendListInPrivateFriendModel:[PrivateFriend]) ->()) , failureBlock:@escaping ((_ message:String) -> ())) {
        
        let friendListUrl = UserDefaultsManager().isBusinessAccountActivated ? kAPIGetUserFriendListForBusiness + "/\(UserDefaultsManager().userFBID!)" + "/\(UserDefaultsManager().userBusinessAccountID!)" : kAPIGetFriendList+(UserDefaultsManager().userFBID!)
        WebService.request(method: .get, url: friendListUrl, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard
                let response   = response.result.value as? [String: AnyObject],
                let success    = response["success"] as? Bool,
                let params     = response["params"] as? [String:AnyObject],
                let friendList = params["friends"] as? [[String:AnyObject]], success == true else {
                    failureBlock(localizedString(forKey: "INVALID_RESPONSE"))
                    return
            }
            let friendsArray = Mapper<MyVIPFriend>().mapArray(JSONArray: friendList)
            let privateFriend = friendList.flatMap(PrivateFriend.init)
            successBlock(friendsArray, privateFriend)
        }) { (message, apiError) in
            failureBlock(message)
        }
    }
    
    func deleteUserFromVIPList(_ parameters:[String:AnyObject], successBlock:@escaping (() ->()) , failureBlock:@escaping ((_ message:String) -> ())) {
        //if businesss account is activated then remove business vips
        let deleteVipUrl = UserDefaultsManager().isBusinessAccountActivated ?  kAPIRemoveBusinessVIPList + UserDefaultsManager().userBusinessAccountID! : kAPIRemoveVIPList + UserDefaultsManager().userFBID!
        WebService.request(method: .post, url: deleteVipUrl, parameter: parameters, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard
                let response   = response.result.value as? [String: AnyObject],
                let success    = response["success"] as? Bool else {
                    return
            }
            if success {
                successBlock()
            } else {
                failureBlock(localizedString(forKey: "FAILED_TO_DELETE"))
            }
        }) { (message, apiError) in
            failureBlock(localizedString(forKey: "FAILED_TO_DELETE"))
        }
    }
}
