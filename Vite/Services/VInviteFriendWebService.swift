//
//  VInviteFriendWebService.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 2/27/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper

class VinviteFriendWebService {
    
    static func getUserFriendsToInviteOnEvent(_ eventId: String, successBlock:@escaping (([MyVIPFriend]) ->()) , failureBlock:@escaping ((_ message:String) -> ())) {
        
        let InvitefriendListUrl = UserDefaultsManager().isBusinessAccountActivated ? kAPIBusinessGetFriendListToInvite + "/\(UserDefaultsManager().userFBID!)" + "/\(UserDefaultsManager().userBusinessAccountID!)" + "/\(eventId)" : kAPIGetFriendListToInvite + UserDefaultsManager().userFBID! + "/\(eventId)"
        WebService.request(method: .get, url: InvitefriendListUrl, success: {
            (response) in
            print(response)
            guard let response = response.result.value as? [String: AnyObject],
                let success = response["success"] as? Bool,
                let param = response["params"] as? [String:AnyObject]
                else {
                    failureBlock(localizedString(forKey: "INVALID_RESPONSE"))
                    return
            }
            if success {
                if let friendArr = param["friends"] as? [[String:AnyObject]] {
                    var friendList = [MyVIPFriend]()
                    for obj in friendArr {
                        if let u = Mapper<MyVIPFriend>().map(JSON: obj) {
                            friendList.append(u)
                        }
                    }
                    successBlock(friendList)
                }
                
            }
        }, failure: { (message, error) in
            failureBlock(message)
        })
    }
    
    static func searchViteUsersToInvite(_ eventId: String, page: Int , searchString:String, successBlock:@escaping (([ViteUser]) ->()), failureBlock:@escaping ((_ message:String) -> ())) {
        WebService.request(method: .get, url: kAPISearchFriendToInvite + "\(eventId)" + "/\(searchString)/\(page)/10/" , success: {
            (response) in
            print(response)
            guard let response = response.result.value as? [String: AnyObject],
                let success = response["success"] as? Bool,
                let param = response["params"] as? [String:AnyObject]
                else {
                    failureBlock(localizedString(forKey: "INVALID_RESPONSE"))
                    return
            }
            if success {
                if let viteUserArr = param["users"] as? [[String:AnyObject]] {
                    var viteUsers = [ViteUser]()
                    for obj in viteUserArr {
                        if let u = Mapper<ViteUser>().map(JSON: obj) {
                            viteUsers.append(u)
                        }
                    }
                    successBlock(viteUsers)
                }
                
            }
        }, failure: { (message, error) in
            failureBlock(message)
        })
    }
    
    
    
    
    static func inviteVIPsCategory(_ eventId: String,categoryId: String,  successBlock:@escaping ((_ message:String) ->()) , failureBlock:@escaping ((_ message:String) -> ())) {
        let InviteVIPCategoryUrl = KAPIInviteVIPCategory + "\(eventId)" + "/\(categoryId)"
        WebService.request(method: .post, url: InviteVIPCategoryUrl, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let msg = response["message"] as? String  {
                    successBlock(msg)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func inviteAll(_ eventId: String, successBlock:@escaping ((_ success: Bool) ->()), failureBlock:@escaping ((_ msg: String) -> ())){
        
        WebService.request(method: .post, url: kAPIInviteAll + eventId, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                successBlock(true)
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
}
