//
//  VMediaWebService.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 4/25/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire


struct VMediaWebService {
    static func uploadMedia(_ mediaData: Data, videoThumbnailData: Data, mediaName: String,  mediaType : String, mediaWidth: Int, mediaHeight:Int, mediaFormat: String,progressBlock:@escaping (_ progressResult : UploadRequest) -> Void, successBlock:@escaping (_ media : VMedia) -> Void, failureBlock: @escaping ((_ message:String) -> Void) ) {
        //  default url for uploadVideo = /upload_file/{mediaType}/{width}/{height}
        let url = KAPIUploadVideo + "\(mediaType)/" + "\(mediaWidth)/" + "\(mediaHeight)"
        WebService.uploadMedia(mediaData, videoThumbnailData: videoThumbnailData,mediaName: mediaName, mediaFormat: mediaFormat, apiURL: url, successBlock: { (response) in
    
            guard let success = response!["success"] as? Bool else {
                return
            }
            if success{
                let responeArray = response!["data"] as! [String:AnyObject]
                if let mediaData = Mapper<VMedia>().map(JSON: responeArray){
                    successBlock(mediaData)
                }
            }
        }, progressBlock: { (progressData) in
                progressBlock(progressData)
        }) { (message) in
            failureBlock(message)
        }
        
    }
    
    static func deleteMedia(_ mediaId: String,  successBlock:@escaping (_ msg : String) -> Void, failureBlock: @escaping ((_ msg: String) -> Void)){
        let url = kAPIDeleteMedia + "\(mediaId)"
        
        
        WebService.request(method: .delete, url: url, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool, let msg = response["message"] as? String else {
                return
            }
            if success {
                successBlock(msg)
            } else {
                failureBlock(localizedString(forKey: "FILE_DELETED"))
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    
    static func deleteMultipleMedia(_ parameter: [String : AnyObject], successBlock:@escaping (_ msg : String) -> Void, failureBlock: @escaping ((_ msg: String) -> Void)){
        let url = kAPIDeleteMultipleMedia
        
        WebService.request(method: .delete, url: url, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool , let msg = response["message"] as? String else {
                return
            }
            if success {
                 successBlock(msg)
            } else {
                 successBlock(localizedString(forKey: "FILE_DELETED"))
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }

    
}
