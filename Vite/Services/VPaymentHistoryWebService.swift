//
//  VPaymentHistoryWebService.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 7/25/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper

class VPaymentHistoryWebService: NSObject {
    
    func getEventsRevenueList(_ pageNumber:Int, pageSize: Int, successBlock:@escaping ((_ payment:VPayment?) -> ()), failureBlock:@escaping ((_ message:String)->())) {
        // if businesss account is activated then get list of business events payment revenues
        var url = ""
        if UserDefaultsManager().isBusinessAccountActivated {
            url = kAPIBusinessRevenueListPaginated + UserDefaultsManager().userBusinessAccountID! + "/"
        } else {
            url = kAPIRevenueListPaginated
        }
        url = url + "\(pageNumber)/\(pageSize)"
        print(url)
        WebService.request(method: .get, url: url, success: {
            (response) in
            print(response)
            guard let response = response.result.value as? [String: AnyObject],
                let success = response["success"] as? Bool,
                let param = response["params"] as? [String:AnyObject], success == true
                else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                    return
            }
            if let u = Mapper<VPayment>().map(JSON: param) {
                successBlock(u)
            }
        }, failure: { (message, error) in
                failureBlock(message)
        })
    }
    
    func getAllAttendeeListWithRevenue(_ eventId:String, pageNo: Int, pageSize: Int, successBlock:@escaping (_ users:[MyEventAttendee], _ totalCount: Int) -> Void, failureBlock:@escaping (_ message:String) -> Void) {
        
        WebService.request(method: .get, url: KAPIEventRevenue + eventId + "/\(pageNo)/\(pageSize)" , success: { (response) in
            guard let response = response.result.value as? [String: AnyObject],
                let success = response["success"] as? Bool,
                let param = response["params"] as? [String:AnyObject], success == true
                else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                    return
            }
            
            if let responseArray =  param["users"] as? [[String:AnyObject]],let count = param["count"] as? Int {
                var attendeeRevenueList = [MyEventAttendee]()
                for user in responseArray {
                    if let u = Mapper<MyEventAttendee>().map(JSON: user) {
                        attendeeRevenueList.append(u)
                    }
                }
                
                successBlock(attendeeRevenueList,count)
            } else {
                failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
            }
            
        }, failure: {
            (message, error) in
            failureBlock(message)
        })
    }
}
