//
//  VPostMediaViteFeedService.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 3/28/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

class VPostMediaViteFeedService {
    static func getNearVite(successBlock:@escaping ((_ eventId: String, _ eventTitle: String) ->()) , failureBlock:@escaping ((_ message:String) -> ())) {
        WebService.request(method: .get, url: KAPIGetNearEvent, success: {
            (response) in
            print(response)
            guard let response = response.result.value as? [String: AnyObject],
                let success = response["success"] as? Bool,
                let param = response["params"] as? [String:AnyObject]
                else {
                    failureBlock(localizedString(forKey: "INVALID_RESPONSE"))
                    return
            }
            if success {
                if let eventId = param["eventId"] as? String, let eventTitle = param ["eventTitle"] as? String {
                    successBlock(eventId, eventTitle)
                } else {
                    successBlock("", "")
                }
            }
        }, failure: { (message, error) in
            failureBlock(message)
        })
    }
    
    static func uploadMedia(_ mediaData: Data, videoThumbnailData: Data, mediaName: String,  mediaType : String, mediaWidth: Int, mediaHeight:Int, mediaFormat: String,progressBlock:@escaping (_ progressResult : UploadRequest) -> Void, successBlock:@escaping (_ media : VMedia) -> Void, failureBlock: @escaping ((_ message:String) -> Void) ) {
        //  default url for uploadVideo = /upload_file/{mediaType}/{width}/{height}
        let url = KAPIUploadVideo + "\(mediaType)/" + "\(mediaWidth)/" + "\(mediaHeight)"
     WebService.uploadMedia(mediaData, videoThumbnailData: videoThumbnailData,mediaName: mediaName, mediaFormat: mediaFormat, apiURL: url, successBlock: { (response) in
            guard let success = response!["success"] as? Bool else {
                return
            }
            if success{
                let responeArray = response!["data"] as! [String:AnyObject]
                if let mediaData = Mapper<VMedia>().map(JSON: responeArray){
                    successBlock(mediaData)
                }
            }
        }, progressBlock: { (progressData) in
            progressBlock(progressData)
        }) { (message) in
            failureBlock(message)
        }
    }
    
    static func postMediaToViteFeed(_ parameter:[String:AnyObject], successBlock:@escaping (_ message : String) -> Void, failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .post, url: KAPIPostMediaToFeed, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            guard let msg = response["message"] as? String else {
                return
            }
            if success {
              successBlock(msg)
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func getNearViteList(successBlock:@escaping ((_ nearViteLists: [VVite]) ->()) , failureBlock:@escaping ((_ message:String) -> ())) {
        WebService.request(method: .get, url: KAPIGetNearViteList + "\(1)/\(10)", success: {
            (response) in
            print(response)
            guard let response = response.result.value as? [String: AnyObject],
                let success = response["success"] as? Bool,
                let param = response["params"] as? [String:AnyObject]
                else {
                    failureBlock(localizedString(forKey: "INVALID_RESPONSE"))
                    return
            }
            if success{
                if let responeArray = param["events"] as? [[String:AnyObject]]{
                    var viteLists = [VVite]()
                    for vite in responeArray {
                        if  let v = Mapper<VVite>().map(JSON: vite){
                            viteLists.append(v)
                        }
                    }
                    successBlock(viteLists)
                }
                
            }
        }, failure: { (message, error) in
            failureBlock(message)
        })
    }
    
    
}
