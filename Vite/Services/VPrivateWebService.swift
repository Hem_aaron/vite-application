//
//  VSocialNetworkWebService.swift
//  Vite
//
//  Created by eeposit2 on 1/16/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper

class VPrivateWebService{
    
    static func getPrivateViteUsers(_ eventId: String, _ successBlock:@escaping ((_ userList: [PrivateFriend]?) -> Void), failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .get, url: kAPIGetPrivateEventUsers + eventId, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let params = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let responseArray =  params["userList"] as? [[String:AnyObject]] {
                    var userList = [PrivateFriend]()
                    userList = responseArray.flatMap(PrivateFriend.init)
                    successBlock(userList)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
            
        }) { (msg, err) in
              failureBlock(msg)
        }
    }
    
}
