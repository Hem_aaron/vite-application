//
//  VProfileWebService.swift
//  Vite
//
//  Created by sajjan giri on 11/30/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper

class VProfileWebService {
    
    static func getMutualFriends(friendFbId : String , successBlock: @escaping (_ users:[VUser]) -> Void, failureBlock: @escaping (_ message:String) -> Void) {
        WebService.request(method: .get, url: KAPIGetMutualFriends + UserDefaultsManager().userFBID! + "/\(friendFbId)", success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let responseArray =  param["mutualFriends"] as? [[String:AnyObject]] {
                    var mutualFriendList = [VUser]()
                    for user in responseArray {
                        if let u = Mapper<VUser>().map(JSON: user) {
                            mutualFriendList.append(u)
                        }
                    }
                    successBlock(mutualFriendList)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
             failureBlock(msg)
        }
    }
    
    static func getUserProfile(url: String, successBlock:@escaping (_ users: UserInfo) -> Void, failureBlock:@escaping (_ message:String) -> Void) {
        WebService.request(method: .get, url: url, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let params = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if  let userDetail = params["users"] as? [String:AnyObject] {
                    if let userData = UserInfo(json: userDetail){
                        successBlock(userData)
                    }
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func getBlockedUserList(successBlock:@escaping (_ blockedUsers:[VUser]) -> Void, failureBlock:@escaping (_ message:String) -> Void) {
        WebService.request(method: .get, url: KAPIBlockedUserList + UserDefaultsManager().userFBID!, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let responseArray =  param["users"] as? [[String:AnyObject]] {
                    var blockedUserList = [VUser]()
                    for user in responseArray {
                        if let u = Mapper<VUser>().map(JSON: user) {
                            blockedUserList.append(u)
                        }
                    }
                    successBlock(blockedUserList)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func getUserInterestList(successBlock:@escaping (_ interestList: [String]) -> Void, failureBlock:@escaping (_ message:String) -> Void) {
        
        WebService.request(method: .get, url: KAPIListFilterTags, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let tagList =  param["filters"] as? [[String:AnyObject]] {
                   var intrests = [String]()
                    for tag in tagList {
                        if let tagName = tag["filterName"] as? String {
                            intrests.append(tagName)
                        }
                    }
                     successBlock(intrests)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    
    static func onDarkMode(successBlock:@escaping (_ successMsg: String) -> Void , failureBlock:@escaping (_ message: String) -> Void) {
        WebService.request(method: .put, url: KAPIDarkModeOn, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                let succesMsg = "Dark Mode Activated"
                successBlock(succesMsg)
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    
    static func onHideAge(successBlock: @escaping (_ successMsg: String) -> Void , failureBlock:@escaping (_ message: String) -> Void){
        
        WebService.request(method: .put, url: KAPIHideAge, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                let succesMsg = "Hide Age Activate"
                successBlock(succesMsg)
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func upgradeToPremium(receiptData: String, successBlock:@escaping (_ successMsg: String) -> Void , failureBlock:@escaping (_ message: String) -> Void){
        let param :[String: AnyObject] = [
            "receiptData": receiptData as AnyObject
        ]
        
        WebService.request(method: .put, url: kAPIUpgradePremium, parameter: param, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                let succesMsg = "Congratulations, Your account is upgraded to premium."
                successBlock(succesMsg)
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    
    
    
    
    
    
    static func checkAutoRenewStatus(successBlock:@escaping (_ success: Bool) -> Void , failureBlock:@escaping (_ message: String) -> Void){
        
        
        WebService.request(method: .get, url: kAPICheckAutoRenewStatus, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if  let autoRenewStatus = param["auto_renew_status"] as? Bool{
                    successBlock(autoRenewStatus)
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    
//    static func checkFriendDarkMode(parameter: [String:AnyObject],successBlock : (filterDarkModeList : [Int]) -> Void , failureBlock:(message:String) -> Void) {
//
//        WebService.postAtWithBody(KAPICheckDarkMode, parameters: parameter, successBlock: { (response) in
//            guard let success = response!["success"] as? Bool , let param = response!["params"] as? [String:AnyObject] else {
//                failureBlock(message: "Response not in proper format")
//                return
//            }
//            if success {
//                let fbID = param["ids"] as? [Int]
//                var idList = [Int]()
//                for id in fbID! {
//                    idList.append(id)
//                }
//                successBlock(filterDarkModeList: idList)
//            }
//
//        }) { (message) in
//            failureBlock(message: message)
//        }
//
//    }
    
    
}
