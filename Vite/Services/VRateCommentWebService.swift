//
//  VRateCommentWebService.swift
//  Vite
//
//  Created by Bibhut Bikram Thakuri on 4/6/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation
import ObjectMapper

class VRateCommentWebService{
    
    static func getRateAndCommentList(fbId: String, _ pageNumber:Int, pageSize: Int, successBlock:@escaping ((_ eventList:[VRateComment]?, _ totalCount:NSNumber) -> ()), failureBlock:@escaping (_ message:String) -> Void) {
        
        let url = kGetRateAndCommentList + "/\(fbId)/\(pageNumber)/\(pageSize)"
        WebService.request(method: .get, url: url, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let responseArray =  param["comments"] as? [[String:AnyObject]]{
                    var commentList = [VRateComment]()
                    for obj in responseArray {
                        let comment = Mapper<VRateComment>().map(JSON: obj)
                        if let c = comment {
                            commentList.append(c)
                        }
                    }
                    let totalCount = param["count"] as! NSNumber
                    successBlock(commentList, totalCount)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func getRateAndCommentListForBusinessAccount(businessAccountId: String, _ pageNumber:Int, pageSize: Int, successBlock:@escaping ((_ eventList:[VRateComment]?, _ totalCount:NSNumber) -> ()), failureBlock:@escaping (_ message:String) -> Void) {
        let url = kGetRateAndCommentListForBusinessAccount + "/\(businessAccountId)/\(pageNumber)/\(pageSize)"
        WebService.request(method: .get, url: url, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let param = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            print(response)
            if success {
                if let responseArray =  param["comments"] as? [[String:AnyObject]]{
                    var commentList = [VRateComment]()
                    for obj in responseArray {
                        let comment = Mapper<VRateComment>().map(JSON: obj)
                        if let c = comment {
                            commentList.append(c)
                        }
                    }
                    let totalCount = param["count"] as! NSNumber
                    successBlock(commentList, totalCount)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
}
 //
