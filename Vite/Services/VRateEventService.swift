//
//  VRateEventService.swift
//  Vite
//
//  Created by Eeposit 01 on 6/5/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit

class  VRateEventService {
  
  static func rateEvent(_ eventId: String,parameter: [String:AnyObject],successBlock:@escaping (_ successMsg: String) -> Void, failureBlock:@escaping ((_ message:String) -> Void)){
    
    WebService.request(method: .post, url: KAPIRateEvent + eventId, parameter: parameter, header: ["accessToken" : UserDefaultsManager().userAccessToken!], isAccessTokenRequired: true, success: {
    (response) in
    guard let response = response.result.value as? [String:AnyObject] else {
        return
    }
    guard let success = response["success"] as? Bool else {
        return
    }
    guard  let message = response["message"] as? String else {
        return
    }
    if success {
        successBlock(message)
    }
    
   }, failure: { (message, APIError) in
        failureBlock(message)
   })
    
  }
}
