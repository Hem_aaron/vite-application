  //
//  VServices.swift
//  Vite
//
//  Created by Hem Poudyal on 1/30/18.
//  Copyright © 2018 Eepos IT. All rights reserved.
//

import Foundation
import ObjectMapper

struct VServices {
    static func updateDeviceToken(_ successBlock:(() -> Void)?, failureBlock: ((_ message:String) -> Void)? ) {
        guard let userDeviceToken = UserDefaultsManager().userDeviceToken, let fbId = UserDefaultsManager().userFBID  else{
            if let block = failureBlock {
                block("Couldn't get device token")
            }
            return
        }
        if userDeviceToken.isEmpty {
            return
        }
        
        let parameters:[String:AnyObject] = [
            "deviceToken" : userDeviceToken as AnyObject
        ]
        print("deviceToken : \(parameters)")
        WebService.request(method: .post, url: kAPIUpdateDeviceToken + fbId, parameter: parameters, header: nil, isAccessTokenRequired: true, success: { (response) in
            print(response)
            if let block = successBlock {
                block()
            }
        }) { (message, error) in
            if let block = failureBlock {
                block(message)
            }
        }
        
    }
    
    static func retrieveAllUserInPeriphery(_ eventId:String, successBlock:@escaping (_ users:[VUser]) -> Void, failureBlock:@escaping (_ message:String) -> Void) {
        //TestEvent = 6dfccd74-e284-428f-83d2-dbd55e92aafc
        WebService.request(method: .get, url: kAPIGetPrivateEventUsers + eventId, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String: AnyObject],
                            let success = response["success"] as? Bool,
                            let param = response["params"] as? [String:AnyObject] , success == true
                            else {
                                failureBlock(localizedString(forKey: localizedString(forKey: "RESPONSE_FORMAT_ERROR")))
                                return
                        }
            if let responseArray =  param["userList"] as? [[String:AnyObject]] {
                                var peripheryUsers = [VUser]()
                                for user in responseArray {
                                    if let u = Mapper<VUser>().map(JSON: user) {
                                        peripheryUsers.append(u)
                                    }
                                }
                                successBlock(peripheryUsers)
                            } else {
                                failureBlock(localizedString(forKey: localizedString(forKey: "RESPONSE_FORMAT_ERROR")))
                            }
        }) { (message, Error) in
            failureBlock(message)
        }
    }
}


