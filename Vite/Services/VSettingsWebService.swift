//
//  VSocialNetworkWebService.swift
//  Vite
//
//  Created by eeposit2 on 1/16/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper

class VSettingsWebService{
    
    static func logOut(_ successBlock:@escaping ((_ successMsg: String) -> Void), failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .post, url: kAPILogOut + UserDefaultsManager().userFBID!, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool, let msg = response["message"] as? String else {
                return
            }
            if success {
               successBlock(msg)
            }
            
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func updatePushNotification(param: [String: AnyObject],_ successBlock:@escaping ((_ msg : String) -> Void), failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .post, url: kAPIUpdatePushNotification!, parameter: param, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
               successBlock(localizedString(forKey: "UPDATED_PUSH_NOTIFICATION"))
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func getPushNotification( _ successBlock:@escaping ((_ pushNotificationSetting: VPushNotification) -> Void), failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .get, url: KAPIGetPushNotification!, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            guard let param = response["params"] as?  [String:AnyObject] else {
                return
            }
            if success {
                if  let setting = param["pushNotificationSetting"] as? [String:AnyObject] {
                    if let detail = Mapper<VPushNotification>().map(JSON: setting) {
                     successBlock(detail)
                    }
                }
               
            }
            
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func setEventDistance(param: [String: AnyObject],_ successBlock:@escaping ((_ msg : String) -> Void), failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .post, url: kAPISetEventDistance, parameter: param, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                successBlock(localizedString(forKey: "DISTANCE_SET"))
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func getEventVisibleDistance( _ successBlock:@escaping ((_ pushNotificationSetting: VPushNotification) -> Void), failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .get, url: kAPIGetEventDistance + UserDefaultsManager().userFBID!, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            print(response)
            guard let success = response["success"] as? Bool else {
                return
            }
            guard let param = response["params"] as?  [String:AnyObject] else {
                return
            }
            if success {
                if  let setting = param["user"] as? [String:AnyObject] {
                    if let detail = Mapper<VPushNotification>().map(JSON: setting) {
                        successBlock(detail)
                    }
                }
            }
            
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func unblockUser(param: [String: AnyObject],_ successBlock:@escaping ((_ msg : String) -> Void), failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .post, url: KAPIUnBlockeUser + UserDefaultsManager().userFBID!, parameter: param, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                successBlock(localizedString(forKey: "USER_UNBLOCKED"))
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
}


