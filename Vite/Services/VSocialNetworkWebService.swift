//
//  VSocialNetworkWebService.swift
//  Vite
//
//  Created by eeposit2 on 1/16/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper

class VSocialNetworkWebService{
    
    static func getAllNetworkType(_ successBlock:@escaping ((_ networkList: [Network]?) -> Void),
                                  failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .get, url: KAPIGetNetworkTypes, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let params = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                
                
                if let responseArray =  params["networkTypes"] as? [[String:AnyObject]] {
                    var networkList = [Network]()
                    for net in responseArray {
                        if let u = Mapper<Network>().map(JSON: net) {
                            networkList.append(u)
                        }
                    }
                    successBlock(networkList)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
            
        }) { (msg, err) in
              failureBlock(msg)
        }
        
    }
    
    //MARK: CONNECT WITH SOCIAL - SEND FRIENDS TO SERVER
    static func connectWithSocial(_ parameter: [String: AnyObject],successBlock:@escaping ((_ message: String?) -> Void),
                                 failureBlock:@escaping ((_ message:String?) -> Void)){
        
        
        WebService.request(method: .post, url: KAPIConnectSocialNetwork, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let msg = response["message"] as? String else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                successBlock(msg)
            }
        }) { (msg, err) in
             failureBlock(msg)
        }
    }

    
    //MARK: DISCONNECT WITH SOCIAL - SEND FRIENDS TO SERVER
    static func disconnectWithSocial(_ parameter: [String: AnyObject],successBlock:@escaping ((_ message: String?) -> Void),
                                    failureBlock:@escaping ((_ message: String?) -> Void)){
        
        /*let parameter :[String: AnyObject] = [
         "networkId":networkID,
         "networkType":networkName,
         ]*/
        
        WebService.request(method: .post, url: KAPIDisconnectSocialNetwork, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let msg = response["message"] as? String else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                successBlock(msg)
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    
    static func getUserNetworkStatus(_ fbId: String ,successBlock:@escaping ((_ networkStatusList: [NetworkStatus]?) -> Void),
                                    failureBlock:@escaping ((_ message:String) -> Void)){
        WebService.request(method: .get, url: KAPIGetUserNetworkStatus + fbId, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            print(response)
            guard let params = response["params"] as? [String:AnyObject] else {
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                if let responseArray =  params["userNetworks"] as? [[String:AnyObject]] {
                    var networkList = [NetworkStatus]()
                    for net in responseArray {
                        if let u = Mapper<NetworkStatus>().map(JSON: net) {
                            networkList.append(u)
                        }
                    }
                    successBlock(networkList)
                } else {
                    failureBlock(localizedString(forKey: "RESPONSE_FORMAT_ERROR"))
                }
            }
            
        }) { (msg, err) in
            failureBlock(msg)
        }
    }

    
}
