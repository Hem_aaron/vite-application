//
//  VSocialNetworkWebService.swift
//  Vite
//
//  Created by eeposit2 on 1/16/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import Foundation
import ObjectMapper

class VStripeWebService{
    
    static func connectWithStripe(_ splitParams: String, _ successBlock:@escaping ((_ successStatus: Bool) -> Void), failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .get, url: kAPIConnectWithStripe + UserDefaultsManager().userFBID! + "/" + splitParams, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
               successBlock(success)
            }
            
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func getEventFeeWithPromoCode(_ trimmedPromo: String,_ eventId: String ,_ successBlock:@escaping ((_ promoInfo: PaymentStripe) -> Void), failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .get, url: kAPIGetEventFeeWithPromoCode + eventId + "/" + trimmedPromo, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            guard let param = response["params"] as?  [String:AnyObject] else {
                return
            }
            if success {
                if  let eventPromoInfo = param["eventFeeInfo"] as? [String:AnyObject] {
                    if  let promo = Mapper<PaymentStripe>().map(JSON: eventPromoInfo){
                        successBlock(promo)
                    }
                }
            }
            
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func getEventFee(_ eventId: String, _ successBlock:@escaping ((_ promoInfo: PaymentStripe) -> Void), failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .get, url: kAPIGetEventFee + eventId, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            guard let param = response["params"] as?  [String:AnyObject] else {
                return
            }
            if success {
                if  let eventPromoInfo = param["eventFeeInfo"] as? [String:AnyObject] {
                    if let promo = Mapper<PaymentStripe>().map(JSON: eventPromoInfo) {
                     successBlock(promo)
                    }
                }
            }
            
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func checkStripeStatus(_ successBlock:@escaping ((_ cardState: Bool) -> Void), failureBlock:@escaping ((_ message:String) -> Void)) {
        
        WebService.request(method: .get, url: KAPICheckStripeStatus + UserDefaultsManager().userFBID!, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            guard let param = response["params"] as?  [String:AnyObject] else {
                return
            }
            if success {
                if  let cardStatus = param["cardStatus"] as? Bool {
                   successBlock(cardStatus)
                }
                
            }
            
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func checkStripeStatusForAccount(fbId: String, _ successBlock:@escaping ((_ accountState: Bool) -> Void), failureBlock:@escaping ((_ message:String) -> Void)) {
      
        WebService.request(method: .get, url: KAPICheckStripeStatus + fbId, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            guard let param = response["params"] as?  [String:AnyObject] else {
                return
            }
          
            if success {
                if  let accountStatus = param["accountStatus"] as? Bool {
                    successBlock(accountStatus)
                }
                
            }
            
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func getStripeInfo(_ successBlock:@escaping ((_ cardInfo: Card ) -> Void), failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .get, url: kAPIGetStripeInfo + UserDefaultsManager().userFBID!, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            guard let param = response["params"] as?  [String:AnyObject] else {
                return
            }
            if success {
                if  let cardInfo = param["cardInfo"] as? [String:AnyObject] {
                    if let card = Mapper<Card>().map(JSON: cardInfo){
                        successBlock(card)
                    }
                }
            }
            
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func getPaymentInfo(_ successBlock:@escaping ((_ fee: NSNumber ) -> Void), failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .get, url: KAPIFeatureEventFee , success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            guard let param = response["params"] as?  [String:AnyObject] else {
                return
            }
            if success {
                if  let cardInfo = param["fee"] as? NSNumber{
                   successBlock(cardInfo)
                }
            }
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
    
    static func updateStripeToken(token: String, _ successBlock:@escaping ((_ success: Bool) -> Void), failureBlock:@escaping ((_ message:String) -> Void)) {
        WebService.request(method: .get, url:  kAPIUpdateStripeToken + UserDefaultsManager().userFBID! + "/token/" + token, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success {
                successBlock(success)
            }
            
        }) { (msg, err) in
            failureBlock(msg)
        }
    }
}


