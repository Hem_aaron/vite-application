//
//  VipGroupService.swift
//  Vite
//
//  Created by Eeposit 01 on 4/4/17.
//  Copyright © 2017 EeposIT_X. All rights reserved.
//

import UIKit
import ObjectMapper

class VipGroupService {
    
    let indicator     = ActivityIndicator()
    
    static func createGroupCategory(_ parameter:[String:AnyObject], successBlock:@escaping (_ category :VipGroupCategory) -> Void, failureBlock:@escaping ((_ message:String) -> Void)) {
        let indicator     = ActivityIndicator()
        indicator.start()
        let createGroupCategoryUrl = UserDefaultsManager().isBusinessAccountActivated ? KAPICreateBusinessVipGroup + "\(UserDefaultsManager().userBusinessAccountID!)" : KAPICreateVipGroup
        WebService.request(method: .post, url: createGroupCategoryUrl, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String: AnyObject],
                            let status = response["success"] as? Bool else {
                                return
            
                        }
                        if status {
            
                            if let responseData = response["data"] as? [String:AnyObject] {
                                if let vipCategoryName = Mapper<VipGroupCategory>().map(JSON: responseData){
            
                                    successBlock(vipCategoryName)
                                    indicator.stop()
                                }
                            }
                        }
        }) { (message, apiError) in
            failureBlock(message)
             indicator.stop()
        }
        
    }
    
    
    static func listVipCategory(_ successBlock:@escaping (_ listVipCategory :[VipGroupCategory]) -> Void, failureBlock:@escaping ((_ message:String) -> Void)){
        
        let indicator     = ActivityIndicator()
        indicator.start()
        let listVipCategoryUrl = UserDefaultsManager().isBusinessAccountActivated ? KAPIListBusinessVipCategory + "\(UserDefaultsManager().userBusinessAccountID!)" : KAPIListVipCategory
        WebService.request(method: .get, url: listVipCategoryUrl, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String: AnyObject],
                            let status = response["success"] as? Bool else {
                                return
            
                        }
                        if status {
                            var vipCategoryList = [VipGroupCategory]()
                            if let responseData = response["list"] as? [[String:AnyObject]]{
            
                                for categoryList in responseData {
                                    if let listData = Mapper<VipGroupCategory>().map(JSON: categoryList){
            
                                        vipCategoryList.append(listData)
                                    }
                                }
                            }
                            successBlock(vipCategoryList)
                            indicator.stop()
                        }
        }) { (message, apiError) in
            failureBlock(message)
            indicator.stop()
        }
 }
    
    static func deleteVipCategory(_ categoryId : String,successBlock:@escaping (_ successMsg :String) -> Void, failureBlock:@escaping ((_ message:String) -> Void)){
        
        let deleteVipCategoryUrl = UserDefaultsManager().isBusinessAccountActivated ? KAPIDeleteBusinessVipCategory  : KAPIDeleteVipCategory
//        let userAcessToken =  [
//            "accessToken" : UserDefaultsManager().userAccessToken!
//        ]
        WebService.request(method: .delete, url: deleteVipCategoryUrl + categoryId, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject],
                            let success =   response["success"] as? Bool else {return}
            
                        if success {
                            if let data = response["message"] as? String {
                                successBlock(data)
                            }
                        }
        }) { (message, apiError) in
            failureBlock(message)
        }
    }
    
    static func addVipToGroupCategory(_ categoryId : String,parameter:[String:AnyObject], successBlock:@escaping (_ successMsg : String) -> Void, failureBlock:@escaping ((_ message:String) -> Void)) {
        
        let addVipToCategoryUrl = UserDefaultsManager().isBusinessAccountActivated ? KAPIAddBusinessVipToCategory  : KAPIAddVipToGroup
        
        WebService.request(method: .post, url: addVipToCategoryUrl + categoryId, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject],
                            let success =   response["success"] as? Bool else {return}
            
                        if success {
            
                            if let data  = response["message"] as? String {
                                successBlock(data)
                            }
                        }
        }) { (message, apiError) in
            failureBlock(message)
        }
    }
    static func listVipByCategory(_ categoryId : String, successBlock:@escaping (_ vipListByCategory : VipGroupCategory) -> Void, failureBlock:@escaping ((_ message:String) -> Void)) {
        
        let indicator = ActivityIndicator()
        indicator.start()
        
        let listVipByCategoryUrl = UserDefaultsManager().isBusinessAccountActivated ? KAPIListBusinessVipByCategory  : KAPIListVipByCategory
        
        WebService.request(method: .get, url: listVipByCategoryUrl + categoryId, parameter: nil, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject],
                            let success =   response["success"] as? Bool else {return}
            
                        if success {
                            if let data = response["data"] as? [String:AnyObject] {
                                if let viplistData = Mapper<VipGroupCategory>().map(JSON: data){
            
                                    successBlock(viplistData)
                                    indicator.stop()
                                }
            
                            }
                        }
        }) { (message, apiError) in
            failureBlock(message)
            indicator.stop()
        }

    }
    
    static func deleteVipFromCategory(_ categoryId : String,parameter : [String:AnyObject],successBlock:@escaping (_ successMsg :String) -> Void, failureBlock:@escaping ((_ message:String) -> Void)){
        
        let indicator = ActivityIndicator()
        indicator.start()
        let deleteVipFromCategoryUrl = UserDefaultsManager().isBusinessAccountActivated ? KAPIDeleteBusinessVipFromCategory  : KAPIDeleteVipFromCategory
//        let userAcessToken =  [
//            "accessToken" : UserDefaultsManager().userAccessToken!
//        ]
        
        WebService.request(method: .delete, url: deleteVipFromCategoryUrl + categoryId, parameter: parameter, header: nil, isAccessTokenRequired: true, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject],
                            let success =   response["success"] as? Bool else {return}
            
                        if success {
                            if let data = response["message"] as? String {
                                successBlock(data)
                                indicator.stop()
                            }
                        }
        }) { (message, apiError) in
            failureBlock(message)
            indicator.stop()
        }
        
    }
    
    
}
