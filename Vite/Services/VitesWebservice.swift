//
//  VitesWebservice.swift
//  Vite
//
//  Created by Eeposit1 on 4/3/16.
//  Copyright © 2016 EeposIT_X. All rights reserved.
//

import UIKit
import ObjectMapper

class VitesWebservice {
    let failureMessage = "Failed to get vite details"
    func getViteDetail(_ viteID:String,
                       successBlock:@escaping ((MyVites) -> ()),
                       failure:@escaping ((_ message:String) -> ())) {
        
        let getViteDetailURL = "\(kAPIGetViteDetail)/\(viteID)/\(UserDefaultsManager().userFBID!)"
        
        WebService.request(method: .get, url: getViteDetailURL, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            guard let param = response["params"] as?  [String:AnyObject] else {
                return
            }
            print(response)
            if success {
                if  let vite = param["viteDetail"] as? [String:AnyObject], let event = vite["event"] as? [String: AnyObject] {
                    let vites = MyVites()
                    
                    vites.eventID            = event["entityId"] as? String
                    vites.acceptedDate       = vite["acceptedDate"] as? String
                    vites.eventTitle         = event["eventTitle"] as? String
                    vites.imageUrl           = event["imageUrl"] as? String
                    var eventLocation        = event["location"] as? [String:AnyObject]
                    vites.latitude           = eventLocation?["latitude"] as? String
                    vites.longitude          = eventLocation?["longitude"] as? String
                    var organizer            = event["organizer"] as? [String:AnyObject]
                    vites.fullName           = organizer?["fullName"] as? String
                    vites.profileImageUrl    = organizer?["profileImageUrl"] as? String
                    vites.eventDate          = event["eventDate"] as? String
                    vites.eventStartTime     = event["eventStartTime"] as? String
                    vites.fileType           = event["fileType"] as? String
                    vites.videoProfileThumbNail = event["videoProfileThumbnail"] as? String
                    if let videoUrl = event["profileVideoUrl"] as? String {
                        vites.eventVideoUrl = videoUrl
                    }
                    if let organizer = event["organizer"] as? [String:AnyObject], let fbID = organizer["facebookId"] {
                        vites.organizerFBID = String(describing: fbID)
                    }
                    vites.eventDetail        = vite["event"]!["eventDetail"] as? String
                    vites.viteStatus         = vite["viteStatus"] as? String
                    vites.qrImagePath        = vite["qrImagePath"] as? String
                    vites.eventGivenLocation = eventLocation?["givenLocation"] as? String
                    vites.rated             =  vite["rated"] as? Bool
                    if let mediaData = event["media"] as? [[String: AnyObject]]{
                        var mediaDataArr = [VMedia]()
                        for obj in mediaData {
                            let media = Mapper<VMedia>().map(JSON: obj)
                            if let m = media {
                                mediaDataArr.append(m)
                            }
                        }
                        vites.media = mediaDataArr
                    }
                    successBlock(vites)
                }
            }
        }) { (msg, err) in
              failure(msg)
        }
    }
    
    
    
    
    func deleteVite(_ eventID:String,facebookID:String,
                    success:@escaping (() -> ()),
                    failure:@escaping ((_ message:String) -> ())) {
        
        let deleteViteURL = kAPIDeleteMyVite + "/" + eventID + "/" + facebookID
        
        WebService.request(method: .post, url: deleteViteURL, success: { (response) in
              success()
        }) { (msg, err) in
             failure(msg)
        }
    }
    
    static func screenShotDetail(_ eventId:String,successBlock:@escaping ((_ message: String) -> ()), failureBlock:@escaping ((_ message:String) -> ())) {
        WebService.request(method: .put, url: KAPIScreenShotTaken + eventId, success: { (response) in
            guard let response = response.result.value as? [String:AnyObject] else {
                print("Network Error!")
                return
            }
            guard let success = response["success"] as? Bool else {
                return
            }
            if success{
                if let msg = response["message"] as? String{
                    successBlock(msg)
                }
            }
        }) { (msg, err) in
             failureBlock(msg)
        }
    }
    
}
