//
//  UserData.swift
//  Vite
//
//  Created by Eeposit 01 on 12/8/17.
//  Copyright © 2017 Eepos IT. All rights reserved.
//

import UIKit
import Foundation

class UserDefaultsManager {
    
    var ud = UserDefaults.standard
    
    private  var userFBIdKey = "kUserFBID"
    private  var userFBFirstNameKey = "kUserFBFirstName"
    private  var userFBFullNameKey = "kUserFBName"
    private  var userFBProfileUrlKey = "kUserFBProfileURL"
    private  var userFriendListKey = "kUserFriendsList"
    private  var userCurrentLatitudeKey = "kUserLatitude"
    private  var userCurrentLongitudeKey = "kUserLongitude"
    private  var userFBGenderKey = "kUserFBGender"
    private  var userAgeKey = "kUserAge"
    private  var userDeviceTokenKey = "userDeviceToken"
    private  var userAccessTokenKey = "kAccessToken"
    private  var userFBWorkKey      = "kUserFBWork"
    private  var userPremiumStatusKey  = "KPremiumStatus"
    private  var celebVerificationStatusKey = "kCelebVerificationStatus"
    private  var eliteVerificationStatusKey = "kEliteVerificationStatus"
    private  var loginSuccessStatuskey  = "kLoginSuccesStatus"
    private  var userBusinessAcoountIdKey = "kBusinessAccountID"
    private  var userBusinessOwnerIdKey  = "kIsBusinessOwnerId"
    private  var userBusinessFullNameKey = "kBusinessFullName"
    private  var userBusinessProfileImageUrlKey = "kBusinessProfileURL"
    private  var businessAccountActivatedKey = "businessAccountActivated"
    private  var nearViteIdKey = "kNearViteId"
    private  var nearViteTitleKey = "kNearViteTitle"
    private  var nearViteCameraKey = "kNearViteCameraShow"
    private  var paypalConnectionStatus = "kpaypalConnectionStatus"
    
    
    var userFriendsList:[String]? {
        get {
            return ud.object(forKey: userFriendListKey) as? [String]
        } set {
            ud.set(newValue, forKey: userFriendListKey)
        }
    }
    
    var userCurrentLatitude:String? {
        get {
            return ud.object(forKey: userCurrentLatitudeKey) as? String
        } set  {
            ud.set(newValue, forKey: userCurrentLatitudeKey)
            //  let notificationCenter = NotificationCenter
            //   notificationCenter.postNotificationName(kNotificationLocationUpdated, object: nil)
        }
    }
    
    var userCurrentLongitude:String? {
        get {
            return ud.object(forKey: userCurrentLongitudeKey) as? String
        } set  {
            ud.set(newValue, forKey: userCurrentLongitudeKey)
        }
    }
    
    var userAge:Int? {
        get {
            return ud.object(forKey: userAgeKey) as? Int
        } set {
            ud.set(newValue, forKey: userAgeKey)
        }
    }
    
    var userDeviceToken:String? {
        get {
            return ud.object(forKey: userDeviceTokenKey) as? String
        } set  {
            ud.set(newValue, forKey: userDeviceTokenKey)
        }
    }
    
    var userAccessToken:String? {
        get {
            return ud.object(forKey: userAccessTokenKey) as? String
        } set  {
            ud.set(newValue, forKey: userAccessTokenKey)
        }
    }
    
    var userPremiumStatus : Bool? {
        get {
            return ud.object(forKey: userPremiumStatusKey) as? Bool
        } set  {
            ud.set(newValue, forKey: userPremiumStatusKey)
        }
        
    }
    
    var celebVerificationStatus:String? {
        get {
            return ud.object(forKey: celebVerificationStatusKey) as? String
        } set {
            ud.set(newValue, forKey: celebVerificationStatusKey)
        }
    }
    
    var eliteVerificationStatus:String? {
        get {
            return ud.object(forKey: eliteVerificationStatusKey) as? String
        } set {
            ud.set(newValue, forKey: eliteVerificationStatusKey)
        }
    }
    
    var loginSuccessStatus: Bool? {
        get {
            return ud.object(forKey: loginSuccessStatuskey) as? Bool
        } set {
            ud.set(newValue, forKey: loginSuccessStatuskey)
        }
    }
    
    var nearViteId:String? {
        get {
            return ud.object(forKey: nearViteIdKey) as? String
        } set {
            ud.set(newValue, forKey: nearViteIdKey)
        }
    }
    
    var nearViteTitle:String? {
        get {
            return ud.object(forKey: nearViteTitleKey) as? String
        } set {
            ud.set(newValue, forKey: nearViteTitleKey)
        }
    }
    
    var nearViteCameraShown: Bool? {
        get {
            return ud.bool(forKey: nearViteCameraKey)
        } set {
            ud.set(newValue, forKey: nearViteCameraKey)
        }
    }
    
    var userPaypalConnectionStatus:Bool? {
        get {
            return ud.object(forKey: paypalConnectionStatus) as? Bool
        } set {
            ud.set(newValue, forKey: paypalConnectionStatus)
        }
    }
}

//MARK: FB userDefaults
extension UserDefaultsManager {
    
    var userFBID:String? {
        get {
            //return "10153939489824731" // phillip
            return  ud.object(forKey: userFBIdKey) as? String
            //ud.objectForKey(userFBIdKey) as? String
        } set {
            //Should be saved while user has successfully logged in through facebook SDK.
            //And logged verified from our server.
            //
            ud.set(newValue, forKey: userFBIdKey)
        }
    }
    
    var userFBFirstName:String? {
        get {
            return ud.object(forKey: userFBFirstNameKey) as? String
        } set {
            ud.set(newValue, forKey: userFBFirstNameKey)
        }
    }
    
    var userFBFullName:String? {
        get {
            return ud.object(forKey: userFBFullNameKey) as? String
        } set {
            ud.set(newValue, forKey: userFBFullNameKey)
        }
    }
    
    var userFBProfileURL:String? {
        get {
            return ud.object(forKey: userFBProfileUrlKey) as? String
        } set {
            ud.set(newValue, forKey: userFBProfileUrlKey)
        }
    }
    
    var userFBWork:String? {
        get {
            return ud.object(forKey: userFBWorkKey) as? String
        } set {
            ud.set(newValue, forKey: userFBWorkKey)
        }
    }
    
    var userFBGender:String? {
        get {
            return ud.object(forKey: userFBGenderKey) as? String
        } set {
            ud.set(newValue, forKey: userFBGenderKey)
        }
    }
}

//MARK: Bussiness account userdefualts
extension UserDefaultsManager {
    
    var userBusinessAccountID:String? {
        get {
            return ud.object(forKey: userBusinessAcoountIdKey) as? String
        } set {
            //Should be saved while user has successfully logged in through facebook SDK.
            //And logged verified from our server.
            //
            ud.set(newValue, forKey: userBusinessAcoountIdKey)
        }
    }
    
    var userBusinessOwnerId:String? {
        get {
            return ud.object(forKey: userBusinessOwnerIdKey) as? String
        } set {
            ud.set(newValue, forKey: userBusinessOwnerIdKey)
        }
    }
    
    var userBusinessFullName:String? {
        get {
            return ud.object(forKey: userBusinessFullNameKey) as? String
        } set {
            ud.set(newValue, forKey: userBusinessFullNameKey)
        }
    }
    
    var userBusinessProfileImageUrl:String? {
        get {
            return ud.object(forKey: userBusinessProfileImageUrlKey) as? String
        } set {
            ud.set(newValue, forKey: userBusinessProfileImageUrlKey)
        }
    }
    
    var isBusinessAccountActivated: Bool {
        get {
            return ud.bool(forKey: businessAccountActivatedKey)
        } set {
            ud.set(newValue, forKey: businessAccountActivatedKey)
        }
    }
}
extension UserDefaultsManager {
    
    static func getActiveUserForNotification() -> NSender {
        var sender:NSender!
        let userDefault = UserDefaultsManager()
        if userDefault.isBusinessAccountActivated {
            sender = NSender(uid: userDefault.userBusinessAccountID!, name: userDefault.userBusinessFullName!, imageUrl: userDefault.userBusinessProfileImageUrl!)
        } else {
            sender =  NSender(uid: userDefault.userFBID!, name: userDefault.userFBFullName!, imageUrl: userDefault.userFBProfileURL!)
        }
        return sender
    }
    
    static func getCurrentChatUser() -> ChatUser {
        let userDefault = UserDefaultsManager()
        let user             = ChatUser()
        user.fbID            =  userDefault.isBusinessAccountActivated ? userDefault.userBusinessAccountID! : userDefault.userFBID!
        user.name            =  userDefault.isBusinessAccountActivated ? userDefault.userBusinessFullName! : userDefault.userFBFullName!
        user.profileImageUrl =  userDefault.isBusinessAccountActivated ? userDefault.userBusinessProfileImageUrl :"http://graph.facebook.com/\(userDefault.userFBID!)/picture?type=large"
        return user
    }
    
    //MARK: LinkedIn User defaults
    
    var linkedInId:String?{
        get {
            return ud.object(forKey: "linkedInId") as? String
            
        } set {
            ud.set(newValue, forKey: "linkedInId")
        }
    }
    
    var linkedInUserName:String?{
        get {
            return ud.object(forKey: "linkedInUserName") as? String
            
        } set {
            ud.set(newValue, forKey: "linkedInUserName")
            
        }
    }
    
    //MARK: Twitter User defaults
    
    var twitterID:String? {
        get {
            return ud.object(forKey: "ktwitterID") as? String
        } set {
            ud.set(newValue, forKey: "ktwitterID")
        }
    }
    
    var twitterUserName:String? {
        get {
            return ud.object(forKey: "ktwitterUserName") as? String
        } set {
            ud.set(newValue, forKey: "ktwitterUserName")
        }
    }
    
    var twitterScreenName:String? {
        get {
            return ud.object(forKey: "twitterScreenName") as? String
        } set {
            ud.set(newValue, forKey: "twitterScreenName")
        }
    }
    
    //MARK: Instagram User defaults
    
    var instaId:String? {
        
        get {
            return ud.object(forKey: "InstaId") as? String
            
        } set {
            ud.set(newValue, forKey: "InstaId")
            
        }
    }
    
    var instaUserName:String?{
        get{
            return ud.object(forKey: "kInstaUserName") as? String
        }
        set{
            ud.set(newValue, forKey:"kInstaUserName")
        }
        
    }
    
}

//MARK: userdefault private function
extension UserDefaultsManager {
    
    static func saveUserFBResponse(fbResponse:FBLoginResponse) {
        //Save facebookID in userdefaults as we need to use this id as parameter in next request.
        let ud              = UserDefaultsManager()
        ud.userFBID         = fbResponse.fbID
        ud.userFBFullName   = fbResponse.fullName
        if (ud.userFBProfileURL == nil ){
            ud.userFBProfileURL = fbResponse.pictureURL
        }
        ud.userFBGender     = fbResponse.gender
        ud.userFBFirstName  =  fbResponse.firstName
        ud.userFBWork = fbResponse.work
    }
    
    static func saveUserFriendsToUserDefaults() {
        FacebookLoginService.friendListOfUser() {
            friends in
            guard let friends = friends else {
                return
            }
            var arrayOfIds = [String]()
            for friend in friends {
                arrayOfIds.append(friend.fbID!)
            }
            let user = UserDefaultsManager()
            user.userFriendsList = arrayOfIds
            
            //add friends of user in server
            if user.userFriendsList?.count != 0 {
                let parameters:[String:Any] = [
                    "fbIdList" : user.userFriendsList!
                ]
                
                WebService.request(method: .post, url: kAPIAddFriendList + user.userFBID!, parameter: parameters as [String : AnyObject], header: nil, success: { (response) in
                }, failure: { (messsage, apiError) in
                    showAlert(controller: self, title: localizedString(forKey: "VITE_EXCLUSIVE_EVENT"), message: messsage)
                })
                
            }
            
        }
    }
    static func saveUserLocationToUserDefaults(locManager:LocationManager) {
        
        locManager.getUserLocation()
        locManager.fetchedUserLocation = {
            location in
            let user = UserDefaultsManager()
            //Save user current location to UserDefaults
            user.userCurrentLatitude  = String(location.latitude)
            user.userCurrentLongitude = String(location.longitude)
            let parameter =
                [
                    "latitude":user.userCurrentLatitude! as AnyObject,
                    "longitude":user.userCurrentLongitude! as AnyObject
            ]
            WebService.request(method: .post, url: kAPIUpdateUserLocation + user.userFBID!, parameter: parameter, header: nil, isAccessTokenRequired: false, success: { (response) in
                guard let response = response.result.value as? [String:AnyObject] else {
                    return
                }
                let Rmessage = response["success"] as! Int
                if Rmessage == 1 {
                    //print("User location successfully updated")
                } else {
                    //print("User location update failed")
                }
            }, failure: { (message, apiError) in
                 print(message)
            })
            FNode.lastUpdatedLocation.child(user.userFBID!).setValue(parameter)
        }
    }
}
